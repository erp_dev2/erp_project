﻿/*
    01/10/2017 [TKG] Ages Of Items (KMI)
    08/10/2017 [TKG] tambah filter batch#, lot dan bin
*/

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgesOfItems : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty;
        private int mNumberOfInventoryUomCode = 1;

        #endregion

        #region Constructor

        public FrmRptAgesOfItems(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueItCtCode(ref LueItCtCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Warehouse",
                        "Type",
                        "Document#",
                        "Date",
                        "Item's Code",

                        //6-10
                        "Item's Name",
                        "Foreign Name",
                        "Category",
                        "Property",
                        "Batch#",
                        
                        //11-15
                        "Source",
                        "Lot",
                        "Bin",
                        "Quantity",
                        "UoM",                        
                        
                        //16-20
                        "Quantity", 
                        "UoM",
                        "Quantity", 
                        "UoM",
                        "Age"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 120, 140, 100, 80, 
                        
                        //6-10
                        150, 200, 180, 60, 200, 
 
                        //11-15
                        180, 60, 60, 80, 80, 

                        //16-20
                        80, 80, 80, 80, 80
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 16, 18, 20 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 9, 11, 12, 13, 16, 17, 18, 19 }, false);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 9, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            
            var lResult1 = new List<Result1>();
            var lResult2 = new List<Result2>();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref lResult1);
                if (lResult1.Count > 0)
                {
                    Process2(ref lResult1);
                    Process3(ref lResult1, ref lResult2);
                    if (lResult2.Count > 0)
                        Process4(ref lResult1, ref lResult2);
                    Process5(ref lResult1);
                    if (lResult1.Count > 0)
                        Process6(ref lResult1);
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lResult1.Clear();
                lResult2.Clear();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void Process1(ref List<Result1> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = " ";
            
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "B.ItCtCode", true);
            Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", false);
            Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "A.Lot", false);
            Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "A.Bin", false);
            Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "B.ItName", "B.ForeignName" });

            SQL.AppendLine("Select A.WhsCode, I.WhsName, A.Lot, A.Bin, A.Source, ");
            SQL.AppendLine("A.ItCode, B.ItName, B.ForeignName, C.ItCtName, H.PropName, A.BatchNo, ");
            SQL.AppendLine("A.Qty, B.InventoryUomCode, A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3, ");
            SQL.AppendLine("E.DocNo As RecvVdDocNo, E.DocDt As RecvVdDocDt, ");
            SQL.AppendLine("G.DocNo As StockInitialDocNo, G.DocDt As StockInitialDocDt ");
            SQL.AppendLine("From TblStockSummary A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
            SQL.AppendLine("Left Join TblRecvVdDtl D On A.Source=D.Source ");
            SQL.AppendLine("Left Join TblRecvVdHdr E On D.DocNo=E.DocNo And A.WhsCode=E.WhsCode ");
            SQL.AppendLine("Left Join TblStockInitialDtl F On A.Source=F.Source ");
            SQL.AppendLine("Left Join TblStockInitialHdr G On F.DocNo=G.DocNo And A.WhsCode=G.WhsCode ");
            SQL.AppendLine("Left Join TblProperty H On A.PropCode=H.PropCode ");
            SQL.AppendLine("Inner Join TblWarehouse I On A.WhsCode=I.WhsCode ");
            SQL.AppendLine("Where A.Qty>0 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "WhsCode", 
                    
                    //1-5
                    "WhsName",
                    "Lot", 
                    "Bin", 
                    "Source",
                    "ItCode",

                    //6-10
                    "ItName", 
                    "ForeignName", 
                    "ItCtName", 
                    "PropName", 
                    "BatchNo", 
                    
                    //11-15
                    "Qty", 
                    "InventoryUomCode", 
                    "Qty2", 
                    "InventoryUomCode2", 
                    "Qty3", 
                    
                    //16-20
                    "InventoryUomCode3", 
                    "RecvVdDocNo", 
                    "RecvVdDocDt", 
                    "StockInitialDocNo", 
                    "StockInitialDocDt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result1()
                        {
                            WhsCode = Sm.DrStr(dr, c[0]),
                            WhsName = Sm.DrStr(dr, c[1]),
                            Lot = Sm.DrStr(dr, c[2]),
                            Bin = Sm.DrStr(dr, c[3]),
                            Source = Sm.DrStr(dr, c[4]),
                            ItCode = Sm.DrStr(dr, c[5]),
                            ItName = Sm.DrStr(dr, c[6]),
                            ForeignName = Sm.DrStr(dr, c[7]),
                            ItCtName  = Sm.DrStr(dr, c[8]),
                            PropName  = Sm.DrStr(dr, c[9]),
                            BatchNo  = Sm.DrStr(dr, c[10]),
                            Qty  = Sm.DrDec(dr, c[11]),
                            InventoyUomCode = Sm.DrStr(dr, c[12]),
                            Qty2 = Sm.DrDec(dr, c[13]),
                            InventoyUomCode2 = Sm.DrStr(dr, c[14]),
                            Qty3 = Sm.DrDec(dr, c[15]),
                            InventoyUomCode3 = Sm.DrStr(dr, c[16]),
                            RecvVdDocNo  = Sm.DrStr(dr, c[17]), 
                            RecvVdDocDt  = Sm.DrStr(dr, c[18]),
                            StockInitialDocNo  = Sm.DrStr(dr, c[19]),
                            StockInitialDocDt = Sm.DrStr(dr, c[20]),
                            RecvWhsDocNo = string.Empty,
                            RecvWhsDocDt = string.Empty,
                            Type = string.Empty,
                            Age = 0
                        });
                    }
                }
                dr.Close();
            }

        }

        private void Process2(ref List<Result1> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].RecvVdDocNo.Length != 0) 
                    l[i].Type = "1";

                if (l[i].StockInitialDocNo.Length != 0)
                    l[i].Type = "2";

                if (l[i].Type.Length==0)
                    l[i].Type = "3";
            }
        }

        private void Process3(ref List<Result1> l1, ref List<Result2> l2)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = string.Empty;
            int i = 0;

            foreach (var x in l1.Where(r => r.Type=="3"))
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += 
                    "(A.WhsCode=@WhsCode0" + i.ToString() + 
                    " And C.Lot=@Lot0" + i.ToString() + 
                    " And C.Bin=@Bin0" + i.ToString() + 
                    " And C.Source=@Source0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@WhsCode0" + i.ToString(), x.WhsCode);
                Sm.CmParam<String>(ref cm, "@Lot0" + i.ToString(), x.Lot);
                Sm.CmParam<String>(ref cm, "@Bin0" + i.ToString(), x.Bin);
                Sm.CmParam<String>(ref cm, "@Source0" + i.ToString(), x.Source);
                i++;
            }

            if (Filter.Length > 0)
                Filter = " And ( " + Filter + ")  ";
            else
                Filter = " And 1=0; ";

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

            SQL.AppendLine("Select A.WhsCode, C.Lot, C.Bin, C.Source, A.DocNo, A.DocDt ");
            SQL.AppendLine("From TblRecvWhsHdr A ");
            SQL.AppendLine("Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "WhsCode", 
                    
                    //1-5
                    "Lot", 
                    "Bin", 
                    "Source",
                    "DocNo", 
                    "DocDt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new Result2()
                        {
                            WhsCode = Sm.DrStr(dr, c[0]),
                            Lot = Sm.DrStr(dr, c[1]),
                            Bin = Sm.DrStr(dr, c[2]),
                            Source = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5])
                        });
                    }
                }
                dr.Close();
            }

        }

        private void Process4(ref List<Result1> l1, ref List<Result2> l2)
        {
            foreach (var i in l1.Where(r => r.Type == "3"))
            {
                foreach (var j in l2.Where(r => 
                    Sm.CompareStr(r.WhsCode, i.WhsCode) &&
                    Sm.CompareStr(r.Lot, i.Lot) &&
                    Sm.CompareStr(r.Bin, i.Bin) &&
                    Sm.CompareStr(r.Source, i.Source)
                    )
                    .OrderBy(o => o.DocDt)
                    .ThenBy(o => o.DocNo))
                {
                    i.RecvWhsDocNo = j.DocNo;
                    i.RecvWhsDocDt = j.DocDt;
                    break;
                }   
            }
        }

        private void Process5(ref List<Result1> l)
        {
            var Dt1 = Sm.Left(Sm.GetDte(DteDocDt1), 8);
            var Dt2 = Sm.Left(Sm.GetDte(DteDocDt2), 8);
            var CurrentDt = Sm.Left(Sm.ServerCurrentDateTime(), 8);

            DateTime Date1 = new DateTime(
                Int32.Parse(Dt1.Substring(0, 4)),
                Int32.Parse(Dt1.Substring(4, 2)),
                Int32.Parse(Dt1.Substring(6, 2)),
                0, 0, 0
                );

            DateTime Date2 = new DateTime(
                Int32.Parse(CurrentDt.Substring(0, 4)),
                Int32.Parse(CurrentDt.Substring(4, 2)),
                Int32.Parse(CurrentDt.Substring(6, 2)),
                0, 0, 0
                );

            for (int i = l.Count-1; i>=0; i--)
            {
                if (l[i].Type == "1")
                {
                    if (l[i].RecvVdDocDt.Length > 0)
                    {
                        if (Sm.CompareDtTm(l[i].RecvVdDocDt, Dt1) == -1 ||
                            Sm.CompareDtTm(Dt2, l[i].RecvVdDocDt) == -1)
                            l.RemoveAt(i);
                        else
                        {
                            Date1 = new DateTime(
                                Int32.Parse(l[i].RecvVdDocDt.Substring(0, 4)),
                                Int32.Parse(l[i].RecvVdDocDt.Substring(4, 2)),
                                Int32.Parse(l[i].RecvVdDocDt.Substring(6, 2)),
                                0, 0, 0
                                );
                            l[i].Age = (Date2 - Date1).Days + 1;
                        }
                    }
                }
                else
                {
                    if (l[i].Type == "2")
                    {
                        if (l[i].StockInitialDocDt.Length > 0)
                        {
                            if (Sm.CompareDtTm(l[i].StockInitialDocDt, Dt1) == -1 ||
                                Sm.CompareDtTm(Dt2, l[i].StockInitialDocDt) == -1)
                                l.RemoveAt(i);
                            else
                            {
                                Date1 = new DateTime(
                                    Int32.Parse(l[i].StockInitialDocDt.Substring(0, 4)),
                                    Int32.Parse(l[i].StockInitialDocDt.Substring(4, 2)),
                                    Int32.Parse(l[i].StockInitialDocDt.Substring(6, 2)),
                                    0, 0, 0
                                    );
                                l[i].Age = (Date2 - Date1).Days + 1;
                            }
                        }
                    }
                    else
                    {
                        if (l[i].Type == "3")
                        {
                            if (l[i].RecvWhsDocDt.Length == 0)
                                l.RemoveAt(i);
                            else
                            {
                                Date1 = new DateTime(
                                    Int32.Parse(l[i].RecvWhsDocDt.Substring(0, 4)),
                                    Int32.Parse(l[i].RecvWhsDocDt.Substring(4, 2)),
                                    Int32.Parse(l[i].RecvWhsDocDt.Substring(6, 2)),
                                    0, 0, 0
                                    );
                                l[i].Age = (Date2 - Date1).Days + 1;
                            }
                        }
                    }
                }
            }
        }

        private void Process6(ref List<Result1> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[1].Value = l[i].WhsName;
                switch (l[i].Type)
                { 
                    case "1":
                        r.Cells[2].Value = "Vendor";
                        r.Cells[3].Value = l[i].RecvVdDocNo;
                        if (l[i].RecvVdDocDt.Length>0) r.Cells[4].Value = Sm.ConvertDate(l[i].RecvVdDocDt);
                        break;
                    case "2":
                        r.Cells[2].Value = "Initial";
                        r.Cells[3].Value = l[i].StockInitialDocNo;
                        if (l[i].StockInitialDocDt.Length > 0) r.Cells[4].Value = Sm.ConvertDate(l[i].StockInitialDocDt);
                        break;
                    case "3":
                        r.Cells[2].Value = "Other Warehouse";
                        r.Cells[3].Value = l[i].RecvWhsDocNo;
                        if (l[i].RecvWhsDocDt.Length > 0) r.Cells[4].Value = Sm.ConvertDate(l[i].RecvWhsDocDt);
                        break;
                }
                r.Cells[5].Value = l[i].ItCode;
                r.Cells[6].Value = l[i].ItName;
                r.Cells[7].Value = l[i].ForeignName;
                r.Cells[8].Value = l[i].ItCtName;
                r.Cells[9].Value = l[i].PropName;
                r.Cells[10].Value = l[i].BatchNo;
                r.Cells[11].Value = l[i].Source;
                r.Cells[12].Value = l[i].Lot;
                r.Cells[13].Value = l[i].Bin;
                r.Cells[14].Value = l[i].Qty;
                r.Cells[15].Value = l[i].InventoyUomCode;
                r.Cells[16].Value = l[i].Qty2;
                r.Cells[17].Value = l[i].InventoyUomCode2;
                r.Cells[18].Value = l[i].Qty3;
                r.Cells[19].Value = l[i].InventoyUomCode3;
                r.Cells[20].Value = l[i].Age;
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 14, 16, 18 });
            Grd1.EndUpdate();
        }

        private void GetParameter()
        {
            var NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length != 0)
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19 }, true);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

       
        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        #endregion

        #region Class

        private class Result1
        {
            public string Type { get; set; }
            public string WhsCode { get; set; }
            public string WhsName { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string Source { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ForeignName { get; set; }
            public string ItCtName { get; set; }
            public string PropName { get; set; }
            public string BatchNo { get; set; }
            public decimal Qty { get; set; }
            public string InventoyUomCode { get; set; }
            public decimal Qty2 { get; set; }
            public string InventoyUomCode2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoyUomCode3 { get; set; }
            public string RecvVdDocNo { get; set; }
            public string RecvVdDocDt { get; set; }
            public string StockInitialDocNo { get; set; }
            public string StockInitialDocDt { get; set; }
            public string RecvWhsDocNo { get; set; }
            public string RecvWhsDocDt { get; set; }
            public int Age { get; set; }
        }

        private class Result2
        {
            public string WhsCode { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string Source { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
        }

        #endregion
    }
}
