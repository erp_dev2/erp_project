﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDOVdFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDOVd mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDOVdFind(FrmDOVd FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueVdCode(ref LueVdCode);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CancelInd, B.RecvVdDocNo, C.DocDt As RecvVdDocDt, D.WhsName, E.VdName, C.VdDONo, ");
            SQL.AppendLine("B.ItCode, F.ItName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.Qty, F.InventoryUOMCode, B.Qty2, F.InventoryUOMCode2, B.Qty3, F.InventoryUOMCode3, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, F.ItGrpCode ");
            SQL.AppendLine("From TblDOVdHdr A ");
            SQL.AppendLine("Inner Join TblDOVdDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblRecvVdHdr C On B.RecvVdDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select WhsCode From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=D.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblVendor E On A.VdCode=E.VdCode ");
            SQL.AppendLine("Inner Join TblItem F On B.ItCode=F.ItCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date", 
                        "Cancel",
                        "Received#",
                        "WareHouse",
                        

                        //6-10
                        "Vendor",
                        "Vendor's"+Environment.NewLine+"DO#",
                        "Item's Code",
                        "Item's Name",
                        "Batch#",
                        
                        //11-15
                        "Source",
                        "Lot",
                        "Bin",
                        "Quantity 1",
                        "Uom 1",
                        
                        //16-20
                        "Quantity 2",
                        "Uom 2",
                        "Quantity 3",
                        "Uom 3",
                        "Created"+Environment.NewLine+"By",
                        
                        //21-25
                        "Created"+Environment.NewLine+"Date",    
                        "Created"+Environment.NewLine+"Time",                       
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",

                        //26
                        "Group"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 16, 18 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 21, 24 });
            Sm.GrdFormatTime(Grd1, new int[] { 22, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 10, 11, 12, 13, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 }, false);
            ShowInventoryUomCode();
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[26].Visible = true;
                Grd1.Cols[26].Move(9);
            }
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 10, 11, 12, 13, 20, 21, 22, 23, 24, 25 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0=0 ";

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " And B.CancelInd='N' ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtRecvVdDocNo.Text, "B.RecvVdDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtVdDONo.Text, "C.VdDONo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "F.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "B.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtSource.Text, "B.Source", false);

                Sm.ShowDataInGrid(
                        ref Grd1,
                        ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        { 
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "CancelInd", "RecvVdDocNo", "WhsName", "VdName", 
                            
                            //6-10
                            "VdDoNo", "ItCode", "ItName", "BatchNo", "Source", 
                            
                            //11-15
                            "Lot", "Bin", "Qty", "InventoryUomCode", "Qty2", 
                            
                            //16-20
                            "InventoryUomCode2", "Qty3", "InventoryUomCode3", "CreateBy", "CreateDt", 
                            
                            //21-23
                            "LastUpBy", "LastUpDt", "ItGrpCode" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 26, 23);
                        }, true, true, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }


        private void TxtRecvVdDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkRecvVdDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Received document number");
        }

        private void TxtVdDONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdDONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor's DO number");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch number");
        }

        private void TxtSource_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSource_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Source");
        }

        #endregion

        #endregion
    }
}
