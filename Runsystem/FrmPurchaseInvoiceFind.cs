﻿#region Update
/*
    21/08/2017 [HAR] di find tambah filter receiving document 
    01/10/2017 [TKG] tambah inventory UoM
    12/10/2017 [TKG] tampilan foreign name salah
    29/12/2017 [TKG] tambah invoice's tax
    13/04/2018 [TKG] filter by site
    26/05/2018 [HAR] wapu dan non wapu di kolom sendirikan
    18/09/2018 [MEY] tambah kolom Tax Rate Amount
    02/10/2018 [HAR] FTP download file
    05/10/2018 [TKG] Bug grid read only false 
    06/03/2018 [HAR] Bug waktu export to excel nilai decimalnya berlebihan  
    29/11/2019 [DITA/IMS] tambah kolom project code, project name, customer po no
    02/03/2020 [TKG/IMS] Berdasarkan parameter IsPITotalWithoutTaxInclDownpaymentEnabled, Total without tax include downpayment
    15/11/2021 [YOG/AMKA] Membuat Menu Purchase Invoice saat menarik data, hanya data berdasarkan warehouse grup user yang ditarik
    27/01/2022 [DEV/PHT] Pada Purchase Invoice, membuat data Unit Price, Discount %, Discount Amount, Rounding Value di PI menarik data dari Purchase Order tab Revision dengan parameter IsPurchaseInvoiceUsePORevision
    03/08/2022 [SET/SIER] LueVdCode otorisasi Group user
    13/09/2022 [SET/SIER] Feedback : penyesuain ShowData() berdasarkan parameter IsFilterByVendorCategory
    17/01/2023 [SET/BBT] menampilkan status dokumen
    24/01/2023 [SET/BBT] Penambahan Local Document
    23/03/2023 [BRI/PHT] bug tampilan
    18/04/2023 [ISN/MNET] Penambahan kolom Tax Invoice# ketika find dan excel dokumen Purchase Invoice
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using System.Xml;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoiceFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPurchaseInvoice mFrmParent;
        private string mSQL = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        private string mDocTitle = string.Empty;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmPurchaseInvoiceFind(FrmPurchaseInvoice FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueVdCode(ref LueVdCode, mFrmParent.mIsFilterByVendorCategory ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, K.TIN, K.VdName, K.Address, A.VdInvNo, Q.VdDONo, A.LocalDocNo, ");
            SQL.AppendLine("Case When A.TaxCode1 = 'PPN2' Then 'TaxCode1' ");
            SQL.AppendLine("When A.TaxCode2 = 'PPN2' Then 'TaxCode2' ");
            SQL.AppendLine("When A.TaxCode3 = 'PPN2' Then 'TaxCode3' ");
            SQL.AppendLine("ENd As WAPUFrom, ");
            SQL.AppendLine("Case When A.TaxCode1 = 'PPN2' Then ifnull((A.Amt*L.TaxRate*0.01), 0) ");
            SQL.AppendLine("When A.TaxCode2 = 'PPN2' Then ifnull((A.Amt*M.TaxRate*0.01), 0) ");
            SQL.AppendLine("When A.TaxCode3 = 'PPN2' Then ifnull((A.Amt*N.TaxRate*0.01), 0) ");
            SQL.AppendLine("ENd As WAPU, ");
            SQL.AppendLine("Case When A.TaxCode1 = 'PPN3' Then 'TaxCode1' ");
            SQL.AppendLine("When A.TaxCode2 = 'PPN3' Then 'TaxCode2' ");
            SQL.AppendLine("When A.TaxCode3 = 'PPN3' Then 'TaxCode3' ");
            SQL.AppendLine("ENd As NWAPUFrom, ");
            SQL.AppendLine("Case When A.TaxCode1 = 'PPN3' Then ifnull((A.Amt*L.TaxRate*0.01), 0) ");
            SQL.AppendLine("When A.TaxCode2 = 'PPN3' Then ifnull((A.Amt*M.TaxRate*0.01), 0) ");
            SQL.AppendLine("When A.TaxCode3 = 'PPN3' Then ifnull((A.Amt*N.TaxRate*0.01), 0) ");
            SQL.AppendLine("ENd As NWAPU, ");
            SQL.AppendLine("A.TaxInvoiceNo, L.TaxName TaxName1, ifnull((A.Amt*L.TaxRate*0.01), 0) As PITaxAmt1, ");
            SQL.AppendLine("A.TaxInvoiceNo2, M.TaxName TaxName2, ifnull((A.Amt*M.TaxRate*0.01), 0) As PITaxAmt2, ");
            SQL.AppendLine("A.TaxInvoiceNo3, N.TaxName TaxName3, ifnull((A.Amt*N.TaxRate*0.01), 0) As PITaxAmt3, ");
            SQL.AppendLine("Case When A.Status = 'O' Then 'Outstanding' ");
            SQL.AppendLine("When A.Status = 'A' Then 'Approved' ");
            SQL.AppendLine("When A.Status = 'C' Then 'Cancel' ");
            SQL.AppendLine("End `Status`, ");
            if (mFrmParent.mIsPITotalWithoutTaxInclDownpaymentEnabled)
                SQL.AppendLine("A.Amt+A.TaxAmt As Amt, ");
            else
                SQL.AppendLine("A.Amt+A.TaxAmt-A.Downpayment As Amt, ");
            SQL.AppendLine("B.RecvVdDocNo, G.ItCode, J.ItName, ");
            SQL.AppendLine("C.QtyPurchase, J.PurchaseUomCode, ");
            SQL.AppendLine("C.Qty2, J.InventoryUomCode2, ");
            SQL.AppendLine("C.Qty3, J.InventoryUomCode3, ");
            if (!mFrmParent.mIsPurchaseInvoiceUsePORevision)
            {
                SQL.AppendLine("I.UPrice, E.Discount, E.DiscountAmt, E.RoundingValue, ");
                SQL.AppendLine("Cast((((C.QtyPurchase* I.UPrice * Case When IfNull(E.Discount, 0)=0 Then 1 Else (100-E.Discount)/100 End)-E.DiscountAmt+E.RoundingValue )*Case When IfNull(L.TaxRate, 0)=0 Then 0 Else L.TaxRate/100 End)AS DECIMAL(18,6)) As TaxAmt1, ");
                SQL.AppendLine("Cast((((C.QtyPurchase* I.UPrice * Case When IfNull(E.Discount, 0)=0 Then 1 Else (100-E.Discount)/100 End)-E.DiscountAmt+E.RoundingValue )*Case When IfNull(M.TaxRate, 0)=0 Then 0 Else M.TaxRate/100 End)AS DECIMAL(18,6)) As TaxAmt2, ");
                SQL.AppendLine("Cast((((C.QtyPurchase* I.UPrice * Case When IfNull(E.Discount, 0)=0 Then 1 Else (100-E.Discount)/100 End)-E.DiscountAmt+E.RoundingValue )*Case When IfNull(N.TaxRate, 0)=0 Then 0 Else N.TaxRate/100 End)AS DECIMAL(18,6)) As TaxAmt3,  ");
                SQL.AppendLine("Cast(( ");
                SQL.AppendLine("    ((C.QtyPurchase* I.UPrice * Case When IfNull(E.Discount, 0)=0 Then 1 Else (100-E.Discount)/100 End)-E.DiscountAmt+E.RoundingValue) + ");
                SQL.AppendLine("    (((C.QtyPurchase* I.UPrice * Case When IfNull(E.Discount, 0)=0 Then 1 Else (100-E.Discount)/100 End)-E.DiscountAmt+E.RoundingValue)*Case When IfNull(L.TaxRate, 0)=0 Then 0 Else L.TaxRate/100 End) + ");
                SQL.AppendLine("    (((C.QtyPurchase* I.UPrice * Case When IfNull(E.Discount, 0)=0 Then 1 Else (100-E.Discount)/100 End)-E.DiscountAmt+E.RoundingValue)*Case When IfNull(M.TaxRate, 0)=0 Then 0 Else M.TaxRate/100 End) + ");
                SQL.AppendLine("    (((C.QtyPurchase* I.UPrice * Case When IfNull(E.Discount, 0)=0 Then 1 Else (100-E.Discount)/100 End)-E.DiscountAmt+E.RoundingValue)*Case When IfNull(N.TaxRate, 0)=0 Then 0 Else N.TaxRate/100 End)  ");
                SQL.AppendLine(")AS DECIMAL(18,6)) As Total, ");
            }
            else
            {
                SQL.AppendLine("IfNull(S.Uprice, I.UPrice) As Uprice, IfNull(S.Discount, E.Discount) As Discount, IfNull(S.DiscountAmt, E.DiscountAmt) As DiscountAmt, IfNull(S.RoundingValue, E.RoundingValue) As RoundingValue, ");
                SQL.AppendLine("Cast((((C.QtyPurchase* IfNull(S.Uprice, I.UPrice) * Case When IfNull(IfNull(S.Discount, E.Discount), 0)=0 Then 1 Else (100-IfNull(S.Discount, E.Discount))/100 End)-IfNull(S.DiscountAmt, E.DiscountAmt)+IfNull(S.RoundingValue, E.RoundingValue) )*Case When IfNull(L.TaxRate, 0)=0 Then 0 Else L.TaxRate/100 End)AS DECIMAL(18,6)) As TaxAmt1, ");
                SQL.AppendLine("Cast((((C.QtyPurchase* IfNull(S.Uprice, I.UPrice) * Case When IfNull(IfNull(S.Discount, E.Discount), 0)=0 Then 1 Else (100-IfNull(S.Discount, E.Discount))/100 End)-IfNull(S.DiscountAmt, E.DiscountAmt)+IfNull(S.RoundingValue, E.RoundingValue) )*Case When IfNull(M.TaxRate, 0)=0 Then 0 Else M.TaxRate/100 End)AS DECIMAL(18,6)) As TaxAmt2, ");
                SQL.AppendLine("Cast((((C.QtyPurchase* IfNull(S.Uprice, I.UPrice) * Case When IfNull(IfNull(S.Discount, E.Discount), 0)=0 Then 1 Else (100-IfNull(S.Discount, E.Discount))/100 End)-IfNull(S.DiscountAmt, E.DiscountAmt)+IfNull(S.RoundingValue, E.RoundingValue) )*Case When IfNull(N.TaxRate, 0)=0 Then 0 Else N.TaxRate/100 End)AS DECIMAL(18,6)) As TaxAmt3,  ");
                SQL.AppendLine("Cast(( ");
                SQL.AppendLine("    ((C.QtyPurchase* IfNull(S.Uprice, I.UPrice) * Case When IfNull(IfNull(S.Discount, E.Discount), 0)=0 Then 1 Else (100-IfNull(S.Discount, E.Discount))/100 End)-IfNull(S.DiscountAmt, E.DiscountAmt)+IfNull(S.RoundingValue, E.RoundingValue)) + ");
                SQL.AppendLine("    (((C.QtyPurchase* IfNull(S.Uprice, I.UPrice) * Case When IfNull(IfNull(S.Discount, E.Discount), 0)=0 Then 1 Else (100-IfNull(S.Discount, E.Discount))/100 End)-IfNull(S.DiscountAmt, E.DiscountAmt)+IfNull(S.RoundingValue, E.RoundingValue))*Case When IfNull(L.TaxRate, 0)=0 Then 0 Else L.TaxRate/100 End) + ");
                SQL.AppendLine("    (((C.QtyPurchase* IfNull(S.Uprice, I.UPrice) * Case When IfNull(IfNull(S.Discount, E.Discount), 0)=0 Then 1 Else (100-IfNull(S.Discount, E.Discount))/100 End)-IfNull(S.DiscountAmt, E.DiscountAmt)+IfNull(S.RoundingValue, E.RoundingValue))*Case When IfNull(M.TaxRate, 0)=0 Then 0 Else M.TaxRate/100 End) + ");
                SQL.AppendLine("    (((C.QtyPurchase* IfNull(S.Uprice, I.UPrice) * Case When IfNull(IfNull(S.Discount, E.Discount), 0)=0 Then 1 Else (100-IfNull(S.Discount, E.Discount))/100 End)-IfNull(S.DiscountAmt, E.DiscountAmt)+IfNull(S.RoundingValue, E.RoundingValue))*Case When IfNull(N.TaxRate, 0)=0 Then 0 Else N.TaxRate/100 End)  ");
                SQL.AppendLine(")AS DECIMAL(18,6)) As Total, ");
            }
            SQL.AppendLine("A.CurCode, A.TaxRateAmt, A.PaymentDt, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, J.ForeignName, A.TaxCode1, A.TaxCode2, A.TaxCode3, B.FileName, O.DeptName, P.SiteName, ");
            if(mFrmParent.mIsBOMShowSpecifications)
                SQL.AppendLine("R.ProjectCode, R.ProjectName, R.PONo ");
            else
                SQL.AppendLine("Null As ProjectCode,Null As ProjectName, Null As NTPDocNo, Null As PONo ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblRecvVdDtl C On B.RecvVdDocNo=C.DocNo And B.RecvVdDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblPOHdr D On C.PODocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl E On C.PODocNo=E.DocNo And C.PODNo=E.DNo ");            
            SQL.AppendLine("Inner Join TblPORequestDtl F On E.PORequestDocNo=F.DocNo And E.PORequestDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl G On F.MaterialRequestDocNo=G.DocNo And F.MaterialRequestDNo=G.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr H On F.QtDocNo=H.DocNo ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {      
                SQL.AppendLine("    And H.PtCode Is Not Null ");
                SQL.AppendLine("    And H.PtCode In (");
                SQL.AppendLine("        Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblQtDtl I On F.QtDocNo=I.DocNo And F.QtDNo=I.DNo ");
            SQL.AppendLine("Inner Join TblItem J On G.ItCode=J.ItCode ");
            SQL.AppendLine("Inner Join TblVendor K On A.VdCode=K.VdCode ");
            SQL.AppendLine("Left Join TblTax L On A.TaxCode1=L.TaxCode ");
            SQL.AppendLine("Left Join TblTax M On A.TaxCode2=M.TaxCode ");
            SQL.AppendLine("Left Join TblTax N On A.TaxCode3=N.TaxCode ");
            SQL.AppendLine("Left Join TblDepartment O On A.DeptCode=O.DeptCode ");
            SQL.AppendLine("Left Join TblSite P On A.SiteCode=P.SiteCode ");
            SQL.AppendLine("Inner Join TblRecvVdHdr Q On B.RecvVdDocNo=Q.DocNo ");
            if (mFrmParent.mIsBOMShowSpecifications)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("Select T0.DocNo, Group_Concat(Distinct IFNULL(T11.ProjectCode, T12.ProjectCode2)) ProjectCode,  ");
                SQL.AppendLine(" Group_Concat(Distinct IFNULL(T11.ProjectName, T10.ProjectName)) ProjectName,  Group_Concat(Distinct T12.PONo)PONo ");
                SQL.AppendLine("From TblPurchaseInvoiceHdr T0 ");
                SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl T1 ON T1.DocNo=T0.DocNo  ");
                SQL.AppendLine("Inner Join TblRecvVdDtl T2 ON T1.RecvVdDocNo=T2.DocNo AND T1.RecvVdDNo=T2.DNo  ");
                SQL.AppendLine("Inner Join TblPOHdr T3 ON T2.PODocNo=T3.DocNo  ");
                SQL.AppendLine("Inner Join TblPODtl T4 ON T2.PODocNo=T4.DocNo AND T2.PODNo=T4.DNo  ");
                SQL.AppendLine("Inner Join TblPORequestDtl T5 ON T4.PORequestDocNo=T5.DocNo AND T4.PORequestDNo=T5.DNo  ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl T6 ON T5.MaterialRequestDocNo=T6.DocNo AND T5.MaterialRequestDNo=T6.DNo  ");
                SQL.AppendLine("Inner Join TblBOMRevisionDtl T7 ON T7.DocNo = T6.BOMRDocNo AND T7.DNo = T6.BOMRDNo ");
                SQL.AppendLine("Inner JOIN TblBOMRevisionHdr T8 ON T7.DocNo = T8.DocNo ");
                SQL.AppendLine("Inner Join TblBOQHdr T9 ON T8.BOQDocNo = T9.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr T10 ON T9.LOPDocNo = T10.DocNo  ");
                SQL.AppendLine("Left Join TblProjectGroup T11 ON T10.PGCode = T11.PGCode ");
                SQL.AppendLine("Left Join TblSOContractHdr T12 ON T12.BOQDocNo = T9.DocNo ");
                SQL.AppendLine("Group By T0.DocNo  ");    	
                SQL.AppendLine(")R On R.DocNo = A.DocNo ");                            
            }
            if (mFrmParent.mIsPurchaseInvoiceUsePORevision)
            {
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("    Select T4.DocNo, T4.DNo, T1.DocNo PIDocNo, T2.DNo PIDNo, T5.PODocNo, T5.PODNo, T5.Discount, T5.DiscountAmt, T5.RoundingValue, T5.Qty, T6.Uprice  ");
                SQL.AppendLine("    From TblPurchaseInvoiceHdr T1 ");
                SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("    Inner Join TblRecvVdDtl T3 On T2.RecvVdDocNo = T3.DocNo And T2.RecvVdDNo = T3.DNo ");
                SQL.AppendLine("    Inner Join TblPODtl T4 On T3.PODocNo = T4.DocNo And T3.PODNo = T4.DNo  ");
                SQL.AppendLine("    Inner Join TblPORevision T5 On T4.DocNo = T5.PODocNo And T4.DNo = T5.PODNo  ");
                SQL.AppendLine("    Inner Join TblQtDtl T6 On T5.QtDocNo = T6.DocNo And T5.QtDNo = T6.DNo  ");
                SQL.AppendLine("    Where T5.PODocNo Is Not Null ");
                SQL.AppendLine("	 And T5.DocNo = ( ");
                SQL.AppendLine("	 	Select Max(DocNo) From TblPORevision Where PODocNo = T5.PODocNo And PODNo = T5.PODNo ");
                SQL.AppendLine("	 	Group By PODocNo, PODNo ");
                SQL.AppendLine("	 )   ");
                SQL.AppendLine("    And T1.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine(") S On A.DocNo=S.PIDocNo And B.DNo=S.PIDNo ");
            }
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or (A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }

            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And (A.DeptCode Is Null Or (A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }
            if (mFrmParent.mIsFilterByVendorCategory)
            {
                SQL.AppendLine("And (A.VdCode Is Null Or (A.VdCode Is Not Null ");
                SQL.AppendLine("And EXISTS ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("	SELECT 1   ");
                SQL.AppendLine("	FROM TblGroupVendorCategory ");
                SQL.AppendLine("	WHERE VdCtCode = K.VdCtCode  ");
                SQL.AppendLine("	AND GrpCode IN   ");
                SQL.AppendLine("	(  ");
                SQL.AppendLine("		SELECT GrpCode FROM tbluser  ");
                SQL.AppendLine("		WHERE UserCode = @UserCode  ");
                SQL.AppendLine("	)  ");
                SQL.AppendLine("))) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 59;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "NPWP",
                        "Vendor",

                        //6-10
                        "Address",
                        "Vendor's"+Environment.NewLine+"Invoice#",
                        "WAPU Code",
                        "PPN"+Environment.NewLine+"WAPU",
                        "NWAPU Code",

                        //11-15
                        "PPN"+Environment.NewLine+"Non WAPU",
                        "Tax Invoice# 1",
                        "Tax 1",
                        "Invoice's"+Environment.NewLine+"Tax 1",
                        "Tax Invoice# 2",

                        //16-20
                        "Tax 2",
                        "Invoice's"+Environment.NewLine+"Tax 2",
                        "Tax Invoice# 3",
                        "Tax 3",
                        "Invoice's"+Environment.NewLine+"Tax 3",

                        //21-25
                        "Invoice"+Environment.NewLine+"Amount",
                        "Received#",
                        "Vendor's DO#",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",

                        //26-30
                        "Foreign Name",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",

                        //31-35
                        "Quantity",
                        "UoM",
                        "Currency",
                        "Unit Price",
                        "Discount"+Environment.NewLine+"%",

                        //36-40
                        "Discount"+Environment.NewLine+"Amount",
                        "Rounding"+Environment.NewLine+"Value",
                        "Item's"+Environment.NewLine+"Tax 1",
                        "Item's"+Environment.NewLine+"Tax 2",
                        "Item's"+Environment.NewLine+"Tax 3",

                        //41-45
                        "Amount",
                        "Payment Date",
                        "Department",
                        "Site",
                        "",

                        //46-50 
                        "File Name",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By",
                        
                        //51-55
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time",
                        "Tax"+Environment.NewLine+"Rate",
                        "Project Code",
                        "Project Name",

                        //56-58
                        "Customer PO#",
                        "Status",
                        "Local Document#"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        150, 80, 80, 130, 200, 
                        //6-10
                        250, 130, 80, 130, 80, 
                        //11-15
                        130, 80, 80, 130, 80, 
                        //16-20
                        80, 130, 80, 80, 130, 
                        //21-25
                        150, 130, 130, 80, 150, 
                        //26-30
                        130, 100, 80, 100, 80,
                        //31-35
                        100, 80, 80, 130, 100, 
                        //36-40
                        100, 100, 100, 100, 100, 
                        //41-45
                        100, 130, 120, 130, 20,  
                        //46-50
                        180, 100, 100, 100, 100,
                        //51-55
                        100, 100, 100, 120, 200,
                        //56-58
                        120, 100, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 45 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11, 14, 17, 20, 21, 27, 29, 31, 34, 35, 36, 37, 38, 39, 40, 41, 53 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 42, 48, 51 });
            Sm.GrdFormatTime(Grd1, new int[] { 49, 52 });
            if (mDocTitle == "KIM")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 8, 10, 26, 29, 30, 31, 32, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52 }, false);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 26, 29, 30, 31, 32, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52 }, false);
            }
            if (mFrmParent.mIsSiteMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 44 }, true);
            if (!mFrmParent.mIsShowForeignName)
                Grd1.Cols[23].Visible = true;
            if (mFrmParent.mIsPIAllowToUploadFile)
                Sm.GrdColInvisible(Grd1, new int[] { 45, 46 }, true);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 54, 55, 56 }, false);
            ShowInventoryUomCode();
            Grd1.Cols[53].Move(22);
            Grd1.Cols[54].Move(47);
            Grd1.Cols[55].Move(48);
            Grd1.Cols[56].Move(49);
            Grd1.Cols[58].Move(4);
            Grd1.Cols[57].Move(5);
            Sm.SetGrdProperty(Grd1, false);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 29, 30 }, true);
                
            if (mNumberOfInventoryUomCode == 3)           
                Sm.GrdColInvisible(Grd1, new int[] { 29, 30, 31, 32 }, true);                
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  47, 48, 49, 50, 51, 52 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "G.ItCode", "J.ItName", "J.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, TxtRecvDocNo.Text, "B.RecvVdDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtVdDONo.Text, "Q.VdDONo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            //1-5
                            "DocDt", "CancelInd", "TIN", "VdName", "Address",
                            //6-10
                            "VdInvNo", "WAPUFROM", "WAPU", "NWAPUFROM", "NWAPU", 
                            //11-15 
                            "TaxInvoiceNo", "TaxName1", "PITaxAmt1", "TaxInvoiceNo2", "TaxName2",
                            //16-20
                            "PITaxAmt2", "TaxInvoiceNo3", "TaxName3", "PITaxAmt3", "Amt",
                            //21-25
                            "RecvVdDocNo", "VdDONo", "ItCode", "ItName", "ForeignName",
                            //26-30
                            "QtyPurchase", "PurchaseUomCode", "Qty2", "InventoryUomCode2", "Qty3",
                            //31-35
                            "InventoryUomCode3", "CurCode", "UPrice", "Discount", "DiscountAmt",
                            //36-40
                            "RoundingValue", "TaxAmt1", "TaxAmt2", "TaxAmt3", "Total", 
                            //41-45
                            "DeptName", "SiteName", "CreateBy", "CreateDt", "LastUpBy",
                            //46-50
                            "LastUpDt", "TaxCode1", "TaxCode2", "TaxCode3", "TaxRateAmt",
                            //51-55
                            "PaymentDt", "FileName", "ProjectCode", "ProjectName", "PONo",
                            //56-57
                            "Status", "LocalDocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            if (mDocTitle == "KIM")
                            {
                                if (Sm.DrStr(dr, c[47]) == "PPN2" || Sm.DrStr(dr, c[47]) == "PPN3")
                                {
                                    Grd1.Cells[Row, 12].Value = string.Empty;
                                    Grd1.Cells[Row, 13].Value = string.Empty;
                                    Grd1.Cells[Row, 14].Value = Sm.FormatNum(0,0);
                                }
                                else
                                {
                                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                                }
                                if (Sm.DrStr(dr, c[48]) == "PPN2" || Sm.DrStr(dr, c[48]) == "PPN3")
                                {
                                    Grd1.Cells[Row, 15].Value = string.Empty;
                                    Grd1.Cells[Row, 16].Value = string.Empty;
                                    Grd1.Cells[Row, 17].Value = Sm.FormatNum(0, 0);
                                }
                                else
                                {
                                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                                }
                                if (Sm.DrStr(dr, c[49]) == "PPN2" || Sm.DrStr(dr, c[49]) == "PPN3")
                                {
                                    Grd1.Cells[Row, 18].Value = string.Empty;
                                    Grd1.Cells[Row, 19].Value = string.Empty;
                                    Grd1.Cells[Row, 20].Value = Sm.FormatNum(0, 0);
                                }
                                else
                                {
                                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19); 
                                }
                            }
                            else
                            {
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12); 
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19); 
                            }
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21); 
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 28);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 32);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 33);

                            Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 34);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 35);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 36);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 37);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 39, 38);

                            Sm.SetGrdValue("N", Grd, dr, c, Row, 40, 39); 
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 41, 40);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 41);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 44, 42);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 47, 43);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 48, 44);

                            Sm.SetGrdValue("T", Grd, dr, c, Row, 49, 44);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 50, 45);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 51, 46);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 52, 46);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 53, 50);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 42, 51);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 46, 52);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 54, 53);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 55, 54);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 56, 55);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 57, 56);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 58, 57);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        private void GetParameter()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length != 0) mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            mDocTitle = Sm.GetParameter("DocTitle");
        }

        private void DownloadFileKu(string TxtFile)
        {
            DownloadFile(mFrmParent.mHostAddrForFTPClient, mFrmParent.mPortForFTPClient, TxtFile, mFrmParent.mUsernameForFTPClient, mFrmParent.mPasswordForFTPClient, mFrmParent.mSharedFolderForFTPClient);
            SFD1.FileName = TxtFile;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (TxtFile.Length > 0)
            {
                if (SFD1.ShowDialog() == DialogResult.OK)
                {
                    //Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

               
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
               

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                reader.Close();
                memStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }


        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 45 && Sm.GetGrdStr(Grd1, e.RowIndex, 46).Length != 0)
            {
                DownloadFileKu(Sm.GetGrdStr(Grd1, e.RowIndex, 46));
            }
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(Sl.SetLueVdCode), mFrmParent.mIsFilterByVendorCategory ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkRecvDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Received#");
        }

        private void TxtRecvDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtVdDONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdDONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor's DO#");
        }

        #endregion       

        #endregion
       
    }
}
