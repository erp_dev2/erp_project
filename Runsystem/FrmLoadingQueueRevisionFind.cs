﻿#region Update
// 11/des/2017 [HAR] bug fixing filter nomor antrian
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmLoadingQueueRevisionFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmLoadingQueueRevision mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmLoadingQueueRevisionFind(FrmLoadingQueueRevision FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LoadingQueueDocNo, C.OptDesc As AreaOld , A.LicenceNoOld, ");
            SQL.AppendLine("A.DrvNameOld, D.OptDesc As Area, A.LicenceNo, A.DrvName, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblLoadingQueueRevision A ");
            SQL.AppendLine("Inner Join TblLoadingQueue B On A.LoadingQueueDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblOption C On A.LoadAreaOld = C.OptCode And C.OptCat = 'LoadingArea' ");
            SQL.AppendLine("Inner Join TblOption D On A.LoadArea = D.OptCode And D.OptCat = 'LoadingArea' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Nomor Dokumen", 
                        "Tanggal",
                        "No Antrian",
                        "Area Bongkar"+Environment.NewLine+"Sebelumnya ",
                        "Nomor Polisi"+Environment.NewLine+"Sebelumnya ",

                        //6-10
                        "Pengemudi / Sopir"+Environment.NewLine+"Sebelumnya ",
                        "Area Bongkar",
                        "Nomor Polisi",
                        "Pengemudi / Sopir",
                        "Catatan",

                        //11-15
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date",

                        //16
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 12, 15 });
            Sm.GrdFormatTime(Grd1, new int[] { 13, 16 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLoadingQueue.Text, "A.LoadingQueueDocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtLicenceNo.Text, new string[] { "A.LicenceNoOld", "A.LicenceNo" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Group By A.DocNo Order By A.DocNo",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "LoadingQueueDocNo", "AreaOld", "LicenceNoOld", "DrvNameOld",  
                            
                            //6-10
                            "Area", "LicenceNo", "DrvName", "Remark", "CreateBy",    
                            
                            //11-13
                            "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);

                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 16, 13);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        private void TxtLoadingQueue_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLoadingQueue_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Nomor Antrian");
        }
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Nomor dokumen");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Tanggal Antrian");
        }

        private void ChkLicenceNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Nomor Polisi");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtLicenceNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion

       
    }
}
