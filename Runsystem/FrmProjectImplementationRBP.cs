﻿#region Update
/*
    18/06/2019 [WED] new apps untuk input biaya renumerasi dan biaya langsung
    30/06/2019 [TKG] bisa diedit
    01/07/2019 [WED] tambah vendor
    17/07/2019 [WED] tambah kolom : SSHealth, SSEmployment, Other, Total
    12/08/2019 [TKG] tambah proses revision
    10/07/2019 [DITA/YK] RBP detail yang belum dropping request bisa di edit
    11/12/2019 [WED/VIR] RBP yang sudah dibuat Dropping Request nya, tidak boleh di cancel
    23/06/2020 [DITA/YK] bug : detail rbp masih menampilkan data dropping request yg sudah cancel
    01/07/2020 [WED/YK] detail RBP yang belom di dropping request, masih boleh di edit, berdasarkan parameter IsRBPWithoutDroppingRequestEditable
    10/07/2020 [WED/YK] saat edit RBP, total remuneration dan total direct cost tidak boleh melebihi nilai PRJI Rev nya
    10/07/2020 [WED/YK] saat edit RBP, juga pakai validasi saat insert
    15/07/2020 [WED/YK] data yg belom di dropping, masih bisa dihapus --> salah ambil nomor kolom Dropping Request nya di grid
    22/07/2020 [WED/YK] SetYrMthInfo() belum mem filter Dropping Request yg sudah cancel
    22/07/2020 [WED/YK] parameter validasi terhadap Total With Tax Remuneration PRJI RBPAmtValidationFormat
    08/11/2021 [VIN/YK] tambah param PRJISourceForRBP
    22/03/2022 [ISD/ALL] tambah param YearEndRange
    05/10/2022 [BRI/VIR] dropping request menjadi partial berdasarkan param IsDroppingRequestWithdraw
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectImplementationRBP : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mPRJISourceForRBP = string.Empty;
        internal bool 
            mIsFilterBySite = false,
            mIsDroppingRequestWithdraw = false;
        private string mRBPAmtValidationFormat = string.Empty,
                       mYearEndRange = string.Empty;
        private bool mIsRBPWithoutDroppingRequestEditable = false;
        iGCell fCell;
        bool fAccept;
        internal FrmProjectImplementationRBPFind FrmFind;

        #endregion

        #region Constructor

        public FrmProjectImplementationRBP(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmProjectImplementationRBP");

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueMth(LueMth);
                Sl.SetLueYr(LueYr, string.Empty, int.Parse(mYearEndRange));
                Sl.SetLueVdCode2(ref LueVdCode, string.Empty);
                LueMth.Visible = LueYr.Visible = LueVdCode.Visible = false;
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsRBPWithoutDroppingRequestEditable = Sm.GetParameterBoo("IsRBPWithoutDroppingRequestEditable");
            mRBPAmtValidationFormat = Sm.GetParameter("RBPAmtValidationFormat");
            mPRJISourceForRBP = Sm.GetParameter("PRJISourceForRBP");
            mYearEndRange = Sm.GetParameter("YearEndRange");
            mIsDroppingRequestWithdraw = Sm.GetParameterBoo("IsDroppingRequestWithdraw");

            if (mRBPAmtValidationFormat.Length == 0) mRBPAmtValidationFormat = "1";
            if (mPRJISourceForRBP.Length == 0) mPRJISourceForRBP = "1";
            if (mYearEndRange.Length == 0) mYearEndRange = "1";
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "Month", 
                    //1-5
                    "Year", "Remuneration", "Direct Cost", "Remark", "Dropping Request",
                    //6-10
                    "DNo", "VendorCode", "Vendor", "Tax", "SS Health", 
                    //11-13
                    "SS Employment", "Others", "Total"
                },
                new int[] 
                { 
                    50, 
                    80, 100, 100, 200, 130,
                    0, 0, 200, 120, 120, 
                    120, 120, 120
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 9, 10, 11, 12, 13 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 5, 6, 7, 13 });
            Grd1.Cols[8].Move(4);
            Grd1.Cols[4].Move(13);
            Grd1.Cols[5].Move(13);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtPRJIDocNo, TxtResourceItCode, 
                        TxtResourceItName, TxtTotalWithTax, MeeRemark, LueVdCode
                    }, true);
                    BtnPRJIDocNo.Enabled = BtnResource.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 8, 9, 10, 11, 12 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, LueVdCode
                    }, false);
                    BtnPRJIDocNo.Enabled = BtnResource.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 1, 2, 3, 4, 8, 9, 10, 11, 12 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueVdCode, MeeCancelReason, ChkCancelInd, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 1, 2, 3, 4, 8, 9, 10, 11, 12 });
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtPRJIDocNo, TxtResourceItCode, TxtResourceItName,
                MeeRemark, LueVdCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtTotalWithTax, TxtTotalDirectCost, TxtTotalRemuneration }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #region Clear Grid

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 3, 9, 10, 11, 12, 13 });
        }

        #endregion

        #endregion

        #region Additional Method

        private void ComputeTotalRemuneration()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd1, row, 0).Length != 0 || Sm.GetGrdStr(Grd1, row, 1).Length != 0)
                {
                    if (Sm.GetGrdDec(Grd1, row, 2) != 0m)
                    {
                        Total += Sm.GetGrdDec(Grd1, row, 13);
                    }
                }
            }
            TxtTotalRemuneration.EditValue = Sm.FormatNum(Total, 0);
        }

        private void ComputeTotalDirectCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd1, row, 0).Length != 0 || Sm.GetGrdStr(Grd1, row, 1).Length != 0)
                {
                    if (Sm.GetGrdDec(Grd1, row, 3) != 0m)
                    {
                        Total += Sm.GetGrdDec(Grd1, row, 13);
                    }
                }
            }
            TxtTotalDirectCost.EditValue = Sm.FormatNum(Total, 0);
        }

        private void ComputeTotalDetail(int Row)
        {
            decimal Total = 0m;

            if (Sm.GetGrdDec(Grd1, Row, 2) != 0m)
            {
                Total = Sm.GetGrdDec(Grd1, Row, 2) + Sm.GetGrdDec(Grd1, Row, 9) +
                    Sm.GetGrdDec(Grd1, Row, 10) + Sm.GetGrdDec(Grd1, Row, 11) + Sm.GetGrdDec(Grd1, Row, 12);
            }

            if (Sm.GetGrdDec(Grd1, Row, 3) != 0m)
            {
                Total = Sm.GetGrdDec(Grd1, Row, 3) + Sm.GetGrdDec(Grd1, Row, 9) +
                    Sm.GetGrdDec(Grd1, Row, 12);
            }

            Grd1.Cells[Row, 13].Value = Total;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProjectImplementationRBPFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.FormatNumTxt(TxtTotalRemuneration, 0);
                Sm.FormatNumTxt(TxtTotalDirectCost, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 2, 3, 9, 10, 11, 12, 13 }, e);
            ComputeTotalDetail(e.RowIndex);
            ComputeTotalRemuneration();
            ComputeTotalDirectCost();
            
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length != 0)
                {
                    if (Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5).Length > 0)
                    {
                        e.DoDefault = false;
                        return;
                    }
                    if (Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6).Length > 0)
                    {
                        if (Sm.IsGrdColSelected(new int[] { 0, 1 }, e.ColIndex))
                        {
                            e.DoDefault = false;
                            return;
                        }
                    }
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 1, 8 }, e.ColIndex))
                {
                    //e.DoDefault = false;
                    if (e.ColIndex == 0)
                    {
                        LueRequestEdit(Grd1, LueMth, ref fCell, ref fAccept, e);
                        //Sl.SetLueMth(LueMth);
                    }
                    if (e.ColIndex == 1)
                    {
                        LueRequestEdit(Grd1, LueYr, ref fCell, ref fAccept, e);
                        //Sl.SetLueYr(LueYr, string.Empty, mYearEndRange);
                    }
                    if (e.ColIndex == 8)
                    {
                        LueRequestEdit(Grd1, LueVdCode, ref fCell, ref fAccept, e);
                        Sl.SetLueVdCode2(ref LueVdCode, string.Empty);
                    }
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3, 9, 10, 11, 12, 13 });
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            var x = Grd1.CurRow.Index;
            var y = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);

            if (BtnSave.Enabled && 
                (
                    (TxtDocNo.Text.Length==0) || // baru insert, atau
                    (TxtDocNo.Text.Length!=0 && Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5).Length==0)) // edit data, tapi belom di dropping request
                )
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeTotalRemuneration();
                ComputeTotalDirectCost();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProjectImplementationRBP", "TblProjectImplementationRBPHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveProjectImplementationRBPHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    cml.Add(SaveProjectImplementationRBPDtl(DocNo, Row));

            cml.Add(SaveProjectImplementationRBPRevision(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false) ||
                Sm.IsTxtEmpty(TxtResourceItCode, "Resource#", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsPRJIAlreadyProcessed() ||
                IsAmtExceedsPRJIAmt();
        }

        private bool IsPRJIAlreadyProcessed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblProjectImplementationRBPHdr ");
            SQL.AppendLine("Where CancelInd = 'N' ");
            SQL.AppendLine("And PRJIDocNo = @Param1 ");
            SQL.AppendLine("And ResourceItCode = @Param2 ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtPRJIDocNo.Text, TxtResourceItCode.Text, string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed in RBP#" + Sm.GetValue(SQL.ToString(), TxtPRJIDocNo.Text, TxtResourceItCode.Text, string.Empty));
                return true;
            }

            return false;
        }

        private bool IsAmtExceedsPRJIAmt()
        {
            if (mRBPAmtValidationFormat == "1")
            {
                if ((Decimal.Parse(TxtTotalDirectCost.Text) + Decimal.Parse(TxtTotalRemuneration.Text)) > Decimal.Parse(TxtTotalWithTax.Text))
                {
                    Sm.StdMsg(mMsgType.Warning, "Remuneration Amount or Direct Cost Amount should be less than Total With Tax Amount.");
                    if (Decimal.Parse(TxtTotalWithTax.Text) != 0m) TxtTotalRemuneration.Focus();
                    else TxtTotalDirectCost.Focus();
                    return true;
                }
            }
            else
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (Sm.GetGrdDec(Grd1, i, 13) > Decimal.Parse(TxtTotalWithTax.Text))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Total amount should be less than Total With Tax Amount.");
                        Sm.FocusGrd(Grd1, i, 13);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 data.");
                Sm.FocusGrd(Grd1, 0, 0);
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 0, false, "Month is empty.")) { return true; }
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Year is empty.")) { return true; }

                if (Sm.GetGrdDec(Grd1, Row, 2) == 0m && Sm.GetGrdDec(Grd1, Row, 3) == 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, "Both amount could not be zero.");
                    Sm.FocusGrd(Grd1, Row, 2);
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 2) != 0m && Sm.GetGrdDec(Grd1, Row, 3) != 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, "Only input Remuneration or Direct Cost amount.");
                    Sm.FocusGrd(Grd1, Row, 2);
                    return true;
                }
            }

            for (int r1 = 0; r1 < Grd1.Rows.Count - 1; r1++)
            {
                for (int r2 = (r1 + 1); r2 < Grd1.Rows.Count; r2++)
                {
                    if ((Sm.GetGrdStr(Grd1, r1, 0) == Sm.GetGrdStr(Grd1, r2, 0))
                        &&
                        (Sm.GetGrdStr(Grd1, r1, 1) == Sm.GetGrdStr(Grd1, r2, 1))
                        )
                    {
                        Sm.StdMsg(mMsgType.Warning, "Duplicate month and year found : " + Sm.GetGrdStr(Grd1, r2, 0));
                        Sm.FocusGrd(Grd1, r2, 0);
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveProjectImplementationRBPHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProjectImplementationRBPHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, PRJIDocNo, ResourceItCode, TotalRemuneration, TotalDirectCost, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'N', @PRJIDocNo, @ResourceItCode, @TotalRemuneration, @TotalDirectCost, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", TxtPRJIDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ResourceItCode", TxtResourceItCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@TotalRemuneration", Sm.GetDecValue(TxtTotalRemuneration.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalDirectCost", Sm.GetDecValue(TxtTotalDirectCost.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveProjectImplementationRBPDtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRBPDtl(DocNo, DNo, Mth, Yr, RemunerationAmt, DirectCostAmt, VdCode, Remark, ");
            SQLDtl.AppendLine("TaxAmt, SSHealthAmt, SSEmploymentAmt, OthersAmt, TotalAmt, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @Mth, @Yr, @RemunerationAmt, @DirectCostAmt, @VdCode, @Remark, ");
            SQLDtl.AppendLine("@TaxAmt, @SSHealthAmt, @SSEmploymentAmt, @OthersAmt, @TotalAmt, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@RemunerationAmt", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@DirectCostAmt", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@SSHealthAmt", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@SSEmploymentAmt", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@OthersAmt", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRBPRevision(string DocNo)
        {
            var SQL = new StringBuilder();
            var RevisionNo = "0";

            if (TxtDocNo.Text.Length > 0)
            {
                RevisionNo = Sm.GetValue("Select RevisionNo From TblProjectImplementationRBPRevisionHdr Where DocNo=@Param Order By RevisionNo Desc Limit 1;", TxtDocNo.Text);
                if (RevisionNo.Length == 0)
                    RevisionNo = "0";
                else
                    RevisionNo = (int.Parse(RevisionNo)+1).ToString();
            }
            SQL.AppendLine("Insert Into TblProjectImplementationRBPRevisionHdr ");
            SQL.AppendLine("(DocNo, DocDt, RevisionNo, CancelInd, PRJIDocNo, ResourceItCode, TotalRemuneration, TotalDirectCost, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, DocDt, @RevisionNo, CancelInd, PRJIDocNo, ResourceItCode, TotalRemuneration, TotalDirectCost, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblProjectImplementationRBPHdr Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblProjectImplementationRBPRevisionDtl ");
            SQL.AppendLine("(DocNo, DNo, RevisionNo, Mth, Yr, RemunerationAmt, DirectCostAmt, VdCode, Remark, ");
            SQL.AppendLine("TaxAmt, SSHealthAmt, SSEmploymentAmt, OthersAmt, TotalAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, DNo, @RevisionNo, Mth, Yr, RemunerationAmt, DirectCostAmt, VdCode, Remark, ");
            SQL.AppendLine("TaxAmt, SSHealthAmt, SSEmploymentAmt, OthersAmt, TotalAmt, CreateBy, CreateDt ");
            SQL.AppendLine("From TblProjectImplementationRBPDtl Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@RevisionNo", RevisionNo);
            
            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
            //ComputeBobotPercentage();
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditProjectImplementationHdr());

            for (int r = 0; r< Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                    cml.Add(SaveProjectImplementationRBPDtl(TxtDocNo.Text, r));

            cml.Add(SaveProjectImplementationRBPRevision(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            SetYrMthInfo();
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsAmtExceedsPRJIAmt() ||
                IsDataAlreadyProcessToDroppingRequest();
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this data.");
                MeeRemark.Focus();
                return true;
            }

            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblProjectImplementationRBPHdr ");
            SQL.AppendLine("Where (CancelInd='Y') And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyProcessToDroppingRequest()
        {
            if (mIsRBPWithoutDroppingRequestEditable) return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblDroppingRequestHdr A ");
            SQL.AppendLine("Inner Join TblDroppingRequestDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.Status In ('O', 'A') ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine("    And B.PRBPDocNo Is Not Null ");
            SQL.AppendLine("    And B.PRBPDocNo=@Param ");
            SQL.AppendLine("Limit 1; ");

            return Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text, "This document already processed to dropping request #" + Sm.GetValue(SQL.ToString(), TxtDocNo.Text)); 
        }

        private MySqlCommand EditProjectImplementationHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProjectImplementationRBPHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, Remark=@Remark, ");
            SQL.AppendLine("    TotalRemuneration = @TotalRemuneration, TotalDirectCost = @TotalDirectCost, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Delete From TblProjectImplementationRBPDtl Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@TotalRemuneration", Decimal.Parse(TxtTotalRemuneration.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalDirectCost", Decimal.Parse(TxtTotalDirectCost.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowProjectImplementationRBPHdr(DocNo);
                ShowProjectImplementationRBPDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProjectImplementationRBPHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.PRJIDocNo, A.ResourceItCode, C.ItName, B.Amt, ");
            SQL.AppendLine("A.TotalRemuneration, A.TotalDirectCost, A.Remark ");
            SQL.AppendLine("From TblProjectImplementationRBPHdr A ");
            SQL.AppendLine("Left Join TblProjectImplementationDtl2 B ");
            SQL.AppendLine("    On A.PRJIDocNo=B.DocNo ");
            SQL.AppendLine("    And A.ResourceItCode=B.ResourceItCode ");
            SQL.AppendLine("Left Join TblItem C On A.ResourceItCode=C.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "PRJIDocNo", "ResourceItCode",  
                    
                    //6-10
                    "ItName", "Amt", "TotalRemuneration", "TotalDirectCost", "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                    TxtPRJIDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtResourceItCode.EditValue = Sm.DrStr(dr, c[5]);
                    TxtResourceItName.EditValue = Sm.DrStr(dr, c[6]);
                    TxtTotalWithTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                    TxtTotalRemuneration.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    TxtTotalDirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                }, true
            );
        }

        private void ShowProjectImplementationRBPDtl(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            if (mIsDroppingRequestWithdraw)
                SQLDtl.AppendLine("Select A.Mth, A.Yr, A.RemunerationAmt, A.DirectCostAmt, A.Remark, Group_Concat(C.DocNo) DocNo, A.DNo, A.VdCode, D.VdName, ");
            else
                SQLDtl.AppendLine("Select A.Mth, A.Yr, A.RemunerationAmt, A.DirectCostAmt, A.Remark, C.DocNo, A.DNo, A.VdCode, D.VdName, ");
            SQLDtl.AppendLine("A.TaxAmt, A.SSHealthAmt, A.SSEmploymentAmt, A.OthersAmt, A.TotalAmt ");
            SQLDtl.AppendLine("From TblProjectImplementationRBPDtl A ");
            SQLDtl.AppendLine("Left Join TblProjectImplementationRBPHdr B On A.DocNo=B.DocNo ");
            SQLDtl.AppendLine("Left Join TblDroppingRequestHdr C On B.PRJIDocNo=C.PRJIDocNo And A.Yr=C.Yr And A.Mth=C.Mth AND C.CancelInd = 'N' ");
            SQLDtl.AppendLine("Left Join TblVendor D On A.VdCode = D.VdCode ");
            SQLDtl.AppendLine("Where A.DocNo = @DocNo ");
            if (mIsDroppingRequestWithdraw) SQLDtl.AppendLine("Group By A.Mth, A.Yr ");
            SQLDtl.AppendLine("Order By A.DNo; ");

            var cm1 = new MySqlCommand();
            Sm.CmParam<String>(ref cm1, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm1, SQLDtl.ToString(),
                new string[] 
                { 
                    "Mth", 
                    "Yr", "RemunerationAmt", "DirectCostAmt", "Remark", "DocNo",
                    "DNo", "VdCode", "VdName", "TaxAmt", "SSHealthAmt",
                    "SSEmploymentAmt", "OthersAmt", "TotalAmt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 13);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3, 9, 10, 11, 12, 13 });
        }

        private void SetYrMthInfo()
        {
            var SQL = new StringBuilder();
            string 
                Mth = string.Empty, 
                Yr = string.Empty, 
                Remark = string.Empty, 
                DocNo = string.Empty;
            decimal RemunerationAmt = 0m, DirectCostAmt = 0m;

            SQL.AppendLine("Select A.Mth, A.Yr, A.RemunerationAmt, A.DirectCostAmt, A.Remark, C.DocNo ");
            SQL.AppendLine("From TblProjectImplementationRBPDtl A ");
            SQL.AppendLine("Left Join TblProjectImplementationRBPHdr B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblDroppingRequestHdr C On B.PRJIDocNo=C.PRJIDocNo And A.Yr=C.Yr And A.Mth=C.Mth And C.CancelInd = 'N' And C.Status In ('O', 'A') ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.DNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "Mth", 
                    "Yr", "RemunerationAmt", "DirectCostAmt", "Remark", "DocNo"
                });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Mth = Sm.DrStr(dr, 0);
                        Yr = Sm.DrStr(dr, 1);
                        RemunerationAmt = Sm.DrDec(dr, 2);
                        DirectCostAmt = Sm.DrDec(dr, 3);
                        Remark = Sm.DrStr(dr, 4);
                        DocNo = Sm.DrStr(dr, 5);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 0), Mth) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), Yr) &&
                                Sm.GetGrdStr(Grd1, r, 5).Length==0 &&
                                DocNo.Length>0
                                )
                            {
                                Sm.StdMsg(mMsgType.Warning, 
                                    "Year : " + Yr + Environment.NewLine +
                                    "Month : " + Mth + Environment.NewLine +
                                    "Dropping Request# : " + DocNo + Environment.NewLine +
                                    "This data already processed to dropping request." + Environment.NewLine +
                                    "All the existing information will be reset to the previous value."
                                    );
                                Grd1.Cells[r, 2].Value = RemunerationAmt;
                                Grd1.Cells[r, 3].Value = DirectCostAmt;
                                Grd1.Cells[r, 4].Value = Remark;
                                Grd1.Cells[r, 5].Value = DocNo;

                                ComputeTotalDetail(r);
                                ComputeTotalRemuneration();
                                ComputeTotalDirectCost();

                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #region Additional Method

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal void ShowResourceDocument(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.ResourceItCode, B.ItName, A.Amt ");
            SQL.AppendLine("From TblProjectImplementationDtl2 A ");
            SQL.AppendLine("Inner Join TblItem B On A.ResourceItCode = B.ItCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-3
                    "ResourceItCode", "ItName", "Amt"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtResourceItCode.EditValue = Sm.DrStr(dr, c[1]);
                    TxtResourceItName.EditValue = Sm.DrStr(dr, c[2]);
                    TxtTotalWithTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                }, true
            );
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtTotalRemuneration_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTotalRemuneration, 0);
        }

        private void TxtTotalDirectCost_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTotalDirectCost, 0);
        }

        private void TxtTotalWithTax_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTotalWithTax, 0);
        }

        private void LueMth_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueMth_Leave(object sender, EventArgs e)
        {
            if (LueMth.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (Sm.GetLue(LueMth).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 0].Value = null;
                else
                    Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueMth);
                //LueMth.Visible = false;
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3, 9, 10, 11, 12, 13 });
            }
        }

        private void LueYr_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueYr_Leave(object sender, EventArgs e)
        {
            if (LueYr.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueYr).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueYr);
                }
                //LueYr.Visible = false;
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3, 9, 10, 11, 12, 13 });
            }
        }


        private void LueMth_Validated(object sender, EventArgs e)
        {
            LueMth.Visible = false;
        }

        private void LueYr_Validated(object sender, EventArgs e)
        {
            LueYr.Visible = false;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(Sl.SetLueVdCode2), string.Empty);
        }

        private void LueVdCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueVdCode_Leave(object sender, EventArgs e)
        {
            if (LueVdCode.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LueVdCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 7].Value =
                    Grd1.Cells[fCell.RowIndex, 8].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LueVdCode);
                    Grd1.Cells[fCell.RowIndex, 8].Value = LueVdCode.GetColumnValue("Col2");
                }
                LueVdCode.Visible = false;
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3, 9, 10, 11, 12, 13 });
            }
        }

        #endregion

        #region Button Clicks

        private void BtnPRJIDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmProjectImplementationRBPDlg(this));
            }
        }

        private void BtnPRJIDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false))
            {
                var f = new FrmProjectImplementation(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmProjectImplementation");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPRJIDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnResource_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false))
            {
                Sm.FormShowDialog(new FrmProjectImplementationRBPDlg2(this, TxtPRJIDocNo.Text));
            }
        }

        #endregion

        #endregion
    }
}