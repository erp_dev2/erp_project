﻿#region Update
/*
    26/06/2019 [WED] tambah informasi & filter : Site dan Customer
    27/06/2019 [WED] dokumen project harus sudah di RBP
    09/08/2019 [WED] RBP PRJI yang muncul sesuai filter tanggal nya
    21/08/2019 [WED] ubah link ke SO Contract Revision
    10/10/2019 [WED/YK] salah di subquery
    03/10/2022 [BRI/VIR] menarik prji di bulan dan tahun yang sama berdasarkan param IsDroppingRequestWithdraw
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDroppingRequestDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDroppingRequest mFrmParent;
        private string 
            mSQL = string.Empty,
            mMth = string.Empty,
            mYr = string.Empty;

        #endregion

        #region Constructor

        public FrmDroppingRequestDlg(FrmDroppingRequest FrmParent, string Mth, string Yr)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mMth = Mth;
            mYr = Yr;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mFrmParent.mIsFilterBySite ? "Y" : "N");
                Sl.SetLueCtCode(ref LueCtCode);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.SOContractDocNo, D.ProjectName, ");
            SQL.AppendLine("D.SiteCode, E.SiteName, D.CtCode, F.CtName ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr A1 On A.SOContractDocNo = A1.Docno ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A1.SOCDocNo = B.DocNo ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.Status = 'A' ");
            SQL.AppendLine("    And A.Achievement < 100.00 ");
            if (!mFrmParent.mIsDroppingRequestWithdraw && mMth.Length > 0 && mYr.Length > 0)
            {
                SQL.AppendLine("    And A.DocNo Not In ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select Distinct PRJIDocNo ");
                SQL.AppendLine("        From TblDroppingRequestHdr ");
                SQL.AppendLine("        Where (CancelInd = 'N' And Status In ('A', 'O')) ");
                SQL.AppendLine("        And Mth = @Mth And Yr = @Yr ");
                SQL.AppendLine("        And PRJIDocNo Is Not Null ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblBOQHdr C On B.BOQDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr D On C.LOPDocNo = D.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (D.SiteCode Is Null Or ( ");
                SQL.AppendLine("    D.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(D.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Inner Join TblSite E On D.SiteCode = E.SiteCode ");
            SQL.AppendLine("Inner Join TblCustomer F On D.CtCode = F.CtCode ");
            SQL.AppendLine("Where A.DocNo In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct X1.PRJIDocNo ");
            SQL.AppendLine("    From TblProjectImplementationRBPHdr X1 ");
            if (mMth.Length > 0 && mYr.Length > 0)
            {
                SQL.AppendLine("    Inner Join TblProjectImplementationRBPDtl X2 On X1.DocNo = X2.DocNo ");
                SQL.AppendLine("        And X2.Mth = @Mth And X2.Yr = @Yr ");
            }
            SQL.AppendLine("    Where X1.CancelInd = 'N' ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "",
                    "Date",
                    "SO Contract#",
                    "Project Name",

                    //6-9
                    "Site Code",
                    "Site",
                    "Customer Code",
                    "Customer"

                }, new int[]
                {
                    //0
                    50,

                    //1-5
                    140, 20, 80, 140, 200,

                    //6-9
                    100, 200, 100, 200
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 8 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 8 }, !ChkHideInfoInGrd.Checked);
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProjectImplementation(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmProjectImplementation(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Show Data
        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@Mth", mMth);
                Sm.CmParam<String>(ref cm, "@Yr", mYr);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtSOContractDocNo.Text, "A.SOContractDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, "D.ProjectName", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "D.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "D.SiteCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo ",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "SOContractDocNo", "ProjectName", "SiteCode", "SiteName",

                            //6-7
                            "CtCode", "CtName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtPRJIDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                this.Hide();
            }
        }
        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtSOContractDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSOContractDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Contract#");
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Name");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mFrmParent.mIsFilterBySite ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        #endregion

        #endregion

    }
}
