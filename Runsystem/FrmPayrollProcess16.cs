﻿#region Update
/*
    29/11/2019 [HAR/IMS] tunjangan kinerja.
    12/12/2019 [HAR/IMS] Over Time
    12/12/2019 [HAR/IMS] TUKIN(perfromance allowance) ambil dari menu baru perfromance allowance (process 5) 
    19/02/2020 [HAR/IMS] performance ambil dari bulan sebelumnya, tax, jahil
    21/02/2020 [HAR/IMS] bug perfromance
    27/02/2020 [HAR/IMS] saat save payrollrpocessAD ambil dari grade juga
    06/12/2020 [TKG/IMS] tambah perhitungan unpaid leave berdasarkan deduction rounding working schedule
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPayrollProcess16 : Form
    {
        #region Field, Property

        private string
            mDeptCode = string.Empty,
            mSystemType = string.Empty,
            mPayrunPeriod = string.Empty,
            mPGCode = string.Empty,
            mSiteCode = string.Empty,

            mNeglectLeaveCode = "002",
            mOTRequestType = "1",
            mCoordinatorLeaveCode = "007",
            mDisasterLeaveCode = "014",
            mGoHomeEarlyLeaveCode = "017",
            mMaternityLeaveCode = "015",
            mMiscarriageLeaveCode = "016",
            mOperatorLeaveCode = "001",
            mEmpSystemTypeHarian = "1",
            mEmpSystemTypeBorongan = "2",
            mEmpSystemTypeAllIn = "3",
            mEmploymentStatusTetap = "1",
            mEmploymentStatusLepas = "2",
            mEmploymentStatusSpesial = "3",
            mPayrunPeriodBulanan = "1",
            mPayrunPeriod2Mingguan = "2",
            mADCodeTransport = "03",
            mADCodeMeal = "05",
            mSSPCodeForHealth = "BPJS-KES",
            mSSPCodeForEmployment = "BPJS-TNK",
            mSSForRetiring = "Pens",
            mSSOldAgeInsurance = "JHT",
            mSalaryInd = "1",
            mWSCodesForNationalHoliday = string.Empty,
            mWSCodesForHoliday = string.Empty,
            mWSCodesHolidayWithExtraFooding = string.Empty,
            mSSCodePension = string.Empty,
            mSSCodePension2 = string.Empty,
            mADCodeOTHoliday = string.Empty,
            mADCodeOTNonHoliday = string.Empty,
            mProratedADBasedOnLeave = string.Empty,
            mLeaveForProratedAD = string.Empty,
            mProratedAD = string.Empty,
            mSSYr = string.Empty,
            mSSMth = string.Empty,
            mPayrunCodeFormatType = string.Empty;
            
        List<string> mlProratedADBasedOnLeave = new List<string>();
        List<string> mlLeaveForProratedAD = new List<string>();

        private decimal
            mDirectLaborDailyWorkHour = 0m,
            mStdHolidayIndex = 2m,
            mWorkingHr = 7m,
            mNoWorkDay2Mingguan = 12,
            mNoWorkDayBulanan = 25,
            mFunctionalExpenses = 5m,
            mFunctionalExpensesMaxAmt = 500000m,
            mOTFormulaHr1 = 1m,
            mOTFormulaHr2 = 24m,
            mOTFormulaIndex1 = 1.5m,
            mOTFormulaIndex2 = 2m,
            mHOWorkingDayPerMth = 25m,
            mSiteWorkingDayPerMth = 21m,
            mHoliday1stHrForHO = 8m,
            mHoliday1stHrForSite = 7m,
            mHoliday2ndHrForHO = 1m,
            mHoliday2ndHrForSite = 1m,
            mHoliday1stHrIndex = 2m,
            mHoliday2ndHrIndex = 3m,
            mHoliday3rdHrIndex = 4m,
            mNonNPWPTaxPercentage = 120m,
            mADCodeEmploymentPeriodYr = 5m,
            mSSEmploymentErPerc = 0m,
            mSSOldAgeEePerc = 0m,
            mSSPensionEePerc = 0m,
            mFieldAssignmentMinWorkDuration = 0m,
            mXMinToleranceLateMealTransportDeduction = 0m,
            mXPercentageLateMealTransportDeduction = 0m,
            mADOTHour = 6m,
            mADOTMinHour = 1m,
            mMinLeaveDaysForProratedAD = 12m,
            mNoWorkDayPerMth = 1m,
            mBruttoPerformanceAllowanceMultiplier = 0m,
            mOTSetNonHolidayAmount = 0m,
            mOTSetHolidayAmount = 0m
            ;

        private bool
            mIsUseLateValidation = false,
            mIsUseOTReplaceLeave = false,
            mIsOTRoutineRounding = false,
            mIsOTBasedOnOTRequest = false,
            mIsWorkingHrBasedOnSchedule = false,
            mIsPayrollProcessUseSiteWorkingDayPerMth = false,
            mHOInd = true,
            mIsOTHolidayDeduct1Hr = false,
            mIsUseLateMealTransportDeduction = false,
            mIsPPProcessAllOutstandingSS = false,
            mIsPayrunSSYrMthEnabled = false,
            mIsPayrunSiteEnabled = false;

        private List<AttendanceLog> mlAttendanceLog = null;
        private List<OT> mlOT = null;
        private List<OTAdjustment> mlOTAdjustment = null;
        private List<EmployeeAllowance> mlEmployeeAllowanceMeal = null;
        private List<EmployeeAllowance> mlEmployeeAllowanceTransport = null;
        private List<AdvancePaymentProcess> mlAdvancePaymentProcess = null;
        private List<OTIncentive> mlOTIncentive = null;
        private List<WorkScheduleADMealTransport> mlWorkScheduleADMealTransport = null;
        
        #endregion

        #region Constructor

        public FrmPayrollProcess16(string MenuCode)
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Setting

        private void GetParameter()
        {
            mIsPayrunSiteEnabled = Sm.GetParameterBoo("IsPayrunSiteEnabled");
            mNeglectLeaveCode = Sm.GetParameter("NeglectLeaveCode");
            mOTRequestType = Sm.GetParameter("OTRequestType");
            mWSCodesHolidayWithExtraFooding = Sm.GetParameter("WSCodesHolidayWithExtraFooding");
            mWSCodesForNationalHoliday = Sm.GetParameter("WSCodesForNationalHoliday");
            mWSCodesForHoliday = Sm.GetParameter("WSCodesForHoliday");
            mDirectLaborDailyWorkHour = Sm.GetParameterDec("DirectLaborDailyWorkHour");
            mWorkingHr = Sm.GetParameterDec("WorkingHr");
            mNoWorkDay2Mingguan = Sm.GetParameterDec("NoWorkDay2Mingguan");
            mNoWorkDayBulanan = Sm.GetParameterDec("NoWorkDayBulanan");
            mFunctionalExpenses = Sm.GetParameterDec("FunctionalExpenses");
            mFunctionalExpensesMaxAmt = Sm.GetParameterDec("FunctionalExpensesMaxAmt");
            mHOWorkingDayPerMth = Sm.GetParameterDec("HOWorkingDayPerMth");
            mSiteWorkingDayPerMth = Sm.GetParameterDec("SiteWorkingDayPerMth");
            mSalaryInd = Sm.GetParameter("SalaryInd");
            mCoordinatorLeaveCode = Sm.GetParameter("CoordinatorLeaveCode");
            mDisasterLeaveCode = Sm.GetParameter("DisasterLeaveCode");
            mGoHomeEarlyLeaveCode = Sm.GetParameter("GoHomeEarlyLeaveCode");
            mMaternityLeaveCode = Sm.GetParameter("MaternityLeaveCode");
            mMiscarriageLeaveCode = Sm.GetParameter("MiscarriageLeaveCode");
            mOperatorLeaveCode = Sm.GetParameter("OperatorLeaveCode");
            mEmpSystemTypeHarian = Sm.GetParameter("EmpSystemTypeHarian");
            mEmpSystemTypeBorongan = Sm.GetParameter("EmpSystemTypeBorongan");
            mEmpSystemTypeAllIn = Sm.GetParameter("EmpSystemTypeAllIn");
            mEmploymentStatusTetap = Sm.GetParameter("EmploymentStatusTetap");
            mEmploymentStatusLepas = Sm.GetParameter("EmploymentStatusLepas");
            mEmploymentStatusSpesial = Sm.GetParameter("EmploymentStatusSpesial");
            mPayrunPeriodBulanan = Sm.GetParameter("PayrunPeriodBulanan");
            mPayrunPeriod2Mingguan = Sm.GetParameter("PayrunPeriod2Mingguan");
            mADCodeTransport = Sm.GetParameter("ADCodeTransport");
            mADCodeMeal = Sm.GetParameter("ADCodeMeal");
            mSSPCodeForHealth = Sm.GetParameter("SSPCodeForHealth");
            mSSPCodeForEmployment = Sm.GetParameter("SSPCodeForEmployment");
            mSSForRetiring = Sm.GetParameter("SSForRetiring");
            mSSOldAgeInsurance = Sm.GetParameter("SSOldAgeInsurance");
            mIsWorkingHrBasedOnSchedule = Sm.GetParameterBoo("IsWorkingHrBasedOnSchedule");
            mIsUseLateValidation = Sm.GetParameterBoo("IsUseLateValidation");
            mIsUseOTReplaceLeave = Sm.GetParameterBoo("IsUseOTReplaceLeave");
            mIsOTRoutineRounding = Sm.GetParameterBoo("IsOTRoutineRounding");
            mIsOTBasedOnOTRequest = Sm.GetParameterBoo("IsOTBasedOnOTRequest");
            mIsPayrollProcessUseSiteWorkingDayPerMth = Sm.GetParameterBoo("IsPayrollProcessUseSiteWorkingDayPerMth");
            mHoliday1stHrForHO = Sm.GetParameterDec("Holiday1stHrForHO");
            mHoliday1stHrForSite = Sm.GetParameterDec("Holiday1stHrForSite");
            mHoliday2ndHrForHO = Sm.GetParameterDec("Holiday2ndHrForHO");
            mHoliday2ndHrForSite = Sm.GetParameterDec("Holiday2ndHrForSite");
            mHoliday1stHrIndex = Sm.GetParameterDec("Holiday1stHrIndex");
            mHoliday2ndHrIndex = Sm.GetParameterDec("Holiday2ndHrIndex");
            mHoliday3rdHrIndex = Sm.GetParameterDec("Holiday3rdHrIndex");
            mNonNPWPTaxPercentage = Sm.GetParameterDec("NonNPWPTaxPercentage");
            mADCodeEmploymentPeriodYr = Sm.GetParameterDec("ADCodeEmploymentPeriodYr");
            mIsOTHolidayDeduct1Hr = Sm.GetParameterBoo("IsOTHolidayDeduct1Hr");
            mFieldAssignmentMinWorkDuration = Sm.GetParameterDec("FieldAssignmentMinWorkDuration");
            mSSCodePension = Sm.GetParameter("SSCodePension");
            mSSCodePension2 = Sm.GetParameter("SSCodePension2");
            mXMinToleranceLateMealTransportDeduction = Sm.GetParameterDec("XMinToleranceLateMealTransportDeduction");
            mXPercentageLateMealTransportDeduction = Sm.GetParameterDec("XPercentageLateMealTransportDeduction");
            mIsUseLateMealTransportDeduction = Sm.GetParameterBoo("IsUseLateMealTransportDeduction");
            mIsPPProcessAllOutstandingSS = Sm.GetParameterBoo("IsPPProcessAllOutstandingSS");
            mADOTHour = Sm.GetParameterDec("ADOTHour");
            mADOTMinHour = Sm.GetParameterDec("ADOTMinHour");
            mMinLeaveDaysForProratedAD= Sm.GetParameterDec("MinLeaveDaysForProratedAD");
            mProratedADBasedOnLeave = Sm.GetParameter("ProratedADBasedOnLeave");
            mLeaveForProratedAD = Sm.GetParameter("LeaveForProratedAD");
            mIsPayrunSSYrMthEnabled = Sm.GetParameterBoo("IsPayrunSSYrMthEnabled");
            mPayrunCodeFormatType = Sm.GetParameter("PayrunCodeFormatType");
            mNoWorkDayPerMth = Sm.GetParameterDec("NoWorkDayPerMth");
            mBruttoPerformanceAllowanceMultiplier = Sm.GetParameterDec("BruttoPerformanceAllowanceMultiplier");
            mOTSetNonHolidayAmount = Sm.GetParameterDec("OTSetNonHolidayAmount");
            mOTSetHolidayAmount = Sm.GetParameterDec("OTSetHolidayAmount");

            if (mProratedADBasedOnLeave.Length > 0)
            {
                foreach(var x in mProratedADBasedOnLeave.Split(','))
                    mlProratedADBasedOnLeave.Add(x);

                foreach (var x in mProratedADBasedOnLeave.Split(','))
                {
                    if (mProratedAD.Length > 0) mProratedAD += ",";
                    mProratedAD += string.Concat("'", x.Trim(), "'");
                }
            }
            if (mLeaveForProratedAD.Length > 0)
            {
                foreach (var x in mLeaveForProratedAD.Split(','))
                    mlLeaveForProratedAD.Add(x);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        "Department",

                        //6-10
                        "Join Date",
                        "Resign Date",
                        "Employment" + Environment.NewLine + "Status",
                        "Type",
                        "Grade Level",
                        
                        //11-13
                        "Period",
                        "Group",
                        "Site"
                    },
                     new int[] 
                    {
                        //0
                        20, 

                        //1-5
                        100, 250, 80, 180, 200, 
                        
                        //6-10
                        100, 100, 130, 130, 150,

                        //11-13
                        130, 150, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            mDeptCode = string.Empty;
            mSystemType = string.Empty; 
            mPayrunPeriod = string.Empty;
            mPGCode = string.Empty;
            mSiteCode = string.Empty;
            mHOInd = true;

            if (mlEmployeeAllowanceMeal.Count > 0)
                mlEmployeeAllowanceMeal.Clear();

            if (mlEmployeeAllowanceTransport.Count > 0)
                mlEmployeeAllowanceTransport.Clear();

            if (mlAdvancePaymentProcess.Count > 0)
                mlAdvancePaymentProcess.Clear();

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtDeptCode, TxtSystemType, TxtPayrunPeriod, TxtPGCode, TxtSiteCode,
                DteStartDt, DteEndDt 
            });
            ChkDelData.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 0 });
        }

        private void SetLuePayrunCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct PayrunCode As Col1, Concat(PayrunCode, ' : ', PayrunName) As Col2 ");
            SQL.AppendLine("From TblPayrun ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And IfNull(Status, 'O')='O' ");
            SQL.AppendLine("Order By Concat(PayrunCode, ' : ', PayrunName);");

            Sm.SetLue2(ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Show Data

        private void ShowPayrunInfo()
        {
            if (Sm.IsLueEmpty(LuePayrunCode, "Payrun")) return;

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                ShowPayrunInfo1();
                ShowPayrunInfo2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPayrunInfo1()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DeptCode, B.DeptName, ");
            SQL.AppendLine("A.SystemType, C.OptDesc As SystemTypeDesc, ");
            SQL.AppendLine("A.PayrunPeriod, D.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("A.PGCode, E.PGName, A.SiteCode, F.SiteName, F.HOInd, ");
            SQL.AppendLine("A.StartDt, A.EndDt, A.SSYr, A.SSMth ");
            SQL.AppendLine("From TblPayrun A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Inner Join TblOption C On A.SystemType=C.OptCode And C.OptCat='EmpSystemType' ");
            SQL.AppendLine("Inner Join TblOption D On A.PayrunPeriod=D.OptCode And D.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr E On A.PGCode=E.PGCode ");
            SQL.AppendLine("Left Join TblSite F On A.SiteCode=F.SiteCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='O' ");
            SQL.AppendLine("And A.PayrunCode=@PayrunCode;");

            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "DeptCode", 
                        
                        //1-5
                        "DeptName", 
                        "SystemType", 
                        "SystemTypeDesc", 
                        "PayrunPeriod", 
                        "PayrunPeriodDesc", 
                        
                        //6-10
                        "PGCode", 
                        "PGName",
                        "SiteCode",
                        "SiteName",
                        "HOInd",
                        
                        //11-14
                        "StartDt", 
                        "EndDt",
                        "SSYr",
                        "SSMth"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        mDeptCode = Sm.DrStr(dr, c[0]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[1]);
                        mSystemType = Sm.DrStr(dr, c[2]);
                        TxtSystemType.EditValue = Sm.DrStr(dr, c[3]);
                        mPayrunPeriod = Sm.DrStr(dr, c[4]);
                        TxtPayrunPeriod.EditValue = Sm.DrStr(dr, c[5]);
                        mPGCode = Sm.DrStr(dr, c[6]);
                        TxtPGCode.EditValue = Sm.DrStr(dr, c[7]);
                        mSiteCode = Sm.DrStr(dr, c[8]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[9]);
                        mHOInd = Sm.DrStr(dr, c[10]) == "Y";
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[11]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[12]));
                        mSSYr = Sm.DrStr(dr, c[13]);
                        mSSMth = Sm.DrStr(dr, c[14]);
                    }, true
                );
        }

        private void ShowPayrunInfo2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, ");
            SQL.AppendLine("E.OptDesc As EmploymentStatusDesc, ");
            SQL.AppendLine("F.OptDesc As SystemTypeDesc, ");
            SQL.AppendLine("G.GrdLvlName, ");
            SQL.AppendLine("H.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("I.PGName, J.SiteName ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select Distinct T1.EmpCode ");
            SQL.AppendLine("    From TblEmployeePPS T1 ");
            SQL.AppendLine("    Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");
            if (ChkEmploymentStatus.Checked)
                SQL.AppendLine("    And T2.EmploymentStatus=@EmploymentStatus ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select X.EmpCode, Max(X.StartDt) As StartDt ");
            SQL.AppendLine("        From TblEmployeePPS X ");
            SQL.AppendLine("        Where Exists ( ");
            SQL.AppendLine("            Select EmpCode ");
            SQL.AppendLine("            From TblEmployee ");
            SQL.AppendLine("            Where EmpCode=X.EmpCode ");
            SQL.AppendLine("            And JoinDt<=@EndDt ");
            SQL.AppendLine("            And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>@StartDt)) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And X.DeptCode=@DeptCode ");
            SQL.AppendLine("        And X.SystemType=@SystemType ");
            SQL.AppendLine("        And X.PayrunPeriod=@PayrunPeriod ");
            SQL.AppendLine("        And IfNull(X.PGCode, '')=IfNull(@PGCode, '') ");
            if (mIsPayrunSiteEnabled)
                SQL.AppendLine("        And IfNull(X.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("        And X.StartDt<=@EndDt ");
            SQL.AppendLine("        And (X.EndDt Is Null Or X.EndDt>=@StartDt)");
            SQL.AppendLine("        Group By X.EmpCode ");
            SQL.AppendLine("    ) T3 On T1.EmpCode=T3.EmpCode And T1.StartDt=T3.StartDt ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblOption E On B.EmploymentStatus=E.OptCode And E.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Left Join TblOption F On B.SystemType=F.OptCode And F.OptCat='EmpSystemType' ");
            SQL.AppendLine("Left Join TblGradeLevelHdr G On B.GrdLvlCode=G.GrdLvlCode ");
            SQL.AppendLine("Left Join TblOption H On B.PayrunPeriod=H.OptCode And H.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr I On B.PGCode=I.PGCode ");
            SQL.AppendLine("Left Join TblSite J On B.SiteCode=J.SiteCode ");
            SQL.AppendLine("Order By B.EmpName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@SystemType", mSystemType);
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", mPayrunPeriod);
            Sm.CmParam<String>(ref cm, "@PGCode", mPGCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
            if (ChkEmploymentStatus.Checked) Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmploymentStatus));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "DeptName", "JoinDt",  
                    
                    //6-10
                    "ResignDt", "EmploymentStatusDesc", "SystemTypeDesc", "GrdLvlName", "PayrunPeriodDesc", 
                    
                    //11-12
                    "PGName", "SiteName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = true;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Check Data

        private bool IsProcessedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LuePayrunCode, "Payrun") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsPayrunNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "List of employees is empty.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsExisted = false;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 0))
                {
                    IsExisted = true;
                    break;
                }
            }
            if (!IsExisted)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsPayrunNotValid()
        {
            return
                Sm.IsDataExist(
                    "Select PayrunCode From TblPayrun Where PayrunCode=@Param And IfNull(Status, 'O')='C';",
                    Sm.GetLue(LuePayrunCode),
                    "Payrun Code : " + Sm.GetLue(LuePayrunCode) + Environment.NewLine +
                    "Payrun Name : " + LuePayrunCode.GetColumnValue("Col2") + Environment.NewLine + Environment.NewLine +
                    "This payrun already closed." + Environment.NewLine +
                    "You need to cancel it's voucher request."
                );
        }

        #endregion

        #region Save Data

        private MySqlCommand DeletePayrollProcess()
        {
            string Filter = string.Empty;
            string EmpCode = string.Empty;
            var cm = new MySqlCommand();

            if (!ChkDelData.Checked)
            {
                if (Grd1.Rows.Count >= 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                        if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                            No += 1;
                        }
                    }
                }
                Filter = " And (" + Filter + ") ";
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblAdvancePaymentProcess Where PayrunCode=@PayrunCode " + Filter + ";");
            SQL.AppendLine("Delete From TblPayrollProcess1 Where PayrunCode=@PayrunCode " + Filter + ";") ;
            SQL.AppendLine("Delete From TblPayrollProcess2 Where PayrunCode=@PayrunCode " + Filter + ";");
            SQL.AppendLine("Delete From TblPayrollProcessAD Where PayrunCode=@PayrunCode " + Filter + ";");
            SQL.AppendLine("Delete From TblPayrollProcessADOT Where PayrunCode=@PayrunCode " + Filter + ";");

            if (ChkDelData.Checked)
            {
                SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

                SQL.AppendLine("Update TblSalaryAdjustmentHdr T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

                SQL.AppendLine("Update TblSalaryAdjustment2Dtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

                SQL.AppendLine("Update TblEmpInsPntDtl Set PayrunCode=Null ");
                SQL.AppendLine("Where PayrunCode Is Not Null And PayrunCode=@PayrunCode ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select A.DocNo ");
                SQL.AppendLine("    From TblEmpInsPntHdr A ");
                SQL.AppendLine("    Inner Join TblInsPnt B On A.InsPntCode=B.InsPntCode ");
                SQL.AppendLine("    Inner Join TblInsPntCategory C On B.InspntCtCode=C.InspntCtCode ");
                SQL.AppendLine("    Where A.CancelInd='N' ");
                SQL.AppendLine("    And A.DocDt Between @StartDt And @EndDt ");
                SQL.AppendLine(");");

                SQL.AppendLine("Update TblEmpSCIDtl A ");
                SQL.AppendLine("Inner Join TblEmpSCIHdr B ");
                SQL.AppendLine("    On A.DocNo=B.DocNo ");
                SQL.AppendLine("    And B.CancelInd='N' ");
                SQL.AppendLine("    And B.DocDt Between @StartDt And @EndDt ");
                SQL.AppendLine("Set A.PayrunCode=Null ");
                SQL.AppendLine("Where A.PayrunCode Is Not Null ");
                SQL.AppendLine("And A.PayrunCode=@PayrunCode; ");
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            return cm;
        }

        private MySqlCommand SavePayrollProcessADOT(PayrollProcessADOT r)
        {
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblPayrollProcessADOT ");
            SQL.AppendLine("(PayrunCode, EmpCode, ADCode, Dt, Amt, Duration, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@PayrunCode, @EmpCode, @ADCode, @Dt, @Amt, @Duration, @UserCode, CurrentDateTime()); ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
            Sm.CmParam<String>(ref cm, "@ADCode", r.ADCode);
            Sm.CmParam<String>(ref cm, "@Dt", r.Dt);
            Sm.CmParam<Decimal>(ref cm, "@Amt", r.Amt);
            Sm.CmParam<Decimal>(ref cm, "@Duration", r.Duration);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePayrollProcess1(ref Result2 r)
        { 
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblPayrollProcess1 ");
            SQL.AppendLine("(PayrunCode, EmpCode, JoinDt, ResignDt, NPWP, ");
            SQL.AppendLine("PTKP, Salary, WorkingDay, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
            SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, ");
            SQL.AppendLine("OT1Amt, OT2Amt, OTHolidayAmt, NonTaxableFixAllowance, TaxableFixAllowance, FixAllowance, EmploymentPeriodAllowance, ");
            SQL.AppendLine("IncEmployee, IncMinWages, IncProduction, IncPerformance, PresenceReward, ");
            SQL.AppendLine("HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, Functional, Housing, MobileCredit, Zakat, ServiceChargeIncentive, SSEmployerHealth, SSEmployerEmployment, SSEmployerPension, SSEmployerPension2, NonTaxableFixDeduction, TaxableFixDeduction, FixDeduction, ");
            SQL.AppendLine("DedEmployee, DedProduction, DedProdLeave, EmpAdvancePayment, SSEmployeeHealth, SSEmployeeEmployment, SSEmployeePension, SSEmployeePension2, ");
            SQL.AppendLine("SalaryPension, SSErLifeInsurance, SSEeLifeInsurance, SSErWorkingAccident, SSEeWorkingAccident, SSErRetirement, SSEeRetirement, SSErPension, SSEePension, ");
            SQL.AppendLine("CreditCode1, CreditCode2, CreditCode3, CreditCode4, CreditCode5, CreditCode6, CreditCode7, CreditCode8, CreditCode9, CreditCode10, ");
            SQL.AppendLine("CreditAdvancePayment1, CreditAdvancePayment2, CreditAdvancePayment3, CreditAdvancePayment4, CreditAdvancePayment5, CreditAdvancePayment6, CreditAdvancePayment7, CreditAdvancePayment8, CreditAdvancePayment9, CreditAdvancePayment10, ");
            SQL.AppendLine("PerformanceStatus, GradePerformance, PerformanceValue, ");
            SQL.AppendLine("SalaryAdjustment, ADOT, Tax, TaxAllowance, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@PayrunCode, @EmpCode, @JoinDt, @ResignDt, @NPWP, ");
            SQL.AppendLine("@PTKP, @Salary, @WorkingDay, @PLDay, @PLHr, @PLAmt, @ProcessPLAmt, @UPLDay, @UPLHr, @UPLAmt, @ProcessUPLAmt, ");
            SQL.AppendLine("@OT1Hr, @OT2Hr, @OTHolidayHr, ");
            SQL.AppendLine("@OT1Amt, @OT2Amt, @OTHolidayAmt, @NonTaxableFixAllowance, @TaxableFixAllowance, @FixAllowance, @EmploymentPeriodAllowance, ");
            SQL.AppendLine("@IncEmployee, @IncMinWages, @IncProduction, @IncPerformance, @PresenceReward, ");
            SQL.AppendLine("@HolidayEarning, @ExtraFooding, @FieldAssignment, @Transport, @Meal, @Functional, @Housing, @MobileCredit, @Zakat, @ServiceChargeIncentive, @SSEmployerHealth, @SSEmployerEmployment, @SSEmployerPension, @SSEmployerPension2, @NonTaxableFixDeduction, @TaxableFixDeduction, @FixDeduction, ");
            SQL.AppendLine("@DedEmployee, @DedProduction, @DedProdLeave, @EmpAdvancePayment, @SSEmployeeHealth, @SSEmployeeEmployment, @SSEmployeePension, @SSEmployeePension2, ");
            SQL.AppendLine("@SalaryPension, @SSErLifeInsurance, @SSEeLifeInsurance, @SSErWorkingAccident, @SSEeWorkingAccident, @SSErRetirement, @SSEeRetirement, @SSErPension, @SSEePension, ");
            SQL.AppendLine("@CreditCode1, @CreditCode2, @CreditCode3, @CreditCode4, @CreditCode5, @CreditCode6, @CreditCode7, @CreditCode8, @CreditCode9, @CreditCode10, ");
            SQL.AppendLine("@CreditAdvancePayment1, @CreditAdvancePayment2, @CreditAdvancePayment3, @CreditAdvancePayment4, @CreditAdvancePayment5, @CreditAdvancePayment6, @CreditAdvancePayment7, @CreditAdvancePayment8, @CreditAdvancePayment9, @CreditAdvancePayment10, ");
            SQL.AppendLine("@PerformanceStatus, @GradePerformance, @PerformanceValue, ");
            SQL.AppendLine("@SalaryAdjustment, @ADOT, @Tax, @TaxAllowance, @Amt, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblPayrollProcessAD ");
            SQL.AppendLine("(PayrunCode, EmpCode, ADCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @PayrunCode, @EmpCode, A.ADCode, @RecomputedValueForFixedAD*Sum(Amt) As Amt, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.AmtType='1' ");
            if (mProratedADBasedOnLeave.Length > 0)
                SQL.AppendLine("And B.ADCode Not In (" + mProratedAD + ") ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And A.EmpCode=@EmpCode ");
            SQL.AppendLine("Group BY A.ADCode ");

            if (mProratedADBasedOnLeave.Length > 0)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select @PayrunCode, @EmpCode, A.ADCode, @RecomputedValueForProratedAD*Sum(Amt) As Amt, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
                SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.AmtType='1' ");
                SQL.AppendLine("And B.ADCode In (" + mProratedAD + ") ");
                SQL.AppendLine("Where ( ");
                SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
                SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
                SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
                SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("And A.EmpCode=@EmpCode ");
                SQL.AppendLine("Group BY A.ADCode ");
            }

            //SQL.AppendLine("Union All ");
            //SQL.AppendLine("Select @PayrunCode, @EmpCode, ParValue, @Meal As Amt, @CreateBy, CurrentDateTime() ");
            //SQL.AppendLine("From TblParameter Where ParCode Is Not Null And ParCode='ADCodeMeal' ");
            //SQL.AppendLine("Union All ");
            //SQL.AppendLine("Select @PayrunCode, @EmpCode, ParValue, @Transport As Amt, @CreateBy, CurrentDateTime() ");
            //SQL.AppendLine("From TblParameter Where ParCode Is Not Null And ParCode='ADCodeTransport' ");
            //SQL.AppendLine("Union All ");
            //SQL.AppendLine("Select PayrunCode, EmpCode, ADCode, Sum(Amt) Amt, @CreateBy, CurrentDateTime() ");
            //SQL.AppendLine("From TblPayrollProcessADOT Where PayrunCode=@PayrunCode And EmpCode=@EmpCode ");
            //SQL.AppendLine("Group By PayrunCode, EmpCode, ADCode ");
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("Select @PayrunCode, @EmpCode, B.ADCode, B.Amt As Amt, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From tblGradeLevelHdr A ");
            SQL.AppendLine("Inner Join TblGradeLevelDtl B On A.GrdLvlcode = B.GrdLvlCode ");
            SQL.AppendLine("Inner Join TblEmployee C On A.GrdLvlCode = C.grdLvlCode ");
            SQL.AppendLine("Where C.EmpCode = @EmpCode ");
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
            Sm.CmParam<String>(ref cm, "@JoinDt", r.JoinDt);
            Sm.CmParam<String>(ref cm, "@ResignDt", r.ResignDt);
            Sm.CmParam<String>(ref cm, "@NPWP", r.NPWP);
            Sm.CmParam<String>(ref cm, "@PTKP", r.PTKP);
	        Sm.CmParam<Decimal>(ref cm, "@Salary", r.Salary);
	        Sm.CmParam<Decimal>(ref cm, "@WorkingDay", r.WorkingDay);
	        Sm.CmParam<Decimal>(ref cm, "@PLDay", r.PLDay);
	        Sm.CmParam<Decimal>(ref cm, "@PLHr", r.PLHr);
	        Sm.CmParam<Decimal>(ref cm, "@PLAmt", r.PLAmt);
            Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt", r.ProcessPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@UPLDay", r.UPLDay);
	        Sm.CmParam<Decimal>(ref cm, "@UPLHr", r.UPLHr);
	        Sm.CmParam<Decimal>(ref cm, "@UPLAmt", r.UPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt", r.ProcessUPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@OT1Hr", r.OT1Hr);
	        Sm.CmParam<Decimal>(ref cm, "@OT2Hr", r.OT2Hr);
	        Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr", r.OTHolidayHr);
	        Sm.CmParam<Decimal>(ref cm, "@OT1Amt", r.OT1Amt);
	        Sm.CmParam<Decimal>(ref cm, "@OT2Amt", r.OT2Amt);
	        Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt", r.OTHolidayAmt);
            Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixAllowance", r.NonTaxableFixAllowance);
	        Sm.CmParam<Decimal>(ref cm, "@TaxableFixAllowance", r.TaxableFixAllowance);
            Sm.CmParam<Decimal>(ref cm, "@FixAllowance", r.FixAllowance);
	        Sm.CmParam<Decimal>(ref cm, "@EmploymentPeriodAllowance", r.EmploymentPeriodAllowance);
	        Sm.CmParam<Decimal>(ref cm, "@IncEmployee", r.IncEmployee);
	        Sm.CmParam<Decimal>(ref cm, "@IncMinWages", r.IncMinWages);
	        Sm.CmParam<Decimal>(ref cm, "@IncProduction", r.IncProduction);
	        Sm.CmParam<Decimal>(ref cm, "@IncPerformance", r.IncPerformance);
	        Sm.CmParam<Decimal>(ref cm, "@PresenceReward", r.PresenceReward);
	        Sm.CmParam<Decimal>(ref cm, "@HolidayEarning", r.HolidayEarning);
	        Sm.CmParam<Decimal>(ref cm, "@ExtraFooding", r.ExtraFooding);
            Sm.CmParam<Decimal>(ref cm, "@FieldAssignment", r.FieldAssignment);
            Sm.CmParam<Decimal>(ref cm, "@Transport", r.Transport);
            Sm.CmParam<Decimal>(ref cm, "@Meal", r.Meal);
            Sm.CmParam<Decimal>(ref cm, "@Functional", r.Functional);
            Sm.CmParam<Decimal>(ref cm, "@Housing", r.Housing);
            Sm.CmParam<Decimal>(ref cm, "@MobileCredit", r.MobileCredit);
            Sm.CmParam<Decimal>(ref cm, "@Zakat", r.Zakat);
	        Sm.CmParam<Decimal>(ref cm, "@SSEmployerHealth", r.SSEmployerHealth);
	        Sm.CmParam<Decimal>(ref cm, "@SSEmployerEmployment", r.SSEmployerEmployment);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension", r.SSEmployerPension);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension2", r.SSEmployerPension2);
            Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixDeduction", r.NonTaxableFixDeduction);
            Sm.CmParam<Decimal>(ref cm, "@TaxableFixDeduction", r.TaxableFixDeduction);
	        Sm.CmParam<Decimal>(ref cm, "@FixDeduction", r.FixDeduction);
	        Sm.CmParam<Decimal>(ref cm, "@DedEmployee", r.DedEmployee);
	        Sm.CmParam<Decimal>(ref cm, "@DedProduction", r.DedProduction);
	        Sm.CmParam<Decimal>(ref cm, "@DedProdLeave", r.DedProdLeave);
            Sm.CmParam<Decimal>(ref cm, "@EmpAdvancePayment", r.EmpAdvancePayment);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeeHealth", r.SSEmployeeHealth);
	        Sm.CmParam<Decimal>(ref cm, "@SSEmployeeEmployment", r.SSEmployeeEmployment);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension", r.SSEmployeePension);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension2", r.SSEmployeePension2);
            Sm.CmParam<Decimal>(ref cm, "@SalaryPension", r.SalaryPension);
            Sm.CmParam<Decimal>(ref cm, "@SSErLifeInsurance", r.SSErLifeInsurance);
            Sm.CmParam<Decimal>(ref cm, "@SSEeLifeInsurance", r.SSEeLifeInsurance);
            Sm.CmParam<Decimal>(ref cm, "@SSErWorkingAccident", r.SSErWorkingAccident);
            Sm.CmParam<Decimal>(ref cm, "@SSEeWorkingAccident", r.SSEeWorkingAccident);
            Sm.CmParam<Decimal>(ref cm, "@SSErRetirement", r.SSErRetirement);
            Sm.CmParam<Decimal>(ref cm, "@SSEeRetirement", r.SSEeRetirement);
            Sm.CmParam<Decimal>(ref cm, "@SSErPension", r.SSErPension);
            Sm.CmParam<Decimal>(ref cm, "@SSEePension", r.SSEePension);
	        Sm.CmParam<Decimal>(ref cm, "@SalaryAdjustment", r.SalaryAdjustment);
            Sm.CmParam<String>(ref cm, "@CreditCode1", r.CreditCode1);
            Sm.CmParam<String>(ref cm, "@CreditCode2", r.CreditCode2);
            Sm.CmParam<String>(ref cm, "@CreditCode3", r.CreditCode3);
            Sm.CmParam<String>(ref cm, "@CreditCode4", r.CreditCode4);
            Sm.CmParam<String>(ref cm, "@CreditCode5", r.CreditCode5);
            Sm.CmParam<String>(ref cm, "@CreditCode6", r.CreditCode6);
            Sm.CmParam<String>(ref cm, "@CreditCode7", r.CreditCode7);
            Sm.CmParam<String>(ref cm, "@CreditCode8", r.CreditCode8);
            Sm.CmParam<String>(ref cm, "@CreditCode9", r.CreditCode9);
            Sm.CmParam<String>(ref cm, "@CreditCode10", r.CreditCode10);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment1", r.CreditAdvancePayment1);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment2", r.CreditAdvancePayment2);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment3", r.CreditAdvancePayment3);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment4", r.CreditAdvancePayment4);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment5", r.CreditAdvancePayment5);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment6", r.CreditAdvancePayment6);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment7", r.CreditAdvancePayment7);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment8", r.CreditAdvancePayment8);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment9", r.CreditAdvancePayment9);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment10", r.CreditAdvancePayment10);
            Sm.CmParam<String>(ref cm, "@PerformanceStatus", r.PerformanceStatus);
            Sm.CmParam<String>(ref cm, "@GradePerformance", r.GradePerformance);
            Sm.CmParam<Decimal>(ref cm, "@PerformanceValue", r.PerformanceValue);
            Sm.CmParam<Decimal>(ref cm, "@ADOT", r.ADOT);
	        Sm.CmParam<Decimal>(ref cm, "@Tax", r.Tax);
            Sm.CmParam<Decimal>(ref cm, "@TaxAllowance", r.TaxAllowance);
            Sm.CmParam<Decimal>(ref cm, "@Amt", r.Amt);
            Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive", r.ServiceChargeIncentive);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@RecomputedValueForFixedAD", r._RecomputedValueForFixedAD);
            Sm.CmParam<Decimal>(ref cm, "@RecomputedValueForProratedAD", r._RecomputedValueForProratedAD);

            return cm;
        }

        private MySqlCommand SavePayrollProcess2(ref Result1 r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPayrollProcess2 ");
            SQL.AppendLine("(PayrunCode, EmpCode, Dt, ProcessInd, LatestPaidDt, ");
            SQL.AppendLine("JoinDt, ResignDt, PosCode, DeptCode, GrdLvlCode, LevelCode, ");
            SQL.AppendLine("SystemType, EmploymentStatus, PayrunPeriod, PGCode, SiteCode, WorkingDay, ");
            SQL.AppendLine("WSCode, HolInd, HolidayIndex, WSHolidayInd, WSIn1, ");
            SQL.AppendLine("WSOut1, WSIn2, WSOut2, WSIn3, WSOut3, ");
            SQL.AppendLine("OneDayInd, LateInd, ActualIn, ActualOut, WorkingIn, ");
            SQL.AppendLine("WorkingOut, WorkingDuration, EmpSalary, EmpSalary2, BasicSalary, BasicSalary2, SalaryPension, ProductionWages, ");
            SQL.AppendLine("Salary, LeaveCode, LeaveType, PaidLeaveInd, LeaveStartTm,");
            SQL.AppendLine("LeaveEndTm, LeaveDuration, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
            SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, OT1Amt, ");
            SQL.AppendLine("OT2Amt, OTHolidayAmt, OTToLeaveInd, IncMinWages, IncProduction, IncEmployee, ");
            SQL.AppendLine("IncPerformance, PresenceRewardInd, HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, ServiceChargeIncentive, DedProduction, ");
            SQL.AppendLine("DedProdLeave, ADOT, CreateBy, CreateDt) ");
            SQL.AppendLine("Values( ");
            SQL.AppendLine("@PayrunCode, @EmpCode, @Dt, @ProcessInd, @LatestPaidDt, ");
            SQL.AppendLine("@JoinDt, @ResignDt, @PosCode, @DeptCode, @GrdLvlCode, @LevelCode, ");
            SQL.AppendLine("@SystemType, @EmploymentStatus, @PayrunPeriod, @PGCode, @SiteCode, @WorkingDay, ");
            SQL.AppendLine("@WSCode, @HolInd, @HolidayIndex, @WSHolidayInd, @WSIn1, ");
            SQL.AppendLine("@WSOut1, @WSIn2, @WSOut2, @WSIn3, @WSOut3, ");
            SQL.AppendLine("@OneDayInd, @LateInd, @ActualIn, @ActualOut, @WorkingIn, ");
            SQL.AppendLine("@WorkingOut, @WorkingDuration, @EmpSalary, @EmpSalary2, @BasicSalary, @BasicSalary2, @SalaryPension, @ProductionWages, ");
            SQL.AppendLine("@Salary, @LeaveCode, @LeaveType, @PaidLeaveInd, @LeaveStartTm,");
            SQL.AppendLine("@LeaveEndTm, @LeaveDuration, @PLDay, @PLHr, @PLAmt, @ProcessPLAmt, @UPLDay, @UPLHr, @UPLAmt, @ProcessUPLAmt, ");
            SQL.AppendLine("@OT1Hr, @OT2Hr, @OTHolidayHr, @OT1Amt, ");
            SQL.AppendLine("@OT2Amt, @OTHolidayAmt, @OTToLeaveInd, @IncMinWages, @IncProduction, @IncEmployee, ");
            SQL.AppendLine("@IncPerformance, @PresenceRewardInd, @HolidayEarning, @ExtraFooding, @FieldAssignment, @Transport, @Meal, @ServiceChargeIncentive, @DedProduction, ");
            SQL.AppendLine("@DedProdLeave, @ADOT, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
            Sm.CmParam<String>(ref cm, "@Dt", r.Dt);
            Sm.CmParam<String>(ref cm, "@ProcessInd", r.ProcessInd?"Y":"N");
            Sm.CmParam<String>(ref cm, "@LatestPaidDt", r.LatestPaidDt);
            Sm.CmParam<String>(ref cm, "@JoinDt", r.JoinDt);
            Sm.CmParam<String>(ref cm, "@ResignDt", r.ResignDt);
            Sm.CmParam<String>(ref cm, "@PosCode", r.PosCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", r.DeptCode);
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", r.GrdLvlCode);
            Sm.CmParam<String>(ref cm, "@LevelCode", r.LevelCode);
            Sm.CmParam<String>(ref cm, "@SystemType", r.SystemType);
            Sm.CmParam<String>(ref cm, "@EmploymentStatus", r.EmploymentStatus);
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", r.PayrunPeriod);
            Sm.CmParam<String>(ref cm, "@PGCode", r.PGCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", r.SiteCode);
            Sm.CmParam<Decimal>(ref cm, "@WorkingDay", r.WorkingDay);
            Sm.CmParam<String>(ref cm, "@WSCode", r.WSCode);
            Sm.CmParam<String>(ref cm, "@HolInd", r.HolInd?"Y":"N");
            Sm.CmParam<Decimal>(ref cm, "@HolidayIndex", r.HolidayIndex);
            Sm.CmParam<String>(ref cm, "@WSHolidayInd", r.WSHolidayInd?"Y":"N");
            Sm.CmParam<String>(ref cm, "@WSIn1", r.WSIn1);
            Sm.CmParam<String>(ref cm, "@WSOut1", r.WSOut1);
            Sm.CmParam<String>(ref cm, "@WSIn2", r.WSIn2);
            Sm.CmParam<String>(ref cm, "@WSOut2", r.WSOut2);
            Sm.CmParam<String>(ref cm, "@WSIn3", r.WSIn3);
            Sm.CmParam<String>(ref cm, "@WSOut3", r.WSOut3);
            Sm.CmParam<String>(ref cm, "@OneDayInd", r.OneDayInd ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LateInd", r.LateInd ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ActualIn", r.ActualIn);
            Sm.CmParam<String>(ref cm, "@ActualOut", r.ActualOut);
            Sm.CmParam<String>(ref cm, "@WorkingIn", r.WorkingIn);
            Sm.CmParam<String>(ref cm, "@WorkingOut", r.WorkingOut);
            Sm.CmParam<Decimal>(ref cm, "@WorkingDuration", r.WorkingDuration);
            Sm.CmParam<Decimal>(ref cm, "@EmpSalary", r.EmpSalary);
            Sm.CmParam<Decimal>(ref cm, "@EmpSalary2", r.EmpSalary2);
            Sm.CmParam<Decimal>(ref cm, "@BasicSalary", r.BasicSalary);
            Sm.CmParam<Decimal>(ref cm, "@BasicSalary2", r.BasicSalary2);
            Sm.CmParam<Decimal>(ref cm, "@SalaryPension", r.SalaryPension);
            Sm.CmParam<Decimal>(ref cm, "@ProductionWages", r.ProductionWages);
            Sm.CmParam<Decimal>(ref cm, "@Salary", r.Salary);
            Sm.CmParam<String>(ref cm, "@LeaveCode", r.LeaveCode);
            Sm.CmParam<String>(ref cm, "@LeaveType", r.LeaveType);
            Sm.CmParam<String>(ref cm, "@PaidLeaveInd", r.PaidLeaveInd ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LeaveStartTm", r.LeaveStartTm);
            Sm.CmParam<String>(ref cm, "@LeaveEndTm", r.LeaveEndTm);
            Sm.CmParam<Decimal>(ref cm, "@LeaveDuration", r.LeaveDuration);
            Sm.CmParam<Decimal>(ref cm, "@UPLDay", r.UPLDay);
            Sm.CmParam<Decimal>(ref cm, "@UPLHr", r.UPLHr);
            Sm.CmParam<Decimal>(ref cm, "@UPLAmt", r.UPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt", r.ProcessUPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@PLDay", r.PLDay);
            Sm.CmParam<Decimal>(ref cm, "@PLHr", r.PLHr);
            Sm.CmParam<Decimal>(ref cm, "@PLAmt", r.PLAmt);
            Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt", r.ProcessPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@OT1Hr", r.OT1Hr);
            Sm.CmParam<Decimal>(ref cm, "@OT2Hr", r.OT2Hr);
            Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr", r.OTHolidayHr);
            Sm.CmParam<Decimal>(ref cm, "@OT1Amt", r.OT1Amt);
            Sm.CmParam<Decimal>(ref cm, "@OT2Amt", r.OT2Amt);
            Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt", r.OTHolidayAmt);
            Sm.CmParam<String>(ref cm, "@OTToLeaveInd", r.OTToLeaveInd ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@IncMinWages", r.IncMinWages);
            Sm.CmParam<Decimal>(ref cm, "@IncProduction", r.IncProduction);
            Sm.CmParam<Decimal>(ref cm, "@IncEmployee", r.IncEmployee);
            Sm.CmParam<Decimal>(ref cm, "@IncPerformance", r.IncPerformance);
            Sm.CmParam<String>(ref cm, "@PresenceRewardInd", r.PresenceRewardInd ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@HolidayEarning", r.HolidayEarning);
            Sm.CmParam<Decimal>(ref cm, "@ExtraFooding", r.ExtraFooding);
            Sm.CmParam<Decimal>(ref cm, "@FieldAssignment", r.FieldAssignment);
            Sm.CmParam<Decimal>(ref cm, "Transport", r.Transport);
            Sm.CmParam<Decimal>(ref cm, "Meal", r.Meal);
            Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive", r.ServiceChargeIncentive);
            Sm.CmParam<Decimal>(ref cm, "@DedProduction", r.DedProduction);
            Sm.CmParam<Decimal>(ref cm, "@DedProdLeave", r.DedProdLeave);
            Sm.CmParam<Decimal>(ref cm, "@ADOT", r.ADOT);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePayrollProcess()
        {
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string PayrunPeriode = string.Empty;
            var cm = new MySqlCommand();

            if (mPayrunCodeFormatType == "1")
                PayrunPeriode = Sm.GetDte(DteStartDt).Substring(0, 6);
            else
                PayrunPeriode = Sm.GetDte(DteEndDt).Substring(0, 6);

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            if (Filter.Length != 0)
                Filter = " And (" + Filter + ")";
            else
                Filter = " And 1=0 ";

           
            SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=Null ");
            SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblEmpSSListHdr ");
            SQL.AppendLine("    Where CancelInd='Y' And DocNo=T.DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or T.PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpSSListHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And DocNo=T.DocNo ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And Yr=@SSYr ");
                SQL.AppendLine("    And Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblSalaryAdjustmentHdr T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or T.PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And T.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("And T.CancelInd='N' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblSalaryAdjustment2Dtl T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ((T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) Or T.PayrunCode Is Null) ");
            SQL.AppendLine("And T.Docno In (");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblSalaryAdjustment2Hdr ");
            SQL.AppendLine("    Where PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblEmpInsPntDtl Set PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (PayrunCode Is Not Null And PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And DocNo In ( ");
            SQL.AppendLine("    Select A.DocNo ");
            SQL.AppendLine("    From TblEmpInsPntHdr A ");
            SQL.AppendLine("    Inner Join TblInsPnt B On A.InsPntCode=B.InsPntCode ");
            SQL.AppendLine("    Inner Join TblInsPntCategory C On B.InspntCtCode=C.InspntCtCode ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblEmpSCIDtl A ");
            SQL.AppendLine("Inner Join TblEmpSCIHdr B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And B.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Set A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where (A.PayrunCode Is Null Or (A.PayrunCode Is Not Null And A.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblEmpPerformanceAllowance A ");
            SQL.AppendLine("Set A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where (A.PayrunCode Is Null Or (A.PayrunCode Is Not Null And A.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("And Concat(A.Yr, Mth) = '"+PayrunPeriode+"'  ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParam<String>(ref cm, "@SSYr", mSSYr);
            Sm.CmParam<String>(ref cm, "@SSMth", mSSMth);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            return cm;
        }

        private MySqlCommand SaveAdvancePaymentProcess(ref AdvancePaymentProcess x)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Delete From TblAdvancePaymentProcess ");
            SQL.AppendLine("Where DocNo=@DocNo And Yr=@Yr And Mth=@Mth And EmpCode=@EmpCode; ");

            SQL.AppendLine("Insert Into TblAdvancePaymentProcess ");
            SQL.AppendLine("(DocNo, Yr, Mth, EmpCode, Amt, PayrunCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @Yr, @Mth, @EmpCode, @Amt, @PayrunCode, @CreateBy, CurrentDateTime()); ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParam<String>(ref cm, "DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "Yr", x.Yr);
            Sm.CmParam<String>(ref cm, "Mth", x.Mth);
            Sm.CmParam<String>(ref cm, "EmpCode", x.EmpCode);
            Sm.CmParam<Decimal>(ref cm, "Amt", x.Amt);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePayrollProcessTax(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI, ref List<TI2> lTI2)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();

            var cm = new MySqlCommand();
            string TaxAddQuery = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='01' ");
            string TaxMinQuery = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='02' ");
            string AmtTHPQuery = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='03' ");

            SQL.AppendLine(TaxAddQuery);
            SQL2.AppendLine(TaxMinQuery);
            SQL3.AppendLine(AmtTHPQuery);

            string TaxAddAmt = Sm.GetValue(SQL.ToString(), r.PayrunCode, r.EmpCode, r.JoinDt);
            string TaxMinAmt = Sm.GetValue(SQL2.ToString(), r.PayrunCode, r.EmpCode, r.JoinDt);
            string THPAmt = Sm.GetValue(SQL3.ToString(), r.PayrunCode, r.EmpCode, r.JoinDt);

            ProcessTax(ref r, ref lNTI, ref  lTI, ref lTI2, decimal.Parse(THPAmt), decimal.Parse(TaxAddAmt), decimal.Parse(TaxMinAmt));

            var SQL4 = new StringBuilder();
            var cm4 = new MySqlCommand();

            SQL4.AppendLine("Update TblPayrollProcess1 ");
            SQL4.AppendLine("SET TaxAllowance = @TaxAllowance, Tax=@Tax, Amt=@Amt, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime()  ");
            SQL4.AppendLine("Where PayrunCode=@PayrunCode And EmpCode=@EmpCode; ");

            cm4.CommandText = SQL4.ToString();
            Sm.CmParam<String>(ref cm4, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParam<Decimal>(ref cm4, "@TaxAllowance", r.TaxAllowance);
            Sm.CmParam<Decimal>(ref cm4, "@Tax", r.Tax);
            Sm.CmParam<Decimal>(ref cm4, "@Amt", r.Amt);
            Sm.CmParam<String>(ref cm4, "@EmpCode", r.EmpCode);
            Sm.CmParam<String>(ref cm4, "@CreateBy", Gv.CurrentUserCode);

            return cm4;
        }


        #endregion

        #region Process Data

        private void ProcessData()
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsProcessedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            
            #region Get Table Data

            var lEmployeePPS = new List<EmployeePPS>();
            var lOT = new List<OT>();
            var lNTI = new List<NTI>();
            var lTI2 = new List<TI2>();
            var lTI = new List<TI>();
            var lEmployeeADOT = new List<EmployeeADOT>();
            var lLeaveMealTransport = new List<LeaveMealTransport>();
            
            ProcessOT(ref lOT);
            ProcessNTI(ref lNTI);
            ProcessTI2(ref lTI2);
            ProcessAdvancePaymentProcess(ref mlAdvancePaymentProcess);
            ProcessEmployeeADOT(ref lEmployeeADOT);
            ProcessLeaveMealTransport(ref lLeaveMealTransport);

            #endregion

            var lPayrollProcessADOT = new List<PayrollProcessADOT>();

            var lResult1 = new List<Result1>();

            Process1(ref lResult1);
            Process2(ref lResult1);
            Process3(ref lResult1);

            if (lResult1.Count > 0)
            {
                lResult1.ForEach(r =>{ 
                    Process4(
                        ref r, 
                        ref lEmployeeADOT, 
                        ref lPayrollProcessADOT,
                        ref lLeaveMealTransport
                        ); });
                lResult1.Sort(
                    delegate(Result1 r1a, Result1 r1b)
                    {
                        int f = r1a.EmpCode.CompareTo(r1b.EmpCode);
                        return f != 0 ? f : r1a.Dt.CompareTo(r1b.Dt);
                    });
            }

            var lResult2 = new List<Result2>();

            Process5(ref lResult2);

            lResult2.ForEach(r => { Process6(ref r, ref lResult1, ref lNTI, ref lTI2); });

            #region Save Data

            var cml = new List<MySqlCommand>();
            var cml2 = new List<MySqlCommand>();

            cml.Add(DeletePayrollProcess());

            if (lPayrollProcessADOT.Count > 0)
            {
                foreach (var r in lPayrollProcessADOT.Where(w => w.ADCode.Length > 0))
                    cml.Add(SavePayrollProcessADOT(r));
            }
            lResult2.ForEach(r =>
            {
                cml.Add(SavePayrollProcess1(ref r));
            });
            lResult1.ForEach(r => { cml.Add(SavePayrollProcess2(ref r)); });
            cml.Add(SavePayrollProcess());

            if (mlAdvancePaymentProcess.Count > 0)
                mlAdvancePaymentProcess.ForEach(x => { cml.Add(SaveAdvancePaymentProcess(ref x)); });

            Sm.ExecCommands(cml);

            if (lResult2.Count > 0)
            {
                lResult2.ForEach(r =>
                {
                    cml2.Add(SavePayrollProcessTax(ref r, ref lNTI, ref lTI, ref lTI2));
                });
            }

            Sm.ExecCommands(cml2);

            #endregion

            LuePayrunCode.EditValue = null;
            ClearData();
        }

        private void Process1(ref List<Result1> lResult1)
        {
            var lDt = new List<string>();
            GetDt(ref lDt);

            var lEmployeePPS = new List<EmployeePPS>();
            ProcessEmployeePPS(ref lEmployeePPS);

            if (Grd1.Rows.Count >= 1 && lDt.Count>0 && lEmployeePPS.Count>0)
            {
                var PayrunCodeTemp = Sm.GetLue(LuePayrunCode);
                var EmpCodeTemp = string.Empty;
                var DeptCodeTemp = string.Empty;
                var GrdLvlCodeTemp = string.Empty;
                var LevelCodeTemp = string.Empty;
                var EmploymentStatusTemp = string.Empty;
                var SystemTypeTemp = string.Empty;
                var PayrunPeriodTemp = string.Empty;
                var PGCodeTemp = string.Empty;
                var SiteCodeTemp = string.Empty;
                var PosCodeTemp = string.Empty;
                var HOIndTemp = string.Empty; 
                
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCodeTemp = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCodeTemp.Length != 0)
                    {
                        foreach (string DtTemp in lDt)
                        {
                            DeptCodeTemp = string.Empty;
                            GrdLvlCodeTemp = string.Empty;
                            LevelCodeTemp = string.Empty;
                            EmploymentStatusTemp = string.Empty;
                            SystemTypeTemp = string.Empty;
                            PayrunPeriodTemp = string.Empty;
                            PGCodeTemp = string.Empty;
                            SiteCodeTemp = string.Empty;
                            PosCodeTemp = string.Empty;
                            HOIndTemp = string.Empty;

                            foreach (var i in 
                                lEmployeePPS
                                    .Where(Index => Index.EmpCode == EmpCodeTemp)
                                    .OrderByDescending(x=>x.StartDt)) 
                            {
                                if(Sm.CompareDtTm(DtTemp, i.StartDt)>=0)
                                {
                                    DeptCodeTemp = i.DeptCode;
                                    GrdLvlCodeTemp = i.GrdLvlCode;
                                    LevelCodeTemp = i.LevelCode;
                                    EmploymentStatusTemp = i.EmploymentStatus;
                                    SystemTypeTemp = i.SystemType;
                                    PayrunPeriodTemp = i.PayrunPeriod;
                                    PGCodeTemp = i.PGCode;
                                    SiteCodeTemp = i.SiteCode;
                                    PosCodeTemp = i.PosCode;
                                    HOIndTemp = i.HOInd;
                                    break;
                                };
                            }

                            lResult1.Add(new Result1()
                                {
                                    PayrunCode = PayrunCodeTemp,
                                    EmpCode = EmpCodeTemp,
                                    Dt = DtTemp,
                                    DeptCode=DeptCodeTemp,
                                    GrdLvlCode = GrdLvlCodeTemp,
                                    LevelCode = LevelCodeTemp,
                                    EmploymentStatus=EmploymentStatusTemp,
                                    SystemType=SystemTypeTemp,
                                    PayrunPeriod=PayrunPeriodTemp,
                                    PGCode = PGCodeTemp,
                                    SiteCode = SiteCodeTemp,
                                    PosCode=PosCodeTemp,
                                    _IsHOInd = HOIndTemp=="Y",
                                    ProcessInd = true,
                                    LatestPaidDt = string.Empty,
                                    JoinDt = string.Empty,
                                    ResignDt = string.Empty,
                                    WorkingDay = 0m,
                                    WSCode  = string.Empty,
                                    HolInd = false,
                                    HolidayIndex = mStdHolidayIndex,
                                    WSHolidayInd = false,
                                    WSIn1  = string.Empty,
                                    WSOut1  = string.Empty,
                                    WSIn2  = string.Empty,
                                    WSOut2  = string.Empty,
                                    WSIn3  = string.Empty,
                                    WSOut3  = string.Empty,
                                    OneDayInd = true,
                                    LateInd = false,
                                    ActualIn  = string.Empty,
                                    ActualOut  = string.Empty,
                                    WorkingIn  = string.Empty,
                                    WorkingOut  = string.Empty,
                                    WorkingDuration = 0m,
                                    BasicSalary = 0m,
                                    BasicSalary2 = 0m,
                                    _LatestMonthlyGrdLvlSalary = 0m,
                                    _LatestDailyGrdLvlSalary = 0m,
                                    ProductionWages = 0m,
                                    Salary = 0m,
                                    LeaveCode  = string.Empty,
                                    LeaveType = string.Empty,
                                    PaidLeaveInd = false,
                                    CompulsoryLeaveInd = false,
                                    _DeductTHPInd = false,
                                    LeaveStartTm  = string.Empty,
                                    LeaveEndTm  = string.Empty,
                                    LeaveDuration = 0m,
                                    PLDay = 0m,
                                    PLHr = 0m,
                                    PLAmt = 0m,
                                    ProcessPLAmt = 0m,
                                    OT1Hr = 0m,
                                    OT2Hr = 0m,
                                    OTHolidayHr = 0m,
                                    OT1Amt = 0m,
                                    OT2Amt = 0m,
                                    OTHolidayAmt = 0m,
                                    OTToLeaveInd = false,
                                    IncMinWages = 0m,
                                    IncProduction = 0m,
                                    IncPerformance = 0m,
                                    PresenceRewardInd = false,
                                    HolidayEarning = 0m,
                                    ExtraFooding = 0m,
                                    FieldAssignment = 0m,
                                    DedProduction = 0m,
                                    DedProdLeave = 0m,
                                    EmpSalary  = 0m,
                                    EmpSalary2 = 0m,
                                    SalaryPension = 0m,
                                    UPLDay  = 0m,
                                    UPLHr  = 0m,
                                    UPLAmt = 0m,
                                    ProcessUPLAmt = 0m,
                                    Transport = 0m,
                                    Meal = 0m,
                                    ServiceChargeIncentive = 0m,
                                    IncEmployee = 0m,
                                    ADOT = 0m,
                                    _ExtraFooding = 0m,
                                    _FieldAssignment = 0m,
                                    _IncPerformance = 0m,
                                    _IsFullDayInd = false,
                                    _IsUseLatestGrdLvlSalary = false
                                });
                        }
                    }
                }
                lDt.Clear();
                lEmployeePPS.Clear();
            }
        }

        private void Process2(ref List<Result1> lResult1)
        {
            if (lResult1.Count == 0) return;

            #region Employee's Work Schedule

            var lEmpWorkSchedule = new List<EmpWorkSchedule>();
            ProcessEmpWorkSchedule(ref lEmpWorkSchedule);
            if (lEmpWorkSchedule.Count > 0)
            {
                var WSCodesForHoliday = mWSCodesForHoliday.Split('#');
                var WSCodesForNationalHoliday = mWSCodesForNationalHoliday.Split('#');

               foreach (var ews in lEmpWorkSchedule)
               {
                    foreach (var r in lResult1.Where(Index => Sm.CompareStr(Index.EmpCode, ews.EmpCode) && Sm.CompareStr(Index.Dt, ews.Dt)))
                    {
                        r.WSCode = ews.WSCode;
                        r.WSHolidayInd = ews.HolidayInd=="Y";
                        r.WSIn1 = ews.In1;
                        r.WSOut1 = ews.Out1;
                        r.WSIn2 = ews.In2;
                        r.WSOut2 = ews.Out2;
                        r.WSIn3 = ews.In3;
                        r.WSOut3 = ews.Out3;
                        r._DeductionRound = ews.DeductionRound;
                    }
               }
               lEmpWorkSchedule.Clear();
            }

            #endregion

            #region Employee's Leave Hour Process

            var lEmpLeaveHourProcess = new List<EmpLeaveHourProcess>();
            ProcessEmpLeaveHourProcess(ref lEmpLeaveHourProcess);
            if (lEmpLeaveHourProcess.Count > 0)
            {
                foreach (var x in lEmpLeaveHourProcess)
                {
                    foreach (var r in lResult1.Where(i => Sm.CompareStr(i.EmpCode, x.EmpCode) && Sm.CompareStr(i.Dt, x.Dt)))
                    {
                        r._LeaveDuration = x.Duration;
                    }
                }
                lEmpLeaveHourProcess.Clear();
            }

            #endregion

            #region Employee's Paid Date

            var lEmpPaidDt = new List<EmpPaidDt>();
            ProcessEmpPaidDt(ref lEmpPaidDt);
            if (lEmpPaidDt.Count > 0)
            {
                lResult1.ForEach(A =>
                {
                    foreach (var x in lEmpPaidDt.Where(x => x.EmpCode == A.EmpCode && x.Dt == A.Dt))
                    {
                        A.LatestPaidDt = x.Dt;
                        break;
                    }
                });

                //lResult1.ForEach(A =>
                //{
                    
                //    A.LatestPaidDt = lEmpPaidDt.FirstOrDefault(
                //        x => A.EmpCode.Equals(x.EmpCode??"") && A.Dt.Equals(x.Dt??"")
                //        ).Dt??"";
                //});
                
               lEmpPaidDt.Clear();
            }

            #endregion

            #region Attendance

            var lAtd = new List<Atd>();
            ProcessAtd(ref lAtd);
            if (lAtd.Count > 0)
            {
                foreach (var atd in lAtd)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == atd.EmpCode && Index.Dt == atd.Dt))
                    {
                        r.ActualIn = atd.ActualIn;
                        r.ActualOut = atd.ActualOut;
                        break;
                    }
                }
                lAtd.Clear();
            }

            #endregion

            #region Leave

            var lLeave = new List<Leave>();
            ProcessLeave(ref lLeave);

            var lLeaveDtl = new List<LeaveDtl>();
            ProcessLeaveDtl(ref lLeaveDtl);

            if (lLeave.Count > 0)
            {
                foreach (var l in lLeave)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == l.EmpCode && Index.Dt == l.Dt))
                    {
                        r.LeaveCode = l.LeaveCode;
                        r.LeaveType = l.LeaveType;
                        r.LeaveStartTm = l.StartTm;
                        r.LeaveEndTm = l.EndTm;
                        r.LeaveDuration = l.DurationHr;
                        r.PaidLeaveInd = l.PaidInd=="Y";
                        r.CompulsoryLeaveInd = l.CompulsoryLeaveInd == "Y";
                        foreach (var ld in lLeaveDtl.Where(
                            Index => 
                                Index.LeaveCode == l.LeaveCode && 
                                Index.SystemType == r.SystemType &&
                                Index.PayrunPeriod == r.PayrunPeriod
                                ))
                        {
                            r._DeductTHPInd = ld.DeductTHPInd;
                        }
                        break;
                    }
                }
                lLeave.Clear();
                lLeaveDtl.Clear();
            }

            #endregion

            #region Employee

            var lEmployee = new List<Employee>();
            ProcessEmployee(ref lEmployee);
            if (lEmployee.Count > 0)
            {
                foreach (var e in lEmployee)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == e.EmpCode))
                    {
                        r.JoinDt = e.JoinDt;
                        r.ResignDt = e.ResignDt;

                        if (Sm.CompareDtTm(r.Dt, r.JoinDt) < 0) r.ProcessInd = false;

                        if (r.ResignDt.Length > 0)
                        {
                            if (Sm.CompareDtTm(r.Dt, r.ResignDt) >= 0) r.ProcessInd = false;
                        }

                        if (r.LatestPaidDt.Length > 0) r.ProcessInd = false;
                    }
                }
                lEmployee.Clear();
            }

            #endregion

            #region EmployeeSalary

            var lEmployeeSalary = new List<EmployeeSalary>();
            ProcessEmployeeSalary(ref lEmployeeSalary);
            if (lEmployeeSalary.Count > 0)
            {
                foreach (var r in lResult1.OrderByDescending(Index => Index.Dt))
                {
                    foreach (var esl in lEmployeeSalary
                        .Where(Index => Index.EmpCode == r.EmpCode)
                        .OrderByDescending(Index => Index.StartDt))
                    {
                        if (Sm.CompareDtTm(esl.StartDt, r.Dt) <= 0)
                        {
                            r.EmpSalary = esl.Amt;
                            r.EmpSalary2 = esl.Amt2;
                            break;
                        }
                    }
                }
                lEmployeeSalary.Clear();
            }

            #endregion

             #region Grade Level

            var lGradeLevel = new List<GradeLevel>();
            ProcessGradeLevel(ref lGradeLevel);
            if (lGradeLevel.Count > 0)
            {
                foreach (var gl in lGradeLevel)
                {
                    foreach (var r in lResult1.Where(Index => Index.GrdLvlCode == gl.GrdLvlCode))
                    {
                        r.BasicSalary = gl.BasicSalary;
                        r.BasicSalary2 = gl.BasicSalary2;
                    }
                }
                lGradeLevel.Clear();
            }

            #endregion

            #region EmployeeSalarySS

            var lEmployeeSalarySS = new List<EmployeeSalarySS>();
            ProcessEmployeeSalarySS(ref lEmployeeSalarySS);
            if (lEmployeeSalarySS.Count > 0)
            {
                foreach (var r in lResult1.OrderByDescending(Index => Index.Dt))
                {
                    foreach (var i in lEmployeeSalarySS
                        .Where(w => Sm.CompareStr(w.EmpCode, r.EmpCode))
                        .OrderByDescending(o => o.StartDt))
                    {
                        if (Sm.CompareDtTm(i.StartDt, r.Dt) <= 0)
                        {
                            r.SalaryPension = i.Amt;
                            break;
                        }
                    }
                }
                lEmployeeSalarySS.Clear();
            }

            #endregion

            #region Attendance

            mlAttendanceLog.Clear();
            ProcessAttendanceLog(ref mlAttendanceLog);

            #endregion

            #region OT

            mlOT.Clear();
            ProcessOT(ref mlOT);

            #endregion

            #region OT Adjustment

            mlOTAdjustment.Clear();
            ProcessOTAdjustment(ref mlOTAdjustment);

            #endregion

            #region Employee Allowance (Meal, Transport)

            ProcessEmployeeAllowance(ref mlEmployeeAllowanceMeal, mADCodeMeal);
            ProcessEmployeeAllowance(ref mlEmployeeAllowanceTransport, mADCodeTransport);

            #endregion

            #region OT Incentive

            ProcessOTIncentive(ref mlOTIncentive);

            #endregion

            #region OT AD MEAL TRANSPORT

            ProcessWorkScheduleADMealTransport(ref mlWorkScheduleADMealTransport);

            #endregion
        }

        private void Process3(ref List<Result1> lResult1)
        {
            if (lResult1.Count == 0) return;

            var EmpCodeTemp = string.Empty;
            var PayrunPeriodTemp = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                string 
                    SystemTypeTemp = string.Empty, 
                    GrdLvlCodeTemp = string.Empty,
                    LevelCodeTemp = string.Empty;

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCodeTemp = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCodeTemp.Length != 0)
                    {
                        SystemTypeTemp = string.Empty;
                        PayrunPeriodTemp = string.Empty;
                        GrdLvlCodeTemp = string.Empty;
                        LevelCodeTemp = string.Empty;

                        foreach (var x in lResult1.Where(x => string.Compare(x.EmpCode, EmpCodeTemp) == 0).OrderBy(x => x.Dt))
                        {
                            x.Salary = x.BasicSalary2;  // Berdasarkan Employee Salary
                            
                            if (x.ProcessInd)
                            {
                                if (!(
                                    Sm.CompareStr(x.DeptCode, mDeptCode) &&
                                    Sm.CompareStr(x.SystemType, mSystemType) &&
                                    Sm.CompareStr(x.PayrunPeriod, mPayrunPeriod) &&
                                    Sm.CompareStr(x.PGCode, mPGCode) 
                                    //&& Sm.CompareStr(x.SiteCode, mSiteCode)
                                    ))
                                    x.ProcessInd = false;
                                else
                                    LevelCodeTemp = x.LevelCode;
                                
                            }
                        }
                        foreach (var x in lResult1
                                .Where(x => string.Compare(x.EmpCode, EmpCodeTemp) == 0)
                                .OrderBy(x => x.Dt))
                            x._LatestLevelCode = LevelCodeTemp;
                    }
                }
            }
        }

        private void Process4(ref Result1 r, 
            ref List<EmployeeADOT> lEmployeeADOT,
            ref List<PayrollProcessADOT> lPayrollProcessADOT,
            ref List<LeaveMealTransport> lLeaveMealTransport
            )
        {
            string
                ActualDtIn = string.Empty, 
                ActualDtOut = string.Empty,
                ActualTmIn = string.Empty, 
                ActualTmOut = string.Empty,
                EmpCode = r.EmpCode, 
                Dt = r.Dt;

            #region Set Date2 untuk Shift 3

            var Dt2 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(1)).Substring(0, 8);

            #endregion

            #region Set Join Date And Resign Date

            var JoinDt = Sm.ConvertDateTime(r.JoinDt + "0000");
            var ResignDt = Sm.ConvertDateTime("900012312359");
            if (r.ResignDt.Length > 0) ResignDt = Sm.ConvertDateTime(r.ResignDt + "2359");

            #endregion

            #region Set Actual Date/Time

            if (r.ActualIn.Length > 0)
            {
                ActualDtIn = Sm.Left(r.ActualIn, 8);
                ActualTmIn = r.ActualIn.Substring(8, 4);
            }

            if (r.ActualOut.Length > 0)
            {
                ActualDtOut = Sm.Left(r.ActualOut, 8);
                ActualTmOut = r.ActualOut.Substring(8, 4);
            }

            #endregion

            #region set shift 1,2 atau shift 3

            if (r.WSIn1.Length > 0 && 
                r.WSOut1.Length > 0 && 
                Sm.CompareDtTm(r.WSIn1, r.WSOut1)>0)
                //int.Parse(r.WSIn1) > int.Parse(r.WSOut1))
                r.OneDayInd = false;

            #endregion

            #region cek apakah karyawan terlambat

            if (ActualTmIn.Length > 0 && r.WSIn1.Length > 0)
            {
                if (Sm.CompareDtTm(r.Dt + r.WSIn1, ActualDtIn + ActualTmIn) < 0)
                {    
                    r.LateInd = true;
                }
            }

            #endregion

            #region set jam masuk

            if (r.Dt.Length > 0 && r.WSIn1.Length > 0 && ActualDtIn.Length > 0 && ActualTmIn.Length > 0)
            {
                if (mIsWorkingHrBasedOnSchedule)
                {
                    r.WorkingIn = r.Dt + r.WSIn1;
                }
                else
                {
                    if (Sm.CompareDtTm(r.Dt + r.WSIn1, ActualDtIn + ActualTmIn) < 0)
                        r.WorkingIn = ActualDtIn + ActualTmIn;
                    else
                        r.WorkingIn = r.Dt + r.WSIn1;
                }
            }

            #endregion

            #region set jam keluar

            if (r.Dt.Length > 0 && r.WSOut1.Length > 0 && ActualDtOut.Length > 0 && ActualTmOut.Length > 0)
            {
                if (r.OneDayInd)
                {
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        r.WorkingOut = r.Dt + r.WSOut1;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(r.Dt + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                            r.WorkingOut = ActualDtOut + ActualTmOut;
                        else
                            r.WorkingOut = r.Dt + r.WSOut1;
                    }
                }
                else
                {
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        r.WorkingOut = Dt2 + r.WSOut1;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(Dt2 + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                            r.WorkingOut = ActualDtOut + ActualTmOut;
                        else
                            r.WorkingOut = Dt2 + r.WSOut1;
                    }
                }
            }

            #endregion

            #region set OT rutin

            string ROT1 = string.Empty, ROT2 = string.Empty;

            #region Set OT Rutin (In)

            if (ActualDtIn.Length > 0 && ActualTmIn.Length > 0 && r.WSIn3.Length > 0)
            {
                if (r.OneDayInd)
                {
                    //Shift 1, 2
                    //if (mIsWorkingHrBasedOnSchedule)
                    //{
                    //    ROT1 = r.Dt + r.WSIn3;
                    //}
                    //else
                    //{
                    if (Sm.CompareDtTm(r.Dt + r.WSIn3, ActualDtIn + ActualTmIn) < 0)
                    {
                        //1500 < 1600
                        ROT1 = ActualDtIn + ActualTmIn;
                    }
                    else
                    {
                        //1500 > 1400
                        ROT1 = r.Dt + r.WSIn3;
                    }
                    //}
                }
                else
                {
                    //Shift 3
                    //if (mIsWorkingHrBasedOnSchedule)
                    //{
                    //    ROT1 = Dt2 + r.WSIn3;
                    //}
                    //else
                    //{
                    if (Sm.CompareDtTm(Dt2 + r.WSIn3, ActualDtIn + ActualTmIn) < 0)
                    {
                        //0400<0500
                        ROT1 = ActualDtIn + ActualTmIn;
                    }
                    else
                    {
                        //0400>0300
                        ROT1 = Dt2 + r.WSIn3;
                    }
                    //}
                }
            }

            #endregion

            #region Set OT Rutin (Out)

            if (ActualDtOut.Length > 0 && ActualTmOut.Length > 0 && r.WSOut3.Length > 0)
            {
                if (r.OneDayInd)
                {
                    ////Shift 1, 2
                    //if (mIsWorkingHrBasedOnSchedule)
                    //{
                    //    ROT2 = r.Dt + r.WSOut3;
                    //}
                    //else
                    //{
                        if (Sm.CompareDtTm(r.Dt + r.WSOut3, ActualDtOut + ActualTmOut) < 0)
                        {
                            //1900<2000
                            ROT2 = r.Dt + r.WSOut3;
                        }
                        else
                        {
                            //1900>1800
                            ROT2 = ActualDtOut + ActualTmOut;
                        }
                    //}
                }
                else
                {
                    //Shift 3
                    //if (mIsWorkingHrBasedOnSchedule)
                    //{
                    //    ROT2 = Dt2 + r.WSOut3;
                    //}
                    //else
                    //{
                    if (Sm.CompareDtTm(Dt2 + r.WSOut3, ActualDtOut + ActualTmOut) < 0)
                    {
                        //0300<0400
                        ROT2 = Dt2 + r.WSOut3;
                    }
                    else
                    {
                        //0300>0200
                        ROT2 = ActualDtOut + ActualTmOut;
                    }
                    //}
                }
            }

            #endregion

            #endregion

            #region set OT rutin duration

            var ROTDuration = 0m;
            if (ROT1.Length > 0 && ROT2.Length > 0)
            {
                ROTDuration = ComputeDuration(ROT2, ROT1);
                if (ROTDuration < 0) ROTDuration = 0m;
                if (mIsOTRoutineRounding && ROTDuration > 0)
                {
                    ROTDuration = RoundTo1Hour((double)ROTDuration);
                }
            }

            #endregion

            #region set working duration

            decimal 
                WorkingDuration = 0m, 
                BreakDuration = 0m;

            if (r.WorkingIn.Length > 0 && r.WorkingOut.Length > 0)
            {
                if (r.LeaveType.Length>0)
                {
                    WorkingDuration = ComputeDuration(r.WorkingOut, r.WorkingIn);
                }
                else
                {
                    //No Leave
                    WorkingDuration = ComputeDuration(r.WorkingOut, r.WorkingIn) + ROTDuration;
                    if (r.WSIn2.Length > 0 && r.WSOut2.Length > 0)
                    {
                        // Menghitung lama waktu istirahat
                        if (r.OneDayInd)
                        {
                            // Shift 1, 2
                            if (Sm.CompareDtTm(r.WorkingOut, r.Dt+r.WSIn2) > 0)
                                BreakDuration = ComputeDuration(r.Dt + r.WSOut2, r.Dt + r.WSIn2);
                        }
                        else
                        {
                            //Shift 3
                            if (Sm.CompareDtTm(r.WSIn2, r.WSOut2)<=0)
                            //if (decimal.Parse(r.WSIn2) <= decimal.Parse(r.WSOut2))
                                BreakDuration = ComputeDuration(r.Dt + r.WSOut2, r.Dt + r.WSIn2);
                            else
                                BreakDuration = ComputeDuration(Dt2 + r.WSOut2, r.Dt + r.WSIn2);
                        }
                    }
                    WorkingDuration -= BreakDuration;  
                }
            }

            r.WorkingDuration = Convert.ToDecimal(WorkingDuration);

            #endregion

            #region set leave duration (fullday leave)

            if (r.LeaveType == "F")
            {
                if (!r.WSHolidayInd && !r.HolInd)
                    r.LeaveDuration = mWorkingHr;
                else
                {
                    r.LeaveCode = string.Empty;
                    r.LeaveType = string.Empty;
                    r.PaidLeaveInd = false;
                    r.CompulsoryLeaveInd = false;
                    r.LeaveDuration = 0m;
                }
            }
            
            #endregion
            
            #region Menghitung OT

            decimal TotalOTHr = 0m;

            if (r.SystemType == mEmpSystemTypeHarian)
            {
                decimal OTHr = 0m;
                string
                    StartTm = string.Empty,
                    EndTm = string.Empty;

                if (mOTRequestType == "2")
                {
                    #region Use OT Request

                    TotalOTHr += ROTDuration;
                    foreach (var x in mlOT.Where(x => x.EmpCode == EmpCode && x.Dt == Dt))
                    {
                        OTHr = 0m;
                        StartTm = x.Tm1;
                        EndTm = x.Tm2;

                        if (Dt.Length > 0 && EndTm.Length > 0 && StartTm.Length > 0)
                        {
                            if (EndTm == "0000")
                                OTHr = ComputeDuration(Dt2 + EndTm, Dt + StartTm);
                            else
                            {
                                if (Sm.CompareDtTm(StartTm, EndTm) <= 0)
                                    OTHr = ComputeDuration(Dt + EndTm, Dt + StartTm);
                                else
                                    OTHr = ComputeDuration(Dt2 + EndTm, Dt + StartTm);
                            }
                        }
                        if (OTHr < 0) OTHr = 0;
                        TotalOTHr += OTHr;
                    }

                    #endregion
                }
                else
                {
                    bool
                        IsOTAdjusted = false,
                        IsOTSpecial = false; // beda tanggal

                    string
                        StartTmTemp = string.Empty,
                        EndTmTemp = string.Empty,
                        OTDtTemp = r.OneDayInd ? Dt : Dt2,
                        SpecialStartDt = string.Empty,
                        SpecialStartTm = string.Empty,
                        SpecialEndDt = string.Empty,
                        SpecialEndTm = string.Empty
                        ;

                    foreach (var x in mlOT.Where(x => x.EmpCode == EmpCode && x.Dt == Dt))
                    {
                        IsOTAdjusted = false;
                        OTHr = 0m;
                        StartTm = x.Tm1;
                        EndTm = x.Tm2;

                        #region Use OT Adjustment

                        foreach (var ota in mlOTAdjustment.Where(
                            ota =>
                                ota.OTDocNo == x.OTDocNo &&
                                ota.EmpCode == EmpCode &&
                                ota.Dt == Dt
                                ))
                        {
                            IsOTAdjusted = true;
                            StartTm = ota.Tm1;
                            EndTm = ota.Tm2;
                        }

                        #endregion

                        #region Use Attendance Barcode

                        if (!IsOTAdjusted)
                        {
                            StartTmTemp = string.Empty;
                            EndTmTemp = string.Empty;

                            //Value1<Value2 : -1
                            //Value1=Value2 : 0
                            //Value1>Value2 : 1
                            var IsOTOneDayInd = true;
                            var Tm1Temp = string.Empty;

                            if (r.OneDayInd)
                            {
                                #region Shift 1 & 2

                                if (Sm.CompareDtTm(StartTm, EndTm) > 0)
                                {
                                    #region Case 1

                                    IsOTSpecial = true;

                                    foreach (var index in mlAttendanceLog
                                                    .Where(
                                                        i =>
                                                            i.EmpCode == EmpCode &&
                                                            i.Dt == Dt &&
                                                            Sm.CompareDtTm(Dt + "0000", (i.Dt + i.Tm)) <= 0
                                                        )
                                                    .OrderBy(o1 => o1.Dt)
                                                    .ThenBy(o2 => o2.Tm)
                                                    .Take(1)
                                                    )
                                    {
                                        SpecialStartDt = index.Dt;
                                        SpecialStartTm = index.Tm;
                                    }

                                    if (
                                        SpecialStartDt.Length > 0 &&
                                        SpecialStartTm.Length > 0 &&
                                        Sm.CompareDtTm(SpecialStartDt + SpecialStartTm, Dt + StartTm) <= 0
                                        )
                                    {
                                        SpecialStartDt = Dt;
                                        SpecialStartTm = StartTm;
                                    }

                                    if (SpecialStartDt.Length > 0 && SpecialStartTm.Length > 0)
                                    {
                                        foreach (var index in mlAttendanceLog
                                               .Where(
                                                   i =>
                                                       i.EmpCode == EmpCode &&
                                                       (i.Dt == Dt || i.Dt == Dt2) &&
                                                       Sm.CompareDtTm(Dt + StartTm, (i.Dt + i.Tm)) < 0 &&
                                                       Sm.CompareDtTm((i.Dt + i.Tm), Dt2 + EndTm) <= 0
                                                   )
                                               .OrderByDescending(o1 => o1.Dt)
                                               .ThenByDescending(o2 => o2.Tm)
                                               .Take(1)
                                               )
                                        {
                                            SpecialEndDt = index.Dt;
                                            SpecialEndTm = index.Tm;
                                        }


                                        if (SpecialEndDt.Length <= 0 || SpecialEndTm.Length <= 0)
                                        {
                                            foreach (var index in mlAttendanceLog
                                                   .Where(
                                                       i =>
                                                           i.EmpCode == EmpCode &&
                                                           i.Dt == Dt2 &&
                                                           Sm.CompareDtTm(Dt2 + EndTm, i.Dt + i.Tm) <= 0
                                                       )
                                                   .OrderBy(o1 => o1.Dt)
                                                   .ThenBy(o2 => o2.Tm)
                                                   .Take(1)
                                                   )
                                            {
                                                SpecialEndDt = index.Dt;
                                                SpecialEndTm = index.Tm;
                                            }

                                            if (
                                                SpecialEndDt.Length > 0 &&
                                                SpecialEndTm.Length > 0 &&
                                                Sm.CompareDtTm(Dt2 + EndTm, SpecialEndDt + SpecialEndTm) <= 0
                                            )
                                            {
                                                SpecialEndDt = Dt2;
                                                SpecialEndTm = EndTm;
                                            }
                                        }
                                    }
                                    StartTmTemp = SpecialStartTm;
                                    EndTmTemp = SpecialEndTm;

                                    #endregion
                                }

                                if (Sm.CompareDtTm(StartTm, EndTm) <= 0)
                                {
                                    #region case 2
                                    // Standard

                                    foreach (var index in mlAttendanceLog
                                    .Where(
                                        i =>
                                            i.EmpCode == EmpCode &&
                                            i.Dt == Dt &&
                                            Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
                                        )
                                    .OrderByDescending(o => o.Tm)
                                    .Take(1)
                                    )
                                    {
                                        StartTmTemp = Sm.Left(index.Tm, 4);
                                    }

                                    if (StartTmTemp.Length > 0)
                                    {
                                        if (x.Tm2 == "0000")
                                        {
                                            foreach (var index in mlAttendanceLog
                                            .Where(
                                                i =>
                                                    i.EmpCode == EmpCode &&
                                                    i.Dt == Dt &&
                                                    Sm.CompareDtTm(i.Tm, x.Tm1) > 0 &&
                                                    Sm.CompareDtTm(i.Tm, "2359") <= 0
                                                )
                                            .OrderByDescending(o => o.Tm)
                                            .Take(1)
                                            )
                                            {
                                                EndTmTemp = Sm.Left(index.Tm, 4);
                                            }

                                            if (EndTmTemp.Length <= 0)
                                            {
                                                foreach (var index in mlAttendanceLog
                                                    .Where(
                                                        i =>
                                                            i.EmpCode == EmpCode &&
                                                            i.Dt == Dt2 &&
                                                            Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                        )
                                                    .OrderBy(o => o.Tm)
                                                    .Take(1)
                                                    )
                                                {
                                                    EndTmTemp = x.Tm2; // Sm.Left(index.Tm, 4);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            foreach (var index in mlAttendanceLog
                                            .Where(
                                                i =>
                                                    i.EmpCode == EmpCode &&
                                                    i.Dt == Dt &&
                                                    Sm.CompareDtTm(i.Tm, x.Tm1) > 0 &&
                                                    Sm.CompareDtTm(i.Tm, x.Tm2) <= 0
                                                )
                                            .OrderByDescending(o => o.Tm)
                                            .Take(1)
                                            )
                                            {
                                                EndTmTemp = Sm.Left(index.Tm, 4);
                                            }

                                            //if (EndTmTemp.Length <= 0)
                                            //{
                                            foreach (var index in mlAttendanceLog
                                                .Where(
                                                    i =>
                                                        i.EmpCode == EmpCode &&
                                                        i.Dt == Dt &&
                                                        Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                    )
                                                .OrderBy(o => o.Tm)
                                                .Take(1)
                                                )
                                            {
                                                EndTmTemp = Sm.Left(index.Tm, 4);
                                            }
                                            //}
                                            if (EndTmTemp.Length <= 0)
                                            {
                                                EndTmTemp = Sm.Left(x.Tm2, 4);
                                            }
                                        }
                                    }

                                    #endregion
                                }
                                #endregion
                            }
                            else
                            {
                                #region Shift 3
                                if (Sm.CompareDtTm(r.WSOut1, x.Tm1) <= 0)
                                {
                                    if (Sm.CompareDtTm(r.WSOut1, ActualTmOut) <= 0)
                                        StartTmTemp = r.WSOut1;
                                    else
                                    {
                                        foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                i.Dt == OTDtTemp &&
                                                Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                        {
                                            StartTmTemp = Sm.Left(index.Tm, 4);
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                i.Dt == OTDtTemp &&
                                                Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                    {
                                        StartTmTemp = Sm.Left(index.Tm, 4);
                                    }
                                }

                                if (StartTmTemp.Length > 0)
                                {
                                    if (x.Tm2 == "0000")
                                    {
                                        foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + "2359") <= 0 &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm1) > 0

                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                        {
                                            EndTmTemp = Sm.Left(index.Tm, 4);
                                        }

                                        if (EndTmTemp.Length <= 0)
                                        {
                                            var OTDtTemp2 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(2)).Substring(0, 8);
                                            foreach (var index in mlAttendanceLog
                                                .Where(
                                                    i =>
                                                        i.EmpCode == EmpCode &&
                                                        i.Dt == OTDtTemp2 &&
                                                        Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                    )
                                                .OrderBy(o => o.Tm)
                                                .Take(1)
                                                )
                                            {
                                                EndTmTemp = x.Tm2; // Sm.Left(index.Tm, 4);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm2) <= 0 &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm1) > 0

                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                        {
                                            EndTmTemp = Sm.Left(index.Tm, 4);
                                        }

                                        if (EndTmTemp.Length <= 0)
                                        {
                                            foreach (var index in mlAttendanceLog
                                                .Where(
                                                    i =>
                                                        i.EmpCode == EmpCode &&
                                                        i.Dt == OTDtTemp &&
                                                        Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                    )
                                                .OrderBy(o => o.Tm)
                                                .Take(1)
                                                )
                                            {
                                                EndTmTemp = Sm.Left(index.Tm, 4);
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (StartTmTemp.Length <= 0 || EndTmTemp.Length <= 0)
                            {
                                StartTm = string.Empty;
                                EndTm = string.Empty;
                            }
                            else
                            {
                                if (!IsOTSpecial)
                                {
                                    StartTm = x.Tm1;
                                    if (x.Tm2 == "0000")
                                    {
                                        if (Sm.CompareDtTm(EndTmTemp, x.Tm2) != 0)
                                            EndTm = EndTmTemp;
                                        else
                                            EndTm = x.Tm2;
                                    }
                                    else
                                    {
                                        if (Sm.CompareDtTm(EndTmTemp, x.Tm2) < 0)
                                            EndTm = EndTmTemp;
                                        else
                                            EndTm = x.Tm2;
                                    }
                                }
                            }
                        }

                        #endregion

                        if (Dt.Length > 0 && EndTm.Length > 0 && StartTm.Length > 0)
                        {
                            //Value1<Value2 : -1
                            //Value1=Value2 : 0
                            //Value1>Value2 : 1
                            if (IsOTSpecial)
                            {
                                OTHr = ComputeDuration(SpecialEndDt + SpecialEndTm, SpecialStartDt + SpecialStartTm);
                            }
                            else
                            {
                                if (EndTm == "0000")
                                {
                                    if (r.OneDayInd)
                                        OTHr = ComputeDuration(Dt2 + EndTm, OTDtTemp + StartTm);
                                    else
                                    {
                                        var Dt3 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(2)).Substring(0, 8);
                                        OTHr = ComputeDuration(Dt3 + EndTm, OTDtTemp + StartTm);
                                    }
                                }
                                else
                                    OTHr = ComputeDuration(OTDtTemp + EndTm, OTDtTemp + StartTm);
                            }
                        }
                        if (OTHr < 0) OTHr = 0;
                        TotalOTHr += OTHr;
                    }
                }
            }

            if (mSalaryInd == "2")
            {
                if (mIsOTHolidayDeduct1Hr && (r.HolInd || r.WSHolidayInd))
                    if (TotalOTHr >= 4) TotalOTHr = TotalOTHr - 1;
            }

            #endregion

            #region Set leave information (PLDay, PLHr, PLAmt)

            if (r.PaidLeaveInd && r.LeaveType.Length > 0)
            {
                if (r.LeaveType == "F")
                {
                    if (!r.WSHolidayInd && !r.HolInd)
                        r.PLDay = 1;
                }
                else
                    r.PLHr = r.LeaveDuration;

                if (
                        r.LeaveDuration != 0m &&
                        mWorkingHr != 0m &&
                        !(r.SystemType == mEmpSystemTypeBorongan && r.LeaveCode == mOperatorLeaveCode)
                    )
                {
                    //if (r.SystemType == mEmpSystemTypeBorongan)
                    if (mSalaryInd == "1")
                    {
                        if (r.SystemType == mEmpSystemTypeBorongan)
                        {
                            r.PLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                        }
                        else
                        {
                            if (r._IsUseLatestGrdLvlSalary)
                                r.PLAmt = r._LatestDailyGrdLvlSalary * (r.LeaveDuration / mWorkingHr);
                            else
                                r.PLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                        }
                    }
                    if (mSalaryInd == "2")
                        r.PLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                }
                if (r.PLAmt > 0)
                    r.ProcessPLAmt = r.PLAmt;
            }

            if (!r.PaidLeaveInd)
            {
                if (r.LeaveCode.Length == 0 && r.WorkingDuration == 0 && !r.WSHolidayInd)
                {
                    if (r._DeductionRound > 0m)
                    {
                        r.UPLHr = r._DeductionRound;
                        r.UPLAmt = (r._DeductionRound / 173m) * r.BasicSalary;
                    }
                }
                else
                {
                    if (r._LeaveDuration > 0m)
                    {
                        r.UPLHr = r._DeductionRound;
                        r.UPLAmt = (r._DeductionRound / 173m) * r.BasicSalary;
                    }
                }
                if (r.UPLAmt > 0m) r.ProcessUPLAmt = r.UPLAmt;
            }

            #endregion

            #region Menghitung OT Amount


            if (r.SystemType == mEmpSystemTypeHarian && r.BasicSalary != 0m && TotalOTHr >= 0.5m)
            {
                decimal TotalOTHrTemp = TotalOTHr;

                if (r.WSHolidayInd)
                {
                    r.OTHolidayHr = RoundTo30Minutes2(Convert.ToDouble(TotalOTHrTemp));
                    if (TotalOTHrTemp > 0)
                        r.OTHolidayAmt = RoundTo30Minutes2(Convert.ToDouble(TotalOTHrTemp)) * mOTSetHolidayAmount;
                }
                else
                {
                    r.OT1Hr = RoundTo30Minutes2(Convert.ToDouble(TotalOTHrTemp));
                    if (TotalOTHrTemp > 0)
                        r.OT1Amt = RoundTo30Minutes2(Convert.ToDouble(TotalOTHrTemp)) * mOTSetNonHolidayAmount;
                }

            }

            #endregion

            #region Compute Transport/Meal

            if ((r.LeaveCode.Length == 0 && r.WorkingDuration > 0m) ||
                (r.OTHolidayHr > 0m || r.OT1Hr > 0m || r.OT2Hr > 0m) ||
                (r.LeaveCode.Length > 0 && IsLeaveGetMealTransport(r.LeaveCode, ref lLeaveMealTransport)))
            {
                foreach (var i in mlEmployeeAllowanceMeal.Where(w => Sm.CompareStr(w.EmpCode, EmpCode)))
                {
                    if (IsWSGetMealTransport(r.WSCode, mADCodeMeal))
                    {
                        if (r.EmploymentStatus == mEmploymentStatusTetap)
                        {
                            if (r.OTHolidayHr >= 3)
                            {
                                r.Meal += i.Amt;
                            }
                            else
                            {
                                if ((r.OT1Hr + r.OT2Hr) >= 3)
                                {
                                    r.Meal += i.Amt;
                                }
                            }
                        }
                    }
                }

                foreach (var i in mlEmployeeAllowanceTransport.Where(w => Sm.CompareStr(w.EmpCode, EmpCode)))
                {
                    if (IsWSGetMealTransport(r.WSCode, mADCodeTransport))
                        if (r.EmploymentStatus == mEmploymentStatusTetap)
                        {
                            if (r.OTHolidayHr > 0)
                                r.Transport += i.Amt;
                        }
                }
            }

            #endregion

            #region Unprocessed data

            if (!r.ProcessInd)
            {
                r._LatestMonthlyGrdLvlSalary = 0m;
                r._LatestDailyGrdLvlSalary = 0m;
                r._IsFullDayInd = false;
                r.ActualIn = string.Empty;
                r.ActualOut = string.Empty;
                r.WorkingIn = string.Empty;
                r.WorkingOut = string.Empty;
                r.WorkingDuration = 0m;
                //r.EmpSalary = 0m;
                //r.EmpSalary2 = 0m;
                //r.SalaryPension = 0m;
                //r.BasicSalary = 0m;
                //if (mSalaryInd == "2") r.BasicSalary2 = 0m;
                //r.ProductionWages = 0m;
                //r.Salary = 0m;
                r.LeaveCode = string.Empty;
                r.LeaveType = string.Empty;
                r.PaidLeaveInd = false;
                r.CompulsoryLeaveInd = false;
                r.LeaveStartTm = string.Empty;
                r.LeaveEndTm = string.Empty;
                r.LeaveDuration = 0m;
                r.PLDay = 0m;
                r.PLHr = 0m;
                r.PLAmt = 0m;
                r.UPLDay = 0m;
                r.UPLHr = 0m;
                r.UPLAmt = 0m;
                r.ProcessPLAmt = 0m;
                r.OT1Hr = 0m;
                r.OT2Hr = 0m;
                r.OTHolidayHr = 0m;
                r.OT1Amt = 0m;
                r.OT2Amt = 0m;
                r.OTHolidayAmt = 0m;
                r.OTToLeaveInd = false;
                r.IncMinWages = 0m;
                r.IncProduction = 0m;
                r.IncPerformance = 0m;
                r.HolidayEarning = 0m;
                r.ExtraFooding = 0m;
                r.FieldAssignment = 0m;
                r.Meal = 0m;
                r.Transport = 0m;
                r.ServiceChargeIncentive = 0m;
                r.DedProduction = 0m;
                r.DedProdLeave = 0m;
                r.IncEmployee = 0m;
                r.ADOT = 0m;
            }
            else
            {
                #region Compute OT Allowance

                ComputeOTAllowance(ref r, Dt2, ref lEmployeeADOT, ref lPayrollProcessADOT);

                #endregion
            }

            #endregion
        }

        private bool IsWSGetMealTransport(string WSCode, string ADCode)
        {
            bool IsGet = false;

            foreach(var x in mlWorkScheduleADMealTransport
                .Where(w=>Sm.CompareStr(w.WSCode, WSCode) && Sm.CompareStr(w.ADCode, ADCode)))
                IsGet = true;

            return IsGet;
        }

        private void ComputeOTAllowance(
            ref Result1 r, 
            string Dt2, 
            ref List<EmployeeADOT> lEmployeeADOT,
            ref List<PayrollProcessADOT> lPayrollProcessADOT
            )
        {
            if (mlOTIncentive.Count == 0) return;

            string EmpCode = r.EmpCode, Dt = r.Dt;

            foreach (var i in mlOTIncentive
                .Where(
                w =>
                Sm.CompareStr(w.EmpCode, EmpCode) &&
                Sm.CompareStr(w.OTIncentiveDt, Dt)
                )
                .Select(s => new { s.ADCode }).Distinct())
            {
                ComputeOTAllowance2(
                   i.ADCode,
                   ref r,
                   Dt2,
                   ref lEmployeeADOT,
                   ref lPayrollProcessADOT
                   );
            }
        }

        private void ComputeOTAllowance2(
            string ADCode, 
            ref Result1 r,
            string Dt2,
            ref List<EmployeeADOT> lEmployeeADOT,
            ref List<PayrollProcessADOT> lPayrollProcessADOT
           )
        {
            string EmpCode = r.EmpCode, Dt = r.Dt, StartTm = string.Empty, EndTm = string.Empty;
            decimal Value = 0m, ADOT = 0m;
            bool IsExisted = false;

            foreach (var x in mlOTIncentive.Where(
                w =>
                Sm.CompareStr(w.EmpCode, EmpCode) &&
                Sm.CompareStr(w.OTIncentiveDt, Dt) &&
                Sm.CompareStr(w.ADCode, ADCode)
                ))
            {
                IsExisted = true;
                StartTm = x.OTIncentiveStartTm;
                EndTm = x.OTIncentiveEndTm;

                if (Sm.CompareDtTm(string.Concat(Dt, StartTm), string.Concat(Dt, EndTm)) <= 0)
                {
                    //one day
                    Value += ComputeDuration(string.Concat(Dt, EndTm), string.Concat(Dt, StartTm));
                }
                else
                {
                    //2 day
                    if (Dt2.Length > 0) Value += ComputeDuration(string.Concat(Dt2, EndTm), string.Concat(Dt, StartTm));
                }
            }

            if (!IsExisted) return;

            IsExisted = false;
            if (Value > mADOTMinHour)
            {
                //if (Value > mADOTHour) Value = mADOTHour;

                bool IsWSHoliday = r.HolInd || r.WSHolidayInd;

                foreach (var x in lEmployeeADOT
                    .Where(x =>
                        Sm.CompareStr(x.EmpCode, EmpCode) &&
                        Sm.CompareStr(x.ADCode, ADCode) &&
                        x.HolidayInd == IsWSHoliday))
                {
                    IsExisted = false;

                    if (mADOTHour != 0)
                        ADOT = Math.Round((Value / mADOTHour) * x.Amt, 2);
                    r.ADOT += ADOT;

                    foreach (var i in lPayrollProcessADOT
                        .Where(i =>
                            Sm.CompareStr(i.EmpCode, EmpCode) &&
                            Sm.CompareStr(i.ADCode, ADCode) &&
                            Sm.CompareStr(i.Dt, Dt)
                            ))
                    {
                        i.Amt += ADOT;
                        i.Duration += Value;
                        IsExisted = true;
                        break;
                    }

                    if (!IsExisted)
                    {
                        lPayrollProcessADOT.Add(new PayrollProcessADOT()
                        {
                            PayrunCode = r.PayrunCode,
                            EmpCode = EmpCode,
                            ADCode = ADCode,
                            Dt = Dt,
                            Amt = ADOT,
                            Duration = Value
                        });
                    }
                }
            }
        }

        private bool IsLeaveGetMealTransport(string LeaveCode, ref List<LeaveMealTransport> l)
        {
            if (LeaveCode.Length == 0 || l.Count==0) return true;

            bool IsExisted = false;
            for (int i = 0; i < l.Count; i++)
            { 
                if (Sm.CompareStr(LeaveCode, l[i].LeaveCode))
                    return true;
            }
            return IsExisted;
        }

        private void Process5(ref List<Result2> r2)
        {
            string
                PayrunCode = Sm.GetLue(LuePayrunCode),
                Filter = string.Empty,
                EmpCode = string.Empty,
                PayrunPeriode = string.Empty;

            if(mPayrunCodeFormatType == "1")
                PayrunPeriode = Sm.GetDte(DteStartDt).Substring(0, 6);
            else
                PayrunPeriode = Sm.GetDte(DteEndDt).Substring(0, 6);

            var cm = new MySqlCommand();

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length>0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length>0)
                Filter = " (" + Filter + ") ";
            else
                Filter = " (0=1)";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.JoinDt, A.ResignDt, A.NPWP, A.PTKP, A.EmploymentStatus, ");
            SQL.AppendLine("IfNull(B.SalaryAdjustment, 0) As SalaryAdjustment, ");
            SQL.AppendLine("IfNull(C.SSEmployerHealth, 0) As SSEmployerHealth, ");
            SQL.AppendLine("IfNull(C.SSEmployeeHealth, 0) As SSEmployeeHealth, ");
            SQL.AppendLine("IfNull(D.SSEmployerEmployment, 0) As SSEmployerEmployment, ");
            SQL.AppendLine("IfNull(D.SSEmployeeEmployment, 0) As SSEmployeeEmployment, ");
            SQL.AppendLine("IfNull(E.SSEmployerPension, 0.00) As SSEmployerPension, ");
            SQL.AppendLine("IfNull(E.SSEmployeePension, 0.00) As SSEmployeePension, ");
            SQL.AppendLine("IfNull(F.SSErLifeInsurance, 0.00) As SSErLifeInsurance, ");
            SQL.AppendLine("IfNull(F.SSEeLifeInsurance, 0.00) As SSEeLifeInsurance, ");
            SQL.AppendLine("IfNull(G.SSErWorkingAccident, 0.00) As SSErWorkingAccident, ");
            SQL.AppendLine("IfNull(G.SSEeWorkingAccident, 0.00) As SSEeWorkingAccident, ");
            SQL.AppendLine("IfNull(H.SSErRetirement, 0.00) As SSErRetirement, ");
            SQL.AppendLine("IfNull(H.SSEeRetirement, 0.00) As SSEeRetirement, ");
            SQL.AppendLine("IfNull(I.SSErPension, 0.00) As SSErPension, ");
            SQL.AppendLine("IfNull(I.SSEePension, 0.00) As SSEePension, ");
            SQL.AppendLine("IfNull(J.Amt, 0.00) As Functional, ");
            SQL.AppendLine("IfNull(K.Receive, 0.00) As PerformanceValue, ");
            SQL.AppendLine("IfNull(L.Amt, 0.00) As FixAllowance1, ");
            SQL.AppendLine("IfNull(M.Amt, 0.00) As FixDeduction, ");
            SQL.AppendLine("IfNull(O.TaxableFixedAllowance, 0.00) As TaxableFixedAllowance1, ");
            SQL.AppendLine("IfNull(P.Amt, 0.00) As FixAllowance2, ");
            SQL.AppendLine("IfNull(Q.TaxableFixedAllowance, 0.00) As TaxableFixedAllowance2, ");
            SQL.AppendLine("IfNull(R.ADTransport, 0.00) As ADTransport, ");
            SQL.AppendLine("IfNull(S.IKP, 0.00) As IKP ");
            SQL.AppendLine("From TblEmployee A ");

            //Salary Adjustment
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.EmpCode, Sum(T.Amt) As SalaryAdjustment ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T1.EmpCode, T1.Amt ");
            SQL.AppendLine("        From TblSalaryAdjustmentHdr T1 ");
            SQL.AppendLine("        Where T1.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("        And (T1.PayrunCode Is Null Or (T1.PayrunCode Is Not Null And T1.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T1.") + ") ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T2.EmpCode, T1.Amt ");
            SQL.AppendLine("        From TblSalaryAdjustment2Hdr T1 ");
            SQL.AppendLine("        Inner Join TblSalaryAdjustment2Dtl T2 ");
            SQL.AppendLine("            On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("            And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("            And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("        Where T1.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("    ) T Group By T.EmpCode ");
            SQL.AppendLine(") B On A.EmpCode=B.EmpCode ");

            //SS Health
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerHealth, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeHealth ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.SSPCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSPCodeForHealth' And ParValue Is Not Null ");
            SQL.AppendLine("            ) ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") C On A.EmpCode=C.EmpCode ");

            //SS Employment (dikurangi BPJS Pensiun)
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerEmployment, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeEmployment ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode))");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("        And T2.SSCode Not In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSForRetiring' And ParValue Is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.SSPCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSPCodeForEmployment' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") D On A.EmpCode=D.EmpCode ");

            //Pension 1 (Pensiun JiwaSraya)
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.EmployerAmt) As SSEmployerPension, Sum(T2.EmployeeAmt) As SSEmployeePension ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSCodePension' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") E On A.EmpCode=E.EmpCode ");


            //BPJS Jaminan Kematian
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErLifeInsurance, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeLifeInsurance ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSLifeInsurance' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");

            //BPJS Jaminan Kecelakaan Kerja
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErWorkingAccident, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeWorkingAccident ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSWorkingAccident' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") G On A.EmpCode=G.EmpCode ");

            //BPJS Jaminan Hari Tua
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErRetirement, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeRetirement ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSOldAgeInsurance' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") H On A.EmpCode=H.EmpCode ");

            //BPJS Jaminan Pensiun
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErPension, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEePension ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSForRetiring' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") I On A.EmpCode=I.EmpCode ");

            //Functional
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblParameter B On A.ADCode=B.ParValue And B.ParCode='ADCodeFunctional' And B.ParValue is Not Null ");
            SQL.AppendLine("    Where (" + Filter + ") ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(") J On A.EmpCode=J.EmpCode ");

            //Employee Performance Allwoance
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select B.EmpCode, B.Receive  ");
            SQL.AppendLine("    from tblempperformanceallowancehdr A ");
            SQL.AppendLine("    Inner Join tblempperformanceallowanceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Where A.cancelind = 'N' And Concat(A.Yr, A.Mth) = Left(@EndDt, 6) ");
            SQL.AppendLine(")K On A.EmpCode=K.EmpCode ");

            //Fixed Allowance (Not Prorated) default ambil dari grade lalu salary allowance deduction
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.EmpCode, SUM(Amt) As Amt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.EmpCode, A.AdCode, A.Amt ");
            SQL.AppendLine("        From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("        Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' And B.AmtType='1' ");
            if (mProratedADBasedOnLeave.Length > 0)
                SQL.AppendLine("    And B.ADCode Not In (" + mProratedAD + ")");
            SQL.AppendLine("        Where ( ");
            SQL.AppendLine("        (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("        (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("        (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("        (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And (" + Filter + ") ");
            SQL.AppendLine("        union all ");
            SQL.AppendLine("        Select A.EmpCode, B.AdCode, B.Amt ");
            SQL.AppendLine("        From TblEmployee A ");
            SQL.AppendLine("        Inner Join TblGradeLevelDtl B On A.GrdLvlCode=B.GrdLvlCode ");
            SQL.AppendLine("        Inner Join TblAllowanceDeduction C On B.ADCode=C.ADCode And C.ADType='A' And C.AmtType='1' ");
            SQL.AppendLine("        Where 0=0 And (" + Filter + ") ");
            SQL.AppendLine("    )T1 ");
            SQL.AppendLine("    Group by T1.EmpCode ");
            SQL.AppendLine(") L On A.EmpCode=L.EmpCode ");

            //Fixed Deduction
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.EmpCode, SUM(Amt) As Amt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.EmpCode, A.AdCode, A.Amt ");
            SQL.AppendLine("        From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("        Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='D' And B.AmtType='1' ");
            SQL.AppendLine("        Where ( ");
            SQL.AppendLine("        (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("        (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("        (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("        (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And (" + Filter + ") ");
            SQL.AppendLine("        union all ");
            SQL.AppendLine("        Select A.EmpCode, B.AdCode, B.Amt ");
            SQL.AppendLine("        From TblEmployee A ");
            SQL.AppendLine("        Inner Join TblGradeLevelDtl B On A.GrdLvlCode=B.GrdLvlCode ");
            SQL.AppendLine("        Inner Join TblAllowanceDeduction C On B.ADCode=C.ADCode And C.ADType='D' And C.AmtType='1' ");
            SQL.AppendLine("        Where 0=0 And (" + Filter + ") ");
            SQL.AppendLine("    )T1 ");
            SQL.AppendLine("    Group BY T1.EmpCode ");
            SQL.AppendLine(") M On A.EmpCode=M.EmpCode ");

            //Taxable Fixed Allowance (Not Prorated)
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As TaxableFixedAllowance ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' And B.TaxInd='Y' And B.AmtType='1' ");
            if (mProratedADBasedOnLeave.Length > 0)
                SQL.AppendLine("    And B.ADCode Not In (" + mProratedAD + ")");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") O On A.EmpCode=O.EmpCode ");

            //Fixed Allowance (Prorated)
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' And B.AmtType='1' ");
            if (mProratedADBasedOnLeave.Length > 0)
                SQL.AppendLine("    And B.ADCode In (" + mProratedAD + ") ");
            else
                SQL.AppendLine("    And 0=1 ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") P On A.EmpCode=P.EmpCode ");

            //Taxable Fixed Allowance (Prorated)
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As TaxableFixedAllowance ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' And B.TaxInd='Y' And B.AmtType='1' ");
            if (mProratedADBasedOnLeave.Length > 0)
                SQL.AppendLine("    And B.ADCode In (" + mProratedAD + ")");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") Q On A.EmpCode=Q.EmpCode ");

            //Tunjangan Transport
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As ADTransport ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADCode='112' ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") R On A.EmpCode=R.EmpCode ");

            //Tunjangan employee performance 
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, A.IKP  ");
            SQL.AppendLine("    From TblEmpPerformanceAllowance A  ");
            SQL.AppendLine("    where Concat(Yr, Mth) = @PayrunPeriod  ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine(") S On A.EmpCode=S.EmpCode ");

            SQL.AppendLine("Where " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);
                Sm.CmParam<String>(ref cm, "@PayrunPeriod", PayrunPeriode);
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
                Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
                Sm.CmParam<String>(ref cm, "@SSYr", mSSYr);
                Sm.CmParam<String>(ref cm, "@SSMth", mSSMth);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "EmpCode", 

                    //1-5
                    "JoinDt", 
                    "ResignDt", 
                    "NPWP", 
                    "PTKP", 
                    "SalaryAdjustment", 

                    //6-10
                    "SSEmployerHealth", 
                    "SSEmployeeHealth", 
                    "SSEmployerEmployment", 
                    "SSEmployeeEmployment",
                    "SSEmployerPension",
                    
                    //11-15
                    "SSEmployeePension",
                    "SSErLifeInsurance",
                    "SSEeLifeInsurance",
                    "SSErWorkingAccident",
                    "SSEeWorkingAccident",
                    
                    //16-20
                    "SSErRetirement",
                    "SSEeRetirement",
                    "SSErPension",
                    "SSEePension",
                    "Functional",
                    
                    //21-25
                    "PerformanceValue",
                    "FixAllowance1",
                    "FixDeduction",
                    "TaxableFixedAllowance1",
                    "FixAllowance2",

                    //26-29
                    "TaxableFixedAllowance2",
                    "ADTransport",
                    "IKP",
                    "EmploymentStatus"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        r2.Add(new Result2()
                        {
                            PayrunCode = PayrunCode,
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            ResignDt = Sm.DrStr(dr, c[2]),
                            NPWP = Sm.DrStr(dr, c[3]),
                            PTKP = Sm.DrStr(dr, c[4]),
                            SalaryAdjustment = Sm.DrDec(dr, c[5]),
                            SSEmployerHealth = Sm.DrDec(dr, c[6]),
                            SSEmployeeHealth = Sm.DrDec(dr, c[7]),
                            SSEmployerEmployment = Sm.DrDec(dr, c[8]),
                            SSEmployeeEmployment = Sm.DrDec(dr, c[9]),
                            SSEmployerPension = Sm.DrDec(dr, c[10]),
                            SSEmployeePension = Sm.DrDec(dr, c[11]),
                            SSErLifeInsurance= Sm.DrDec(dr, c[12]),
                            SSEeLifeInsurance= Sm.DrDec(dr, c[13]),
                            SSErWorkingAccident= Sm.DrDec(dr, c[14]),
                            SSEeWorkingAccident= Sm.DrDec(dr, c[15]),
                            SSErRetirement= Sm.DrDec(dr, c[16]),
                            SSEeRetirement = Sm.DrDec(dr, c[17]),
                            SSErPension = Sm.DrDec(dr, c[18]),
                            SSEePension = Sm.DrDec(dr, c[19]),
                            Functional = Sm.DrDec(dr, c[20]),
                            PerformanceValue = Sm.DrDec(dr, c[21]),
                            FixAllowance1 = Sm.DrDec(dr, c[22]),
                            FixDeduction = Sm.DrDec(dr, c[23]),
                            TaxableFixAllowance1 = Sm.DrDec(dr, c[24]),
                            FixAllowance2 = Sm.DrDec(dr, c[25]),
                            TaxableFixAllowance2 = Sm.DrDec(dr, c[26]),
                            _ADTransport = Sm.DrDec(dr, c[27]),
                            IKP = Sm.DrDec(dr, c[28]),
                            EmploymentStatus = Sm.DrStr(dr, c[29]),
                            IncPerformance = 0m,
                            DedEmployee = 0m,
                            FixAllowance = 0m,
                            NonTaxableFixAllowance = 0m,
                            NonTaxableFixDeduction = 0m,
                            TaxableFixAllowance = 0m,
                            TaxableFixDeduction = 0m,
                            EmploymentPeriodAllowance = 0m,
                            SSEmployerPension2 = 0m,
                            SSEmployeePension2 = 0m,
                            Housing = 0m,
                            MobileCredit = 0m,
                            Zakat = 0m,
                            PerformanceStatus = string.Empty,
                            GradePerformance = string.Empty,
                            DedProduction = 0m,
                            IncProduction = 0m,
                            SSEmployerNonRetiring = 0m,
                            SSEmployeeOldAgeInsurance = 0m,
                            Salary = 0m,
                            WorkingDay = 0m,
                            PLDay = 0m,
                            PLHr = 0m,
                            PLAmt = 0m,
                            UPLDay = 0m,
                            UPLHr = 0m,
                            UPLAmt = 0m,
                            OT1Hr = 0m,
                            OT2Hr = 0m,
                            OTHolidayHr = 0m,
                            OT1Amt = 0m,
                            OT2Amt = 0m,
                            OTHolidayAmt = 0m,
                            IncMinWages = 0m,
                            PresenceReward = 0m,
                            HolidayEarning = 0m,
                            ExtraFooding = 0m,
                            FieldAssignment = 0m,
                            DedProdLeave = 0m,
                            IncEmployee = 0m,
                            ServiceChargeIncentive = 0m,
                            SalaryPension = 0m,
                            CreditCode1 = string.Empty,
                            CreditCode2 = string.Empty,
                            CreditCode3 = string.Empty,
                            CreditCode4 = string.Empty,
                            CreditCode5 = string.Empty,
                            CreditCode6 = string.Empty,
                            CreditCode7 = string.Empty,
                            CreditCode8 = string.Empty,
                            CreditCode9 = string.Empty,
                            CreditCode10 = string.Empty,
                            CreditAdvancePayment1 = 0m,
                            CreditAdvancePayment2 = 0m,
                            CreditAdvancePayment3 = 0m,
                            CreditAdvancePayment4 = 0m,
                            CreditAdvancePayment5 = 0m,
                            CreditAdvancePayment6 = 0m,
                            CreditAdvancePayment7 = 0m,
                            CreditAdvancePayment8 = 0m,
                            CreditAdvancePayment9 = 0m,
                            CreditAdvancePayment10 = 0m,
                            ADOT = 0m,
                            Tax = 0m,
                            TaxAllowance = 0m,
                            Amt = 0m,
                            _RecomputedValueForFixedAD = 1m,
                            _RecomputedValueForProratedAD = 1m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process6(ref Result2 r, ref List<Result1> lResult1, ref List<NTI> lNTI, ref List<TI2> lTI2)
        {
            string
                EmpCode = r.EmpCode,
                EmploymentStatus = string.Empty;
            decimal 
                LatestSalary = 0m,
                LatestSalary2 = 0m,
                LatestSalaryPension = 0m;

            foreach (var x in lResult1.Where(x => x.EmpCode == EmpCode && x.ProcessInd).OrderBy(Index=>Index.Dt))
            {
                EmploymentStatus = x.EmploymentStatus;
                r.EmploymentStatus = EmploymentStatus;
                LatestSalary = x.BasicSalary; //x.EmpSalary;
                LatestSalary2 = x.BasicSalary2;//x.EmpSalary2;
                LatestSalaryPension = x.SalaryPension;
                r.Salary = LatestSalary;
                if (x.ProcessInd)
                {
                    //LatestSalary = x.EmpSalary;
                    //LatestSalary2 = x.EmpSalary2;
                    //LatestSalaryPension = x.SalaryPension;
                    //r.Salary += x.Salary;
                    r.PLDay += x.PLDay;
                    r.PLHr += x.PLHr;
                    r.PLAmt += x.PLAmt;
                    r.UPLDay += x.UPLDay;
                    r.UPLHr += x.UPLHr;
                    r.UPLAmt += x.UPLAmt;
                    r.ProcessPLAmt += x.ProcessPLAmt;
                    r.ProcessUPLAmt += x.ProcessUPLAmt;
                    r.OT1Hr += x.OT1Hr;
                    r.OT2Hr += x.OT2Hr;
                    r.OTHolidayHr += x.OTHolidayHr;
                    r.OT1Amt += x.OT1Amt;
                    r.OT2Amt += x.OT2Amt;
                    r.OTHolidayAmt += x.OTHolidayAmt;
                    r.IncPerformance += x.IncPerformance;
                    r.FieldAssignment += x.FieldAssignment;
                    r.Transport += x.Transport;
                    r.Meal += x.Meal;
                    r.ADOT += x.ADOT;
                    if (x.WorkingDuration > 0) r.WorkingDay += 1;
                }
            }

            //r.ProcessUPLAmt = (1 / mHOWorkingDayPerMth) * r.UPLDay * r.Salary; //hitungan jam hilang

            //untuk menghitung salary dan fixed allowance apabila pada 1 period terdiri dari lbh dari 1 payrun

            //kalau resign.
            //kalau payrun info berbeda dalam 1 periode.
            //kalau sudah pernah dibayar.

        
            Boolean IsPayrunDifferent = false, IsLatestPaidDtExisted = false;
            decimal
                AbsentDayForJoin = 0m,
                AbsentDayForResign = 0m,
                RecomputedProcessedWorkingDay = 0m,
                RecomputedProratedDayBasedOnLeave = 0m,
                RecomputedAllWorkingDay = 0m,
                LeaveDaysForProratedAD = 0m;
            string LeaveCode = string.Empty;

            foreach (var x in lResult1
                .Where(x => Sm.CompareStr(x.EmpCode, EmpCode))
                .OrderBy(Index => Index.Dt))
            {
                if (Sm.CompareDtTm(x.Dt, x.JoinDt) <= 0)
                    AbsentDayForJoin += 1;

                if (x.ResignDt.Length > 0 && Sm.CompareDtTm(x.ResignDt, x.Dt) <= 0)
                    AbsentDayForResign += 1;
                else
                {
                    if (x.LatestPaidDt.Length > 0 && Sm.CompareDtTm(x.LatestPaidDt, x.Dt) == 0)
                        IsLatestPaidDtExisted = true;

                    if (!IsPayrunDifferent &&
                        !(
                        Sm.CompareStr(x.DeptCode, mDeptCode) &&
                        Sm.CompareStr(x.SystemType, mSystemType) &&
                        Sm.CompareStr(x.PayrunPeriod, mPayrunPeriod) &&
                        Sm.CompareStr(x.PGCode, mPGCode) 
                        //&& Sm.CompareStr(x.SiteCode, mSiteCode)
                        ))

                        IsPayrunDifferent = true;
                }

                if (!(x.HolInd || x.WSHolidayInd))
                {
                    if (x.ProcessInd)
                    {
                        RecomputedProcessedWorkingDay += 1;
                        if (x.LeaveCode.Length > 0 && mLeaveForProratedAD.Length>0)
                        {
                            foreach(var l in mlLeaveForProratedAD)
                            {
                                if (Sm.CompareStr(l.ToString(), x.LeaveCode))
                                {
                                    if (Sm.CompareStr(x.LeaveCode, LeaveCode))
                                        RecomputedProratedDayBasedOnLeave += 1;
                                    else
                                        RecomputedProratedDayBasedOnLeave = 1;
                                    break;
                                }
                                //if (Sm.CompareStr(l.ToString(), x.LeaveCode) &&
                                //    RecomputedProratedDayBasedOnLeave <= mMinLeaveDaysForProratedAD)
                                //{
                                //    if (Sm.CompareStr(x.LeaveCode, LeaveCode))
                                //        RecomputedProratedDayBasedOnLeave += 1;
                                //    else
                                //        RecomputedProratedDayBasedOnLeave = 1;
                                //    break;
                                //}
                            }
                        }
                        LeaveCode = x.LeaveCode;
                    }
                    //if (!(x.ResignDt.Length>0 && Sm.CompareDtTm(x.ResignDt, x.Dt)<=0))
                    RecomputedAllWorkingDay += 1;
                }
            }

            r.Salary = LatestSalary;

            if (RecomputedProratedDayBasedOnLeave < mMinLeaveDaysForProratedAD)
                RecomputedProratedDayBasedOnLeave = 0m;
            
            if (AbsentDayForJoin > 0 || AbsentDayForResign > 0 || IsLatestPaidDtExisted || IsPayrunDifferent || RecomputedProcessedWorkingDay < RecomputedAllWorkingDay)
            {
                r._RecomputedValueForFixedAD = RecomputedProcessedWorkingDay / RecomputedAllWorkingDay;
                r.Salary = RecomputedProcessedWorkingDay * LatestSalary2; //r._RecomputedValueForFixedAD;

                if (RecomputedProratedDayBasedOnLeave > 0)
                {
                    if (RecomputedAllWorkingDay > (RecomputedProratedDayBasedOnLeave-RecomputedProratedDayBasedOnLeave))
                    {
                        r._RecomputedValueForProratedAD = ((RecomputedProratedDayBasedOnLeave - RecomputedProratedDayBasedOnLeave) / RecomputedAllWorkingDay);
                        r.FixAllowance2 *= r._RecomputedValueForProratedAD;
                        r.TaxableFixAllowance2 *= r._RecomputedValueForProratedAD;
                    }
                    else
                    {
                        r._RecomputedValueForProratedAD = 0m;
                        r.FixAllowance2 = 0m;
                        r.TaxableFixAllowance2 = 0m;
                    }
                }

                r.FixAllowance1 *= r._RecomputedValueForFixedAD;
                r.TaxableFixAllowance1 *= r._RecomputedValueForFixedAD;
                r.NonTaxableFixAllowance *= r._RecomputedValueForFixedAD;

                r.FixDeduction *= r._RecomputedValueForFixedAD;
                r.TaxableFixDeduction *= r._RecomputedValueForFixedAD;
                r.NonTaxableFixDeduction *= r._RecomputedValueForFixedAD;
            }
            else
            {
                if (RecomputedProratedDayBasedOnLeave > 0)
                {
                    if (RecomputedAllWorkingDay > RecomputedProratedDayBasedOnLeave)
                    {
                        r._RecomputedValueForProratedAD = ((RecomputedAllWorkingDay - RecomputedProratedDayBasedOnLeave) / RecomputedAllWorkingDay);
                        r.FixAllowance2 *= r._RecomputedValueForProratedAD;
                        r.TaxableFixAllowance2 *= r._RecomputedValueForProratedAD;
                    }
                    else
                    {
                        r._RecomputedValueForProratedAD = 0m;
                        r.FixAllowance2 = 0m;
                        r.TaxableFixAllowance2 = 0m;
                    }
                }
            }


            r.FixAllowance = r.FixAllowance1 + r.FixAllowance2;
            r.TaxableFixAllowance = r.TaxableFixAllowance1 + r.TaxableFixAllowance2;


            r.SalaryPension = LatestSalaryPension;
            if (r.SalaryPension < 0m) r.SalaryPension = 0m;
            if (!IsPayrunDifferent) r.Salary = LatestSalary;
            if (r.Salary < 0m) r.Salary = 0m;
            if (r.TaxableFixAllowance < 0) r.TaxableFixAllowance = 0;
            if (r.NonTaxableFixAllowance < 0) r.NonTaxableFixAllowance = 0;
            if (r.FixAllowance < 0) r.FixAllowance = 0;

            decimal EmpAdvancePayment = 0m;
            int CreditAdvancePaymentNo = 0;
            string CreditCode = string.Empty;
            foreach (var x in mlAdvancePaymentProcess.Where(x => Sm.CompareStr(x.EmpCode, EmpCode)).OrderBy(o => o.CreditCode))
            {
                if (x.CreditCode.Length > 0)
                {
                    if (Sm.CompareStr(x.CreditCode, CreditCode))
                    {
                        if (CreditAdvancePaymentNo == 1)
                            r.CreditAdvancePayment1 += x.Amt;

                        if (CreditAdvancePaymentNo == 2)
                            r.CreditAdvancePayment2 += x.Amt;

                        if (CreditAdvancePaymentNo == 3)
                            r.CreditAdvancePayment3 += x.Amt;

                        if (CreditAdvancePaymentNo == 4)
                            r.CreditAdvancePayment4 += x.Amt;

                        if (CreditAdvancePaymentNo == 5)
                            r.CreditAdvancePayment5 += x.Amt;

                        if (CreditAdvancePaymentNo == 6)
                            r.CreditAdvancePayment6 += x.Amt;

                        if (CreditAdvancePaymentNo == 7)
                            r.CreditAdvancePayment7 += x.Amt;

                        if (CreditAdvancePaymentNo == 8)
                            r.CreditAdvancePayment8 += x.Amt;

                        if (CreditAdvancePaymentNo == 9)
                            r.CreditAdvancePayment9 += x.Amt;

                        if (CreditAdvancePaymentNo == 10)
                            r.CreditAdvancePayment10 += x.Amt;
                    }
                    else
                    {
                        CreditAdvancePaymentNo += 1;

                        if (CreditAdvancePaymentNo == 1)
                        {
                            r.CreditCode1 = x.CreditCode;
                            r.CreditAdvancePayment1 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 2)
                        {
                            r.CreditCode2 = x.CreditCode;
                            r.CreditAdvancePayment2 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 3)
                        {
                            r.CreditCode3 = x.CreditCode;
                            r.CreditAdvancePayment3 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 4)
                        {
                            r.CreditCode4 = x.CreditCode;
                            r.CreditAdvancePayment4 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 5)
                        {
                            r.CreditCode5 = x.CreditCode;
                            r.CreditAdvancePayment5 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 6)
                        {
                            r.CreditCode6 = x.CreditCode;
                            r.CreditAdvancePayment6 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 7)
                        {
                            r.CreditCode7 = x.CreditCode;
                            r.CreditAdvancePayment7 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 8)
                        {
                            r.CreditCode8 = x.CreditCode;
                            r.CreditAdvancePayment8 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 9)
                        {
                            r.CreditCode9 = x.CreditCode;
                            r.CreditAdvancePayment9 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 10)
                        {
                            r.CreditCode10 = x.CreditCode;
                            r.CreditAdvancePayment10 += x.Amt;
                        }
                    }
                    CreditCode = x.CreditCode;
                }
                EmpAdvancePayment += x.Amt;
            }

            r.EmpAdvancePayment = EmpAdvancePayment;
            
        }

        private void ProcessTax(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI, ref List<TI2> lTI2, decimal Salary, decimal penambahTax, decimal pengurangTax)
        {
            r.Amt = Salary;
            //if (r.NPWP.Length == 0 || r.PTKP.Length == 0) return;

            if (r.PTKP.Length > 0 && r.Amt > 0)
                ComputeTax(ref r, ref lNTI, ref lTI, ref lTI2, penambahTax, pengurangTax);
        }



        private void ComputeTax(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI, ref List<TI2> lTI2, decimal penambahTax, decimal PengurangTax)
        {
            decimal
                    Value = 0m,
                    NTIAmt = 0m,
                    FunctionalExpenses = 0m,
                    Amt = 0m,
                    JoinYr = 0m;

            string NTI = r.PTKP;

            bool IsFullYr = true;

            var Penambah = penambahTax;
            var Pengurang = PengurangTax;

            Amt = Penambah;

            FunctionalExpenses = Amt * mFunctionalExpenses * 0.01m;
            if (FunctionalExpenses > mFunctionalExpensesMaxAmt)
                FunctionalExpenses = mFunctionalExpensesMaxAmt;

            Value = Amt - FunctionalExpenses - Pengurang;

            if (r.JoinDt.Length >= 4)
                JoinYr = Decimal.Parse(Sm.Left(r.JoinDt, 4));

            if (JoinYr < Decimal.Parse(Sm.Left(r.PayrunCode, 4)))
                Value = Value * 12m;
            else
            {
                if (Sm.CompareStr(Sm.Left(r.JoinDt, 4), Sm.Left(r.PayrunCode, 4)))
                {
                    Value = Value * (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1);
                    IsFullYr = false;
                }
            }

            foreach (var x in lNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (Value > NTIAmt)
                Value -= NTIAmt;
            else
                Value = 0m;

            //if (Value > 0 && lTI.Count > 0)
            //{
            //    foreach (TI i in lTI.OrderBy(x => x.SeqNo))
            //    {
            //        if (Value >= i.Amt1 && Value <= i.Amt2)
            //        {
            //            Value = ((Value - (i.Amt1 - 1m)) * (i.TaxRate / 100));
            //            break;
            //        }
            //    }
            //}

            if (Value > 0 && lTI2.Count > 0)
            {
                foreach (TI2 i in lTI2.OrderBy(x => x.SeqNo))
                {
                    if (Value >= i.Amt1 && Value <= i.Amt2)
                    {
                        Value = ((Value - (i.Amt1 - 1m)) * (i.Value1 / i.Value2)) + i.Value3;
                        break;
                    }
                }
            }

            Value = Value / (IsFullYr ? 12m : (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1));
            if (Value <= 0) Value = 0m;
            if (r.NPWP.Length > 0)
            {
                r.TaxAllowance = Math.Round(Value, 0);
                r.Tax = r.TaxAllowance;
            }
            else
            {
                r.TaxAllowance = Math.Round(Value, 0)* 1.2m;
                r.Tax = r.TaxAllowance;
            }
        }
       
        #endregion

        #region Get Tables Data

        private void ProcessAdvancePaymentProcess(ref List<AdvancePaymentProcess> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T1.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Select T1.DocNo, T1.EmpCode, T1.CreditCode, T2.Amt ");
            SQL.AppendLine("From TblAdvancePaymentHdr T1 ");
            SQL.AppendLine("Inner Join TblAdvancePaymentDtl T2 ");
            SQL.AppendLine("    On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T2.Yr=@Yr ");
            SQL.AppendLine("    And T2.Mth=@Mth ");
            //SQL.AppendLine("    And Not Exists( ");
            //SQL.AppendLine("        Select 1 From TblAdvancePaymentProcess ");
            //SQL.AppendLine("        Where DocNo=T2.DocNo ");
            //SQL.AppendLine("        And PayrunCode<>@PayrunCode ");
            //SQL.AppendLine("        And Yr=T2.Yr ");
            //SQL.AppendLine("        And Mth=T2.Mth ");
            //SQL.AppendLine("    ) ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    Exists( ");
            SQL.AppendLine("        Select 1 From TblAdvancePaymentProcess ");
            SQL.AppendLine("        Where DocNo=T2.DocNo ");
            SQL.AppendLine("        And PayrunCode=@PayrunCode ");
            SQL.AppendLine("        And Yr=@Yr ");
            SQL.AppendLine("        And Mth=@Mth ");
            SQL.AppendLine(Filter.Replace("T1.", string.Empty));
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Or Not Exists( ");
            SQL.AppendLine("        Select 1 From TblAdvancePaymentProcess ");
            SQL.AppendLine("        Where DocNo=T2.DocNo ");
            SQL.AppendLine("        And Yr=@Yr ");
            SQL.AppendLine("        And Mth=@Mth ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A' " + Filter);;
            SQL.AppendLine(";");

            string Year = Sm.Left(Sm.GetDte(DteEndDt), 4), Month = Sm.GetDte(DteEndDt).Substring(4, 2);

            Sm.CmParam<String>(ref cm, "@Yr", Year);
            Sm.CmParam<String>(ref cm, "@Mth", Month);
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "EmpCode", "CreditCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AdvancePaymentProcess()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            CreditCode = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            Yr = Year,
                            Mth = Month
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOT(ref List<OT> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, OTDt = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T2.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            Filter = " And (" + Filter + ")";

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            SQL.AppendLine("Select T1.DocNo, T1.OTDt, T2.EmpCode, T2.OTStartTm, T2.OTEndTm ");
            SQL.AppendLine("From TblOTRequestHdr T1 ");
            SQL.AppendLine("Inner Join TblOTRequestDtl T2 On T1.DocNo=T2.DocNo " + Filter);
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A' ");
            //SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.OTDt Between @StartDt And @EndDt;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "OTDt", "EmpCode", "OTStartTm", "OTEndTm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OT()
                        {
                            OTDocNo = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            Tm1 = Sm.DrStr(dr, c[3]),
                            Tm2 = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOTAdjustment(ref List<OTAdjustment> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, OTDt = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T3.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            Filter = " And (" + Filter + ")";

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            SQL.AppendLine("Select T1.OTRequestDocNo, T2.OTDt, T3.EmpCode, T1.StartTm, T1.EndTm ");
            SQL.AppendLine("From TblOTAdjustment T1 ");
            SQL.AppendLine("Inner Join TblOTRequestHdr T2 On T1.OTRequestDocNo=T2.DocNo And T2.OTDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblOTRequestDtl T3 On T1.OTRequestDocNo=T3.DocNo And T1.OTRequestDNo=T3.DNo " + Filter);
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A';");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OTRequestDocNo", "OTDt", "EmpCode", "StartTm", "EndTm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OTAdjustment()
                        {
                            OTDocNo = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            Tm1 = Sm.DrStr(dr, c[3]),
                            Tm2 = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessNTI(ref List<NTI> l)
        {
            string Payrun = Sm.GetLue(LuePayrunCode);
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Status, B.Amt ");
            SQL.AppendLine("From TblNTIHdr A ");
            SQL.AppendLine("Inner Join TblNTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Status", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new NTI()
                        {
                            PayrunCode = Payrun,
                            DocNo = Sm.DrStr(dr, c[0]),
                            Status = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI2(ref List<TI2> l)
        {
            string Payrun = Sm.GetLue(LuePayrunCode);
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.Value1, B.Value2, B.Value3 ");
            SQL.AppendLine("From TblTI2Hdr A ");
            SQL.AppendLine("Inner Join TblTI2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DocNo", 
                    "SeqNo", "Amt1", "Amt2", "Value1", "Value2",
                    "Value3"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI2()
                        {
                            PayrunCode = Payrun,
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            Value1 = Sm.DrDec(dr, c[4]),
                            Value2 = Sm.DrDec(dr, c[5]),
                            Value3 = Sm.DrDec(dr, c[6])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeaveDtl(ref List<LeaveDtl> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select LeaveCode, SystemType, PayrunPeriod, DeductTHPInd ");
            SQL.AppendLine("From TblLeaveDtl;");
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "LeaveCode", "SystemType", "PayrunPeriod", "DeductTHPInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new LeaveDtl()
                        {
                            LeaveCode = Sm.DrStr(dr, c[0]),
                            SystemType = Sm.DrStr(dr, c[1]),
                            PayrunPeriod = Sm.DrStr(dr, c[2]),
                            DeductTHPInd = Sm.DrStr(dr, c[3])=="Y"
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeePPS(ref List<EmployeePPS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.StartDt, A.EndDt, ");
            SQL.AppendLine("A.PosCode, A.DeptCode, A.GrdLvlCode, C.LevelCode, A.EmploymentStatus, ");
            SQL.AppendLine("A.SystemType, A.PayrunPeriod, A.PGCode, A.SiteCode, IfNull(B.HOInd, 'N') As HOInd ");
            SQL.AppendLine("From TblEmployeePPS A ");
            SQL.AppendLine("Left Join TblSite B On A.SiteCode=B.SiteCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr C On A.GrdLvlCode=C.GrdLvlCode ");
            SQL.AppendLine("Where ");
            if (Filter.Length != 0)
                SQL.AppendLine(Filter + ";");
            else
                SQL.AppendLine(" 0=1;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "StartDt", "EndDt", "PosCode", "DeptCode", "GrdLvlCode", 
                    
                    //6-10
                    "LevelCode", "EmploymentStatus", "SystemType", "PayrunPeriod", "PGCode", 

                    //11-12
                    "SiteCode", "HOInd"
                
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeePPS()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            EndDt = Sm.DrStr(dr, c[2]),
                            PosCode = Sm.DrStr(dr, c[3]),
                            DeptCode = Sm.DrStr(dr, c[4]),
                            GrdLvlCode = Sm.DrStr(dr, c[5]),
                            LevelCode = Sm.DrStr(dr, c[6]),
                            EmploymentStatus = Sm.DrStr(dr, c[7]),
                            SystemType = Sm.DrStr(dr, c[8]),
                            PayrunPeriod = Sm.DrStr(dr, c[9]),
                            PGCode = Sm.DrStr(dr, c[10]),
                            SiteCode = Sm.DrStr(dr, c[11]),
                            HOInd = Sm.DrStr(dr, c[12])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpWorkSchedule(ref List<EmpWorkSchedule> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = string.Empty;
            var EmpCode = string.Empty;
            var DeductionRoundTemp = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select C.OptCode As DeductionRound, A.EmpCode, A.Dt, A.WSCode, B.HolidayInd, ");
            SQL.AppendLine("B.In1, B.Out1, B.In2, B.Out2, B.In3, B.Out3 ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode=B.WSCode ");
            SQL.AppendLine("Left Join TblOption C On B.DeductionRoundCode=C.OptCode And C.OptCat='DeductionRound' ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine(";");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DeductionRound", 
                    
                    //1-5
                    "EmpCode", "Dt", "WSCode", "HolidayInd", "In1", 
                    
                    //6-10
                    "Out1", "In2", "Out2", "In3", "Out3"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        DeductionRoundTemp = Sm.DrStr(dr, c[0]);
                        if (DeductionRoundTemp.Length==0) DeductionRoundTemp = "0";
                        l.Add(new EmpWorkSchedule()
                        {
                            DeductionRound=decimal.Parse(DeductionRoundTemp),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            Dt = Sm.DrStr(dr, c[2]),
                            WSCode = Sm.DrStr(dr, c[3]),
                            HolidayInd = Sm.DrStr(dr, c[4]),
                            In1 = Sm.DrStr(dr, c[5]),
                            Out1 = Sm.DrStr(dr, c[6]),
                            In2 = Sm.DrStr(dr, c[7]),
                            Out2 = Sm.DrStr(dr, c[8]),
                            In3 = Sm.DrStr(dr, c[9]),
                            Out3 = Sm.DrStr(dr, c[10])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpLeaveHourProcess(ref List<EmpLeaveHourProcess> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = string.Empty;
            var EmpCode = string.Empty;
            var DeductionRoundTemp = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r= 0; r< Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(C.EmpCode=@EmpCode_" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode_" + r.ToString(), EmpCode);
                    }
                }
            }

            SQL.AppendLine("Select C.EmpCode, C.StartDt As Dt, Sum(B.Duration) Duration ");
            SQL.AppendLine("From TblEmpLeaveHourProcessHdr A ");
            SQL.AppendLine("Inner Join TblEmpLeaveHourProcessDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And Find_In_Set( ");
            SQL.AppendLine("        IfNull(B.LeaveCode, ''), ");
            SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParCode Is Not Null And ParCode='LeaveCodeDeductionRound'), '') ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("Inner Join TblEmpleaveHdr C ");
            SQL.AppendLine("    On B.LeaveHourDocNo=C.DocNo ");
            SQL.AppendLine("    And C.StartDt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            else
                SQL.AppendLine(" And 1=0 ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("Group By C.EmpCode, C.StartDt ");
            SQL.AppendLine("Having Sum(B.Duration)> ");
            SQL.AppendLine("Cast(IfNull((Select ParValue From TblParameter Where ParCode Is Not Null And ParCode='DeductionRoundHour'), '4.00') As Decimal) ");
            SQL.AppendLine("; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "EmpCode", "Dt", "Duration" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        DeductionRoundTemp = Sm.DrStr(dr, c[0]);
                        if (DeductionRoundTemp.Length == 0) DeductionRoundTemp = "0";
                        l.Add(new EmpLeaveHourProcess()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            Duration = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAtd(ref List<Atd> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select B.EmpCode, B.Dt, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else Left(B.InOutA1, 12) End As ActualIn, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else Right(B.InOutA1, 12) End As ActualOut ");
            SQL.AppendLine("From TblAtdHdr A ");
            SQL.AppendLine("Inner Join TblAtdDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("Where A.CancelInd='N'; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "EmpCode", "Dt", "ActualIn", "ActualOut" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Atd()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            ActualIn = Sm.DrStr(dr, c[2]) == "XXXXXXXXXXXX" ? string.Empty : Sm.DrStr(dr, c[2]),
                            ActualOut = Sm.DrStr(dr, c[3]) == "XXXXXXXXXXXX" ? string.Empty : Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeave(ref List<Leave> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(Tbl.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select T1.EmpCode, T2.LeaveDt, T1.LeaveCode, T1.LeaveType, T1.StartTm, T1.EndTm, T1.DurationHour, T3.PaidInd, 'N' As CompulsoryLeaveInd ");
            SQL.AppendLine("From TblEmpLeaveHdr T1 ");
            SQL.AppendLine("Inner Join TblEmpLeaveDtl T2 On T1.DocNo=T2.DocNo And T2.LeaveDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblLeave T3 On T1.LeaveCode=T3.LeaveCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status='A' ");
            if (Filter.Length != 0) SQL.AppendLine("And (" + Filter.Replace("Tbl.", "T1.") + ") ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T2.EmpCode, T3.LeaveDt, T1.LeaveCode, T1.LeaveType, T1.StartTm, T1.EndTm, T2.DurationHour, T4.PaidInd, T1.CompulsoryLeaveInd As CompulsoryLeaveInd ");
            SQL.AppendLine("From TblEmpLeave2Hdr T1 ");
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl T2 On T1.DocNo=T2.DocNo ");
            if (Filter.Length != 0) SQL.AppendLine("And (" + Filter.Replace("Tbl.", "T2.") + ") ");
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl2 T3 ");
            SQL.AppendLine("    On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("    And T3.LeaveDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblLeave T4 On T1.LeaveCode=T4.LeaveCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status='A';");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "LeaveDt", "LeaveCode", "LeaveType", "StartTm", "EndTm", 
                    
                    //6-8
                    "DurationHour", "PaidInd", "CompulsoryLeaveInd"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Leave()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            LeaveCode = Sm.DrStr(dr, c[2]),
                            LeaveType = Sm.DrStr(dr, c[3]),
                            StartTm = Sm.DrStr(dr, c[4]),
                            EndTm = Sm.DrStr(dr, c[5]),
                            DurationHr = Sm.DrDec(dr, c[6]),
                            PaidInd = Sm.DrStr(dr, c[7]),
                            CompulsoryLeaveInd = Sm.DrStr(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployee(ref List<Employee> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r< Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, JoinDt, ResignDt ");
            SQL.AppendLine("From TblEmployee ");
            if (Filter.Length != 0)
                SQL.AppendLine("Where (" + Filter + ");");
            else
                SQL.AppendLine("Where 1=0;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "EmpCode", "JoinDt", "ResignDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            ResignDt = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeSalary(ref List<EmployeeSalary> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, StartDt, Amt, Amt2 ");
            SQL.AppendLine("From TblEmployeeSalary ");
            SQL.AppendLine("Where StartDt<=@EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ");");
            else
                SQL.AppendLine("And 0=1;");

            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                    { 
                        "EmpCode", 
                        "StartDt", "Amt", "Amt2" 
                    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeSalary()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }


        private void ProcessGradeLevel(ref List<GradeLevel> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select GrdLvlCode, BasicSalary, BasicSalary2 From TblGradeLevelHdr;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "GrdLvlCode", "BasicSalary", "BasicSalary2" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(new GradeLevel()
                        {
                            GrdLvlCode = Sm.DrStr(dr, c[0]),
                            BasicSalary = Sm.DrDec(dr, c[1]),
                            BasicSalary2 = Sm.DrDec(dr, c[2])
                        });
                }
                dr.Close();
            }
        }


        private void ProcessEmployeeSalarySS(ref List<EmployeeSalarySS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r= 0; r< Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            if (Filter.Length > 0)
                Filter = " Where ( " + Filter + ") "; 
            else
                Filter = " Where 1=0 ";

            SQL.AppendLine("Select A.EmpCode, A.StartDt, A.EndDt, A.Amt ");
            SQL.AppendLine("From TblEmployeeSalarySS A ");
            SQL.AppendLine("Inner Join TblParameter B On A.SSPCode=B.ParValue And B.ParCode='SSProgramForPension' And B.ParValue Is Not Null ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("); ");
            
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "StartDt", "EndDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeSalarySS()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            EndDt = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAttendanceLog(ref List<AttendanceLog> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;
            var EmpCodeTemp = string.Empty;
            var DtTemp = string.Empty;
            var TmTemp = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, Dt, Tm ");
            SQL.AppendLine("From TblAttendanceLog Where Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            var EndDtTemp = Sm.GetDte(DteEndDt);

            DateTime EndDt = new DateTime(
                      Int32.Parse(EndDtTemp.Substring(0, 4)),
                      Int32.Parse(EndDtTemp.Substring(4, 2)),
                      Int32.Parse(EndDtTemp.Substring(6, 2)),
                      0, 0, 0).AddDays(1);

            Sm.CmParamDt(ref cm, "@EndDt", EndDt.Year.ToString() +
                        ("00" + EndDt.Month.ToString()).Substring(("00" + EndDt.Month.ToString()).Length - 2, 2) +
                        ("00" + EndDt.Day.ToString()).Substring(("00" + EndDt.Day.ToString()).Length - 2, 2));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Tm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCodeTemp = Sm.DrStr(dr, c[0]);
                        DtTemp = Sm.DrStr(dr, c[1]);
                        TmTemp = Sm.DrStr(dr, c[2]);
                        if (TmTemp.Length > 4) TmTemp = Sm.Left(TmTemp, 4);
                        l.Add(new AttendanceLog()
                        {
                            EmpCode = EmpCodeTemp,
                            Dt = DtTemp,
                            Tm = TmTemp
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpPaidDt(ref List<EmpPaidDt> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select Distinct A.EmpCode, A.Dt ");
            SQL.AppendLine("From TblPayrollProcess2 A ");
            SQL.AppendLine("Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("And IfNull(A.PayrunCode, '')<>@PayrunCode ");
            SQL.AppendLine("And A.ProcessInd='Y' ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<string>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpPaidDt()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeAllowance(ref List<EmployeeAllowance> l, string ADCode)
        {
            l.Clear();

            if (mADCodeMeal.Length == 0) return;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, EmpCode = string.Empty;

            Sm.CmParam<String>(ref cm, "@ADCode", ADCode);

            if (Grd1.Rows.Count > 0)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, Amt ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction ");
            SQL.AppendLine("Where ADCode=@ADCode ");
            if (Filter.Length > 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            else
                SQL.AppendLine(" And 0=1 ");
            SQL.AppendLine("Order By EmpCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeAllowance()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Amt = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeADOT(ref List<EmployeeADOT> l)
        {
            l.Clear();

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, EmpCode = string.Empty;

            if (Grd1.Rows.Count > 0)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, ADCode, Amt, HolidayInd From (");
            SQL.AppendLine("    Select EmpCode, ADCode, Amt, 'Y' As HolidayInd ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Where Find_In_Set( ");
            SQL.AppendLine("        IfNull(ADCode, ''), ");
            SQL.AppendLine("        (Select ParValue From TblParameter Where ParCode Is Not Null And ParCode='ADCodeOTHoliday') ");
            SQL.AppendLine("        ) ");
            if (Filter.Length > 0)
                SQL.AppendLine("    And (" + Filter + ") ");
            else
                SQL.AppendLine("    And 0=1 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select EmpCode, ADCode, Amt, 'N' As HolidayInd ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Where Find_In_Set( ");
            SQL.AppendLine("        IfNull(ADCode, ''), ");
            SQL.AppendLine("        (Select ParValue From TblParameter Where ParCode Is Not Null And ParCode='ADCodeOTNonHoliday') ");
            SQL.AppendLine("        ) ");
            if (Filter.Length > 0)
                SQL.AppendLine("    And (" + Filter + ") ");
            else
                SQL.AppendLine("    And 0=1 ");
            SQL.AppendLine(") T Order By EmpCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "ADCode", "Amt", "HolidayInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeADOT()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            HolidayInd = Sm.DrStr(dr, c[3])=="Y",
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeaveMealTransport(ref List<LeaveMealTransport> l)
        {
            l.Clear();

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            
            SQL.AppendLine("Select LeaveCode From TblLeave ");
            SQL.AppendLine("Where Find_In_Set( ");
            SQL.AppendLine("IfNull(LeaveCode, ''), ");
            SQL.AppendLine("    (Select ParValue From TblParameter Where ParCode Is Not Null And ParCode='LeaveCodeGetMealTransport') ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By LeaveCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "LeaveCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new LeaveMealTransport()
                        {
                            LeaveCode = Sm.DrStr(dr, c[0])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessWorkScheduleADMealTransport(ref List<WorkScheduleADMealTransport> l)
        {
            l.Clear();

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select WSCode, ADCode From TblWorkScheduleAD ");
            if (mADCodeTransport.Length > 0 || mADCodeMeal.Length > 0)
            {
                SQL.AppendLine("Where ADCode In ('' ");
                if (mADCodeTransport.Length > 0)
                    SQL.AppendLine(", @ADCodeTransport");
                if (mADCodeMeal.Length > 0)
                    SQL.AppendLine(", @ADCodeMeal");
                SQL.AppendLine(") ");
            }
            else
                SQL.AppendLine("Where 0=1 ");
            SQL.AppendLine("Order By WSCode; ");

            Sm.CmParam<String>(ref cm, "@ADCodeTransport", mADCodeTransport);
            Sm.CmParam<String>(ref cm, "@ADCodeMeal", mADCodeMeal);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WSCode", "ADCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new WorkScheduleADMealTransport()
                        {
                            WSCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOTIncentive(ref List<OTIncentive> l)
        {
            l.Clear();

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            string Filter = string.Empty, EmpCode = string.Empty;

            if (Grd1.Rows.Count > 0)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (B.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ")";
            else
                Filter = " And 1=0 ";

            SQL.AppendLine("Select B.EmpCode, A.ADCode, A.OTIncentiveDt, B.OTIncentiveStartTm, B.OTIncentiveEndTm ");
            SQL.AppendLine("From TblOTIncentiveHdr A ");
            SQL.AppendLine("Inner Join TblOTIncentiveDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.OTIncentiveDt Between @StartDt And @EndDt ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By B.EmpCode, A.ADCode, A.OTIncentiveDt; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {  
                    "EmpCode", 
                    "ADCode", "OTIncentiveDt", "OTIncentiveStartTm", "OTIncentiveEndTm"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OTIncentive()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1]),
                            OTIncentiveDt = Sm.DrStr(dr, c[2]),
                            OTIncentiveStartTm = Sm.DrStr(dr, c[3]),
                            OTIncentiveEndTm = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Additional Method

        private decimal ComputeDuration(string Value1, string Value2)
        {
            return Convert.ToDecimal((Sm.ConvertDateTime(Value1) - Sm.ConvertDateTime(Value2)).TotalHours);
        }

        private void GetDt(ref List<string> l)
        {
            string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

            DateTime
                Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    ),
                Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

            var TotalDays = (Dt2 - Dt1).Days;
            l.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );
            for (int i = 1; i <= TotalDays; i++)
            {
                Dt1 = Dt1.AddDays(1);
                l.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );
            }
        }

        private decimal RoundTo30Minutes(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0.5)
                return (decimal)(Value2 + 0.5);
            else
            {
                if (Value - Value2 == 0)
                    return (decimal)(Value);
                else
                {
                    if (Value - Value2 > 0 && Value - Value2 < 0.5)
                        return (decimal)(Value2);
                    else
                        return (decimal)(Value);
                }
            }
        }

        private decimal RoundTo30Minutes2(double Value) // jika kurang dari setengah jam di bulatkan kebawah jika lbh jadiin jam atas
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0.5)
                return (decimal)(Value2 + 1);
            else
            {
                if (Value - Value2 == 0)
                    return (decimal)(Value);
                else
                {
                    if (Value - Value2 > 0 && Value - Value2 < 0.5)
                        return (decimal)(Value2);
                    else
                        return (decimal)(Value);
                }
            }
        }


        private decimal RoundTo1Hour(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0)
                return (decimal)(Value2);
            else
                return (decimal)(Value);
        }

        #endregion

        #endregion

        #region Event

        private void FrmPayrollProcess_Load(object sender, EventArgs e)
        {
            try
            {
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                Sl.SetLueOption(ref LueEmploymentStatus, "EmploymentStatus");
                GetParameter();
                SetLuePayrunCode(ref LuePayrunCode);
                SetGrd();

                mlAttendanceLog = new List<AttendanceLog>();
                mlOT = new List<OT>();
                mlOTAdjustment = new List<OTAdjustment>();
                mlEmployeeAllowanceMeal = new List<EmployeeAllowance>();
                mlEmployeeAllowanceTransport = new List<EmployeeAllowance>();
                mlAdvancePaymentProcess = new List<AdvancePaymentProcess>();
                mlOTIncentive = new List<OTIncentive>();
                mlWorkScheduleADMealTransport = new List<WorkScheduleADMealTransport>();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LuePayrunCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearData();
            Sm.RefreshLookUpEdit(LuePayrunCode, new Sm.RefreshLue1(SetLuePayrunCode));
        }

        private void BtnPayrunCode_Click(object sender, EventArgs e)
        {
            ClearData();
            ShowPayrunInfo();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
               ProcessData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 0);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        Grd1.Cells[Row, 0].Value = !IsSelected;
            }
        }

        private void LueEmploymentStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmploymentStatus, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEmploymentStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employment status");
        }

        #endregion

        #region Class

        private class Holiday
        {
            public string HolDt { get; set; }
        }

        private class Result1
        {
            public bool _IsUseLatestGrdLvlSalary { get; set; }
            public string _LatestGrdLvlCode { get; set; }
            public string _LatestLevelCode { get; set; }
            public decimal _LatestMonthlyGrdLvlSalary { get; set; }
            public decimal _LatestDailyGrdLvlSalary { get; set; }
            public bool _IsFullDayInd { get; set; }
            public bool _IsHOInd { get; set; }
            public decimal _ExtraFooding { get; set; }
            public decimal _FieldAssignment { get; set; }
            public decimal _IncPerformance { get; set; }
            public decimal _DeductionRound { get; set; }
            public decimal _LeaveDuration { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public bool ProcessInd { get; set; }
            public string LatestPaidDt { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string PosCode { get; set; }
            public string DeptCode { get; set; }
            public string GrdLvlCode { get; set; }
            public string LevelCode { get; set; }
            public string SystemType { get; set; }
            public string EmploymentStatus { get; set; }
            public string PayrunPeriod { get; set; }
            public string PGCode { get; set; }
            public string SiteCode { get; set; }
            public decimal WorkingDay { get; set; }
            public string WSCode { get; set; }
            public bool HolInd { get; set; }
            public decimal HolidayIndex { get; set; }
            public bool WSHolidayInd { get; set; }
            public string WSIn1 { get; set; }
            public string WSOut1 { get; set; }
            public string WSIn2 { get; set; }
            public string WSOut2 { get; set; }
            public string WSIn3 { get; set; }
            public string WSOut3 { get; set; }
            public bool OneDayInd { get; set; }
            public bool LateInd { get; set; }
            public string ActualIn { get; set; }
            public string ActualOut { get; set; }
            public string WorkingIn { get; set; }
            public string WorkingOut { get; set; }
            public decimal WorkingDuration { get; set; }
            public decimal BasicSalary { get; set; }
            public decimal BasicSalary2 { get; set; }
            public decimal SalaryPension { get; set; }
            public decimal ProductionWages { get; set; }
            public decimal Salary { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveType { get; set; }
            public bool PaidLeaveInd { get; set; }
            public bool CompulsoryLeaveInd { get; set; }
            public bool _DeductTHPInd { get; set; }
            public string LeaveStartTm { get; set; }
            public string LeaveEndTm { get; set; }
            public decimal LeaveDuration { get; set; }
            public decimal PLDay { get; set; }
            public decimal PLHr { get; set; }
            public decimal PLAmt { get; set; }
            public decimal ProcessPLAmt { get; set; }
            public decimal OT1Hr { get; set; }
            public decimal OT2Hr { get; set; }
            public decimal OTHolidayHr { get; set; }
            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public decimal OTHolidayAmt { get; set; }
            public bool OTToLeaveInd { get; set; }
            public decimal IncMinWages { get; set; }
            public decimal IncProduction { get; set; }
            public decimal IncPerformance { get; set; }
            public bool PresenceRewardInd { get; set; }
            public decimal HolidayEarning { get; set; }
            public decimal ExtraFooding { get; set; }
            public decimal FieldAssignment { get; set; }
            public decimal DedProduction { get; set; }
            public decimal DedProdLeave { get; set; }
            public decimal EmpSalary { get; set; }
            public decimal EmpSalary2 { get; set; }
            public decimal SalaryAD { get; set; }
            public decimal UPLDay { get; set; }
            public decimal UPLHr { get; set; }
            public decimal UPLAmt { get; set; }
            public decimal ProcessUPLAmt { get; set; }
            public decimal IncEmployee { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
            public decimal ServiceChargeIncentive { get; set; }
            public decimal ADOT { get; set; }
        }

        private class Result2
        {
            public decimal _ADTransport { get; set; }
            public decimal _RecomputedValueForFixedAD { get; set; }
            public decimal _RecomputedValueForProratedAD { get; set; }
            public string _LatestGrdLvlCode { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string EmploymentStatus { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string NPWP { get; set; }
            public string PTKP { get; set; }
	        public decimal Salary { get; set; }
	        public decimal WorkingDay { get; set; }
	        public decimal PLDay { get; set; }
	        public decimal PLHr { get; set; }
	        public decimal PLAmt { get; set; }
            public decimal ProcessPLAmt { get; set; }
            public decimal OT1Hr { get; set; }
	        public decimal OT2Hr { get; set; }
	        public decimal OTHolidayHr { get; set; }
	        public decimal OT1Amt { get; set; }
	        public decimal OT2Amt { get; set; }
	        public decimal OTHolidayAmt { get; set; }
	        public decimal FixAllowance { get; set; }
	        public decimal EmploymentPeriodAllowance { get; set; }
	        public decimal IncEmployee { get; set; }
	        public decimal IncMinWages { get; set; }
	        public decimal IncProduction { get; set; }
	        public decimal IncPerformance { get; set; }
	        public decimal PresenceReward { get; set; }
	        public decimal HolidayEarning { get; set; }
	        public decimal ExtraFooding { get; set; }
	        public decimal SSEmployerHealth { get; set; }
	        public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployerPension { get; set; }
            public decimal SSEmployerPension2 { get; set; }
            public decimal SSEmployerNonRetiring { get; set; }
            public decimal SSEmployeeOldAgeInsurance { get; set; }
	        public decimal FixDeduction { get; set; }
	        public decimal DedEmployee { get; set; }
	        public decimal DedProduction { get; set; }
	        public decimal DedProdLeave { get; set; }
            public decimal EmpAdvancePayment { get; set; }
	        public decimal SSEmployeeHealth { get; set; }
	        public decimal SSEmployeeEmployment { get; set; }
            public decimal SSEmployeePension { get; set; }
            public decimal SSEmployeePension2 { get; set; }
            public decimal SalaryAdjustment { get; set; }
	        public decimal Tax { get; set; }
            public decimal TaxAllowance { get; set; }
            public decimal Amt { get; set; }
            public decimal UPLDay { get; set; }
            public decimal UPLHr { get; set; }
            public decimal UPLAmt { get; set; }
            public decimal ProcessUPLAmt { get; set; }
            public decimal NonTaxableFixAllowance { get; set; }
            public decimal NonTaxableFixDeduction { get; set; }
            public decimal TaxableFixAllowance { get; set; }
            public decimal TaxableFixDeduction { get; set; }
            public decimal FieldAssignment { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
            public decimal Functional { get; set; }
            public decimal Housing { get; set; }
            public decimal MobileCredit { get; set; }
            public decimal Zakat { get; set; }
            public decimal ServiceChargeIncentive { get; set; }
            public decimal SalaryPension { get; set; }
            public decimal SSErLifeInsurance { get; set; }
            public decimal SSEeLifeInsurance { get; set; }
            public decimal SSErWorkingAccident { get; set; }
            public decimal SSEeWorkingAccident { get; set; }
            public decimal SSErRetirement { get; set; }
            public decimal SSEeRetirement { get; set; }
            public decimal SSErPension { get; set; }
            public decimal SSEePension { get; set; }
            public string CreditCode1 { get; set; }
            public string CreditCode2 { get; set; }
            public string CreditCode3 { get; set; }
            public string CreditCode4 { get; set; }
            public string CreditCode5 { get; set; }
            public string CreditCode6 { get; set; }
            public string CreditCode7 { get; set; }
            public string CreditCode8 { get; set; }
            public string CreditCode9 { get; set; }
            public string CreditCode10 { get; set; }
            public decimal CreditAdvancePayment1 { get; set; }
            public decimal CreditAdvancePayment2 { get; set; }
            public decimal CreditAdvancePayment3 { get; set; }
            public decimal CreditAdvancePayment4 { get; set; }
            public decimal CreditAdvancePayment5 { get; set; }
            public decimal CreditAdvancePayment6 { get; set; }
            public decimal CreditAdvancePayment7 { get; set; }
            public decimal CreditAdvancePayment8 { get; set; }
            public decimal CreditAdvancePayment9 { get; set; }
            public decimal CreditAdvancePayment10 { get; set; }
            public string PerformanceStatus { get; set; }
            public string GradePerformance { get; set; }
            public decimal PerformanceValue { get; set; }
            public decimal ADOT { get; set; }
            public decimal FixAllowance1 { get; set; }
            public decimal FixAllowance2 { get; set; }
            public decimal TaxableFixAllowance1 { get; set; }
            public decimal TaxableFixAllowance2 { get; set; }
            public decimal IKP { get; set; }
        }

        #region Additional Class

        private class AdvancePaymentProcess
        {
            public string DocNo { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string EmpCode { get; set; }
            public string CreditCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmployeeSalary
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
        }

        private class EmployeeSalarySS
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class OT
        {
            public string OTDocNo { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm1 { get; set; }
            public string Tm2 { get; set; }
        }

        private class OTAdjustment
        {
            public string OTDocNo { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm1 { get; set; }
            public string Tm2 { get; set; }
        }

        private class NTI
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string Status { get; set; }
            public decimal Amt { get; set; }
        }

        private class TI
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal TaxRate { get; set; }
        }

        private class TI2
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Value1 { get; set; }
            public decimal Value2 { get; set; }
            public decimal Value3 { get; set; }
        }

        private class EmployeePPS
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string DeptCode { get; set; }
            public string GrdLvlCode { get; set; }
            public string LevelCode { get; set; }
            public string EmploymentStatus { get; set; }
            public string SystemType { get; set; }
            public string PayrunPeriod { get; set; }
            public string PGCode { get; set; }
            public string SiteCode { get; set; }
            public string PosCode { get; set; }
            public string HOInd { get; set; }
        }

        private class EmpWorkSchedule
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string WSCode { get; set; }
            public string HolidayInd { get; set; }
            public string In1 { get; set; }
            public string Out1 { get; set; }
            public string In2 { get; set; }
            public string Out2 { get; set; }
            public string In3 { get; set; }
            public string Out3 { get; set; }
            public decimal DeductionRound { get; set; }
        }

        private class EmpLeaveHourProcess
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public decimal Duration { get; set; }
        }

        private class Atd
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string ActualIn { get; set; }
            public string ActualOut { get; set; }
        }

        private class Leave
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveType { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public decimal DurationHr { get; set; }
            public string PaidInd { get; set; }
            public string CompulsoryLeaveInd { get; set; }
        }

        private class LeaveDtl
        {
            public string LeaveCode { get; set; }
            public string SystemType { get; set; }
            public string PayrunPeriod { get; set; }
            public bool DeductTHPInd { get; set; }
        }

        private class Employee
        {
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string LatestPaidDt { get; set; }
            public decimal IncPerformance { get; set; }
        }

        private class AttendanceLog
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm { get; set; }
        }

        private class EmpPaidDt
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
        }

        private class EmployeeAllowance
        {
            public string EmpCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmployeeADOT
        {
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public decimal Amt { get; set; }
            public bool HolidayInd { get; set; }
        }

        private class PayrollProcessADOT
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string Dt { get; set; }
            public decimal Amt { get; set; }
            public decimal Duration { get; set; }
        }

        private class LeaveMealTransport
        {
            public string LeaveCode { get; set; }
        }

        private class OTIncentive
        {
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string OTIncentiveDt { get; set; }
            public string OTIncentiveStartTm { get; set; }
            public string OTIncentiveEndTm { get; set; }
        }

        private class WorkScheduleADMealTransport
        {
            public string WSCode { get; set; }
            public string ADCode { get; set; }
        }

        private class GradeLevel
        {
            public string GrdLvlCode { get; set; }
            public decimal BasicSalary { get; set; }
            public decimal BasicSalary2 { get; set; }
        }

        #endregion

        #endregion
    }
}
