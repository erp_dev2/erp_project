﻿#region Update
/*
    23/10/2019 [DITA/KSM] Bug saat refresh data
 *  09/12/2020 [ICA/IMS] Mengubah keterangan Material Request menjadi Purchase Request
  */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmMRQtyCancelFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmMRQtyCancel mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmMRQtyCancelFind(FrmMRQtyCancel FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);

            if (mFrmParent.mDocTitle == "IMS")
            {
                this.label7.Text = "Purchase Request#";
                this.label7.Location = new System.Drawing.Point(8, 50);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocNo, T.CancelInd, T.DocDt, T.MRDocNo, T.DeptName, T.ItCode, T.ItName, T.ItCtName, T.PurchaseUomCode, T.Remark, T.Qty, T.OutstandingQty, T.CancelQty, ");
            SQL.AppendLine("    T.CreateBy, T.CreateDt, T.LastUpBy, T.LastUpDt From ( ");
            SQL.AppendLine("    Select H.DocNo, H.CancelInd, H.DocDt, H.MRDocNo, H.MRDNo, C.DeptName, B.ItCode, D.ItName, E.ItCtName, D.PurchaseUomCode, H.Remark, ");
            SQL.AppendLine("    B.Qty, B.Qty-IfNull(( ");
            SQL.AppendLine("        Select Sum(T.Qty) From TblPORequestDtl T ");
            SQL.AppendLine("        Where T.CancelInd='N' ");
            SQL.AppendLine("        And IfNull(T.Status,'X')<>'C' "); 
            SQL.AppendLine("        And T.MaterialRequestDocNo=A.DocNo "); 
            SQL.AppendLine("        And T.MaterialRequestDNo=B.DNo ");
            SQL.AppendLine("        ), 0) As OutstandingQty, ifnull(H.Qty, 0) As CancelQty, H.CreateBy, H.CreateDt, H.LastUpBy, H.LastUpDt ");
            SQL.AppendLine("    From TblMaterialRequestHdr A ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And IfNull(B.Status,'X')='A' And B.CancelInd='N' ");
            SQL.AppendLine("    Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("    Left Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("    Left Join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
            SQL.AppendLine("    Inner Join TblMRQtyCancel H On B.DocNo = H.MRDocNo And B.DNo = H.MRDNo ");
            SQL.AppendLine("    Where A.Status='A' And A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And (A.DeptCode Is Null Or ( ");
                SQL.AppendLine("    A.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine(") T Where OutstandingQty>0  ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        (mFrmParent.mDocTitle == "IMS") ? "Purchase Request#" : "Material Request#",
                        "Department",
                        
                        //6-10
                        "Item's Code",
                        "Item's Name",
                        "Item"+Environment.NewLine+"Category",
                        "UoM",
                        "Quantity",

                        //11-15
                        "Outstanding"+Environment.NewLine+"Quantity",
                        "Cancelled"+Environment.NewLine+"Quantity",
                        "Remark",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",

                        //16-19
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                     new int[] 
                    {
                        40,
                        150, 100, 50, 150, 150,  
                        100, 200, 200, 50, 100, 
                        100, 100, 200, 100, 100, 
                        100, 100, 100, 100 
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 15, 18 });
            Sm.GrdFormatTime(Grd1, new int[] { 16, 19 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 14, 15, 16, 17, 18, 19 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 14, 15, 16, 17, 18, 19 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtMRDocNo.Text, "T.MRDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T.DocDt, T.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            //1-5
                            "DocDt", "CancelInd", "MRDocNo", "DeptName", "ItCode", 
                            //6-10
                            "ItName", "ItCtName", "PurchaseUomCode",  "Qty", "OutstandingQty", 
                            //11-15
                            "CancelQty", "Remark", "CreateBy", "CreateDt", "LastUpBy",   
                            //16
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 16);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkMRDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, (mFrmParent.mDocTitle == "IMS") ? "Purchase request#" : "Material request#");
        }

        private void TxtMRDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
         
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
