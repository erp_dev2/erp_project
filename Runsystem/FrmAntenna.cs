﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAntenna : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmAntennaFind FrmFind;
        
        #endregion

        #region Constructor

        public FrmAntenna(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtAntennaCode, TxtAntennaName, TxtdBi, ChkActInd, LueBrCode }, true);
                    TxtAntennaCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtAntennaCode, TxtAntennaName, TxtdBi, LueBrCode }, false);
                    TxtAntennaCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtAntennaName, TxtdBi, ChkActInd, LueBrCode }, false);
                    TxtAntennaName.Focus();
                    break;
                default: break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtAntennaCode, TxtAntennaName, LueBrCode });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtdBi }, 1);
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAntennaFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                Sl.SetLueBrCode(ref LueBrCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAntennaCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAntennaCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblAntenna Where AntennaCode=@AntennaCode" };
                Sm.CmParam<String>(ref cm, "@AntennaCode", TxtAntennaCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblAntenna(AntennaCode, AntennaName, BrCode, dBi, ActInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@AntennaCode, @AntennaName, @BrCode, @dBi, @ActInd, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update AntennaName=@AntennaName, BrCode=@BrCode, dBi=@dBi, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@AntennaCode", TxtAntennaCode.Text);
                Sm.CmParam<String>(ref cm, "@AntennaName", TxtAntennaName.Text);
                Sm.CmParam<String>(ref cm, "@BrCode", Sm.GetLue(LueBrCode));
                Sm.CmParam<Decimal>(ref cm, "@dBi", decimal.Parse(TxtdBi.Text));
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtAntennaCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string AntennaCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@AntennaCode", AntennaCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select AntennaCode, AntennaName, BrCode, dBi, ActInd From TblAntenna Where AntennaCode=@AntennaCode;",
                        new string[]{ "AntennaCode", "AntennaName", "BrCode", "dBi", "ActInd" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtAntennaCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtAntennaName.EditValue = Sm.DrStr(dr, c[1]);
                            Sl.SetLueBrCode(ref LueBrCode, Sm.DrStr(dr, c[2]));
                            TxtdBi.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 1);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtAntennaCode, "Antenna code", false) ||
                Sm.IsTxtEmpty(TxtAntennaName, "Antenna name", false) ||
                IsCodeAlreadyExisted();
        }

        private bool IsCodeAlreadyExisted()
        {
            return 
                !TxtAntennaCode.Properties.ReadOnly && 
                Sm.IsDataExist(
                    "Select AntennaCode From TblAntenna Where AntennaCode=@Param;",
                    TxtAntennaCode.Text,
                    "Antenna code ( " + TxtAntennaCode.Text + " ) already existed."
                    );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtAntennaCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAntennaCode);
        }

        private void TxtAntennaName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAntennaName);
        }

        private void LueBrCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBrCode, new Sm.RefreshLue2(Sl.SetLueBrCode), string.Empty);
        }

        private void TxtdBi_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtdBi, 1);
        }

        #endregion

        #endregion
    }
}
