﻿namespace RunSystem
{
    partial class FrmCompanyProcurementPlanFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblDocNo = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtItem = new DevExpress.XtraEditors.TextEdit();
            this.ChkItem = new DevExpress.XtraEditors.CheckEdit();
            this.LueYr = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkYr = new DevExpress.XtraEditors.CheckEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkSite = new DevExpress.XtraEditors.CheckEdit();
            this.LueSite = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkYr.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSite.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.ChkYr);
            this.panel2.Controls.Add(this.LueYr);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtItem);
            this.panel2.Controls.Add(this.ChkItem);
            this.panel2.Controls.Add(this.LblDocNo);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.ChkDocNo);
            this.panel2.Size = new System.Drawing.Size(672, 72);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 401);
            this.Grd1.TabIndex = 18;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // LblDocNo
            // 
            this.LblDocNo.AutoSize = true;
            this.LblDocNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDocNo.Location = new System.Drawing.Point(8, 8);
            this.LblDocNo.Name = "LblDocNo";
            this.LblDocNo.Size = new System.Drawing.Size(73, 14);
            this.LblDocNo.TabIndex = 10;
            this.LblDocNo.Text = "Document#";
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(84, 5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 250;
            this.TxtDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtDocNo.TabIndex = 11;
            this.TxtDocNo.Validated += new System.EventHandler(this.TxtDocNo_Validated);
            // 
            // ChkDocNo
            // 
            this.ChkDocNo.Location = new System.Drawing.Point(328, 4);
            this.ChkDocNo.Name = "ChkDocNo";
            this.ChkDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDocNo.Properties.Caption = " ";
            this.ChkDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDocNo.Size = new System.Drawing.Size(31, 22);
            this.ChkDocNo.TabIndex = 12;
            this.ChkDocNo.ToolTip = "Remove filter";
            this.ChkDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDocNo.ToolTipTitle = "Run System";
            this.ChkDocNo.CheckedChanged += new System.EventHandler(this.ChkDocNo_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "Item";
            // 
            // TxtItem
            // 
            this.TxtItem.EnterMoveNextControl = true;
            this.TxtItem.Location = new System.Drawing.Point(84, 25);
            this.TxtItem.Name = "TxtItem";
            this.TxtItem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItem.Properties.Appearance.Options.UseFont = true;
            this.TxtItem.Properties.MaxLength = 250;
            this.TxtItem.Size = new System.Drawing.Size(242, 20);
            this.TxtItem.TabIndex = 14;
            this.TxtItem.Validated += new System.EventHandler(this.TxtItem_Validated);
            // 
            // ChkItem
            // 
            this.ChkItem.Location = new System.Drawing.Point(328, 24);
            this.ChkItem.Name = "ChkItem";
            this.ChkItem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItem.Properties.Appearance.Options.UseFont = true;
            this.ChkItem.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItem.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItem.Properties.Caption = " ";
            this.ChkItem.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItem.Size = new System.Drawing.Size(31, 22);
            this.ChkItem.TabIndex = 15;
            this.ChkItem.ToolTip = "Remove filter";
            this.ChkItem.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItem.ToolTipTitle = "Run System";
            this.ChkItem.CheckedChanged += new System.EventHandler(this.ChkItem_CheckedChanged);
            // 
            // LueYr
            // 
            this.LueYr.EnterMoveNextControl = true;
            this.LueYr.Location = new System.Drawing.Point(84, 45);
            this.LueYr.Margin = new System.Windows.Forms.Padding(5);
            this.LueYr.Name = "LueYr";
            this.LueYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.Appearance.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueYr.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueYr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueYr.Properties.DropDownRows = 12;
            this.LueYr.Properties.NullText = "[Empty]";
            this.LueYr.Properties.PopupWidth = 500;
            this.LueYr.Size = new System.Drawing.Size(242, 20);
            this.LueYr.TabIndex = 17;
            this.LueYr.ToolTip = "F4 : Show/hide list";
            this.LueYr.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueYr.EditValueChanged += new System.EventHandler(this.LueYr_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(49, 47);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "Year";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkYr
            // 
            this.ChkYr.Location = new System.Drawing.Point(328, 43);
            this.ChkYr.Name = "ChkYr";
            this.ChkYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkYr.Properties.Appearance.Options.UseFont = true;
            this.ChkYr.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkYr.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkYr.Properties.Caption = " ";
            this.ChkYr.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkYr.Size = new System.Drawing.Size(31, 22);
            this.ChkYr.TabIndex = 18;
            this.ChkYr.ToolTip = "Remove filter";
            this.ChkYr.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkYr.ToolTipTitle = "Run System";
            this.ChkYr.CheckedChanged += new System.EventHandler(this.ChkYr_CheckedChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ChkSite);
            this.panel4.Controls.Add(this.LueSite);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(348, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(320, 68);
            this.panel4.TabIndex = 19;
            // 
            // ChkSite
            // 
            this.ChkSite.Location = new System.Drawing.Point(289, 4);
            this.ChkSite.Name = "ChkSite";
            this.ChkSite.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSite.Properties.Appearance.Options.UseFont = true;
            this.ChkSite.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSite.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSite.Properties.Caption = " ";
            this.ChkSite.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSite.Size = new System.Drawing.Size(31, 22);
            this.ChkSite.TabIndex = 20;
            this.ChkSite.ToolTip = "Remove filter";
            this.ChkSite.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSite.ToolTipTitle = "Run System";
            this.ChkSite.CheckedChanged += new System.EventHandler(this.ChkSite_CheckedChanged);
            // 
            // LueSite
            // 
            this.LueSite.EnterMoveNextControl = true;
            this.LueSite.Location = new System.Drawing.Point(43, 6);
            this.LueSite.Margin = new System.Windows.Forms.Padding(5);
            this.LueSite.Name = "LueSite";
            this.LueSite.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSite.Properties.Appearance.Options.UseFont = true;
            this.LueSite.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSite.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSite.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSite.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSite.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSite.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSite.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSite.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSite.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSite.Properties.DropDownRows = 12;
            this.LueSite.Properties.NullText = "[Empty]";
            this.LueSite.Properties.PopupWidth = 500;
            this.LueSite.Size = new System.Drawing.Size(242, 20);
            this.LueSite.TabIndex = 21;
            this.LueSite.ToolTip = "F4 : Show/hide list";
            this.LueSite.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSite.EditValueChanged += new System.EventHandler(this.LueSite_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(8, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 14);
            this.label2.TabIndex = 20;
            this.label2.Text = "Site";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmCompanyProcurementPlanFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmCompanyProcurementPlanFind";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkYr.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSite.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblDocNo;
        private DevExpress.XtraEditors.TextEdit TxtDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtItem;
        private DevExpress.XtraEditors.CheckEdit ChkItem;
        private DevExpress.XtraEditors.LookUpEdit LueYr;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkYr;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.LookUpEdit LueSite;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.CheckEdit ChkSite;
    }
}