﻿#region Update
/*
    04/10/2017 [WED] perbaikan export ke excel untuk VdCode yg awalnya angka 0
    26/05/2018 [HAR] tambah informasi NIB
    15/11/2021 [NJP/RM] Menamlahkan Filter Vendor Sector dan Vendor SubSector
    24/03/2022 [WED/RM] Perbaikan query join ke Vendor Sector, SubSector, berdasarkan parameter IsUseECatalog
    22/12/2022 [VIN/PHT] BUG Export to excel
    03/01/2022 [ICA/MNET] menambah kolom Entity Type berdasarkan parameter IsVendorUseEntityType
    28/02/2023 [SET/HEX] add field Grd Vendor External Code & Selection Score berdasar param IsMasterVendorUseExternalCode & IsMasterVendorUseSelectionScore
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmVendorFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmVendor mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVendorFind(FrmVendor FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                if (!mFrmParent.mIsUseECatalog)
                {
                    LblVendorSector.Visible = TxtSector.Visible = ChkSector.Visible = LblVendorSubSector.Visible = TxtSubSector.Visible = ChkSubSector.Visible = false;
                    panel2.Height = LueVdCtCode.Top + LueVdCtCode.Height + 5;
                }
                SetSQL();
                SetGrd();
                Sl.SetLueVdCtCode(ref LueVdCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        private void SetSQL()
        { 
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.VdCode, A.VdName, A.ShortName, A.ActInd, E.VdCtName, ");
            SQL.AppendLine("A.TIN, A.TaxInd, A.Phone, A.Fax, A.Email, A.Mobile, ");
            SQL.AppendLine("A.Address, B.CityName, C.SDName, D.VilName, F.OptDesc EntityType, A.NIB, ");
            SQL.AppendLine("A.VdExternalCode, A.SelectionScore, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblVendor A ");
            SQL.AppendLine("Left Join TblCity B On A.CityCode=B.CityCode ");
            SQL.AppendLine("Left Join TblSubDistrict C On A.SDCode=C.SDCode ");
            SQL.AppendLine("Left Join TblVillage D On A.VilCode=D.VilCode ");
            SQL.AppendLine("Left Join TblVendorCategory E On A.VdCtCode=E.VdCtCode ");
            SQL.AppendLine("Left Join TblOption F On F.OptCat = 'VendorEntityType' And F.OptCode = A.EntityType ");
            //if (mFrmParent.mIsUseECatalog)
            //{
            //    SQL.AppendLine("Left Join TblVendorSector F On A.VdCode=F.VdCode ");
            //    SQL.AppendLine("Left Join TblSector G On F.SectorCode=G.SectorCode ");
            //    SQL.AppendLine("Left Join TblSubSector H On F.SubSectorCode=H.SubSectorCode ");
            //}

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Short"+Environment.NewLine+"Name",
                        "Active",
                        "Category",
                         
                        //6-10
                        "Taxpayer"+Environment.NewLine+"Indetification#",
                        "Tax",
                        "Phone",
                        "Fax",
                        "Email",

                        //11-15
                        "Mobile",
                        "Address",
                        "City",
                        "Sub District",
                        "Village",

                        //16-20
                        "Entity Type",
                        "NIB",
                        "Vendor External Code",
                        "Selection Score",
                        "Created"+Environment.NewLine+"By",
                        
                        //21-25
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 250, 120, 60, 170, 
                        
                        //6-10
                        150, 60, 100, 100, 100,
  
                        //11-15
                        100, 200, 170, 150, 150,

                        //16-20
                        100, 100, 150, 100, 100, 
                        
                        //21
                        100, 100, 100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4, 7 });
            Sm.GrdFormatDate(Grd1, new int[] { 20, 24 });
            Sm.GrdFormatTime(Grd1, new int[] { 22, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5, 6, 9, 10, 11, 17, 20, 21, 22, 23, 24, 25 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 16 }, mFrmParent.mIsVendorUseEntityType);
            Sm.GrdColInvisible(Grd1, new int[] { 18 }, mFrmParent.mIsMasterVendorUseExternalCode);
            Sm.GrdColInvisible(Grd1, new int[] { 19 }, mFrmParent.mIsMasterVendorUseSelectionScore);
            Sm.GrdFormatDec(Grd1, new int[] { 19 }, 0);
            Grd1.Cols[18].Move(8);
            Grd1.Cols[19].Move(9);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5, 6, 9, 10, 11, 17, 20, 21, 22, 23, 24, 25 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                string Filter2 = string.Empty;
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtVdName.Text, new string[] { "A.VdCode", "A.VdName", "A.ShortName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCtCode), "A.VdCtCode", true);
                if (mFrmParent.mIsUseECatalog)
                {
                    if (TxtSector.Text.Trim().Length > 0 || TxtSubSector.Text.Trim().Length > 0)
                    {
                        var SQLS = new StringBuilder();

                        SQLS.AppendLine("Inner Join TblVendorSector F On A.VdCode = F.VdCode ");
                        if (TxtSector.Text.Trim().Length > 0)
                        {
                            SQLS.AppendLine("Inner Join TblSector G On F.SectorCode = G.SectorCode ");
                            Sm.FilterStr(ref Filter, ref cm, TxtSector.Text, new string[] { "G.SectorCode", "G.SectorName" });
                        }
                        if (TxtSubSector.Text.Trim().Length > 0)
                        {
                            SQLS.AppendLine("Inner Join TblSubSector H On F.SubSectorCode=H.SubSectorCode And F.SectorCode = H.SectorCode ");
                            Sm.FilterStr(ref Filter, ref cm, TxtSubSector.Text, new string[] { "H.SubSectorCode", "H.SubSectorName" });
                        }

                        Filter2 = SQLS.ToString();
                    }
                }

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter2 + Filter + " Order By VdName;",
                    new string[]
                    {
                        //0
                        "VdCode", 
                                
                        //1-5
                        "VdName", "ShortName", "ActInd", "VdCtName",   "TIN",   
                            
                        //6-10
                        "TaxInd", "Phone", "Fax", "Email",  "Mobile",  

                        //11-15
                        "Address", "CityName", "SDName", "VilName", "EntityType",
                            
                        //16-20
                        "NIB", "VdExternalCode", "SelectionScore", "CreateBy", "CreateDt", 
                        
                        //20-21
                        "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);                           
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);

                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 21);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 22);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 25, 22);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Button Method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                //Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtVdName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor");
        }

        private void ChkVdCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor Category");
        }

        private void LueVdCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCtCode, new Sm.RefreshLue1(Sl.SetLueVdCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtSector_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSector_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor Sector");
        }

        private void TxtSubSector_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSubSector_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor Sub Sector");
        }

        #endregion

        #endregion

    }
}
