﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptCompetencePosition : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptCompetencePosition(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLuePosCode(ref LuePosCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PosCode, B.PosName, C.CompetenceName, A.MinScore, IfNull(D.EmpCode, '') As EmpCode, ");
            SQL.AppendLine("IfNull(D.EmpName, '') As EmpName, IfNull(D.Score, 0) As EmpScore, ");
            SQL.AppendLine("(IfNull(D.Score, 0) - IfNull(A.MinScore, 0)) As CompetenceGap ");
            SQL.AppendLine("From TblOrganizationalStructureDtl4 A ");
            SQL.AppendLine("Inner Join TblPosition B On A.PosCode = B.PosCode ");
            SQL.AppendLine("Inner Join TblCompetence C On A.CompetenceCode = C.CompetenceCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select T1.EmpCode, T1.EmpName, T2.CompetenceCode, T3.CompetenceName, T1.PosCode, T2.Score ");
	        SQL.AppendLine("    From TblEmployee T1 ");
	        SQL.AppendLine("    Inner Join TblEmployeeCompetence T2 On T1.EmpCode = T2.EmpCode ");
	        SQL.AppendLine("    Inner Join TblCompetence T3 On T2.CompetenceCode = T3.CompetenceCode ");
            SQL.AppendLine("    Where T1.ResignDt Is Null ");
            SQL.AppendLine(") D On A.PosCode = D.PosCode And A.CompetenceCode = D.CompetenceCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 3;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Position Code",
                        "",
                        "Position", 
                        "Competence", 
                        "Minimum"+Environment.NewLine+"Score",
                                                
                        //6-10
                        "Employee Code",
                        "",
                        "Job Holder (Employee)", 
                        "Competence"+Environment.NewLine+"Score",
                        "Competence"+Environment.NewLine+"Gap",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 20, 200, 150, 80,
                        
                        //6-10
                        100, 20, 250, 80, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 9, 10 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 8, 9, 10 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 6, 7 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 6, 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePosCode), "A.PosCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By B.PosName;",
                        new string[]
                        {
                            //0
                            "PosCode",  
                            
                            //1-5
                            "PosName", "CompetenceName", "MinScore", "EmpCode", "EmpName",
                            
                            //6-7
                            "EmpScore", "CompetenceGap"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);

                            if (Sm.DrDec(dr, 7) < 0)
                            {
                                Grd.Rows[Row].BackColor = Color.Salmon;
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                            }
                            else
                            {
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                            }
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(3);
                Grd1.Group();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            SetGrd();
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }


        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmOrganizationalStructure(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mPosCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f1.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmOrganizationalStructure(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mPosCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f1.ShowDialog();
            }
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPosCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Position");
        }

        #endregion

        #endregion

    }
}
