﻿#region Update
/*
    16/06/2022 [IBL/PRODUCT] new apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptDebtSecuritiesSchedule : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            mBankAccountTypeForInvestment = string.Empty;
        internal string
            mDebtInvestmentCtCode = string.Empty;
        internal int
            mInterestFrequencyforDebtInvestment = 0;

        #endregion

        #region Constructor

        public FrmRptDebtSecuritiesSchedule(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDteCurrentDate(DteValuationDt);
                SetLueFinancialInstitutionCode(ref LueFinancialInstitution);
                SetLueBankAccountCode(ref LueBankAcCode);
                SetLueInvestmentType(ref LueInvestmentType, string.Empty);
                Sl.SetLueOption(ref LueInvestmentType, "InvestmentType");
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> {
                    TxtNominalAmt, TxtMovingAvgPrice, TxtInvestmentCost, TxtCouponInterestRate,
                    TxtAnnualDaysAssumption, TxtInterestFrequency, TxtAccruedInterest
                }, 0);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd2.Cols.Count = 8;
            Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd2, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Date",
                        "Nominal Amount",
                        "Interest Amount",
                        "Journal#",
                        "Voucher Doc#",

                        //6-7
                        "Coupon Schedule",
                        "Status",
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        100, 150, 150, 150, 150,

                        //6-7
                        150, 150
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 2, 3 }, 0);
            Sm.GrdFormatDate(Grd2, new int[] { 1 });
            Sm.GrdColReadOnly(true, false, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 }); ;
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd2, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd2, false);
            if (Sm.IsDteEmpty(DteValuationDt, "Valuation Date") ||
                Sm.IsTxtEmpty(TxtInvestmentCode, "Investment Code", false)
                ) return;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                var lAcquisitionDebtItem = new List<AcquisitionDebtItem>();

                Process1(ref lAcquisitionDebtItem);
                if (lAcquisitionDebtItem.Count > 0)
                    Process2(ref lAcquisitionDebtItem);
                else
                    Sm.StdMsg(mMsgType.NoData, "");
            }
            catch(Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'BankAccountTypeForInvestment','DebtInvestmentCtCode','InterestFrequencyforDebtInvestment' ");
            SQL.AppendLine(" );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //String
                            case "BankAccountTypeForInvestment": mBankAccountTypeForInvestment = ParValue; break;
                            case "DebtInvestmentCtCode": mDebtInvestmentCtCode = ParValue; break;

                            //int
                            case "InterestFrequencyforDebtInvestment":
                            if (ParValue.Length > 0)
                                    mInterestFrequencyforDebtInvestment = int.Parse(ParValue);
                            break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process1(ref List<AcquisitionDebtItem> lAcquisitionDebtItem)
        {
            DateTime StartDt = Sm.ConvertDate(Sm.GetDte(DteLastCouponDt)).AddDays(1);
            DateTime EndDt = Sm.ConvertDate(Sm.GetDte(DteMaturityDt));
            DateTime LastCouponDt = Sm.ConvertDate(Sm.GetDte(DteLastCouponDt));
            decimal mInterestRate= Convert.ToDecimal(TxtCouponInterestRate.Text) * 0.01m;
            decimal mNominalAmt = Convert.ToDecimal(TxtNominalAmt.Text);
            decimal mAnnualDays = Convert.ToDecimal(TxtAnnualDaysAssumption.Text);
            decimal mInterestAmt = mNominalAmt * (mInterestRate / mAnnualDays);
            string mCouponSchedule = string.Empty;

            for (DateTime date = StartDt; date <= EndDt; date = date.AddDays(1))
            {
                if (date == LastCouponDt.AddMonths(mInterestFrequencyforDebtInvestment))
                {
                    mCouponSchedule = "Coupon Schedule";
                    LastCouponDt = date;
                }
                else
                    mCouponSchedule = "";

                lAcquisitionDebtItem.Add(new AcquisitionDebtItem()
                {
                    Date = Sm.FormatDate(date),
                    NominalAmt = mNominalAmt,
                    InterestAmt = mInterestAmt,
                    JournalDocNo = string.Empty,
                    VoucherDocNo = string.Empty,
                    CouponSchedule = mCouponSchedule,
                    Status = string.Empty
                });
            }
        }

        private void Process2(ref List<AcquisitionDebtItem> lAcquisitionDebtItem)
        {
            int row = 0;
            decimal mAccruedInterest = 0m;
            Grd2.BeginUpdate();
            Grd2.Rows.Count = 0;
            foreach (var x in lAcquisitionDebtItem.OrderBy(o => o.Date))
            {
                Grd2.Rows.Add();
                Grd2.Cells[row, 0].Value = row + 1;
                Grd2.Cells[row, 1].Value = Sm.ConvertDate(x.Date);
                Grd2.Cells[row, 2].Value = x.NominalAmt;
                Grd2.Cells[row, 3].Value = x.InterestAmt;
                Grd2.Cells[row, 4].Value = x.JournalDocNo;
                Grd2.Cells[row, 5].Value = x.VoucherDocNo;
                Grd2.Cells[row, 6].Value = x.CouponSchedule;
                Grd2.Cells[row, 7].Value = (x.JournalDocNo != "" ? x.VoucherDocNo != "" ? "Paid" : "Accrued" : "Outstanding" );
                if (Sm.GetGrdStr(Grd2, row, 7) == "Accrued" && Sm.ConvertDate(x.Date) <= Sm.ConvertDate(Sm.GetDte(DteValuationDt)))
                    mAccruedInterest += x.InterestAmt;
                row += 1;
            }
            Grd2.EndUpdate();

            TxtAccruedInterest.EditValue = Sm.FormatNum(mAccruedInterest, 0);
        }

        private void SetLueFinancialInstitutionCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select SekuritasCode Col1, SekuritasName Col2 ");
                SQL.AppendLine("From TblInvestmentSekuritas; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueBankAccountCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select BankAcCode Col1, BankAcNm Col2 ");
                SQL.AppendLine("From TblBankAccount ");
                SQL.AppendLine("Where ActInd = 'Y' ");
                SQL.AppendLine("And Find_In_Set(BankAcTp, @BankAccountTypeForInvestment) ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@BankAccountTypeForInvestment", mBankAccountTypeForInvestment);
                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueInvestmentType(ref DXE.LookUpEdit Lue, string Code)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select InvestmentCtCode Col1, InvestmentCtName Col2 ");
                SQL.AppendLine("From TblInvestmentCategory ");
                if(Code.Length > 0 )
                    SQL.AppendLine("Where InvestmentCtCode = @Code ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (Code.Length > 0) Sm.SetLue(Lue, Code);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        private void Grd2_AfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd2, 0, 1, true);
        }

        private void Grd2_AfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd2.Font = new Font(
                    Grd2.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueFinancialInstitution_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueFinancialInstitution, new Sm.RefreshLue1(SetLueFinancialInstitutionCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkFinancialInstitution_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Financial Institution");
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(SetLueBankAccountCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkBankAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Investment Bank Account (RDN)");
        }

        private void BtnInvestmentCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmRptDebtSecuritiesScheduleDlg(this));
        }

        #endregion

        #endregion

        #region Class

        private class AcquisitionDebtItem
        {
            public string Date { get; set; }
            public decimal NominalAmt { get; set; }
            public decimal InterestAmt { get; set; }
            public string JournalDocNo { get; set; }
            public string VoucherDocNo { get; set; }
            public string CouponSchedule { get; set; }
            public string Status { get; set; }
        }

        #endregion
    }
}
