﻿#region Update
/*
    10/05/2017 [TKG] journal depresiasi asset untuk credit menggunakan account# depresiasi asset di master asset.
    25/10/2017 [TKG] memisahkan proses depresiasi asset dengan selisih kurs. 
    17/05/2018 [TKG] ubah journal. 
    24/05/2018 [HAR] nilai debet ambil dari coa di trx costcenter yang asset categorynya sesuai dgn asset yang sedang di depresiasikan.
    28/05/2018 [TKG] tambah validasi apabila no coa di cost center belum disetting. 
    17/07/2018 [TKG] tambah cost center saat journal (Custom for KMI).
    04/09/2018 [TKG] proses journal berdasarkan entity.
    13/10/2020 [TKG/SIER] ubah query IsDataInvalid3()
    09/06/2021 [IBL/ALL] tambah validasi setting journal
    02/07/2021 [IBL] Bug : ilangin @CCCode di proses save journal by entity
    03/02/2022 [RDA/PHT] : tambah field multi profit center berdasarkan param IsFicoUseMultiProfitCenterFilter
    10/02/2022 [IBL/PHT] : Filter multi profit center, jika pilih parent maka child2nya akan ikut terfilter.
    12/03/2022 [TKG/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    13/07/2022 [IBL/PHT] di method ProcessDataByCostCenter1() ditambah parameter IsAADJournalBasedOnAssetCategory. Agar tidak memvalidasi Depre Cost AcNo di master CC, krn jika param = Y, maka tidak melihat Depre Cost AcNo di master CC
    10/01/2023 [WED/PHT] penomoran journal berdasarkan parameter JournalDocNoFormat
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAAD : Form
    {
        #region Field

        private string mMenuCode= string.Empty, mMainCurCode = string.Empty, mJournalDocNoFormat = string.Empty;
        private List<String> mlProfitCenter = null;
        private bool
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsAADJournalBasedOnAssetCategory = false,
            mIsAADJournalBasedOnEntity = false,
            mIsJournalCostCenterEnabled = false,
            mIsJournalValidationAADEnabled = false,
            mIsFicoUseMultiProfitCenterFilter = false,
            mIsAllProfitCenterSelected = false;

        #endregion

        #region Constructor

        public FrmAAD(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAADJournalBasedOnAssetCategory', 'IsJournalCostCenterEnabled', 'IsAADJournalBasedOnEntity', 'IsJournalValidationAADEnabled', 'IsFicoUseMultiProfitCenterFilter', ");
            SQL.AppendLine("'IsClosingJournalBasedOnMultiProfitCenter', 'MainCurCode', 'JournalDocNoFormat' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "JournalDocNoFormat": mJournalDocNoFormat = ParValue; break;

                            //boolean
                            case "IsFicoUseMultiProfitCenterFilter": mIsFicoUseMultiProfitCenterFilter = ParValue == "Y"; break;
                            case "IsJournalValidationAADEnabled": mIsJournalValidationAADEnabled = ParValue == "Y"; break;
                            case "IsAADJournalBasedOnEntity": mIsAADJournalBasedOnEntity = ParValue == "Y"; break;
                            case "IsJournalCostCenterEnabled": mIsJournalCostCenterEnabled = ParValue == "Y"; break;
                            case "IsAADJournalBasedOnAssetCategory": mIsAADJournalBasedOnAssetCategory = ParValue == "Y"; break;
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }

            if (mJournalDocNoFormat.Length == 0) mJournalDocNoFormat = "1";
        }

        private bool IsProcessedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                IsDataInvalid1() ||
                IsDataInvalid2() ||
                IsDataInvalid3() ||
                IsJournalSettingInvalid() ||
                (mIsFicoUseMultiProfitCenterFilter && IsProfitCenterInvalid())
                ; 
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsJournalValidationAADEnabled) return false;

            var l = new List<ResultEntity>();
            var l2 = new List<ResultCostCenter>();
            var Mth = Sm.GetLue(LueMth);
            var Yr = Sm.GetLue(LueYr);

            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Table
            if (mIsAADJournalBasedOnEntity)
            {
                ProcessDataByEntity1(ref l, Mth, Yr);
                if (l.Count > 0)
                {
                    foreach (var i in l)
                        if(IsJournalSettingInvalid_Entity(i, Msg)) return true;
                }
                l.Clear();
                if (IsJournalSettingInvalid_Asset(Mth, Yr, Msg)) return true;
            }
            else
            {
                //cek untuk jurnal By Cost Center dan All Cost Center
                ProcessDataByCostCenter1(ref l2, Mth, Yr);
                if (l2.Count > 0)
                {
                    foreach (var i in l2)
                        if(IsJournalSettingInvalid_CostCenter(i, Mth, Yr, Msg)) return true;
                }
                l2.Clear();
            }

            return false;        
        }

        private bool IsJournalSettingInvalid_Entity(ResultEntity x, string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string EntName = string.Empty;

            SQL.AppendLine("Select EntName ");
            SQL.AppendLine("From TblEntity Where AcNo7 Is Null ");
            SQL.AppendLine("And EntCode = @EntCode; ");

            Sm.CmParam<String>(ref cm, "@EntCode", x.EntCode);

            cm.CommandText = SQL.ToString();
            EntName = Sm.GetValue(cm);

            if (EntName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Entity's COA account# (" + EntName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid_Asset(string Mth, string Yr    , string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Asset = string.Empty;

            SQL.AppendLine("Select Group_Concat(C.AssetCode Separator ', ') As Asset ");
            SQL.AppendLine("From TblDepreciationAssetHdr A ");
            SQL.AppendLine("Inner Join TblDepreciationAssetDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.JournalDocNo Is Null ");
            SQL.AppendLine("    And Right(Concat('0',B.Mth), 2)=@Mth ");
            SQL.AppendLine("    And B.Yr=@Yr ");
            SQL.AppendLine("Inner Join TblAsset C ");
            SQL.AppendLine("    On C.ActiveInd='Y' ");
            SQL.AppendLine("    And A.AssetCode=C.AssetCode ");
            SQL.AppendLine("    And C.AcNo2 Is Null ");
            SQL.AppendLine("    And C.CCCode Is Not Null ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("Limit 1;");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "Asset",
                });
                
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Asset = Sm.DrStr(dr, c[0]);
                        if (Asset.Length > 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Asset's COA account# (" + Sm.DrStr(dr, c[0]) + ") is empty.");
                            return true;
                        }
                    }
                }
                dr.Close();
            }

            return false;
        }

        private bool IsJournalSettingInvalid_CostCenter(ResultCostCenter x, string Mth, string Yr, string Msg) 
        {
            if (mIsAADJournalBasedOnAssetCategory) return false;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string CCName = string.Empty;

            SQL.AppendLine("Select Distinct(C.CCName) As CCName ");
            SQL.AppendLine("From TblDepreciationAssetHdr A ");
            SQL.AppendLine("Inner Join TblDepreciationAssetDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.JournalDocNo Is Null ");
            SQL.AppendLine("    And Right(Concat('0',B.Mth), 2)=@Mth ");
            SQL.AppendLine("    And B.Yr=@Yr ");
            SQL.AppendLine("Inner Join TblCostCenter C On B.CCCode=C.CCCode And (C.AcNo Is Null Or Length(C.AcNo) = 0) ");
            SQL.AppendLine("Inner Join TblAsset D On D.ActiveInd='Y' And A.AssetCode=D.AssetCode And D.CCCode Is Not Null And D.CCCode=@CCCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("Limit 1;");

            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@CCCode", x.CCCode);

            cm.CommandText = SQL.ToString();
            CCName = Sm.GetValue(cm);

            if (CCName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Cost Center's COA account# (" + CCName + ") is empty.");
                return true;
            }

            return false;
        }

        private bool IsDataInvalid1()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDepreciationAssetHdr A ");
            SQL.AppendLine("Inner Join TblDepreciationAssetDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.JournalDocNo Is Null ");
            SQL.AppendLine("    And Right(Concat('0',B.Mth), 2)=@Mth ");
            SQL.AppendLine("    And B.Yr=@Yr ");
            SQL.AppendLine("Inner Join TblAsset C On C.ActiveInd='Y' And A.AssetCode=C.AssetCode ");
            if (!mIsJournalValidationAADEnabled)
                SQL.AppendLine("And C.AcNo2 Is Not Null ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "There is no asset depreciation data (Master Asset) to be processed.");
                return true;
            }
            return false;
        }

        private bool IsDataInvalid2()
        {
            var SQL = new StringBuilder();

            if (mIsAADJournalBasedOnEntity)
            {
                SQL.AppendLine("Select 1 ");
                SQL.AppendLine("From TblDepreciationAssetHdr A ");
                SQL.AppendLine("Inner Join TblDepreciationAssetDtl B ");
                SQL.AppendLine("    On A.DocNo=B.DocNo ");
                SQL.AppendLine("    And B.JournalDocNo Is Null ");
                SQL.AppendLine("    And Right(Concat('0',B.Mth), 2)=@Mth ");
                SQL.AppendLine("    And B.Yr=@Yr ");
                SQL.AppendLine("Inner Join TblAsset C On C.ActiveInd='Y' And A.AssetCode=C.AssetCode And C.CCCode Is Not Null ");
                SQL.AppendLine("Inner Join TblCostCenter D On C.CCCode=D.CCCode And D.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("Inner Join TblProfitCenter E On D.ProfitCenterCode=E.ProfitCenterCode And E.EntCode Is Not Null ");
                SQL.AppendLine("Inner Join TblEntity F On E.EntCode=F.EntCode ");
                if (!mIsJournalValidationAADEnabled)
                    SQL.AppendLine("And F.AcNo7 Is Not Null ");
                SQL.AppendLine("Where A.CancelInd='N' ");
                SQL.AppendLine("Limit 1;");
            }
            else
            {
                if (mIsAADJournalBasedOnAssetCategory)
                {
                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblDepreciationAssetHdr A ");
                    SQL.AppendLine("Inner Join TblDepreciationAssetDtl B ");
                    SQL.AppendLine("    On A.DocNo=B.DocNo ");
                    SQL.AppendLine("    And B.JournalDocNo Is Null ");
                    SQL.AppendLine("    And Right(Concat('0',B.Mth), 2)=@Mth ");
                    SQL.AppendLine("    And B.Yr=@Yr ");
                    SQL.AppendLine("Inner Join TblCostCenter C On B.CCCode=C.CCCode  ");
                    SQL.AppendLine("Inner Join TblAsset D On D.ActiveInd='Y' And A.AssetCode=D.AssetCode  ");
                    SQL.AppendLine("Inner Join TblCostCenterDtl E On D.AssetcategoryCode = E.AssetcategoryCode And D.CCCode=E.CCCode  ");
                    SQL.AppendLine("Where A.CancelInd='N' ");
                    SQL.AppendLine("Limit 1;");
                }
                else
                {
                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblDepreciationAssetHdr A ");
                    SQL.AppendLine("Inner Join TblDepreciationAssetDtl B ");
                    SQL.AppendLine("    On A.DocNo=B.DocNo ");
                    SQL.AppendLine("    And B.JournalDocNo Is Null ");
                    SQL.AppendLine("    And Right(Concat('0',B.Mth), 2)=@Mth ");
                    SQL.AppendLine("    And B.Yr=@Yr ");
                    SQL.AppendLine("Inner Join TblCostCenter C On B.CCCode=C.CCCode ");
                    if (!mIsJournalValidationAADEnabled)
                        SQL.AppendLine("And C.AcNo Is Not Null ");
                    SQL.AppendLine("Inner Join TblAsset D On D.ActiveInd='Y' And A.AssetCode=D.AssetCode ");
                    SQL.AppendLine("Where A.CancelInd='N' ");
                    SQL.AppendLine("Limit 1;");
                }
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "There is no asset depreciation data (Asset category/Cost Center/Entity) to be processed.");
                return true;
            }
            return false;
        }

        private bool IsDataInvalid3()
        {
            if (!mIsAADJournalBasedOnAssetCategory) return false;
            if (mIsAADJournalBasedOnEntity) return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDepreciationAssetHdr A ");
            SQL.AppendLine("Inner Join TblDepreciationAssetDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.JournalDocNo Is Null ");
            SQL.AppendLine("    And Right(Concat('0',B.Mth), 2)=@Mth ");
            SQL.AppendLine("    And B.Yr=@Yr ");
            SQL.AppendLine("Inner Join TblCostCenter C On B.CCCode=C.CCCode  ");
            SQL.AppendLine("Inner Join TblAsset D On D.ActiveInd='Y' And A.AssetCode=D.AssetCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("    Select 1 From TblCostCenterDtl ");
            SQL.AppendLine("    Where AssetcategoryCode=D.AssetcategoryCode ");
            SQL.AppendLine("    And CCCode=D.CCCode  ");
            SQL.AppendLine("    And AcNo Is Null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            
            SQL.AppendLine("Limit 1;");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "There is one or more coa account in cost center hasn't been set.");
                return true;
            }
            return false;
        }

        #endregion

        #region Event

        private void FrmAAD_Load(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                if (mIsFicoUseMultiProfitCenterFilter)
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                else
                {
                    label2.Visible = false;
                    CcbProfitCenterCode.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No) return;
                if (IsProcessedDataNotValid()) return;

                var Dt = new DateTime(
                       Int32.Parse(Sm.GetLue(LueYr)),
                       Int32.Parse(Sm.GetLue(LueMth)),
                       1, 0, 0, 0
                       );
                Dt = Dt.AddMonths(1);
                Dt = Dt.AddDays(-1);
                string DocDt = Sm.Left(Sm.GenerateDateFormat(Dt), 8);

                if (Sm.IsClosingJournalInvalid(DocDt)) return;

                if(mIsFicoUseMultiProfitCenterFilter)
                    SetProfitCenter();

                Cursor.Current = Cursors.WaitCursor;

                var code1 = Sm.GetCode1ForJournalDocNo("FrmAAD", string.Empty, string.Empty, mJournalDocNoFormat);

                if (mIsAADJournalBasedOnEntity)
                {
                    ProcessDataByEntity(DocDt, code1);
                }
                else
                {
                    if (mIsJournalCostCenterEnabled)
                    {
                        if (!IsProcessDataByCostCenterCompleted(DocDt, code1)) 
                            return;
                    }
                    else
                        ProcessDataAllCostCenter(DocDt);
                }
                
                Sm.StdMsg(mMsgType.Info, "Process is completed.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #region All Cost center

        private void ProcessDataAllCostCenter(string DocDt)
        {
            var Mth = Sm.GetLue(LueMth);
            var Yr = Sm.GetLue(LueYr);

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Asset Depreciation On ', @Mth, ' ', @Yr), ");
            SQL.AppendLine("@MenuCode, Concat((Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ' (Asset Depreciation)'), ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAmt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            if (mIsAADJournalBasedOnAssetCategory)
            {
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.DepreciationValue As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblDepreciationAssetHdr A ");
                SQL.AppendLine("        Inner Join TblDepreciationAssetDtl B ");
                SQL.AppendLine("            On A.DocNo=B.DocNo ");
                SQL.AppendLine("            And B.JournalDocNo Is Null ");
                SQL.AppendLine("            And Right(Concat('0',B.Mth), 2)=@Mth ");
                SQL.AppendLine("            And B.Yr=@Yr ");
                SQL.AppendLine("        Inner Join TblCostCenter C On B.CCCode=C.CCCode  ");
                SQL.AppendLine("        Inner Join TblAsset D On D.ActiveInd='Y' And A.AssetCode=D.AssetCode  ");
                SQL.AppendLine("        Inner Join TblCostCenterDtl E On D.AssetcategoryCode = E.AssetcategoryCode And D.CCCode = E.CCCode  ");
                SQL.AppendLine("        Where A.CancelInd='N' ");
            }
            else
            {
                SQL.AppendLine("        Select C.AcNo, ");
                SQL.AppendLine("        B.DepreciationValue As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblDepreciationAssetHdr A ");
                SQL.AppendLine("        Inner Join TblDepreciationAssetDtl B ");
                SQL.AppendLine("            On A.DocNo=B.DocNo ");
                SQL.AppendLine("            And B.JournalDocNo Is Null ");
                SQL.AppendLine("            And Right(Concat('0',B.Mth), 2)=@Mth ");
                SQL.AppendLine("            And B.Yr=@Yr ");
                SQL.AppendLine("        Inner Join TblCostCenter C On B.CCCode=C.CCCode And C.AcNo Is Not Null ");
                SQL.AppendLine("        Inner Join TblAsset D On D.ActiveInd='Y' And A.AssetCode=D.AssetCode ");
                SQL.AppendLine("        Where A.CancelInd='N' ");
            }
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select C.AcNo2 As AcNo, 0.00 As DAmt, B.DepreciationValue As CAmt ");
            SQL.AppendLine("        From TblDepreciationAssetHdr A ");
            SQL.AppendLine("        Inner Join TblDepreciationAssetDtl B ");
            SQL.AppendLine("            On A.DocNo=B.DocNo ");
            SQL.AppendLine("            And B.JournalDocNo Is Null ");
            SQL.AppendLine("            And Right(Concat('0',B.Mth), 2)=@Mth ");
            SQL.AppendLine("            And B.Yr=@Yr ");
            SQL.AppendLine("        Inner Join TblAsset C On C.ActiveInd='Y' And A.AssetCode=C.AssetCode And C.AcNo2 Is Not Null ");
            SQL.AppendLine("        Where A.CancelInd='N' ");

            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            SQL.AppendLine("Update TblDepreciationAssetDtl A ");
            SQL.AppendLine("Inner Join TblDepreciationAssetHdr B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblAsset C On C.ActiveInd='Y' And B.AssetCode=C.AssetCode ");
            SQL.AppendLine("Set A.JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where A.JournalDocNo Is Null ");
            SQL.AppendLine("And Right(Concat('0', A.Mth), 2)=@Mth ");
            SQL.AppendLine("And A.Yr=@Yr;");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);

            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        #endregion

        #region By Cost center

        private bool IsProcessDataByCostCenterCompleted(string DocDt, string code1)
        {
            var l = new List<ResultCostCenter>();
            var Mth = Sm.GetLue(LueMth);
            var Yr = Sm.GetLue(LueYr);

            ProcessDataByCostCenter1(ref l, Mth, Yr);
            if (l.Count > 0)
            {
                if (mIsClosingJournalBasedOnMultiProfitCenter)
                {
                    var lProfitCenterCode = new List<string>();
                    GetProfitCenterCode(ref l, ref lProfitCenterCode);
                    foreach (var x in lProfitCenterCode)
                    {
                        if (Sm.IsClosingJournalInvalid(true, false, DocDt, x))
                            return false;
                    }
                    lProfitCenterCode.Clear();
                }
                foreach (var i in l)
                    ProcessDataByCostCenter2(DocDt, i, Mth, Yr, code1);
            }
            l.Clear();
            return true;
        }

        private void GetProfitCenterCode(ref List<ResultCostCenter> lCostCenter, ref List<string> lProfitCenterCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Query = new StringBuilder();
            int i = 0;

            foreach (var x in lCostCenter.Distinct())
            {
                Query.AppendLine(" Or CCCode=@CCCode_" + i.ToString());
                Sm.CmParam<String>(ref cm, "@CCCode_" + i.ToString(), x.CCCode);
                i++;
            }

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblCostCenter ");
            SQL.AppendLine("Where ProfitCenterCode Is Not Null And (CCCode='***' ");
            SQL.AppendLine(Query.ToString());
            SQL.AppendLine("); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read()) lProfitCenterCode.Add(Sm.DrStr(dr, c[0]));
                }
                dr.Close();
            }
        }

        private void ProcessDataByCostCenter1(ref List<ResultCostCenter> l, string Mth, string Yr)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select Distinct C.CCCode, D.ProfitCenterCode ");
            SQL.AppendLine("From TblDepreciationAssetDtl A ");
            SQL.AppendLine("Inner Join TblDepreciationAssetHdr B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblAsset C On C.ActiveInd='Y' And B.AssetCode=C.AssetCode And C.CCCode Is Not Null ");
            SQL.AppendLine("Inner Join TblCostCenter D On C.CCCode=D.CCCode ");
            if (!mIsAADJournalBasedOnAssetCategory)
                SQL.AppendLine("    And D.AcNo Is Not Null ");
            if (mIsFicoUseMultiProfitCenterFilter)
                SQL.AppendLine("Inner Join TblProfitCenter E ON D.ProfitCenterCode = E.ProfitCenterCode And D.ProfitCenterCode Is Not Null ");
            SQL.AppendLine("Where A.JournalDocNo Is Null ");
            SQL.AppendLine("And Right(Concat('0', A.Mth), 2)=@Mth ");
            SQL.AppendLine("And A.Yr=@Yr ");
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                SQL.AppendLine("    And E.ProfitCenterCode Is Not Null ");

                if (!mIsAllProfitCenterSelected)
                {
                    var Filter_2 = string.Empty;
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter_2.Length > 0) Filter_2 += " Or ";
                        Filter_2 += " (E.ProfitCenterCode=@ProfitCenter2_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter2_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter_2.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter_2 + ") ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And Find_In_Set(E.ProfitCenterCode, @ProfitCenterCode2) ");
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And E.ProfitCenterCode In ( ");
                        SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                        SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                }
            }

            SQL.AppendLine("Order By C.CCCode;");

            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "CCCode", "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(new ResultCostCenter(){ CCCode = Sm.DrStr(dr, c[0]), ProfitCenterCode = Sm.DrStr(dr, c[1]) });
                }
                dr.Close();
            }
        }

        private void ProcessDataByCostCenter2(string DocDt, ResultCostCenter i, string Mth, string Yr, string code1)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Asset Depreciation On ', @Mth, ' ', @Yr), ");
            SQL.AppendLine("@MenuCode, Concat((Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ' (Asset Depreciation)'), ");
            SQL.AppendLine("@CCCode, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt ");
            SQL.AppendLine("    From (");

            if (mIsAADJournalBasedOnAssetCategory)
            {
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.DepreciationValue As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblDepreciationAssetHdr A ");
                SQL.AppendLine("        Inner Join TblDepreciationAssetDtl B ");
                SQL.AppendLine("            On A.DocNo=B.DocNo ");
                SQL.AppendLine("            And B.JournalDocNo Is Null ");
                SQL.AppendLine("            And Right(Concat('0',B.Mth), 2)=@Mth ");
                SQL.AppendLine("            And B.Yr=@Yr ");
                SQL.AppendLine("        Inner Join TblCostCenter C On B.CCCode=C.CCCode  ");
                SQL.AppendLine("        Inner Join TblAsset D On D.ActiveInd='Y' And A.AssetCode=D.AssetCode And D.CCCode Is Not Null And D.CCCode=@CCCode ");
                SQL.AppendLine("        Inner Join TblCostCenterDtl E On D.AssetcategoryCode = E.AssetcategoryCode And D.CCCode = E.CCCode  ");
                SQL.AppendLine("        Where A.CancelInd='N' ");
            }
            else
            {
                SQL.AppendLine("        Select C.AcNo, ");
                SQL.AppendLine("        B.DepreciationValue As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblDepreciationAssetHdr A ");
                SQL.AppendLine("        Inner Join TblDepreciationAssetDtl B ");
                SQL.AppendLine("            On A.DocNo=B.DocNo ");
                SQL.AppendLine("            And B.JournalDocNo Is Null ");
                SQL.AppendLine("            And Right(Concat('0',B.Mth), 2)=@Mth ");
                SQL.AppendLine("            And B.Yr=@Yr ");
                SQL.AppendLine("        Inner Join TblCostCenter C On B.CCCode=C.CCCode And C.AcNo Is Not Null ");
                SQL.AppendLine("        Inner Join TblAsset D On D.ActiveInd='Y' And A.AssetCode=D.AssetCode And D.CCCode Is Not Null And D.CCCode=@CCCode ");
                SQL.AppendLine("        Where A.CancelInd='N' ");
            }
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select C.AcNo2 As AcNo, 0.00 As DAmt, B.DepreciationValue As CAmt ");
            SQL.AppendLine("        From TblDepreciationAssetHdr A ");
            SQL.AppendLine("        Inner Join TblDepreciationAssetDtl B ");
            SQL.AppendLine("            On A.DocNo=B.DocNo ");
            SQL.AppendLine("            And B.JournalDocNo Is Null ");
            SQL.AppendLine("            And Right(Concat('0',B.Mth), 2)=@Mth ");
            SQL.AppendLine("            And B.Yr=@Yr ");
            SQL.AppendLine("        Inner Join TblAsset C On C.ActiveInd='Y' And A.AssetCode=C.AssetCode And C.AcNo2 Is Not Null And C.CCCode Is Not Null And C.CCCode=@CCCode ");
            SQL.AppendLine("        Where A.CancelInd='N' ");

            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            SQL.AppendLine("Update TblDepreciationAssetDtl A ");
            SQL.AppendLine("Inner Join TblDepreciationAssetHdr B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblAsset C On C.ActiveInd='Y' And B.AssetCode=C.AssetCode And C.CCCode Is Not Null And C.CCCode=@CCCode ");
            SQL.AppendLine("Set A.JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where A.JournalDocNo Is Null ");
            SQL.AppendLine("And Right(Concat('0', A.Mth), 2)=@Mth ");
            SQL.AppendLine("And A.Yr=@Yr;");

            cm.CommandText = SQL.ToString();

            if (mJournalDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(DocDt, 1, code1, i.ProfitCenterCode, string.Empty, string.Empty, string.Empty)));
            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CCCode", i.CCCode);

            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        #endregion

        #region By Entity

        private void ProcessDataByEntity(string DocDt, string code1)
        {
            var l = new List<ResultEntity>();
            var Mth = Sm.GetLue(LueMth);
            var Yr = Sm.GetLue(LueYr);

            ProcessDataByEntity1(ref l, Mth, Yr);
            if (l.Count > 0)
            {
                foreach (var i in l)
                    ProcessDataByEntity2(DocDt, i, Mth, Yr, code1);
            }
            l.Clear();
        }

        private void ProcessDataByEntity1(ref List<ResultEntity> l, string Mth, string Yr)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct E.EntCode, F.EntName ");
            if (mJournalDocNoFormat == "1") SQL.AppendLine(", Null As ProfitCenterCode ");
            else SQL.AppendLine(", D.ProfitCenterCode ");
            SQL.AppendLine("From TblDepreciationAssetDtl A ");
            SQL.AppendLine("Inner Join TblDepreciationAssetHdr B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblAsset C On C.ActiveInd='Y' And B.AssetCode=C.AssetCode And C.CCCode Is Not Null ");
            SQL.AppendLine("Inner Join TblCostCenter D On C.CCCode=D.CCCode And D.ProfitCenterCode Is Not Null ");
            SQL.AppendLine("Inner Join TblProfitCenter E On D.ProfitCenterCode=E.ProfitCenterCode And E.EntCode Is Not Null ");
            SQL.AppendLine("Inner Join TblEntity F On E.EntCode=F.EntCode ");
            SQL.AppendLine("Where A.JournalDocNo Is Null ");
            SQL.AppendLine("And Right(Concat('0', A.Mth), 2)=@Mth ");
            SQL.AppendLine("And A.Yr=@Yr ");
            SQL.AppendLine("Order By E.EntCode;");

            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EntCode", "EntName", "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(new ResultEntity() { EntCode = Sm.DrStr(dr, c[0]), EntName = Sm.DrStr(dr, c[1]), ProfitCenterCode = Sm.DrStr(dr, c[2]) });
                }
                dr.Close();
            }
        }

        private void ProcessDataByEntity2(string DocDt, ResultEntity i, string Mth, string Yr, string code1)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Asset Depreciation Entity ', @EntName, ' On ', @Mth, ' ', @Yr), ");
            SQL.AppendLine("@MenuCode, Concat((Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ' (Asset Depreciation)'), ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt ");
            SQL.AppendLine("    From (");
            SQL.AppendLine("        Select F.AcNo7 As AcNo, ");
            SQL.AppendLine("        B.DepreciationValue As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("        From TblDepreciationAssetHdr A ");
            SQL.AppendLine("        Inner Join TblDepreciationAssetDtl B ");
            SQL.AppendLine("            On A.DocNo=B.DocNo ");
            SQL.AppendLine("            And B.JournalDocNo Is Null ");
            SQL.AppendLine("            And Right(Concat('0',B.Mth), 2)=@Mth ");
            SQL.AppendLine("            And B.Yr=@Yr ");
            SQL.AppendLine("        Inner Join TblAsset C On C.ActiveInd='Y' And A.AssetCode=C.AssetCode And C.CCCode Is Not Null ");
            SQL.AppendLine("        Inner Join TblCostCenter D On C.CCCode=D.CCCode And D.ProfitCenterCode Is Not Null ");
            SQL.AppendLine("        Inner Join TblProfitCenter E On D.ProfitCenterCode=E.ProfitCenterCode And E.EntCode Is Not Null ");
            SQL.AppendLine("        Inner Join TblEntity F On E.EntCode=F.EntCode And F.EntCode=@EntCode And F.AcNo7 Is Not Null ");
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select C.AcNo2 As AcNo, 0.00 As DAmt, B.DepreciationValue As CAmt ");
            SQL.AppendLine("        From TblDepreciationAssetHdr A ");
            SQL.AppendLine("        Inner Join TblDepreciationAssetDtl B ");
            SQL.AppendLine("            On A.DocNo=B.DocNo ");
            SQL.AppendLine("            And B.JournalDocNo Is Null ");
            SQL.AppendLine("            And Right(Concat('0',B.Mth), 2)=@Mth ");
            SQL.AppendLine("            And B.Yr=@Yr ");
            SQL.AppendLine("        Inner Join TblAsset C ");
            SQL.AppendLine("            On C.ActiveInd='Y' ");
            SQL.AppendLine("            And A.AssetCode=C.AssetCode ");
            SQL.AppendLine("            And C.AcNo2 Is Not Null ");
            SQL.AppendLine("            And C.CCCode Is Not Null ");
            SQL.AppendLine("        Inner Join TblCostCenter D On C.CCCode=D.CCCode And D.ProfitCenterCode Is Not Null ");
            SQL.AppendLine("        Inner Join TblProfitCenter E On D.ProfitCenterCode=E.ProfitCenterCode And E.EntCode Is Not Null And E.EntCode=@EntCode ");
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("    ) Tbl Where AcNo Is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            SQL.AppendLine("Update TblDepreciationAssetDtl A ");
            SQL.AppendLine("Inner Join TblDepreciationAssetHdr B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblAsset C On C.ActiveInd='Y' And B.AssetCode=C.AssetCode And C.CCCode Is Not Null ");
            SQL.AppendLine("Set A.JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where A.JournalDocNo Is Null ");
            SQL.AppendLine("And Right(Concat('0', A.Mth), 2)=@Mth ");
            SQL.AppendLine("And A.Yr=@Yr;");

            cm.CommandText = SQL.ToString();

            if (mJournalDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(DocDt, 1, code1, i.ProfitCenterCode, string.Empty, string.Empty, string.Empty)));
            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@EntCode", i.EntCode);
            Sm.CmParam<String>(ref cm, "@EntName", i.EntName);

            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        #endregion

        #endregion

        #region Additional Method

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Multi profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsFicoUseMultiProfitCenterFilter) return;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T1.ProfitCenterName As Col, T1.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T1 ");
            SQL.AppendLine("    WHERE Exists(  ");
            SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
            SQL.AppendLine("        Where ProfitCenterCode=T1.ProfitCenterCode  ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }
        #endregion

        #endregion

        #region Class

        private class ResultCostCenter
        {
            public string CCCode { get; set; }
            public string ProfitCenterCode { get; set; }
        }

        private class ResultEntity
        {
            public string EntCode { get; set; }
            public string EntName { get; set; }
            public string ProfitCenterCode { get; set; }
        }

        #endregion
    }
}
