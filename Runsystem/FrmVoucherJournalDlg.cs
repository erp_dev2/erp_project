﻿#region Update
/*
    05/07/2017 [TKG] tambah informasi vr, vr date, pic, dept, bank account
    28/05/2018 [HAR] tambah filter year dan month
    22/07/2020 [WED/SRN] tambah ExcRate dan Amt2 dari VoucherRequestHdr
    02/03/2021 [WED/IMS] tambah show SO Contract
    22/03/2021 [VIN/IMS] tambah VoucherJournalCCCodeSource
 *  12/04/2021 [HAR/PHT] VoucherJournalCCCodeSource : jika 1  maka tidak ambil costcenter dari department
 *  29/04/2021 [VIN/ALL] Voucher Remarks berasal dr Remark Header(VC) sj 
 *  24/11/2021 [TYO/AMKA] filter voucher by department group berdasarkan parameter mIsJournalVoucherFilteredByGroup
 *  22/04/2022 [BRI/PHT] date ambil dari voucher
 *  19/01/2023 [MAU/BBT] set otomatis amount ke parent berdasarkan bank & coa
 *  15/02/2023 [IBL/BBT] Bug set otomatis amt ke dtl : voucher bernilai credit, tapi saat di JV nominalnya masuk ke debit.
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherJournalDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherJournal mFrmParent;
        private string
            mSQL = string.Empty,
            mVoucherDocTypeManual = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherJournalDlg(FrmVoucherJournal FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e); this.Text = "List Of Voucher";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                SetGrd();
                SetSQL();
                GetParameter();
                Sl.SetLueYr(LueYr, mFrmParent.mAccountingRptStartFrom);
                Sl.SetLueMth(LueMth);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Voucher#", 
                        "Date",
                        "Description",
                        "Currency",
                        "Amount",
                        
                        //6-10
                        "Voucher"+Environment.NewLine+"Request#",
                        "Voucher"+Environment.NewLine+"Request Date",
                        "PIC",
                        "Department",
                        "Bank Account",

                        //11-15
                        "Entity Code",
                        "Entity Name",
                        "Remark",
                        "ExcRate",
                        "Amt2", 

                        //16-17
                        "Cost Center Code",
                        "Cost Center Name"

                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 250, 80, 130, 
                        
                        //6-10
                        120, 100, 150, 180, 300, 
                        
                        //11-15
                        0, 200, 250, 0, 0, 
                        
                        //16-17
                        100, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 14, 15 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 7 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 11, 12, 13, 14, 15, 16, 17 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.Description, A.CurCode, A.Amt, ");
            SQL.AppendLine("A.VoucherRequestDocNo, C.DocDt As VoucherRequestDocDt, ");
            SQL.AppendLine("D.UserName, E.DeptName, ");
            SQL.AppendLine("Concat( ");
            SQL.AppendLine("    Case When G.BankName Is Not Null Then Concat(G.BankName, ' : ') Else '' End, ");
            SQL.AppendLine("    Case When F.BankAcNo Is Not Null  ");
            SQL.AppendLine("    Then Concat(F.BankAcNo, ' [', IfNull(F.BankAcNm, ''), ']') ");
            SQL.AppendLine("    Else IfNull(F.BankAcNm, '') End ");
            SQL.AppendLine(") As BankAcDesc, ");
            SQL.AppendLine("A.EntCode, H.EntName, A.Remark, ");
            if (mFrmParent.mIsVoucherJournalUseCostCenter && mFrmParent.mVoucherJournalCCCodeSource == "2")
            {
                SQL.AppendLine("I.CCCode, I.CCName, ");
            }
            else
            {
                SQL.AppendLine("null As CCCode, null As CCName, ");
            }
            if (mFrmParent.mIsVRManualUseOtherCurrency)
                SQL.AppendLine("C.ExcRate, C.Amt2 ");
            else
                SQL.AppendLine("0.00 As ExcRate, 0.00 As Amt2 ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Left Join TblVoucherDtl B On A.DocNo=B.DocNo And B.DNo='001' ");
            SQL.AppendLine("left Join TblVoucherRequestHdr C On A.VoucherRequestDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblUser D On C.PIC=D.UserCode ");
            SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblBankAccount F On A.BankAcCode=F.BankAcCode ");
            SQL.AppendLine("Left Join TblBank G On F.BankCode=G.BankCode ");
            SQL.AppendLine("Left Join TblEntity H On A.EntCode=H.EntCode ");

            if (mFrmParent.mIsVoucherJournalUseCostCenter && mFrmParent.mVoucherJournalCCCodeSource == "2")
            {
                SQL.AppendLine("Left Join tblcostcenter I ON E.DeptCode=I.DeptCode ");
            }

            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And A.DocType=@VoucherDocTypeManual ");
            if (Sm.GetDte(DteDocDt1).Length > 0 && Sm.GetDte(DteDocDt2).Length > 0)
            {
                SQL.AppendLine("And (A.DocDt Between @DocDt1 And @DocDt2) ");
            }

            if (Sm.GetLue(LueYr).Length>0 && Sm.GetLue(LueMth).Length>0)
            {
                SQL.AppendLine("And left(A.DocDt, 6) = @YrMth ");
            }
            
            SQL.AppendLine("And A.DocNo Not In ( ");
            SQL.AppendLine("    Select VoucherDocNo From TblVoucherJournalHdr ");
            SQL.AppendLine("    Where CancelInd = 'N' ");
            SQL.AppendLine(") ");
            
            SQL.AppendLine("And A.VoucherJournalDocNo Is Null ");
            SQL.AppendLine("And A.AcNo Is Null ");

            if (mFrmParent.mIsJournalVoucherFilteredByGroup)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode = C.DeptCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (
                //Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                //Sm.IsDteEmpty(DteDocDt2, "End date") ||
                //Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)||
                
                IsFilterDateInvalid()||
                IsFilterPeriodInvalid()
                ) return;

            try
            {
                SetSQL();
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@VoucherDocTypeManual", mVoucherDocTypeManual);
                Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "Description", "CurCode", "Amt", "VoucherRequestDocNo",
                        
                        //6-10
                        "VoucherRequestDocDt", "UserName", "DeptName", "BankAcDesc", "EntCode",
                        
                        //11-15
                        "EntName", "Remark", "ExcRate", "Amt2", "CCCode", 

                        //16
                        "CCName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        public bool IsFilterDateInvalid()
        {
            if (Sm.GetDte(DteDocDt1).Length>0 && Sm.GetLue(LueMth).Length>0)
            {
                Sm.StdMsg(mMsgType.Warning, "You must filter by period or by month.");
                return true;
            }
            return false;
        }

        public bool IsFilterPeriodInvalid()
        {
            if (Sm.GetLue(LueMth).Length > 0 && Sm.GetLue(LueYr).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Year empty");
                return true;
            }

            if (Sm.GetLue(LueMth).Length == 0 && Sm.GetLue(LueYr).Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Month empty");
                return true;
            }
            return false;
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                int r = Grd1.CurRow.Index;
                string AcType = Sm.GetValue("Select AcType From TblVoucherHdr Where DocNo = @Param;", Sm.GetGrdStr(Grd1, r, 1));
                //Row1 = mFrmParent.Grd1.Rows.Count - 1;
                mFrmParent.TxtVoucherJournalDocNo.EditValue = null;
                mFrmParent.TxtVoucherDocNo.EditValue = Sm.GetGrdStr(Grd1, r, 1);
                Sm.SetDte(mFrmParent.DteDocDt, Sm.GetValue("Select DocDt From TblVoucherHdr Where DocNo = '" + Sm.GetGrdStr(Grd1, r, 1) + "'"));
                mFrmParent.TxtCurCode.EditValue = Sm.GetGrdStr(Grd1, r, 4);
                mFrmParent.TxtAmt.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 5), 0);
                mFrmParent.mEntCode = Sm.GetGrdStr(Grd1, r, 11);
                mFrmParent.TxtEntCode.EditValue = Sm.GetGrdStr(Grd1, r, 12);
                mFrmParent.MeeVoucherRemark.EditValue = Sm.GetGrdStr(Grd1, r, 13); 
                if (mFrmParent.mIsVRManualUseOtherCurrency)
                {
                    mFrmParent.TxtExcRate.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 14), 0);
                    mFrmParent.TxtAmt2.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 15), 0);
                }
                mFrmParent.SetVoucherBankAccountCOAAccount();
                if (mFrmParent.mDocDtVoucherJournalBasedOnVoucher)
                {
                    Sm.SetDte(mFrmParent.DteDocDt, Sm.GetValue("Select DocDt From TblVoucherHdr Where DocNo = '" + Sm.GetGrdStr(Grd1, r, 1) + "'"));
                }
                if (mFrmParent.mIsVoucherJournalUseCostCenter && mFrmParent.mVoucherJournalCCCodeSource == "2")
                {
                    mFrmParent.TxtCCCode.EditValue = Sm.GetGrdStr(Grd1, r, 16);
                    mFrmParent.TxtCCName.EditValue = Sm.GetGrdStr(Grd1, r, 17);
                }
                //Sm.ClearGrd(mFrmParent.Grd1, true);
                if (AcType == "D")
                {
                    mFrmParent.Grd1.Cells[0, 3].Value = mFrmParent.TxtAmt.Text;
                    mFrmParent.TxtDbt.Text = mFrmParent.TxtAmt.Text;
                }
                else
                {
                    mFrmParent.Grd1.Cells[0, 4].Value = mFrmParent.TxtAmt.Text;
                    mFrmParent.TxtCdt.Text = mFrmParent.TxtAmt.Text;
                }
                mFrmParent.ComputeBalanced();
                mFrmParent.ShowSOContractData();
                this.Close();
            }
        }

        private void GetParameter()
        {
            mVoucherDocTypeManual = Sm.GetParameter("VoucherDocTypeManual");
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.FilterDteSetCheckEdit2(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.FilterDteSetCheckEdit(this, sender);
        }
        #endregion

       

       

       
        #endregion

    }
}
