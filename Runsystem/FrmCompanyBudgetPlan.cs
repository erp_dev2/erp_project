#region Update
/*
    16/01/2020 [WED/VIR] new apps --> RKAP (Rencana Kerja Anggaran Perusahaan)
    12/08/2022 [ICA/SIER] tambah import data
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmCompanyBudgetPlan : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        private bool mIsEntityMandatory = false;
        private int[] mInputAmt = { 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28 };
        private int[] mPrevAmt = { 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29 };
        internal FrmCompanyBudgetPlanFind FrmFind;

        #endregion

        #region Constructor

        public FrmCompanyBudgetPlan(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                SetFormControl(mState.View);
                GetParameter();
                if (mIsEntityMandatory)
                    LblEntity.ForeColor = Color.Red;
                else
                    LblEntity.ForeColor = Color.Black;
                SetLueEntCode(ref LueEntCode);
                Sl.SetLueYr(LueYr, "");
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 31;
            Grd1.FrozenArea.ColCount = 5;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "Account",
                    //1-5
                    "Account#", "Alias", "Allow", "Total", "Prev Total",
                    //6-10
                    "01", "Prev 01", "02", "Prev 02", "03",
                    //11-15
                    "Prev 03", "04", "Prev 04", "05", "Prev 05",
                    //16-20
                    "06", "Prev 06", "07", "Prev 07", "08", 
                    //21-25
                    "Prev 08", "09", "Prev 09", "10", "Prev 10",
                    //26-30
                    "11", "Prev 11", "12", "Prev 12", "Saved Total"
                },
                new int[] 
                {
                    600,
                    150, 150, 20, 150, 150, 
                    150, 150, 150, 150, 150, 
                    150, 150, 150, 150, 150, 
                    150, 150, 150, 150, 150, 
                    150, 150, 150, 150, 150, 
                    150, 150, 150, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 30 }, 0);
            Sm.GrdFormatDec(Grd1, mInputAmt, 0);
            Sm.GrdFormatDec(Grd1, mPrevAmt, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 30 });
            Sm.GrdColReadOnly(Grd1, mPrevAmt);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 30 });
            Sm.GrdColInvisible(Grd1, mPrevAmt);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, LueYr, LueEntCode, MeeRemark
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnShowAc.Enabled = false;
                    Grd1.ReadOnly = true;
                    LueYr.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueYr, MeeRemark, LueEntCode }, false);
                    Grd1.ReadOnly = false;
                    BtnShowAc.Enabled = true;
                    LueYr.Focus();
                    break;
                case mState.Edit:
                    Grd1.ReadOnly = false;
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNo, DteDocDt, LueYr, LueEntCode, MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 5, 30 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, mInputAmt);
            Sm.SetGrdNumValueZero(ref Grd1, 0, mPrevAmt);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(mInputAmt, e.ColIndex))
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 3) == "Y")
                {
                    ComputeAmt(e.RowIndex, e.ColIndex, mInputAmt);
                }
                else
                {
                    Sm.StdMsg(mMsgType.Info, "You can't input amount for this account");
                }
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                if (Sm.IsGrdColSelected(mInputAmt, e.ColIndex))
                {
                    //e.DoDefault = false;
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCompanyBudgetPlanFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueYr, Sm.ServerCurrentDateTime().Substring(0, 4));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            if (ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "You can't edit this data." + Environment.NewLine +
                    "This data already cancelled.");
                return;
            }
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData();
                }
                else
                {
                    UpdateData();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsDataNotValid())
                return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "CompanyBudgetPlan", "TblCompanyBudgetPlanHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveCompanyBudgetPlanHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (
                    Sm.GetGrdStr(Grd1, Row, 0).Length > 0 &&
                    Sm.GetGrdDec(Grd1, Row, 4) != 0m
                    )
                    cml.Add(SaveCompanyBudgetPlanDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsGrdEmpty() ||
                IsDocAlreadyCreated()
                ;
        }

        private bool IsDocAlreadyCreated()
        {
            var EntCode = Sm.GetLue(LueEntCode);
            var SQL = new StringBuilder();
            SQL.AppendLine("Select 1 From TblCompanyBudgetPlanHdr ");
            SQL.AppendLine("Where Yr=@Yr ");
            SQL.AppendLine("And CancelInd='N' ");
            if (mIsEntityMandatory)
                SQL.AppendLine(" And EntCode Is Not Null And EntCode=@EntCode ");
            SQL.AppendLine(";");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            if (mIsEntityMandatory) Sm.CmParam<String>(ref cm, "@EntCode", EntCode);

            if (Sm.IsDataExist(cm))
            {
                var Msg = string.Empty;

                Msg = ("Year : " + Sm.GetLue(LueYr) + Environment.NewLine);
                if (EntCode.Length > 0) Msg += ("Entity : " + LueEntCode.GetColumnValue("Col2") + Environment.NewLine);
                Msg += (Environment.NewLine + "Data has been created.");

                Sm.StdMsg(mMsgType.Warning, Msg);
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveCompanyBudgetPlanHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            string CurrentDateTime = Sm.ServerCurrentDateTime();

            SQL.AppendLine("Insert Into TblCompanyBudgetPlanHdr(DocNo, DocDt, Yr, CancelInd, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Yr, 'N', @EntCode, @Remark, @CreateBy, @CreateDt); ");

            SQL.AppendLine("Insert Into TblCompanyBudgetPlanDtl(DocNo, AcNo, Amt, Amt01, Amt02, Amt03, Amt04, Amt05, Amt06, Amt07, Amt08, Amt09, Amt10, Amt11, Amt12, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, AcNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, @CreateBy, @CreateDt ");
            SQL.AppendLine("From TblCOA ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("Where AcNo In ( ");
                SQL.AppendLine("    Select AcNo From TblCOADtl ");
                SQL.AppendLine("    Where EntCode Is Not Null ");
                SQL.AppendLine("    And EntCode=@EntCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CreateDt", CurrentDateTime);

            return cm;
        }

        private MySqlCommand SaveCompanyBudgetPlanDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCompanyBudgetPlanDtl Set ");
            SQL.AppendLine("    Amt=@Amt, ");
            SQL.AppendLine("    Amt01=@Amt01, ");
            SQL.AppendLine("    Amt02=@Amt02, ");
            SQL.AppendLine("    Amt03=@Amt03, ");
            SQL.AppendLine("    Amt04=@Amt04, ");
            SQL.AppendLine("    Amt05=@Amt05, ");
            SQL.AppendLine("    Amt06=@Amt06, ");
            SQL.AppendLine("    Amt07=@Amt07, ");
            SQL.AppendLine("    Amt08=@Amt08, ");
            SQL.AppendLine("    Amt09=@Amt09, ");
            SQL.AppendLine("    Amt10=@Amt10, ");
            SQL.AppendLine("    Amt11=@Amt11, ");
            SQL.AppendLine("    Amt12=@Amt12 ");
            SQL.AppendLine("Where DocNo=@DocNo And AcNo=@AcNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Amt01", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Amt02", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Amt03", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt04", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Amt05", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Amt06", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Amt07", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Amt08", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@Amt09", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<Decimal>(ref cm, "@Amt10", Sm.GetGrdDec(Grd1, Row, 24));
            Sm.CmParam<Decimal>(ref cm, "@Amt11", Sm.GetGrdDec(Grd1, Row, 26));
            Sm.CmParam<Decimal>(ref cm, "@Amt12", Sm.GetGrdDec(Grd1, Row, 28));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void UpdateData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditCompanyBudgetPlanHdr());
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (
                    Sm.GetGrdStr(Grd1, Row, 0).Length > 0 &&
                    Sm.GetGrdDec(Grd1, Row, 4) != Sm.GetGrdDec(Grd1, Row, 30)
                    )
                    cml.Add(EditCompanyBudgetPlanDtl(Row));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocAlreadyCancel();
        }

        private bool IsDocAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblCompanyBudgetPlanHdr Where DocNo=@DocNo And CancelInd='Y';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancel.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditCompanyBudgetPlanHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblCompanyBudgetPlanHdr Set " +
                    "   CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand EditCompanyBudgetPlanDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCompanyBudgetPlanDtl Set ");
            SQL.AppendLine("    Amt=@Amt, ");
            SQL.AppendLine("    Amt01=@Amt01, ");
            SQL.AppendLine("    Amt02=@Amt02, ");
            SQL.AppendLine("    Amt03=@Amt03, ");
            SQL.AppendLine("    Amt04=@Amt04, ");
            SQL.AppendLine("    Amt05=@Amt05, ");
            SQL.AppendLine("    Amt06=@Amt06, ");
            SQL.AppendLine("    Amt07=@Amt07, ");
            SQL.AppendLine("    Amt08=@Amt08, ");
            SQL.AppendLine("    Amt09=@Amt09, ");
            SQL.AppendLine("    Amt10=@Amt10, ");
            SQL.AppendLine("    Amt11=@Amt11, ");
            SQL.AppendLine("    Amt12=@Amt12, ");
            SQL.AppendLine("    LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And AcNo=@AcNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Amt01", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Amt02", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Amt03", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt04", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Amt05", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Amt06", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Amt07", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Amt08", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@Amt09", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<Decimal>(ref cm, "@Amt10", Sm.GetGrdDec(Grd1, Row, 24));
            Sm.CmParam<Decimal>(ref cm, "@Amt11", Sm.GetGrdDec(Grd1, Row, 26));
            Sm.CmParam<Decimal>(ref cm, "@Amt12", Sm.GetGrdDec(Grd1, Row, 28));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowCompanyBudgetPlanHdr(DocNo);
                ShowAccount();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowCompanyBudgetPlanHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.Yr, A.CancelInd, A.EntCode, A.Remark " +
                    "From TblCompanyBudgetPlanHdr A " +
                    "Where A.DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "Yr", "CancelInd", "EntCode", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[2]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[4]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    }, true
                );
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
        }

        private void ComputeAmt(int Row, int Col, int[] Cols)
        {
            int r = Row;
            var AcNo = Sm.GetGrdStr(Grd1, r, 1);
            var Amt = Sm.GetGrdDec(Grd1, r, Col);
            var AmtPrev = Sm.GetGrdDec(Grd1, r, (Col + 1));
            Grd1.Cells[r, (Col + 1)].Value = Amt;
            var l = new List<string>();
            var Pos = 1;

            while (Pos > 0)
            {
                Pos = AcNo.LastIndexOf(".");
                if (Pos > 0)
                {
                    AcNo = AcNo.Substring(0, Pos);
                    l.Add(AcNo);
                }
            }

            //calculate row's total
            decimal mTotalR = 0m;
            for (int i = 0; i < Cols.Length; ++i)
            {
                mTotalR += Sm.GetGrdDec(Grd1, Row, Cols[i]);
            }

            Grd1.Cells[Row, 4].Value = mTotalR;
            Grd1.Cells[Row, 5].Value = Sm.GetGrdDec(Grd1, Row, 5);

            // calculate parent
            foreach (var x in l)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, i, 1), x))
                    {
                        decimal mTotalP = 0m;

                        Grd1.Cells[i, Col].Value = Sm.GetGrdDec(Grd1, i, Col) - AmtPrev + Amt;
                        Grd1.Cells[i, (Col + 1)].Value = Sm.GetGrdDec(Grd1, i, (Col + 1));

                        for (int k = 0; k < Cols.Length; ++k)
                        {
                            mTotalP += Sm.GetGrdDec(Grd1, i, Cols[k]);
                        }

                        Grd1.Cells[i, 4].Value = mTotalP;
                        Grd1.Cells[i, 5].Value = Sm.GetGrdDec(Grd1, i, 5);
                        break;
                    }
                }
            }
        }

        internal void SetLueEntCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select 'Consolidate' As Col1, 'CONSOLIDATE' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 ");
            SQL.AppendLine("From TblEntity T Where ActInd='Y' ");
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ShowAccount()
        {
            int Level = 0;
            int PrevLevel = -1;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var EntCode = Sm.GetLue(LueEntCode);

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    if (TxtDocNo.Text.Length == 0)
                    {
                        SQL.AppendLine("Select A.Level, Concat(A.AcNo, ' ', A.AcDesc) As Account, ");
                        SQL.AppendLine("A.AcNo, A.Alias, 0.00 As Amt, 0.00 As Amt01, ");
                        SQL.AppendLine("0.00 As Amt02, 0.00 As Amt03, ");
                        SQL.AppendLine("0.00 As Amt04, 0.00 As Amt05, ");
                        SQL.AppendLine("0.00 As Amt06, 0.00 As Amt07, ");
                        SQL.AppendLine("0.00 As Amt08, 0.00 As Amt09, ");
                        SQL.AppendLine("0.00 As Amt10, 0.00 As Amt11, ");
                        SQL.AppendLine("0.00 As Amt12, ");
                        SQL.AppendLine("If(B.AcNo Is Null, 'N', 'Y') As Allow ");
                        SQL.AppendLine("From TblCOA A ");
                        SQL.AppendLine("Left Join ( ");
                        SQL.AppendLine("    Select AcNo From TblCOA ");
                        SQL.AppendLine("    Where AcNo Not In (");
                        SQL.AppendLine("        Select Parent From TblCoa ");
                        SQL.AppendLine("        Where Parent Is Not Null ");
                        SQL.AppendLine("        ) ");
                        if (EntCode.Length > 0 && (mIsEntityMandatory && EntCode != "Consolidate"))
                        {
                            SQL.AppendLine("    And AcNo In ( ");
                            SQL.AppendLine("        Select AcNo From TblCOADtl ");
                            SQL.AppendLine("        Where EntCode Is Not Null ");
                            SQL.AppendLine("        And EntCode=@EntCode ");
                            SQL.AppendLine("        ) ");
                        }
                        SQL.AppendLine(") B On A.AcNo=B.AcNo ");
                        SQL.AppendLine("Where A.ActInd='Y' ");
                        if (EntCode.Length > 0 && EntCode != "Consolidate") //&& (mIsEntityMandatory && EntCode != "Consolidate"))
                        {
                            SQL.AppendLine("And A.AcNo In ( ");
                            SQL.AppendLine("    Select AcNo From TblCOADtl ");
                            SQL.AppendLine("    Where EntCode Is Not Null ");
                            SQL.AppendLine("    And EntCode=@EntCode) ");
                        }
                        SQL.AppendLine("Order by A.AcNo;");

                        cm.CommandText = SQL.ToString();
                        if (EntCode.Length > 0)
                            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
                    }
                    else
                    {
                        SQL.AppendLine("Select B.Level, Concat(A.AcNo, ' ', B.AcDesc) As Account, ");
                        SQL.AppendLine("A.AcNo, B.Alias, A.Amt, A.Amt01, ");
                        SQL.AppendLine("A.Amt02, A.Amt03, ");
                        SQL.AppendLine("A.Amt04, A.Amt05, ");
                        SQL.AppendLine("A.Amt06, A.Amt07, ");
                        SQL.AppendLine("A.Amt08, A.Amt09, ");
                        SQL.AppendLine("A.Amt10, A.Amt11, ");
                        SQL.AppendLine("A.Amt12, ");
                        SQL.AppendLine("If(C.AcNo Is Null, 'N', 'Y') As Allow ");
                        SQL.AppendLine("From TblCompanyBudgetPlanDtl A ");
                        SQL.AppendLine("Inner Join TblCOA B On A.AcNo=B.AcNo ");
                        SQL.AppendLine("Left Join ( ");
                        SQL.AppendLine("   Select AcNo From TblCoa ");
                        SQL.AppendLine("   Where Acno Not In (Select Parent From TblCoa Where Parent Is Not Null) ");
                        SQL.AppendLine(") C On B.Acno=C.AcNo ");
                        SQL.AppendLine("Where DocNo=@DocNo ");
                        SQL.AppendLine("Order By AcNo;");

                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    }
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        "Level", 
                        "Account", "AcNo", "Alias", "Allow", "Amt",
                        "Amt01", "Amt02", "Amt03", "Amt04", "Amt05",
                        "Amt06", "Amt07", "Amt08", "Amt09", "Amt10",
                        "Amt11", "Amt12"
                    });
                    if (dr.HasRows)
                    {
                        int Row = 0;
                        Grd1.Rows.Count = 0;
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Level = int.Parse(Sm.DrStr(dr, 0));
                            //Level = Sm.DrStr(dr, 1).Length - ((Sm.DrStr(dr, 1).Length + 1) / 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 4);
                            if (TxtDocNo.Text.Length == 0)
                            {
                                Grd1.Cells[Row, 4].Value = 0m;
                                Grd1.Cells[Row, 5].Value = 0m;
                                Grd1.Cells[Row, 6].Value = 0m;
                                Grd1.Cells[Row, 7].Value = 0m;
                                Grd1.Cells[Row, 8].Value = 0m;
                                Grd1.Cells[Row, 9].Value = 0m;
                                Grd1.Cells[Row, 10].Value = 0m;
                                Grd1.Cells[Row, 11].Value = 0m;
                                Grd1.Cells[Row, 12].Value = 0m;
                                Grd1.Cells[Row, 13].Value = 0m;
                                Grd1.Cells[Row, 14].Value = 0m;
                                Grd1.Cells[Row, 15].Value = 0m;
                                Grd1.Cells[Row, 16].Value = 0m;
                                Grd1.Cells[Row, 17].Value = 0m;
                                Grd1.Cells[Row, 18].Value = 0m;
                                Grd1.Cells[Row, 19].Value = 0m;
                                Grd1.Cells[Row, 20].Value = 0m;
                                Grd1.Cells[Row, 21].Value = 0m;
                                Grd1.Cells[Row, 22].Value = 0m;
                                Grd1.Cells[Row, 23].Value = 0m;
                                Grd1.Cells[Row, 24].Value = 0m;
                                Grd1.Cells[Row, 25].Value = 0m;
                                Grd1.Cells[Row, 26].Value = 0m;
                                Grd1.Cells[Row, 27].Value = 0m;
                                Grd1.Cells[Row, 28].Value = 0m;
                                Grd1.Cells[Row, 29].Value = 0m;
                                Grd1.Cells[Row, 30].Value = 0m;
                            }
                            else
                            {
                                Grd1.Cells[Row, 4].Value = Sm.DrDec(dr, c[5]);
                                Grd1.Cells[Row, 5].Value = Sm.DrDec(dr, c[5]);
                                Grd1.Cells[Row, 6].Value = Sm.DrDec(dr, c[6]);
                                Grd1.Cells[Row, 7].Value = Sm.DrDec(dr, c[6]);
                                Grd1.Cells[Row, 8].Value = Sm.DrDec(dr, c[7]);
                                Grd1.Cells[Row, 9].Value = Sm.DrDec(dr, c[7]);
                                Grd1.Cells[Row, 10].Value = Sm.DrDec(dr, c[8]);
                                Grd1.Cells[Row, 11].Value = Sm.DrDec(dr, c[8]);
                                Grd1.Cells[Row, 12].Value = Sm.DrDec(dr, c[9]);
                                Grd1.Cells[Row, 13].Value = Sm.DrDec(dr, c[9]);
                                Grd1.Cells[Row, 14].Value = Sm.DrDec(dr, c[10]);
                                Grd1.Cells[Row, 15].Value = Sm.DrDec(dr, c[10]);
                                Grd1.Cells[Row, 16].Value = Sm.DrDec(dr, c[11]);
                                Grd1.Cells[Row, 17].Value = Sm.DrDec(dr, c[11]);
                                Grd1.Cells[Row, 18].Value = Sm.DrDec(dr, c[12]);
                                Grd1.Cells[Row, 19].Value = Sm.DrDec(dr, c[12]);
                                Grd1.Cells[Row, 20].Value = Sm.DrDec(dr, c[13]);
                                Grd1.Cells[Row, 21].Value = Sm.DrDec(dr, c[13]);
                                Grd1.Cells[Row, 22].Value = Sm.DrDec(dr, c[14]);
                                Grd1.Cells[Row, 23].Value = Sm.DrDec(dr, c[14]);
                                Grd1.Cells[Row, 24].Value = Sm.DrDec(dr, c[15]);
                                Grd1.Cells[Row, 25].Value = Sm.DrDec(dr, c[15]);
                                Grd1.Cells[Row, 26].Value = Sm.DrDec(dr, c[16]);
                                Grd1.Cells[Row, 27].Value = Sm.DrDec(dr, c[16]);
                                Grd1.Cells[Row, 28].Value = Sm.DrDec(dr, c[17]);
                                Grd1.Cells[Row, 29].Value = Sm.DrDec(dr, c[17]);
                                Grd1.Cells[Row, 30].Value = Sm.DrDec(dr, c[5]);
                            }

                            if (Sm.GetGrdStr(Grd1, Row, 3) == "N")
                            {
                                Grd1.Rows[Row].ReadOnly = iGBool.True;
                                Grd1.Rows[Row].BackColor = Color.FromArgb(224, 224, 224);
                            }
                            Grd1.Rows[Row].Level = Level;
                            if (Row > 0)
                                Grd1.Rows[Row - 1].TreeButton =
                                    (PrevLevel >= Level) ? //(PrevAcNo.Length >= Sm.DrStr(dr, 2).Length) ?
                                        iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                            PrevLevel = Level; //Sm.DrStr(dr, 2);
                            Row++;
                        }
                    }
                    Grd1.TreeLines.Visible = true;
                    Grd1.EndUpdate();
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #region Import CSV

        private void ProcessImport()
        {
            string FileName = string.Empty, AcType = string.Empty;
            var l = new List<COACSV>();

            FileName = OpenFileDialog();
            if (FileName.Length == 0 || FileName == "openFileDialog1") return;
            Sm.ClearGrd(Grd1, true);
            ShowAccount();
            ReadFileToList(ref l, FileName);

            if (l.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 3) == "Y")
                    {
                        foreach (var x in l
                            .OrderBy(y => y.AcNo)
                            .Where(z => z.AcNo == Sm.GetGrdStr(Grd1, i, 1))
                            )
                        {
                            decimal[] Amt = { x.Amt, x.Amt2, x.Amt3, x.Amt4, x.Amt5, x.Amt6, x.Amt7, x.Amt8, x.Amt9, x.Amt10, x.Amt11, x.Amt12 };
                            int col = 0;
                            foreach(var c in mInputAmt)
                            {
                                Grd1.Cells[i, c].Value = Amt[col];
                                if (Amt[col] > 0) ComputeAmt(i, c, mInputAmt);
                                col += 1;
                            }
                            break;
                        }
                    }
                }
            }

            l.Clear();
        }

        private string OpenFileDialog()
        {
            OD.InitialDirectory = "c:";
            OD.Filter = "CSV files (*.csv)|*.CSV";
            OD.FilterIndex = 2;
            OD.ShowDialog();

            return OD.FileName;
        }

        private void ReadFileToList(ref List<COACSV> l, string FileName)
        {
            bool IsFirst = true;

            using (var rd = new StreamReader(FileName))
            {
                int mRow = 0;

                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits.Count() != 13)
                    {
                        if (splits.Count() > 13)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                        }
                        else if (splits.Count() < 13)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Make sure this data " + splits[0].Trim() + " have a correct column count data.");
                        }

                        l.Clear();
                        return;
                    }
                    else
                    {
                        if (IsFirst) IsFirst = false;
                        else
                        {
                            mRow += 1;

                            var arr = splits.ToArray();
                            if (arr[0].Trim().Length > 0)
                            {
                                l.Add(new COACSV()
                                {
                                    AcNo = splits[0].Trim(),
                                    Amt = Decimal.Parse(splits[1].Trim()),
                                    Amt2 = Decimal.Parse(splits[2].Trim()),
                                    Amt3 = Decimal.Parse(splits[3].Trim()),
                                    Amt4 = Decimal.Parse(splits[4].Trim()),
                                    Amt5 = Decimal.Parse(splits[5].Trim()),
                                    Amt6 = Decimal.Parse(splits[6].Trim()),
                                    Amt7 = Decimal.Parse(splits[7].Trim()),
                                    Amt8 = Decimal.Parse(splits[8].Trim()),
                                    Amt9 = Decimal.Parse(splits[9].Trim()),
                                    Amt10 = Decimal.Parse(splits[10].Trim()),
                                    Amt11 = Decimal.Parse(splits[11].Trim()),
                                    Amt12 = Decimal.Parse(splits[12].Trim()),
                                });
                            }
                            else
                            {
                                Sm.StdMsg(mMsgType.Warning, "No data at row#" + mRow.ToString());
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Clicks

        private void BtnImport_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (IsGrdEmpty()) return;
                ProcessImport();
            }
        }

        private void BtnShowAc_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueYr, "Year") && !(mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity")))
                ShowAccount();
        }

        #endregion

        #endregion

        #region Class 

        private class COACSV
        {
            public string AcNo { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Amt3{ get; set; }
            public decimal Amt4 { get; set; }
            public decimal Amt5 { get; set; }
            public decimal Amt6 { get; set; }
            public decimal Amt7 { get; set; }
            public decimal Amt8 { get; set; }
            public decimal Amt9 { get; set; }
            public decimal Amt10 { get; set; }
            public decimal Amt11 { get; set; }
            public decimal Amt12 { get; set; }
        }

        #endregion
    }
}
