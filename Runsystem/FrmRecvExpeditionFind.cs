﻿#region Update
/*
    01/02/2020 [TKG/IMS] New Application
    18/12/2020 [WED/IMS] tambah informasi status
    04/08/2021 [vin/IMS] PO service dapat ditarik 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRecvExpeditionFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmRecvExpedition mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsInventoryShowTotalQty = false;

        #endregion

        #region Constructor

        public FrmRecvExpeditionFind(FrmRecvExpedition FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueVdCode(ref LueVdCode);
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsInventoryShowTotalQty = Sm.GetParameterBoo("IsInventoryShowTotalQty");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.* From( ");

            SQL.AppendLine("Select A.DocNo,  A.DocDt, B.CancelInd, A.LocalDocNo, A.VdCode, A.WhsCode,  ");
            SQL.AppendLine("Case B.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' Else '' End As StatusDesc, ");
            SQL.AppendLine("E.WhsName, D.VdName, A.VdDONo, ifnull(N.DocNo, B.PODocNo)PODocNo, ");
            SQL.AppendLine("B.ItCode, C.ItName, C.ItCodeInternal, C.Specification, B.Source, B.BatchNo, B.Lot, B.Bin, ");
            SQL.AppendLine("B.QtyPurchase, C.PurchaseUomCode, B.Qty, C.InventoryUomCode, B.Qty2, C.InventoryUomCode2, B.Qty3, C.InventoryUomCode3, ");
            SQL.AppendLine("L.ProjectCode, L.ProjectName, K.PONo, ");
            SQL.AppendLine("A.Remark As RemarkH, B.Remark As RemarkD, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, B.LastUpBy, B.LastUpDt ");
            SQL.AppendLine("From TblRecvExpeditionHdr A ");
            SQL.AppendLine("Inner Join TblRecvExpeditionDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblVendor D On A.VdCode=D.VdCode ");
            SQL.AppendLine("Inner Join TblWarehouse E On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=E.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblPODtl H On B.PODocNo=H.DocNo And B.PODNo=H.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl I On H.PORequestDocNo=I.DocNo And H.PORequestDNo=I.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr J On I.MaterialRequestDocNo=J.DocNo ");
            SQL.AppendLine("Left Join TblSOContractHdr K ON J.SOCDocNo=K.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup L On J.PGCode=L.PGCode ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl M On I.MaterialRequestDocNo=M.DocNo And I.MaterialRequestDNo=M.DNo ");
            SQL.AppendLine("Left Join tblmaterialrequestdtl N ON M.DocNo = N.DocNo AND M.DNo = N.DNo ");
            SQL.AppendLine("   AND N.MaterialRequestServiceDocNo IS NOT NULL ");
            SQL.AppendLine(")A  ");

            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 37;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Local Document#",
                        "Warehouse",

                        //6-10
                        "Vendor",
                        "DO#", 
                        "PO#",
                        "Item's Code",
                        "Item's Name",

                        //11-15
                        "Local Code",
                        "Specification",
                        "Batch#",
                        "Source",
                        "Lot",

                        //16-20
                        "Bin",
                        "Quantity"+Environment.NewLine+"(Purchase)",
                        "UoM"+Environment.NewLine+"(Purchase)",
                        "Quantity"+Environment.NewLine+"(Inventory)",
                        "UoM"+Environment.NewLine+"(Inventory)",

                        //21-25
                        "Quantity"+Environment.NewLine+"(Inventory 2)",
                        "UoM"+Environment.NewLine+"(Inventory 2)",
                        "Quantity"+Environment.NewLine+"(Inventory 3)",
                        "UoM"+Environment.NewLine+"(Inventory 3)",
                        "Project Code",

                        //26-30
                        "Project Name",
                        "Customer's PO#",
                        "Document's Remark",
                        "Item's Remark",
                        "Created By",

                        //31-35
                        "Created Date", 
                        "Created Time", 
                        "Last Updated By", 
                        "Last Updated Date", 
                        "Last Updated Time",

                        //36
                        "Status"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 100, 80, 130, 200, 
                        
                        //6-10
                        200, 130, 130, 100, 200, 
                        
                        //11-15
                        100, 200, 150, 150, 60, 
                        
                        //16-20
                        60, 80, 80, 80, 80, 
                        
                        //21-25
                        80, 80, 80, 80, 130, 
                        
                        //26-30
                        200, 130, 250, 250, 130, 
                        
                        //31-35
                        130, 130, 130, 130, 130,

                        //36
                        120
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 17, 19, 21, 23 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 31, 34 });
            Sm.GrdFormatTime(Grd1, new int[] { 32, 35 });
            Sm.GrdColInvisible(Grd1, new int[] { 9, 12, 14, 15, 16, 21, 22, 23, 24, 30, 31, 32, 33, 34, 35 }, false);
            ShowInventoryUomCode();
            Grd1.Cols[36].Move(4);
            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 12, 14, 15, 16, 30, 31, 32, 33, 34, 35 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23, 24 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " And A.CancelInd='N' ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtPODocNo.Text, "A.PODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItCodeInternal", "A.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "A.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "A.Bin", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "LocalDocNo", "WhsName", "VdName", 
                            
                            //6-10
                            "VdDONo", "PODocNo", "ItCode", "ItName", "ItCodeInternal", 

                            //11-15
                            "Specification", "BatchNo", "Source", "Lot", "Bin", 
                            
                            //16-20
                            "QtyPurchase", "PurchaseUomCode", "Qty", "InventoryUomCode", "Qty2", 
                            
                            //21-25
                            "InventoryUomCode2", "Qty3", "InventoryUomCode3", "ProjectCode", "ProjectName", 
                            
                            //26-30
                            "PONo", "RemarkH", "RemarkD", "CreateBy", "CreateDt", 
                            
                            //31-33
                            "LastUpBy", "LastUpDt", "StatusDesc"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 31, 30);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 32, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 31);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 34, 32);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 35, 32);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 33);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtPODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        #endregion

        #endregion 
    }
}
