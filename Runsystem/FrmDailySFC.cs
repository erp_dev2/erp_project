﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDailySFC : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmDailySFCFind FrmFind;

        #endregion

        #region Constructor

        public FrmDailySFC(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Daily ShopFloor Control";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid1

            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 3;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Dno",
                        
                        //1-5
                        "",
                        "Workcenter Code",
                        "",
                        "Workcenter",
                        "Location",
                        
                        //6-8
                        "Total Machine",
                        "Running Machine",
                        "%",
                    },
                    new int[] 
                    {
                        20, 
                        20, 120, 20, 150, 250, 
                        100, 100, 100 
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 6,7,8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3 });

            #endregion

            #region Grid2

            Grd2.Cols.Count = 12;
            Grd2.FrozenArea.ColCount = 3;
            Grd2.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                       //0
                        "Dno",

                       //1-5
                        "Workcenter Code",
                        "",
                        "Workcenter",
                        "Location",
                        "Speed (CD)",
                        
                        //6-10
                        "RPM",
                        "TPI (CD)",
                        "Yard",
                        "Grain (CD)",
                        "Constant",
                        //11
                        "Average "+Environment.NewLine+" Count Riil"
                    },
                    new int[] 
                    {
                        20, 
                        120, 20, 150, 250, 120, 
                        120, 120, 120, 120, 120,
                        120
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 2 });
            Sm.GrdFormatDec(Grd2, new int[] { 5, 6, 7, 8, 9, 10, 11 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 1, 2 });

            #endregion

            #region Grid3

            Grd3.Cols.Count = 7;
            Grd3.FrozenArea.ColCount = 1;
            Grd3.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                       //0
                        "Workcenter Code",
                        //1-5
                        "",
                        "Workcenter",
                        "Warehouse",
                        "Result",
                        "",
                        //6
                        "Percentage"
                    },
                    new int[] 
                    {
                        120, 
                        20, 150, 250, 150, 20, 
                        120
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 1, 5 });
            Sm.GrdFormatDec(Grd3, new int[] { 4, 6 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 1, 6 });

            #endregion
        }

        private void ChkHideInfoInGrd_CheckedChanged_1(object sender, EventArgs e)
        {

            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 1 }, !ChkHideInfoInGrd.Checked);
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtWorkDay, MeeRemark, 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 3, 4, 6 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtWorkDay, MeeRemark,
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 1, 5 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 2, 5, 6, 7, 8, 9, 10, 11 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3, 7 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, MeeRemark, 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtWorkDay }, 0); 
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4, 6});
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 5, 6, 7, 8, 9, 10, 11 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 7, 8});
            Sm.FocusGrd(Grd3, 0, 0);
            Sm.FocusGrd(Grd2, 0, 0);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDailySFCFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                ChkCancelInd.Checked = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        #region Grid Method

        private void Grd1_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                if (e.ColIndex == 1) Sm.FormShowDialog(new FrmDailySFCDlg(this));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmDailySFCDlg(this));
            }


            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {

                if (Grd1.SelectedRows.Count > 0)
                {
                    if (Grd1.Rows[Grd1.Rows[Grd1.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd1.SelectedRows.Count - 1; Index >= 0; Index--)
                                Grd1.Rows.RemoveAt(Grd1.SelectedRows[Index].Index);

                            if (Grd1.Rows.Count <= 0) Grd1.Rows.Add();

                            string Key1 = string.Empty;
                            bool IsFind = false;
                            for (int Row = Grd2.Rows.Count - 1; Row >= 0; Row--)
                            {
                                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                                {
                                    Key1 =
                                        Sm.GetGrdStr(Grd2, Row, 1) ;

                                    IsFind = false;
                                    for (int Row2 = 0; Row2 < Grd1.Rows.Count; Row2++)
                                    {
                                        if (Sm.GetGrdStr(Grd1, Row2, 2).Length > 0 &&
                                            Sm.CompareStr(
                                                Key1,
                                                Sm.GetGrdStr(Grd1, Row2, 2) 
                                                ))
                                        {
                                            IsFind = true;
                                            break;
                                        }
                                    }
                                    if (!IsFind) Grd2.Rows.RemoveAt(Row);
                                }
                            }
                            if (Grd2.Rows.Count <= 0) Grd2.Rows.Add();

                        }
                    }
                }
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 7 }, e.ColIndex))
                ComputePercentageMachine(e.RowIndex);
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex))
               ComputeAverageCountRiil(e.RowIndex);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd3, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd3, e.RowIndex, 0);
                f.ShowDialog();
            }

            if (TxtDocNo.Text.Length != 0)
            {
                if (e.ColIndex == 5) Sm.FormShowDialog(new FrmDailySFCDlg2(this, Sm.GetGrdStr(Grd3, e.RowIndex, 0), string.Concat(Sm.GetDte(DteDocDt).Substring(0, 4), Sm.GetDte(DteDocDt).Substring(4, 2), "01"), Sm.GetDte(DteDocDt).Substring(0, 8)));
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd3, e.RowIndex, 0).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd3, e.RowIndex, 0);
                f.ShowDialog();
            }
            if (e.ColIndex == 5 && TxtDocNo.Text.Length != 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmDailySFCDlg2(this, Sm.GetGrdStr(Grd3, e.RowIndex, 0), string.Concat(Sm.GetDte(DteDocDt).Substring(0, 4), Sm.GetDte(DteDocDt).Substring(4, 2), "01"), Sm.GetDte(DteDocDt).Substring(0, 8)));
            }
        }


        #region Cancel data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            //cml.Add(CancelProductionRoutingHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document number", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            string a = ChkCancelInd.Checked ? "Y" : "N";
            var SQL = new StringBuilder();
            if (a == "N")
            {
                SQL.AppendLine("Select DocNo From TblProductionRoutingHdr ");
                SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This data already Cancelled.");
                    return true;
                }

            }
            return false;
        }

        #endregion

        #endregion


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DailySFC", "TblDailySFCHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDailySFCHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveDailySFCDtl(DocNo, Row));

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveDailySFCDtl2(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document date") ||
                Sm.IsTxtEmpty(TxtWorkDay, "Working Day", true) ||
                IsGrdEmpty() 
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least one workcenter.");
                return true;
            }

            return false;
        }

        private MySqlCommand SaveDailySFCHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDailySFCHdr(DocNo, DocDt, CancelInd, WorkDay, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'N', @WorkDay, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<Decimal>(ref cm, "@WorkDay", Decimal.Parse(TxtWorkDay.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDailySFCDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDailySFCDtl(DocNo, DNo, WorkCenterDocNo, QtyRunningMachine,  CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @WorkCenterDocNo, @QtyRunningMachine,  @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@QtyRunningMachine", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDailySFCDtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDailySFCDtl2(DocNo, DNo, WorkCenterDocNo, Speed, RPM, TPI, Yard, Grain, Constant, Average,  CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @WorkCenterDocNo, @Speed, @RPM, @TPI, @Yard, @Grain, @Constant, @Average,  @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Speed", Sm.GetGrdDec(Grd2, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@RPM", Sm.GetGrdDec(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@TPI", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Yard", Sm.GetGrdDec(Grd2, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Grain", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Constant", Sm.GetGrdDec(Grd2, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Average", Sm.GetGrdDec(Grd2, Row, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
       

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            EditDailySFCHdr();
            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Daily SFC Document", false) ||
                IsCancelIndEditedAlready();
        }

        private bool IsCancelIndEditedAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblDailySFCHdr Where DocNo=@DocNo And CancelInd = 'Y' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already Cancelled.");
                return true;
            }
            return false;
        }

        private void EditDailySFCHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblDailySFCHdr Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowDailySFCHdr(DocNo);
                ShowDailySFCDtl(DocNo);
                ShowDailySFCDtl2(DocNo);
                ComputePercentageMachine();
                ShowAcumulateSFC();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDailySFCHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, cancelInd, WorkDay, Remark " +
                    "From TblDailySFCHdr  " +
                    "Where DocNo=@DocNo",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "cancelInd", "WorkDay", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtWorkDay.EditValue = Sm.DrDec(dr, c[3]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowDailySFCDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.WorkcenterDocNo, C.DocName, D.WhsName, C.Capacity, ");
            SQL.AppendLine("B.Speed, B.RPM, B.TPI, B.Yard, B.Grain, B.Constant, B.Average ");
            SQL.AppendLine("From  TblDailySFCDTL2 B  ");
            SQL.AppendLine("Inner Join TblWorkcenterHdr C On B.WorkcenterDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse D On C.WhsCode = D.WhsCode ");
            SQL.AppendLine("Where B.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                     //0
                   "Dno",
                    //1-5
                   "WorkcenterDocNo", "DocName", "WhsName",  "Speed", "RPM", 
                    //6-10
                   "TPI", "Yard", "Grain", "Constant", "Average"
                   
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);

                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowDailySFCDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.WorkcenterDocNo, C.DocName, D.WhsName, ");
            SQL.AppendLine("C.Capacity, B.QtyRunningMachine, B.Percentage ");
            SQL.AppendLine("From  TblDailySFCDTL B  ");
            SQL.AppendLine("Inner Join TblWorkcenterHdr C On B.WorkcenterDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse D On C.WhsCode = D.WhsCode ");
            SQL.AppendLine("Where B.DocNo=@DocNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "Dno",
                    //1-5
                    "WorkcenterDocNo", "DocName", "WhsName", "Capacity", "QtyRunningMachine", 
                    //6
                    "Percentage" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7, 8 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowAcumulateSFC()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select X.WorkCenterDocNo, X.DocName, X.WhsName, SUM(X.result) As Result ");
            SQL.AppendLine("From (  ");
            SQL.AppendLine("    Select A.WorkCenterDocNo, D.DocName, C.WhsName, SUM(Qty) As Result ");
            SQL.AppendLine("    from TblShopfloorControlHdr A ");
            SQL.AppendLine("    Inner Join TblShopfloorControlDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join Tblwarehouse C On A.WhsCode = C.WhsCode ");
            SQL.AppendLine("    Inner Join TblWorkcenterHdr D On  A.WorkcenterDocNo = D.DocNo ");
            SQL.AppendLine("    Where A.CancelInd = 'N' And A.WorkCenterDocNo In (" + GetSelectedItem() + ") ");
            SQL.AppendLine("    And A.DocDt between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By A.Docno, A.WorkCenterDocNo, C.WhsName ");
            SQL.AppendLine(")X ");
            SQL.AppendLine("Group By X.WorkCenterDocNo, X.DocName, X.WhsName ");

            var cm = new MySqlCommand();

            string yr = Sm.GetDte(DteDocDt).Substring(0, 4);
            string mth = Sm.GetDte(DteDocDt).Substring(4, 2);
            string DocDt1 = string.Concat(yr, mth, "01");
            string DocDt2 = Sm.GetDte(DteDocDt).Substring(0, 8);

            Sm.CmParam<String>(ref cm, "@DocDt1", DocDt1);
            Sm.CmParam<String>(ref cm, "@DocDt2", DocDt2);

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "WorkCenterDocNo",
                    //1-3
                    "DocName", "WhsName", "Result"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
            Sm.FocusGrd(Grd3, 0, 1);
        }


        #endregion

        #region Additional Method

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void ComputePercentageMachine(int RowX)
        {
            decimal Qty = Sm.GetGrdDec(Grd1, RowX, 6);
            decimal Percentage = 0m;
            
            if ( Sm.GetGrdStr(Grd1, RowX, 2).Length != 0 )
            {
                if (Sm.GetGrdDec(Grd1, RowX, 7) > Qty)
                {
                    Sm.StdMsg(mMsgType.Warning, "Work machine should not be bigger than total machine");
                    Grd1.Cells[RowX, 7].Value = 0;
                }
                else
                {
                    if (Qty != 0)
                    {
                        Percentage = (Sm.GetGrdDec(Grd1, RowX, 7) / Qty) * 100m;
                        Grd1.Cells[RowX, 8].Value = Sm.FormatNum(Percentage, 0);
                    }
                    else
                    {
                        Grd1.Cells[RowX, 8].Value = 0m;
                    }
                }
            }
        }

        internal void ComputePercentageMachine()
        {
            decimal Qty = 0m, Percentage = 0m;
            
            for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
            {
                if (Sm.GetGrdStr(Grd1, Row2, 2).Length != 0)
                {
                    Qty = Sm.GetGrdDec(Grd1, Row2, 6);
                    if (Qty != 0)
                    {
                        Percentage = (Sm.GetGrdDec(Grd1, Row2, 7) / Qty) * 100m;
                        Grd1.Cells[Row2, 8].Value = Sm.FormatNum(Percentage, 0);
                    }
                    else
                    {
                        Grd1.Cells[Row2, 8].Value = 0m;
                    }
                }
            }
        }

        internal void ComputeAverageCountRiil(int RowX)
        {
            decimal Qty = Sm.GetGrdDec(Grd2, RowX, 9);
            decimal Average = 0m;

            if (Sm.GetGrdStr(Grd2, RowX, 1).Length != 0)
            {
                if (Qty != 0)
                {
                    Average = (Sm.GetGrdDec(Grd2, RowX, 10) / Qty);
                    Grd2.Cells[RowX, 11].Value = Sm.FormatNum(Average, 0);
                }
                else
                {
                    Grd2.Cells[RowX, 11].Value = 0m;
                }
            }
        }


        #endregion
        
        #endregion

        #region Event
        private void BtnAccumulate_Click(object sender, EventArgs e)
        {
            ShowAcumulateSFC();
        }

        private void TxtWorkDay_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtWorkDay, 0);
        }
        #endregion

    }
}
