﻿#region Update
/*
    26/07/2018 [WED] waktu cancel dokumen juga cek ke approval status nya udah cancel atau belum
    03/08/2018 [WED] waktu cancel dokumen juga cek ke survey nya
    28/02/2019 [MEY] Penambahan input lue Types of Loans 
    11/07/2019 [DITA] Bug saat menampilkan partner isi data tidak sesuai
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRequestLP : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmRequestLPFind FrmFind;

        #endregion

        #region Constructor

        public FrmRequestLP(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmRequestLP");
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);

                Sl.SetLueCityCode(ref LueCityCode);
                Sl.SetLueProvCode(ref LueProvCode);
                Sl.SetLueOption(ref LueGender, "Gender");
                Sl.SetLueBCCode(ref LueBCCode);
                Sl.SetLueBSCode(ref LueBSCode);
                Sl.SetLueTypesofLoans(ref LueTypesofLoans);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtAmt, MeeRemark, TxtAssurance, TxtOmzet, MeeCancelReason, LueTypesofLoans
                    }, true);
                    BtnPnCode.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, TxtAmt, TxtAssurance, TxtOmzet,  LueTypesofLoans
                    }, false);
                    BtnPnCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    MeeCancelReason.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeRemark, TxtStatus, TxtPnCode, TxtPnName, TxtAssurance,
                TxtShortName, MeeAddress, LueCityCode, 
                LueProvCode, TxtPostalCd, TxtIdentityNo, LueGender, DteBirthDt, 
                TxtTIN, TxtPhone, TxtMobile, 
                LueBCCode, LueBSCode, TxtCompanyName, MeeCompanyAddress, MeeCancelReason,  LueTypesofLoans
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt, TxtOmzet }, 0);
            ChkCancelInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRequestLPFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.Text = "Outstanding";
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RequestLP", "TblRequestLP");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRequestLP(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtPnCode, "Partner Code", false) ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsTxtEmpty(TxtOmzet, "Omzet", true) ||
               Sm.IsLueEmpty(LueTypesofLoans, "Types of Loans");
        }

        private MySqlCommand SaveRequestLP(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRequestLP (DocNo, DocDt, Status, CancelInd, ProcessInd, PnCode, Amt, Omzet, Assurance, TypesOfLoans, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', 'N', 'O', @PnCode, @Amt, @Omzet, @Assurance, @TypesOfLoans, @Remark, @CreateBy, CurrentDateTime()); ");

            if(IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting ");
                SQL.AppendLine("Where DocType='RequestLP'; ");
            }

            SQL.AppendLine("Update TblRequestLP Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RequestLP' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PnCode", TxtPnCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Omzet", decimal.Parse(TxtOmzet.Text));
            Sm.CmParam<String>(ref cm, "@Assurance", TxtAssurance.Text);
            Sm.CmParam<String>(ref cm, "@TypesOfLoans", Sm.GetLue(LueTypesofLoans));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditRequestLP());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDocumentAlreadyCancelled() ||
                IsDocumentAlreadyProcessed();
        }

        private bool IsDocumentAlreadyProcessed()
        {
            if (Sm.IsDataExist("Select 1 From TblSurvey Where RQLPDocNo = @Param And Status = 'A' And CancelInd = 'N' Limit 1; ", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already processed to Survey.");
                return true;
            }            

            return false;
        }

        private bool IsDocumentNotCancelled()
        {
            if(!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                MeeCancelReason.Focus();
                return true;
            }

            return false;
        }

        private bool IsDocumentAlreadyCancelled()
        {
            if(Sm.IsDataExist("Select DocNo From TblRequestLP Where DocNo = @Param And (CancelInd = 'Y' Or Status = 'C') Limit 1;", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already cancelled.");
                TxtDocNo.Focus();
                return true;
            }

            return false;
        }

        private MySqlCommand EditRequestLP()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRequestLP Set ");
            SQL.AppendLine("    CancelReason = @CancelReason, CancelInd = @CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd = 'N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowRequestLP(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowRequestLP(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("A.PnCode, A.Amt, A.Remark,  ");
            SQL.AppendLine("B.PnName, B.ShortName, B.Address, B.CityCode, B.ProvCode, B.PostalCd, B.IdentityNo, B.Gender, ");
            SQL.AppendLine("B.BirthDt, B.TIN, B.Phone, B.Mobile, B.ManPower, B.Asset, B.Turnover, B.BCCode, B.BSCode, A.Assurance, ");
            SQL.AppendLine("B.CompanyName, B.CompanyAddress, A.Omzet, A.CancelReason, A.CancelInd, A.TypesOfLoans ");
            SQL.AppendLine("From TblRequestLP A ");
            SQL.AppendLine("Inner Join TblPartner B On A.PnCode = B.PnCode And A.DocNo = @DocNo; ");
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "StatusDesc", "PnCode", "Amt", "Remark",

                    //6-10
                    "PnName", "ShortName", "Address", "CityCode", "ProvCode", 

                    //11-15
                    "PostalCd", "IdentityNo", "Gender", "BirthDt", "TIN", 

                    //16-20
                    "Phone", "Mobile", "ManPower", "Asset", "Turnover", 

                    //21-25
                    "BCCode", "BSCode", "Assurance", "CompanyName", "CompanyAddress", 

                    //26-28
                    "Omzet", "CancelReason", "CancelInd", "TypesOfLoans"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                     TxtPnCode.EditValue = Sm.DrStr(dr, c[3]);
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                     TxtPnName.EditValue = Sm.DrStr(dr, c[6]);
                     TxtShortName.EditValue = Sm.DrStr(dr, c[7]);
                     MeeAddress.EditValue = Sm.DrStr(dr, c[8]);
                     Sm.SetLue(LueCityCode, Sm.DrStr(dr, c[9]));
                     Sm.SetLue(LueProvCode, Sm.DrStr(dr, c[10]));
                     TxtPostalCd.EditValue = Sm.DrStr(dr, c[11]);
                     TxtIdentityNo.EditValue = Sm.DrStr(dr, c[12]);
                     Sm.SetLue(LueGender, Sm.DrStr(dr, c[13]));
                     Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[14]));
                     TxtTIN.EditValue = Sm.DrStr(dr, c[15]);
                     TxtPhone.EditValue = Sm.DrStr(dr, c[16]);
                     TxtMobile.EditValue = Sm.DrStr(dr, c[17]);
                     TxtManPower.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 11);
                     TxtAsset.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[19]), 0);
                     TxtTurnover.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[20]), 0);
                     Sm.SetLue(LueBCCode, Sm.DrStr(dr, c[21]));
                     Sm.SetLue(LueBSCode, Sm.DrStr(dr, c[22]));
                     TxtAssurance.EditValue = Sm.DrStr(dr, c[23]);
                     TxtCompanyName.EditValue = Sm.DrStr(dr, c[24]);
                     MeeCompanyAddress.EditValue = Sm.DrStr(dr, c[25]);
                     TxtOmzet.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[26]), 0);
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[27]);
                     ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[28]), "Y");
                     Sm.SetLue(LueTypesofLoans, Sm.DrStr(dr, c[29]));

                 }, true
             );
        }

        #endregion

        #region Additional Methods

        private bool IsNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType = 'RequestLP' Limit 1;");
        }

        internal void ShowPartner(string PnCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select PnName, ActInd, ShortName, Address, ");
            SQL.AppendLine("CityCode, ProvCode, PostalCd, IdentityNo, Gender, ");
            SQL.AppendLine("BirthDt, TIN, Phone, Fax, Email, ");
            SQL.AppendLine("Mobile, ManPower, Asset, Turnover, BCCode, ");
            SQL.AppendLine("BSCode, Remark, CompanyName, CompanyAddress ");
            SQL.AppendLine("From TblPartner Where PnCode=@PnCode; ");

            Sm.CmParam<String>(ref cm, "@PnCode", PnCode);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                    { 
                        "PnName", 
                        "ActInd", "ShortName", "Address", "CityCode", "ProvCode", 
                        "PostalCd", "IdentityNo", "Gender", "BirthDt", "TIN", 
                        "Phone", "Fax", "Email", "Mobile", "ManPower", 
                        "Asset", "Turnover", "BCCode", "BSCode", "Remark",
                        "CompanyName", "CompanyAddress"
                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtPnCode.EditValue = PnCode;
                    TxtPnName.EditValue = Sm.DrStr(dr, c[0]);
                    TxtShortName.EditValue = Sm.DrStr(dr, c[2]);
                    MeeAddress.EditValue = Sm.DrStr(dr, c[3]);
                    Sm.SetLue(LueCityCode, Sm.DrStr(dr, c[4]));
                    Sm.SetLue(LueProvCode, Sm.DrStr(dr, c[5]));
                    TxtPostalCd.EditValue = Sm.DrStr(dr, c[6]);
                    TxtIdentityNo.EditValue = Sm.DrStr(dr, c[7]);
                    Sm.SetLue(LueGender, Sm.DrStr(dr, c[8]));
                    Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[9]));
                    TxtTIN.EditValue = Sm.DrStr(dr, c[10]);
                    TxtPhone.EditValue = Sm.DrStr(dr, c[11]);
                    TxtMobile.EditValue = Sm.DrStr(dr, c[14]);
                    TxtManPower.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[15]), 11);
                    TxtAsset.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                    TxtTurnover.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]), 0);
                    Sm.SetLue(LueBCCode, Sm.DrStr(dr, c[18]));
                    Sm.SetLue(LueBSCode, Sm.DrStr(dr, c[19]));
                    TxtCompanyName.EditValue = Sm.DrStr(dr, c[21]);
                    MeeCompanyAddress.EditValue = Sm.DrStr(dr, c[22]);
                }, true
            );
        }

        #endregion

        #endregion

        #region events

        #region Misc Control events

        private void TxtPnCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPnCode);
        }

        private void TxtPnName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPnName);
        }

        private void TxtShortName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtShortName);
        }

        private void LueCityCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                LueProvCode.EditValue = null;
                Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue1(Sl.SetLueCityCode));
                var CityCode = Sm.GetLue(LueCityCode);
                if (CityCode.Length > 0)
                {
                    var ProvCode = Sm.GetValue("Select ProvCode From TblCity Where CityCode=@Param;", CityCode);
                    if (ProvCode.Length > 0) Sm.SetLue(LueProvCode, ProvCode);
                }
            }
        }

        private void LueProvCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueProvCode, new Sm.RefreshLue1(Sl.SetLueProvCode));
        }

        private void TxtPostalCd_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPostalCd);
        }

        private void TxtIdentityNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtIdentityNo);
        }

        private void LueGender_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueGender, new Sm.RefreshLue2(Sl.SetLueOption), "Gender");
        }

        private void TxtTIN_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTIN);
        }

        private void TxtPhone_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPhone);
        }

        private void TxtMobile_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtMobile);
        }

        private void TxtManPower_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtManPower, 11);
        }

        private void TxtAsset_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAsset, 0);
        }

        private void TxtTurnover_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtTurnover, 0);
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue1(Sl.SetLueBCCode));
        }

        private void LueBSCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBSCode, new Sm.RefreshLue1(Sl.SetLueBSCode));
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void TxtAssurance_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtAssurance);
        }

        private void TxtOmzet_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtOmzet, 0);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }
        private void LueTypesofLoans_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTypesofLoans, new Sm.RefreshLue1(Sl.SetLueTypesofLoans));
        }
        #endregion

        #region Button Click

        private void BtnPnCode_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmRequestLPDlg(this));
            }
        }

        #endregion



        #endregion

    }
}
