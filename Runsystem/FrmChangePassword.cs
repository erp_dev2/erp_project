﻿#region Update
/*
    08/01/2020 [DITA/ALL] sesuai parameter : IsPasswordForLoginNeedToEncode dapat memilih login berdasarkan password yg di encode atau tidak
    09/04/2021 [TKG/PHT] tambah update PwdLastUpDt
    16/02/2022 [RIS/ALL] Mnambah batas min password dengan param MinPwdLength
    18/04/2022 [WED/GSS] tambah validasi karakter password sesuai rule huruf besar+huruf kecil+simbol berdasarkan parameter PwdCharFormat
    04/08/2022 [MYA/PRODUCT] Perlu adanya Enforce password histori berkaitan dengan pengaturan keamanan terkait sistem informasi aplikasi
    04/09/2022 [WED/GSS] bug kurang Char.IsPunctuation untuk deteksi simbol
    17/03/2023 [HAR/PHT] tambah validasi password baru tidak boleh sama dengan 2 hostoruical terakhir
    26/03/2023 [HAR/TWC] number tidak tervalidasi (function IsNewPwdNotValid)
    06/04/2023 [HAR/PHT] auto close kalo mereka gak update password berdasarkan param : IsForceToUpdatePassword
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using Fl = RunSystem.FrmLogin;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmChangePassword : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal bool mClosed = false;
        private string mNewPwdEncode = string.Empty, mOldPwdEncode = string.Empty, 
               mOldPwd= string.Empty, mNewPwd = string.Empty, mPwdCharFormat = string.Empty, mEnforcePwdHistory = string.Empty;
        private decimal mMinPwdLength = 0m;
        internal bool mChangePassValidateByHistoricalPass = false,
            mIsPwdUseSymbol = false, mIsForceToUpdatePassword = false, mIsPwdAlreadyExpired = false
            ;
        //internal Frm FrmFind;

        #endregion

        #region Constructor

        public FrmChangePassword(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            GetParameter();
            SetFormControl(mState.Edit);
            try
            {
                TxtUser.Text = Gv.CurrentUserCode;
                TxtUserName.Text = Sm.GetValue("Select UserName From TblUser Where UserCode='" + Gv.CurrentUserCode + "'");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
      
  
        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
          

            switch (state)
            {
               
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtUser, true);
                    Sm.SetControlReadOnly(TxtUserName, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtOldPwd,TxtNewPwd
                    }, false);
                    TxtOldPwd.Focus();
                    break;
                default:
                    break;
            }
        }

     
        #endregion

        #region Button Method

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                mOldPwdEncode = Sm.GetHashBase64(Sm.GetHashSha256(TxtOldPwd.Text));
                mNewPwdEncode = Sm.GetHashBase64(Sm.GetHashSha256(TxtNewPwd.Text));
                string DNo = Sm.GetValue("Select IFNULL(DNo, 0) From TblPwdHistory Where UserCode = '" + Gv.CurrentUserCode + "' ORDER BY DNo DESC limit 1");
                if (DNo.Length == 0)
                    DNo = "0";
                if (Sm.GetParameter("IsPasswordForLoginNeedToEncode") == "Y")
                {
                    mOldPwd = mOldPwdEncode;
                    mNewPwd = mNewPwdEncode;
                }
                else
                {
                    mOldPwd = TxtOldPwd.Text;
                    mNewPwd = TxtNewPwd.Text;
                }

                if (IsDataNotValid()) return;
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

                if (mEnforcePwdHistory != "0")
                {
                    SQL.AppendLine("Insert Into TblPwdHistory(UserCode, DNo, PwdHistory, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@UserCode, @DNo, @PwdHistory, @UserCode, CurrentDateTime());");
                }

                SQL.AppendLine("Update TblUser Set ");
                SQL.AppendLine("    Pwd=@Pwd, PwdLastUpDt=Left(@Dt, 8), LastUpBy=@UserCode, LastUpDt=@Dt ");
                SQL.AppendLine("Where UserCode=@UserCode; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@Pwd", mNewPwd);
                Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Convert.ToInt32(DNo) + 1), 3));
                Sm.CmParam<String>(ref cm, "@PwdHistory", mOldPwd);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                
                Sm.ExecCommand(cm);

                Sm.StdMsg(mMsgType.Info, "Your password is successfully changed");

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        bool click = false;
        private void BtnShowOldPass_Click(object sender, EventArgs e)
        {
            if (click == false)
            {
                TxtOldPwd.Properties.PasswordChar = '*';
                click = true;
            }
            else
            {
                TxtOldPwd.Properties.PasswordChar = '\0';
                click = false;
            }
        }

        bool click2 = false;
        private void BtnShowNewPassword_Click(object sender, EventArgs e)
        {
            if (click2 == false)
            {
                TxtNewPwd.Properties.PasswordChar = '*';
                click2 = true;
            }
            else
            {
                TxtNewPwd.Properties.PasswordChar = '\0';
                click2 = false;
            }
        }

        private void FrmChangePassword_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (mIsPwdAlreadyExpired && TxtNewPwd.Text.Length == 0 && mIsForceToUpdatePassword && mClosed)
            {
                Application.Exit();
            }
         }

        private void FrmChangePassword_FormClosing(object sender, FormClosingEventArgs e)
        {        
            if (mIsPwdAlreadyExpired && TxtNewPwd.Text.Length == 0 && mIsForceToUpdatePassword)
            {
                if (BtnSave.Enabled &&
               Sm.StdMsgYN("Question",
                   "New password still empty, you must change your password." + Environment.NewLine +
                   "Continue change password ?") == DialogResult.Yes)
                    e.Cancel = true; 
                else
                    mClosed = true;
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mPwdCharFormat = Sm.GetParameter("PwdCharFormat");
            mEnforcePwdHistory = Sm.GetParameter("EnforcePwdHistory");
            mChangePassValidateByHistoricalPass = Sm.GetParameterBoo("ChangePassValidateByHistoricalPass");
            mIsPwdUseSymbol = Sm.GetParameterBoo("IsPwdUseSymbol");
            mIsForceToUpdatePassword = Sm.GetParameterBoo("IsForceToUpdatePassword");

            if (mPwdCharFormat.Trim().Length == 0) mPwdCharFormat = "1";
        }

        private bool CheckHistoryPwd()
        {
            string param = string.Empty;

            if (mEnforcePwdHistory != "0" && mEnforcePwdHistory.Length > 0)
            {
                if (mPwdCharFormat == "2")
                    param = "BINARY";

                string CheckPwd = Sm.GetValue("SELECT 1 " +
                    "FROM tblPwdHistory A " +
                    "WHERE A.UserCode = '" + Gv.CurrentUserCode + "' " +
                    "AND FIND_IN_SET(" + param + " '" + TxtNewPwd.Text + "', " +
                    "( " +
                    "SELECT GROUP_CONCAT(T.PwdHistory) AS PwdHistory " +
                    "From ( " +
                    "SELECT * " +
                    "FROM tblpwdhistory T1  " +
                    "WHERE T1.UserCode = '" + Gv.CurrentUserCode + "' " +
                    "ORDER BY T1.DNo DESC LIMIT " + mEnforcePwdHistory + " " +
                    ") T " +
                    "Group By T.UserCode" +
                    ") " +
                    ") " +
                    "GROUP BY A.UserCode; ");

                if (CheckPwd.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "You have already used that password, Please Change Your New Password");
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
                return false;


        }

        #endregion

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtUser, "User", false) ||
                Sm.IsTxtEmpty(TxtUserName, "User Name", false) ||
                Sm.IsTxtEmpty(TxtOldPwd, "Old Password", false) ||
                Sm.IsTxtEmpty(TxtNewPwd, "New Password", false)  ||
                IsOldPwdNotValid() ||
                IsOldPwdTheSameAsNewPwd() ||
                IsNewPwdNotValid() ||
                CheckHistoryPwd() ||
                (mChangePassValidateByHistoricalPass && IsOldPwd2NotValid()) 
                ;
        }

        private bool IsOldPwdTheSameAsNewPwd()
        {
            if (mOldPwd == mNewPwd)
            {
                Sm.StdMsg(mMsgType.Warning, "Same Password, Please Change Your New Password .");
                return true;
            }
            return false;
        }


        private bool IsOldPwdNotValid()
        {
            if (!Sm.IsDataExist("Select Pwd From TblUser Where UserCode ='" + TxtUser.Text + "' and Pwd = '"+ mOldPwd +"' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Wrong Password, Please Check Your Password .");
                return true;
            }
            return false;
        }

        private bool IsOldPwd2NotValid()
        {
            string pass = String.Empty;
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
            var SQL = new StringBuilder();

            pass = Sm.GetValue("SELECT GROUP_CONCAT(T.pwdhistory) FROM (SELECT pwdhistory FROM tblpwdhistory  WHERE usercode = '" + Gv.CurrentUserCode+ "' ORDER BY DNO DESC Limit "+ mEnforcePwdHistory + " )T; ");

            string[] passList = pass.Split(',');

            if(passList.Length>0)
            {
                foreach (string psw in passList)
                {
                    if (psw == mNewPwd)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Same with historical Password, Please Change Your New Password .");
                        return true;
                    }
                }
            }
            
            return false;
        }

        private bool IsNewPwdNotValid()
        {
            string MinPwdLengthStr = Sm.GetParameter("MinPwdLength");

            if (MinPwdLengthStr.Trim().Length == 0) return false;

            mMinPwdLength = decimal.Parse(MinPwdLengthStr);

            if (TxtNewPwd.Text.Length < mMinPwdLength)
            {
                Sm.StdMsg(mMsgType.Warning, "New Password Can't Less than " + mMinPwdLength + " Character");
                return true;
            }

            if (mPwdCharFormat == "2")
            {
                string InputPwd = TxtNewPwd.Text.Trim();
                bool UpperCaseExists = false;
                bool LowerCaseExists = false;
                bool SymbolExists = false;
                bool NumberExists = false;

                char[] pwdChars = InputPwd.ToCharArray();
                foreach (var x in pwdChars)
                {
                    if (!UpperCaseExists)
                    {
                        for (char c = 'A'; c <= 'Z'; ++c)
                        {
                            if (x == c)
                            {
                                UpperCaseExists = true;
                                break;
                            }
                        }
                    }

                    if (!LowerCaseExists)
                    {
                        for (char c = 'a'; c <= 'z'; ++c)
                        {
                            if (x == c)
                            {
                                LowerCaseExists = true;
                                break;
                            }
                        }
                    }

                    if (!NumberExists)
                    {
                        if (Char.IsDigit(x))
                            NumberExists = true;
                    }

                    if(mIsPwdUseSymbol)
                    {
                        if (!SymbolExists)
                        {
                            if (Char.IsSymbol(x) || Char.IsPunctuation(x))
                            {
                                SymbolExists = true;
                            }
                        }
                    }
                    else
                    {
                        SymbolExists = true;
                    }
                   

                    if (UpperCaseExists && LowerCaseExists && SymbolExists && NumberExists  == true) break;
                }

                if (!UpperCaseExists || !LowerCaseExists || !SymbolExists || !NumberExists) // mIsPwdUseSymbol = true? "number and one symbol." : "and number"
                {
                    string addMsg = mIsPwdUseSymbol ? "number and one symbol." : "and number";
                    Sm.StdMsg(mMsgType.Warning, "The new password must contain at least one lower case, one upper case, "+ addMsg + "");
                    TxtNewPwd.Focus();
                    return true;
                }
            }

            return false;
        }

        #endregion
        
    }
}
