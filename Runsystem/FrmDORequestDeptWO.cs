﻿#region Update
/*
    24/05/2017 [WED] tampilkan status Supply (ProccesInd) untuk tiap detail Item --> Fulfilled dan Outstanding
    25/05/2017 [WED] validasi terhadap stock dihilangkan menggunakan parameter
    26/07/2017 [TKG] otorisasi group untuk department dan warehouse
    03/08/2017 [TKG] tambah fitur memunculkan item dari semua gudang saat refresh kedua, berdasarkan parameter IsDORequestNeedStockValidation
    04/08/2017 [WED] tambah print out
    10/08/2017 [WED] tambah approval berdasarkan Department, DocType = DORequestDeptWO
    04/09/2017 [HAR] tambah parameter untuk menampilkan item baru, dan asset yang ditampilkan adalaha asset yang terdaftar di profit center bukan costcenter (KMI)
    28/09/2017 [HAR] ubah formload agar bisa di insert dari transaksi WO
    09/10/2017 [TKG] tambah filter berdasarkan otorisasi group thd cost center
    30/01/2018 [HAR] tambah parameter buat nentuin asset default 
    15/03/2018 [ARI] button informasi approval 
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDORequestDeptWO : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmDORequestDeptWOFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        internal int mStateIndicator = 0;
        internal bool
            mIsSystemUseCostCenter = false,
            mIsItGrpCodeShow = false,
            mIsShowForeignName = false,
            mIsFilterByDept = false,
            mUserIndicator = false,
            mIsDORequestNeedStockValidation = false,
            mIsDODeptFilterByAuthorization = false,
            mIsFilterByCC = false,
            mIsRPLAssetDefaultFromWO = false;
        public string mDocType = "0";

        #endregion

        #region Constructor

        public FrmDORequestDeptWO(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Department's DO Request";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                if (mIsSystemUseCostCenter) LblCCCode.ForeColor = Color.Red;
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0 && mDocType == "0")
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                else if(mDocType=="1")
                {
                    SetFormControl(mState.Insert);
                    formLoadInsert();
                    mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                    Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "Approval" +Environment.NewLine+ "Status",
                        "",
                        "",

                        //6-10
                        "Item's Code",
                        "Item's Name",
                        "Foreign Name",
                        "Replacement",
                        "Stock",
                        
                        //11-15
                        "Requested"+Environment.NewLine+"Quantity",
                        "UoM",
                        "Stock",
                        "Requested"+Environment.NewLine+"Quantity",
                        "UoM",
                        
                        //16-20
                        "Stock",
                        "Requested"+Environment.NewLine+"Quantity",
                        "UoM",
                        "",
                        "Asset Code",

                        //21-25
                        "Asset",
                        "Remark",
                        "Cost Category",
                        "Inventory's"+Environment.NewLine+"COA Account#",
                        "Group",

                        //26-27
                        "Display Name",
                        "Status"+Environment.NewLine+"Supply"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        50, 0, 100, 20, 20,

                        //6-10
                        100, 250, 230, 80, 80,
                        
                        //11-15
                        80, 80, 80, 80, 80,
                        
                        //16-20
                        80, 80, 80, 20, 0, 
                        
                        //21-25
                        200, 300, 250, 200, 100,

                        //26-27
                        300, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2, 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 13, 14, 16, 17 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 4, 5, 19 });
            Grd1.Cols[26].Move(22);
            Grd1.Cols[27].Move(3);
            if (mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 6, 13, 14, 15, 16, 17, 18, 25, 27 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 6, 8, 13, 14, 15, 16, 17, 18, 25, 27 }, false);
            
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 6, 7, 8, 10, 12, 13, 15, 16, 18, 20, 21, 23, 24, 25, 26, 27 });
            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[25].Visible = true;
                Grd1.Cols[25].Move(7);
            }
            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6, 27 }, !ChkHideInfoInGrd.Checked);
            ShowInventoryUomCode();
            if (!(BtnSave.Enabled && TxtDocNo.Text.Length==0))
            {
                if (mNumberOfInventoryUomCode == 2)
                    Sm.GrdColInvisible(Grd1, new int[] { 13 }, true);
                if (mNumberOfInventoryUomCode == 3)
                    Sm.GrdColInvisible(Grd1, new int[] { 13, 16 }, true);
            }
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15 }, true);
                
            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16, 17, 18 }, true);
        }
        
        public void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueDeptCode, LueWhsCode, LueCCCode, TxtWODocNo, MeeRemark 
                    }, true);
                    if (mIsSystemUseCostCenter)
                        LblCCCode.ForeColor = Color.Red;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 5, 9, 11, 14, 17, 19, 22 });
                    Sm.GrdColInvisible(Grd1, new int[] { 10, 13, 16 }, false);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueDeptCode, LueWhsCode, LueCCCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 5, 9, 11, 14, 17, 19, 22 });
                    Sm.GrdColInvisible(Grd1, new int[] { 10 }, true);
                    if (mNumberOfInventoryUomCode == 2)
                        Sm.GrdColInvisible(Grd1, new int[] { 13 }, true);
                    if (mNumberOfInventoryUomCode == 3)
                        Sm.GrdColInvisible(Grd1, new int[] { 13, 16 }, true);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();                  
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueDeptCode, LueWhsCode,  MeeRemark, LueCCCode,  TxtWODocNo
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10, 11, 13, 14, 16, 17 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDORequestDeptWOFind(this);
            mStateIndicator = 1;
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                formLoadInsert();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            mStateIndicator = 1;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            ParPrint();
        }

        #endregion

        #region Grid Method

        public void formLoadInsert()
        {
            try
            {
                if (mDocType == "0")
                {
                    ClearData();
                }
                SetFormControl(mState.Insert);
                mStateIndicator = 1;
                SetLueDeptCode(ref LueDeptCode, string.Empty);
                SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueCCCode(ref LueCCCode, string.Empty, mIsFilterByCC?"Y":"N");
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 5 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                            Sm.FormShowDialog(new FrmDORequestDeptWODlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCCCode), TxtWODocNo.Text));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 5, 9, 11, 14, 17, 19, 22 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 11, 13, 14, 16, 17 });
                    }
                }
                else
                {
                    if (
                        e.ColIndex == 1 &&
                        (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 4 && TxtDocNo.Text.Length != 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                e.DoDefault = false; 
                ShowDORequestDeptApprovalInfo(e.RowIndex);
            }


            if (e.ColIndex == 19 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 6, false, "Item is empty."))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDORequestDeptWODlg2(this, e.RowIndex, Sm.GetLue(LueCCCode)));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && TxtDocNo.Text.Length != 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
                ShowDORequestDeptApprovalInfo(e.RowIndex);
            
            if (e.ColIndex == 5 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmDORequestDeptWODlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCCCode), TxtWODocNo.Text));

            if (e.ColIndex == 19 && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 6, false, "Item is empty."))
                Sm.FormShowDialog(new FrmDORequestDeptWODlg2(this, e.RowIndex, Sm.GetLue(LueCCCode)));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 11, 14, 17 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 19 }, e);

            if (e.ColIndex == 11)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 6, 11, 14, 17, 12, 15, 18);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 6, 11, 17, 14, 12, 18, 15);
            }

            if (e.ColIndex == 14)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 6, 14, 11, 17, 15, 12, 18);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 6, 14, 17, 11, 15, 18, 12);
            }

            if (e.ColIndex == 17)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 6, 17, 11, 14, 18, 12, 15);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 6, 17, 14, 11, 18, 15, 12);
            }

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 12), Sm.GetGrdStr(Grd1, e.RowIndex, 15)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 14, Grd1, e.RowIndex, 11);

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 12), Sm.GetGrdStr(Grd1, e.RowIndex, 18)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 17, Grd1, e.RowIndex, 11);

            if (e.ColIndex == 14 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), Sm.GetGrdStr(Grd1, e.RowIndex, 18)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 17, Grd1, e.RowIndex, 14);

            if (e.ColIndex == 19)
                Grd1.Cells[e.RowIndex, 20].Value = (Sm.GetGrdStr(Grd1, e.RowIndex, 21).Length == 0)
                    ? null : GetAssetCode(e.RowIndex);
        }
        
        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DORequestDept", "TblDORequestDeptHdr");
            var cml = new List<MySqlCommand>();
            cml.Add(SaveDORequestDeptHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 6).Length > 0)
                    cml.Add(SaveDORequestDeptDtl(DocNo, Row));

            Sm.ExecCommands(cml);            
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtWODocNo, "Work Order", false)||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueCCCode, "Cost center") ||
                IsWOAlreadyCancelled() ||
                IsWOAlreadySettled() ||
                IsCostCenterEmpty() ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid();
        }

        private bool IsWOAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblWOHdr Where CancelInd='Y' And DocNo=@Param;",
                TxtWODocNo.Text,
                "WO# : " + TxtWODocNo.Text + Environment.NewLine +
                "This WO already cancelled."
                );
        }

        private bool IsWOAlreadySettled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblWOHdr Where SettleInd='Y' And DocNo=@Param;",
                TxtWODocNo.Text,
                "WO# : " + TxtWODocNo.Text + Environment.NewLine +
                "This WO already settled."
                );
        }

        private bool IsCostCenterEmpty()
        {
            if (mIsSystemUseCostCenter)
            {
                return
                    Sm.IsLueEmpty(LueCCCode, "Cost center");
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 6, false, "Item is empty.")) return true;
                if (mIsSystemUseCostCenter)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 23, false, "Cost category is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 24, false, "COA account# is empty.")) return true;
                }

                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 11) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if ((Sm.GetGrdDec(Grd1, Row, 11) > Sm.GetGrdDec(Grd1, Row, 10)) && mIsDORequestNeedStockValidation )
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity should not be bigger than available stock.");
                    return true;
                }

                if (Grd1.Cols[14].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 14) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if ((Sm.GetGrdDec(Grd1, Row, 14) > Sm.GetGrdDec(Grd1, Row, 13)) && mIsDORequestNeedStockValidation )
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity (2) should not be bigger than available stock (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[16].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 17) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }

                    if ((Sm.GetGrdDec(Grd1, Row, 17) > Sm.GetGrdDec(Grd1, Row, 16)) && mIsDORequestNeedStockValidation )
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity (3) should not be bigger than available stock (3).");
                        return true;
                    }
                }
            }
            return false;
        }
        
        private MySqlCommand SaveDORequestDeptHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDORequestDeptHdr(DocNo, DocDt, WODocNo, DeptCode, WhsCode, CCCode, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @WODocNo, @DeptCode, @WhsCode, @CCCode, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WODocNo", TxtWODocNo.Text);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDORequestDeptDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Insert Into TblDORequestDeptDtl(DocNo, DNo, CancelInd, Status, ItCode, Qty, Qty2, Qty3, ReplacementInd, AssetCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', 'O', @ItCode, @Qty, @Qty2, @Qty3, @ReplacementInd, @AssetCode, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='DORequestDeptWO' ");
            SQL.AppendLine("And T.DeptCode=@DeptCode; ");

            SQL.AppendLine("Update TblDORequestDeptDtl Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocType From TblDocApproval ");
            SQL.AppendLine("    Where DocType='DORequestDeptWO' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    And DNo=@DNo ");
            SQL.AppendLine("); ");
            
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<String>(ref cm, "@ReplacementInd", Sm.GetGrdBool(Grd1, Row, 9) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            UpdateCancelledItem();

            string DNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 6).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

            if (
                Sm.StdMsgYN("Save", "") == DialogResult.No ||
                IsCancelledDataNotValid(DNo)
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelDORequestDeptDtl(TxtDocNo.Text, DNo));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblDORequestDeptDtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                {
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                    break;
                                }
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            if (IsCancelledItemNotExisted(DNo)) return true;

            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //{
            //    if (
            //        Sm.GetGrdBool(Grd1, Row, 1) && 
            //        !Sm.GetGrdBool(Grd1, Row, 2) && 
            //        Sm.GetGrdStr(Grd1, Row, 6).Length > 0 &&
            //        IsCancelledItemAlreadyChecked(Row)
            //        ) return true;
            //}

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (
                    Sm.GetGrdBool(Grd1, Row, 1) &&
                    !Sm.GetGrdBool(Grd1, Row, 2) &&
                    Sm.GetGrdStr(Grd1, Row, 6).Length > 0 &&
                    IsCancelledItemAlreadyProcessed(Row)
                    ) return true;
            }
            return false;
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        //private bool IsCancelledItemAlreadyChecked(int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select DocNo From TblDocApproval ");
        //    SQL.AppendLine("Where DocNo=@DocNo ");
        //    SQL.AppendLine("And DNo=@DNo ");
        //    SQL.AppendLine("And DocType='DORequestDept' ");
        //    SQL.AppendLine("And Status Is Not Null;");
            
        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));

        //    if (Sm.IsDataExist(cm))
        //    {
        //        Sm.StdMsg(mMsgType.Warning, 
        //            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
        //            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine +
        //            "You can't cancel this data." + Environment.NewLine + "This data already processed.");
        //        return true;
        //    }
        //    return false;
        //}

        private bool IsCancelledItemAlreadyProcessed(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblDORequestDeptDtl ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");
            SQL.AppendLine("And IfNull(ProcessInd, 'O')<>'O'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Item Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine +
                    "You can't cancel this data." + Environment.NewLine + "This data already processed.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelDORequestDeptDtl(string DocNo, string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDORequestDeptDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DNo In (" + DNo + "); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowDORequestDeptHdr(DocNo);
                ShowDORequestDeptDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDORequestDeptHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, WODocNo, DeptCode, WhsCode, CCCode, Remark " +
                    "From TblDORequestDeptHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo",
 
                        //1-5
                        "DocDt", "WODocNo", "DeptCode", "WhsCode", "CCCode", 
                        //6
                        "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtWODocNo.EditValue = Sm.DrStr(dr, c[2]);
                        SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[3]));
                        SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[4]));
                        Sl.SetLueCCCode(ref LueCCCode, Sm.DrStr(dr, c[5]), string.Empty);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        private void ShowDORequestDeptDtl(string DocNo)
        {
            try
            {
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DNo, A.CancelInd, ");
                SQL.AppendLine("Case IfNull(A.Status, '') ");
                SQL.AppendLine("    When 'O' Then 'Outstanding' ");
                SQL.AppendLine("    When 'A' Then 'Approved' ");
                SQL.AppendLine("    When 'C' Then 'Cancel' ");
                SQL.AppendLine("    Else '' ");
                SQL.AppendLine("End As StatusDesc, ");
                SQL.AppendLine("Case IfNull(A.ProcessInd, '') ");
                SQL.AppendLine("    When 'O' Then 'Outstanding' ");
                SQL.AppendLine("    When 'F' Then 'Fulfilled' ");
                SQL.AppendLine("    When 'P' Then 'Partial' ");
                SQL.AppendLine("    Else '' ");
                SQL.AppendLine("End As StatusSupply, ");
                SQL.AppendLine("A.ItCode, B.ItName, B.ForeignName, "); 
                SQL.AppendLine("A.Qty, B.InventoryUOMCode, "); 
                SQL.AppendLine("A.Qty2, B.InventoryUOMCode2, "); 
                SQL.AppendLine("A.Qty3, B.InventoryUOMCode3, ");
                SQL.AppendLine("A.ReplacementInd, A.AssetCode, F.AssetName, F.DisplayName, A.Remark, E.CCtName, C.AcNo, B.ItGrpCode ");
                SQL.AppendLine("From TblDORequestDeptDtl A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
                SQL.AppendLine("Left Join TblItemCostCategory D On A.ItCode=D.ItCode And D.CCCode=@CCCode ");
                SQL.AppendLine("Left Join TblCostCategory E On D.CCCode=E.CCCode And D.CCtCode=E.CCtCode ");
                SQL.AppendLine("Left Join TblAsset F On A.AssetCode=F.AssetCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");
                
                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "CancelInd", "StatusDesc", "ItCode", "ItName", "ForeignName",
                        
                        //6-10
                        "ReplacementInd", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2",
                        
                        //11-15
                        "Qty3", "InventoryUomCode3", "AssetCode", "AssetName", "Remark",
                        
                        //16-20
                        "CCtName", "AcNo", "ItGrpCode", "DisplayName", "StatusSupply"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 20);
                        Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 10, 13, 16 });
                    }, false, false, true, false
                );
                Sm.FocusGrd(Grd1, 0, 1);
                Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 11, 13, 14, 16, 17 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length != 0) mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            mIsSystemUseCostCenter = Sm.GetParameter("IsSystemUseCostCenter") == "Y";
            if (mIsSystemUseCostCenter) LblCCCode.ForeColor = Color.Red;
            mIsItGrpCodeShow = Sm.GetParameter("IsItGrpCodeShow") == "Y";
            mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
            mIsFilterByDept = Sm.GetParameter("IsFilterByDept") == "Y";
            mUserIndicator = Sm.GetParameter("ReportTitle1") == "PT. Karebet Mas Indonesia";
            mIsDORequestNeedStockValidation = Sm.GetParameter("IsDORequestNeedStockValidation") == "Y";
            mIsDODeptFilterByAuthorization = Sm.GetParameter("IsDODeptFilterByAuthorization") == "Y";
            mIsFilterByCC = Sm.GetParameter("IsFilterByCC") == "Y";
            mIsRPLAssetDefaultFromWO = Sm.GetParameterBoo("IsRPLAssetDefaultFromWO");
        }

        public void SetLueDeptCode(ref DXE.LookUpEdit Lue, string DeptCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
                SQL.AppendLine("From TblDepartment T ");
                SQL.AppendLine("Where (T.ActInd='Y' ");
                if (DeptCode.Length != 0)
                    SQL.AppendLine("Or T.DeptCode=@DeptCode ");
                SQL.AppendLine(") ");
                if ((mIsFilterByDept) && (mStateIndicator == 1))
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                else
                {
                    if (mIsDODeptFilterByAuthorization)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                        SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                }
                SQL.AppendLine("Order By T.DeptName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                if (DeptCode.Length != 0) Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (DeptCode.Length != 0) Sm.SetLue(LueDeptCode, DeptCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public void SetLueWhsCode(ref DXE.LookUpEdit Lue, string WhsCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.WhsCode As Col1, T.WhsName As Col2 ");
                SQL.AppendLine("From TblWarehouse T ");
                SQL.AppendLine("Where 0=0 ");
                if (WhsCode.Length != 0) SQL.AppendLine("And T.WhsCode=@WhsCode ");
                if (mIsDODeptFilterByAuthorization)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select WhsCode From TblGroupWarehouse ");
                    SQL.AppendLine("    Where WhsCode=T.WhsCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By T.WhsName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (WhsCode.Length != 0) Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (WhsCode.Length != 0) Sm.SetLue(LueWhsCode, WhsCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0) 
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 6) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private void ShowDORequestDeptApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
            SQL.AppendLine("    Concat(Substring(LastUpDt, 7, 2), '/', Substring(LastUpDt, 5, 2), '/', Left(LastUpDt, 4))  ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("Remark  ");
            SQL.AppendLine("From TblDocApproval ");
            SQL.AppendLine("Where DocType='DORequestDeptWO' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("Order By ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, new string[]{ "UserCode", "StatusDesc", "LastUpDt", "Remark" });
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            "Remark : " + Sm.DrStr(dr, 3) + Environment.NewLine + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        private string GetAssetCode(int row)
        {
            string AssetName = Sm.GetGrdStr(Grd1, row, 21);
            var cm = new MySqlCommand()
            {
                CommandText = "Select AssetCode From TblAsset Where AssetName=@AssetName Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@AssetName", AssetName);
            string AssetCode = Sm.GetValue(cm);
            if (AssetCode.Length == 0)
            {
                Sm.StdMsg(
                    mMsgType.Warning,
                    "Asset Name : " + AssetName + Environment.NewLine +
                    "Invalid asset."
                    );
                Grd1.Cells[row, 20].Value = null;
                Grd1.Cells[row, 21].Value = null;
                Grd1.Cells[row, 26].Value = null;
            }
            return AssetCode;
        }

        private void ParPrint()
        {
            string ParValue = Sm.GetValue("Select ParValue From TblParameter Where Parcode='NumberOfInventoryUomCode' ");
            string[] TableName = { "DORequestDeptWOHdr", "DORequestDeptWODtl", "DORequestDeptWOSign" };

            var l = new List<DORequestDeptWOHdr>();
            var ldtl = new List<DORequestDeptWODtl>();
            var lSign = new List<DORequestDeptWOSign>();

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();
            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As Phone, ");
            SQL.AppendLine("A.DocNo, A.WODocNo, Date_Format(A.DocDt,'%d %M %Y') As DocDt, B.WhsName, C.DeptName, IfNull(D.CCName, '') As CCName, A.Remark, ");
            SQL.AppendLine("(Select parvalue from tblparameter where parcode='IsShowForeignName') As IsForeignName ");
            SQL.AppendLine("From TblDORequestDeptHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblCostCenter D On A.CCCode = D.CCCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "Company",

                         //1-5
                         "Address",
                         "Phone",
                         "DocNo",
                         "DocDt",
                         "WhsName",
                         
                         //6-10
                         "DeptName",
                         "CCName",
                         "Remark",
                         "CompanyLogo",
                         "IsForeignName",

                         //11
                         "WODocNo"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DORequestDeptWOHdr()
                        {
                            Company = Sm.DrStr(dr, c[0]),
                            Address = Sm.DrStr(dr, c[1]),
                            Phone = Sm.DrStr(dr, c[2]),
                            DocNo = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            WhsName = Sm.DrStr(dr, c[5]),
                            DeptName = Sm.DrStr(dr, c[6]),
                            CCName = Sm.DrStr(dr, c[7]),
                            Remark = Sm.DrStr(dr, c[8]),
                            CompanyLogo = Sm.DrStr(dr, c[9]),
                            IsForeignName = Sm.DrStr(dr, c[10]),
                            WODocNo = Sm.DrStr(dr, c[11]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }

                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.DNo, ");
                SQLDtl.AppendLine("A.ItCode, B.ItName, B.ForeignName, ");
                SQLDtl.AppendLine("A.Qty, B.InventoryUOMCode, ");
                SQLDtl.AppendLine("A.Qty2, B.InventoryUOMCode2, ");
                SQLDtl.AppendLine("A.Qty3, B.InventoryUOMCode3, ");
                SQLDtl.AppendLine("IfNull(F.AssetName, F.DisplayName) As AssetName, A.Remark ");
                SQLDtl.AppendLine("From TblDORequestDeptDtl A ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQLDtl.AppendLine("Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
                SQLDtl.AppendLine("Left Join TblItemCostCategory D On A.ItCode=D.ItCode And D.CCCode=@CCCode ");
                SQLDtl.AppendLine("Left Join TblCostCategory E On D.CCCode=E.CCCode And D.CCtCode=E.CCtCode ");
                SQLDtl.AppendLine("Left Join TblAsset F On A.AssetCode=F.AssetCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo And A.CancelInd = 'N' Order By A.DNo;");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0                         
                         "ItCode",

                         //1-5
                         "ItName",
                         "Qty",
                         "Qty2",
                         "Qty3",
                         "InventoryUOMCode",

                         //6-10
                         "InventoryUOMCode2",
                         "InventoryUOMCode3",
                         "Remark",
                         "ForeignName",
                         "AssetName"
                        });
                if (drDtl.HasRows)
                {
                    int Nomor = 0;
                    while (drDtl.Read())
                    {
                        Nomor++;
                        ldtl.Add(new DORequestDeptWODtl()
                        {
                            No = Nomor,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            Qty = Sm.DrDec(drDtl, cDtl[2]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[3]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[4]),
                            InventoryUOMCode = Sm.DrStr(drDtl, cDtl[5]),
                            InventoryUOMCode2 = Sm.DrStr(drDtl, cDtl[6]),
                            InventoryUOMCode3 = Sm.DrStr(drDtl, cDtl[7]),
                            Remark = Sm.DrStr(drDtl, cDtl[8]),
                            ForeignName = Sm.DrStr(drDtl, cDtl[9]),
                            AssetName = Sm.DrStr(drDtl, cDtl[10])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            #endregion

            #region Sign

            var cmSign = new MySqlCommand();

            var SQLSign = new StringBuilder();
            using (var cnSign = new MySqlConnection(Gv.ConnectionString))
            {
                cnSign.Open();
                cmSign.Connection = cnSign;

                var SQLLvl = new StringBuilder();

                SQLLvl.AppendLine("Select IfNull(Max(D.Level), 0) As Level  ");
                SQLLvl.AppendLine("From TblDORequestDeptDtl A  ");
                SQLLvl.AppendLine("Inner Join TblDocApproval B On A.DocNo = B.DocNo And A.DNo = B.DNo And B.DocType = 'DORequestDept'  ");
                SQLLvl.AppendLine("Inner Join TblUser C On B.UserCode = C.UserCode  ");
                SQLLvl.AppendLine("Inner Join TblDocApprovalSetting D On B.DocType = D.DocType And B.ApprovalDNo = D.DNo  ");
                SQLLvl.AppendLine("Where A.CancelInd = 'N' And A.DocNo = '" + TxtDocNo.Text + "'; ");

                SQLSign.AppendLine("Select T.CreateBy, T.CreateDt, T.PosName, T.AcknowledgeBy, T.ApprovedBy, ");
                SQLSign.AppendLine("Concat(IfNull(T1.ParValue, ''), UCase(T.CreateByCode), '.jpg') As CreateByCode, ");
                SQLSign.AppendLine("Concat(IfNull(T1.ParValue, ''), UCase(T.AcknowledgeByCode), '.jpg') As AcknowledgeByCode, ");
                SQLSign.AppendLine("Concat(IfNull(T1.ParValue, ''), UCase(T.ApprovedByCode), '.jpg') As ApprovedByCode ");
                SQLSign.AppendLine("From ( ");
                SQLSign.AppendLine("    Select Distinct B.UserName As CreateBy, DATE_FORMAT(Left(A.CreateDt, 8), '%d %M %Y') As CreateDt, IfNull(D.PosName, '') As PosName, IfNull(E.AcknowledgeBy, '') As AcknowledgeBy, IfNull(F.ApprovedBy, '') As ApprovedBy, ");
                SQLSign.AppendLine("    A.CreateBy As CreateByCode, IfNull(E.AcknowledgeByCode, '') As AcknowledgeByCode, IfNull(F.ApprovedByCode, '') As ApprovedByCode ");
                SQLSign.AppendLine("    From TblDORequestDeptHdr A ");
                SQLSign.AppendLine("    Inner Join TblUser B On A.CreateBy = B.UserCode ");
                SQLSign.AppendLine("    Left Join TblEmployee C On B.UserCode = C.UserCode ");
                SQLSign.AppendLine("    Left Join TblPosition D On C.PosCode = D.PosCode ");
                SQLSign.AppendLine("    Left Join ");
                SQLSign.AppendLine("    ( ");
                SQLSign.AppendLine("        Select A.DocNo, IfNull(C.UserName, '') As AcknowledgeBy, IfNull(B.UserCode, '') As AcknowledgeByCode");
                SQLSign.AppendLine("        From TblDORequestDeptDtl A ");
                SQLSign.AppendLine("        Inner Join TblDocApproval B On A.DocNo = B.DocNo And A.DNo = B.DNo And B.DocType = 'DORequestDept' ");
                SQLSign.AppendLine("        Inner Join TblUser C On B.UserCode = C.UserCode ");
                SQLSign.AppendLine("        Inner Join TblDocApprovalSetting D On B.DocType = D.DocType And B.ApprovalDNo = D.DNo ");
                SQLSign.AppendLine("        Where A.CancelInd = 'N' And D.Level = @Lv1 And A.DocNo = @DocNo ");
                SQLSign.AppendLine("    )E On A.DocNo = E.DocNo ");
                SQLSign.AppendLine("    Left Join ");
                SQLSign.AppendLine("    ( ");
                SQLSign.AppendLine("        Select A.DocNo, IfNull(C.UserName, '') As ApprovedBy, IfNull(B.UserCode, '') As ApprovedByCode");
                SQLSign.AppendLine("        From TblDORequestDeptDtl A ");
                SQLSign.AppendLine("        Inner Join TblDocApproval B On A.DocNo = B.DocNo And A.DNo = B.DNo And B.DocType = 'DORequestDept' ");
                SQLSign.AppendLine("        Inner Join TblUser C On B.UserCode = C.UserCode ");
                SQLSign.AppendLine("        Inner Join TblDocApprovalSetting D On B.DocType = D.DocType And B.ApprovalDNo = D.DNo ");
                SQLSign.AppendLine("        Where A.CancelInd = 'N' And D.Level = @Lv2 And A.DocNo = @DocNo ");
                SQLSign.AppendLine("    )F On A.DocNo = F.DocNo ");
                SQLSign.AppendLine("    Where A.DocNo = @DocNo ");
                SQLSign.AppendLine(") T ");
                SQLSign.AppendLine("Inner Join TblParameter T1 On T1.ParCode = 'ImgFileSignature' ");

                cmSign.CommandText = SQLSign.ToString();

                Sm.CmParam<String>(ref cmSign, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmSign, "@Lv1", (Decimal.Parse(Sm.GetValue(SQLLvl.ToString())) - 1) <= 0 ? "1" : (Decimal.Parse(Sm.GetValue(SQLLvl.ToString())) - 1).ToString());
                Sm.CmParam<String>(ref cmSign, "@Lv2", Decimal.Parse(Sm.GetValue(SQLLvl.ToString())) <= 1 ? "1" : Decimal.Parse(Sm.GetValue(SQLLvl.ToString())).ToString());

                var drSign = cmSign.ExecuteReader();
                var cSign = Sm.GetOrdinal(drSign, new string[] 
                        {
                         //0                         
                         "CreateBy",

                         //1-5
                         "CreateDt",
                         "PosName",
                         "AcknowledgeBy",
                         "ApprovedBy",
                         "CreateByCode",
                         
                         //6-7
                         "AcknowledgeByCode",
                         "ApprovedByCode"
                        });
                if (drSign.HasRows)
                {
                    while (drSign.Read())
                    {
                        lSign.Add(new DORequestDeptWOSign()
                        {
                            CreateBy = Sm.DrStr(drSign, cSign[0]),
                            CreateDt = Sm.DrStr(drSign, cSign[1]),
                            PosName = Sm.DrStr(drSign, cSign[2]),
                            AcknowledgeBy = Sm.DrStr(drSign, cSign[3]),
                            ApprovedBy = Sm.DrStr(drSign, cSign[4]),
                            CreateByCode = Sm.DrStr(drSign, cSign[5]),
                            AcknowledgeByCode = Sm.DrStr(drSign, cSign[6]),
                            ApprovedByCode = Sm.DrStr(drSign, cSign[7]),
                        });
                    }
                }
                else
                {
                    lSign.Add(new DORequestDeptWOSign()
                    {
                        CreateBy = string.Empty,
                        CreateDt = string.Empty,
                        PosName = string.Empty,
                        AcknowledgeBy = string.Empty,
                        ApprovedBy = string.Empty,
                        CreateByCode = string.Empty,
                        AcknowledgeByCode = string.Empty,
                        ApprovedByCode = string.Empty,
                    });
                }
                drSign.Close();
            }
            myLists.Add(lSign);

            #endregion

            int a = int.Parse(ParValue);
            if (a == 1)
            {
                Sm.PrintReport("DORequestDeptWO1", myLists, TableName, false);
            }
            else if (a == 2)
            {
                Sm.PrintReport("DORequestDeptWO2", myLists, TableName, false);
            }
            else
            {
                Sm.PrintReport("DORequestDeptWO3", myLists, TableName, false);
            }
        }

        #endregion

        #endregion

        #region Event

        private void BtnWODocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDORequestDeptWODlg3(this));
        }

        private void BtnWODocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtWODocNo, "Work Order#", false))
            {
                var f = new FrmWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtWODocNo.Text;
                f.ShowDialog();
            }
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtDocNo);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(SetLueDeptCode), string.Empty);
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(SetLueWhsCode), string.Empty);
                ClearGrd();
            }
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mIsFilterByCC ? "Y" : "N");
                ClearGrd();
            }
        }

        #endregion

        #region Class

        class DORequestDeptWOHdr
        {
            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WODocNo { get; set; }
            public string WhsName { get; set; }
            public string CCName { get; set; }
            public string DeptName { get; set; }
            public string Remark { get; set; }
            public string CompanyLogo { get; set; }
            public string IsForeignName { get; set; }
            public string PrintBy { get; set; }
        }

        class DORequestDeptWODtl
        {
            public int No { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUOMCode { get; set; }
            public string InventoryUOMCode2 { get; set; }
            public string InventoryUOMCode3 { get; set; }
            public string Remark { get; set; }
            public string AssetName { get; set; }
            public string ForeignName { get; set; }
        }

        class DORequestDeptWOSign
        {
            public string CreateBy { get; set; }
            public string CreateDt { get; set; }
            public string PosName { get; set; }
            public string AcknowledgeBy { get; set; }
            public string ApprovedBy { get; set; }
            public string CreateByCode { get; set; }
            public string AcknowledgeByCode { get; set; }
            public string ApprovedByCode { get; set; }
        }

        #endregion

    }
}
