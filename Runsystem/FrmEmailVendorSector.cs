﻿#region Update
/*
    12/07/2021 [WED/PADI] email credential pakai parameter
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

// email
using System.Net;
using System.Net.Security;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Security.Cryptography.X509Certificates;

#endregion

namespace RunSystem
{
    public partial class FrmEmailVendorSector : RunSystem.FrmBase15
    {
        #region Field

        internal string mMenuCode = string.Empty;
        private string
            mVdRegisterEmailFrom = string.Empty,
            mVdRegisterEmailPassword = string.Empty,
            mVdRegisterEmailPort = string.Empty,
            mVdRegisterEmailHost = string.Empty,
            mReportTitle1 = string.Empty,
            mVdRegisterEmailLogo = string.Empty,
            mVdRegisterEmailSubtitle = string.Empty
            ;
        private bool
            mIsVdRegisterEmailUseSSL = false
            ;
        iGCell fCell;
        bool fAccept;
        
        #endregion

        #region Constructor

        public FrmEmailVendorSector(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmEmailVendorSector");
            try
            {
                BtnSave.Text = " &Send";
                GetParameter();
                SetGrd();
                ClearGrd();
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTenderDocNo, MeeTenderName, DteDocDt, DteExpiredDt }, true);
                TxtTenderDocNo.Focus();
                SetLueSector(ref LueSector);
                LueSector.Visible = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 2;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] { "SectorCode", "Sector Name" },
                new int[] { 0, 250 }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtTenderDocNo, MeeTenderName, DteDocDt, DteExpiredDt });
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                SentData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void SentData()
        {
            if (Sm.StdMsgYN("Process", string.Empty) == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string mSectorCode = string.Empty;
            var l = new List<EmailVendorSector>();

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                {
                    if (mSectorCode.Length > 0) mSectorCode += ",";
                    mSectorCode += Sm.GetGrdStr(Grd1, r, 0);
                }
            }

            if(mSectorCode.Length > 0)
            {
                GetVendorData(ref l, mSectorCode);
                if (l.Count > 0)
                {
                    ProcessSentEmail(ref l);
                    Sm.StdMsg(mMsgType.Info, "Process is completed.");
                }
            }

            ClearData();
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtTenderDocNo, "Tender#", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Sector is empty.")) return true;
                }
            }

            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                for (int j = i; j < Grd1.Rows.Count - 1; j++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 0) == Sm.GetGrdStr(Grd1, j + 1, 0))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Duplicate sector : " + Sm.GetGrdStr(Grd1, j + 1, 1));
                        Sm.FocusGrd(Grd1, j + 1, 1);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 sector in the list.");
                return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1) LueRequestEdit(Grd1, LueSector, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            if (e.KeyCode == Keys.Enter && Grd1.CurRow.Index != Grd1.Rows.Count - 1)
                Sm.FocusGrd(Grd1, Grd1.CurRow.Index + 1, Grd1.CurCell.Col.Index);

            if (e.KeyCode == Keys.Tab && Grd1.CurCell != null && Grd1.CurCell.RowIndex == Grd1.Rows.Count - 1)
            {
                int LastVisibleCol = Grd1.CurCell.ColIndex;
                for (int Col = LastVisibleCol; Col <= Grd1.Cols.Count - 1; Col++)
                    if (Grd1.Cols[Col].Visible) LastVisibleCol = Col;

                if (Grd1.CurCell.Col.Order == LastVisibleCol) BtnSave.Focus();
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mVdRegisterEmailFrom = Sm.GetParameter("VdRegisterEmailFrom");
            mVdRegisterEmailPassword = Sm.GetParameter("VdRegisterEmailPassword");
            mVdRegisterEmailPort = Sm.GetParameter("VdRegisterEmailPort");
            mVdRegisterEmailHost = Sm.GetParameter("VdRegisterEmailHost");
            mIsVdRegisterEmailUseSSL = Sm.GetParameterBoo("IsVdRegisterEmailUseSSL");
            mReportTitle1 = Sm.GetParameter("ReportTitle1");
            mVdRegisterEmailLogo = Sm.GetParameter("VdRegisterEmailLogo");
            mVdRegisterEmailSubtitle = Sm.GetParameter("VdRegisterEmailSubtitle");

            if (mVdRegisterEmailLogo.Length == 0) mVdRegisterEmailLogo = "https://approve.runsystem.id/assets/img/runsystem2.png";
        }

        private void SetLueSector(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SectorCode As Col1, SectorName As Col2 ");
            SQL.AppendLine("From TblSector ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By SectorName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
            iGrid Grd,
            DevExpress.XtraEditors.LookUpEdit Lue,
            ref iGCell fCell,
            ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal void ShowTenderData(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, ExpiredDt, TenderName ");
            SQL.AppendLine("From TblTender ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-3
                    "DocDt", "ExpiredDt", "TenderName"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtTenderDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     Sm.SetDte(DteExpiredDt, Sm.DrStr(dr, c[2]));
                     MeeTenderName.EditValue = Sm.DrStr(dr, c[3]);
                 }, true
             );
        }

        private void GetVendorData(ref List<EmailVendorSector> l, string SectorCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.VdCode, Replace(A.VdName, '\"', '\\'') As VdName, A.UserCode, A.Email ");
            SQL.AppendLine("From TblVendor A ");
            SQL.AppendLine("Inner Join TblVendorSector B On A.VdCode = B.VdCode And Find_In_Set(B.SectorCode, @SectorCode) ");
            SQL.AppendLine("Where A.UserCode Is Not Null ");
            SQL.AppendLine("And A.VendorRegisterEmail In (Select Email From TblVendorRegister Where VerifiedInd = 'Y') ");
            SQL.AppendLine("And A.Email Is Not Null ");
            SQL.AppendLine("And Length(A.Email) > 0 ");
            SQL.AppendLine("And A.ActInd = 'Y'; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SectorCode", SectorCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "VdCode", "VdName", "UserCode", "Email" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmailVendorSector()
                        {
                            VdCode = Sm.DrStr(dr, c[0]),
                            VdName = Sm.DrStr(dr, c[1]),
                            UserCode = Sm.DrStr(dr, c[2]),
                            Email = Sm.DrStr(dr, c[3]),
                            TenderDocNo = TxtTenderDocNo.Text,
                            TenderName = MeeTenderName.Text,
                            DocDt = DteDocDt.Text,
                            ExpiredDt = DteExpiredDt.Text
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessSentEmail(ref List<EmailVendorSector> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                var mBody = new StringBuilder();
                string emailFrom = mVdRegisterEmailFrom;
                string mEmailTo = l[i].Email.Replace(";", ",").Replace(" ", string.Empty);
                string[] mEmailTos = mEmailTo.Split(',');
                string mEmailPassword = mVdRegisterEmailPassword;

                mBody.AppendLine("<!DOCTYPE html>");
                mBody.AppendLine("<html lang='en'>");
                mBody.AppendLine("<head>");
                mBody.AppendLine("<meta charset='utf-8'>");
                mBody.AppendLine("<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>");
                mBody.AppendLine("</head>");
                mBody.AppendLine("<body>");
                mBody.AppendLine("<div class='container'>");
                mBody.AppendLine("<table>");
                mBody.AppendLine("    <tr>");
                mBody.AppendLine("	    <td rowspan='3'>");
                //mBody.AppendLine("		    <img src='http://203.130.205.237/runmobile/assets/img/Logo2.png' width=75px />");
                mBody.AppendLine("		    <img src='" + mVdRegisterEmailLogo + "' width=75px />");
                mBody.AppendLine("	    </td>");
                mBody.AppendLine("	    <td>");
                mBody.AppendLine("		    <span style='font-size: 20px; font-weight: bold;'>" + mReportTitle1 + "</span><br />");
                mBody.AppendLine("		    <span style='font-size: 14px; font-weight: bold;'>" + mVdRegisterEmailSubtitle + "</span><br />");
                mBody.AppendLine("	    </td>");
                mBody.AppendLine("    </tr>");
                mBody.AppendLine("</table>");
                mBody.AppendLine("<br />");
                mBody.AppendLine("Yth. " + l[i].VdName + ", ");
                mBody.AppendLine("<br /><br />");
                mBody.AppendLine("Bersama dengan email ini kami informasikan : ");
                mBody.AppendLine("<table class='table'>");
                mBody.AppendLine("    <tr>");
                mBody.AppendLine("        <td><strong>Dokumen Lelang</strong></td>");
                mBody.AppendLine("        <td> : " + l[i].TenderDocNo + "</td>");
                mBody.AppendLine("    </tr>");
                mBody.AppendLine("    <tr>");
                mBody.AppendLine("        <td><strong>Nama Lelang</strong></td>");
                mBody.AppendLine("        <td> : " + l[i].TenderName + "</td>");
                mBody.AppendLine("    </tr>");
                mBody.AppendLine("    <tr>");
                mBody.AppendLine("        <td><strong>Tanggal Mulai Lelang</strong></td>");
                mBody.AppendLine("        <td> : " + l[i].DocDt + "</td>");
                mBody.AppendLine("    </tr>");
                mBody.AppendLine("    <tr>");
                mBody.AppendLine("        <td><strong>Tanggal Berakhir Lelang</strong></td>");
                mBody.AppendLine("        <td> : " + l[i].ExpiredDt + "</td>");
                mBody.AppendLine("    </tr>");
                mBody.AppendLine("</table>");
                mBody.AppendLine("<br /><br />");
                mBody.AppendLine("Demikian informasi lelang yang dapat kami sampaikan. Terimakasih.");
                mBody.AppendLine("<br /><br />");
                mBody.AppendLine("Ttd.");
                mBody.AppendLine("<br /><br />");
                mBody.AppendLine(mReportTitle1);

                foreach (string emailTo in mEmailTos)
                {
                    var cml = new List<MySqlCommand>();
                    MailMessage mail = new MailMessage(emailFrom, emailTo);
                    SmtpClient client = new SmtpClient();
                    client.Port = Int32.Parse(mVdRegisterEmailPort);
                    client.Host = mVdRegisterEmailHost;
                    client.Credentials = new NetworkCredential(emailFrom, mEmailPassword);
                    client.EnableSsl = mIsVdRegisterEmailUseSSL;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;

                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object s,
                               X509Certificate certificate,
                               X509Chain chain,
                               SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };

                    string mSubject = Sm.GetParameter("DocTitle").ToUpper() + " Lelang : " + l[i].TenderName;
                    mail.Subject = mSubject;
                    mail.IsBodyHtml = true;
                    mail.Body = mBody.ToString();
                    client.Send(mail);

                    cml.Add(SaveEmailVendorSector(ref l, i, emailTo, mSubject, mBody.ToString()));
                    Sm.ExecCommands(cml);
                }
            }
        }

        private MySqlCommand SaveEmailVendorSector(ref List<EmailVendorSector> l, int i, string EmailTo, string Subject, string Body)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmailVendorSector(EmailTo, Subject, Body, TenderDocNo, VdCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmailTo, @Subject, @Body, @TenderDocNo, @VdCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmailTo", EmailTo);
            Sm.CmParam<String>(ref cm, "@Subject", Subject);
            Sm.CmParam<String>(ref cm, "@Body", Body);
            Sm.CmParam<String>(ref cm, "@TenderDocNo", l[i].TenderDocNo);
            Sm.CmParam<String>(ref cm, "@VdCode", l[i].VdCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Events

        #region Button Click

        private void BtnTenderDocNo_Click(object sender, EventArgs e)
        {
            if(BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmEmailVendorSectorDlg(this));
            }
        }

        #endregion

        #region Misc Control Events

        private void LueSector_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSector, new Sm.RefreshLue1(SetLueSector));
        }

        private void LueSector_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueSector_Leave(object sender, EventArgs e)
        {
            if (LueSector.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueSector).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 0].Value =
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueSector);
                    Grd1.Cells[fCell.RowIndex, 1].Value = LueSector.GetColumnValue("Col2");
                }
                LueSector.Visible = false;
            }
        }

        #endregion

        #endregion

        #region Class

        private class EmailVendorSector
        {
            public string VdCode { get; set; }
            public string VdName { get; set; }
            public string UserCode { get; set; }
            public string Email { get; set; }
            public string TenderDocNo { get; set; }
            public string TenderName { get; set; }
            public string DocDt { get; set; }
            public string ExpiredDt { get; set; }
        }

        #endregion

    }
}
