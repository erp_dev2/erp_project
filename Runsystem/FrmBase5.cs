﻿#region Update
/*
    24/08/2018 [WED] tambah fasilitas Print dan Excel
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmBase5 : Form
    {
        #region Constructor

        public FrmBase5()
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Standard Method

        virtual protected void SetSQL()
        {
            
        }

        virtual protected void ShowData()
        {

        }

        virtual protected void SaveData()
        {

        }

        virtual protected void HideInfoInGrd()
        {

        }

        virtual protected string SetReportName()
        {
            return "";
        }

        #endregion

        #region Form Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {
            Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12", "14", "16" });
            Sm.SetLue(LueFontSize, "9");
        }

        virtual protected void FrmClosing(object sender, EventArgs e)
        {

        }

        #endregion

        #region Button Method

        virtual protected void BtnRefreshClick(object sender, EventArgs e)
        {
            ShowData();
        }

        virtual protected void BtnSaveClick(object sender, EventArgs e)
        {
            SaveData();
        }

        virtual protected void BtnPrintClick(object sender, EventArgs e)
        {
            iGSubtotalManager.ForeColor = Color.Black;

            string ReportName = SetReportName();

            if (ReportName.Length == 0)
            {
                var Title = this.Text.Trim();
                int Pos = Title.IndexOf("-");
                if (Pos > 0) ReportName = Sm.Right(Title, Title.Length - Pos - 1).Trim();
            }

            PM1.PageHeader.LeftSection.Text = Environment.NewLine + "Report : " + ReportName;
            Sm.SetPrintManagerProperty(PM1, Grd1, false);
            iGSubtotalManager.ForeColor = Color.White;
        }

        virtual protected void BtnExcelClick(object sender, EventArgs e)
        {
            Sm.ExportToExcel(Grd1);
        }

        #endregion

        #region Grid Method

        virtual protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {

        }

        virtual protected void GrdKeyDown(object sender, KeyEventArgs e)
        {

        }

        virtual protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {

        }

        virtual protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {

        }

        virtual protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        { 
        
        }

        #endregion

        #endregion

        #region Event

        #region Form Event

        private void FrmBase5_Load(object sender, EventArgs e)
        {
            FrmLoad(sender, e);
        }

        private void FrmBase5_FormClosing(object sender, FormClosingEventArgs e)
        {
            FrmClosing(sender, e);
        }

        #endregion

        #region Button Event

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            BtnRefreshClick(sender, e);
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            BtnSaveClick(sender, e);
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            BtnPrintClick(sender, e);
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            BtnExcelClick(sender, e);
        }

        #endregion

        #region Misc Event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                //Sm.SetGrdAutoSize(Grd1);
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdRequestEdit(sender, e);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            GrdKeyDown(sender, e);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            GrdEllipsisButtonClick(sender, e);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdAfterCommitEdit(sender, e);
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdColHdrDoubleClick(sender, e);
        }

        #endregion

        #endregion       
    }
}
