﻿#region Update
/*
    11/02/2019 [TKG] approval menggunakan validasi approval group
    12/12/2019 [VIN+DITA/IMS] Printout Leave Request
    10/01/2020 [DITA/IMS] parameter untuk print out
    17/09/2020 [WED/SRN] approval berdasarkan leave code yang ada di DocApprovalSetting, berdasarkan parameter IsDocApprovalSettingUseLeaveCode
    03/11/2020 [IBL/PHT] membuat validasi cuti besar. Berdasarkan parameter IsLongServiceLeaveUseWorkingPeriodValidation dan IsLongServiceLeaveUseWarningLetterValidation
    25/11/2020 [IBL/PHT] membuat validasi cuti. Untuk cuti2 tertentu tidak bisa save ketika selisih leave start date dan docdt <= mimum leave yg ada pada master leave
                         Kode2 cuti diambil dari parameter LeaveCodeWithMinimumNoOfDay
    11/05/2021 [RDH/PHT] penyesuaian printout
    28/05/2021 [BRI/PHT] penyesuaian printout
    11/06/2021 [IQA/PHT] Filter Multi Level Leave Request dan Leave code dengan paramater IsApprovalLeaveRequestByLevelCode
    06/08/2021 [HAR/PHT] di PHT ada cuti dengan jumlah hari melebihi 1000, shg dno perlu diuubah dari 3 digit ke 4 digit
    30/09/2021 [SET/ALL] mengubah source duration day dengan menghitung holiday
    28/10/2021 [AJH/PHT] Pada print out untuk semua dokumen dicantumkan note/catatan seperti surat elektronik (ASME)
    29/10/2021 [DEV/PHT] Pada print out employee leave saat ini alamat menarik addres diharapkan bisa diganti menarik domicile yang diambil dari master employee
    29/10/2021 [SET/PHT] Pada print out untuk employee leave, ketika sudah di approve maka akan memunculkan nama yang mengapprove berdasarkan approval yang telah disetting di menu document approval setting
    16/11/2021 [TRI/PHT] menampilkan button empcode, sebelumnya tidak kelihatan
    10/12/2021 [TRI/PHT] mengganti format tanggal periode leave dan menambah terbilang untuk jumlah hari leave
    14/01/2022 [TKG/PHT] mengganti GetParameter() dan proses save
    10/03/2022 [TRI/PHT] bug saat print out beda tahun startdt dan enddt
    25/10/2022 [VIN/PHT] bug printout PHT duration leave ditulis lengkap
    14/03/2023 [SET/PHT] Penyesuaian printout alamat pada header printout
    14/03/2023 [DITA/PHT] tambah field address dan phone no based on param IsLeaveRequestUseAddressAndPhone
    15/03/2023 [SET/PHT] Penyesuaian printout
    17/03/2023 [SET/PHT] feedback penyesuaian printout
    21/03/2023 [SET/PHT] feedback penyesuaian printout
    23/03/2023 [SET/PHT] validasi save Leave Date & Employee mengacu Travel Request
    24/03/2023 [SET/PHT] Penyesuaian level terakhir approval untuk source city
    29/03/2023 [SET/PHT] penyesuaian tampilan bulan tanggal printout 
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmEmpLeave3 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mAnnualLeaveCode = string.Empty,
            mLongServiceLeaveCode = string.Empty,
            mLongServiceLeaveCode2 = string.Empty;
        internal bool
            mIsFilterByDeptHR = false,
            mIsApprovalLeaveRequestByLevelCode = false,
            mIsFilterBySiteHR = false;
        private bool
            mIsApprovalByDept = false,
            mIsApprovalBySite = false,
            mIsDocApprovalSettingUseLeaveCode = false,
            mIsLongServiceLeaveUseWorkingPeriodValidation = false,
            mIsLongServiceLeaveUseWarningLetterValidation = false,
            mIsEmpLeaveDurationDayCalculateHoliday = false,
            mIsLeaveRequestUseAddressAndPhone = false
            ;
        private string mNeglectLeaveCode = string.Empty,
            mFormPrintOutLeaveRequest = string.Empty,
            mEmpLeaveReqAutoApprovedByEmpCode = string.Empty,
            mLeaveCodeForLongServiceLeaveValidation = string.Empty,
            mLeaveCodeWithMinimumNoOfDay = string.Empty,
            mDocTitle = string.Empty
            ;
        private decimal mNoOfYearWorkingPeriodLongServiceLeaveValidation = 0m;
        internal FrmEmpLeave3Find FrmFind;

        #endregion

        #region Constructor

        public FrmEmpLeave3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Leave";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                GetValue();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueLeaveType, "LeaveType");
                SetGrd();
                if (!mIsLeaveRequestUseAddressAndPhone)
                {
                    LblAddress.Visible = LblPhone.Visible = MeeAddress.Visible = TxtPhone.Visible = false;
                    LblRemark.Location = new System.Drawing.Point(78, 330);
                    MeeRemark.Location = new System.Drawing.Point(130, 327);
                }
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            SetGrd1();
            SetGrd2();
        }

        private void SetGrd1()
        {
            Grd1.Cols.Count = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] { "Leave Date" },
                    new int[] { 150 }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 0 });
        }

        private void SetGrd2()
        {
            Grd2.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] { "Checked By", "Status", "Date", "Remark" },
                    new int[] { 200, 100, 100, 250 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, LueLeaveCode, LueLeaveType, 
                        DteStartDt, DteEndDt, TmeStartTm, TmeEndTm, TxtDurationHour, 
                        ChkBreakInd, MeeRemark, MeeAddress, TxtPhone
                    }, true);
                    BtnEmpCode.Enabled = false;
                    BtnDurationHrDefault.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueLeaveCode, LueLeaveType, DteStartDt, MeeRemark, MeeAddress, TxtPhone }, false);
                    BtnEmpCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, LueLeaveCode, 
                LueLeaveType, DteStartDt, DteEndDt, TxtEmpCode, TxtEmpName, 
                TxtEmpCodeOld, TxtPosCode, TxtDeptCode, TxtSiteCode, DteJoinDt, 
                DteResignDt, DteLeaveStartDt, TmeStartTm, TmeEndTm, TxtPeriodYr, 
                DteLeaveSummaryStartDt, DteLeaveSummaryEndDt, MeeRemark, MeeAddress, TxtPhone
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtDurationDay, TxtDurationHour, TxtBalance }, 0);
            ChkCancelInd.Checked = false;
            ChkBreakInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
        }

        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 
                DteStartDt, DteEndDt, TxtEmpCode, TxtEmpName, 
                TxtEmpCodeOld, TxtPosCode, TxtDeptCode, TxtSiteCode, DteJoinDt, 
                DteResignDt, DteLeaveStartDt, TmeStartTm, TmeEndTm, TxtPeriodYr, 
                DteLeaveSummaryStartDt, DteLeaveSummaryEndDt, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtDurationDay, TxtDurationHour, TxtBalance }, 0);
            ChkBreakInd.Checked = false;
        }

        private void ClearData3()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 
                TxtEmpCode, TxtEmpName, TxtEmpCodeOld, TxtPosCode, TxtDeptCode, 
                TxtSiteCode, DteJoinDt, DteResignDt, DteLeaveStartDt, TmeStartTm, 
                TmeEndTm, TxtPeriodYr, DteLeaveSummaryStartDt, DteLeaveSummaryEndDt, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtDurationDay, TxtDurationHour, TxtBalance }, 0);
            ChkBreakInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpLeave3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueLeaveCode(ref LueLeaveCode, string.Empty);
                //Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ DteEndDt, TmeStartTm, TmeEndTm, TxtDurationHour, ChkBreakInd }, true);
                //BtnDurationHrDefault.Enabled = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (mFormPrintOutLeaveRequest.Length != 0) ParPrint();
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpLeave", "TblEmpLeaveHdr");
            bool IsUseLeaveSummary = IsLeaveUseLeaveSummary();

            var cml = new List<MySqlCommand>();
            var l = new List<EmpLeaveDtl>();
            
            SetEmpLeaveDtl(ref l);

            cml.Add(SaveEmpLeave(DocNo, IsUseLeaveSummary, ref l));

            //cml.Add(SaveEmpLeaveHdr(DocNo, IsUseLeaveSummary, l.Count));
            //for (int i = 0; i < l.Count; i++)
            //{
            //    if (!l[i].HolidayInd)
            //    {
            //        DNo += 1;
            //        cml.Add(SaveEmpLeaveDtl(DocNo, l[i].LeaveDt, DNo));
            //    }
            //}

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueLeaveCode, "Leave name") ||
                Sm.IsLueEmpty(LueLeaveType, "Leave type") ||
                Sm.IsDteEmpty(DteStartDt, "Leave's started date") ||
                (!DteEndDt.Properties.ReadOnly && Sm.IsDteEmpty(DteEndDt, "Leave's end date")) ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee code", false) ||
                (!TmeStartTm.Properties.ReadOnly && Sm.IsTmeEmpty(TmeStartTm, "Leave's start time")) ||
                (!TmeEndTm.Properties.ReadOnly && Sm.IsTmeEmpty(TmeEndTm, "Leave's end time")) ||
                (!TmeStartTm.Properties.ReadOnly && Sm.IsTxtEmpty(TxtDurationHour, "Duration (hour)", true)) ||
                Sm.IsTransactionsCantSameDate("TravelRequestHdr", TxtEmpCode.Text, Sm.GetDte(DteStartDt), Sm.GetDte(DteEndDt)) ||
                IsLeaveDateNotValid() ||
                IsLeaveDateAlreadyUsed() ||
                IsLeaveTimeNotValid() ||
                IsDocDtForResingeeInvalid() ||
                IsLeaveStartDtForResingeeInvalid() ||
                IsLeaveEndDtForResingeeInvalid() ||
                IsNeglectLeaveTypeNotValid() ||
                IsDurationDayNotValid1() ||
                (mIsLongServiceLeaveUseWorkingPeriodValidation && IsYearsOfServiceNotValid()) ||
                (mIsLongServiceLeaveUseWarningLetterValidation && IsEmpHasActiveWarningLetter()) ||
                IsMinimumDayInvalid()
                //|| IsDurationDayNotValid2()
                ;
        }

        private bool IsDocDtForResingeeInvalid()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@DocDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + TxtEmpCode.Text + Environment.NewLine +
                    "Employee's Name : " + TxtEmpName.Text + Environment.NewLine +
                    "Resign Date : " + DteResignDt.Text + Environment.NewLine +
                    "Document Date :" + DteDocDt.Text + Environment.NewLine + Environment.NewLine +
                    "Document date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsLeaveStartDtForResingeeInvalid()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@StartDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + TxtEmpCode.Text + Environment.NewLine +
                    "Employee's Name : " + TxtEmpName.Text + Environment.NewLine +
                    "Resign Date : " + DteResignDt.Text + Environment.NewLine +
                    "Leave's Start Date :" + DteStartDt.Text + Environment.NewLine + Environment.NewLine +
                    "Leave's start date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsLeaveEndDtForResingeeInvalid()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@EndDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + TxtEmpCode.Text + Environment.NewLine +
                    "Employee's Name : " + TxtEmpName.Text + Environment.NewLine +
                    "Resign Date : " + DteResignDt.Text + Environment.NewLine +
                    "Leave's End Date :" + DteEndDt.Text + Environment.NewLine + Environment.NewLine +
                    "Leave's end date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsNeglectLeaveTypeNotValid()
        {
            if (mNeglectLeaveCode.Length != 0 &&
                Sm.CompareStr(Sm.GetLue(LueLeaveCode), mNeglectLeaveCode) &&
                Sm.GetLue(LueLeaveType) != "F")
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Leave : " + LueLeaveCode.GetColumnValue("Col2") + Environment.NewLine +
                    "Invalid leave type." + Environment.NewLine +
                    "It should be full day leave.");
                return true;
            }
            return false;
        }

        private bool IsLeaveDateNotValid()
        {
            string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

            if (StartDt.Length != 0 && EndDt.Length != 0)
            {
                if (decimal.Parse(StartDt) > decimal.Parse(EndDt))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid leave period.");
                    return true;
                }
            }
            return false;
        }

        private bool IsLeaveDateAlreadyUsed()
        {
            string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

            if (StartDt.Length != 0 && EndDt.Length != 0)
            {
                string Dt = string.Empty;
                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    );
                DateTime Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

                if (IsLeaveDateAlreadyUsed2(
                        Dt1.Year.ToString() +
                        ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                        ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                    ))
                    return true;

                var TotalDays = (Dt2 - Dt1).Days;
                for (int i = 1; i <= TotalDays; i++)
                {
                    Dt1 = Dt1.AddDays(1);

                    if (IsLeaveDateAlreadyUsed2(
                            Dt1.Year.ToString() +
                            ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                            ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                        ))
                        return true;
                }
            }
            return false;
        }

        private bool IsLeaveDateAlreadyUsed2(string Dt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From (");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpLeaveHdr A ");
            SQL.AppendLine("    Inner Join TblEmpLeaveDtl B On A.DocNo=B.DocNo And B.LeaveDt=@Dt ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("    And A.EmpCode=@EmpCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpLeave2Hdr A ");
            SQL.AppendLine("    Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo And B.EmpCode=@EmpCode ");
            SQL.AppendLine("    Inner Join TblEmpLeave2Dtl2 C On A.DocNo=C.DocNo And C.LeaveDt=@Dt ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine(") T Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@Dt", Dt);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Date : " + Sm.Right(Dt, 2) + "/" + Dt.Substring(4, 2) + "/" + Sm.Left(Dt, 4) + Environment.NewLine +
                    "Invalid leave date.");
                return true;
            }
            return false;
        }

        private bool IsLeaveTimeNotValid()
        {
            string
                StartTm = Sm.GetDte(DteStartDt) + Sm.GetTme(TmeStartTm),
                EndTm = Sm.GetDte(DteStartDt) + Sm.GetTme(TmeEndTm);

            if (StartTm.Length != 0 && EndTm.Length != 0)
            {
                if (decimal.Parse(StartTm) > decimal.Parse(EndTm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid leave time.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDurationDayNotValid1()
        {
            //Untuk cuti tahunan dan cuti besar
            if (IsLeaveUseLeaveSummary())
            {
                SetBalance();

                var Balance = decimal.Parse(TxtBalance.Text);
                var DurationDay = decimal.Parse(TxtDurationDay.Text);
                var LeaveCode = Sm.GetLue(LueLeaveCode);

                if (Balance < DurationDay)
                {
                    if (mAnnualLeaveCode.Length>0 && Sm.CompareStr(LeaveCode, mAnnualLeaveCode))
                    {
                        Sm.StdMsg(mMsgType.Warning, 
                            "Remaining Days : " + TxtBalance.Text + Environment.NewLine +
                            "Duration Days : " + TxtDurationDay.Text + Environment.NewLine +
                            "Invalid duration days.");
                        return true;
                    }

                    if (mLongServiceLeaveCode.Length > 0 && Sm.CompareStr(LeaveCode, mLongServiceLeaveCode))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Remaining Days : " + TxtBalance.Text + Environment.NewLine +
                            "Duration Days : " + TxtDurationDay.Text + Environment.NewLine +
                            "Invalid duration days.");
                        return true;
                    }

                    if (mLongServiceLeaveCode2.Length > 0 && Sm.CompareStr(LeaveCode, mLongServiceLeaveCode2))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Remaining Days : " + TxtBalance.Text + Environment.NewLine +
                            "Duration Days : " + TxtDurationDay.Text + Environment.NewLine +
                            "Invalid duration days.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsDurationDayNotValid2()
        {
            if (!IsLeavePaid()) return false;

            //Untuk paid leave
            var Balance = decimal.Parse(TxtBalance.Text);
            var DurationDay = decimal.Parse(TxtDurationDay.Text);
            var LeaveCode = Sm.GetLue(LueLeaveCode);

            if (IsLeaveUseLeaveSummary())
            {
                if (Balance < DurationDay)
                {
                    if (mAnnualLeaveCode.Length > 0 && Sm.CompareStr(LeaveCode, mAnnualLeaveCode))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Remaining Days : " + TxtBalance.Text + Environment.NewLine +
                            "Duration Days : " + TxtDurationDay.Text + Environment.NewLine +
                            "Invalid duration days.");
                        return true;
                    }

                    if (mLongServiceLeaveCode.Length > 0 && Sm.CompareStr(LeaveCode, mLongServiceLeaveCode))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Remaining Days : " + TxtBalance.Text + Environment.NewLine +
                            "Duration Days : " + TxtDurationDay.Text + Environment.NewLine +
                            "Invalid duration days.");
                        return true;
                    }

                    if (mLongServiceLeaveCode2.Length > 0 && Sm.CompareStr(LeaveCode, mLongServiceLeaveCode2))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Remaining Days : " + TxtBalance.Text + Environment.NewLine +
                            "Duration Days : " + TxtDurationDay.Text + Environment.NewLine +
                            "Invalid duration days.");
                        return true;
                    }
                }
            }
            return false;
        }

        private void SetBalance()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Balance From TblLeaveSummary ");
            SQL.AppendLine("Where EmpCode=@EmpCode ");
            SQL.AppendLine("And LeaveCode=@LeaveCode ");
            SQL.AppendLine("And Yr=@Yr;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Yr", TxtPeriodYr.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetLue(LueLeaveCode));

            decimal Balance = Sm.GetValueDec(cm);
            TxtBalance.EditValue = Sm.FormatNum(Balance, 0);
        }

        private bool IsLeavePaid()
        {
            var LeaveCode = Sm.GetLue(LueLeaveCode);
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblLeave Where PaidInd='Y' And LeaveCode=@LeaveCode ");
            if (mAnnualLeaveCode.Length>0 && Sm.CompareStr(mAnnualLeaveCode, LeaveCode))
                SQL.AppendLine("And LeaveCode<>@AnnualLeaveCode ");
            if (mLongServiceLeaveCode.Length>0 && Sm.CompareStr(mLongServiceLeaveCode, LeaveCode))
                SQL.AppendLine("And LeaveCode<>@LongServiceLeaveCode ");
            if (mLongServiceLeaveCode2.Length > 0 && Sm.CompareStr(mLongServiceLeaveCode2, LeaveCode))
                SQL.AppendLine("And LeaveCode<>@LongServiceLeaveCode2 ");
            SQL.AppendLine(";");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@LeaveCode", LeaveCode);
            Sm.CmParam<String>(ref cm, "@AnnualLeaveCode", mAnnualLeaveCode);
            Sm.CmParam<String>(ref cm, "@LongServiceLeaveCode", mLongServiceLeaveCode);
            Sm.CmParam<String>(ref cm, "@LongServiceLeaveCode2", mLongServiceLeaveCode2);

            return Sm.IsDataExist(cm);
        }

        private bool IsYearsOfServiceNotValid()
        {
            DateTime JoinDt = new DateTime(), ResignDt = new DateTime();
            JoinDt = (DteJoinDt.Text.Length == 0 ? JoinDt : Convert.ToDateTime(DteJoinDt.Text));
            ResignDt = (DteResignDt.Text.Length == 0 ? ResignDt : Convert.ToDateTime(DteResignDt.Text));

            bool mLongServiceLeave = Sm.Find_In_Set(Sm.GetLue(LueLeaveCode), mLeaveCodeForLongServiceLeaveValidation);

            if (mLongServiceLeave)
            {
                if (DteResignDt.Text.Length == 0 || (DteResignDt.Text.Length > 0 && ResignDt > DateTime.Now.Date))
                {
                    TimeSpan YearsOfService = DateTime.Now.Date - JoinDt;
                    int NoOfYearService = (int)Math.Floor(YearsOfService.Days / 365.2425);

                    if (NoOfYearService < mNoOfYearWorkingPeriodLongServiceLeaveValidation)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "You are not allowed to apply for long service leave." + Environment.NewLine +
                            "Your tenure is less than " + Sm.FormatNum(mNoOfYearWorkingPeriodLongServiceLeaveValidation, 1) +" year(s)");
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsEmpHasActiveWarningLetter()
        {
            bool mLongServiceLeave = Sm.Find_In_Set(Sm.GetLue(LueLeaveCode), mLeaveCodeForLongServiceLeaveValidation);
            if (mLongServiceLeave)
            {
                if (Sm.IsDataExist(
                   "Select 1 From TblEmpWL " +
                   "Where CancelInd='N' And EmpCode=@Param1 And" +
                   "(EndDt Is Null And StartDt Between @Param2 And @Param3) Or " +
                   "( " +
                   "    EndDt Is Not Null And StartDt Is Not Null " +
                   "    And " +
                   "    ( " +
                   "        StartDt Between @Param2 And @Param3 Or " +
                   "        EndDt Between @Param2 And @Param3 " +
                   "    ) " +
                   "); ", TxtEmpCode.Text, Sm.GetDte(DteStartDt), Sm.GetDte(DteEndDt)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                                "You are not allowed to apply for long service leave." + Environment.NewLine +
                                "You have active warning letter on this long service leave period.");
                    return true;
                }
            }
            return false;
        }

        private bool IsMinimumDayInvalid()
        {
            decimal mMinimumLeave = 0m, mDifference = 0m;
            DateTime mDocDt = Convert.ToDateTime(DteDocDt.Text), mLeaveStartDt = Convert.ToDateTime(DteStartDt.Text);
            bool mLeaveCode = Sm.Find_In_Set(Sm.GetLue(LueLeaveCode), mLeaveCodeWithMinimumNoOfDay);

            if(mLeaveCode)
            {
                mMinimumLeave = Sm.GetValueDec("Select MinimumLeave From TblLeave Where LeaveCode=@Param", Sm.GetLue(LueLeaveCode));
                mDifference = Convert.ToDecimal((mLeaveStartDt.Date - mDocDt.Date).Days);
                if (mDifference <= mMinimumLeave && mMinimumLeave != 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Leave start date not valid.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveEmpLeave(string DocNo, bool IsUseLeaveSummary, ref List<EmpLeaveDtl> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int DNo = 0;
            bool IsFirst = true;
            var LeaveType = Sm.GetLue(LueLeaveType);
            decimal DurationDay = l.Count;
            if (LeaveType == "O" || LeaveType == "T") DurationDay = l.Count * 0.5m;
            int MaxNoofDay = 0;
            MaxNoofDay = Convert.ToInt32(Sm.GetValue("select max(cast(noofday As Int)) From tblLeave where actInd = 'Y';"));

            SQL.AppendLine("/* Leave Request */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblEmpLeaveHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, Status, ");
            SQL.AppendLine("LeaveCode, LeaveType, StartDt, EndDt, ");
            SQL.AppendLine("EmpCode, LeaveStartDt, PeriodYr, ");
            SQL.AppendLine("DurationDay, DurationHour, StartTm, EndTm, BreakInd, Remark, Address, Phone, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', 'O', ");
            SQL.AppendLine("@LeaveCode, @LeaveType, @StartDt, @EndDt, ");
            SQL.AppendLine("@EmpCode, @LeaveStartDt, @PeriodYr, ");
            SQL.AppendLine("@DurationDay, @DurationHour, @StartTm, @EndTm, @BreakInd, @Remark, @Address, @Phone, ");
            SQL.AppendLine("@UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblDocApproval ");
            SQL.AppendLine("(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @UserCode, @Dt ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='EmpLeave' ");
            if (mIsApprovalBySite)
                SQL.AppendLine("And SiteCode In (Select SiteCode From TblEmployee Where EmpCode=@EmpCode And SiteCode Is Not Null) ");
            if (mIsApprovalByDept)
                SQL.AppendLine("And DeptCode In (Select DeptCode From TblEmployee Where EmpCode=@EmpCode And DeptCode Is Not Null) ");
            if (mIsApprovalLeaveRequestByLevelCode)
            {
                SQL.AppendLine("And LevelCode Is Not Null ");
                SQL.AppendLine("And Find_in_set( ");
                SQL.AppendLine("( ");
                SQL.AppendLine(" Select Levelcode From tblemployee Where EmpCode=@EmpCode And LevelCode Is not Null ");
                SQL.AppendLine(" ), LevelCode ");
                SQL.AppendLine(") ");
            }

            if (Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='EmpLeave' And DAGCode Is Not Null Limit 1;"))
            {
                SQL.AppendLine("And (DAGCode Is Null Or ");
                SQL.AppendLine("(DAGCode Is Not Null ");
                SQL.AppendLine("And DAGCode In ( ");
                SQL.AppendLine("    Select A.DAGCode ");
                SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                SQL.AppendLine("    And A.ActInd='Y' ");
                SQL.AppendLine("    And B.EmpCode=@EmpCode ");
                SQL.AppendLine("))) ");
            }

            if (mIsDocApprovalSettingUseLeaveCode || mIsApprovalLeaveRequestByLevelCode)
            {
                SQL.AppendLine("And LeaveCode Is Not Null And Find_In_Set(@LeaveCode, LeaveCode) ");
            }

            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblEmpLeaveHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='EmpLeave' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            if (IsUseLeaveSummary)
            {
                SQL.AppendLine("Update TblLeaveSummary Set ");
                SQL.AppendLine("    Balance=Balance-@DurationDay, ");
                SQL.AppendLine("    NoOfDays2=NoOfDays2+@DurationDay, ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=@Dt ");
                SQL.AppendLine("Where Yr=@PeriodYr ");
                SQL.AppendLine("And EmpCode=@EmpCode ");
                SQL.AppendLine("And LeaveCode=@LeaveCode; ");
            }

            if (l.Count > 0)
            {
                SQL.AppendLine("Insert Into TblEmpLeaveDtl ");
                SQL.AppendLine("(DocNo, DNo, LeaveDt, CreateBy, CreateDt) ");
                SQL.AppendLine("Values ");

                for (int i = 0; i < l.Count; i++)
                {
                    if (!l[i].HolidayInd)
                    {
                        DNo += 1;
                        if (IsFirst)
                            IsFirst = false;
                        else
                            SQL.AppendLine(", ");
                        SQL.AppendLine("(@DocNo, @DNo_" + DNo.ToString() + ", @LeaveDt_" + DNo.ToString() + ", @UserCode, @Dt) ");

                        if (MaxNoofDay < 1000)
                            Sm.CmParam<String>(ref cm, "@DNo_" + DNo.ToString(), Sm.Right(string.Concat("00", i.ToString()), 3));
                        else
                            Sm.CmParam<String>(ref cm, "@DNo_" + DNo.ToString(), Sm.Right(string.Concat("000", i.ToString()), 4));
                        Sm.CmParam<String>(ref cm, "@LeaveDt_" + DNo.ToString(), l[i].LeaveDt);
                    }
                }
                SQL.AppendLine("; ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetLue(LueLeaveCode));
            Sm.CmParam<String>(ref cm, "@LeaveType", Sm.GetLue(LueLeaveType));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@LeaveStartDt", Sm.GetDte(DteJoinDt));
            Sm.CmParam<String>(ref cm, "@PeriodYr", TxtPeriodYr.Text);
            if (!mIsEmpLeaveDurationDayCalculateHoliday)
                Sm.CmParam<Decimal>(ref cm, "@DurationDay", DurationDay);
            else
                Sm.CmParam<Decimal>(ref cm, "@DurationDay", Decimal.Parse(TxtDurationDay.Text));
            Sm.CmParam<Decimal>(ref cm, "@DurationHour", Decimal.Parse(TxtDurationHour.Text));
            Sm.CmParam<String>(ref cm, "@StartTm", Sm.GetTme(TmeStartTm));
            Sm.CmParam<String>(ref cm, "@EndTm", Sm.GetTme(TmeEndTm));
            Sm.CmParam<String>(ref cm, "@BreakInd", ChkBreakInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Address", MeeAddress.Text);
            Sm.CmParam<String>(ref cm, "@Phone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code

        //private MySqlCommand SaveEmpLeaveHdr(string DocNo, bool IsUseLeaveSummary, decimal NumberOfLeaveDays)
        //{
        //    decimal DurationDay = NumberOfLeaveDays;
        //    var LeaveType = Sm.GetLue(LueLeaveType);
            
        //    if (LeaveType == "O" || LeaveType == "T")
        //        DurationDay = NumberOfLeaveDays * 0.5m;
            
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblEmpLeaveHdr ");
        //    SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, Status, ");
        //    SQL.AppendLine("LeaveCode, LeaveType, StartDt, EndDt, ");
        //    SQL.AppendLine("EmpCode, LeaveStartDt, PeriodYr, ");
        //    SQL.AppendLine("DurationDay, DurationHour, StartTm, EndTm, BreakInd, Remark, ");
        //    SQL.AppendLine("CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', 'O', ");
        //    SQL.AppendLine("@LeaveCode, @LeaveType, @StartDt, @EndDt, ");
        //    SQL.AppendLine("@EmpCode, @LeaveStartDt, @PeriodYr, ");
        //    SQL.AppendLine("@DurationDay, @DurationHour, @StartTm, @EndTm, @BreakInd, @Remark, ");
        //    SQL.AppendLine("@UserCode, CurrentDateTime()); ");

        //    SQL.AppendLine("Insert Into TblDocApproval ");
        //    SQL.AppendLine("(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @UserCode, CurrentDateTime() ");
        //    SQL.AppendLine("From TblDocApprovalSetting ");
        //    SQL.AppendLine("Where DocType='EmpLeave' ");
        //    if (mIsApprovalBySite)
        //        SQL.AppendLine("And SiteCode In (Select SiteCode From TblEmployee Where EmpCode=@EmpCode And SiteCode Is Not Null) ");
        //    if (mIsApprovalByDept)
        //        SQL.AppendLine("And DeptCode In (Select DeptCode From TblEmployee Where EmpCode=@EmpCode And DeptCode Is Not Null) ");
        //    if (mIsApprovalLeaveRequestByLevelCode)
        //    {
        //        SQL.AppendLine("And LevelCode Is Not Null ");
        //        SQL.AppendLine("And Find_in_set( ");
        //        SQL.AppendLine("( ");
        //        SQL.AppendLine(" Select Levelcode From tblemployee Where EmpCode=@EmpCode And LevelCode Is not Null ");
        //        SQL.AppendLine(" ), LevelCode ");
        //        SQL.AppendLine(") ");
        //    }

        //    if (Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='EmpLeave' And DAGCode Is Not Null Limit 1;"))
        //    {
        //        SQL.AppendLine("And (DAGCode Is Null Or ");
        //        SQL.AppendLine("(DAGCode Is Not Null ");
        //        SQL.AppendLine("And DAGCode In ( ");
        //        SQL.AppendLine("    Select A.DAGCode ");
        //        SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
        //        SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
        //        SQL.AppendLine("    And A.ActInd='Y' ");
        //        SQL.AppendLine("    And B.EmpCode=@EmpCode ");
        //        SQL.AppendLine("))) ");
        //    }

        //    if (mIsDocApprovalSettingUseLeaveCode || mIsApprovalLeaveRequestByLevelCode)
        //    {
        //        SQL.AppendLine("And LeaveCode Is Not Null And Find_In_Set(@LeaveCode, LeaveCode) ");
        //    }

        //    SQL.AppendLine("; ");

        //    SQL.AppendLine("Update TblEmpLeaveHdr Set Status='A' ");
        //    SQL.AppendLine("Where DocNo=@DocNo ");
        //    SQL.AppendLine("And Not Exists( ");
        //    SQL.AppendLine("    Select 1 From TblDocApproval ");
        //    SQL.AppendLine("    Where DocType='EmpLeave' And DocNo=@DocNo ");
        //    SQL.AppendLine("); ");

        //    if (IsUseLeaveSummary)
        //    {
        //        SQL.AppendLine("Update TblLeaveSummary Set ");
        //        SQL.AppendLine("    Balance=Balance-@DurationDay, ");
        //        SQL.AppendLine("    NoOfDays2=NoOfDays2+@DurationDay, ");
        //        SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
        //        SQL.AppendLine("Where Yr=@PeriodYr ");
        //        SQL.AppendLine("And EmpCode=@EmpCode ");
        //        SQL.AppendLine("And LeaveCode=@LeaveCode; ");
        //    }

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetLue(LueLeaveCode));
        //    Sm.CmParam<String>(ref cm, "@LeaveType", Sm.GetLue(LueLeaveType));
        //    Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
        //    Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
        //    Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
        //    Sm.CmParamDt(ref cm, "@LeaveStartDt", Sm.GetDte(DteJoinDt));
        //    Sm.CmParam<String>(ref cm, "@PeriodYr", TxtPeriodYr.Text);
        //    if (!mIsEmpLeaveDurationDayCalculateHoliday)
        //        Sm.CmParam<Decimal>(ref cm, "@DurationDay", DurationDay);
        //    else
        //        Sm.CmParam<Decimal>(ref cm, "@DurationDay", Decimal.Parse(TxtDurationDay.Text));
        //    Sm.CmParam<Decimal>(ref cm, "@DurationHour", Decimal.Parse(TxtDurationHour.Text));
        //    Sm.CmParam<String>(ref cm, "@StartTm", Sm.GetTme(TmeStartTm));
        //    Sm.CmParam<String>(ref cm, "@EndTm", Sm.GetTme(TmeEndTm));
        //    Sm.CmParam<String>(ref cm, "@BreakInd", ChkBreakInd.Checked ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveEmpLeaveDtl(string DocNo, string LeaveDt, int i)
        //{
        //    var SQL = new StringBuilder();
        //    int MaxNoofDay = 0;
        //    MaxNoofDay = Convert.ToInt32(Sm.GetValue("select max(cast(noofday As Int)) From tblLeave where actInd = 'Y';"));

        //    SQL.AppendLine("Insert Into TblEmpLeaveDtl ");
        //    SQL.AppendLine("(DocNo, DNo, LeaveDt, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @LeaveDt, @UserCode, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    if (MaxNoofDay < 1000)
        //        Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("00", i.ToString()), 3));
        //    else
        //        Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("000", i.ToString()), 4));
        //    Sm.CmParam<String>(ref cm, "@LeaveDt", LeaveDt);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        private void SetEmpLeaveDtl(ref List<EmpLeaveDtl> l)
        {
            SetEmpLeaveDtl1(ref l);
            if (l.Count>0) SetEmpLeaveDtl2(ref l);
        }

        private void SetEmpLeaveDtl1(ref List<EmpLeaveDtl> l)
        {
            string StartDt = Sm.GetDte(DteStartDt).Substring(0, 8);

            l.Add(new EmpLeaveDtl() { LeaveDt = StartDt, HolidayInd = false });

            if (!DteEndDt.Properties.ReadOnly)
            {
                string Dt = string.Empty;
                string EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);
                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    );
                DateTime Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

                var TotalDays = (Dt2 - Dt1).Days;
                for (int i = 1; i <= TotalDays; i++)
                {
                    Dt1 = Dt1.AddDays(1);
                    Dt =
                        Dt1.Year.ToString() +
                        ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                        ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2);

                    l.Add(new EmpLeaveDtl() { LeaveDt = Dt, HolidayInd = false });
                }
            }
        }

        private void SetEmpLeaveDtl2(ref List<EmpLeaveDtl> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, Dt = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {    
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.Dt=@Dt0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@Dt0" + i.ToString(), l[i].LeaveDt);
            }
            

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ");";
            else
                Filter = " And 0=1;";

            SQL.AppendLine("Select A.Dt ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode=B.WSCode And B.HolidayInd='Y' ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine(Filter);

            Sm.CmParam<string>(ref cm, "@EmpCode", TxtEmpCode.Text);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Dt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Dt = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(Dt, l[i].LeaveDt))
                            {
                                l[i].HolidayInd = true;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsCancelledDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelEmpLeave(IsLeaveUseLeaveSummary()));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblEmpLeaveHdr Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private MySqlCommand CancelEmpLeave(bool IsLeaveUseLeaveSummary)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Leave Request - Cancel */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Update TblEmpLeaveHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=@Dt ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Status In ('O', 'A'); ");

            if (IsLeaveUseLeaveSummary)
            {
                SQL.AppendLine("Update TblLeaveSummary Set ");
                SQL.AppendLine("    NoOfDays2=NoOfDays2-");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("        Select Sum(DurationDay) As Value From ( ");
                SQL.AppendLine("            Select 1.00* ");
                SQL.AppendLine("            Case A.LeaveType When 'F' Then 1.00 When 'O' Then 0.50 When 'T' Then 0.50 Else 0.00 End ");
                SQL.AppendLine("            As DurationDay ");
                SQL.AppendLine("            From TblEmpLeaveHdr A, TblEmpLeaveDtl B ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("            And A.DocNo=B.DocNo ");
                SQL.AppendLine("            ) T ");
                SQL.AppendLine("        ), 0.00), ");
                SQL.AppendLine("    Balance=Balance+IfNull(( ");
                SQL.AppendLine("        Select Sum(DurationDay) As Value From ( ");
                SQL.AppendLine("            Select 1.00* ");
                SQL.AppendLine("            Case A.LeaveType When 'F' Then 1.00 When 'O' Then 0.50 When 'T' Then 0.50 Else 0.00 End ");
                SQL.AppendLine("            As DurationDay ");
                SQL.AppendLine("            From TblEmpLeaveHdr A, TblEmpLeaveDtl B ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("            And A.DocNo=B.DocNo ");
                SQL.AppendLine("            ) T ");
                SQL.AppendLine("        ), 0.00), ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=@Dt ");
                SQL.AppendLine("Where Yr=@PeriodYr ");
                SQL.AppendLine("And EmpCode=@EmpCode ");
                SQL.AppendLine("And LeaveCode=@LeaveCode; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@PeriodYr", TxtPeriodYr.Text);
            Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetLue(LueLeaveCode));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmpLeaveHdr(DocNo);
                ShowEmpLeaveDtl(DocNo);
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpLeaveHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select DocNo, DocDt, CancelReason, CancelInd, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancel' ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("A.LeaveCode, A.LeaveType, A.StartDt, A.EndDt, ");
            SQL.AppendLine("A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, E.SiteName, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, B.LeaveStartDt, ");
            SQL.AppendLine("A.DurationDay, A.StartTm, A.EndTm, A.DurationHour, A.BreakInd, A.Remark, ");
            SQL.AppendLine("A.PeriodYr, F.StartDt As LeaveSummaryStartDt, F.EndDt As LeaveSummaryEndDt, F.Balance, A.Address, A.Phone ");
            SQL.AppendLine("From TblEmpLeaveHdr A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblSite E On B.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join TblLeaveSummary F On A.PeriodYr=F.Yr And A.EmpCode=F.EmpCode And A.LeaveCode=F.LeaveCode ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "StatusDesc", "LeaveCode", 

                        //6-10
                        "LeaveType", "StartDt", "EndDt", "EmpCode", "EmpName", 
                        
                        //11-15
                        "EmpCodeOld", "PosName", "DeptName", "SiteName", "JoinDt", 
                        
                        //16-20
                        "ResignDt", "LeaveStartDt", "DurationDay", "StartTm", "EndTm", 
                        
                        //21-25
                        "DurationHour", "BreakInd", "Remark", "PeriodYr", "LeaveSummaryStartDt", 
                        
                        //26-29
                        "LeaveSummaryEndDt", "Balance", "Address", "Phone"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[3])=="Y";
                        TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                        Sl.SetLueLeaveCode(ref LueLeaveCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueLeaveType, Sm.DrStr(dr, c[6]));
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[7]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[8]));
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[9]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[10]);
                        TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[11]);
                        TxtPosCode.EditValue = Sm.DrStr(dr, c[12]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[13]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[14]);
                        Sm.SetDte(DteJoinDt, Sm.DrStr(dr, c[15]));
                        Sm.SetDte(DteResignDt, Sm.DrStr(dr, c[16]));
                        Sm.SetDte(DteLeaveStartDt, Sm.DrStr(dr, c[17]));
                        TxtDurationDay.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                        Sm.SetTme(TmeStartTm, Sm.DrStr(dr, c[19]));
                        Sm.SetTme(TmeEndTm, Sm.DrStr(dr, c[20]));
                        TxtDurationHour.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[21]), 0);
                        ChkBreakInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[22]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[23]);
                        TxtPeriodYr.EditValue = Sm.DrStr(dr, c[24]);
                        if (TxtPeriodYr.Text.Length > 0)
                        {
                            Sm.SetDte(DteLeaveSummaryStartDt, Sm.DrStr(dr, c[25]));
                            Sm.SetDte(DteLeaveSummaryEndDt, Sm.DrStr(dr, c[26]));
                            if (ChkCancelInd.Checked)
                                TxtBalance.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[27]), 0);
                            else
                                TxtBalance.EditValue = 
                                    Sm.FormatNum(Sm.DrDec(dr, c[27]) + 
                                    decimal.Parse(TxtDurationDay.Text), 0);
                        }
                        MeeAddress.EditValue = Sm.DrStr(dr, c[28]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[29]);
                    }, true
                );
        }

        private void ShowEmpLeaveDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select LeaveDt From TblEmpLeaveDtl Where DocNo=@DocNo Order By LeaveDt;",
                    new string[] { "LeaveDt" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    { Sm.SetGrdValue("D", Grd, dr, c, Row, 0, 0); }, 
                    false, false, false, false
                );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode=B.UserCode ");
            SQL.AppendLine("Where A.DocType='EmpLeave' ");
            SQL.AppendLine("And IfNull(Status, 'O') In ('A', 'C') ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    SQL.ToString(),
                    new string[]{ "UserName", "StatusDesc", "LastUpDt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsLongServiceLeaveUseWorkingPeriodValidation', 'IsDocApprovalSettingUseLeaveCode', 'IsApprovalLeaveRequestByLevelCode', 'IsFilterByDeptHR', 'IsFilterBySiteHR', ");
            SQL.AppendLine("'LongServiceLeaveCode', 'LongServiceLeaveCode2', 'AnnualLeaveCode', 'IsEmpLeaveDurationDayCalculateHoliday', 'IsLongServiceLeaveUseWarningLetterValidation', ");
            SQL.AppendLine("'LeaveCodeWithMinimumNoOfDay', 'LeaveCodeForLongServiceLeaveValidation', 'FormPrintOutLeaveRequest', 'EmpLeaveReqAutoApprovedByEmpCode', 'NeglectLeaveCode', ");
            SQL.AppendLine("'NoOfYearWorkingPeriodLongServiceLeaveValidation', 'IsLeaveRequestUseAddressAndPhone', 'DocTitle' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                            case "IsFilterByDeptHR": mIsFilterByDeptHR = ParValue == "Y"; break;
                            case "IsApprovalLeaveRequestByLevelCode": mIsApprovalLeaveRequestByLevelCode = ParValue == "Y"; break;
                            case "IsDocApprovalSettingUseLeaveCode": mIsDocApprovalSettingUseLeaveCode = ParValue == "Y"; break;
                            case "IsLongServiceLeaveUseWorkingPeriodValidation": mIsLongServiceLeaveUseWorkingPeriodValidation = ParValue == "Y"; break;
                            case "IsLongServiceLeaveUseWarningLetterValidation": mIsLongServiceLeaveUseWarningLetterValidation = ParValue == "Y"; break;
                            case "IsEmpLeaveDurationDayCalculateHoliday": mIsEmpLeaveDurationDayCalculateHoliday = ParValue == "Y"; break;
                            case "IsLeaveRequestUseAddressAndPhone": mIsLeaveRequestUseAddressAndPhone = ParValue == "Y"; break;

                            //string
                            case "AnnualLeaveCode": mAnnualLeaveCode = ParValue; break;
                            case "LongServiceLeaveCode": mLongServiceLeaveCode = ParValue; break;
                            case "LongServiceLeaveCode2": mLongServiceLeaveCode2 = ParValue; break;
                            case "NeglectLeaveCode": mNeglectLeaveCode = ParValue; break;
                            case "EmpLeaveReqAutoApprovedByEmpCode": mEmpLeaveReqAutoApprovedByEmpCode = ParValue; break;
                            case "FormPrintOutLeaveRequest": mFormPrintOutLeaveRequest = ParValue; break;
                            case "LeaveCodeForLongServiceLeaveValidation": mLeaveCodeForLongServiceLeaveValidation = ParValue; break;
                            case "LeaveCodeWithMinimumNoOfDay": mLeaveCodeWithMinimumNoOfDay = ParValue; break;
                            case "DocTitle": mDocTitle = ParValue; break;

                            //Number
                            case "NoOfYearWorkingPeriodLongServiceLeaveValidation":
                                if (ParValue.Length>0) mNoOfYearWorkingPeriodLongServiceLeaveValidation = decimal.Parse(ParValue); 
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void GetValue()
        {
            mIsApprovalByDept = Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='EmpLeave' And DeptCode Is Not Null Limit 1;");
            mIsApprovalBySite = Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='EmpLeave' And SiteCode Is Not Null Limit 1;");
        }

        public void ComputeDurationDay()
        {
            TxtDurationDay.EditValue = 0;

            if (Sm.GetDte(DteStartDt).ToString().Length > 0 && Sm.GetDte(DteEndDt).ToString().Length > 0)
            {
                string
                    StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                    EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    );
                DateTime Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

                decimal holiday = ComputeHoliday(TxtEmpCode.Text, StartDt, EndDt);

                TxtDurationDay.EditValue = (((Dt2 - Dt1).Days + 1) - holiday);

            }
        }

        private static decimal ComputeHoliday(string EmpCode, string Dt1, string Dt2)
        {
            decimal HolidayAmt = 0m;
            string Value = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Count(1) ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode=B.WSCode And B.HolidayInd='Y' ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("And A.Dt Between @Dt1 And @Dt2;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@Dt1", Dt1);
            Sm.CmParamDt(ref cm, "@Dt2", Dt2);

            try
            {
                Value = Sm.GetValue(cm);
                if (Value.Length > 0) HolidayAmt = decimal.Parse(Value.ToString());
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return HolidayAmt;
        }

        public void ComputeDurationHour()
        {
            TxtDurationHour.EditValue = 0;

            if (Sm.GetDte(DteStartDt).ToString().Length > 0 &&
                Sm.GetTme(TmeStartTm).ToString().Length > 0 &&
                Sm.GetTme(TmeEndTm).ToString().Length > 0)
            {
                string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                StartTm = Sm.GetTme(TmeStartTm),
                EndTm = Sm.GetTme(TmeEndTm);

                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    Int32.Parse(StartTm.Substring(0, 2)),
                    Int32.Parse(StartTm.Substring(2, 2)),
                    0
                    );
                DateTime Dt2 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    Int32.Parse(EndTm.Substring(0, 2)),
                    Int32.Parse(EndTm.Substring(2, 2)),
                    0
                    );

                if (Dt2 < Dt1) Dt2 = Dt2.AddDays(1);
                double TotalHours = (Dt2 - Dt1).TotalHours;

                if (ChkBreakInd.Checked) TotalHours -= 1;
                if (TotalHours < 0) TotalHours = 0;

                if (TotalHours - (int)(TotalHours) > 0.5)
                    TxtDurationHour.EditValue = (int)(TotalHours) + 1;
                else
                {
                    if (TotalHours - (int)(TotalHours) == 0)
                        TxtDurationHour.EditValue = TotalHours;
                    else
                    {
                        if (TotalHours - (int)(TotalHours) > 0 &&
                            TotalHours - (int)(TotalHours) <= 0.5)
                            TxtDurationHour.EditValue = (int)(TotalHours) + 0.5;
                    }
                }


            }
        }

        private string GetBreakSchedule()
        {
            if (TxtEmpCode.Text.Length == 0 || Sm.GetDte(DteStartDt).Length == 0) return string.Empty;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(In2, Out2) As Break ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkschedule B On A.WSCode=B.WSCode ");
            SQL.AppendLine("Where A.Dt=@Dt And A.EmpCode=@EmpCode ");
            SQL.AppendLine("And In2 Is Not Null And Out2 Is Not Null ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteStartDt));

            var b = Sm.GetValue(cm);
            if (b.Length == 0)
                return string.Empty;
            else
                return
                    " (" +
                    Sm.Left(b, 2) + ":" + b.Substring(2, 2) + " - " +
                    b.Substring(4, 2) + ":" + Sm.Right(b, 2) +
                    ")";
        }

        private void ParPrint()
        {
            if (Sm.StdMsgYN("Print", "") == DialogResult.No || Sm.IsTxtEmpty(TxtDocNo, "Document#", false)) return;

            string[] TableName = { "EmpLeave", "EmpLeaveSign", "EmpLeaveSign2", "SignApproval", "EmpLeaveSumary" };

            var l = new List<EmpLeave>();
            var lDtlS = new List<EmpLeaveSign>();
            var lDtlS2 = new List<EmpLeaveSign2>();
            var lDtlS3 = new List<EmpLeaveSumary>();
            var lSignApproval = new List<SignApproval>();

            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("CONCAT(");
	        SQL.AppendLine("TRIM(LEADING '0' from LEFT(A.DocNo,(select parvalue from tblparameter where parcode = 'DocSeqNo')))");
	        SQL.AppendLine(",'/',A.LeaveCode,'/SDM/DIR/',LEFT(A.DocDT,4)");
            SQL.AppendLine(")as Nomor,");
            SQL.AppendLine("A.DocNo, Concat(DATE_FORMAT(docdt, '%d'), ' ', ");
            SQL.AppendLine("case when DATE_FORMAT(docdt, '%m') = '01' then 'Januari' ");
            SQL.AppendLine("when DATE_FORMAT(docdt, '%m') = '02' then 'Februari' ");
            SQL.AppendLine("when DATE_FORMAT(docdt, '%m') = '03' then 'Maret'  ");
            SQL.AppendLine("when DATE_FORMAT(docdt, '%m') = '04' then 'April' ");
            SQL.AppendLine("when DATE_FORMAT(docdt, '%m') = '05' then 'Mei' ");
            SQL.AppendLine("when DATE_FORMAT(docdt, '%m') = '06' then 'Juni' ");
            SQL.AppendLine("when DATE_FORMAT(docdt, '%m') = '07' then 'Juli' ");
            SQL.AppendLine("when DATE_FORMAT(docdt, '%m') = '08' then 'Agustus' ");
            SQL.AppendLine("when DATE_FORMAT(docdt, '%m') = '09' then 'September' ");
            SQL.AppendLine("when DATE_FORMAT(docdt, '%m') = '10' then 'Oktober' ");
            SQL.AppendLine("when DATE_FORMAT(docdt, '%m') = '11' then 'November' ");
            SQL.AppendLine("when DATE_FORMAT(docdt, '%m') = '12' then 'December' END, ' ',  ");
            SQL.AppendLine("DATE_FORMAT(DocDt, '%Y')) AS DocDt, ");
            SQL.AppendLine("A.EmpCode, B.EmpName, C.DeptName, B.Address, A.Address Domicile, B.Mobile, ");
            SQL.AppendLine("IfNull(A.Remark, D.LeaveName) Remark, ");
            SQL.AppendLine("LOWER(D.LeaveName) LeaveName, A.DurationDay, IFNULL(DATE_FORMAT(A.StartDt, '%d-%b-%Y'), '') StartDt, ");
            SQL.AppendLine("IFNULL(DATE_FORMAT(A.EndDt, '%d-%b-%Y'), '') EndDt, A.DurationHour, ");
            SQL.AppendLine("IF(A.StartTm IS NULL, '', DATE_FORMAT(CONCAT('20101010', A.StartTm, '00'), '%H:%i')) StartTm, ");
            SQL.AppendLine("IF(A.EndTm IS NULL, '', DATE_FORMAT(CONCAT('20101010', A.EndTm, '00'), '%H:%i')) EndTm, B.EmpCodeOld,E.PosName,F.LevelName ");
            SQL.AppendLine(",LEFT(A.DocDt,4) years, A.DurationDay days, A.Phone, A.Status ");
            if(mDocTitle=="PHT")
                SQL.AppendLine(", I.CityName ");
            else
                SQL.AppendLine(", H.CityName ");

            //PHT
            SQL.AppendLine(",CONCAT (");
            SQL.AppendLine("case when A.StartDt = A.EndDt");
            SQL.AppendLine("THEN concat(RIGHT(A.StartDt,2),' ')");
            SQL.AppendLine("ELSE");
            SQL.AppendLine("CONCAT (RIGHT(A.StartDt,2),");

            SQL.AppendLine("CASE WHEN LEFT (a.StartDt,6) != LEFT (a.enddt,6) then");
            SQL.AppendLine("CONCAT(' ', CASE MONTH(A.StartDt)");
            SQL.AppendLine("WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' WHEN 4 THEN 'April'");
            SQL.AppendLine("WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus'");
            SQL.AppendLine("WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END");
            SQL.AppendLine(",' ', LEFT (a.StartDt,4)) else '' END,");
            
            SQL.AppendLine("' - ',RIGHT(A.EndDt,2),' ') END,");
            SQL.AppendLine("CASE MONTH(A.Enddt)");
            SQL.AppendLine("WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' WHEN 4 THEN 'April'");
            SQL.AppendLine("WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus'");
            SQL.AppendLine("WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END");
            SQL.AppendLine(",' ', LEFT (a.enddt,4)");
            SQL.AppendLine(") leavedtperiod, ");
            SQL.AppendLine("G.SiteName, G.Address SiteAddress ");
            SQL.AppendLine("FROM TblEmpLeaveHdr A ");
            SQL.AppendLine("INNER JOIN TblEmployee B ON A.EmpCode = B.EmpCode ");
            SQL.AppendLine("AND A.DocNo = @DocNo ");
            SQL.AppendLine("INNER JOIN TblDepartment C ON B.DeptCode = C.DeptCode ");
            SQL.AppendLine("INNER JOIN TblLeave D ON A.LeaveCode = D.LeaveCode ");
            SQL.AppendLine("INNER JOIN tblposition E ON B.PosCode = E.PosCode ");
            SQL.AppendLine("INNER JOIN tbllevelhdr F ON B.LevelCode = F.LevelCode");
            SQL.AppendLine("INNER JOIN tblsite G ON B.SiteCode = G.SiteCode");
            SQL.AppendLine("INNER JOIN tblcity H ON G.CityCode = H.CityCode");
            if(mDocTitle=="PHT")
            {
                SQL.AppendLine("LEFT JOIN ( ");
                SQL.AppendLine("    SELECT I1.DocNo, I5.CityName ");
                SQL.AppendLine("    FROM tbldocapproval I1 ");
                SQL.AppendLine("    INNER JOIN tbldocapprovalsetting I2 ON I2.DocType='EmpLeave' AND I1.ApprovalDNo=I2.DNo AND I1.DocType=I2.DocType ");
                SQL.AppendLine("    INNER JOIN tblemployee I3 ON REPLACE(I2.UserCode, '#', '')=I3.UserCode ");
                SQL.AppendLine("    INNER JOIN tblsite I4 ON I3.SiteCode=I4.SiteCode ");
                SQL.AppendLine("    INNER JOIN tblcity I5 ON I4.CityCode=I5.CityCode ");
                SQL.AppendLine("    WHERE I1.DocNo = @DocNo AND I2.`Level` = ( ");
                SQL.AppendLine("        SELECT MAX(X2.`Level`) FROM tbldocapproval X1 ");
                SQL.AppendLine("        INNER JOIN tbldocapprovalsetting X2 ON X2.DocType='EmpLeave' AND X1.ApprovalDNo=X2.DNo AND X1.DocNo=@DocNo) ");
                SQL.AppendLine(") I ON A.DocNo = I.DocNo ");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "Nomor",
                         

                         //6-10
                         "DocNo", 
                         "DocDt",
                         "EmpName",
                         "DeptName",
                         "Remark",

                         //11-15
                         "StartDt",
                         "EndDt",
                         "StartTm",
                         "EndTm",
                         "LeaveName",
                         
                         //16-20
                         "EmpCodeOld",
                         "PosName",
                         "LevelName",
                         "years",
                         "days",

                         //21-25
                         "Address",
                         "Domicile",
                         "Phone",
                         "CityName",
                         "Status",

                         //26-28
                         "leavedtperiod",
                         "SiteName",
                         "SiteAddress"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpLeave()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            Nomor = Sm.DrStr(dr,c[5]),
                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            EmpName = Sm.DrStr(dr, c[8]),
                            DeptName = Sm.DrStr(dr, c[9]),
                            Remark = Sm.DrStr(dr, c[10]),
                            StartDt = Sm.DrStr(dr, c[11]),
                            EndDt = Sm.DrStr(dr,c[12]),
                            StartTm = Sm.DrStr(dr, c[13]),
                            EndTm = Sm.DrStr(dr, c[14]),
                            LeaveName = Sm.DrStr(dr, c[15]),
                            EmpCodeOld = Sm.DrStr(dr,c[16]),
                            PosName = Sm.DrStr(dr,c[17]),
                            LevelName = Sm.DrStr(dr, c[18]),
                            years = Sm.DrStr(dr,c[19]),
                            days = Convert.ToInt32((Sm.DrDec(dr, c[20]))),
                            Address = Sm.DrStr(dr,c[21]),
                            Domicile = Sm.DrStr(dr, c[22]),
                            Phone = Sm.DrStr(dr,c[23]),
                            CityName = Sm.DrStr(dr, c[24]),
                            Status = Sm.DrStr(dr, c[25]),

                            //PHT
                            LeaveDtPeriod = Sm.DrStr(dr, c[26]),
                            SiteName = Sm.DrStr(dr, c[27]),
                            SiteAddress = Sm.DrStr(dr, c[28]),
                            DaysText = Sm.Terbilang(Sm.DrDec(dr, c[20])).Replace(" rupiah",""),
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);
            #endregion

            #region Detail Signature IMS

            //Dibuat Oleh
            var cmDtlS = new MySqlCommand();

            var SQLDtlS = new StringBuilder();
            using (var cnDtlS = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS.Open();
                cmDtlS.Connection = cnDtlS;

                SQLDtlS.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtlS.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtlS.AppendLine("From ( ");
                SQLDtlS.AppendLine("    Select Distinct ");
                SQLDtlS.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, '1' As DNo, 0 As Level, 'Prepared By,' As Title, Left(A.CreateDt, 8) As LastUpDt  ");
                SQLDtlS.AppendLine("    From TblEmpLeaveHdr A ");
                SQLDtlS.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtlS.AppendLine("    Where  A.DocNo=@DocNo ");
                SQLDtlS.AppendLine(") T1 ");
                SQLDtlS.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtlS.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtlS.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtlS.AppendLine("Group By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Title ");
                SQLDtlS.AppendLine("Order By T1.Level; ");

                cmDtlS.CommandText = SQLDtlS.ToString();
                Sm.CmParam<String>(ref cmDtlS, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS, "@DocNo", TxtDocNo.Text);
                var drDtlS = cmDtlS.ExecuteReader();
                var cDtlS = Sm.GetOrdinal(drDtlS, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",

                         //6
                         "LastupDt"
                        });
                if (drDtlS.HasRows)
                {
                    while (drDtlS.Read())
                    {
                        lDtlS.Add(new EmpLeaveSign()
                        {
                            Signature = Sm.DrStr(drDtlS, cDtlS[0]),
                            UserName = Sm.DrStr(drDtlS, cDtlS[1]),
                            PosName = Sm.DrStr(drDtlS, cDtlS[2]),
                            Space = Sm.DrStr(drDtlS, cDtlS[3]),
                            DNo = Sm.DrStr(drDtlS, cDtlS[4]),
                            Title = Sm.DrStr(drDtlS, cDtlS[5]),
                            LastUpDt = Sm.DrStr(drDtlS, cDtlS[6])
                        });
                    }
                }
                drDtlS.Close();
            }
            myLists.Add(lDtlS);

            //Disetujui Oleh
            var cmDtlS2 = new MySqlCommand();

            var SQLDtlS2 = new StringBuilder();
            using (var cnDtlS2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS2.Open();
                cmDtlS2.Connection = cnDtlS2;

                SQLDtlS2.AppendLine("Select A.EmpName, B.PosName  ");
                SQLDtlS2.AppendLine("From TblEmployee A  ");
                SQLDtlS2.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode  ");
                SQLDtlS2.AppendLine("Where A.EmpCode = @EmpCode  ");

                cmDtlS2.CommandText = SQLDtlS2.ToString();
                Sm.CmParam<String>(ref cmDtlS2, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS2, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmDtlS2, "@EmpCode", mEmpLeaveReqAutoApprovedByEmpCode);
                var drDtlS2 = cmDtlS2.ExecuteReader();
                var cDtlS2 = Sm.GetOrdinal(drDtlS2, new string[] 
                        {
                         //0
                         "EmpName" ,

                         //1
                         "PosName" 
                        });
                if (drDtlS2.HasRows)
                {
                    while (drDtlS2.Read())
                    {

                        lDtlS2.Add(new EmpLeaveSign2()
                        {
                            EmpName = Sm.DrStr(drDtlS2, cDtlS2[0]),
                            PosName = Sm.DrStr(drDtlS2, cDtlS2[1]),
                        });
                    }
                }
                drDtlS2.Close();
            }
            myLists.Add(lDtlS2);

            #endregion

            #region signature approval
            var cml = new MySqlCommand();
            var SQLl = new StringBuilder();
            var i = 0;
            int[] level = new int[3];

            SQLl.AppendLine("SELECT B.`Level` Level ");
            SQLl.AppendLine("FROM tbldocapproval A ");
            SQLl.AppendLine("INNER JOIN tbldocapprovalsetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo ");
            SQLl.AppendLine("WHERE A.DocType = 'EmpLeave' AND A.DocNo = @DocNo ");
            SQLl.AppendLine("ORDER BY B.`Level` ");
            SQLl.AppendLine("LIMIT 3;");

            using (var cnl = new MySqlConnection(Gv.ConnectionString))
            {
                cnl.Open();
                cml.Connection = cnl;
                cml.CommandText = SQLl.ToString();
                Sm.CmParam<String>(ref cml, "@DocNo", TxtDocNo.Text);
                var dr = cml.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "Level"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        level[i] = Sm.DrInt(dr, c[0]);
                        i = i + 1;
                    }
                }
                dr.Close();
            }

            var cmSignaturApproval = new MySqlCommand();

            var SQLSignaturApproval = new StringBuilder();


                SQLSignaturApproval.AppendLine("SELECT * FROM ");
                SQLSignaturApproval.AppendLine("( ");
	            SQLSignaturApproval.AppendLine("    SELECT A.Docno , D.EmpName AS username1, E.PosName AS posname1, D.EmpCodeOld AS NIK1, A.Remark Remark1, ");
	            SQLSignaturApproval.AppendLine("    Case When A.`Status`='A' Then 'Approved' When A.`Status`='C' Then 'Cancel' End Status1 ");
	            SQLSignaturApproval.AppendLine("    FROM tbldocapproval A ");
	            SQLSignaturApproval.AppendLine("    INNER JOIN tbldocapprovalsetting B ON A.DocType = B.DocType");
		        SQLSignaturApproval.AppendLine("    AND A.DocNo = @DocNo"); 
		        SQLSignaturApproval.AppendLine("    AND B.DNo = A.ApprovalDNo");
		        SQLSignaturApproval.AppendLine("    AND B.`Level` = '" + level[0] + "' ");
		        SQLSignaturApproval.AppendLine("    AND (A.`Status` = 'A' OR A.`Status` = 'C') ");
		        SQLSignaturApproval.AppendLine("    AND A.DocType = 'EmpLeave' ");
                SQLSignaturApproval.AppendLine("    LEFT JOIN tbluser C ON A.UserCode = C.UserCode ");
                SQLSignaturApproval.AppendLine("    LEFT JOIN tblemployee D ON A.UserCode = D.UserCode ");
                SQLSignaturApproval.AppendLine("    LEFT JOIN tblposition E ON D.PosCode = E.PosCode ");
                SQLSignaturApproval.AppendLine(")T1 ");
                SQLSignaturApproval.AppendLine("LEFT JOIN  ");
                SQLSignaturApproval.AppendLine("( ");
                SQLSignaturApproval.AppendLine("    SELECT A.Docno, D.EmpName AS username2, E.PosName AS posname2, D.EmpCodeOld AS NIK2, A.Remark Remark2, ");
                SQLSignaturApproval.AppendLine("    Case When A.`Status`='A' Then 'Approved' When A.`Status`='C' Then 'Cancel' End Status2 ");
                SQLSignaturApproval.AppendLine("    FROM tbldocapproval A ");
	            SQLSignaturApproval.AppendLine("    INNER JOIN tbldocapprovalsetting B ON A.DocType = B.DocType ");
		        SQLSignaturApproval.AppendLine("    AND A.DocNo = @DocNo ");
                SQLSignaturApproval.AppendLine("    AND B.DNo = A.ApprovalDNo ");
                SQLSignaturApproval.AppendLine("    AND B.`Level` = '" + level[1] + "' ");
                SQLSignaturApproval.AppendLine("    AND (A.`Status` = 'A' OR A.`Status` = 'C') ");
                SQLSignaturApproval.AppendLine("    AND A.DocType = 'EmpLeave' ");
                SQLSignaturApproval.AppendLine("    LEFT JOIN tbluser C ON A.UserCode = C.UserCode ");
                SQLSignaturApproval.AppendLine("    LEFT JOIN tblemployee D ON A.UserCode = D.UserCode ");
                SQLSignaturApproval.AppendLine("    LEFT JOIN tblposition E ON D.PosCode = E.PosCode ");
                SQLSignaturApproval.AppendLine(")T2 ON T1.DocNo = T2.DocNo ");
                SQLSignaturApproval.AppendLine("LEFT JOIN  ");
                SQLSignaturApproval.AppendLine("( ");
                SQLSignaturApproval.AppendLine("    SELECT A.Docno, D.EmpName AS username3, E.PosName AS posname3, D.EmpCodeOld AS NIK3, A.Remark Remark3, ");
                SQLSignaturApproval.AppendLine("    Case When A.`Status`='A' Then 'Approved' When A.`Status`='C' Then 'Cancel' End Status3 ");
                SQLSignaturApproval.AppendLine("    FROM tbldocapproval A ");
                SQLSignaturApproval.AppendLine("    INNER JOIN tbldocapprovalsetting B ON A.DocType = B.DocType ");
                SQLSignaturApproval.AppendLine("    AND A.DocNo = @DocNo ");
                SQLSignaturApproval.AppendLine("    AND B.DNo = A.ApprovalDNo ");
                SQLSignaturApproval.AppendLine("    AND B.`Level` = '" + level[2] + "' ");
                SQLSignaturApproval.AppendLine("    AND (A.`Status` = 'A' OR A.`Status` = 'C') ");
                SQLSignaturApproval.AppendLine("    AND A.DocType = 'EmpLeave' ");
                SQLSignaturApproval.AppendLine("    LEFT JOIN tbluser C ON A.UserCode = C.UserCode ");
                SQLSignaturApproval.AppendLine("    LEFT JOIN tblemployee D ON A.UserCode = D.UserCode ");
                SQLSignaturApproval.AppendLine("    LEFT JOIN tblposition E ON D.PosCode = E.PosCode ");
                SQLSignaturApproval.AppendLine(")T3 ON T1.DocNo = T3.DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cmSignaturApproval.Connection = cn;
                cmSignaturApproval.CommandText = SQLSignaturApproval.ToString();
                Sm.CmParam<String>(ref cmSignaturApproval, "@DocNo", TxtDocNo.Text);
                var dr = cmSignaturApproval.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "Docno",

                         //1-5
                         "username1",
                         "posname1",
                         "NIK1",
                         "username2",
                         "posname2",
                         
                         
                         //6-10
                         "NIK2",
                         "username3",
                         "posname3",
                         "NIK3",
                         "Remark1",

                         //11-15
                         "Remark2",
                         "Remark3",
                         "Status1",
                         "Status2",
                         "Status3"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lSignApproval.Add(new SignApproval()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),

                            username1 = Sm.DrStr(dr, c[1]),
                            posname1 = Sm.DrStr(dr, c[2]),
                            NIK1 = Sm.DrStr(dr, c[3]),
                            username2 = Sm.DrStr(dr, c[4]),
                            posname2 = Sm.DrStr(dr, c[5]),
                            NIK2 = Sm.DrStr(dr, c[6]),
                            username3 = Sm.DrStr(dr, c[7]),
                            posname3 = Sm.DrStr(dr, c[8]),
                            NIK3 = Sm.DrStr(dr, c[9]),
                            Remark1 = Sm.DrStr(dr, c[10]),
                            Remark2 = Sm.DrStr(dr, c[11]),
                            Remark3 = Sm.DrStr(dr, c[12]),
                            Status1 = Sm.DrStr(dr, c[13]),
                            Status2 = Sm.DrStr(dr, c[14]),
                            Status3 = Sm.DrStr(dr, c[15]),
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(lSignApproval);
            #endregion 

            #region detailsumary
            var cmdtlsumary = new MySqlCommand();

            var SqlDtlSumary = new StringBuilder();

            SqlDtlSumary.AppendLine("Select T.* From");
            SqlDtlSumary.AppendLine("( ");
	        SqlDtlSumary.AppendLine("Select A.LeaveCode, A.LeaveName, ");
	        SqlDtlSumary.AppendLine("Case When B.CountData > 0 Then B.CountData ");
	        SqlDtlSumary.AppendLine("Else '-' ");
            SqlDtlSumary.AppendLine("End As CountData ");
	        SqlDtlSumary.AppendLine("From TblLeave A ");
	        SqlDtlSumary.AppendLine("Left Join ");
	        SqlDtlSumary.AppendLine("   ( ");
            SqlDtlSumary.AppendLine("Select T1.LeaveCode, Count(T2.LeaveDt) CountData"); 
            SqlDtlSumary.AppendLine("From TblEmpLeaveHdr T1 ");
            SqlDtlSumary.AppendLine("Inner Join TblEmpLeaveDtl T2 On T1.DocNo = T2.DocNo ");
            SqlDtlSumary.AppendLine("And T1.CancelInd = 'N' ");
            if (Sm.Right(Sm.GetDte(DteStartDt), 4) == Sm.Right(Sm.GetDte(DteEndDt), 4))
                SqlDtlSumary.AppendLine("And Left(T2.LeaveDt, 4) = @Yr ");
            else
                SqlDtlSumary.AppendLine("And Left(T2.LeaveDt, 4) Between @Yr And @Yr2 ");

            SqlDtlSumary.AppendLine("ANd T1.EmpCode = @EmpCode ");
            SqlDtlSumary.AppendLine("And Find_In_Set(T1.LeaveCode, (Select ParValue From TblParameter Where ParCode = 'LeaveCodeForPrintOut')) ");
            SqlDtlSumary.AppendLine("Group By T1.LeaveCode ");
            SqlDtlSumary.AppendLine(") B On A.LeaveCode = B.LeaveCode ");
            SqlDtlSumary.AppendLine(" Where Find_In_Set(A.LeaveCode, (Select ParValue From TblParameter Where ParCode = 'LeaveCodeForPrintOut')) ");
            SqlDtlSumary.AppendLine("Union All ");
            SqlDtlSumary.AppendLine("Select A.LeaveCode, A.LeaveName, ");
            SqlDtlSumary.AppendLine(" Case When A.CountData > 0 Then A.CountData ");
            SqlDtlSumary.AppendLine("Else '-' ");
            SqlDtlSumary.AppendLine("End As CountData ");
            SqlDtlSumary.AppendLine("From ");
            SqlDtlSumary.AppendLine("( ");
            SqlDtlSumary.AppendLine("Select IfNull(T1.LeaveCode, '0002') As LeaveCode, 'Cuti Besar' As LeaveName, Count(T2.LeaveDt) CountData ");
            SqlDtlSumary.AppendLine("From TblEmpLeaveHdr T1 ");
            SqlDtlSumary.AppendLine("Inner Join TblEmpLeaveDtl T2 On T1.DocNo = T2.DocNo ");
            SqlDtlSumary.AppendLine("And T1.CancelInd = 'N' ");
            if (Sm.Right(Sm.GetDte(DteStartDt),4) == Sm.Right(Sm.GetDte(DteEndDt),4))
                SqlDtlSumary.AppendLine("And Left(T2.LeaveDt, 4) = @Yr ");
            else
                SqlDtlSumary.AppendLine("And Left(T2.LeaveDt, 4) Between @Yr And @Yr2 ");
            SqlDtlSumary.AppendLine("ANd T1.EmpCode = @EmpCode ");
            SqlDtlSumary.AppendLine("And Find_In_Set(T1.LeaveCode, (Select ParValue From TblParameter Where ParCode = 'LongServiceLeaveCode3')) ");
            SqlDtlSumary.AppendLine(") A ");
            SqlDtlSumary.AppendLine("Union All ");
            SqlDtlSumary.AppendLine("Select A.LeaveCode, A.LeaveName, ");
            SqlDtlSumary.AppendLine("Case When A.CountData > 0 Then A.CountData ");
            SqlDtlSumary.AppendLine("Else '-' ");
            SqlDtlSumary.AppendLine("End As CountData ");
            SqlDtlSumary.AppendLine("From ");
            SqlDtlSumary.AppendLine("( ");
            SqlDtlSumary.AppendLine("Select IfNull(T1.LeaveCode, '0006') As LeaveCode, 'Cuti Sakit' As LeaveName, Count(T2.LeaveDt) CountData ");
            SqlDtlSumary.AppendLine("From TblEmpLeaveHdr T1 ");
            SqlDtlSumary.AppendLine("Inner Join TblEmpLeaveDtl T2 On T1.DocNo = T2.DocNo ");
            SqlDtlSumary.AppendLine("And T1.CancelInd = 'N' ");
            if (Sm.Right(Sm.GetDte(DteStartDt), 4) == Sm.Right(Sm.GetDte(DteEndDt), 4))
                SqlDtlSumary.AppendLine("And Left(T2.LeaveDt, 4) = @Yr ");
            else
                SqlDtlSumary.AppendLine("And Left(T2.LeaveDt, 4) Between @Yr1 And @Yr2 ");
            SqlDtlSumary.AppendLine("ANd T1.EmpCode = @EmpCode ");
            SqlDtlSumary.AppendLine("And Find_In_Set(T1.LeaveCode, (Select ParValue From TblParameter Where ParCode = 'SickLeaveCOde2')) ");
            SqlDtlSumary.AppendLine(") A ");
            SqlDtlSumary.AppendLine(")T Order By T.LeaveCode");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cmdtlsumary.Connection = cn;
                cmdtlsumary.CommandText = SqlDtlSumary.ToString();
               // Sm.CmParam<String>(ref cmdtlsumary, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmdtlsumary, "@EmpCode", TxtEmpCode.Text);
                Sm.CmParam<String>(ref cmdtlsumary, "@Yr", Sm.Left(Sm.GetDte(DteStartDt),4));
                Sm.CmParam<String>(ref cmdtlsumary, "@Yr2", Sm.Left(Sm.GetDte(DteEndDt),4));

                var dr = cmdtlsumary.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "LeaveCode",

                         //1-2
                         "LeaveName",
                         "CountData"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lDtlS3.Add(new EmpLeaveSumary()
                        {
                            LeaveCode = Sm.DrStr(dr, c[0]),

                            LeaveName = Sm.DrStr(dr, c[1]),
                            countData = Sm.DrStr(dr, c[2])                           

                        });
                    }
                }
                dr.Close();
                
            }

            myLists.Add(lDtlS3);

            #endregion 

            Sm.PrintReport(mFormPrintOutLeaveRequest, myLists, TableName, false);

        }

        private bool IsLeaveUseLeaveSummary()
        {
            var LeaveCode = Sm.GetLue(LueLeaveCode);

            if (mAnnualLeaveCode.Length > 0 && Sm.CompareStr(mAnnualLeaveCode, LeaveCode))
                return true;

            if (mLongServiceLeaveCode.Length > 0 && Sm.CompareStr(mLongServiceLeaveCode, LeaveCode))
                return true;

            if (mLongServiceLeaveCode2.Length > 0 && Sm.CompareStr(mLongServiceLeaveCode2, LeaveCode))
                return true;

            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueLeaveCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueLeaveCode, new Sm.RefreshLue2(Sl.SetLueLeaveCode), string.Empty);
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueLeaveType });
                ClearData2();
            }
        }

        private void LueLeaveType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueLeaveType, new Sm.RefreshLue2(Sl.SetLueOption), "LeaveType");
                ClearData2();

                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    DteEndDt, TmeStartTm, TmeEndTm, TxtDurationHour, ChkBreakInd 
                }, true);
                BtnDurationHrDefault.Enabled = false;

                if (Sm.GetLue(LueLeaveType) == "F")
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteEndDt }, false);
                else
                {
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TmeStartTm, TmeEndTm, TxtDurationHour, ChkBreakInd 
                    }, false);
                    BtnDurationHrDefault.Enabled = true;
                }
            }
        }

        private void DteStartDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                DteEndDt.EditValue = DteStartDt.EditValue;
                if (TxtDocNo.Text.Length == 0) ClearData3();
                ComputeDurationDay();
            }
        }

        private void DteEndDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0) ClearData3();
                ComputeDurationDay();
            }
        }

        private void TmeStartTm_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                TmeEndTm.EditValue = TmeStartTm.EditValue;
                ComputeDurationHour();
            }
        }

        private void TmeEndTm_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ComputeDurationHour();
        }

        private void TxtDurationHour_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNum(TxtDurationHour.Text, 0);
        }

        private void ChkBreakInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                try
                {
                    ChkBreakInd.Text = "Include Break Time";
                    if (ChkBreakInd.Checked) ChkBreakInd.Text += GetBreakSchedule();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        #endregion

        #region Button Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueLeaveCode, "Leave")) return;
            if (Sm.IsDteEmpty(DteStartDt, "Start Leave")) return;
            
            Sm.FormShowDialog(new FrmEmpLeave3Dlg(this, Sm.GetLue(LueLeaveCode)));
        }

        private void BtnDurationHrDefault_Click(object sender, EventArgs e)
        {
            ComputeDurationHour();
        }

        #endregion

        #endregion

        #region Class

        private class SignApproval {
            public string DocNo { get; set; }
            public string username1 { get; set; }
            public string posname1 { get; set; }
            public string NIK1 { get; set; }
            public string Remark1 { get; set; }
            public string username2 { get; set; }
            public string posname2 { get; set; }
            public string NIK2 { get; set; }
            public string Remark2 { get; set; }
            public string username3 { get; set; }
            public string posname3 { get; set; }
            public string NIK3 { get; set; }
            public string Remark3 { get; set; }
            public string Status1 { get; set; }
            public string Status2 { get; set; }
            public string Status3 { get; set; }
        }
        private class EmpLeaveDtl
        {
            public string DNo { get; set; }
            public string LeaveDt { get; set; }
            public bool HolidayInd { get; set; }
        }


        private class EmpLeave
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyPhone { get; set; }
            public string Nomor { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string Remark { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public string LeaveName { get; set; }
            public string EmpCodeOld { get; set; }
            public string PosName { get; set; }
            public string LevelName { get; set; }
            public string years { get; set; }
            public int days { get; set; }
            public string Address { get; set; }
            public string Domicile { get; set; }
            public string Phone { get; set; }
            public string CityName { get; set; }
            public string Status { get; set; }
            public string LeaveDtPeriod { get; set; }
            public string DaysText { get; set; }
            public string SiteName { get; set; }
            public string SiteAddress { get; set; }
        }

        private class EmployeeLeave
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string LeaveStartDt { get; set; }
            public string LeaveEndDt { get; set; }
            public string JoinDt { get; set; }
            public string Period { get; set; }
            public bool IsValid { get; set; }
        }

        private class LeaveSummary1
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string InitialDt { get; set; }
            public bool IsSummary1Existed { get; set; }
            public bool IsSummary2Existed { get; set; }
            public string LeaveCode1 { get; set; }
            public string LeaveCode2 { get; set; }
        }

        private class EmpLeaveSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class EmpLeaveSign2
        {
            public string EmpName { get; set; }
            public string PosName { get; set; }
        }

        private class EmpLeaveSumary
        {
            public string LeaveCode { get; set; }
            public string LeaveName { get; set; }
            public string countData { get; set; }
        }
        

        #endregion
    }
}
