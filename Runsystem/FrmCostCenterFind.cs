﻿#region Update
/*
    11/06/2020 [TKG/SRN] tambah indikator IsCCPayrollJournalEnabled
    03/04/2021 [VIN/ALL] tambah exporttoexcel 
    18/05/2021 [TKG/PHT] menambah not parent indicator
    21/05/2021 [VIN/ALL] bug show data
 * 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmCostCenterFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmCostCenter mFrmParent;

        #endregion

        #region Constructor

        public FrmCostCenterFind(FrmCostCenter FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Active",
                        "Parent",
                        "Profit Center",
                        
                        //6-10 
                        "Department",
                        "Voucher Payroll's Journal",
                        "Not Parent",
                        "Created By",
                        "Created Date",
                        
                        //11-14
                        "Created Time", 
                        "Last Updated By",
                        "Last Updated Date", 
                        "Last Updated Time"
                      },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 80, 150, 200, 
                        
                        //6-10
                        200, 180, 130, 130, 130, 
                        
                        //11-14
                        130, 130, 130, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 7, 8 });
            Sm.GrdFormatDate(Grd1, new int[] { 10, 13 });
            Sm.GrdFormatTime(Grd1, new int[] { 11, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 7 }, mFrmParent.mIsCCPayrollJournalEnabled);
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                Sm.FilterStr(ref Filter, ref cm, TxtCCCode.Text, new string[] { "A.CCCode", "A.CCName" });

                SQL.AppendLine("Select A.CCCode, A.CCName, A.ActInd, B.CCName As Parent, C.ProfitCenterName, D.DeptName, A.IsCCPayrollJournalEnabled, ");
                if (mFrmParent.mIsCostCenterNotParentIndEnabled)
                    SQL.AppendLine("A.NotParentInd, ");
                else
                    SQL.AppendLine("Case When Exists(Select 1 From TblCostCenter Where Parent Is Not Null And Parent=A.CCCode Limit 1) Then 'N' Else 'Y' End As NotParentInd, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblCostCenter A  ");
                SQL.AppendLine("Left Join TblcostCenter B on A.Parent=B.CCCode ");
                SQL.AppendLine("Left Join TblProfitCenter C On A.ProfitCenterCode=C.ProfitCenterCode ");
                SQL.AppendLine("Left Join TblDepartment D On A.DeptCode=D.DeptCode ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(" Order By A.CCName;");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SQL.ToString(),
                        new string[]
                        {
                            //0
                            "CCCode",

                            //1-5
                            "CCName",
                            "ActInd",
                            "Parent",
                            "ProfitCenterName",
                            "DeptName",
                            
                            //6-10
                            "IsCCPayrollJournalEnabled",
                            "NotParentInd",
                            "CreateBy", 
                            "CreateDt", 
                            "LastUpBy", 

                            //11
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 14, 11);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }


        #region Additional method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 7].Value = "'" + Sm.GetGrdStr(Grd1, Row, 7);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                Grd1.Cells[Row, 7].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 7), Sm.GetGrdStr(Grd1, Row, 7).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cost Center");
        }

        private void TxtCCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
