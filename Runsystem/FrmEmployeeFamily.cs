﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmployeeFamily : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mEmpCode = string.Empty;
        internal FrmEmployeeFamilyFind FrmFind;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmEmployeeFamily(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Family";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);

                Sl.SetLueFamilyStatus(ref LueStatus);
                LueStatus.Visible = false;
                Sl.SetLueGender(ref LueGender);
                LueGender.Visible = false;
                DteBirthDt.Visible = false;
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mEmpCode.Length != 0)
                {
                    ShowData(mEmpCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Family Name",
                        "Status Code",
                        "Status",
                        "Gender Code",
                        "Gender",
                        
                        //6-9
                        "Birth Date",
                        "ID Card",
                        "National"+Environment.NewLine+"Identity#",
                        "Remark"
                    },
                     new int[] 
                    {
                        0, 
                        200, 0, 100, 0, 80, 
                        80, 150, 150, 300
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 9 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 5, 6, 7, 8, 9 });
                    TxtEmpCode.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3, 5, 6, 7, 8, 9 });
                    TxtEmpCode.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtEmpCode, TxtEmpName, TxtEmpCodeOld, TxtPosCode, TxtDeptCode, 
                TxtSiteCode, LueStatus, LueGender, DteBirthDt
            });
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmployeeFamilyFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            Sm.StdMsg(mMsgType.Info, "You only can edit existing data instead of inserting new one.");
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtEmpCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1, 3, 5, 6, 7, 8, 9 }, e.ColIndex))
            {
                if (e.ColIndex == 3) LueRequestEdit(Grd1, LueStatus, ref fCell, ref fAccept, e);
                if (e.ColIndex == 5) LueRequestEdit(Grd1, LueGender, ref fCell, ref fAccept, e);
                if (e.ColIndex == 6) Sm.DteRequestEdit(Grd1, DteBirthDt, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 1, 7, 8, 9 }, e);
        }

        #endregion

        #region Save Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(DelEmployeeFamily());

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(InsertEmployeeFamily(Row));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtEmpCode.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtEmpCode, "Employee code", false) ||
                IsGrdExceedMaxRecords() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee's family.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's family entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Family name is empty.")) return true;
                    
                    for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                    {
                        if (
                            Row != Row2 &&
                            Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row2, 1)) 
                            )
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Family Name : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                                "Duplicate entry."
                                );
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private MySqlCommand DelEmployeeFamily()
        {
            var cm = new MySqlCommand() { CommandText = "Delete From TblEmployeeFamily Where EmpCode=@EmpCode;" };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);

            return cm;
        }

        private MySqlCommand InsertEmployeeFamily(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblEmployeeFamily(EmpCode, DNo, FamilyName, Status, Gender, BirthDt, IDNo, NIN, Remark, CreateBy, CreateDt) " +
                    "Values(@EmpCode, @DNo, @FamilyName, @Status, @Gender, @BirthDt, @IDNo, @NIN, @Remark, @CreateBy, CurrentDateTime()); "+

                    "Update TblEmpSSHdr A " +
                    "Inner Join TblEmpSSDtl2 B On A.Docno = B.Docno Set B.Status=@Status, B.Gender=@Gender, B.BirthDt=@BirthDt, B.IDNo=@IDNo, B.NIN=@NIN "+
                    "Where A.EmpCode=@EmpCode And B.FamilyName=@FamilyName ; "
            };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@FamilyName", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@Gender", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParamDt(ref cm, "@BirthDt", Sm.GetGrdDate(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@IDNo", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@NIN", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region  Show Data

        public void ShowData(string EmpCode)
        {
            try
            {
                ClearData();
                ShowEmployee(EmpCode);
                ShowEmployeeFamily(EmpCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmployee(string EmpCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, D.SiteName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblSite D On A.SiteCode=D.SiteCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "EmpCode", 
                        
                        //1-5
                        "EmpName", "EmpCodeOld", "PosName", "DeptName", "SiteName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[1]);
                        TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[2]);
                        TxtPosCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[5]);
                    }, true
                );
        }

        private void ShowEmployeeFamily(string EmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.FamilyName, A.Status, B.OptDesc as StatusDesc, A.Gender, C.OptDesc As GenderDesc, ");
            SQL.AppendLine("A.BirthDt, A.IDNo, A.NIN, A.Remark ");
            SQL.AppendLine("From TblEmployeeFamily A ");
            SQL.AppendLine("Left Join TblOption B On A.Status=B.OptCode and B.OptCat='FamilyStatus' ");
            SQL.AppendLine("Left Join TblOption C On A.Gender=C.OptCode and C.OptCat='Gender' ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("Order By A.DNo;");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",

                        //1-5
                        "FamilyName", "Status", "StatusDesc", "Gender", "GenderDesc", 
                        
                        //6-9
                        "BirthDt", "IDNo", "NIN", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex-1));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueStatus_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(Sl.SetLueFamilyStatus));
        }

        private void LueStatus_Leave(object sender, EventArgs e)
        {
            if (LueStatus.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (Sm.GetLue(LueStatus).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 2].Value =
                    Grd1.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueStatus);
                    Grd1.Cells[fCell.RowIndex, 3].Value = LueStatus.GetColumnValue("Col2");
                }
                LueStatus.Visible = false;
            }
        }

        private void LueGender_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGender, new Sm.RefreshLue1(Sl.SetLueGender));
        }

        private void LueGender_Leave(object sender, EventArgs e)
        {
            if (LueGender.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueGender).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 4].Value =
                    Grd1.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueGender);
                    Grd1.Cells[fCell.RowIndex, 5].Value = LueGender.GetColumnValue("Col2");
                }
                LueGender.Visible = false;
            }
        }

        private void LueGender_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void DteBirthDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteBirthDt, ref fCell, ref fAccept);
        }

        private void DteBirthDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        #endregion

        #endregion
    }
}
