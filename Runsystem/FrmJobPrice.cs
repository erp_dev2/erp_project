﻿#region Update
/*
    07/11/2019 [TKG/SIER] New Application
    11/11/2019 [TKG/SIER] divalidasi dengan site
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmJobPrice : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mMainCurCode = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        internal FrmJobPriceFind FrmFind;
        internal bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmJobPrice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Updating Job's Price";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                BtnExcel.Visible = false;
                GetParameter();
                SetGrd();
                Sl.SetLueCurCode(ref LueCurCode);
                SetFormControl(mState.View);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "",

                        //1-5
                        "Job's Code",
                        "Job's Name",
                        "Category",
                        "UoM",
                        "Previous"+Environment.NewLine+"Currency",
                        
                        //6-9
                        "Previous"+Environment.NewLine+"Price",
                        "Currency",
                        "Price",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20, 

                        //1-5
                        100, 200, 200, 80, 80,
                        
                        //6-8
                        120, 100, 120, 300
                    }
                );

            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5, 6 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5, 6 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueCurCode, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 7, 8, 9 });
                    Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
                    Sm.GrdColInvisible(Grd1, new int[] { 7 }, true);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueCurCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 7, 8, 9 });
                    Sm.GrdColInvisible(Grd1, new int[] { 0 }, true);
                    Sm.GrdColInvisible(Grd1, new int[] { 7 }, false);
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtDocNo, DteDocDt, LueCurCode, MeeRemark });
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 8 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmJobPriceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, mMainCurCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueCurCode, "Currency"))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmJobPriceDlg(this));
                }
                if (Sm.IsGrdColSelected(new int[] { 0, 8, 9 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 8 });
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueCurCode, "Currency")) 
                Sm.FormShowDialog(new FrmJobPriceDlg(this));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 8 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 9 }, e);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
           
           if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()) return;
            
            Cursor.Current = Cursors.WaitCursor;
           
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "JobPrice", "TblJobPriceHdr");

            var cml = new List<MySqlCommand>();
            cml.Add(SaveJobPrice(DocNo));
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in job list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 2, false, "Job is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, r, 8, true, "Price should be bigger than 0.00.")) return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveJobPrice(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblJobPriceHdr ");
            SQL.AppendLine("(DocNo, DocDt, CurCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @CurCode, @Remark, @UserCode, @Dt);");

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    SQL.AppendLine("Insert Into TblJobPriceDtl ");
                    SQL.AppendLine("(DocNo, JobCode, PrevCurCode, PrevUPrice, CurCode, UPrice, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values ");
                    SQL.AppendLine("(@DocNo, @JobCode" + r + ", @PrevCurCode" + r + ", @PrevUPrice" + r + ", @CurCode, @UPrice" + r + ", @Remark" + r + ", @UserCode, @Dt); ");

                    SQL.AppendLine("Update TblJob Set ");
                    SQL.AppendLine("    CurCode=@CurCode, UPrice=@UPrice" + r);
                    SQL.AppendLine(" Where JobCode=@JobCode" + r + ";");

                    Sm.CmParam<String>(ref cm, "@JobCode" + r, Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<String>(ref cm, "@PrevCurCode" + r, Sm.GetGrdStr(Grd1, r, 5));
                    Sm.CmParam<Decimal>(ref cm, "@PrevUPrice" + r, Sm.GetGrdDec(Grd1, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice" + r, Sm.GetGrdDec(Grd1, r, 8));
                    Sm.CmParam<String>(ref cm, "@Remark" + r, Sm.GetGrdStr(Grd1, r, 9));
                }
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowJobPriceHdr(DocNo);
                ShowJobPriceDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowJobPriceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, CurCode, Remark ");
            SQL.AppendLine("From TblJobPriceHdr ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-3
                        "DocDt", "CurCode", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[2]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        private void ShowJobPriceDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.JobCode, B.JobName, C.JobCtName, B.UomCode, ");
            SQL.AppendLine("A.PrevCurCode, A.PrevUPrice, A.CurCode, A.UPrice, A.Remark ");
            SQL.AppendLine("From TblJobPriceDtl A ");
            SQL.AppendLine("Inner Join TblJob B On A.JobCode=B.JobCode ");
            SQL.AppendLine("Inner Join TblJobCategory C On B.JobCtCode=C.JobCtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By C.JobCtName, B.JobName;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "JobCode", 
                    //1-5
                    "JobName", "JobCtName", "UomCode", "PrevCurCode", "PrevUPrice", 
                    //6-8
                    "CurCode", "UPrice", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 8 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void ProcessImport()
        {
            if (Sm.IsLueEmpty(LueCurCode, "Currency")) return;

            Cursor.Current = Cursors.WaitCursor;

            var lResult = new List<Result>();

            ClearGrd();
            try
            {
                ProcessImport1(ref lResult);
                if (lResult.Count > 0)
                {
                    ProcessImport2(ref lResult);
                    ProcessImport3(ref lResult);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lResult.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void ProcessImport1(ref List<Result> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var ListSeparator = Convert.ToChar(Sm.GetListSeparator());
            var FileName = openFileDialog1.FileName;
            var JobCodeTemp = string.Empty;
            var UPriceTemp = 0m;
            bool IsFirst = true;

            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(ListSeparator); // line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            JobCodeTemp = arr[0].Trim();
                            if (arr[1].Trim().Length > 0)
                                UPriceTemp = decimal.Parse(arr[1].Trim());
                            else
                                UPriceTemp = 0m;
                            l.Add(new Result()
                            {
                                JobCode = JobCodeTemp,
                                JobName = string.Empty,
                                JobCtName = string.Empty,
                                PrevCurCode = string.Empty,
                                PrevUPrice = 0m,
                                UPrice = UPriceTemp
                            });
                        }
                    }
                }
            }
        }

        private void ProcessImport2(ref List<Result> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var JobCode = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.JobCode=@JobCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@JobCode0" + i.ToString(), l[i].JobCode);
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select A.JobCode, A.JobName, B.JobCtName, A.UomCode, A.CurCode, A.UPrice ");
            SQL.AppendLine("From TblJob A ");
            SQL.AppendLine("Inner Join TblJobCategory B On A.JobCtCode=B.JobCtCode ");
            SQL.AppendLine(Filter);
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And A.JobCode In ( ");
                SQL.AppendLine("    Select Distinct JobCode From TblJobSite T ");
                SQL.AppendLine("    Where Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(T.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "JobCode", 
                    
                    //1-5
                    "JobName", 
                    "JobCtName", 
                    "UomCode", 
                    "CurCode", 
                    "UPrice"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        JobCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].JobCode, JobCode))
                            {
                                l[i].JobName = Sm.DrStr(dr, c[1]);
                                l[i].JobCtName = Sm.DrStr(dr, c[2]);
                                l[i].UomCode = Sm.DrStr(dr, c[3]);
                                l[i].PrevCurCode = Sm.DrStr(dr, c[4]);
                                l[i].PrevUPrice = Sm.DrDec(dr, c[5]);
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessImport3(ref List<Result> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[1].Value = l[i].JobCode;
                r.Cells[2].Value = l[i].JobName;
                r.Cells[3].Value = l[i].JobCtName;
                r.Cells[4].Value = l[i].UomCode;
                r.Cells[5].Value = l[i].PrevCurCode;
                r.Cells[6].Value = l[i].PrevUPrice;
                r.Cells[7].Value = string.Empty;
                r.Cells[8].Value = l[i].UPrice;
                r.Cells[9].Value = string.Empty;
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 8 });
            Grd1.EndUpdate();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }
       
        #endregion

        #region Button Event

        private void BtnImport_Click(object sender, EventArgs e)
        {
            ProcessImport();
        }

        #endregion        

        #endregion

        #region Class

        private class Result
        {
            public string JobCode { get; set; }
            public string JobName { get; set; }
            public string JobCtName { get; set; }
            public string UomCode { get; set; }
            public string PrevCurCode { get; set; }
            public decimal PrevUPrice { get; set; }
            public decimal UPrice { get; set; }
        }

        #endregion
    }
}
