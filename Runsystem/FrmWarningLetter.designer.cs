﻿namespace RunSystem
{
    partial class FrmWarningLetter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtNoOfDays = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtWlName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtWlCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LueWLType = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSeqNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtPDeduction = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtPoint = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LueADCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoOfDays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWlName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWlCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWLType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeqNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPDeduction.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPoint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueADCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LueADCode);
            this.panel2.Controls.Add(this.TxtPoint);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtPDeduction);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.TxtSeqNo);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.LueWLType);
            this.panel2.Controls.Add(this.TxtNoOfDays);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtWlName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtWlCode);
            this.panel2.Controls.Add(this.label1);
            // 
            // TxtNoOfDays
            // 
            this.TxtNoOfDays.EnterMoveNextControl = true;
            this.TxtNoOfDays.Location = new System.Drawing.Point(198, 62);
            this.TxtNoOfDays.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNoOfDays.Name = "TxtNoOfDays";
            this.TxtNoOfDays.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNoOfDays.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNoOfDays.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNoOfDays.Properties.Appearance.Options.UseFont = true;
            this.TxtNoOfDays.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtNoOfDays.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtNoOfDays.Size = new System.Drawing.Size(78, 20);
            this.TxtNoOfDays.TabIndex = 14;
            this.TxtNoOfDays.Validated += new System.EventHandler(this.TxtNoOfDays_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(99, 64);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 14);
            this.label3.TabIndex = 13;
            this.label3.Text = "Number of Days";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWlName
            // 
            this.TxtWlName.EnterMoveNextControl = true;
            this.TxtWlName.Location = new System.Drawing.Point(198, 41);
            this.TxtWlName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWlName.Name = "TxtWlName";
            this.TxtWlName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWlName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWlName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWlName.Properties.Appearance.Options.UseFont = true;
            this.TxtWlName.Properties.MaxLength = 80;
            this.TxtWlName.Size = new System.Drawing.Size(419, 20);
            this.TxtWlName.TabIndex = 12;
            this.TxtWlName.Validated += new System.EventHandler(this.TxtWlName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(68, 43);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Warning Letter Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWlCode
            // 
            this.TxtWlCode.EnterMoveNextControl = true;
            this.TxtWlCode.Location = new System.Drawing.Point(198, 20);
            this.TxtWlCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWlCode.Name = "TxtWlCode";
            this.TxtWlCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWlCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWlCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWlCode.Properties.Appearance.Options.UseFont = true;
            this.TxtWlCode.Properties.MaxLength = 16;
            this.TxtWlCode.Size = new System.Drawing.Size(205, 20);
            this.TxtWlCode.TabIndex = 10;
            this.TxtWlCode.Validated += new System.EventHandler(this.TxtWlCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(71, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Warning Letter Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(137, 85);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 14);
            this.label7.TabIndex = 15;
            this.label7.Text = "Category";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWLType
            // 
            this.LueWLType.EnterMoveNextControl = true;
            this.LueWLType.Location = new System.Drawing.Point(198, 83);
            this.LueWLType.Margin = new System.Windows.Forms.Padding(5);
            this.LueWLType.Name = "LueWLType";
            this.LueWLType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLType.Properties.Appearance.Options.UseFont = true;
            this.LueWLType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWLType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWLType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWLType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWLType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWLType.Properties.DropDownRows = 30;
            this.LueWLType.Properties.MaxLength = 1;
            this.LueWLType.Properties.NullText = "[Empty]";
            this.LueWLType.Properties.PopupWidth = 500;
            this.LueWLType.Size = new System.Drawing.Size(419, 20);
            this.LueWLType.TabIndex = 16;
            this.LueWLType.ToolTip = "F4 : Show/hide list";
            this.LueWLType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWLType.EditValueChanged += new System.EventHandler(this.LueWLType_EditValueChanged);
            // 
            // TxtSeqNo
            // 
            this.TxtSeqNo.EnterMoveNextControl = true;
            this.TxtSeqNo.Location = new System.Drawing.Point(198, 104);
            this.TxtSeqNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeqNo.Name = "TxtSeqNo";
            this.TxtSeqNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeqNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeqNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeqNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSeqNo.Properties.MaxLength = 1;
            this.TxtSeqNo.Size = new System.Drawing.Size(78, 20);
            this.TxtSeqNo.TabIndex = 18;
            this.TxtSeqNo.Validated += new System.EventHandler(this.TxtSeqNo_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(131, 106);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 14);
            this.label4.TabIndex = 17;
            this.label4.Text = "Sequence";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPDeduction
            // 
            this.TxtPDeduction.EnterMoveNextControl = true;
            this.TxtPDeduction.Location = new System.Drawing.Point(198, 125);
            this.TxtPDeduction.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPDeduction.Name = "TxtPDeduction";
            this.TxtPDeduction.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPDeduction.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPDeduction.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPDeduction.Properties.Appearance.Options.UseFont = true;
            this.TxtPDeduction.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPDeduction.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPDeduction.Size = new System.Drawing.Size(78, 20);
            this.TxtPDeduction.TabIndex = 20;
            this.TxtPDeduction.Validated += new System.EventHandler(this.TxtPDeduction_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(63, 128);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(130, 14);
            this.label16.TabIndex = 19;
            this.label16.Text = "Percentage Deduction";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(280, 128);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 14);
            this.label5.TabIndex = 21;
            this.label5.Text = "%";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPoint
            // 
            this.TxtPoint.EnterMoveNextControl = true;
            this.TxtPoint.Location = new System.Drawing.Point(199, 146);
            this.TxtPoint.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPoint.Name = "TxtPoint";
            this.TxtPoint.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPoint.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPoint.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPoint.Properties.Appearance.Options.UseFont = true;
            this.TxtPoint.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPoint.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPoint.Size = new System.Drawing.Size(78, 20);
            this.TxtPoint.TabIndex = 23;
            this.TxtPoint.Validated += new System.EventHandler(this.TxtPoint_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(158, 149);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 14);
            this.label6.TabIndex = 22;
            this.label6.Text = "Point";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(79, 170);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 14);
            this.label8.TabIndex = 24;
            this.label8.Text = "Affected Allowance";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueADCode
            // 
            this.LueADCode.EnterMoveNextControl = true;
            this.LueADCode.Location = new System.Drawing.Point(199, 167);
            this.LueADCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueADCode.Name = "LueADCode";
            this.LueADCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueADCode.Properties.Appearance.Options.UseFont = true;
            this.LueADCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueADCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueADCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueADCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueADCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueADCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueADCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueADCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueADCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueADCode.Properties.DropDownRows = 30;
            this.LueADCode.Properties.MaxLength = 1;
            this.LueADCode.Properties.NullText = "[Empty]";
            this.LueADCode.Properties.PopupWidth = 500;
            this.LueADCode.Size = new System.Drawing.Size(419, 20);
            this.LueADCode.TabIndex = 25;
            this.LueADCode.ToolTip = "F4 : Show/hide list";
            this.LueADCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueADCode.EditValueChanged += new System.EventHandler(this.LueADCode_EditValueChanged);
            // 
            // FrmWarningLetter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmWarningLetter";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoOfDays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWlName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWlCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWLType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeqNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPDeduction.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPoint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueADCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtNoOfDays;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtWlName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtWlCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueWLType;
        internal DevExpress.XtraEditors.TextEdit TxtSeqNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtPDeduction;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit LueADCode;
        internal DevExpress.XtraEditors.TextEdit TxtPoint;
        private System.Windows.Forms.Label label6;
    }
}