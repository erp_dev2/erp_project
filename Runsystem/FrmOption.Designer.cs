﻿namespace RunSystem
{
    partial class FrmOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtOptCode = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtOptCat = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtCustomize = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.MeeOptDesc = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtProperty1 = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtProperty2 = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtProperty3 = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtProperty6 = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtProperty5 = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtProperty4 = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtProperty9 = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtProperty8 = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtProperty7 = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtProperty10 = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOptCat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCustomize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOptDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty10.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(764, 0);
            this.panel1.Size = new System.Drawing.Size(70, 341);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtProperty10);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.TxtProperty9);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.TxtProperty8);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.TxtProperty7);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.TxtProperty6);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtProperty5);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.TxtProperty4);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.TxtProperty3);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.TxtProperty2);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtProperty1);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.MeeOptDesc);
            this.panel2.Controls.Add(this.TxtCustomize);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtOptCode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtOptCat);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(764, 341);
            // 
            // TxtOptCode
            // 
            this.TxtOptCode.EnterMoveNextControl = true;
            this.TxtOptCode.Location = new System.Drawing.Point(123, 29);
            this.TxtOptCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOptCode.Name = "TxtOptCode";
            this.TxtOptCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtOptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOptCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOptCode.Properties.Appearance.Options.UseFont = true;
            this.TxtOptCode.Properties.MaxLength = 40;
            this.TxtOptCode.Size = new System.Drawing.Size(319, 20);
            this.TxtOptCode.TabIndex = 12;
            this.TxtOptCode.Validated += new System.EventHandler(this.TxtOptCode_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(43, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Option Code";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOptCat
            // 
            this.TxtOptCat.EnterMoveNextControl = true;
            this.TxtOptCat.Location = new System.Drawing.Point(123, 8);
            this.TxtOptCat.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOptCat.Name = "TxtOptCat";
            this.TxtOptCat.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtOptCat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOptCat.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOptCat.Properties.Appearance.Options.UseFont = true;
            this.TxtOptCat.Properties.MaxLength = 46;
            this.TxtOptCat.Size = new System.Drawing.Size(319, 20);
            this.TxtOptCat.TabIndex = 10;
            this.TxtOptCat.Validated += new System.EventHandler(this.TxtOptCat_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(22, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Option Category";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(11, 53);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 14);
            this.label3.TabIndex = 13;
            this.label3.Text = "Option Description";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCustomize
            // 
            this.TxtCustomize.EnterMoveNextControl = true;
            this.TxtCustomize.Location = new System.Drawing.Point(123, 71);
            this.TxtCustomize.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCustomize.Name = "TxtCustomize";
            this.TxtCustomize.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCustomize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCustomize.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCustomize.Properties.Appearance.Options.UseFont = true;
            this.TxtCustomize.Properties.MaxLength = 255;
            this.TxtCustomize.Size = new System.Drawing.Size(319, 20);
            this.TxtCustomize.TabIndex = 16;
            this.TxtCustomize.Validated += new System.EventHandler(this.TxtCostumize_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(29, 74);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 14);
            this.label4.TabIndex = 15;
            this.label4.Text = "Customized For";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeOptDesc
            // 
            this.MeeOptDesc.EditValue = "";
            this.MeeOptDesc.EnterMoveNextControl = true;
            this.MeeOptDesc.Location = new System.Drawing.Point(123, 50);
            this.MeeOptDesc.Margin = new System.Windows.Forms.Padding(5);
            this.MeeOptDesc.Name = "MeeOptDesc";
            this.MeeOptDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOptDesc.Properties.Appearance.Options.UseFont = true;
            this.MeeOptDesc.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOptDesc.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeOptDesc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOptDesc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeOptDesc.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOptDesc.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeOptDesc.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOptDesc.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeOptDesc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeOptDesc.Properties.MaxLength = 400;
            this.MeeOptDesc.Properties.PopupFormSize = new System.Drawing.Size(650, 20);
            this.MeeOptDesc.Properties.ShowIcon = false;
            this.MeeOptDesc.Size = new System.Drawing.Size(611, 20);
            this.MeeOptDesc.TabIndex = 14;
            this.MeeOptDesc.ToolTip = "F4 : Show/hide text";
            this.MeeOptDesc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeOptDesc.ToolTipTitle = "Run System";
            // 
            // TxtProperty1
            // 
            this.TxtProperty1.EnterMoveNextControl = true;
            this.TxtProperty1.Location = new System.Drawing.Point(123, 92);
            this.TxtProperty1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProperty1.Name = "TxtProperty1";
            this.TxtProperty1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProperty1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProperty1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProperty1.Properties.Appearance.Options.UseFont = true;
            this.TxtProperty1.Properties.MaxLength = 250;
            this.TxtProperty1.Size = new System.Drawing.Size(319, 20);
            this.TxtProperty1.TabIndex = 18;
            this.TxtProperty1.Validated += new System.EventHandler(this.TxtProperty1_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(54, 95);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 14);
            this.label5.TabIndex = 17;
            this.label5.Text = "Property 1";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProperty2
            // 
            this.TxtProperty2.EnterMoveNextControl = true;
            this.TxtProperty2.Location = new System.Drawing.Point(123, 113);
            this.TxtProperty2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProperty2.Name = "TxtProperty2";
            this.TxtProperty2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProperty2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProperty2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProperty2.Properties.Appearance.Options.UseFont = true;
            this.TxtProperty2.Properties.MaxLength = 250;
            this.TxtProperty2.Size = new System.Drawing.Size(319, 20);
            this.TxtProperty2.TabIndex = 20;
            this.TxtProperty2.Validated += new System.EventHandler(this.TxtProperty2_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(54, 116);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 14);
            this.label6.TabIndex = 19;
            this.label6.Text = "Property 2";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProperty3
            // 
            this.TxtProperty3.EnterMoveNextControl = true;
            this.TxtProperty3.Location = new System.Drawing.Point(123, 134);
            this.TxtProperty3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProperty3.Name = "TxtProperty3";
            this.TxtProperty3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProperty3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProperty3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProperty3.Properties.Appearance.Options.UseFont = true;
            this.TxtProperty3.Properties.MaxLength = 250;
            this.TxtProperty3.Size = new System.Drawing.Size(319, 20);
            this.TxtProperty3.TabIndex = 22;
            this.TxtProperty3.Validated += new System.EventHandler(this.TxtProperty3_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(54, 137);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 14);
            this.label7.TabIndex = 21;
            this.label7.Text = "Property 3";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EditValue = "";
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(122, 302);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(650, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(611, 20);
            this.MeeRemark.TabIndex = 38;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            this.MeeRemark.EditValueChanged += new System.EventHandler(this.MeeRemark_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(72, 305);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 37;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProperty6
            // 
            this.TxtProperty6.EnterMoveNextControl = true;
            this.TxtProperty6.Location = new System.Drawing.Point(123, 197);
            this.TxtProperty6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProperty6.Name = "TxtProperty6";
            this.TxtProperty6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProperty6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProperty6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProperty6.Properties.Appearance.Options.UseFont = true;
            this.TxtProperty6.Properties.MaxLength = 250;
            this.TxtProperty6.Size = new System.Drawing.Size(319, 20);
            this.TxtProperty6.TabIndex = 28;
            this.TxtProperty6.Validated += new System.EventHandler(this.TxtProperty6_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(54, 200);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 14);
            this.label9.TabIndex = 27;
            this.label9.Text = "Property 6";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProperty5
            // 
            this.TxtProperty5.EnterMoveNextControl = true;
            this.TxtProperty5.Location = new System.Drawing.Point(123, 176);
            this.TxtProperty5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProperty5.Name = "TxtProperty5";
            this.TxtProperty5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProperty5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProperty5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProperty5.Properties.Appearance.Options.UseFont = true;
            this.TxtProperty5.Properties.MaxLength = 250;
            this.TxtProperty5.Size = new System.Drawing.Size(319, 20);
            this.TxtProperty5.TabIndex = 26;
            this.TxtProperty5.Validated += new System.EventHandler(this.TxtProperty5_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(54, 179);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 14);
            this.label10.TabIndex = 25;
            this.label10.Text = "Property 5";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProperty4
            // 
            this.TxtProperty4.EnterMoveNextControl = true;
            this.TxtProperty4.Location = new System.Drawing.Point(123, 155);
            this.TxtProperty4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProperty4.Name = "TxtProperty4";
            this.TxtProperty4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProperty4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProperty4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProperty4.Properties.Appearance.Options.UseFont = true;
            this.TxtProperty4.Properties.MaxLength = 250;
            this.TxtProperty4.Size = new System.Drawing.Size(319, 20);
            this.TxtProperty4.TabIndex = 24;
            this.TxtProperty4.Validated += new System.EventHandler(this.TxtProperty4_Validated);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(54, 158);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 14);
            this.label11.TabIndex = 23;
            this.label11.Text = "Property 4";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProperty9
            // 
            this.TxtProperty9.EnterMoveNextControl = true;
            this.TxtProperty9.Location = new System.Drawing.Point(122, 260);
            this.TxtProperty9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProperty9.Name = "TxtProperty9";
            this.TxtProperty9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProperty9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProperty9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProperty9.Properties.Appearance.Options.UseFont = true;
            this.TxtProperty9.Properties.MaxLength = 250;
            this.TxtProperty9.Size = new System.Drawing.Size(319, 20);
            this.TxtProperty9.TabIndex = 34;
            this.TxtProperty9.Validated += new System.EventHandler(this.TxtProperty9_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(54, 263);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 14);
            this.label12.TabIndex = 33;
            this.label12.Text = "Property 9";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProperty8
            // 
            this.TxtProperty8.EnterMoveNextControl = true;
            this.TxtProperty8.Location = new System.Drawing.Point(122, 239);
            this.TxtProperty8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProperty8.Name = "TxtProperty8";
            this.TxtProperty8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProperty8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProperty8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProperty8.Properties.Appearance.Options.UseFont = true;
            this.TxtProperty8.Properties.MaxLength = 250;
            this.TxtProperty8.Size = new System.Drawing.Size(319, 20);
            this.TxtProperty8.TabIndex = 32;
            this.TxtProperty8.Validated += new System.EventHandler(this.TxtProperty8_Validated);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(54, 242);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 14);
            this.label13.TabIndex = 31;
            this.label13.Text = "Property 8";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProperty7
            // 
            this.TxtProperty7.EnterMoveNextControl = true;
            this.TxtProperty7.Location = new System.Drawing.Point(122, 218);
            this.TxtProperty7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProperty7.Name = "TxtProperty7";
            this.TxtProperty7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProperty7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProperty7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProperty7.Properties.Appearance.Options.UseFont = true;
            this.TxtProperty7.Properties.MaxLength = 250;
            this.TxtProperty7.Size = new System.Drawing.Size(319, 20);
            this.TxtProperty7.TabIndex = 30;
            this.TxtProperty7.Validated += new System.EventHandler(this.TxtProperty7_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(54, 221);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 14);
            this.label14.TabIndex = 29;
            this.label14.Text = "Property 7";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProperty10
            // 
            this.TxtProperty10.EnterMoveNextControl = true;
            this.TxtProperty10.Location = new System.Drawing.Point(122, 281);
            this.TxtProperty10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProperty10.Name = "TxtProperty10";
            this.TxtProperty10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProperty10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProperty10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProperty10.Properties.Appearance.Options.UseFont = true;
            this.TxtProperty10.Properties.MaxLength = 250;
            this.TxtProperty10.Size = new System.Drawing.Size(319, 20);
            this.TxtProperty10.TabIndex = 36;
            this.TxtProperty10.Validated += new System.EventHandler(this.TxtProperty10_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(47, 284);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 14);
            this.label15.TabIndex = 35;
            this.label15.Text = "Property 10";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 341);
            this.Name = "FrmOption";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOptCat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCustomize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOptDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProperty10.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtOptCode;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtOptCat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtCustomize;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.MemoExEdit MeeOptDesc;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private DevExpress.XtraEditors.TextEdit TxtProperty3;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.TextEdit TxtProperty2;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtProperty1;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit TxtProperty6;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.TextEdit TxtProperty5;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.TextEdit TxtProperty4;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.TextEdit TxtProperty10;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.TextEdit TxtProperty9;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.TextEdit TxtProperty8;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.TextEdit TxtProperty7;
        private System.Windows.Forms.Label label14;
    }
}