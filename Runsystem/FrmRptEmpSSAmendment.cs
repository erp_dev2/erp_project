﻿#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpSSAmendment : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptEmpSSAmendment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetLueStatus(ref LueStatus);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);

                var Dt = Sm.ServerCurrentDateTime();
                Sm.SetLue(LueYr, Sm.Left(Dt, 4));
                Sm.SetLue(LueMth, Dt.Substring(4, 2));
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 9].Value = "'" + Sm.GetGrdStr(Grd1, Row, 9);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                Grd1.Cells[Row, 9].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 9), Sm.GetGrdStr(Grd1, Row, 9).Length - 1);
            }
            Grd1.EndUpdate();
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Tbl1.EmpCode, Tbl1.PName, if(Tbl1.EmpInd='Y', 'Employee', Tbl4.OptDesc) As FamilyStatus, ");
            SQL.AppendLine("Tbl2.EmpCodeOld, Tbl3.DeptName, ");
            SQL.AppendLine("Tbl1.JoinDt, Tbl1.ResignDt, Tbl1.SSName, Tbl1.CardNo, ");
            SQL.AppendLine("Tbl1.StartDt, Tbl1.EndDt, Tbl1.Previous, Tbl1.Current "); 
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T4.SSPCode, T4.SSName, T1.EmpCode, T1.CardNo, ");
            SQL.AppendLine("    IfNull(T2.EmpInd, T3.EmpInd) As EmpInd,  ");
            SQL.AppendLine("    IfNull(T2.FamilyStatus, T3.FamilyStatus) As FamilyStatus, "); 
            SQL.AppendLine("    IfNull(T2.PName, T3.PName) As PName, ");
            SQL.AppendLine("    IfNull(T2.DeptCode, T3.DeptCode) As DeptCode, ");
            SQL.AppendLine("    IfNull(T2.JoinDt, T3.JoinDt) As JoinDt, "); 
            SQL.AppendLine("    IfNull(T2.ResignDt, T3.ResignDt) As ResignDt, ");
            SQL.AppendLine("    IfNull(T2.StartDt, T3.StartDt) As StartDt, ");
            SQL.AppendLine("    IfNull(T2.EndDt, T3.EndDt) As EndDt, ");
            SQL.AppendLine("    If(T2.SSCode Is Null, 'N', 'Y') As Current, ");
            SQL.AppendLine("    If(T3.SSCode Is Null, 'N', 'Y') As Previous ");
            SQL.AppendLine("    From ( ");
	        SQL.AppendLine("        Select Distinct SSCode, EmpCode, CardNo "); 
	        SQL.AppendLine("        From ( ");
		    SQL.AppendLine("            Select B.SSCode, B.EmpCode, B.CardNo ");   
		    SQL.AppendLine("            From TblEmpSSListHdr A ");
		    SQL.AppendLine("            Inner Join TblEmpSSListDtl B On A.DocNo=B.DocNo ");
		    SQL.AppendLine("            Where A.CancelInd='N' ");
		    SQL.AppendLine("            And A.Yr=@Yr1 ");
		    SQL.AppendLine("            And A.Mth=@Mth1 ");
		    SQL.AppendLine("            Union All ");
		    SQL.AppendLine("            Select B.SSCode, B.EmpCode, B.CardNo ");
		    SQL.AppendLine("            From TblEmpSSListHdr A ");
		    SQL.AppendLine("            Inner Join TblEmpSSListDtl B On A.DocNo=B.DocNo ");
		    SQL.AppendLine("            Where A.CancelInd='N' ");
		    SQL.AppendLine("            And A.Yr=@Yr2 ");
		    SQL.AppendLine("            And A.Mth=@Mth2 ");
	        SQL.AppendLine("        ) T ");
            SQL.AppendLine("    ) T1  ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
	        SQL.AppendLine("        Select B.SSCode, B.EmpCode, B.CardNo, B.EmpInd, B.FamilyStatus, B.PName, B.DeptCode, B.JoinDt, B.ResignDt, B.StartDt, B.EndDt ");   
	        SQL.AppendLine("        From TblEmpSSListHdr A ");
            SQL.AppendLine("        Inner Join TblEmpSSListDtl B On A.DocNo=B.DocNo ");
	        SQL.AppendLine("        Where A.CancelInd='N' ");
	        SQL.AppendLine("        And A.Yr=@Yr1 ");
	        SQL.AppendLine("        And A.Mth=@Mth1 ");
            SQL.AppendLine("    ) T2 On T1.SSCode=T2.SSCode And T1.EmpCode=T2.EmpCode And T1.CardNo=T2.CardNo ");
            SQL.AppendLine("    Left Join ( ");
	        SQL.AppendLine("        Select B.SSCode, B.EmpCode, B.CardNo, B.EmpInd, B.FamilyStatus, B.PName, B.DeptCode, B.JoinDt, B.ResignDt, B.StartDt, B.EndDt ");   
	        SQL.AppendLine("        From TblEmpSSListHdr A ");
	        SQL.AppendLine("        Inner Join TblEmpSSListDtl B On A.DocNo=B.DocNo ");
	        SQL.AppendLine("        Where A.CancelInd='N' ");
	        SQL.AppendLine("        And A.Yr=@Yr2 ");
	        SQL.AppendLine("        And A.Mth=@Mth2 ");
            SQL.AppendLine("    ) T3 On T1.SSCode=T3.SSCode And T1.EmpCode=T3.EmpCode And T1.CardNo=T3.CardNo ");
            SQL.AppendLine("    Inner Join TblSS T4 On T1.SSCode=T4.SSCode ");
            SQL.AppendLine(") Tbl1 ");
            SQL.AppendLine("Left Join TblEmployee Tbl2 On Tbl1.EmpCode=Tbl2.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment Tbl3 On Tbl1.DeptCode=Tbl3.DeptCode ");
            SQL.AppendLine("Left Join TblOption Tbl4 On Tbl1.FamilyStatus=Tbl4.OptCode And Tbl4.OptCat='FamilyStatus' ");
            SQL.AppendLine("Where Tbl1.Current<>Tbl1.Previous ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By Tbl1.EmpCode, Tbl1.SSPCode Desc, Tbl1.SSName, Tbl1.CardNo, Tbl1.EmpInd Desc; ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Employee Code",
                        "Participants Name",
                        "Status", 
                        "Old"+Environment.NewLine+"Code",
                        "Department",

                        //6-10
                        "Join"+Environment.NewLine+"Date",
                        "Resign"+Environment.NewLine+"Date",
                        "Social Security",
                        "Card#",
                        "Start"+Environment.NewLine+"Date",
                        
                        //11-13
                        "End"+Environment.NewLine+"Date",
                        "Previous",
                        "Current"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        100, 250, 100, 100, 180, 
                        
                        //6-10
                        100, 100, 200, 150, 100, 
                        
                        //11-13
                        100, 80, 80
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 12, 13 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 6, 7, 10, 11 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 6, 7, 10, 11 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Sm.ClearGrd(Grd1, false);

                if (
                    Sm.IsLueEmpty(LueYr, "Year") ||
                    Sm.IsLueEmpty(LueMth, "Month")
                    ) return;

                Cursor.Current = Cursors.WaitCursor;

                string
                    Yr1 = Sm.GetLue(LueYr),
                    Mth1 = Sm.GetLue(LueMth),
                    Yr2 = string.Empty,
                    Mth2 = string.Empty;

                if (Mth1 == "01")
                {
                    Mth2 = "12";
                    Yr2 = (int.Parse(Yr1) - 1).ToString();
                }
                else
                {
                    Mth2 = Sm.Right("0"+(int.Parse(Mth1) - 1).ToString(), 2);
                    Yr2 = Yr1;
                }

                Grd1.Cols[12].Text = "Previous" + Environment.NewLine + Sm.GetMonthName(int.Parse(Mth2)) + " " + Yr2;
                Grd1.Cols[13].Text = "Current" + Environment.NewLine + Sm.GetMonthName(int.Parse(Mth1)) + " " + Yr1;

                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@Yr1", Yr1);
                Sm.CmParam<String>(ref cm, "@Mth1", Mth1);
                Sm.CmParam<String>(ref cm, "@Yr2", Yr2);
                Sm.CmParam<String>(ref cm, "@Mth2", Mth2);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "Tbl1.DeptCode", true);
                switch (Sm.GetLue(LueStatus))
                {
                    case "1": Filter += " And Tbl1.Current='Y' "; break;
                    case "2": Filter += " And Tbl1.Previous='Y' "; break;
                }

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = GetSQL(Filter);
                    cm.CommandTimeout = 600;
                    using (var dr = cm.ExecuteReader())
                    {
                        var c = Sm.GetOrdinal(dr, new string[] 
                            { 
                                //0
                                "EmpCode", 

                                //1-5
                                "PName", "FamilyStatus", "EmpCodeOld", "DeptName", "JoinDt", 
                                
                                //6-10
                                "ResignDt", "SSName", "CardNo", "StartDt", "EndDt",   
                                
                                //11-12
                                "Previous", "Current"
                            });
                        if (!dr.HasRows)
                            Sm.StdMsg(mMsgType.NoData, "");
                        else
                        {
                            int Row = 0, r = 0;
                            Grd1.Rows.Count = 0;
                            Grd1.ProcessTab = true;
                            Grd1.BeginUpdate();
                            while (dr.Read())
                            {

                                Grd1.Rows.Add();
                                r++;
                                Row = r - 1;
                                Grd1.Cells[Row, 0].Value = r;
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 5);
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 7, 6);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 10, 9);
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 11, 10);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 12, 11);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 13, 12);
                            }
                            Grd1.EndUpdate();
                            dr.Close();
                            dr.Dispose();
                            cm.Dispose();
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'New Participant' As Col2 Union All " +
                "Select '2' As Col1, 'Withdrawing Participant' As Col2; ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion


        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        #endregion

        #endregion
    }
}
