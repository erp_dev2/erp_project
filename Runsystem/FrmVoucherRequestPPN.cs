﻿#region Update
/*
    26/04/2017 [TKG] tambah purchase return invoice
    12/05/2017 [TKG] untuk ppn masukan, parameter jenis tax bisa lbh dari 1.
    12/05/2017 [TKG] bug fixing dokumen ppn bisa dipilih lbh dari 1 kali. 
    18/05/2017 [TKG] tambah voucher request tax. 
    26/05/2017 [WED] tambah status approval VR VAT
    09/06/2017 [TKG] tambah pilihan dari POS
    19/07/2017 [TKG] tambah exchange rate dan amount main currency untuk purchase invoice
    27/07/2017 [ARI] tambah print out
    30/07/2017 [TKG] bisa edit data, tambah rate dam indikator final.
    02/08/2017 [TKG] bug saat generate vr#
    10/08/2017 [TKG] untuk PI dan PRI, ambil tax# dan tax date 2+3 yg baru
    22/08/2017 [TKG] menggunakan tax group
    28/08/2017 [ARI] Tambah Tax Group di printout 
    31/08/2017 [TKG] bg fixing saat generate vr utk data yg belum final.
    03/09/2017 [TKG] tambah informasi outgoing payment dan date, incoming payment# dan date
                     vr tax hanya yg sudah divoucherkan
    29/09/2017 [TKG] remark bisa diupdate sebelum final.
    12/11/2017 [TKG] VR Tax bisa in atau out berdasarkan debit/creditnya.
    28/11/2017 [TKG] perubahan perhitungan vr tax
    14/04/2018 [TKG] menggunakan format khusus voucher atau format standard dokumen run system berdasarkan parameter VoucherCodeFormatType
    31/07/2018 [WED] tambah CSV PPH23
    31/07/2018 [WED] tambah field Tax Period (Masa Pajak)
    31/07/2018 [WED] tambah CSV PPN In dan CSV PPN Out
    01/08/2018 [WED] Kolom TaxInvoice# untuk tipe POS, tidak readonly saat insert
    06/08/2018 [WED] Tax Period bisa diubah selama belum final
    06/08/2018 [WED] Tambah tombol print PPH23
    07/08/2018 [WED] Print out PPH23, dibagian Jasa Lain, ambil dari master Service nya. Kalau nggak ada Service nya, ambil dari Vendor
    13/08/2018 [WED] tambah validasi In tidak boleh lbh besar dari out
    15/08/2018 [WED] CSV PPN In dan Out format nya dirubah menyesuaikan e-faktur
    21/08/2018 [WED] keterangan jasa di printout PPH23, ditambahin dari ServiceNote di PI
    21/12/2018 [TKG] pph23 tambar vr tax
    08/01/2019 [TKG] pada ppn masukan, tanda - dan . dihilangkan di informasi nomor faktur pajak dan npwp.
    09/01/2019 [TKG] pada ppn masukan, perubahan format kode jenis transaksi (tambah 0) dan nomor faktur pajak (3 angka di depan dihilangkan).
    25/01/2019 [TKG] utk issue tax invoice# purchase return
    02/04/2019 [WED] PI, PRI, VRTax tidak perlu di voucher kan untuk bisa diproses di VRVAT
    13/05/2019 [TKG] tambah PI raw material
    03/07/2019 [TKG] apabila in > out, user masih bisa menyimpan data.
    08/08/2019 [TKG] PI raw material menghasilkan vr dengan tipe credit.
    15/09/2020 [IBL/YK] Bisa menarik sales invoice for project
    14/12/2020 [ICA/SIER] menambah validasi IsDocAlreadyProceedToVoucher, jika document sudah divoucherkan maka tidak bisa diedit
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    08/02/2021 [WED/IOK] tambah informasi total In dan Total Out
    25/10/2021 [TKG/TWC] menggunakan parameter IsVoucherDocSeqNoEnabled saat membuat Voucher Request#
    29/11/2021 [RDA/PHT] tambah field Billing ID berdasarkan parameter IsVRVATUseBillingID
    01/12/2021 [BRI/IOK] tambah button Excel untuk tab In dan tab Out
    02/12/2021 [IBL/PHT] Fasilitas send csv ke BRI
    13/12/2021 [HAR/PHT] BUG saat save bank, yang tersimpan malah bank account code
    03/02/2022 [BRI/PHT] tambah filter multi profit center saat memilih tab in dan tab out berdasarkan param IsFicoUseMultiProfitCenterFilter
    07/06/2022 [DITA/IOK] save taxcode PI untuk keperluan jika ada update tax di PI
    30/06/2022 [BRI/PHT] merubah nama file csv
    09/08/2022 [SET/PHT] add param SalesInvoiceTaxCalculationFormula untuk penyesuaian source amount tax
    11/08/2022 [MYA/SIER] Memunculkan seluruh approver pada tab approval information (seperti pada monitoring document approval setting) dan menambahkan kolom jabatan di menu Voucher Request VAT
    22/09/2022 [SET/IOK] add parameter IsVRVATFilteredByTaxGroup
    27/09/2022 [ICA/PHT] Source amount berdasarkan param PurchaseInvoiceTaxCalculationFormula, dapat menarik lebih dari satu tax dari PI, penambahan kolom
    17/01/2023 [VIN/PHT] VR bank account ambil dari LueBankAcCode
    19/01/2023 [TYO/IOK] menambah mDocTitle
    02/02/2023 [MAU/PHT] validasi monthly closing journal VR VAT
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using System.IO;
using System.Net;
using System.Threading;
using Renci.SshNet;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestPPN : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mAcType = string.Empty,
            mPPNTaxCode = string.Empty,
            mMainCurCode = string.Empty,
            mSalesInvoiceTaxCalculationFormula = string.Empty,
            mPurchaseInvoiceRawMaterialTaxGrpCode = string.Empty,
            mPurchaseInvoiceTaxCalculationFormula = string.Empty,
            mDocTitle = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmVoucherRequestPPNFind FrmFind;
        internal bool 
            mIsFilterByDept = false,
            mIsPurchaseReturnInvoiceCanOnlyHave1Record = false,
            mIsSalesReturnInvoiceCanOnlyHave1Record = false,
            mIsFicoUseMultiProfitCenterFilter = false,
            mIsVRVATFilteredByTaxGroup = false,
            mIsPurchaseInvoiceUsePORevision = false;
        private bool
            mIsVoucherRequestPPNApprovalBasedOnDept = false,
            mSaveState = false,
            mIsVRVATUseBillingID = false,
            mIsVRVATSendtoBank = false,
            mIsCSVUseRealAmt = false,
            mIsVoucherBankAccountFilteredByCurrency = false,
            mIsVoucherRequestBankAccountFilteredByGrp = false,
            mIsTransactionUseDetailApprovalInformation = false,
            mIsAutoJournalActived = false,
            mIsVRVATTransactionValidatedbyClosingJournal = false;
        private string
            mVoucherCodeFormatType = "1",
            mPPH23TaxGrpCode = string.Empty,
            mPPNTaxGrpCode = string.Empty,
            mPPH23TaxCode = string.Empty,
            mEmptaxwithholder = string.Empty,
            //send csv to bri
            mHostAddrForBRIVR = string.Empty,
            mSharedFolderForBRIVR = string.Empty,
            mSharedFolderForBRIVRMPN = string.Empty,
            mUserNameForBRIVR = string.Empty,
            mPasswordForBRIVR = string.Empty,
            mPortForBRIVR = string.Empty,
            mProtocolForBRIVR = string.Empty,
            mPathToSaveExportedBRIVR = string.Empty,
            //end send csv to bri
            mBankAccountFormat = string.Empty;
        #endregion

        #region Constructor

        public FrmVoucherRequestPPN(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Voucher Request VAT";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueTaxGrpCode(ref LueTaxGrpCode);
                Sl.SetLueBankCode(ref LueBankCode);
                SetLueDeptCode(ref LueDeptCode, string.Empty);
                Sl.SetLueVoucherDocType(ref LueDocType);
                Sl.SetLueVoucherPaymentType(ref LuePaymentType);
                Sl.SetLueCurCode(ref LueCurCode);
                SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                Sm.SetLue(LueCurCode, mMainCurCode);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtVoucherRequestDocNo, TxtVoucherDocNo, TxtStatus }, true);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (!mIsVRVATUseBillingID)
                {
                    TxtBillingID.Visible = label17.Visible = false;
                    LblBankAcCode.Visible = LueBankAcCode.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region PPn In

            Grd3.Cols.Count = 18;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Type",
                        "Type",
                        "Document#",
                        "Date",
                        
                        //6-10
                        "Description",
                        "Tax Invoice#",
                        "Tax Invoice Date",
                        "Amount",
                        "Rate",

                        //11-14
                        "Amount" + Environment.NewLine + "(" + mMainCurCode + ")",
                        "Currency",
                        "No", 
                        "TaxCode",
                        "",

                        //16-17
                        "Tax Invoice Name",
                        "Tax Invoice Rate"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        20, 0, 180, 130, 80,
                        
                        //6-10
                        400, 130, 120, 120, 100,

                        //11-14
                        120, 80, 50, 0, 20, 

                        //16-17
                        140, 120
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 1, 15 });
            Sm.GrdColReadOnly(Grd3, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17 });
            Sm.GrdFormatDec(Grd3, new int[] { 9, 10, 11, 17 }, 0);
            Sm.GrdFormatDate(Grd3, new int[] { 5, 8 });
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2, 5, 14 }, false);
            Grd3.Cols[12].Move(9);
            Grd3.Cols[13].Move(0);
            Grd3.Cols[15].Move(3);
            Grd3.Cols[16].Move(11);
            Grd3.Cols[17].Move(12);

            #endregion

            #region PPn Out

            Grd2.Cols.Count = 18;
            Grd2.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Type",
                        "Type",
                        "Document#",
                        "Date",
                        
                        //6-10
                        "Description",
                        "Tax Invoice#",
                        "Tax Invoice Date",
                        "Amount",
                        "Rate",

                        //11-12
                        "Amount" + Environment.NewLine + "(" + mMainCurCode + ")",
                        "Currency",
                        "No",
                        "TaxCode",
                        "",

                        //16-17
                        "Tax Invoice Name",
                        "Tax Invoice Rate"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        20, 0, 180, 130, 80,
                        
                        //6-10
                        400, 130, 120, 120, 120,

                        //11-12
                        120, 80, 50, 0, 20, 

                        //16-17
                        140, 120
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 1, 15 });
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17 });
            Sm.GrdFormatDec(Grd2, new int[] { 9, 10, 11, 17 }, 0);
            Sm.GrdFormatDate(Grd2, new int[] { 5, 8 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2, 5, 14 }, false);
            Grd2.Cols[12].Move(9);
            Grd2.Cols[13].Move(0);
            Grd2.Cols[15].Move(3);
            Grd2.Cols[16].Move(11);
            Grd2.Cols[17].Move(12);

            #endregion

            #region Approval Info

            Grd4.Cols.Count = 6;
            Grd4.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[]
                    {
                        //0
                        "No",

                        //1-4
                        "User",
                        "Position",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd4, new int[] { 4 });
            Sm.GrdColReadOnly(Grd4, new int[] { 0, 1, 2, 3, 4, 5 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtAmt, MeeCancelReason, LueDeptCode, 
                        LueDocType, LueCurCode, LuePaymentType, LueBankCode, TxtGiroNo, 
                        DteDueDt, LuePIC, LueTaxGrpCode, MeeRemark, TxtTaxPeriod, TxtBillingID, LueBankAcCode
                    }, true);
                    ChkFinalInd.Properties.ReadOnly = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    BtnPPH23.Enabled = BtnPPNIn.Enabled = BtnPPNOut.Enabled = BtnPrintPPH23.Enabled = BtnPPH23.Visible = BtnPPNIn.Visible = BtnPPNOut.Visible = BtnPrintPPH23.Visible = false;
                    if(IsAbleToGenerateCSV())
                    {
                        if(Sm.GetLue(LueTaxGrpCode) == mPPH23TaxGrpCode)
                            BtnPPH23.Enabled = BtnPPH23.Visible = BtnPrintPPH23.Enabled = BtnPrintPPH23.Visible = true;
                        if (Sm.GetLue(LueTaxGrpCode) == mPPNTaxGrpCode)
                            BtnPPNIn.Enabled = BtnPPNOut.Enabled = BtnPPNIn.Visible = BtnPPNOut.Visible = true;
                    }

                    //Send Csv to Bank
                    if (mIsVRVATUseBillingID && TxtBillingID.Text.Length > 0)
                    {
                        if (!BtnSave.Enabled &&
                                TxtDocNo.Text.Length > 0 &&
                                mIsVRVATSendtoBank &&
                                !ChkCancelInd.Checked &&
                                Sm.GetValue("Select Status From TblVoucherRequestPPNHdr Where DocNo = '" + TxtDocNo.Text + "'") == "A" &&
                                !Sm.IsDataExist("Select 1 From TblVoucherHdr Where VoucherRequestDocNo = @Param", TxtDocNo.Text) &&
                                Sm.GetValue("Select CSVInd From TblVoucherRequestPPNHdr Where DocNo = '" + TxtDocNo.Text + "'") == "N"
                            )
                        {
                            BtnCSV.Visible = true;
                            BtnCSV.Enabled = true;
                        }
                        else
                        {
                            BtnCSV.Visible = false;
                            BtnCSV.Enabled = false;
                        }
                    }

                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                    Sm.SetLue(LueDocType, "15");
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueDeptCode, LuePaymentType, LuePIC, LueTaxGrpCode, 
                        MeeRemark, TxtTaxPeriod, TxtBillingID, LueBankAcCode
                    }, false);
                    ChkFinalInd.Properties.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    BtnPPH23.Enabled = BtnPPNIn.Enabled = BtnPPNOut.Enabled = BtnPrintPPH23.Enabled = BtnPPH23.Visible = BtnPPNIn.Visible = BtnPPNOut.Visible = BtnPrintPPH23.Visible = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    BtnPPH23.Enabled = BtnPPNIn.Enabled = BtnPPNOut.Enabled = BtnPrintPPH23.Enabled = BtnPPH23.Visible = BtnPPNIn.Visible = BtnPPNOut.Visible = BtnPrintPPH23.Visible = false;
                    if (!ChkFinalInd.Checked)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeRemark, TxtTaxPeriod }, false);
                        ChkFinalInd.Properties.ReadOnly = false;
                        Grd3.ReadOnly = false;
                        Grd2.ReadOnly = false;
                        ChkFinalInd.Focus();
                    }
                    else
                        ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mAcType = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, LueDeptCode, LueDocType, 
                LuePaymentType, LueBankCode, TxtGiroNo, DteDueDt, LuePIC, 
                MeeRemark, TxtVoucherRequestDocNo, TxtVoucherDocNo, TxtStatus, LueTaxGrpCode,
                TxtBillingID, LueBankAcCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt, TxtTotalInAmt, TxtTotalOutAmt }, 0);
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtTaxPeriod }, 11);
            ChkFinalInd.Checked = false;
            ChkCancelInd.Checked = false;
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 9, 10, 11, 17 });
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 9, 10, 11 });
            SetNo(ref Grd2);
            SetNo(ref Grd3);
            Sm.FocusGrd(Grd2, 0, 1);
            Sm.FocusGrd(Grd3, 0, 1);
            mSaveState = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVoucherRequestPPNFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueUserCode(ref LuePIC, string.Empty);
                SetLueDeptCode(ref LueDeptCode, string.Empty);
                SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                ChkFinalInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false) || IsDocAlreadyCancelled() || IsDocAlreadyProceedToVoucher()) return;
            SetFormControl(mState.Edit); 
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            mSaveState = true;
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || 
                IsDocNotYetFinal() ||
                IsDocAlreadyCancelled() ||
                Sm.StdMsgYN("Print", "") == DialogResult.No
                ) return;

            string[] TableName = { "VoucherReq", "VoucherReqDtl", "VoucherReqDtl2" };

            var l = new List<VoucherReqHdr>();
            var ldtl = new List<VoucherReqDtl>();
            var ldtl2 = new List<VoucherReqDtl2>();

            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine(" Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
            SQL.AppendLine(" A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.CancelInd, A.DocType, A.AcType, B.VoucherDocNo, ");
            SQL.AppendLine(" (Select DATE_FORMAT(DocDt,'%d %M %Y') As DocDt from tblvoucherhdr Where DocNo=B.VoucherDocNo Limit 1) As VoucherDocDt, ");
            SQL.AppendLine(" Concat(IfNull(C.BankAcNo, ''), ' [', IfNull(C.BankAcNm, ''), ']') As BankAcc, ");
            SQL.AppendLine(" (Select OptDesc From TblOption Where OptCat='VoucherPaymentType' AND OptCode=A.PaymentType Limit 1) As PaymentType, ");
            SQL.AppendLine(" A.GiroNo,E.BankName As GiroBankName, DATE_FORMAT(A.DueDt,'%d %M %Y') As GiroDueDt, F.UserName As PICName, Round(A.Amt,2) As AmtHdr, A.Remark As RemarkHdr, ");
            SQL.AppendLine(" A.CurCode, B.DocEnclosure, B.PaymentUser, G.DeptName, ");
            SQL.AppendLine(" (Select ParValue From tblparameter Where ParCode='IsPrintOutVoucherRequestShowDocument') As PrintFormat, H.TaxGrpName ");
            SQL.AppendLine(" From TblVoucherRequestPPNHdr A ");
            SQL.AppendLine(" Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine(" Left Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
            SQL.AppendLine(" Left Join TblBank D On C.BankCode=D.BankCode ");
            SQL.AppendLine(" Left Join TblBank E On A.BankCode=E.BankCode ");
            SQL.AppendLine(" Left Join TblUser F On A.PIC=F.UserCode ");
            SQL.AppendLine(" Left Join TblDepartment G On A.DeptCode = G.DeptCode ");
            SQL.AppendLine(" Left Join TblTaxGrp H On A.TaxGrpCode=H.TaxGrpCode ");
            SQL.AppendLine(" Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyName",

                         //1-5
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocNo", 
                         "DocDt", 
                        
                         //6-10
                         "CancelInd",
                         "DocType", 
                         "AcType",
                         "VoucherDocNo", 
                         "VoucherDocDt", 
                          
                         //11-15
                         "BankAcc",
                         "PaymentType",
                         "GiroNo",
                         "GiroBankName", 
                         "GiroDueDt", 
                         
                         //16-20
                         "PICName",
                         "AmtHdr", 
                         "RemarkHdr",
                         "CompanyLogo",
                         "CurCode",
                         
                         //21-25
                         "DocEnclosure",
                         "PaymentUser",
                         "DeptName",
                         "PrintFormat",
                         "TaxGrpName"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new VoucherReqHdr()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),
                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyPhone = Sm.DrStr(dr, c[2]),
                            CompanyFax = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),
                            CancelInd = Sm.DrStr(dr, c[6]),
                            DocType = Sm.DrStr(dr, c[7]),
                            AcType = Sm.DrStr(dr, c[8]),
                            VoucherDocNo = Sm.DrStr(dr, c[9]),
                            VoucherDocDt = Sm.DrStr(dr, c[10]),
                            BankAcc = Sm.DrStr(dr, c[11]),
                            PaymentType = Sm.DrStr(dr, c[12]),
                            GiroNo = Sm.DrStr(dr, c[13]),
                            GiroBankName = Sm.DrStr(dr, c[14]),
                            GiroDueDt = Sm.DrStr(dr, c[15]),
                            PICName = Sm.DrStr(dr, c[16]),
                            AmtHdr = Sm.DrDec(dr, c[17]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[17])) + dec(Sm.DrDec(dr, c[17])),
                            Terbilang2 = Sm.Terbilang2(Sm.DrDec(dr, c[17])),
                            RemarkHdr = Sm.DrStr(dr, c[18]),

                            CompanyLogo = Sm.DrStr(dr, c[19]),
                            CurCode = Sm.DrStr(dr, c[20]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            DocEnclosure = Sm.DrDec(dr, c[21]),
                            PaymentUser = Sm.DrStr(dr, c[22]),
                            Department = Sm.DrStr(dr, c[23]),
                            PrintFormat = Sm.DrStr(dr, c[24]),
                            TaxGrpName = Sm.DrStr(dr, c[25]),
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            #endregion

            #region Detail In

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.DNo, Case A.DocType ");
                SQLDtl.AppendLine("     When '1' Then 'Purchase Invoice' ");
                SQLDtl.AppendLine("     When '2' Then 'Purchase Return Invoice' ");
                SQLDtl.AppendLine("     When '3' Then 'Voucher Request Tax' ");
                SQLDtl.AppendLine("     When '4' Then 'Purchase Invoice Raw Material' ");
                SQLDtl.AppendLine("End As DocTypeDesc, A.DocNoIn, A.Amt, A.Amt*A.ExcRate As Amt2, A.Remark ");
                SQLDtl.AppendLine("From TblVoucherRequestPPNDtl A ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");
               
                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DNo",

                         //1-5
                         "DocTypeDesc",
                         "DocNoIn",
                         "Amt",
                         "Amt2",
                         "Remark" 
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new VoucherReqDtl()
                        {
                            DNo = Sm.DrStr(drDtl, cDtl[0]),
                            DocTypeDesc = Sm.DrStr(drDtl, cDtl[1]),
                            DocNoIn = Sm.DrStr(drDtl, cDtl[2]),
                            Amt = Sm.DrDec(drDtl, cDtl[3]),
                            Amt2 = Sm.DrDec(drDtl, cDtl[4]),
                            Remark = Sm.DrStr(drDtl, cDtl[5])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            #endregion

            #region Detail Out

            var cmDtl2 = new MySqlCommand();
            var SQLDtl2 = new StringBuilder();

            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select A.DNo, "); 
                SQLDtl2.AppendLine("Case A.DocType ");
                SQLDtl2.AppendLine("    When '1' Then 'Sales Invoice' ");
                SQLDtl2.AppendLine("    When '2' Then 'Sales Return Invoice' ");
                SQLDtl2.AppendLine("    When '3' Then 'Point of Sale' ");
                SQLDtl2.AppendLine("    When '4' Then 'Sales Invoice for Project' ");
                SQLDtl2.AppendLine("End As DocTypeDesc, ");
                SQLDtl2.AppendLine("A.DocNoOut, "); 
                SQLDtl2.AppendLine("Case A.DocType ");
                SQLDtl2.AppendLine("    When '1' Then Date_Format(B.DocDt,'%d %M %Y') ");
                SQLDtl2.AppendLine("    When '2' Then Date_Format(C.DocDt,'%d %M %Y') ");
                SQLDtl2.AppendLine("    When '3' Then Date_Format(D.DocDt,'%d %M %Y') ");
                SQLDtl2.AppendLine("    When '4' Then Date_Format(E.DocDt,'%d %M %Y') ");
                SQLDtl2.AppendLine("End As DocDt, ");
                SQLDtl2.AppendLine("A.Remark, A.Amt*A.ExcRate As Amt ");
                SQLDtl2.AppendLine("From TblVoucherRequestPPNDtl2 A ");
                SQLDtl2.AppendLine("Left Join TblSalesInvoiceHdr B On A.DocNoOut=B.DocNo ");
                SQLDtl2.AppendLine("Left Join TblSalesReturnInvoiceHdr C On A.DocNoOut=C.DocNo ");
                SQLDtl2.AppendLine("Left Join TblPOSVat D On A.DocNoOut=D.DocNo ");
                SQLDtl2.AppendLine("Left Join TblSalesInvoice5Hdr E On A.DocNoOut=E.DocNo ");
                SQLDtl2.AppendLine("Where A.DocNo=@DocNo ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "DNo",

                         //1-5
                         "DocTypeDesc",
                         "DocNoOut",
                         "DocDt",
                         "Remark",
                         "Amt"
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new VoucherReqDtl2()
                        {
                            DNo = Sm.DrStr(drDtl2, cDtl2[0]),
                            DocTypeDesc = Sm.DrStr(drDtl2, cDtl2[1]),
                            DocNoOut = Sm.DrStr(drDtl2, cDtl2[2]),
                            DocDt = Sm.DrStr(drDtl2, cDtl2[3]),
                            Remark = Sm.DrStr(drDtl2, cDtl2[4]),
                            Amt = Sm.DrDec(drDtl2, cDtl2[5]),
                            
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);

            #endregion

            Sm.PrintReport("VoucherRequestVAT", myLists, TableName, false);
        }

        override protected void BtnExcelClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false)) return;

            if (TcPPn.SelectedTabPage == TpPPnIn)
                Sm.ExportToExcel(Grd3);
            if (TcPPn.SelectedTabPage == TpPPnOut)
                Sm.ExportToExcel(Grd2);
        }

        #endregion

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            bool NoNeedApproval = IsDocApprovalSettingNotExisted();
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequestPPN", "TblVoucherRequestPPNHdr");
            string VoucherRequestDocNo = string.Empty;

            if (ChkFinalInd.Checked)
            {
                if (mVoucherCodeFormatType == "2")
                    VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr");
                else
                    VoucherRequestDocNo = GenerateVoucherRequestDocNo();
            }

            var cml = new List<MySqlCommand>();
            
            cml.Add(SaveVoucherRequestPPNHdr(DocNo, VoucherRequestDocNo, NoNeedApproval));

            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0) cml.Add(SaveVoucherRequestPPNDtl(DocNo, Row));
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 4).Length > 0) cml.Add(SaveVoucherRequestPPNDtl2(DocNo, Row));
            }

            if (ChkFinalInd.Checked)
            {
                cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo, DocNo));
                cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo));
            }

            cml.Add(UpdatePOSVAT(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||

                (mIsVRVATTransactionValidatedbyClosingJournal && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, false, Sm.GetDte(DteDocDt), GetProfitCenterCode()))||

                Sm.IsLueEmpty(LuePIC, "Person In Charge") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsLueEmpty(LueDocType, "Document Type") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment Type") ||
                Sm.IsLueEmpty(LueTaxGrpCode, "Tax") ||
                IsPaymentTypeNotValid() ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsFinalDataInBiggerThanOut() ||
                (mIsVRVATUseBillingID && Sm.IsTxtEmpty(TxtBillingID, "Billing ID", false)) ||
                (mIsVRVATUseBillingID && Sm.IsLueEmpty(LueBankAcCode, "Bank Account"));
        }

        private bool IsFinalDataInBiggerThanOut()
        {
            if (ChkFinalInd.Checked)
            {
                decimal In = 0m, Out = 0m;

                for (int r = 0; r < Grd3.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd3, r, 11).Length>0)
                        In += Sm.GetGrdDec(Grd3, r, 11);
                }

                for (int r = 0; r < Grd2.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd2, r, 11).Length > 0)
                        Out += Sm.GetGrdDec(Grd2, r, 11);
                }

                if (In > Out)
                {
                    return Sm.StdMsgYN("Question", 
                        "PPn In is be bigger than PPn Out." +Environment.NewLine + 
                        "Do you want to save this data ?")==DialogResult.No;
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd3.Rows.Count == 1 && Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in PPn In and / or PPn Out.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var SQL = new StringBuilder();
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd3, Row, 4, false, "PPn In Document# is empty.")) return true;
                if (Sm.GetGrdStr(Grd3, Row, 2) == "1")
                {
                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblPurchaseInvoiceHdr ");
                    SQL.AppendLine("Where CancelInd='N' ");
                    SQL.AppendLine("And DocNo=@Param1 ");
                    SQL.AppendLine("And ( ");
                    SQL.AppendLine("   (IfNull(TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And VoucherRequestPPNDocNo Is Not Null And VoucherRequestPPNDocNo<>@Param3) Or ");
                    SQL.AppendLine("   (IfNull(TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And VoucherRequestPPNDocNo2 Is Not Null And VoucherRequestPPNDocNo2<>@Param3) Or ");
                    SQL.AppendLine("   (IfNull(TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And VoucherRequestPPNDocNo3 Is Not Null And VoucherRequestPPNDocNo3<>@Param3) ");
                    SQL.AppendLine(");");
                    if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd3, Row, 4), Sm.GetLue(LueTaxGrpCode), TxtDocNo.Text))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd3, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd3, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to another voucher request vat."
                            );
                        return true;
                    }    

                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblPurchaseInvoiceHdr ");
                    SQL.AppendLine("Where CancelInd='N' ");
                    SQL.AppendLine("And DocNo=@Param1 ");
                    SQL.AppendLine("And ( ");
                    SQL.AppendLine("   IfNull(TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And VoucherRequestPPNCompleteInd='Y' ");
                    SQL.AppendLine("   Or IfNull(TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And VoucherRequestPPNCompleteInd2='Y' ");
                    SQL.AppendLine("   Or IfNull(TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And VoucherRequestPPNCompleteInd3='Y' ");
                    SQL.AppendLine(");");
                    if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd3, Row, 4), Sm.GetLue(LueTaxGrpCode), string.Empty))
                    {
                        Sm.StdMsg(mMsgType.Warning, 
                            "Type : " + Sm.GetGrdStr(Grd3, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd3, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to voucher request vat/vat settlement."
                            );
                        return true;
                    }    
                }
                if (Sm.GetGrdStr(Grd3, Row, 2) == "2")
                {
                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblPurchaseReturnInvoiceHdr ");
                    SQL.AppendLine("Where CancelInd='N' ");
                    SQL.AppendLine("And DocNo=@Param1 ");
                    SQL.AppendLine("And ( ");
                    SQL.AppendLine("   (IfNull(TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And VoucherRequestPPNDocNo Is Not Null And VoucherRequestPPNDocNo<>@Param3) Or ");
                    SQL.AppendLine("   (IfNull(TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And VoucherRequestPPNDocNo2 Is Not Null And VoucherRequestPPNDocNo2<>@Param3) Or ");
                    SQL.AppendLine("   (IfNull(TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And VoucherRequestPPNDocNo3 Is Not Null And VoucherRequestPPNDocNo3<>@Param3) ");
                    SQL.AppendLine(");");
                    if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd3, Row, 4), Sm.GetLue(LueTaxGrpCode), TxtDocNo.Text))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd3, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd3, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to another voucher request vat."
                            );
                        return true;
                    }    

                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblPurchaseReturnInvoiceHdr ");
                    SQL.AppendLine("Where CancelInd='N' ");
                    SQL.AppendLine("And DocNo=@Param1 ");
                    SQL.AppendLine("And ( ");
                    SQL.AppendLine("   IfNull(TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And VoucherRequestPPNCompleteInd='Y' ");
                    SQL.AppendLine("   Or IfNull(TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And VoucherRequestPPNCompleteInd2='Y' ");
                    SQL.AppendLine("   Or IfNull(TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And VoucherRequestPPNCompleteInd3='Y' ");
                    SQL.AppendLine(");");
                    if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd3, Row, 4), Sm.GetLue(LueTaxGrpCode), string.Empty))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd3, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd3, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to voucher request vat/vat settlement."
                            );
                        return true;
                    }
                }

                if (Sm.GetGrdStr(Grd3, Row, 2) == "3")
                {
                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblVoucherRequestTax ");
                    SQL.AppendLine("Where CancelInd='N' ");
                    SQL.AppendLine("And DocNo=@Param1 ");
                    SQL.AppendLine("And ( ");
                    SQL.AppendLine("   IfNull(TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And VoucherRequestPPNDocNo Is Not Null And VoucherRequestPPNDocNo<>@Param3 ");
                    SQL.AppendLine(");");
                    if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd3, Row, 4), Sm.GetLue(LueTaxGrpCode), TxtDocNo.Text))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd3, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd3, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to another voucher request vat."
                            );
                        return true;
                    }    

                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblVoucherRequestTax ");
                    SQL.AppendLine("Where CancelInd='N' ");
                    SQL.AppendLine("And DocNo=@Param1 ");
                    SQL.AppendLine("And IfNull(TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@Param2) And VoucherRequestPPNCompleteInd='Y';");
                    if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd3, Row, 4), Sm.GetLue(LueTaxGrpCode), string.Empty))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd3, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd3, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to voucher request vat/vat settlement."
                            );
                        return true;
                    }
                }

                if (Sm.GetGrdStr(Grd3, Row, 2) == "4")
                {
                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr ");
                    SQL.AppendLine("Where CancelInd='N' ");
                    SQL.AppendLine("And TaxInd='Y' ");
                    SQL.AppendLine("And DocNo=@Param1 ");
                    SQL.AppendLine("And VoucherRequestPPNDocNo Is Not Null ");
                    SQL.AppendLine("And VoucherRequestPPNDocNo<>@Param2;");
                    if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd3, Row, 4), TxtDocNo.Text, string.Empty))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd3, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd3, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to another voucher request vat."
                            );
                        return true;
                    }

                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr ");
                    SQL.AppendLine("Where CancelInd='N' ");
                    SQL.AppendLine("And TaxInd='Y' ");
                    SQL.AppendLine("And DocNo=@Param ");
                    SQL.AppendLine("And VoucherRequestPPNCompleteInd='Y';");
                    if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd3, Row, 4)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd3, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd3, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to voucher request vat/vat settlement."
                            );
                        return true;
                    }
                }
            }

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd2, Row, 4, false, "PPn Out Document# is empty.")) return true;
                if (Sm.GetGrdStr(Grd2, Row, 2) == "1")
                {
                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblSalesInvoiceHdr ");
                    SQL.AppendLine("Where CancelInd='N' ");
                    SQL.AppendLine("And DocNo=@Param1 ");
                    SQL.AppendLine("And (VoucherRequestPPNDocNo Is Not Null And VoucherRequestPPNDocNo<>@Param2);");

                    if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd2, Row, 4), TxtDocNo.Text, string.Empty))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to another voucher request vat."
                            );
                        return true;
                    }    

                    if (Sm.IsDataExist(
                        "Select DocNo From TblSalesInvoiceHdr " +
                        "Where CancelInd='N' And DocNo=@Param And VoucherRequestPPNCompleteInd='Y';", 
                        Sm.GetGrdStr(Grd2, Row, 4)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to voucher request vat/vat settlement."
                            );
                        return true;
                    }
                }

                if (Sm.GetGrdStr(Grd2, Row, 2) == "2")
                {
                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblSalesReturnInvoiceHdr ");
                    SQL.AppendLine("Where CancelInd='N' ");
                    SQL.AppendLine("And DocNo=@Param1 ");
                    SQL.AppendLine("And (VoucherRequestPPNDocNo Is Not Null And VoucherRequestPPNDocNo<>@Param2);");

                    if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd2, Row, 4), TxtDocNo.Text, string.Empty))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to another voucher request vat."
                            );
                        return true;
                    }    

                    if (Sm.IsDataExist(
                        "Select DocNo From TblSalesReturnInvoiceHdr " +
                        "Where CancelInd='N' And DocNo=@Param And VoucherRequestPPNCompleteInd='Y';", 
                        Sm.GetGrdStr(Grd2, Row, 4)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to voucher request vat/vat settlement."
                            );
                        return true;
                    }
                }
                if (Sm.GetGrdStr(Grd2, Row, 2) == "3")
                {
                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblPOSVat ");
                    SQL.AppendLine("Where CancelInd='N' ");
                    SQL.AppendLine("And DocNo=@Param1 ");
                    SQL.AppendLine("And (VoucherRequestPPNDocNo Is Not Null And VoucherRequestPPNDocNo<>@Param2);");

                    if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd2, Row, 4), TxtDocNo.Text, string.Empty))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to another voucher request vat."
                            );
                        return true;
                    }    

                    if (Sm.IsDataExist(
                        "Select DocNo From TblPOSVat " +
                        "Where CancelInd='N' And DocNo=@Param And VoucherRequestPPNCompleteInd='Y';", 
                        Sm.GetGrdStr(Grd2, Row, 4)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to voucher request vat/vat settlement."
                            );
                        return true;
                    }
                }

                if (Sm.GetGrdStr(Grd2, Row, 2) == "4")
                {
                    SQL.Length = 0;
                    SQL.Capacity = 0;

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblSalesInvoice5Hdr ");
                    SQL.AppendLine("Where CancelInd='N' ");
                    SQL.AppendLine("And DocNo=@Param1 ");
                    SQL.AppendLine("And (VoucherRequestPPNDocNo Is Not Null And VoucherRequestPPNDocNo<>@Param2);");

                    if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd2, Row, 4), TxtDocNo.Text, string.Empty))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to another voucher request vat."
                            );
                        return true;
                    }

                    if (Sm.IsDataExist(
                        "Select DocNo From TblSalesInvoice5Hdr " +
                        "Where CancelInd='N' And DocNo=@Param And VoucherRequestPPNCompleteInd='Y';",
                        Sm.GetGrdStr(Grd2, Row, 4)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Type : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                            "Document# : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine + Environment.NewLine +
                            "This document already processed to voucher request vat/vat settlement."
                            );
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsDocApprovalSettingNotExisted()
        {
            return !Sm.IsDataExist(
                "Select 1 From TblDocApprovalSetting " +
                "Where UserCode Is Not Null " +
                "And DocType='VoucherRequestPPN' " +
                "Limit 1;");
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.GetLue(LuePaymentType) == "B")
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank name")) return true;
            }
            else if (Sm.GetLue(LuePaymentType) == "G")
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank name")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Bilyet#", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due date ")) return true;
            }
            else if (Sm.GetLue(LuePaymentType) == "K")
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank name")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Cheque#", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due date ")) return true;
            }

            return false;
        }

        private MySqlCommand SaveVoucherRequestPPNHdr(string DocNo, string VoucherRequestDocNo, bool NoNeedApproval)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestPPNHdr ");
            SQL.AppendLine("(DocNo, DocDt, FinalInd, CancelInd, Status, Amt, TaxGrpCode, AcType, DeptCode, DocType, PaymentType, GiroNo, ");
            SQL.AppendLine("BankCode, DueDt, PIC, CurCode, Remark, VoucherRequestDocNo, TaxPeriod, ");
            if (mIsVRVATUseBillingID)
                SQL.AppendLine("BRIBillingID, BankAcCode, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @FinalInd, 'N', 'O', @Amt, @TaxGrpCode, @AcType, @DeptCode, @DocType, @PaymentType, @GiroNo, ");
            SQL.AppendLine("@BankCode, @DueDt, @PIC, @CurCode, @Remark, @VoucherRequestDocNo, @TaxPeriod, ");
            if (mIsVRVATUseBillingID)
                SQL.AppendLine("@BRIBillingID, @BankAcCode, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            if (!NoNeedApproval)
            {
                // harus approve
                if (ChkFinalInd.Checked)
                {
                    SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                    SQL.AppendLine("From TblDocApprovalSetting T ");
                    SQL.AppendLine("Where T.DocType='VoucherRequestPPN' ");

                    if (mIsVoucherRequestPPNApprovalBasedOnDept)
                        SQL.AppendLine("And T.DeptCode = (Select DeptCode From TblVoucherRequestPPNHdr Where DocNo = @DocNo) ");

                    SQL.AppendLine("And (T.StartAmt=0 ");
                    SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                    SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
                    SQL.AppendLine("    From TblVoucherRequestHdr A ");
                    SQL.AppendLine("    Left Join ( ");
                    SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
                    SQL.AppendLine("        From TblCurrencyRate B1 ");
                    SQL.AppendLine("        Inner Join ( ");
                    SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
                    SQL.AppendLine("            From TblCurrencyRate ");
                    SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
                    SQL.AppendLine("            Group By CurCode1 ");
                    SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
                    SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");
                    SQL.AppendLine("), 0)); ");

                    SQL.AppendLine("Update TblVoucherRequestPPNHdr Set Status='A' ");
                    SQL.AppendLine("Where DocNo=@DocNo ");
                    SQL.AppendLine("And Not Exists( ");
                    SQL.AppendLine("    Select DocNo From TblDocApproval ");
                    SQL.AppendLine("    Where DocType='VoucherRequestPPN' ");
                    SQL.AppendLine("    And DocNo=@DocNo ");
                    SQL.AppendLine("    ); ");
                }
            }
            else
            { 
                //tidak harus approve
                if (ChkFinalInd.Checked)
                {
                    SQL.AppendLine("Update TblVoucherRequestPPNHdr Set Status='A' ");
                    SQL.AppendLine("Where DocNo=@DocNo ");
                    SQL.AppendLine("And Not Exists( ");
                    SQL.AppendLine("    Select DocNo From TblDocApproval ");
                    SQL.AppendLine("    Where DocType='VoucherRequestPPN' ");
                    SQL.AppendLine("    And DocNo=@DocNo ");
                    SQL.AppendLine("    ); ");
                }
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FinalInd", ChkFinalInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<Decimal>(ref cm, "@TaxPeriod", decimal.Parse(TxtTaxPeriod.Text));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", NoNeedApproval ? "A" : "O");
            Sm.CmParam<String>(ref cm, "@TaxGrpCode", Sm.GetLue(LueTaxGrpCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@AcType", mAcType);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@BRIBillingID", TxtBillingID.Text);
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestPPNDtl(string DocNo, int r)
        {
            var SQL = new StringBuilder();
            var DocType = Sm.GetGrdStr(Grd3, r, 2);

            SQL.AppendLine("Insert Into TblVoucherRequestPPNDtl ");
            SQL.AppendLine("(DocNo, DNo, DocType, DocNoIn, Amt, ExcRate, TaxInvoiceNo, TaxInvoiceDt, TaxCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @DocType, @DocNoIn, @Amt, @ExcRate, @TaxInvoiceNo, @TaxInvoiceDt, @TaxCode, @Remark, @CreateBy, CurrentDateTime()); ");

            if (DocType == "1")
            {
                SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
                SQL.AppendLine("    VoucherRequestPPNDocNo=@DocNo, ");
                SQL.AppendLine("    VoucherRequestPPNCompleteInd=@CompleteInd ");
                SQL.AppendLine("Where DocNo=@DocNoIn ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And IfNull(TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("And VoucherRequestPPNCompleteInd='N'; ");

                SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
                SQL.AppendLine("    VoucherRequestPPNDocNo2=@DocNo, ");
                SQL.AppendLine("    VoucherRequestPPNCompleteInd2=@CompleteInd ");
                SQL.AppendLine("Where DocNo=@DocNoIn ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And IfNull(TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("And VoucherRequestPPNCompleteInd2='N'; ");

                SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
                SQL.AppendLine("    VoucherRequestPPNDocNo3=@DocNo, ");
                SQL.AppendLine("    VoucherRequestPPNCompleteInd3=@CompleteInd ");
                SQL.AppendLine("Where DocNo=@DocNoIn ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And IfNull(TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("And VoucherRequestPPNCompleteInd3='N'; ");
            }

            if (DocType == "2")
            {
                SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
                SQL.AppendLine("    VoucherRequestPPNDocNo=@DocNo, ");
                SQL.AppendLine("    VoucherRequestPPNCompleteInd=@CompleteInd ");
                SQL.AppendLine("Where DocNo=@DocNoIn ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And IfNull(TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("And VoucherRequestPPNCompleteInd='N'; ");

                SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
                SQL.AppendLine("    VoucherRequestPPNDocNo2=@DocNo, ");
                SQL.AppendLine("    VoucherRequestPPNCompleteInd2=@CompleteInd ");
                SQL.AppendLine("Where DocNo=@DocNoIn ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And IfNull(TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("And VoucherRequestPPNCompleteInd2='N'; ");

                SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
                SQL.AppendLine("    VoucherRequestPPNDocNo3=@DocNo, ");
                SQL.AppendLine("    VoucherRequestPPNCompleteInd3=@CompleteInd ");
                SQL.AppendLine("Where DocNo=@DocNoIn ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And IfNull(TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("And VoucherRequestPPNCompleteInd3='N'; ");
            }

            if (DocType == "3")
            {
                SQL.AppendLine("Update TblVoucherRequestTax Set ");
                SQL.AppendLine("    VoucherRequestPPNDocNo=@DocNo, ");
                SQL.AppendLine("    VoucherRequestPPNCompleteInd=@CompleteInd ");
                SQL.AppendLine("Where DocNo=@DocNoIn ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And IfNull(TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                SQL.AppendLine("And VoucherRequestPPNCompleteInd='N'; ");
            }

            if (DocType == "4")
            {
                SQL.AppendLine("Update TblPurchaseInvoiceRawMaterialHdr Set ");
                SQL.AppendLine("    VoucherRequestPPNDocNo=@DocNo, ");
                SQL.AppendLine("    VoucherRequestPPNCompleteInd=@CompleteInd ");
                SQL.AppendLine("Where DocNo=@DocNoIn ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And TaxInd='Y' ");
                SQL.AppendLine("And VoucherRequestPPNCompleteInd='N'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (r + 1).ToString(), 4));
            Sm.CmParam<String>(ref cm, "@TaxGrpCode", Sm.GetLue(LueTaxGrpCode));
            Sm.CmParam<String>(ref cm, "@CompleteInd", ChkFinalInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetGrdStr(Grd3, r, 2));
            Sm.CmParam<String>(ref cm, "@DocNoIn", Sm.GetGrdStr(Grd3, r, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, r, 6));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", Sm.GetGrdStr(Grd3, r, 7));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetGrdDate(Grd3, r, 8));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, r, 9));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", Sm.GetGrdDec(Grd3, r, 10));
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetGrdStr(Grd3, r, 14));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestPPNDtl2(string DocNo, int r)
        {
            var SQL = new StringBuilder();
            var DocType = Sm.GetGrdStr(Grd2, r, 2);

            SQL.AppendLine("INSERT INTO TblVoucherRequestPPNDtl2 ");
            SQL.AppendLine("(DocNo, DNo, DocType, DocNoOut, ExcRate, Amt, TaxInvoiceNo, TaxInvoiceDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("VALUES ");
            SQL.AppendLine("(@DocNo, @DNo, @DocType, @DocNoOut, @ExcRate, @Amt, @TaxInvoiceNo, @TaxInvoiceDt, @Remark, @CreateBy, CurrentDateTime()); ");

            if (DocType == "1")
            {
                SQL.AppendLine("Update TblSalesInvoiceHdr Set ");
                SQL.AppendLine("    VoucherRequestPPNDocNo=@DocNo, ");
                SQL.AppendLine("    VoucherRequestPPNCompleteInd=@CompleteInd ");
                SQL.AppendLine("Where DocNo=@DocNoOut ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And VoucherRequestPPNCompleteInd='N'; ");
            }

            if (DocType == "2")
            {
                SQL.AppendLine("Update TblSalesReturnInvoiceHdr Set ");
                SQL.AppendLine("    VoucherRequestPPNDocNo=@DocNo, ");
                SQL.AppendLine("    VoucherRequestPPNCompleteInd=@CompleteInd ");
                SQL.AppendLine("Where DocNo=@DocNoOut ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VoucherRequestPPNCompleteInd='N'; ");
            }

            if (DocType == "3")
            {
                SQL.AppendLine("Update TblPOSVAT Set ");
                SQL.AppendLine("    VoucherRequestPPNDocNo=@DocNo, ");
                SQL.AppendLine("    VoucherRequestPPNCompleteInd=@CompleteInd ");
                SQL.AppendLine("Where DocNo=@DocNoOut ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VoucherRequestPPNCompleteInd='N'; ");
            }

            if (DocType == "4")
            {
                SQL.AppendLine("Update TblSalesInvoice5Hdr Set ");
                SQL.AppendLine("    VoucherRequestPPNDocNo=@DocNo, ");
                SQL.AppendLine("    VoucherRequestPPNCompleteInd=@CompleteInd ");
                SQL.AppendLine("Where DocNo=@DocNoOut ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And VoucherRequestPPNCompleteInd='N'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (r + 1).ToString(), 4));
            Sm.CmParam<String>(ref cm, "@CompleteInd", ChkFinalInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetGrdStr(Grd2, r, 2));
            Sm.CmParam<String>(ref cm, "@DocNoOut", Sm.GetGrdStr(Grd2, r, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, r, 6));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", Sm.GetGrdStr(Grd2, r, 7));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetGrdDate(Grd2, r, 8));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, r, 9));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", Sm.GetGrdDec(Grd2, r, 10));
            Sm.CmParam<String>(ref cm, "@TaxGrpCode", Sm.GetLue(LueTaxGrpCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string VoucherRequestPPNDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
            SQL.AppendLine("DocType, AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            SQL.AppendLine("PIC, DocEnclosure, CurCode, Amt, VoucherDocNo, ");
            SQL.AppendLine("PaymentUser, PaidToBankCode, PaidToBankBranch, PaidToBankAcNo, PaidToBankAcName, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', @DeptCode, ");
            SQL.AppendLine("@DocType, @AcType, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("@PIC, 0, @CurCode, @Amt, NULL, ");
            SQL.AppendLine("NULL, NULL, NULL, NULL, NULL, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequestPPN' ");
            SQL.AppendLine("    And DocNo=@VoucherRequestPPNDocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblVoucherRequestPPNHdr Set VoucherRequestDocNo=@DocNo ");
            SQL.AppendLine("Where DocNo=@VoucherRequestPPNDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestPPNDocNo", VoucherRequestPPNDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@AcType", mAcType);
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @Description, @Amt, NULL, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Description", "Voucher Request VAT - "+MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdatePOSVAT(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPOSVat A ");
            SQL.AppendLine("Inner Join TblVoucherRequestPPNDtl2 B On A.DocNo = B.DocNoOut And B.DocType = '3' And B.DocNo = @DocNo ");
            SQL.AppendLine("Set A.TaxInvoiceNo = B.TaxInvoiceNo, A.LastUpBy = @CreateBy, A.LastUpDt = CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            bool NoNeedApproval = IsDocApprovalSettingNotExisted();

            if (ChkCancelInd.Checked)
                cml.Add(EditVoucherRequestPPNHdr());
            else
            {
                cml.Add(DeleteVoucherRequestPPNDtl());

                if (Grd3.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0) cml.Add(SaveVoucherRequestPPNDtl(TxtDocNo.Text, Row));
                }

                if (Grd2.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd2, Row, 4).Length > 0) cml.Add(SaveVoucherRequestPPNDtl2(TxtDocNo.Text, Row));
                }

                if (ChkFinalInd.Checked)
                {
                    string VoucherRequestDocNo = GenerateVoucherRequestDocNo();

                    cml.Add(EditVoucherRequestPPNHdr2(NoNeedApproval));
                    cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo, TxtDocNo.Text));
                    cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo));
                }
                else
                {
                    cml.Add(EditVoucherRequestPPNHdr3());
                }

                cml.Add(UpdatePOSVAT(TxtDocNo.Text));
            }

         
            Sm.ExecCommands(cml);
                    
            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||

                (mIsVRVATTransactionValidatedbyClosingJournal && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode())) ||

                (!ChkCancelInd.Checked && IsDocAlreadyFinal()) ||
                IsDocAlreadyCancelled() ||
                IsGrdEmpty() ||
                (!ChkCancelInd.Checked && IsGrdValueNotValid()) ||
                (!ChkCancelInd.Checked && IsFinalDataInBiggerThanOut()) ||
                IsDocAlreadyProceedToVoucher() ;
        }

        private MySqlCommand EditVoucherRequestPPNHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestPPNHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, ");
            SQL.AppendLine("    CancelReason=@CancelReason, ");
            SQL.AppendLine("    Amt=@Amt, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status In ('O', 'A'); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, ");
            SQL.AppendLine("    CancelReason=@CancelReason, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status In ('O', 'A'); ");

            SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo, '')=@DocNo; ");

            SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo2=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd2='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo2, '')=@DocNo; ");

            SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo3=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd3='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo3, '')=@DocNo; ");

            SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo, '')=@DocNo; ");

            SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo2=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd2='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo2, '')=@DocNo; ");

            SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo3=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd3='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo3, '')=@DocNo; ");

            SQL.AppendLine("Update TblVoucherRequestTax Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo, '')=@DocNo; ");

            SQL.AppendLine("Update TblPurchaseInvoiceRawMaterialHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where VoucherRequestPPNDocNo Is Not Null ");
            SQL.AppendLine("And VoucherRequestPPNDocNo=@DocNo; ");


            SQL.AppendLine("Update TblSalesInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo, '')=@DocNo; ");

            SQL.AppendLine("Update TblSalesReturnInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo, '')=@DocNo; ");

            SQL.AppendLine("Update TblPOSVAT Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo, '')=@DocNo; ");

            SQL.AppendLine("Update TblSalesInvoice5Hdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo, '')=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditVoucherRequestPPNHdr2(bool NoNeedApproval)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestPPNHdr Set ");
            SQL.AppendLine("    FinalInd='Y', ");
            SQL.AppendLine("    Amt=@Amt, ");
            SQL.AppendLine("    TaxPeriod=@TaxPeriod, ");
            SQL.AppendLine("    Remark=@Remark, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            if (!NoNeedApproval)
            {
                // harus approve
                if (ChkFinalInd.Checked)
                {
                    SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                    SQL.AppendLine("From TblDocApprovalSetting T ");
                    SQL.AppendLine("Where T.DocType='VoucherRequestPPN' ");

                    if (mIsVoucherRequestPPNApprovalBasedOnDept)
                        SQL.AppendLine("And T.DeptCode = (Select DeptCode From TblVoucherRequestPPNHdr Where DocNo = @DocNo) ");

                    SQL.AppendLine("And (T.StartAmt=0 ");
                    SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                    SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
                    SQL.AppendLine("    From TblVoucherRequestHdr A ");
                    SQL.AppendLine("    Left Join ( ");
                    SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
                    SQL.AppendLine("        From TblCurrencyRate B1 ");
                    SQL.AppendLine("        Inner Join ( ");
                    SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
                    SQL.AppendLine("            From TblCurrencyRate ");
                    SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
                    SQL.AppendLine("            Group By CurCode1 ");
                    SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
                    SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");
                    SQL.AppendLine("), 0)); ");

                    SQL.AppendLine("Update TblVoucherRequestPPNHdr Set Status='A' ");
                    SQL.AppendLine("Where DocNo=@DocNo ");
                    SQL.AppendLine("And Not Exists( ");
                    SQL.AppendLine("    Select DocNo From TblDocApproval ");
                    SQL.AppendLine("    Where DocType='VoucherRequestPPN' ");
                    SQL.AppendLine("    And DocNo=@DocNo ");
                    SQL.AppendLine("    ); ");
                }
            }
            else
            {
                //tidak harus approve
                if (ChkFinalInd.Checked)
                {
                    SQL.AppendLine("Update TblVoucherRequestPPNHdr Set Status='A' ");
                    SQL.AppendLine("Where DocNo=@DocNo ");
                    SQL.AppendLine("And Not Exists( ");
                    SQL.AppendLine("    Select DocNo From TblDocApproval ");
                    SQL.AppendLine("    Where DocType='VoucherRequestPPN' ");
                    SQL.AppendLine("    And DocNo=@DocNo ");
                    SQL.AppendLine("    ); ");
                }
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxPeriod", decimal.Parse(TxtTaxPeriod.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditVoucherRequestPPNHdr3()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestPPNHdr Set ");
            SQL.AppendLine("    Amt=@Amt, ");
            SQL.AppendLine("    TaxPeriod=@TaxPeriod, ");
            SQL.AppendLine("    Remark=@Remark, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxPeriod", decimal.Parse(TxtTaxPeriod.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeleteVoucherRequestPPNDtl()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblVoucherRequestPPNDtl Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblVoucherRequestPPNDtl2 Where DocNo=@DocNo; ");

            SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo, '')=@DocNo; ");
                
            SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo2=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd2='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo2, '')=@DocNo; ");

            SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo3=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd3='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo3, '')=@DocNo; ");

            SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo, '')=@DocNo; ");
                
            SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo2=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd2='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo2, '')=@DocNo; ");

            SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo3=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd3='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo3, '')=@DocNo; ");

            SQL.AppendLine("Update TblVoucherRequestTax Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo, '')=@DocNo; ");

            SQL.AppendLine("Update TblPurchaseInvoiceRawMaterialHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where VoucherRequestPPNDocNo Is Not Null ");
            SQL.AppendLine("And VoucherRequestPPNDocNo=@DocNo; ");

            SQL.AppendLine("Update TblSalesInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo, '')=@DocNo; ");

            SQL.AppendLine("Update TblSalesReturnInvoiceHdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo, '')=@DocNo; ");

            SQL.AppendLine("Update TblPOSVAT Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo, '')=@DocNo; ");

            SQL.AppendLine("Update TblSalesInvoice5Hdr Set ");
            SQL.AppendLine("    VoucherRequestPPNDocNo=Null, ");
            SQL.AppendLine("    VoucherRequestPPNCompleteInd='N' ");
            SQL.AppendLine("Where IfNull(VoucherRequestPPNDocNo, '')=@DocNo; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            return cm;
        }

        private MySqlCommand EmptyVRPPNIn(int Row)
        {
            var SQLX = new StringBuilder();

            if (Sm.GetGrdStr(Grd3, Row, 2) == "1")
            {
                SQLX.AppendLine("UPDATE TblPurchaseInvoiceHdr SET ");
                SQLX.AppendLine("  VoucherRequestPPNDocNo = NULL ");
                SQLX.AppendLine("WHERE DocNo = @DocNo AND CancelInd = 'N' AND VoucherRequestPPNDocNo IS NOT NULL; ");
            }

            if (Sm.GetGrdStr(Grd3, Row, 2) == "2")
            {
                SQLX.AppendLine("UPDATE TblPurchaseReturnInvoiceHdr SET ");
                SQLX.AppendLine("  VoucherRequestPPNDocNo = NULL ");
                SQLX.AppendLine("WHERE DocNo = @DocNo AND CancelInd = 'N' AND VoucherRequestPPNDocNo IS NOT NULL; ");
            }

            if (Sm.GetGrdStr(Grd3, Row, 2) == "3")
            {
                SQLX.AppendLine("Update TblVoucherRequestTaxHdr Set ");
                SQLX.AppendLine("  VoucherRequestPPNDocNo=Null ");
                SQLX.AppendLine("Where DocNo=@DocNo And CancelInd='N' And VoucherRequestPPNDocNo Is Not Null; ");
            }

            if (Sm.GetGrdStr(Grd3, Row, 2) == "4")
            {
                SQLX.AppendLine("Update TblPurchaseInvoiceRawMaterialHdr Set ");
                SQLX.AppendLine("   VoucherRequestPPNDocNo=Null ");
                SQLX.AppendLine("Where DocNo=@DocNo And CancelInd='N' And TaxInd='Y' And VoucherRequestPPNDocNo Is Not Null; ");
            }

            var cm = new MySqlCommand() { CommandText = SQLX.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd3, Row, 4));
            
            return cm;
        }

        private MySqlCommand EmptyVRPPNOut(int Row)
        {
            var SQLY = new StringBuilder();

            if (Sm.GetGrdStr(Grd2, Row, 2) == "1")
            {
                SQLY.AppendLine("UPDATE TblSalesInvoiceHdr SET ");
                SQLY.AppendLine("  VoucherRequestPPNDocNo = NULL ");
                SQLY.AppendLine("WHERE DocNo = @DocNo AND CancelInd = 'N' AND VoucherRequestPPNDocNo IS NOT NULL; ");
            }

            if (Sm.GetGrdStr(Grd2, Row, 2) == "2")
            {
                SQLY.AppendLine("UPDATE TblSalesReturnInvoiceHdr SET ");
                SQLY.AppendLine("  VoucherRequestPPNDocNo = NULL ");
                SQLY.AppendLine("WHERE DocNo = @DocNo AND CancelInd = 'N' AND VoucherRequestPPNDocNo IS NOT NULL; ");
            }

            if (Sm.GetGrdStr(Grd2, Row, 2) == "3")
            {
                SQLY.AppendLine("UPDATE TblPOSVAT SET ");
                SQLY.AppendLine("  VoucherRequestPPNDocNo = NULL ");
                SQLY.AppendLine("WHERE DocNo = @DocNo AND CancelInd = 'N' AND VoucherRequestPPNDocNo IS NOT NULL; ");
            }

            if (Sm.GetGrdStr(Grd2, Row, 2) == "4")
            {
                SQLY.AppendLine("UPDATE TblSalesInvoice5Hdr SET ");
                SQLY.AppendLine("  VoucherRequestPPNDocNo = NULL ");
                SQLY.AppendLine("WHERE DocNo = @DocNo AND CancelInd = 'N' AND VoucherRequestPPNDocNo IS NOT NULL; ");
            }
            
            var cm = new MySqlCommand() { CommandText = SQLY.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd2, Row, 4));

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                string DocType = Sm.GetLue(LueDocType);
                ClearData();
                ShowVoucherRequestPPNHdr(DocNo);
                ShowVoucherRequestPPNDtl(DocNo);
                ShowVoucherRequestPPNDtl2(DocNo);
                Sm.ShowDocApproval(DocNo, "VoucherRequestPPN", ref Grd4, mIsTransactionUseDetailApprovalInformation);
                ComputeAmt();
                string Profitcentertest = GetProfitCenterCode();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowVoucherRequestPPNHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.Amt, A.CancelInd, A.CancelReason, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' Else 'Cancelled' End As Status, ");
            SQL.AppendLine("A.DeptCode, A.DocType, A.PaymentType, A.GiroNo, A.BankCode, A.DueDt, A.PIC, A.CurCode, A.Remark, ");
            SQL.AppendLine("A.VoucherRequestDocNo, B.VoucherDocNo, A.FinalInd, A.TaxGrpCode, A.AcType, A.TaxPeriod, ");
            if (mIsVRVATUseBillingID)
                SQL.AppendLine("A.BRIBillingID, A.BankAcCode ");
            else
                SQL.AppendLine("Null As BRIBillingID, Null As BankAcCode ");
            SQL.AppendLine("FROM TblVoucherRequestPPNHdr A ");
            SQL.AppendLine("LEFT JOIN TblVoucherRequestHdr B ON A.VoucherRequestDocNo = B.DocNo ");
            SQL.AppendLine("WHERE A.DocNo=@DocNo;");
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "Amt", "CancelInd", "CancelReason", "DeptCode",

                    //6-10
                    "DocType", "PaymentType", "GiroNo", "BankCode", "DueDt",

                    //11-15
                    "PIC", "CurCode", "Remark", "VoucherRequestDocNo", "VoucherDocNo",

                    //16-20
                    "Status", "FinalInd", "TaxGrpCode", "AcType", "TaxPeriod",

                    //21-22
                    "BRIBillingID", "BankAcCode"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     mSaveState = true;
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[4]);
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                     ChkCancelInd.Checked = Sm.DrStr(dr, c[3])=="Y";
                     SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[5]));
                     Sm.SetLue(LueDocType, Sm.DrStr(dr, c[6]));
                     Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[7]));
                     TxtGiroNo.EditValue = Sm.DrStr(dr, c[8]);
                     Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[9]));
                     Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[10]));
                     Sl.SetLueUserCode(ref LuePIC, Sm.DrStr(dr, c[11]));
                     Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[12]));
                     MeeRemark.EditValue = Sm.DrStr(dr, c[13]);
                     TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[14]);
                     TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[15]);
                     TxtStatus.EditValue = Sm.DrStr(dr, c[16]);
                     ChkFinalInd.Checked = Sm.DrStr(dr, c[17])=="Y";
                     Sm.SetLue(LueTaxGrpCode, Sm.DrStr(dr, c[18]));
                     mAcType = Sm.DrStr(dr, c[19]);
                     TxtTaxPeriod.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[20]), 11);
                     TxtBillingID.EditValue = Sm.DrStr(dr, c[21]);
                     SetLueBankAcCode(ref LueBankAcCode, Sm.DrStr(dr, c[22]));
                 }, true
             );
        }

        private void ShowVoucherRequestPPNDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.DocType, ");
            SQL.AppendLine("Case A.DocType ");
            SQL.AppendLine("    When '1' Then 'Purchase Invoice' ");
            SQL.AppendLine("    When '2' Then 'Purchase Return Invoice' ");
            SQL.AppendLine("    When '3' Then 'Voucher Request Tax' ");
            SQL.AppendLine("    When '4' Then 'Purchase Invoice Raw Material' ");
            SQL.AppendLine("End As DocTypeDesc, ");
            SQL.AppendLine("A.DocNoIn, ");
            SQL.AppendLine("Case A.DocType ");
            SQL.AppendLine("    When '1' Then B.DocDt ");
            SQL.AppendLine("    When '2' Then C.DocDt ");
            SQL.AppendLine("    When '3' Then D.DocDt ");
            SQL.AppendLine("    When '4' Then E.DocDt ");
            SQL.AppendLine("End As DocDt, ");
            SQL.AppendLine("A.Amt, A.ExcRate As ExcRate, ");
            SQL.AppendLine("(A.Amt*IfNull(A.ExcRate, 0.00)) As Amt2, ");
            SQL.AppendLine("A.TaxInvoiceNo, A.TaxInvoiceDt, A.Remark, ");
            SQL.AppendLine("Case A.DocType ");
            SQL.AppendLine("    When '1' Then B.CurCode ");
            SQL.AppendLine("    When '2' Then C.CurCode ");
            SQL.AppendLine("    When '3' Then D.CurCode ");
            SQL.AppendLine("    When '4' Then E.CurCode ");
            SQL.AppendLine("End As CurCode, A.TaxCode, F.TaxName, F.TaxRate ");
            SQL.AppendLine("From TblVoucherRequestPPNDtl A ");
            SQL.AppendLine("Left Join TblPurchaseInvoiceHdr B On A.DocNoIn=B.DocNo ");
            SQL.AppendLine("Left Join TblPurchaseReturnInvoiceHdr C On A.DocNoIn=C.DocNo ");
            SQL.AppendLine("Left Join TblVoucherRequestTax D On A.DocNoIn=D.DocNo ");
            SQL.AppendLine("Left Join TblPurchaseInvoiceRawMaterialHdr E On A.DocNoIn=E.DocNo ");
            SQL.AppendLine("Left Join TblTax F On A.TaxCode = F.TaxCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",
                        
                        //1-5
                        "DocType", "DocTypeDesc", "DocNoIn", "DocDt", "Remark", 
                        
                        //6-10
                        "TaxInvoiceNo", "TaxInvoiceDt", "Amt", "ExcRate", "Amt2",

                        //11
                        "CurCode", "TaxCode", "TaxName", "TaxRate"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 9, 10, 11, 17 });
            SetNo(ref Grd3);
            Sm.FocusGrd(Grd3, 0, 2);
        }

        private void ShowVoucherRequestPPNDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.DocType, ");
            SQL.AppendLine("Case A.DocType ");
            SQL.AppendLine("    When '1' Then 'Sales Invoice' ");
            SQL.AppendLine("    When '2' Then 'Sales Return Invoice' ");
            SQL.AppendLine("    When '3' Then 'Point of Sale' ");
            SQL.AppendLine("    When '4' Then 'Sales Invoice For Project' ");
            SQL.AppendLine("End As DocTypeDesc, ");
            SQL.AppendLine("A.DocNoOut, ");
            SQL.AppendLine("Case A.DocType ");
            SQL.AppendLine("    When '1' Then B.DocDt ");
            SQL.AppendLine("    When '2' Then C.DocDt ");
            SQL.AppendLine("    When '3' Then D.DocDt ");
            SQL.AppendLine("    When '4' Then E.DocDt ");
            SQL.AppendLine("End As DocDt, ");
            SQL.AppendLine("A.Remark, A.TaxInvoiceNo, A.TaxInvoiceDt, ");
            SQL.AppendLine("A.Amt, A.ExcRate, A.Amt*A.ExcRate As Amt2, ");
            SQL.AppendLine("Case A.DocType ");
            SQL.AppendLine("    When '1' Then B.CurCode ");
            SQL.AppendLine("    When '2' Then C.CurCode ");
            SQL.AppendLine("    When '3' Then D.CurCode ");
            SQL.AppendLine("    When '4' Then E.CurCode ");
            SQL.AppendLine("End As CurCode ");
            SQL.AppendLine("From TblVoucherRequestPPNDtl2 A ");
            SQL.AppendLine("Left Join TblSalesInvoiceHdr B On A.DocNoOut=B.DocNo ");
            SQL.AppendLine("Left Join TblSalesReturnInvoiceHdr C On A.DocNoOut=C.DocNo ");
            SQL.AppendLine("Left Join TblPOSVat D On A.DocNoOut=D.DocNo ");
            SQL.AppendLine("Left Join TblSalesInvoice5Hdr E On A.DocNoOut=E.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",
                        
                        //1-5
                        "DocType", "DocTypeDesc", "DocNoOut", "DocDt", "Remark", 
                        
                        //6-10
                        "TaxInvoiceNo", "TaxInvoiceDt", "Amt", "ExcRate", "Amt2",

                        //11
                        "CurCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 9, 10, 11 });
            SetNo(ref Grd2);
            Sm.FocusGrd(Grd2, 0, 2);
        }

        private void ShowDocApproval(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQLAppr = new StringBuilder();

            SQLAppr.AppendLine("Select A.ApprovalDNo, B.UserName, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQLAppr.AppendLine("Case When A.LastUpDt Is Not Null Then ");
            SQLAppr.AppendLine("A.LastUpDt Else Null End As LastUpDt, A.Remark ");
            SQLAppr.AppendLine("From TblDocApproval A ");
            SQLAppr.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQLAppr.AppendLine("Where A.DocType='VoucherRequestPPN' ");
            SQLAppr.AppendLine("And Status In ('A', 'C') ");
            SQLAppr.AppendLine("And A.DocNo=@DocNo ");
            SQLAppr.AppendLine("Order By A.ApprovalDNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd4, ref cm, SQLAppr.ToString(),
                    new string[] 
                    { 
                        "ApprovalDNo",
                        "UserName","StatusDesc","LastUpDt", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("D", Grd4, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 0);
        }

        #endregion

        #region Additional Method

        internal void SetNo(ref iGrid Grd)
        {
            for (int r = 0; r < Grd.Rows.Count; r++)
                Grd.Cells[r, 13].Value = r + 1;
        }

        internal void GetPPH23Data(ref List<PPH23> l, string DocNo)
        {
            if (Grd3.Rows.Count > 0)
            {
                if (DocNo.Length <= 0)
                {
                    string mPIDocNo = string.Empty, mPRIDocNo = string.Empty;

                    for (int i = 0; i < Grd3.Rows.Count; i++)
                    {
                        if (Sm.GetGrdStr(Grd3, i, 2) == "1" || Sm.GetGrdStr(Grd3, i, 2) == "3") // DocType == "1"
                        {
                            if (mPIDocNo.Length > 0) mPIDocNo += ",";
                            mPIDocNo += Sm.GetGrdStr(Grd3, i, 4);
                        }

                        if (Sm.GetGrdStr(Grd3, i, 2) == "2") //DocType == "2"
                        {
                            if (mPRIDocNo.Length > 0) mPRIDocNo += ",";
                            mPRIDocNo += Sm.GetGrdStr(Grd3, i, 4);
                        }
                    }

                    if (mPIDocNo.Length > 0) GetPIData(ref l, mPIDocNo);
                    if (mPRIDocNo.Length > 0) GetPRIData(ref l, mPRIDocNo);
                }
                else
                {
                    GetPIData(ref l, DocNo);
                    GetPRIData(ref l, DocNo);
                }

                if (l.Count > 0)
                {
                    GetTaxInvoiceAndServiceValue(ref l);
                    ProcessPPHAmt(ref l);
                }
            }
        }

        internal void ProcessPrintPPH23(string DocNo)
        {
            if(Grd3.Rows.Count > 0)
            {
                var l = new List<PPH23>();
                GetPPH23Data(ref l, DocNo);
                if (l.Count > 0)
                {
                    l.ForEach(i => { PrintPPH23(i); });
                }
                l.Clear();
            }
        }

        internal void PrintPPH23(PPH23 l)
        {
            var l1 = new List<PPH23_1>();
            var l2 = new List<Employee>();
            string[] TableName = { "PPH23", "Employee" };
            List<IList> myLists = new List<IList>();

            #region PPH23

            l1.Add(new PPH23_1()
            {
                Vendor = l.Vendor,
                VdAddress = l.VdAddress,
                TIN = l.TIN,
                TaxInvoice = l.TaxInvoice,
                TaxInvoiceDt = l.TaxInvoiceDt,
                PPhAmt = l.PPhAmt,
                TotalTax = l.TotalTax,
                Terbilang = l.Terbilang,
                PrintBy = l.PrintBy,
                PrintPPH23Date = l.PrintPPH23Date,
                ServiceCode = l.ServiceCode,
                ServiceName = l.ServiceName,
                ServiceNote = l.ServiceNote
            });

            #endregion

            #region Pemotong Pajak
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select A.EmpCode, A.EmpName, ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle6') As 'NPWP' ");
            SQL2.AppendLine("From TblEmployee A ");
            SQL2.AppendLine("Where A.EmpCode=@EmpCode; ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@EmpCode", mEmptaxwithholder);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                    {
                     //0-3
                     "EmpCode",
                     "EmpName",
                     "CompanyName",
                     "NPWP"
                    
                    });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr2, c2[0]),
                            EmpName = Sm.DrStr(dr2, c2[1]),
                            CompanyName = Sm.DrStr(dr2, c2[2]),
                            NPWP = Sm.DrStr(dr2, c2[3]),
                        });
                    }
                }
                dr2.Close();
            }

            #endregion

            myLists.Add(l1);
            myLists.Add(l2);

            Sm.PrintReport("PPH23VAT", myLists, TableName, false);

            l1.Clear();
            l2.Clear();
        }

        private void ProcessCSVPPH23()
        {
            var l = new List<PPH23>();
            GetPPH23Data(ref l, string.Empty);
            if(l.Count > 0)
                GenerateCSVPPH23(ref l);
            l.Clear();
        }

        private void GenerateCSVPPH23(ref List<PPH23> l)
        {
            var FileName = "PPH23";
            var sf = new SaveFileDialog();
            sf.Filter = "CSV files (*.csv)|*.csv";
            sf.FileName = FileName;

            if (sf.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(sf.FileName, false, Encoding.GetEncoding(1252));

                sw.Write(
                    /*1*/  "Kode Form Bukti Potong;Masa Pajak;Tahun Pajak;Pembetulan;NPWP WP yang Dipotong;Nama WP yang Dipotong;Alamat WP yang Dipotong;" +
                    /*2*/  "Nomor Bukti Potong;Tanggal Bukti Potong;Nilai Bruto 1;Tarif 1;PPh Yang Dipotong  1;Nilai Bruto 2;" +
                    /*3*/  "Tarif 2;PPh Yang Dipotong  2;Nilai Bruto 3;Tarif 3;PPh Yang Dipotong  3;Nilai Bruto 4;Tarif 4;" +
                    /*4*/  "PPh Yang Dipotong  4;Nilai Bruto 5;Tarif 5;PPh Yang Dipotong  5;Nilai Bruto 6a/Nilai Bruto 6;" +
                    /*5*/  "Tarif 6a/Tarif 6;PPh Yang Dipotong  6a/PPh Yang Dipotong  6;Nilai Bruto 6b/Nilai Bruto 7;" +
                    /*6*/  "Tarif 6b/Tarif 7;PPh Yang Dipotong  6b/PPh Yang Dipotong  7;Nilai Bruto 6c/Nilai Bruto 8;" +
                    /*7*/  "Tarif 6c/Tarif 8;PPh Yang Dipotong  6c/PPh Yang Dipotong  8;Nilai Bruto 9;Tarif 9;" +
                    /*8*/  "PPh Yang Dipotong  9;Nilai Bruto 10;Perkiraan Penghasilan Netto10;Tarif 10;PPh Yang Dipotong  10;" +
                    /*9*/  "Nilai Bruto 11;Perkiraan Penghasilan Netto11;Tarif 11;PPh Yang Dipotong  11;Nilai Bruto 12;" +
                    /*10*/ "Perkiraan Penghasilan Netto12;Tarif 12;PPh Yang Dipotong  12;Nilai Bruto 13;Tarif 13;" +
                    /*11*/ "PPh Yang Dipotong  13;Kode Jasa 6d1 PMK-244/PMK.03/2008;" +
                    /*12*/ "Nilai Bruto 6d1;Tarif 6d1;PPh Yang Dipotong  6d1;" +
                    /*13*/ "Kode Jasa 6d2 PMK-244/PMK.03/2008;Nilai Bruto 6d2;Tarif 6d2;PPh Yang Dipotong  6d2;" +
                    /*14*/ "Kode Jasa 6d3 PMK-244/PMK.03/2008;Nilai Bruto 6d3;Tarif 6d3;PPh Yang Dipotong  6d3;" +
                    /*15*/ "Kode Jasa 6d4 PMK-244/PMK.03/2008;Nilai Bruto 6d4;Tarif 6d4;PPh Yang Dipotong  6d4;" +
                    /*16*/ "Kode Jasa 6d5 PMK-244/PMK.03/2008;Nilai Bruto 6d5;Tarif 6d5;PPh Yang Dipotong  6d5;" +
                    /*17*/ "Kode Jasa 6d6 PMK-244/PMK.03/2008;Nilai Bruto 6d6;Tarif 6d6;PPh Yang Dipotong  6d6;" +
                    /*18*/ "Jumlah Nilai Bruto ;Jumlah PPh Yang Dipotong"
                        );

                foreach (var x in l)
                {
                    sw.Write(sw.NewLine);
                    sw.Write(
                        /*1*/  "F113306" + ";" + TxtTaxPeriod.Text + ";" + Sm.Right(x.TaxInvoiceDt, 4) + ";" + "0;" + (x.TIN.Replace("-",string.Empty)).Replace(".", string.Empty) + ";" + x.Vendor + ";" + x.VdAddress + ";" +
                        /*2*/  x.TaxInvoice + ";" + x.TaxInvoiceDt + ";0;15;0;0;" +
                        /*3*/  "15;0;0;15;0;0;15;" +
                        /*4*/  "0;0;2;0;0;" +
                        /*5*/  "2;0;0;" +
                        /*6*/  "2;0;0;" +
                        /*7*/  "2;0;;;" +
                        /*8*/  ";;;;;" +
                        /*9*/  ";;;;;" +
                        /*10*/ ";;;;;" +
                        /*11*/ ";" + x.ServiceCode + ";" +
                        /*12*/ x.TotalTax + ";2;" + x.PPhAmt + ";" +
                        /*13*/ "0;0;2;0;" +
                        /*14*/ "0;0;2;0;" +
                        /*15*/ "0;0;2;0;" +
                        /*16*/ "0;0;2;0;" +
                        /*17*/ "0;0;2;0;" +
                        /*18*/ x.TotalTax + ";" + x.PPhAmt
                    );
                }
                sw.Close();
                System.Diagnostics.Process.Start(sf.FileName);
            }
        }

        private void GetPIData(ref List<PPH23> l, string PIDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As DocType, A.DocNo, DATE_FORMAT(A.DocDt, '%d/%m/%Y') As DocDt, ");
            SQL.AppendLine("Replace(B.TIN, ';', '') As TIN, ");
            SQL.AppendLine("Replace(B.VdName, ';', '') As VdName, ");
            SQL.AppendLine("Replace(IfNull(G.CityName, ''), ';', '') As VdAddress, ");
            SQL.AppendLine("A.TaxCode1, A.TaxCode2, A.TaxCode3, C.TaxName As Tax1, D.TaxName As Tax2, E.TaxName As Tax3, ");
            SQL.AppendLine("replace(A.TaxInvoiceNo, ';', '') As TaxInvoiceNo, ");
            SQL.AppendLine("replace(A.TaxInvoiceNo2, ';', '') As TaxInvoiceNo2, ");
            SQL.AppendLine("replace(A.TaxInvoiceNo3, ';', '') As TaxInvoiceNo3, ");
            SQL.AppendLine("IfNull(DATE_FORMAT(A.TaxInvoiceDt, '%d/%m/%Y'), '') As TaxInvoiceDt, ");
            SQL.AppendLine("IfNull(DATE_FORMAT(A.TaxInvoiceDt2, '%d/%m/%Y'), '') As TaxInvoiceDt2, ");
            SQL.AppendLine("IfNull(DATE_FORMAT(A.TaxInvoiceDt3, '%d/%m/%Y'), '') As TaxInvoiceDt3, ");
            SQL.AppendLine("A.Curcode, A.VdCode, A.COATaxInd, ");
            SQL.AppendLine("Case When A.TaxRateAmt=0.00 Then 1.00 Else A.TaxRateAmt End As TaxRateAmt, ");
            SQL.AppendLine("F.Voucher, ");
            SQL.AppendLine("A.ServiceCode1, A.ServiceCode2, A.ServiceCode3, ");
            SQL.AppendLine("A.ServiceNote1, A.ServiceNote2, A.ServiceNote3 ");
            SQL.AppendLine("From (Select * From TblPurchaseInvoiceHdr Where Find_In_Set(DocNo, @PIDocNo)) A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
            SQL.AppendLine("Left Join TblTax C On A.TaxCode1 = C.TaxCode ");
            SQL.AppendLine("Left Join TblTax D On A.TaxCode2 = D.TaxCode ");
            SQL.AppendLine("Left Join TblTax E On A.TaxCode3 = E.TaxCode ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct Concat(T4.DocNo, ' (', Date_Format(T4.DocDt, '%d/%m/%Y'), ')') Order By T4.DocDt, T4.DocNo Separator ', ') As Voucher ");
            SQL.AppendLine("    From (Select * From TblPurchaseInvoiceHdr Where Find_In_Set(DocNo, @PIDocNo)) T1 ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo = T2.InvoiceDocNo And T2.InvoiceType = '1' ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T3 On T2.DocNo = T3.DocNo And T3.Status = 'A' And T3.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblVoucherHdr T4 On T3.VoucherRequestDocNo = T4.VoucherRequestDocNo And T4.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblParameter T5 On T5.ParCode='PPH23TaxCode' And T5.ParValue Is Not Null ");
            SQL.AppendLine("        And ((T1.TaxCode1 Is Not Null And Find_In_Set(T1.TaxCode1,T5.ParValue)) Or ");
            SQL.AppendLine("        (T1.TaxCode2 Is Not Null And Find_In_Set(T1.TaxCode2,T5.ParValue)) Or ");
            SQL.AppendLine("        (T1.TaxCode3 Is Not Null And Find_In_Set(T1.TaxCode3,T5.ParValue))) ");
            SQL.AppendLine("    And T1.ProcessInd = 'F' ");
            SQL.AppendLine("    And T1.CancelInd = 'N' ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") F On A.DocNo = F.DocNo ");
            SQL.AppendLine("Left Join TblCity G On B.CityCode = G.CityCode ");
            SQL.AppendLine("Where Exists (Select 1 From TblParameter Where ParCode = 'DocTitle' And ParValue = 'IOK') ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select '1' As DocType, A.DocNo, DATE_FORMAT(A.DocDt, '%d/%m/%Y') As DocDt, ");
            SQL.AppendLine("Replace(B.TIN, ';', '') As TIN, ");
            SQL.AppendLine("Replace(B.VdName, ';', '') As VdName, ");
            SQL.AppendLine("Replace(IfNull(D.CityName, ''), ';', '') As VdAddress, ");
            SQL.AppendLine("A.TaxCode, Null As TaxCode2, Null As TaxCode3, C.TaxName As Tax1, Null As Tax2, Null As Tax3, ");
            SQL.AppendLine("Replace(A.TaxInvNo, ';', '') As TaxInvoiceNo, ");
            SQL.AppendLine("Null As TaxInvoiceNo2, ");
            SQL.AppendLine("Null As TaxInvoiceNo3, ");
            SQL.AppendLine("IfNull(DATE_FORMAT(A.TaxInvDt, '%d/%m/%Y'), '') As TaxInvoiceDt, ");
            SQL.AppendLine("Null As TaxInvoiceDt2, ");
            SQL.AppendLine("Null As TaxInvoiceDt3, ");
            SQL.AppendLine("A.Curcode, A.VdCode, 'N' As COATaxInd, ");
            SQL.AppendLine("Case When A.CurCode=F.ParValue Then 1.00 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=F.ParValue ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0.00) End As TaxRateAmt, ");
            SQL.AppendLine("E.Voucher, ");
            SQL.AppendLine("Null As ServiceCode1, Null As ServiceCode2, Null As ServiceCode3, ");
            SQL.AppendLine("Null As ServiceNote1, Null As ServiceNote2, Null As ServiceNote3 ");
            SQL.AppendLine("From TblVoucherRequestTax A ");
            SQL.AppendLine("Left Join TblVendor B On A.VdCode=B.VdCode ");
            SQL.AppendLine("Left Join TblTax C On A.TaxCode=C.TaxCode ");
            SQL.AppendLine("Left Join TblCity D On B.CityCode=D.CityCode ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct Concat(T2.DocNo, ' (', Date_Format(T2.DocDt, '%d/%m/%Y'), ')') Order By T2.DocDt, T2.DocNo Separator ', ') As Voucher ");
            SQL.AppendLine("    From TblVoucherRequestTax T1 ");
            SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblParameter T3 ");
            SQL.AppendLine("        On T3.ParCode='PPH23TaxCode' ");
            SQL.AppendLine("        And T3.ParValue Is Not Null ");
            SQL.AppendLine("        And Find_In_Set(T1.TaxCode, T3.ParValue) ");
            SQL.AppendLine("    Where Find_In_Set(T1.DocNo, @PIDocNo) ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status='A' ");
            SQL.AppendLine("    And T1.TaxCode Is Not Null ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") E On A.DocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblParameter F On F.ParCode='MainCurCode' And F.ParValue Is Not Null ");
            SQL.AppendLine("Where Exists (Select 1 From TblParameter Where ParCode = 'DocTitle' And ParValue = 'IOK') ");
            SQL.AppendLine("And Find_In_Set(A.DocNo, @PIDocNo) ");
            SQL.AppendLine("Order By DocDt, DocNo; ");
            
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@PIDocNo", PIDocNo);
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "DocNo", 
                    
                    //1-5
                    "DocType", "DocDt", "VdName", "VdAddress", "TaxCode1", 
                    
                    //6-10
                    "TaxCode2", "TaxCode3", "Tax1", "Tax2", "Tax3", 

                    //11-15
                    "TaxInvoiceNo", "TaxInvoiceNo2", "TaxInvoiceNo3", "TaxInvoiceDt", "TaxInvoiceDt2", 
                    
                    //16-20
                    "TaxInvoiceDt3", "COATaxInd", "TIN", "TaxRateAmt", "CurCode",

                    //21-25
                    "VdCode", "ServiceCode1", "ServiceCode2", "ServiceCode3", "ServiceNote1",

                    //26-27
                    "ServiceNote2", "ServiceNote3"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PPH23()
                        {
                            KodeForm = "F113306",

                            DocNo = Sm.DrStr(dr, c[0]),
                            DocType = Sm.DrStr(dr, c[1]),
                            Date = Sm.DrStr(dr, c[2]),
                            Vendor = Sm.DrStr(dr, c[3]).Replace("\r\n", " ").Replace(",", " "),
                            VdAddress = Sm.DrStr(dr, c[4]).Replace("\r\n", " ").Replace(",", " "),

                            TaxCode1 = Sm.DrStr(dr, c[5]),
                            TaxCode2 = Sm.DrStr(dr, c[6]),
                            TaxCode3 = Sm.DrStr(dr, c[7]),
                            Tax1 = Sm.DrStr(dr, c[8]),
                            Tax2 = Sm.DrStr(dr, c[9]),

                            Tax3 = Sm.DrStr(dr, c[10]),
                            TaxInvoiceNo1 = Sm.DrStr(dr, c[11]),
                            TaxInvoiceNo2 = Sm.DrStr(dr, c[12]),
                            TaxInvoiceNo3 = Sm.DrStr(dr, c[13]),
                            TaxInvoiceDt1 = Sm.DrStr(dr, c[14]),

                            TaxInvoiceDt2 = Sm.DrStr(dr, c[15]),
                            TaxInvoiceDt3 = Sm.DrStr(dr, c[16]),
                            COATaxInd = Sm.DrStr(dr, c[17]),
                            TIN = Sm.DrStr(dr, c[18]).Replace("\r\n", " "),
                            TaxRateAmt = Sm.DrDec(dr, c[19]),

                            CurCode = Sm.DrStr(dr, c[20]),
                            VdCode = Sm.DrStr(dr, c[21]),
                            ServiceCode1 = Sm.DrStr(dr, c[22]),
                            ServiceCode2 = Sm.DrStr(dr, c[23]),
                            ServiceCode3 = Sm.DrStr(dr, c[24]),

                            ServiceNote1 = Sm.DrStr(dr, c[25]),
                            ServiceNote2 = Sm.DrStr(dr, c[26]),
                            ServiceNote3 = Sm.DrStr(dr, c[27]),
                        });
                    }
                }
                dr.Close();
            }

            if(l.Count > 0)
            {
                ComputeTotalWithoutTax(ref l, PIDocNo);
            }
        }

        private void GetTaxInvoiceAndServiceValue(ref List<PPH23> l)
        {
            string mTaxInvoiceNo = string.Empty, mTaxInvoiceDt = string.Empty, mServiceCode = string.Empty, mServiceNote = string.Empty;
            for (int x = 0; x < l.Count; x++)
            {
                string[] taxcode = { }; 
                
                if(Sm.GetLue(LueTaxGrpCode) == mPPH23TaxGrpCode)
                    taxcode = mPPH23TaxCode.Split(',');
                if (Sm.GetLue(LueTaxGrpCode) == mPPNTaxGrpCode)
                    taxcode = mPPNTaxCode.Split(',');

                foreach (string t in taxcode)
                {
                    if (l[x].TaxCode1 == t)
                    {
                        mTaxInvoiceNo = l[x].TaxInvoiceNo1;
                        mTaxInvoiceDt = l[x].TaxInvoiceDt1;
                        mServiceCode = l[x].ServiceCode1;
                        mServiceNote = l[x].ServiceNote1;
                    }

                    if (l[x].TaxCode2 == t)
                    {
                        mTaxInvoiceNo = l[x].TaxInvoiceNo2;
                        mTaxInvoiceDt = l[x].TaxInvoiceDt2;
                        mServiceCode = l[x].ServiceCode2;
                        mServiceNote = l[x].ServiceNote2;
                    }

                    if (l[x].TaxCode3 == t)
                    {
                        mTaxInvoiceNo = l[x].TaxInvoiceNo3;
                        mTaxInvoiceDt = l[x].TaxInvoiceDt3;
                        mServiceCode = l[x].ServiceCode3;
                        mServiceNote = l[x].ServiceNote3;
                    }
                }

                l[x].TaxInvoice = mTaxInvoiceNo;
                l[x].TaxInvoiceDt = mTaxInvoiceDt;
                l[x].ServiceCode = mServiceCode;
                l[x].ServiceNote = mServiceNote;

                if (l[x].ServiceCode.Length > 0) l[x].ServiceName = Sm.GetValue("Select ServiceName From TblService Where ServiceCode = @Param; ", l[x].ServiceCode);

                if (mTaxInvoiceDt.Length > 0)
                {
                    var dt = new DateTime(
                        Int32.Parse(Sm.Right(mTaxInvoiceDt, 4)),
                        Int32.Parse(mTaxInvoiceDt.Substring(3, 2)),
                        Int32.Parse(Sm.Left(mTaxInvoiceDt, 2))
                        );

                    l[x].PrintPPH23Date = dt.ToString("dd MMMM yyyy");
                }
                else
                {
                    var dt = new DateTime(
                        Int32.Parse(Sm.Right(l[x].Date, 4)),
                        Int32.Parse(l[x].Date.Substring(3, 2)),
                        Int32.Parse(Sm.Left(l[x].Date, 2))
                        );

                    l[x].PrintPPH23Date = dt.ToString("dd MMMM yyyy");
                }
            }
        }

        private void ComputeTotalWithoutTax(ref List<PPH23> l, string PIDocNo)
        {
            var ld = new List<Dtl>();
            var ld2 = new List<Dtl2>();
            var lc = new List<AmtCOA>();

            ProcessDtl(ref l, ref ld, PIDocNo);
            ProcessDtl2(ref l, ref ld2, PIDocNo);
            ProcessAmtCOA(ref l, ref lc);

            for (int x = 0; x < l.Count; x++)
            {
                decimal mTotalWithoutTax = 0;

                if (ld.Count > 0)
                {
                    for (int y = 0; y < ld.Count; y++)
                    {
                        if (l[x].DocNo == ld[y].DocNo)
                        {
                            mTotalWithoutTax += ld[y].Total;
                            break;
                        }
                    }
                }

                if (ld2.Count > 0)
                {
                    for (int z = 0; z < ld2.Count; z++)
                    {
                        if (l[x].DocNo == ld2[z].DocNo)
                        {
                            mTotalWithoutTax += ld2[z].Amt;
                        }
                    }
                }

                if (lc.Count > 0)
                {
                    for (int a = 0; a < lc.Count; a++)
                    {
                        if (l[x].DocNo == lc[a].DocNo)
                        {
                            string mVendorAcNoDownpayment = Sm.GetValue("Select Concat(ParValue, '" + l[x].VdCode + "') As AcNo From TblParameter Where ParCode = 'VendorAcNoAP'; ");
                            if (lc[a].AcNo == mVendorAcNoDownpayment)
                            {
                                string mAcType = Sm.GetValue("Select AcType From TblCoa Where AcNo = '" + lc[a].AcNo + "'");
                                if (lc[a].DAmt != 0)
                                {
                                    string AcType = "D";
                                    if (AcType == mAcType) mTotalWithoutTax += lc[a].DAmt;
                                    else mTotalWithoutTax -= lc[a].DAmt;
                                }

                                if (lc[a].CAmt != 0)
                                {
                                    string AcType = "C";
                                    if (AcType == mAcType) mTotalWithoutTax += lc[a].CAmt;
                                    else mTotalWithoutTax -= lc[a].CAmt;
                                }
                            }
                        }
                    }
                }
                if (Sm.CompareStr(l[x].CurCode, mMainCurCode))
                    l[x].TotalTax = Math.Truncate(mTotalWithoutTax);
                else
                    l[x].TotalTax = Math.Truncate(mTotalWithoutTax * l[x].TaxRateAmt);
            }

            ld.Clear();
            ld2.Clear();
            lc.Clear();
        }

        private void ProcessDtl(ref List<PPH23> l, ref List<Dtl> ld, string PIDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, ");
            SQL.AppendLine("Sum( ");
            SQL.AppendLine("    ((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0.00)=0.00 Then 1.00 Else (100.00-D.Discount)/100.00 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+D.RoundingValue) ");
            SQL.AppendLine(") As Total ");
            SQL.AppendLine("From (Select * From TblPurchaseInvoiceDtl Where Find_In_Set(DocNo, '" + PIDocNo + "')) A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo ");
            SQL.AppendLine("Inner Join TblRecvVdhdr B2 On A.RecvVdDocNo=B2.DocNo ");
            SQL.AppendLine("Inner Join TblPOHdr C On B.PODocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo=F.DocNo And E.MaterialRequestDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr G On E.QtDocNo=G.DocNo ");
            SQL.AppendLine("Inner Join TblQtDtl H On E.QtDocNo=H.DocNo And E.QtDNo=H.DNo ");
            SQL.AppendLine("Inner Join TblItem I On B.ItCode=I.ItCode ");
            SQL.AppendLine("Group By A.DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select DocNo, Amt As Total ");
            SQL.AppendLine("From TblVoucherRequestTax ");
            SQL.AppendLine("Where Find_In_Set(DocNo, '" + PIDocNo + "') ");
            SQL.AppendLine("Group By DocNo ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Total" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ld.Add(new Dtl()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            Total = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessDtl2(ref List<PPH23> l, ref List<Dtl2> ld2, string PIDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.AmtType, Sum(A.Amt) As Amt ");
            SQL.AppendLine("From (Select * From TblPurchaseInvoiceDtl2 Where Find_In_Set(DocNo, '" + PIDocNo + "')) A ");
            SQL.AppendLine("Inner Join TblOption B On A.AmtType=B.OptCode And B.OptCat='InvoiceAmtType' ");
            SQL.AppendLine("Group By A.DocNo, A.AmtType; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "AmtType", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ld2.Add(new Dtl2()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            AmtType = Sm.DrStr(dr, c[1]),
                            Amt = (Sm.DrStr(dr, c[1]) == "2") ? Sm.DrDec(dr, c[2]) : (Sm.DrDec(dr, c[2]) * -1)
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAmtCOA(ref List<PPH23> l, ref List<AmtCOA> lc)
        {
            string mPIDocNo2 = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].COATaxInd == "N")
                {
                    if (mPIDocNo2.Length > 0) mPIDocNo2 += ",";
                    mPIDocNo2 += l[i].DocNo;
                }
            }

            if (mPIDocNo2.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo, AcNo, DAMt, CAmt ");
                SQL.AppendLine("From TblPurchaseInvoiceDtl4 ");
                SQL.AppendLine("Where Find_In_Set(DocNo, '" + mPIDocNo2 + "'); ");

                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "AcNo", "DAmt", "CAMt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lc.Add(new AmtCOA()
                            {
                                DocNo = Sm.DrStr(dr, c[0]),
                                AcNo = Sm.DrStr(dr, c[1]),
                                DAmt = Sm.DrDec(dr, c[2]),
                                CAmt = Sm.DrDec(dr, c[3])
                            });
                        }
                    }
                    dr.Close();
                }
            }
        }

        private void ProcessPPHAmt(ref List<PPH23> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                string[] taxcode = mPPH23TaxCode.Split(',');
                decimal mTaxAmt = 0m, mTotalWithoutTax = 0m;
                if (l[i].TotalTax != 0) mTotalWithoutTax = l[i].TotalTax;
                foreach (string TaxCodePPH in taxcode)
                {
                    if (l[i].TaxCode1 == TaxCodePPH) mTaxAmt = GetTaxRate(l[i].TaxCode1) / 100 * mTotalWithoutTax;
                    if (l[i].TaxCode2 == TaxCodePPH) mTaxAmt = GetTaxRate(l[i].TaxCode2) / 100 * mTotalWithoutTax;
                    if (l[i].TaxCode3 == TaxCodePPH) mTaxAmt = GetTaxRate(l[i].TaxCode3) / 100 * mTotalWithoutTax;
                }
                l[i].PPhAmt = Math.Truncate((mTaxAmt < 0) ? (mTaxAmt * -1) : mTaxAmt);
                l[i].Terbilang = Sm.Terbilang(l[i].PPhAmt);
                l[i].PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()));
            }
        }

        private decimal GetTaxRate(string TaxCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select TaxRate from TblTax Where TaxCode=@TaxCode"
            };
            Sm.CmParam<String>(ref cm, "@TaxCode", TaxCode);
            string TaxRate = Sm.GetValue(cm);

            if (TaxRate.Length != 0) return decimal.Parse(TaxRate);
            return 0m;
        }

        private string GetProfitCenterCode()
        {
            //var Value = string.Empty ;
            var Value = Sm.GetLue(LueBankAcCode);
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    " SELECT B.ProfitCenterCode FROM tblbankaccount A " +
                    " INNER JOIN tblsite B ON A.SiteCode = B.SiteCode " +
                    " INNER JOIN tblprofitcenter C ON B.ProfitCenterCode = C.ProfitCenterCode " +
                    " WHERE A.Bankaccode = @Param ",

                    



            Value);
        }

        private void GetPRIData(ref List<PPH23> l, string PRIDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '2' As DocType, A.DocNo, DATE_FORMAT(A.DocDt, '%d/%m/%Y') As DocDt, replace(B.TIN, ';', '') As TIN, replace(B.VdName, ';', '') As VdName, replace(IfNull(G.CityName, ''), ';', '') As VdAddress, ");
            SQL.AppendLine("A.TaxCode1, A.TaxCode2, A.TaxCode3, C.TaxName As Tax1, D.TaxName As Tax2, E.TaxName As Tax3, ");
            SQL.AppendLine("replace(A.TaxInvoiceNo, ';', '') As TaxInvoiceNo, replace(A.TaxInvoiceNo2, ';', '') As TaxInvoiceNo2, ");
            SQL.AppendLine("replace(A.TaxInvoiceNo3, ';', '') As TaxInvoiceNo3, IfNull(DATE_FORMAT(A.TaxInvoiceDt, '%d/%m/%Y'), '') As TaxInvoiceDt, ");
            SQL.AppendLine("IfNull(DATE_FORMAT(A.TaxInvoiceDt2, '%d/%m/%Y'), '') As TaxInvoiceDt2, IfNull(DATE_FORMAT(A.TaxInvoiceDt3, '%d/%m/%Y'), '') As TaxInvoiceDt3, ");
            SQL.AppendLine("A.Curcode, A.VdCode, 1 As TaxRateAmt, A.AmtBefTax, A.ServiceCode1, A.ServiceCode2, A.ServiceCode3 ");
            SQL.AppendLine("From (Select * From TblPurchaseReturnInvoiceHdr Where Find_In_Set(DocNo, @PRIDocNo)) A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
            SQL.AppendLine("Left Join TblTax C On A.TaxCode1 = C.TaxCode ");
            SQL.AppendLine("Left Join TblTax D On A.TaxCode2 = D.TaxCode ");
            SQL.AppendLine("Left Join TblTax E On A.TaxCode3 = E.TaxCode ");
            SQL.AppendLine("Left Join TblCity G On B.CityCode = G.CityCode ");
            SQL.AppendLine("Where Exists (Select 1 From TblParameter Where ParCode = 'DocTitle' And ParValue = 'IOK') ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@PRIDocNo", PRIDocNo);
                cm.CommandText = SQL.ToString() + " Order By A.DocDt, A.DocNo; ";

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "DocNo", 
                    
                    //1-5
                    "DocType", "DocDt", "VdName", "VdAddress", "TaxCode1", 
                    
                    //6-10
                    "TaxCode2", "TaxCode3", "Tax1", "Tax2", "Tax3", 

                    //11-15
                    "TaxInvoiceNo", "TaxInvoiceNo2", "TaxInvoiceNo3", "TaxInvoiceDt", "TaxInvoiceDt2", 
                    
                    //16-20
                    "TaxInvoiceDt3", "TIN", "TaxRateAmt", "CurCode", "VdCode",

                    //21-24
                    "AmtBefTax", "ServiceCode1", "ServiceCode2", "ServiceCode3"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PPH23()
                        {
                            KodeForm = "F113306",

                            DocNo = Sm.DrStr(dr, c[0]),
                            DocType = Sm.DrStr(dr, c[1]),
                            Date = Sm.DrStr(dr, c[2]),
                            Vendor = Sm.DrStr(dr, c[3]).Replace("\r\n", " ").Replace(",", " "),
                            VdAddress = Sm.DrStr(dr, c[4]).Replace("\r\n", " ").Replace(",", " "),

                            TaxCode1 = Sm.DrStr(dr, c[5]),
                            TaxCode2 = Sm.DrStr(dr, c[6]),
                            TaxCode3 = Sm.DrStr(dr, c[7]),
                            Tax1 = Sm.DrStr(dr, c[8]),
                            Tax2 = Sm.DrStr(dr, c[9]),

                            Tax3 = Sm.DrStr(dr, c[10]),
                            TaxInvoiceNo1 = Sm.DrStr(dr, c[11]),
                            TaxInvoiceNo2 = Sm.DrStr(dr, c[12]),
                            TaxInvoiceNo3 = Sm.DrStr(dr, c[13]),
                            TaxInvoiceDt1 = Sm.DrStr(dr, c[14]),

                            TaxInvoiceDt2 = Sm.DrStr(dr, c[15]),
                            TaxInvoiceDt3 = Sm.DrStr(dr, c[16]),
                            TIN = Sm.DrStr(dr, c[17]).Replace("\r\n", " "),
                            TaxRateAmt = Sm.DrDec(dr, c[18]),
                            CurCode = Sm.DrStr(dr, c[19]),

                            VdCode = Sm.DrStr(dr, c[20]),
                            TotalTax = Math.Truncate(Sm.DrDec(dr, c[21])),
                            ServiceCode1 = Sm.DrStr(dr, c[22]),
                            ServiceCode2 = Sm.DrStr(dr, c[23]),
                            ServiceCode3 = Sm.DrStr(dr, c[24]),

                            ServiceNote1 = string.Empty,
                            ServiceNote2 = string.Empty,
                            ServiceNote3 = string.Empty,
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                ProcessPPHAmt(ref l);
            }
        }

        private void ProcessCSVPPNIn()
        {
            if (Grd3.Rows.Count > 0)
            {
                var lI = new List<PPNIn>();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DNo, A.DocType,  ");
                SQL.AppendLine("Case A.DocType  ");
                SQL.AppendLine("    When '1' Then 'Purchase Invoice'  ");
                SQL.AppendLine("    When '2' Then 'Purchase Return Invoice'  ");
                SQL.AppendLine("    When '3' Then 'Voucher Request Tax'  ");
                SQL.AppendLine("    When '4' Then 'Purchase Invoice Raw Material'  ");
                SQL.AppendLine("End As DocTypeDesc,  ");
                SQL.AppendLine("A.DocNoIn,  ");
                SQL.AppendLine("Case A.DocType  ");
                SQL.AppendLine("    When '1' Then B.DocDt  ");
                SQL.AppendLine("    When '2' Then C.DocDt  ");
                SQL.AppendLine("    When '3' Then D.DocDt  ");
                SQL.AppendLine("    When '4' Then E.DocDt  ");
                SQL.AppendLine("End As DocDt,  ");
                SQL.AppendLine("A.Amt, A.ExcRate As ExcRate,  ");
                SQL.AppendLine("(A.Amt*IfNull(A.ExcRate, 0.00)) As Amt2,  ");
                SQL.AppendLine("A.TaxInvoiceNo, ");
                SQL.AppendLine("Case When A.TaxInvoiceDt Is Null Then ");
                SQL.AppendLine("    Null ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    IfNull(DATE_FORMAT(A.TaxInvoiceDt, '%d/%m/%Y'), '') ");
                SQL.AppendLine("End As TaxInvoiceDt, ");
                SQL.AppendLine("A.Remark,  ");
                SQL.AppendLine("Case A.DocType  ");
                SQL.AppendLine("    When '1' Then B.CurCode  ");
                SQL.AppendLine("    When '2' Then C.CurCode  ");
                SQL.AppendLine("    When '3' Then D.CurCode  ");
                SQL.AppendLine("    When '4' Then E.CurCode  ");
                SQL.AppendLine("End As CurCode, ");
                SQL.AppendLine("Case A.DocType ");
                SQL.AppendLine("    When '1' Then B.JmlDPP ");
                SQL.AppendLine("    When '2' Then C.JmlDPP ");
                SQL.AppendLine("    When '3' Then D.JmlDPP ");
                SQL.AppendLine("    When '4' Then E.JmlDPP ");
                SQL.AppendLine("End As JmlDPP, ");
                SQL.AppendLine("Case A.DocType ");
                SQL.AppendLine("    When '1' Then (B.JmlDPP*IfNull(A.ExcRate, 0.00)) ");
                SQL.AppendLine("    When '2' Then (C.JmlDPP*IfNull(A.ExcRate, 0.00)) ");
                SQL.AppendLine("    When '3' Then (D.JmlDPP*IfNull(A.ExcRate, 0.00)) ");
                SQL.AppendLine("    When '4' Then (E.JmlDPP*IfNull(A.ExcRate, 0.00)) ");
                SQL.AppendLine("End As JmlDPP2, ");
                SQL.AppendLine("Case A.DocType ");
                SQL.AppendLine("    When '1' Then Replace(Replace(B.Name, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("    When '2' Then Replace(Replace(C.Name, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("    When '3' Then Replace(Replace(D.Name, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("    When '4' Then Replace(Replace(E.Name, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("End As Name, ");
                SQL.AppendLine("Case A.DocType ");
                SQL.AppendLine("    When '1' Then Replace(Replace(B.Address, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("    When '2' Then Replace(Replace(C.Address, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("    When '3' Then Replace(Replace(D.Address, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("    When '4' Then Replace(Replace(E.Address, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("End As Address, ");
                SQL.AppendLine("Case A.DocType ");
                SQL.AppendLine("    When '1' Then B.NPWP ");
                SQL.AppendLine("    When '2' Then C.NPWP ");
                SQL.AppendLine("    When '3' Then D.NPWP ");
                SQL.AppendLine("    When '4' Then E.NPWP ");
                SQL.AppendLine("End As NPWP ");
                SQL.AppendLine("From  ");
                SQL.AppendLine("( ");
	            SQL.AppendLine("    Select * From TblVoucherRequestPPNDtl Where DocNo = @DocNo ");
                SQL.AppendLine(")A  ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("( ");
	            SQL.AppendLine("    Select T1.DocNo, T1.Amt As JmlDPP, T2.VdName As Name, T3.CityName As Address, T2.TIN As NPWP, ");
	            SQL.AppendLine("    T1.CurCode, T1.DocDt ");
	            SQL.AppendLine("    From TblPurchaseInvoiceHdr T1 ");
	            SQL.AppendLine("    Inner Join TblVendor T2 On T1.VdCode = T2.VdCode ");
	            SQL.AppendLine("    Left Join TblCity T3 On T2.CityCode = T3.CityCode ");
                SQL.AppendLine("    Where ( ");
                SQL.AppendLine("        T1.VoucherRequestPPNDocNo = @DocNo Or ");
                SQL.AppendLine("        T1.VoucherRequestPPNDocNo2 = @DocNo Or ");
                SQL.AppendLine("        T1.VoucherRequestPPNDocNo3 = @DocNo ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") B On A.DocNoIn = B.DocNo And A.DocType = '1' ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("( ");
	            SQL.AppendLine("    Select T1.DocNo, T1.AmtBefTax As JmlDPP, T2.VdName As Name, T3.CityName As Address, T2.TIN As NPWP, ");
	            SQL.AppendLine("    T1.CurCode, T1.DocDt ");
	            SQL.AppendLine("    From TblPurchaseReturnInvoiceHdr T1 ");
	            SQL.AppendLine("    Inner Join TblVendor T2 On T1.VdCode = T2.VdCode ");
	            SQL.AppendLine("    Left Join TblCity T3 On T2.CityCode = T3.CityCode ");
                SQL.AppendLine("    Where ( ");
                SQL.AppendLine("        T1.VoucherRequestPPNDocNo = @DocNo Or ");
                SQL.AppendLine("        T1.VoucherRequestPPNDocNo2 = @DocNo Or ");
                SQL.AppendLine("        T1.VoucherRequestPPNDocNo3 = @DocNo ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") C On A.DocNoIn=C.DocNo And A.DocType = '2' ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("( ");
	            SQL.AppendLine("    Select T1.DocNo, (T1.Amt * T2.TaxRate) As JmlDPP, T3.VdName As Name, T4.CityName As Address, T3.TIN As NPWP, ");
	            SQL.AppendLine("    T1.CurCode, T1.DocDt ");
	            SQL.AppendLine("    From TblVoucherRequestTax T1 ");
	            SQL.AppendLine("    Inner Join TblTax T2 On T1.TaxCode = T2.TaxCode ");
                SQL.AppendLine("    Left Join TblVendor T3 On T1.VdCode = T3.VdCode ");
                SQL.AppendLine("    Left Join TblCity T4 On T3.CityCode = T4.CityCode ");
                SQL.AppendLine("    Where T1.VoucherRequestPPNDocNo = @DocNo ");
                SQL.AppendLine(") D On A.DocNoIn=D.DocNo And A.DocType = '3' ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, T1.Tax As JmlDPP, T4.VdName As Name, ");
                SQL.AppendLine("    T5.CityName As Address, T4.TIN As NPWP, ");
                SQL.AppendLine("    T1.CurCode, T1.DocDt ");
                SQL.AppendLine("    From TblPurchaseInvoiceRawMaterialHdr T1 ");
                SQL.AppendLine("    Inner Join TblRawMaterialVerify T2 On T1.RawMaterialVerifyDocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblLegalDocVerifyHdr T3 On T2.LegalDocVerifyDocNo=T3.DocNo ");
                SQL.AppendLine("    Left Join TblVendor T4 On T3.VdCode = T4.VdCode ");
                SQL.AppendLine("    Left Join TblCity T5 On T4.CityCode = T5.CityCode ");
                SQL.AppendLine("    Where T1.VoucherRequestPPNDocNo = @DocNo ");
                SQL.AppendLine("    And T1.CancelInd='N' ");
                SQL.AppendLine("    And T1.TaxInd='Y' ");
                SQL.AppendLine(") E On A.DocNoIn=E.DocNo And A.DocType='4' ");
                SQL.AppendLine("; ");

                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    cm.CommandText = SQL.ToString();

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    {
                        //0
                        "TaxInvoiceNo", 
                        
                        //1-5
                        "TaxInvoiceDt", "Amt2", "JmlDPP2", "Name", "Address", 
                        
                        //6
                        "NPWP"
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lI.Add(new PPNIn()
                            {
                                TaxInvoiceNo = Sm.DrStr(dr, c[0]),

                                TaxInvoiceDt = Sm.DrStr(dr, c[1]),
                                JmlPPN = Math.Truncate(Sm.DrDec(dr, c[2])),
                                JmlDPP = Math.Truncate(Sm.DrDec(dr, c[3])),
                                Name = Sm.DrStr(dr, c[4]).Replace("\r\n", " ").Replace(",", " "),
                                Address = Sm.DrStr(dr, c[5]).Replace("\r\n", " ").Replace(",", " "),

                                NPWP = Sm.DrStr(dr, c[6]).Replace("\r\n", " ").Replace(",", " ")
                            });
                        }
                    }
                    dr.Close();
                }

                if(lI.Count > 0)
                {
                    GenerateCSVPPNIn(ref lI);
                }

                lI.Clear();
            }
        }

        private void GenerateCSVPPNIn(ref List<PPNIn> lI)
        {
            var FileName = "PPNIn";
            var sf = new SaveFileDialog();
            sf.Filter = "CSV files (*.csv)|*.csv";
            sf.FileName = FileName;

            if (sf.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(sf.FileName, false, Encoding.GetEncoding(1252));
                string TaxInvoiceNo = string.Empty;

                sw.Write(
                    /*1*/  "\"FM\",\"KD_JENIS_TRANSAKSI\",\"FG_PENGGANTI\",\"NOMOR_FAKTUR\",\"MASA_PAJAK\",\"TAHUN_PAJAK\"," +
                    /*2*/  "\"TANGGAL_FAKTUR\",\"NPWP\",\"NAMA\",\"ALAMAT_LENGKAP\",\"JUMLAH_DPP\",\"JUMLAH_PPN\",\"JUMLAH_PPNBM\",\"IS_CREDITABLE\""
                        );

                foreach (var x in lI)
                {
                    
                    TaxInvoiceNo = ((x.TaxInvoiceNo.Replace(".", string.Empty)).Replace("-", string.Empty)).Trim();
                    if (TaxInvoiceNo.Length > 3)
                        TaxInvoiceNo = Sm.Right(TaxInvoiceNo, TaxInvoiceNo.Length - 3);
                    else
                        TaxInvoiceNo = string.Empty;
                    sw.Write(sw.NewLine);
                    sw.Write(
                        /*1*/  "\"FM\"" + ",\"" + string.Concat("0", (x.TaxInvoiceNo.Length==0?string.Empty:x.TaxInvoiceNo.Substring(1, 1))) + "\",\"" + (x.TaxInvoiceNo.Length==0?string.Empty:x.TaxInvoiceNo.Substring(2, 1)) + "\",\"" +
                        TaxInvoiceNo + 
                        "\",\"" + TxtTaxPeriod.Text + "\",\"" + Sm.Right(x.TaxInvoiceDt, 4) + "\",\"" +
                        /*2*/  x.TaxInvoiceDt + "\",\"" + (x.NPWP.Replace(".", string.Empty)).Replace("-", string.Empty) + "\",\"" + x.Name + "\",\"" + x.Address + "\",\"" + x.JmlDPP + "\",\"" + x.JmlPPN + "\",\"0\",\"1\""
                    );
                }
                sw.Close();
                System.Diagnostics.Process.Start(sf.FileName);
            }
        }

        private void ProcessCSVPPNOut()
        {
            if(Grd2.Rows.Count > 0)
            {
                var lO = new List<PPNOut>();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DNo, A.DocType,  ");
                SQL.AppendLine("Case A.DocType  ");
                SQL.AppendLine("    When '1' Then 'Sales Invoice'  ");
                SQL.AppendLine("    When '2' Then 'Sales Return Invoice'  ");
                SQL.AppendLine("    When '3' Then 'Point of Sale'  ");
                SQL.AppendLine("    When '4' Then 'Sales Invoice for Project'  ");
                SQL.AppendLine("End As DocTypeDesc,  ");
                SQL.AppendLine("A.DocNoOut,  ");
                SQL.AppendLine("Case A.DocType  ");
                SQL.AppendLine("    When '1' Then IfNull(DATE_FORMAT(B.DocDt, '%d/%m/%Y'), '') ");
                SQL.AppendLine("    When '2' Then IfNull(DATE_FORMAT(C.DocDt, '%d/%m/%Y'), '') ");
                SQL.AppendLine("    When '3' Then IfNull(DATE_FORMAT(D.DocDt, '%d/%m/%Y'), '') ");
                SQL.AppendLine("    When '4' Then IfNull(DATE_FORMAT(E.DocDt, '%d/%m/%Y'), '') ");
                SQL.AppendLine("End As DocDt,  ");
                SQL.AppendLine("A.Remark, A.TaxInvoiceNo, IfNull(DATE_FORMAT(A.TaxInvoiceDt, '%d/%m/%Y'), '') As TaxInvoiceDt, ");
                SQL.AppendLine("A.Amt, A.ExcRate, A.Amt*A.ExcRate As Amt2,  ");
                SQL.AppendLine("Case A.DocType  ");
                SQL.AppendLine("    When '1' Then B.CurCode  ");
                SQL.AppendLine("    When '2' Then C.CurCode  ");
                SQL.AppendLine("    When '3' Then D.CurCode  ");
                SQL.AppendLine("    When '4' Then E.CurCode  ");
                SQL.AppendLine("End As CurCode,  ");
                SQL.AppendLine("Case A.DocType ");
                SQL.AppendLine("    When '1' Then B.JmlDPP ");
                SQL.AppendLine("    When '2' Then C.JmlDPP ");
                SQL.AppendLine("    When '3' Then D.JmlDPP ");
                SQL.AppendLine("    When '4' Then E.JmlDPP ");
                SQL.AppendLine("End As JmlDPP, ");
                SQL.AppendLine("Case A.DocType ");
                SQL.AppendLine("    When '1' Then (B.JmlDPP*IfNull(A.ExcRate, 0.00)) ");
                SQL.AppendLine("    When '2' Then (C.JmlDPP*IfNull(A.ExcRate, 0.00)) ");
                SQL.AppendLine("    When '3' Then (D.JmlDPP*IfNull(A.ExcRate, 0.00)) ");
                SQL.AppendLine("    When '4' Then (E.JmlDPP*IfNull(A.ExcRate, 0.00)) ");
                SQL.AppendLine("End As JmlDPP2, ");
                SQL.AppendLine("Case A.DocType ");
                SQL.AppendLine("    When '1' Then Replace(Replace(B.Name, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("    When '2' Then Replace(Replace(C.Name, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("    When '3' Then Replace(Replace(D.Name, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("    When '4' Then Replace(Replace(E.Name, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("End As Name, ");
                SQL.AppendLine("Case A.DocType ");
                SQL.AppendLine("    When '1' Then Replace(Replace(B.Address, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("    When '2' Then Replace(Replace(C.Address, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("    When '3' Then Replace(Replace(D.Address, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("    When '4' Then Replace(Replace(E.Address, '\r\n', ' '), ';', '') ");
                SQL.AppendLine("End As Address, ");
                SQL.AppendLine("Case A.DocType ");
                SQL.AppendLine("    When '1' Then B.NPWP ");
                SQL.AppendLine("    When '2' Then C.NPWP ");
                SQL.AppendLine("    When '3' Then D.NPWP ");
                SQL.AppendLine("    When '4' Then E.NPWP ");
                SQL.AppendLine("End As NPWP ");
                SQL.AppendLine("From  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select * From TblVoucherRequestPPNDtl2 Where DocNo = @DocNo ");
                SQL.AppendLine(") A  ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.DocNo, T1.TotalAmt As JmlDPP, T2.CtName As Name, T3.CityName As Address, T2.NPWP, ");
                SQL.AppendLine("    T1.CurCode, T1.DocDt ");
                SQL.AppendLine("    From TblSalesInvoiceHdr T1 ");
                SQL.AppendLine("    Inner Join TblCustomer T2 On T1.CtCode = T2.CtCode ");
                SQL.AppendLine("    Left Join TblCity T3 On T2.CityCode = T3.CityCode ");
                SQL.AppendLine("    Where T1.VoucherRequestPPNDocNo = @DocNo ");
                SQL.AppendLine(") B On A.DocNoOut=B.DocNo And A.DocType = '1' ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.DocNo, T1.AmtBefTax As JmlDPP, T2.CtName As Name, T3.CityName As Address, T2.NPWP, ");
                SQL.AppendLine("    T1.CurCode, T1.DocDt ");
                SQL.AppendLine("    From TblSalesReturnInvoiceHdr T1 ");
                SQL.AppendLine("    Inner Join TblCustomer T2 On T1.CtCode = T2.CtCode ");
                SQL.AppendLine("    Left Join TblCity T3 On T2.CityCode = T3.CityCode ");
                SQL.AppendLine("    Where T1.VoucherRequestPPNDocNo = @DocNo ");
                SQL.AppendLine(") C On A.DocNoOut=C.DocNo And A.DocType = '2' ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.DocNo, (T1.Amt * IfNull(T4.TaxRate, 1)) As JmlDPP, T2.CtName As Name, T5.CityName As Address, T2.NPWP, ");
                SQL.AppendLine("    T1.CurCode, T1.DocDt ");
                SQL.AppendLine("    From TblPOSVat T1 ");
                SQL.AppendLine("    Inner Join TblCustomer T2 On T1.CtCode = T2.CtCode ");
                SQL.AppendLine("    Inner Join TblPosSetting T3 On T1.PosNo = T3.PosNo And T3.SetCode = 'Tax1Code' ");
                SQL.AppendLine("    Left Join TblTax T4 On T3.SetValue = T4.TaxCode ");
                SQL.AppendLine("    Left Join TblCity T5 On T2.CityCode = T5.CityCode ");
                SQL.AppendLine("    Where T1.VoucherRequestPPNDocNo = @DocNo ");
                SQL.AppendLine(") D On A.DocNoOut=D.DocNo And A.DocType = '3' ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.DocNo, T1.TotalAmt As JmlDPP, T2.CtName As Name, T3.CityName As Address, T2.NPWP, ");
                SQL.AppendLine("    T1.CurCode, T1.DocDt ");
                SQL.AppendLine("    From TblSalesInvoice5Hdr T1 ");
                SQL.AppendLine("    Inner Join TblCustomer T2 On T1.CtCode = T2.CtCode ");
                SQL.AppendLine("    Left Join TblCity T3 On T2.CityCode = T3.CityCode ");
                SQL.AppendLine("    Where T1.VoucherRequestPPNDocNo = @DocNo ");
                SQL.AppendLine(") E On A.DocNoOut=E.DocNo And A.DocType = '4' ");
                SQL.AppendLine("; ");

                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    cm.CommandText = SQL.ToString();

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    {
                        //0
                        "TaxInvoiceNo", 
                        
                        //1-5
                        "TaxInvoiceDt", "Amt2", "JmlDPP2", "Name", "NPWP",
                        
                        //6-7
                        "DocNoOut", "DocDt"
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lO.Add(new PPNOut()
                            {
                                TaxInvoiceNo = Sm.DrStr(dr, c[0]),

                                TaxInvoiceDt = Sm.DrStr(dr, c[1]),
                                JmlPPN = Math.Truncate(Sm.DrDec(dr, c[2])),
                                JmlDPP = Math.Truncate(Sm.DrDec(dr, c[3])),
                                Name = Sm.DrStr(dr, c[4]).Replace("\r\n", " ").Replace(",", " "),
                                NPWP = Sm.DrStr(dr, c[5]).Replace("\r\n", " ").Replace(",", " "),

                                DocNoOut = Sm.DrStr(dr, c[6]),
                                DocDt = Sm.DrStr(dr, c[7])
                            });
                        }
                    }
                    dr.Close();
                }

                if (lO.Count > 0)
                {
                    GenerateCSVPPNOut(ref lO);
                }

                lO.Clear();
            }
        }

        private void GenerateCSVPPNOut(ref List<PPNOut> lO)
        {
            var FileName = "PPNOut";
            var sf = new SaveFileDialog();
            sf.Filter = "CSV files (*.csv)|*.csv";
            sf.FileName = FileName;

            if (sf.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(sf.FileName, false, Encoding.GetEncoding(1252));

                sw.Write(
                    /*1*/  "\"RK\",\"NPWP\",\"NAMA\",\"KD_JENIS_TRANSAKSI\",\"FG_PENGGANTI\",\"NOMOR_FAKTUR\",\"TANGGAL_FAKTUR\",\"NOMOR_DOKUMEN_RETUR\"," +
                    /*2*/  "\"TANGGAL_RETUR\",\"MASA_PAJAK_RETUR\",\"TAHUN_PAJAK_RETUR\",\"NILAI_RETUR_DPP\",\"NILAI_RETUR_PPN\",\"NILAI_RETUR_PPNBM\""
                        );

                foreach (var x in lO)
                {
                    sw.Write(sw.NewLine);
                    sw.Write(
                        /*1*/  "\"RK\"" + ",\"" + x.NPWP + "\",\"" + x.Name + "\",\"" + x.TaxInvoiceNo.Substring(1, 1) + "\",\"" + x.TaxInvoiceNo.Substring(2, 1) + "\",\"" + x.TaxInvoiceNo + "\",\"" + x.TaxInvoiceDt + "\",\"" + x.DocNoOut + "\",\"" +
                        /*2*/  x.DocDt + "\",\"" + TxtTaxPeriod.Text + "\",\"" + Sm.Right(x.TaxInvoiceDt, 4) + "\",\"" + x.JmlDPP + "\",\"" + x.JmlPPN + "\",\"0\""
                    );
                }
                sw.Close();
                System.Diagnostics.Process.Start(sf.FileName);
            }
        }

        private bool IsAbleToGenerateCSV()
        {
            if (TxtDocNo.Text.Length <= 0)
            {
                return false;
            }
            else
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 ");
                SQL.AppendLine("From (Select * From TblVoucherRequestPPNHdr Where DocNo = @Param) A ");
                SQL.AppendLine("Inner Join TblVoucherHdr B On A.VoucherRequestDocNo = B.VoucherRequestDocNo ");
                SQL.AppendLine("    And B.DocType = '15' ");
                SQL.AppendLine("    And A.FinalInd = 'Y' ");
                SQL.AppendLine("    And A.Status = 'A' ");
                SQL.AppendLine("    And A.CancelInd = 'N' ");
                SQL.AppendLine("    And B.CancelInd = 'N' ");
                SQL.AppendLine("Where Exists (Select 1 From TblParameter Where ParCode = 'DocTitle' And ParValue = 'IOK') ");
                SQL.AppendLine("Limit 1; ");

                return (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text) && 
                    mPPH23TaxGrpCode.Length > 0 && 
                    mPPNTaxGrpCode.Length > 0);
            }
        }

        private bool IsDocAlreadyFinal()
        {
            // sewaktu edit
            return Sm.IsDataExist(
                "Select 1 From TblVoucherRequestPPNHdr " +
                "Where DocNo=@Param And FinalInd='Y' And CancelInd='N' And Status In ('O', 'A');",
                TxtDocNo.Text,
                "This document already final."
                );
        }

        private bool IsDocNotYetFinal()
        {
            // sewaktu print
            return Sm.IsDataExist(
                "Select 1 From TblVoucherRequestPPNHdr " +
                "Where DocNo=@Param And FinalInd='N' And CancelInd='N';",
                TxtDocNo.Text,
                "This document not yet final."
                );
        }

        private bool IsDocAlreadyCancelled()
        {
            // sewaktu edit
            return Sm.IsDataExist(
                "Select 1 From TblVoucherRequestPPNHdr " +
                "Where DocNo=@Param And (CancelInd='Y' And Status='C');",
                TxtDocNo.Text,
                "This document already cancelled."
                );
        }

        private bool IsDocAlreadyProceedToVoucher()
        {
            return Sm.IsDataExist(
                "SELECT 1 " +
                "FROM TblVoucherRequestPPNHdr A "+
                "INNER JOIN TblVoucherRequestHdr B ON A.VoucherRequestDocNo = B.DocNo "+
                "INNER JOIN TblVoucherHdr C ON B.VoucherDocNo = C.DocNo AND C.CancelInd = 'N' "+
                "WHERE A.DocNo = @Param;",
                TxtDocNo.Text, 
                "This document already proceed to voucher."
                );
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            var IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            var IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest';"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.AppendLine("Select Concat('" + Yr + "','/', '" + Mth + "', '/', ");
            SQL.AppendLine("(Select IfNull( ");
            SQL.AppendLine("    (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.AppendLine("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
            //SQL.AppendLine("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb ");
            //SQL.AppendLine("From ( ");
            //SQL.AppendLine("    Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNoTemp ");
            SQL.AppendLine("    From TblVoucherRequestHdr ");
            SQL.AppendLine("    Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
            SQL.AppendLine("    Order By Substring(DocNo, 7, "+DocSeqNo+") Desc Limit 1) As temp ");
            SQL.AppendLine("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.AppendLine("), '0001' ");
            SQL.AppendLine(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            
            return Sm.GetValue(SQL.ToString());
        }
        
        internal string GetSelectedPPNIn()
        {
            var SQLIN = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 0).Length != 0)
                        SQLIN += "##" + Sm.GetGrdStr(Grd3, Row, 0) + "##";
            }
            return (SQLIN.Length == 0 ? "##XXX##" : SQLIN);
        }

        internal string GetSelectedPPNOut()
        {
            var SQLOut = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length != 0)
                        SQLOut += "##" + Sm.GetGrdStr(Grd2, Row, 0) + "##";
            }
            return (SQLOut.Length == 0 ? "##XXX##" : SQLOut);
        }

        internal void SetLueDeptCode(ref DXE.LookUpEdit Lue, string DeptCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");

            if (DeptCode.Length != 0)
                SQL.AppendLine("Where DeptCode=@DeptCode ");
            else
            {
                SQL.AppendLine("Where ActInd='Y' ");
                if (mIsFilterByDept)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (DeptCode.Length > 0) Sm.SetLue(LueDeptCode, DeptCode);
        }

        internal void SetLueBankAcCode(ref LookUpEdit Lue, string BankAcCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.BankAcCode As Col1, ");
            if (mBankAccountFormat == "1")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            else if (mBankAccountFormat == "2")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(IfNull(A.BankAcNm, ''), ' : ', A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(' [', B.BankName, ']') Else '' End ");
                SQL.AppendLine("    )) As Col2 ");
            }
            else
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            if (mIsVoucherBankAccountFilteredByCurrency)
            {
                string mcurrency = string.Empty;
                mcurrency = Sm.GetLue(LueCurCode);
                SQL.AppendLine("Left Join TblCurrency C On A.CurCode=C.CurCode where C.CurCode= '" + mcurrency + "'");
            }
            if (!mIsVoucherBankAccountFilteredByCurrency)
                SQL.AppendLine("Where 0=0 ");

            if (BankAcCode.Length != 0)
                SQL.AppendLine("And A.BankAcCode=@BankAcCode ");
            else
            {
                if (mIsVoucherRequestBankAccountFilteredByGrp)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select BankAcCode From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("And A.ActInd = 'Y' ");
            }

            SQL.AppendLine("Order By A.Sequence;");


            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BankAcCode.Length != 0) Sm.SetLue(Lue, BankAcCode);
        }

        private void GetParameter()
        {
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsVoucherRequestPPNApprovalBasedOnDept = Sm.GetParameterBoo("IsVoucherRequestPPNApprovalBasedOnDept");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mPPNTaxCode = Sm.GetParameter("PPNTaxCode");
            mIsPurchaseReturnInvoiceCanOnlyHave1Record = Sm.GetParameterBoo("IsPurchaseReturnInvoiceCanOnlyHave1Record");
            mIsSalesReturnInvoiceCanOnlyHave1Record = Sm.GetParameterBoo("IsSalesReturnInvoiceCanOnlyHave1Record");
            mVoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType");
            mPPH23TaxGrpCode = Sm.GetParameter("PPH23TaxGrpCode");
            mPPNTaxGrpCode = Sm.GetParameter("PPNTaxGrpCode");
            mPPH23TaxCode = Sm.GetParameter("PPH23TaxCode");
            mEmptaxwithholder = Sm.GetParameter("EmpTaxWithholder");
            mPurchaseInvoiceRawMaterialTaxGrpCode = Sm.GetParameter("PurchaseInvoiceRawMaterialTaxGrpCode");
            mIsVRVATUseBillingID = Sm.GetParameterBoo("IsVRVATUseBillingID");
            mIsVoucherBankAccountFilteredByCurrency = Sm.GetParameterBoo("IsVoucherBankAccountFilteredByCurrency");
            mIsVoucherRequestBankAccountFilteredByGrp = Sm.GetParameterBoo("IsVoucherRequestBankAccountFilteredByGrp");
            mBankAccountFormat = Sm.GetParameter("BankAccountFormat");
            mSalesInvoiceTaxCalculationFormula = Sm.GetParameter("SalesInvoiceTaxCalculationFormula");
            mIsFicoUseMultiProfitCenterFilter = Sm.GetParameterBoo("IsFicoUseMultiProfitCenterFilter");
            mIsVRVATFilteredByTaxGroup = Sm.GetParameterBoo("IsVRVATFilteredByTaxGroup");
            mIsPurchaseInvoiceUsePORevision = Sm.GetParameterBoo("IsPurchaseInvoiceUsePORevision");
            mPurchaseInvoiceTaxCalculationFormula = Sm.GetParameter("PurchaseInvoiceTaxCalculationFormula");
            if (mPurchaseInvoiceTaxCalculationFormula.Length == 0) mPurchaseInvoiceTaxCalculationFormula = "1";
            mDocTitle = Sm.GetParameter("DocTitle");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsVRVATTransactionValidatedbyClosingJournal = Sm.GetParameterBoo("IsVRVATTransactionValidatedbyClosingJournal");

                //Send CSV To Bank
                mIsVRVATSendtoBank = Sm.GetParameterBoo("IsVRVATSendtoBank");
            mIsCSVUseRealAmt = Sm.GetParameterBoo("IsCSVUseRealAmt");
            mPathToSaveExportedBRIVR = Sm.GetParameter("PathToSaveExportedBRIVR");
            mHostAddrForBRIVR = Sm.GetParameter("HostAddrForBRIVR");
            mSharedFolderForBRIVR = Sm.GetParameter("SharedFolderForBRIVR");
            mSharedFolderForBRIVRMPN = Sm.GetParameter("SharedFolderForBRIVRMPN");
            mUserNameForBRIVR = Sm.GetParameter("UserNameForBRIVR");
            mPasswordForBRIVR = Sm.GetParameter("PasswordForBRIVR");
            mPortForBRIVR = Sm.GetParameter("PortForBRIVR");
            mProtocolForBRIVR = Sm.GetParameter("ProtocolForBRIVR");
            mIsVRVATUseBillingID = Sm.GetParameterBoo("IsVRUseBillingID");
            mIsTransactionUseDetailApprovalInformation = Sm.GetParameterBoo("IsTransactionUseDetailApprovalInformation");
            //end Send CSV To Bank
        }

        private void GrdRemoveRow(iGrid Grd, KeyEventArgs e, SimpleButton Btn)
        {
            if (Btn.Enabled && e.KeyCode == Keys.Delete)
            {
                if (Grd.SelectedRows.Count > 0)
                {
                    if (Grd.Rows[Grd.Rows[Grd.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd.SelectedRows.Count - 1; Index >= 0; Index--)
                                Grd.Rows.RemoveAt(Grd.SelectedRows[Index].Index);
                            if (Grd.Rows.Count <= 0) Grd.Rows.Add();
                            ComputeAmt();
                        }
                    }
                }
            }
        }

        internal void ComputeAmt()
        {
            decimal Amt = 0m, PPnIn = 0m, PPnOut = 0m;

            try
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 4).Length != 0)
                    {
                        if (Sm.GetGrdStr(Grd3, Row, 2) == "1")
                            PPnIn += Sm.GetGrdDec(Grd3, Row, 11);
                        if (Sm.GetGrdStr(Grd3, Row, 2) == "2")
                            PPnIn -= Sm.GetGrdDec(Grd3, Row, 11);
                        if (Sm.GetGrdStr(Grd3, Row, 2) == "3")
                            PPnIn += Sm.GetGrdDec(Grd3, Row, 11);
                        if (Sm.GetGrdStr(Grd3, Row, 2) == "4")
                            PPnIn -= Sm.GetGrdDec(Grd3, Row, 11);
                    }
                }

                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 4).Length != 0)
                    {
                        if (Sm.GetGrdStr(Grd2, Row, 2) == "1")
                            PPnOut += Sm.GetGrdDec(Grd2, Row, 11);

                        if (Sm.GetGrdStr(Grd2, Row, 2) == "2")
                            PPnOut -= Sm.GetGrdDec(Grd2, Row, 11);

                        if (Sm.GetGrdStr(Grd2, Row, 2) == "3")
                            PPnOut += Sm.GetGrdDec(Grd2, Row, 11);

                        if (Sm.GetGrdStr(Grd2, Row, 2) == "4")
                            PPnOut += Sm.GetGrdDec(Grd2, Row, 11);
                    }
                }

                TxtTotalInAmt.EditValue = Sm.FormatNum(PPnIn, 0);
                TxtTotalOutAmt.EditValue = Sm.FormatNum(PPnOut, 0);
                
                Amt = PPnOut - PPnIn;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            if (Amt >= 0) mAcType = "C"; //kalau amount itu lebih dari atau sama dengan 0 ==> Credit
            if (Amt < 0) mAcType = "D"; //kalau amount itu kurang dari 0 (negatif hasilnya) ==> Debit
            TxtAmt.Text = Sm.FormatNum(Math.Abs(Amt), 0);
        }

        private string dec(Decimal a)
        {
            string numbEnd = " Koma ";
            string numbInd = string.Empty;

            string numb = a.ToString();
            int indexChar = numb.IndexOf(".") + 1;

            numb = numb.Substring(indexChar, (numb.Length - indexChar));

            string numbDec = numb;
            for (int i = 0; i < numbDec.Length; i++)
            {
                if (numbDec[i].ToString() == "0")
                {
                    numbInd = "Nol ";
                }
                else if (numbDec[i].ToString() == "1")
                {
                    numbInd = "Satu ";
                }
                else if (numbDec[i].ToString() == "2") 
                {
                    numbInd = "Dua ";
                }
                else if (numbDec[i].ToString() == "3")
                {
                    numbInd = "Tiga ";
                }
                else if (numbDec[i].ToString() == "4")
                {
                    numbInd = "Empat ";
                }
                else if (numbDec[i].ToString() == "5")
                {
                    numbInd = "Lima ";
                }
                else if (numbDec[i].ToString() == "6")
                {
                    numbInd = "Enam ";
                }
                else if (numbDec[i].ToString() == "7")
                {
                    numbInd = "Tujuh ";
                }
                else if (numbDec[i].ToString() == "8")
                {
                    numbInd = "Delapan ";
                }
                else
                {
                    numbInd = "Sembilan ";
                }

                numbEnd = numbEnd + numbInd;
            }

            numb = numbEnd;
            return numb;
        }

        private string RemoveChar(Decimal x)
        {
            string numb = x.ToString();
            int indexChar = numb.IndexOf(".");
            numb = numb.Substring(0, indexChar);
            return numb;
        }

        private void ExportCSVBRI(ref List<Bank> l)
        {
            try
            {
                // format : DocNo_CurrentDtTime() -> '/' replace '_'
                //var FileName = TxtDocNo.Text.Replace('/', '_') + "_" + Sm.ServerCurrentDateTime() + ".csv";
                var FileName = MeeRemark.Text + "_" + TxtDocNo.Text.Replace('/', '_') + ".csv";
                if (l.Count > 0)
                {
                    CreateCSVFileBRI(ref l, FileName);

                    if (IsBankBRIDataNotValid() || Sm.StdMsgYN("Question", "Bank BRI" + Environment.NewLine + "Do you want to send the exported data ?") == DialogResult.No) return;

                    SendDataBRI(mPathToSaveExportedBRIVR, FileName);

                    UpdateCSVInd(TxtDocNo.Text);

                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void CreateCSVFileBRI(ref List<Bank> l, string FileName)
        {
            StreamWriter sw = new StreamWriter(mPathToSaveExportedBRIVR + FileName, false, Encoding.GetEncoding(1252));
            sw.WriteLine("ProcCode,ID Billing,Nomor Rekening Debet,Referensi Unik,Value Date,Email");
            foreach (var x in l)
            {
                sw.WriteLine(
                     x.ProcCode + "|" + x.BillingID + "|" + x.DebitAccount + "|" + x.Reference + "|" + x.Date + "|" + x.Email
                );
            }
            sw.WriteLine("COUNT," + l.Count);
            sw.Close();
            Sm.StdMsg(mMsgType.Info, "Successfully export file to" + Environment.NewLine + mPathToSaveExportedBRIVR + FileName);
            l.Clear();
        }

        private void SendDataBRI(string sourceDrive, string FileName)
        {
            if (mProtocolForBRIVR.ToUpper() == "FTP")
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", sourceDrive + FileName));
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForBRIVR, mPortForBRIVR, ((mIsVRVATUseBillingID) ? mSharedFolderForBRIVRMPN : mSharedFolderForBRIVR), toUpload.Name));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(mUserNameForBRIVR, mPasswordForBRIVR);
                request.KeepAlive = false;
                request.EnableSsl = true;


                Stream ftpStream = request.GetRequestStream();

                FileStream file = File.OpenRead(string.Format(@"{0}", sourceDrive + FileName));

                int length = 1024;
                byte[] buffer = new byte[length];
                int bytesRead = 0;

                do
                {
                    bytesRead = file.Read(buffer, 0, length);
                    ftpStream.Write(buffer, 0, bytesRead);
                }
                while (bytesRead != 0);

                file.Close();
                ftpStream.Close();
            }
            else if (mProtocolForBRIVR.ToUpper() == "SFTP")
            {

                using (SftpClient client = new SftpClient(mHostAddrForBRIVR, Int32.Parse(mPortForBRIVR), mUserNameForBRIVR, mPasswordForBRIVR))
                {
                    string destinationPath = string.Format(@"{0}{1}{0}", "/", ((mIsVRVATUseBillingID) ? mSharedFolderForBRIVRMPN : mSharedFolderForBRIVR));
                    client.Connect();
                    client.ChangeDirectory(destinationPath);
                    using (FileStream fs = new FileStream(string.Format(@"{0}", sourceDrive + FileName), FileMode.Open))
                    {
                        client.BufferSize = 4 * 1024;
                        client.UploadFile(fs, Path.GetFileName(string.Format(@"{0}", sourceDrive + FileName)), null);
                    }
                    client.Dispose();
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "Unknown protocol.");
                return;
            }

            Sm.StdMsg(mMsgType.Info, "File uploaded to BRI Server.");
        }

        private bool IsBankBRIDataNotValid()
        {
            if (mProtocolForBRIVR.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Protocol (BRI) is empty.");
                return true;
            }

            if (mHostAddrForBRIVR.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address (BRI) is empty.");
                return true;
            }

            if (mIsVRVATUseBillingID && mSharedFolderForBRIVRMPN.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder (BRI) is empty.");
                return true;
            }

            if (mUserNameForBRIVR.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username (BRI) is empty.");
                return true;
            }

            if (mPortForBRIVR.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number (BRI) is empty.");
                return true;
            }

            return false;
        }

        private void UpdateCSVInd(string DocNo)
        {
            var cml = new List<MySqlCommand>();

            cml.Add(UpdateCSVInd2(DocNo));

            Sm.ExecCommands(cml);

            BtnCSV.Enabled = false;
            BtnCSV.Visible = false;
        }

        private MySqlCommand UpdateCSVInd2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestPPNHdr ");
            SQL.AppendLine("    Set CSVInd = 'Y', LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region Event

        #region Grid Event

        #region Grid 3

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueTaxGrpCode, "Tax"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmVoucherRequestPPNDlg(this));
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueTaxGrpCode, "Tax")) Sm.FormShowDialog(new FrmVoucherRequestPPNDlg(this));
            if(e.ColIndex == 15 && Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length > 0)
            {
                if(Sm.GetGrdStr(Grd3, e.RowIndex, 2) == "1")
                {
                    var fPurchaseInvoice = new FrmPurchaseInvoice(mMenuCode);
                    fPurchaseInvoice.Tag = mMenuCode;
                    fPurchaseInvoice.Text = "Purchase Invoice";
                    fPurchaseInvoice.WindowState = FormWindowState.Normal;
                    fPurchaseInvoice.StartPosition = FormStartPosition.CenterScreen;
                    fPurchaseInvoice.mDocNo = Sm.GetGrdStr(Grd3, e.RowIndex, 4);
                    fPurchaseInvoice.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 2) == "2")
                {
                    var fPurchaseReturnInvoice = new FrmPurchaseReturnInvoice(mMenuCode);
                    fPurchaseReturnInvoice.Tag = mMenuCode;
                    fPurchaseReturnInvoice.Text = "Purchase Return Invoice";
                    fPurchaseReturnInvoice.WindowState = FormWindowState.Normal;
                    fPurchaseReturnInvoice.StartPosition = FormStartPosition.CenterScreen;
                    fPurchaseReturnInvoice.mDocNo = Sm.GetGrdStr(Grd3, e.RowIndex, 4);
                    fPurchaseReturnInvoice.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 2) == "3")
                {
                    var fVoucherRequestTax = new FrmVoucherRequestTax(mMenuCode);
                    fVoucherRequestTax.Tag = mMenuCode;
                    fVoucherRequestTax.Text = "Voucher Request for Tax";
                    fVoucherRequestTax.WindowState = FormWindowState.Normal;
                    fVoucherRequestTax.StartPosition = FormStartPosition.CenterScreen;
                    fVoucherRequestTax.mDocNo = Sm.GetGrdStr(Grd3, e.RowIndex, 4);
                    fVoucherRequestTax.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 2) == "4")
                {
                    var fPurchaseInvoiceRawMaterial = new FrmPurchaseInvoiceRawMaterial(mMenuCode);
                    fPurchaseInvoiceRawMaterial.Tag = mMenuCode;
                    fPurchaseInvoiceRawMaterial.Text = "Purchase Invoice (Raw material)";
                    fPurchaseInvoiceRawMaterial.WindowState = FormWindowState.Normal;
                    fPurchaseInvoiceRawMaterial.StartPosition = FormStartPosition.CenterScreen;
                    fPurchaseInvoiceRawMaterial.mDocNo = Sm.GetGrdStr(Grd3, e.RowIndex, 4);
                    fPurchaseInvoiceRawMaterial.ShowDialog();
                }
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd3.ReadOnly)
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                ComputeAmt();
                SetNo(ref Grd3);
            }
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid 2

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueTaxGrpCode, "Tax"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmVoucherRequestPPNDlg2(this));
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueTaxGrpCode, "Tax")) Sm.FormShowDialog(new FrmVoucherRequestPPNDlg2(this));
            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd2, e.RowIndex, 4).Length > 0)
            {
                if(Sm.GetGrdStr(Grd2, e.RowIndex, 2) == "1")
                {
                    var fSalesInvoiceInvoice = new FrmSalesInvoice(mMenuCode);
                    fSalesInvoiceInvoice.Tag = mMenuCode;
                    fSalesInvoiceInvoice.Text = "Sales Invoice";
                    fSalesInvoiceInvoice.WindowState = FormWindowState.Normal;
                    fSalesInvoiceInvoice.StartPosition = FormStartPosition.CenterScreen;
                    fSalesInvoiceInvoice.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 4);
                    fSalesInvoiceInvoice.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd2, e.RowIndex, 2) == "2")
                {
                    var fSalesReturnInvoice = new FrmSalesReturnInvoice(mMenuCode);
                    fSalesReturnInvoice.Tag = mMenuCode;
                    fSalesReturnInvoice.Text = "Sales Return Invoice";
                    fSalesReturnInvoice.WindowState = FormWindowState.Normal;
                    fSalesReturnInvoice.StartPosition = FormStartPosition.CenterScreen;
                    fSalesReturnInvoice.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 4);
                    fSalesReturnInvoice.ShowDialog();
                }
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd2.ReadOnly)
            {
                Sm.GrdRemoveRow(Grd2, e, BtnSave);
                ComputeAmt();
                SetNo(ref Grd3);
            }
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue2(Sl.SetLueUserCode), string.Empty);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(SetLueDeptCode), string.Empty);
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(Sl.SetLueVoucherDocType));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd2, true);
                Sm.ClearGrd(Grd3, true);
                Sm.RefreshLookUpEdit(LueCurCode, new StdMtd.RefreshLue1(Sl.SetLueCurCode));
            }
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueBankCode, TxtGiroNo, DteDueDt
            });

                if (Sm.GetLue(LuePaymentType) == "C")
                {
                    Sm.SetControlReadOnly(LueBankCode, true);
                    Sm.SetControlReadOnly(TxtGiroNo, true);
                    Sm.SetControlReadOnly(DteDueDt, true);
                }
                else if (Sm.GetLue(LuePaymentType) == "B")
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, true);
                    Sm.SetControlReadOnly(DteDueDt, true);
                }
                else if (Sm.GetLue(LuePaymentType) == "G")
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, false);
                    Sm.SetControlReadOnly(DteDueDt, false);
                }
                else if (Sm.GetLue(LuePaymentType) == "K")
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, false);
                    Sm.SetControlReadOnly(DteDueDt, false);
                }
                else
                {
                    Sm.SetControlReadOnly(LueBankCode, true);
                    Sm.SetControlReadOnly(TxtGiroNo, true);
                    Sm.SetControlReadOnly(DteDueDt, true);
                }
            }
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void TxtGiroNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGiroNo);
        }

        private void LueTaxGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd2, true);
                Sm.ClearGrd(Grd3, true);
                Sm.RefreshLookUpEdit(LueTaxGrpCode, new Sm.RefreshLue1(Sl.SetLueTaxGrpCode));
            }
        }

        private void TxtTaxPeriod_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtTaxPeriod, 11);
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                if(!mSaveState)
                    if (DteDocDt.Text.Length > 0)
                        if (Decimal.Parse(TxtTaxPeriod.Text) <= 0)
                            TxtTaxPeriod.EditValue = Decimal.Parse(DteDocDt.Text.Substring(3, 2));
        }

        private void TxtTotalInAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtTotalInAmt, 0);
        }

        private void TxtTotalOutAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtTotalOutAmt, 0);
        }

        private void TxtBillingID_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtBillingID);
        }
        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue2(SetLueBankAcCode), string.Empty);
        }


        #endregion

        #region Button Click

        private void BtnPPH23_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsAbleToGenerateCSV())
                {
                    if (Sm.GetLue(LueTaxGrpCode) == mPPH23TaxGrpCode) ProcessCSVPPH23();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnPPNIn_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsAbleToGenerateCSV())
                {
                    if (Sm.GetLue(LueTaxGrpCode) == mPPNTaxGrpCode) ProcessCSVPPNIn();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnPPNOut_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsAbleToGenerateCSV())
                {
                    if (Sm.GetLue(LueTaxGrpCode) == mPPNTaxGrpCode) ProcessCSVPPNOut();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnPrintPPH23_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsAbleToGenerateCSV())
                {
                    if (Sm.GetLue(LueTaxGrpCode) == mPPH23TaxGrpCode)
                        Sm.FormShowDialog(new FrmVoucherRequestPPNDlg3(this, TxtDocNo.Text));
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnCSV_Click(object sender, EventArgs e)
        {
            if (mIsVRVATUseBillingID && TxtBillingID.Text.Length > 0)
            {
                if (!BtnSave.Enabled &&
                    TxtDocNo.Text.Length > 0 &&
                    mIsVRVATSendtoBank &&
                    !ChkCancelInd.Checked &&
                    Sm.GetValue("Select Status From TblVoucherRequestPPNHdr Where DocNo = '" + TxtDocNo.Text + "'") == "A" &&
                    !Sm.IsDataExist("Select 1 From TblVoucherHdr Where VoucherRequestDocNo = @Param", TxtDocNo.Text) &&
                    Sm.GetValue("Select CSVInd From TblVoucherRequestPPNHdr Where DocNo = '" + TxtDocNo.Text + "'") == "N"
                    )
                {
                    var lBank = new List<Bank>();

                    string mProcCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'ProcCodeForBRIBillingID';");
                    string mDebitAccount = Sm.GetValue("Select BankAcNo From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "'");
                    string mReference = string.Concat(TxtDocNo.Text, "_1");
                    string mDate = string.Concat(Sm.Left(Sm.GetDte(DteDocDt), 8), Sm.Right(Sm.ServerCurrentDateTime(), 4));

                    lBank.Add(new Bank()
                    {
                        ProcCode = mProcCode,
                        BillingID = TxtBillingID.Text,
                        DebitAccount = mDebitAccount,
                        Reference = mReference,
                        Date = mDate,
                        Email = "",
                        Count = 1,
                    });

                    if (lBank.Count > 0)
                    {
                        foreach (var x in lBank)
                        {
                            if (x.ProcCode.Length <= 0)
                            {
                                Sm.StdMsg(mMsgType.Warning, "Proc Code can't be empty.");
                                return;
                            }
                            if (x.BillingID.Length <= 0)
                            {
                                Sm.StdMsg(mMsgType.Warning, "Billing Id can't be empty.");
                                return;
                            }
                            if (x.DebitAccount.Length <= 0)
                            {
                                Sm.StdMsg(mMsgType.Warning, "Account# cant't be empty.");
                                return;
                            }
                            if (x.Reference.Length <= 0)
                            {
                                Sm.StdMsg(mMsgType.Warning, "Unique Reference cant't be empty.");
                                return;
                            }
                        }
                        ExportCSVBRI(ref lBank);

                    }
                }
            }
        }

        #endregion

        #endregion

        #region Report Class

        private class VoucherReqHdr
        {
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CancelInd { get; set; }
            public string DocType { get; set; }
            public string AcType { get; set; }
            public string VoucherDocNo { get; set; }
            public string VoucherDocDt { get; set; }
            public string BankAcc { get; set; }
            public string PaymentType { get; set; }
            public string GiroNo { get; set; }
            public string GiroBankName { get; set; }
            public string GiroDueDt { get; set; }
            public string PICName { get; set; }
            public decimal AmtHdr { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public string RemarkHdr { get; set; }
            public string CompanyLogo { get; set; }
            public string PrintBy { get; set; }
            public string CurCode { get; set; }
            public decimal DocEnclosure { get; set; }
            public string PaymentUser { get; set; }
            public string Department { get; set; }
            public string PrintFormat { get; set; }
            public string TaxGrpName { get; set; }
            
        }

        private class VoucherReqDtl
        {
            public string DNo { get; set; }
            public string DocTypeDesc { get; set; }
            public string DocNoIn { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
            public string Remark { get; set; }
        }

        private class VoucherReqDtl2
        {
            public string DNo { get; set; }
            public string DocTypeDesc { get; set; }
            public string DocNoOut { get; set; }
            public string DocDt { get; set; }
            public string Remark { get; set; }
            public decimal Amt { get; set; }
        }

        internal class PPH23
        {
            public string DocNo { set; get; }
            public string Date { get; set; }
            public string Vendor { get; set; }
            public string VdAddress { get; set; }
            public string TIN { get; set; }
            public string TaxInvoice { get; set; }
            public string TaxInvoiceDt { get; set; }
            public decimal PPhAmt { get; set; }
            public decimal TotalTax { get; set; }
            public string DocType { get; set; }
            public string KodeForm { get; set; }
            public string CurCode { get; set; }
            public string VdCode { get; set; }
            public string ServiceCode { get; set; }
            public string ServiceCode1 { get; set; }
            public string ServiceCode2 { get; set; }
            public string ServiceCode3 { get; set; }
            public string Terbilang { get; set; }
            public string PrintBy { get; set; }
            public string PrintPPH23Date { get; set; }
            public string ServiceName { get; set; }
            public string ServiceNote1 { get; set; }
            public string ServiceNote2 { get; set; }
            public string ServiceNote3 { get; set; }
            public string ServiceNote { get; set; }

            public string TaxCode1 { get; set; }
            public string Tax1 { get; set; }
            public string TaxInvoiceNo1 { get; set; }
            public string TaxInvoiceDt1 { get; set; }
            public string TaxCode2 { get; set; }
            public string Tax2 { get; set; }
            public string TaxInvoiceNo2 { get; set; }
            public string TaxInvoiceDt2 { get; set; }
            public string TaxCode3 { get; set; }
            public string Tax3 { get; set; }
            public string TaxInvoiceNo3 { get; set; }
            public string TaxInvoiceDt3 { get; set; }
            public string COATaxInd { get; set; }
            public decimal TaxRateAmt { get; set; }
        }

        internal class PPH23_1
        {
            public string Vendor { get; set; }
            public string VdAddress { get; set; }
            public string TIN { get; set; }
            public string TaxInvoice { get; set; }
            public string TaxInvoiceDt { get; set; }
            public decimal PPhAmt { get; set; }
            public decimal TotalTax { get; set; }
            public string Terbilang { get; set; }
            public string PrintBy { get; set; }
            public string PrintPPH23Date { get; set; }
            public string ServiceName { get; set; }
            public string ServiceCode { get; set; }
            public string ServiceNote { get; set; }
        }

        private class Dtl
        {
            public string DocNo { get; set; }
            public decimal Total { get; set; }
        }

        private class Dtl2
        {
            public string DocNo { get; set; }
            public string AmtType { get; set; }
            public decimal Amt { get; set; }
        }

        private class AmtCOA
        {
            public string DocNo { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class PPNIn
        {
            public string TaxInvoiceNo { get; set; }
            public string TaxInvoiceDt { get; set; }
            public string NPWP { get; set; }
            public string Name { get; set; }
            public string Address { get; set; }
            public decimal JmlDPP { get; set; }
            public decimal JmlPPN { get; set; }
        }

        private class PPNOut
        {
            public string TaxInvoiceNo { get; set; }
            public string TaxInvoiceDt { get; set; }
            public string NPWP { get; set; }
            public string Name { get; set; }
            public string DocNoOut { get; set; }
            public string DocDt { get; set; }
            public decimal JmlDPP { get; set; }
            public decimal JmlPPN { get; set; }
        }

        #endregion

        #region Class
        private class Bank
        {
            public string ProcCode { get; set; }
            public string BillingID { get; set; }
            public string DebitAccount { get; set; }
            public string Reference { get; set; }
            public string Date { get; set; }

            public string Email { get; set; }
            public int Count { get; set; }
        }
        #endregion

    }
}
