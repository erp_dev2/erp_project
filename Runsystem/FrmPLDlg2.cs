﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPLDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPL mFrmParent;
          private string mSQL = string.Empty, mCtCode = string.Empty;

        #endregion

        #region Constructor

          public FrmPLDlg2(FrmPL FrmParent, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

          #region Methods

          override protected void FrmLoad(object sender, EventArgs e)
          {
              try
              {
                  base.FrmLoad(sender, e);
                  this.Text = mFrmParent.Text;
                  Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                  SetGrd();
                  SetSQL();
                  Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -3);
              }
              catch (Exception Exc)
              {
                  Sm.ShowErrorMsg(Exc);
              }
          }

          override protected void SetSQL()
          {
              var SQL = new StringBuilder();

              SQL.AppendLine("Select * From ( ");
              SQL.AppendLine("  Select Distinct A.DocNo, A.DocDt, D.CtCode, F.CtName, ");
              SQL.AppendLine("  A.Account, A.InvoiceNo, A.SalesContractNo, LcNo, Issued1, issued2, Toorder1, Toorder2 ");
              SQL.AppendLine("  From TblPlHdr A ");
              SQL.AppendLine("  Inner Join TblPlDtl B On A.DocNo=B.DocNo ");
              SQL.AppendLine("  Inner Join TblSIHdr C On A.SIDocNo = C.DocNo");
              SQL.AppendLine("  Inner Join TblSp D On C.SPDocNo = D.DocNo ");
              SQL.AppendLine("  Inner Join TblItem E On B.ItCode=E.ItCode");
              SQL.AppendLine("  Inner Join TblCustomer F On D.CtCode= F.CtCode ");
              SQL.AppendLine(" ) T ");
              SQL.AppendLine("Where T.CtCode=@CtCode ");

              mSQL = SQL.ToString();
          }

          private void SetGrd()
          {
              Grd1.Cols.Count = 12;
              Grd1.FrozenArea.ColCount = 2;
              Grd1.ReadOnly = false;
              Sm.GrdHdrWithColWidth(
                      Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document Number", 
                        "Document"+Environment.NewLine+"Date",
                        "Customer",
                        "Account of Messrs",
                        "Invoice",
                        //6-10
                        "Sales Contract",
                        "LC Number",
                        "Issued by (1)",
                        "Issued by (2)",
                        "To Order (1)",
                        //11
                        "To Order (2)",
                    },
                      new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 200, 100, 100,  
                        
                        //6-10
                        100, 100, 100, 100, 100,   
                        
                        //11-15
                        100
                    }
                  );
              Sm.GrdFormatDate(Grd1, new int[] { 2 });
              Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 9, 11 });
              Sm.SetGrdProperty(Grd1, false);
          }

          override protected void HideInfoInGrd()
          {
              Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
              //Sm.SetGrdAutoSize(Grd1);
          }

          override protected void ShowData()
          {
              try
              {
                  Cursor.Current = Cursors.WaitCursor;
                  string Filter = " ";

                  var cm = new MySqlCommand();

                  Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                  Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                  Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "T.DocDt");

                  Sm.ShowDataInGrid(
                          ref Grd1, ref cm,
                          mSQL + Filter + " Order By T.DocNo ",
                          new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "CtName", "Account", "InvoiceNo", "SalesContractNo", 
                            
                            //6-10
                            "LCNo", "Issued1", "Issued2", "ToOrder1", "ToOrder2", 
                          
                        },
                          (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                          {
                              Grd1.Cells[Row, 0].Value = Row + 1;
                              Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                              Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                              Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                              Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                              Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                              Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                              Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                              Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                              Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                              Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                              Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                          }, true, false, false, false
                      );
              }
              catch (Exception Exc)
              {
                  Sm.ShowErrorMsg(Exc);
              }
              finally
              {
                  Sm.FocusGrd(Grd1, 0, 1);
                  Cursor.Current = Cursors.Default;
              }
          }

          override protected void ChooseData()
          {
              if (Sm.IsFindGridValid(Grd1, 1))
              {
                  mFrmParent.ShowPLHdr(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), 1);
                  this.Hide();
              }
          }

          #region Grid Method

         
          override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
          {
              Sm.SetGridNo(Grd1, 0, 1, true);
              //Grd1.Rows.AutoHeight();
          }

          override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
          {
          }

          override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
          {
              if (e.RowIndex >= 0) ChooseData();
          }


          #endregion



          #endregion

        #region event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }
        #endregion
    }
}
