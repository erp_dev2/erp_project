﻿#region Update
/*
    22/10/2020 [VIN/MGI] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingInventory : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptAgingInventory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-30);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
                DteDate.DateTime = Sm.ConvertDate(CurrentDate);
                SetLueBatchNo(ref LueBatchNo);
                Sl.SetLueOption(ref LueType, "AgingInventoryType");

                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Item Code",
                    "Item Name",
                    "Type",
                    "Document#", 
                    "Document"+Environment.NewLine+"Date",
                    
                    //6-10
                    "Batch#", 
                    "Quantity",
                    "UOM",
                    "Quantity",
                    "UOM",

                    //11-12
                    "Date", 
                    "Aging Item"+Environment.NewLine+"(Day)"

                },
                new int[] 
                {
                    //0
                    30, 

                    //1-5
                    150, 150, 130, 165, 100,  
                    
                    //6-10
                    150, 80, 80, 80, 80,

                    //11-12
                    100, 100

                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 9, 12 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 });
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdFormatDate(Grd1, new int[] { 5, 11 });
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.Type, A.OptCode, A.ItCode, A.ItName, A.BatchNo, ");
            SQL.AppendLine("A.Qty, A.Qty2, A.UOM, A.UOM2 ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, 'Item Mutation (Many on Many)' Type, '05' OptCode, B.ItCode, C.ItName,  B.BatchNo,  ");
            SQL.AppendLine("B.Qty, B.Qty2, C.InventoryUomCode UOM, C.InventoryUomCode2 UOM2 ");
            SQL.AppendLine("From TblMutationsHdr A ");
            SQL.AppendLine("Inner Join TblMutationsDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode  ");
            SQL.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode ");
            SQL.AppendLine("Left Join TblStockSummary E  ");
            SQL.AppendLine("On A.WhsCode=E.WhsCode  ");
            SQL.AppendLine("And B.Lot=E.Lot ");
            SQL.AppendLine("And B.Bin=E.Bin  ");
            SQL.AppendLine("And B.Source=E.Source ");
            SQL.AppendLine("AND A.CancelInd='N' ");

            SQL.AppendLine("UNION ALL ");

            SQL.AppendLine("Select C.DocNo, C.DocDt, 'Initial Stock' TYPE, '04' OptCode, A.ItCode, B.ItName,  A.BatchNo, ");
            SQL.AppendLine("A.Qty, A.Qty2, B.InventoryUomCode UOM, B.InventoryUomCode2 UOM2 ");
            SQL.AppendLine("From TblStockInitialDtl A  ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("INNER JOIN tblstockinitialhdr C ON A.DocNo=C.DocNo ");
            SQL.AppendLine("AND A.CancelInd='N' ");

            SQL.AppendLine("UNION ALL ");

            SQL.AppendLine("Select D.DocNo, D.DocDt, 'Shop Floor Controll' TYPE, '03' OptCode, A.ItCode, B.ItName, A.BatchNo, ");
            SQL.AppendLine("A.Qty, A.Qty2, B.PlanningUomCode UOM, B.PlanningUomCode2 UOM2 ");
            SQL.AppendLine("From TblShopFloorControlDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
            SQL.AppendLine("INNER JOIN tblshopfloorcontrolhdr D ON A.DocNo=D.DocNo ");
            SQL.AppendLine("AND D.CancelInd='N' ");

            SQL.AppendLine("UNION ALL ");

            SQL.AppendLine("Select H.DocNo, H.DocDt, 'Receiving Item From Vendor' TYPE, '01' OptCode, D.ItCode, E.ItName,  A.BatchNo, ");
            SQL.AppendLine("A.Qty, A.Qty2, E.InventoryUomCode, E.InventoryUomCode2 ");
            SQL.AppendLine("From TblRecvVdDtl A  ");
            SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo  ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("Inner Join TblPOHdr F On A.PODocNo=F.DocNo ");
            SQL.AppendLine("Left Join TblItemSubcategory G On E.ItScCode = G.ItScCode ");
            SQL.AppendLine("INNER JOIN tblrecvvdhdr H ON A.DocNo=H.DocNo AND SUBSTRING(H.DocNo, 10, 4) = 'RVPO' ");
            SQL.AppendLine("AND A.CancelInd='N'  ");

            SQL.AppendLine("UNION ALL ");

            SQL.AppendLine("Select D.DocNo, D.DocDt, 'Receiving Item From Vendor (Without PO)' TYPE, '02' OptCode, A.ItCode, B.ItName, A.BatchNo, ");
            SQL.AppendLine("A.Qty, A.Qty2, B.InventoryUomCode, B.InventoryUomCode2 ");
            SQL.AppendLine("From TblRecvVdDtl A  ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("left join TblItemSubcategory C On B.ItScCode = C.ItScCode  ");
            SQL.AppendLine("INNER JOIN tblrecvvdhdr D ON A.DocNo=D.DocNo AND SUBSTRING(D.DocNo, 10, 4) != 'RVPO' ");
            SQL.AppendLine("AND A.CancelInd='N' ");

            SQL.AppendLine(") A ");
            SQL.AppendLine("WHERE A.DocDt BETWEEN @DocDt1 AND @DocDt2 ");


            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDate, "Date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                
                
                var cm = new MySqlCommand();
               
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName", });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueBatchNo), "A.BatchNo", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "A.OptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.ItName ;",
                        new string[]
                        {
                            //0
                            "ItCode",

                            //1-5
                            "ItName", "Type", "DocNo", "DocDt", "BatchNo",

                            //6-9
                            "Qty", "UOM", "Qty2", "UOM2"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            
                        }, true, false, false, false
                    );
                CpDate();
                ComputeAging();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }


        #endregion 

        #region Additional Method

        private void CpDate()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 11].Value = DteDate.DateTime;
            }
        }

        internal void ComputeAging()
        {
            DateTime Date = Convert.ToDateTime(DteDate.Text);
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                DateTime DocDt = Convert.ToDateTime(Sm.GetGrdStr(Grd1, Row, 5));
                TimeSpan Aging = Date - DocDt;
                int days = (int)Math.Ceiling(Aging.TotalDays);

                Grd1.Cells[Row, 12].Value = Convert.ToDecimal(days);
            }

        }

        private void SetLueBatchNo(ref DXE.LookUpEdit Lue)
        {
            var cm = new MySqlCommand()
            {
                CommandText =

                    "SELECT batchno As Col1, batchno As Col2 " +
                    "FROM ( " +
                    "SELECT distinct batchno FROM tblmutationsdtl2 " +
                    "UNION ALL  " +
                    "SELECT distinct batchno FROM tblstockinitialdtl " +
                    "UNION ALL  " +
                    "SELECT distinct batchno FROM tblshopfloorcontroldtl " +
                    "UNION ALL  " +
                    "SELECT distinct batchno FROM tblrecvvddtl " +
                    ") A ; "
            };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        #endregion 

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event 

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;

        }
        private void DteDate_EditValueChanged(object sender, EventArgs e)
        {
            
        }
        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void LueBatchNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBatchNo, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Batch#");

        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }
        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue2(Sl.SetLueOption), "AgingInventoryType");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");

        }

        #endregion
    }
}
