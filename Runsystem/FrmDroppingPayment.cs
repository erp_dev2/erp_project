﻿#region Update
/*
    25/06/2019 [WED] new apps
    27/06/2019 [WED] Amount yg di update untuk Dropping Request Project adalah Direct Cost nya
    01/07/2019 [WED] Validasi jika payment melebihi outstanding payment yg ada
    08/08/2019 [TKG] menampilkan text di tombol sesuai dengan aplikasi yg akan ditampilkan.
    06/09/2019 [WED] approval berdasarkan site ataupun department
    28/01/2020 [WED/VIR] remuneration bisa di dropping payment, berdasarkan parameter IsDroppingRequestRemunerationProceedToPayment
    06/01/2023 [RDA/MNET] get parameter IsDroppingRequestUseType
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDroppingPayment : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        private string 
            mBankAccountFormat = string.Empty, 
            mVoucherDocType = "55",
            mDroppingPaymentDeptCode = string.Empty,
            mMainCurCode = string.Empty;
        internal bool 
            mIsFilterBySite = false,
            mIsDroppingRequestRemunerationProceedToPayment = false,
            mIsDroppingRequestUseType = false;
        internal FrmDroppingPaymentFind FrmFind;

        #endregion

        #region Constructor

        public FrmDroppingPayment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmDroppingPayment");

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);
                SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                SetLueBankAcCode(ref LueBankAcCode2, string.Empty);
                SetGrd();

                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                { 
                    "", 
                    "DRQDNo", "Resource / Category", "Request Amount", "Amount", "Remark"
                },
                new int[] 
                { 
                    20, 
                    20, 200, 120, 120, 250
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, TxtDRQDocNo, TxtPRJIDocNo,
                        LueDeptCode, LueMth, LueYr, LueBankAcCode, LueBankAcCode2, TxtAmt, MeeRemark
                    }, true);
                    BtnDRQDocNo.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    {
                        DteDocDt, LueMth, LueYr, LueBankAcCode, LueBankAcCode2, MeeRemark
                    }, false);
                    BtnDRQDocNo.Enabled = true;
                    Grd1.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtStatus, TxtDRQDocNo, TxtPRJIDocNo,
                LueDeptCode, LueMth, LueYr, LueBankAcCode, LueBankAcCode2, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #region Clear Grid

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDroppingPaymentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        { }

        #endregion

        #region Grid Method

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 3, 4 }, e);
                if (Sm.IsGrdColSelected(new int[] { 3, 4 }, e.ColIndex))
                {
                    ComputeTotalAmt();
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (!Grd1.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
                {
                    if (e.ColIndex == 0 && BtnSave.Enabled)
                    {
                        if (!Sm.IsTxtEmpty(TxtDRQDocNo, "Dropping Request#", false))
                        {
                            Sm.FormShowDialog(new FrmDroppingPaymentDlg2(this, TxtDRQDocNo.Text));
                        }
                    }
                }
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!Grd1.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
                {
                    if (e.ColIndex == 0 && BtnSave.Enabled)
                    {
                        if (!Sm.IsTxtEmpty(TxtDRQDocNo, "Dropping Request#", false))
                        {
                            Sm.FormShowDialog(new FrmDroppingPaymentDlg2(this, TxtDRQDocNo.Text));
                        }
                    }
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeTotalAmt();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty, VoucherRequestDocNo = string.Empty;

            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DroppingPayment", "TblDroppingPaymentHdr");
            VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDroppingPaymentHdr(DocNo, VoucherRequestDocNo));
            cml.Add(SaveVoucherRequestHdr(DocNo, VoucherRequestDocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SaveDroppingPaymentDtl(DocNo, Row));
                    cml.Add(SaveVoucherRequestDtl(DocNo, Row, VoucherRequestDocNo));
                }
            }

            cml.Add(UpdateDroppingRequestPaidInd(DocNo, "I"));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            UpdateExistingOutstandingAmt();

            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtDRQDocNo, "Dropping Request#", false) ||
                Sm.IsLueEmpty(LueBankAcCode, "Account From") ||
                Sm.IsLueEmpty(LueBankAcCode2, "Account To") ||
                IsDRQAlreadyFulfilled() ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsDRQAlreadyFulfilled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblDroppingRequestHdr ");
            SQL.AppendLine("Where DocNo = @Param1 ");
            SQL.AppendLine("And PaidInd = @Param2 ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDRQDocNo.Text, "F", string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "This dropping request already fulfilled.");
                TxtDRQDocNo.Focus();
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Resource / Category.");
                Sm.FocusGrd(Grd1, 0, 1);
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, i, 2, false, "Resource / Category is empty.")) { return true; }
                if (Sm.IsGrdValueEmpty(Grd1, i, 4, true, "Payment Amount is zero.")) { return true; }

                if (Sm.GetGrdDec(Grd1, i, 4) > Sm.GetGrdDec(Grd1, i, 3))
                {
                    var mMsgs = new StringBuilder();

                    mMsgs.AppendLine("Request Amount  : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, i, 3), 0));
                    mMsgs.AppendLine("Payment Amount : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, i, 4), 0));
                    mMsgs.AppendLine("Payment Amount shold be less than or equal to Request Amount ");

                    Sm.StdMsg(mMsgType.Warning, mMsgs.ToString());
                    Sm.FocusGrd(Grd1, i, 4);
                    return true;
                }
            }

            for (int r1 = 0; r1 < Grd1.Rows.Count - 1; r1++)
            {
                for (int r2 = (r1 + 1); r2 < Grd1.Rows.Count; r2++)
                {
                    if ((Sm.GetGrdStr(Grd1, r1, 1) == Sm.GetGrdStr(Grd1, r2, 1)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Duplicate resource / category found : " + Sm.GetGrdStr(Grd1, r2, 2));
                        Sm.FocusGrd(Grd1, r2, 2);
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveDroppingPaymentHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDroppingPaymentHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status, CancelInd, DRQDocNo, VoucherRequestDocNo, ");
            SQL.AppendLine("BankAcCode, BankAcCode2, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'O', 'N', @DRQDocNo, @VoucherRequestDocNo, ");
            SQL.AppendLine("@BankAcCode, @BankAcCode2, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='DroppingPayment' ");
                if (TxtPRJIDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("And T.SiteCode Is Not Null ");
                    SQL.AppendLine("And T.SiteCode In ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select X5.SiteCode ");
                    SQL.AppendLine("    From TblProjectImplementationHdr X1 ");
                    SQL.AppendLine("    Inner Join TblSOContractRevisionHdr X2 On X1.SOContractDocNo = X2.DocNo ");
                    SQL.AppendLine("        And X1.DocNo = @PRJIDocNo ");
                    SQL.AppendLine("    Inner Join TblSOContractHdr X3 On X2.SOCDocNo = X3.DocNo ");
                    SQL.AppendLine("    Inner Join TblBOQHdr X4 On X3.BOQDocNo = X4.DocNo ");
                    SQL.AppendLine("    Inner Join TblLOPHdr X5 On X4.LOPDocNo = X5.DocNo ");
                    SQL.AppendLine(") ");
                }
                if (Sm.GetLue(LueDeptCode).Length > 0)
                {
                    SQL.AppendLine("And T.DeptCode Is Not Null ");
                    SQL.AppendLine("And T.DeptCode = @DeptCode ");
                }
                SQL.AppendLine("; ");
            }

            SQL.AppendLine("Update TblDroppingPaymentHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And DocType = 'DroppingPayment' ");
            SQL.AppendLine(");     ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DRQDocNo", TxtDRQDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", TxtPRJIDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetDecValue(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankAcCode2", Sm.GetLue(LueBankAcCode2));
            return cm;
        }

        private MySqlCommand SaveDroppingPaymentDtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblDroppingPaymentDtl(DocNo, DNo, DRQDocNo, DRQDNo, Amt, Remark, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @DRQDocNo, @DRQDNo, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DRQDocNo", TxtDRQDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DRQDNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status, CancelInd, DeptCode, DocType, ");
            SQL.AppendLine("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, BankCode, ");
            SQL.AppendLine("PIC, CurCode, CurCode2, Amt, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@VoucherRequestDocNo, @DocDt, 'O', 'N', @DeptCode, @DocType, ");
            SQL.AppendLine("@AcType, @BankAcCode, @AcType2, @BankAcCode2, @PaymentType, @BankCode, ");
            SQL.AppendLine("@PIC, @CurCode, @CurCode2, @Amt, @ExcRate, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblVoucherRequestHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @VoucherRequestDocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And DocType = 'DroppingPayment' ");
            SQL.AppendLine(");     ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode).Length > 0 ? Sm.GetLue(LueDeptCode) : mDroppingPaymentDeptCode);
            Sm.CmParam<String>(ref cm, "@DocType", mVoucherDocType);
            Sm.CmParam<String>(ref cm, "@AcType", "C");
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@AcType2", "D");
            Sm.CmParam<String>(ref cm, "@BankAcCode2", Sm.GetLue(LueBankAcCode2));
            Sm.CmParam<String>(ref cm, "@PaymentType", "B");
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetValue("Select BankCode From TblBankAccount Where BankAcCode = @Param Limit 1; ", Sm.GetLue(LueBankAcCode)));
            Sm.CmParam<String>(ref cm, "@PIC", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode2", mMainCurCode);
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", 1m);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetDecValue(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string DocNo, int Row, string VoucherRequestDocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@VoucherRequestDocNo, @DNo, @Description, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Description", string.Concat("Dropping Payment #" + DocNo + " (" + Sm.GetGrdStr(Grd1, Row, 2) + ")"));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateDroppingRequestPaidInd(string DocNo, string mStateInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDroppingRequestDtl A ");
            SQL.AppendLine("Inner Join TblDroppingRequestHdr B On A.DocNo = B.DocNo And B.PRJIDocNo Is Not Null ");
            SQL.AppendLine("Inner Join TblDroppingPaymentDtl C On A.DocNo = C.DRQDocNo And A.DNo = C.DRQDNo ");
            SQL.AppendLine("Set ");
            if (mStateInd == "I") SQL.AppendLine("    A.OutstandingAmt = (A.OutstandingAmt - C.Amt), ");
            if (mStateInd == "E") SQL.AppendLine("    A.OutstandingAmt = (A.OutstandingAmt + C.Amt), ");
            SQL.AppendLine("    A.LastUpBy = @CreateBy, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where C.DocNo = @DocNo ");
            SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");

            SQL.AppendLine("Update TblDroppingRequestDtl2 A ");
            SQL.AppendLine("Inner Join TblDroppingRequestHdr B On A.DocNo = B.DocNo And B.DeptCode Is Not Null ");
            SQL.AppendLine("Inner Join TblDroppingPaymentDtl C On A.DocNo = C.DRQDocNo And A.DNo = C.DRQDNo ");
            SQL.AppendLine("Set ");
            if (mStateInd == "I") SQL.AppendLine("    A.OutstandingAmt = (A.OutstandingAmt - C.Amt), ");
            if (mStateInd == "E") SQL.AppendLine("    A.OutstandingAmt = (A.OutstandingAmt + C.Amt), ");
            SQL.AppendLine("    A.LastUpBy = @CreateBy, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where C.DocNo = @DocNo ");
            SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");




            SQL.AppendLine("Update TblDroppingRequestDtl T1 ");
            SQL.AppendLine("Inner Join TblDroppingRequestHdr T2 On T1.DocNo = T2.DocNo And T2.PRJIDocNo Is Not Null ");
            SQL.AppendLine("Inner Join TblDroppingPaymentDtl T3 On T1.DocNo = T3.DRQDocNo And T1.DNo = T3.DRQDNo And T3.DocNo = @DocNo ");
            SQL.AppendLine("Set T1.PaidInd = 'O', T1.LastUpBy = @CreateBy, T1.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where T1.OutstandingAmt = T1.DirectCostAmt And T1.RemunerationAmt = 0 ");
            SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");

            if (mIsDroppingRequestRemunerationProceedToPayment)
            {
                SQL.AppendLine("Update TblDroppingRequestDtl T1 ");
                SQL.AppendLine("Inner Join TblDroppingRequestHdr T2 On T1.DocNo = T2.DocNo And T2.PRJIDocNo Is Not Null ");
                SQL.AppendLine("Inner Join TblDroppingPaymentDtl T3 On T1.DocNo = T3.DRQDocNo And T1.DNo = T3.DRQDNo And T3.DocNo = @DocNo ");
                SQL.AppendLine("Set T1.PaidInd = 'O', T1.LastUpBy = @CreateBy, T1.LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where T1.OutstandingAmt = T1.RemunerationAmt And T1.DirectCostAmt = 0 ");
                SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");
            }

            SQL.AppendLine("Update TblDroppingRequestDtl T1 ");
            SQL.AppendLine("Inner Join TblDroppingRequestHdr T2 On T1.DocNo = T2.DocNo And T2.PRJIDocNo Is Not Null ");
            SQL.AppendLine("Inner Join TblDroppingPaymentDtl T3 On T1.DocNo = T3.DRQDocNo And T1.DNo = T3.DRQDNo And T3.DocNo = @DocNo ");
            SQL.AppendLine("Set T1.PaidInd = 'P', T1.LastUpBy = @CreateBy, T1.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where T1.OutstandingAmt != T1.DirectCostAmt And T1.OutstandingAmt > 0 And T1.RemunerationAmt = 0 ");
            SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");

            if (mIsDroppingRequestRemunerationProceedToPayment)
            {
                SQL.AppendLine("Update TblDroppingRequestDtl T1 ");
                SQL.AppendLine("Inner Join TblDroppingRequestHdr T2 On T1.DocNo = T2.DocNo And T2.PRJIDocNo Is Not Null ");
                SQL.AppendLine("Inner Join TblDroppingPaymentDtl T3 On T1.DocNo = T3.DRQDocNo And T1.DNo = T3.DRQDNo And T3.DocNo = @DocNo ");
                SQL.AppendLine("Set T1.PaidInd = 'P', T1.LastUpBy = @CreateBy, T1.LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where T1.OutstandingAmt != T1.RemunerationAmt And T1.OutstandingAmt > 0 And T1.DirectCostAmt = 0 ");
                SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");
            }

            SQL.AppendLine("Update TblDroppingRequestDtl T1 ");
            SQL.AppendLine("Inner Join TblDroppingRequestHdr T2 On T1.DocNo = T2.DocNo And T2.PRJIDocNo Is Not Null ");
            SQL.AppendLine("Inner Join TblDroppingPaymentDtl T3 On T1.DocNo = T3.DRQDocNo And T1.DNo = T3.DRQDNo And T3.DocNo = @DocNo ");
            SQL.AppendLine("Set T1.PaidInd = 'F', T1.LastUpBy = @CreateBy, T1.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where T1.OutstandingAmt <= 0 ");
            SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");

            SQL.AppendLine("Update TblDroppingRequestDtl2 T1  ");
            SQL.AppendLine("Inner Join TblDroppingRequestHdr T2 On T1.DocNo = T2.DocNo And T2.DeptCode Is Not Null ");
            SQL.AppendLine("Inner Join TblDroppingPaymentDtl T3 On T1.DocNo = T3.DRQDocNo And T1.DNo = T3.DRQDNo And T3.DocNo = @DocNo  ");
            SQL.AppendLine("Set T1.PaidInd = 'O', T1.LastUpBy = @CreateBy, T1.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where T1.OutstandingAmt = T1.Amt ");
            SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");

            SQL.AppendLine("Update TblDroppingRequestDtl2 T1  ");
            SQL.AppendLine("Inner Join TblDroppingRequestHdr T2 On T1.DocNo = T2.DocNo And T2.DeptCode Is Not Null ");
            SQL.AppendLine("Inner Join TblDroppingPaymentDtl T3 On T1.DocNo = T3.DRQDocNo And T1.DNo = T3.DRQDNo And T3.DocNo = @DocNo  ");
            SQL.AppendLine("Set T1.PaidInd = 'P', T1.LastUpBy = @CreateBy, T1.LastUpDt = CurrentDateTime()  ");
            SQL.AppendLine("Where T1.OutstandingAmt != T1.Amt And T1.OutstandingAmt > 0 ");
            SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");

            SQL.AppendLine("Update TblDroppingRequestDtl2 T1  ");
            SQL.AppendLine("Inner Join TblDroppingRequestHdr T2 On T1.DocNo = T2.DocNo And T2.DeptCode Is Not Null ");
            SQL.AppendLine("Inner Join TblDroppingPaymentDtl T3 On T1.DocNo = T3.DRQDocNo And T1.DNo = T3.DRQDNo And T3.DocNo = @DocNo  ");
            SQL.AppendLine("Set T1.PaidInd = 'F', T1.LastUpBy = @CreateBy, T1.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where T1.OutstandingAmt <= 0 ");
            SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");



            SQL.AppendLine("Update TblDroppingRequestHdr ");
            SQL.AppendLine("Set PaidInd = 'O', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DRQDocNo ");
            SQL.AppendLine("And PRJIDocNo Is Not Null ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDroppingRequestDtl X1 ");
            SQL.AppendLine("    Where X1.DocNo = @DRQDocNo ");
            SQL.AppendLine("    And X1.PaidInd In ('P', 'F') ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");

            SQL.AppendLine("Update TblDroppingRequestHdr ");
            SQL.AppendLine("Set PaidInd = 'P', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DRQDocNo ");
            SQL.AppendLine("And PRJIDocNo Is Not Null ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDroppingRequestDtl X1 ");
            SQL.AppendLine("    Where X1.DocNo = @DRQDocNo ");
            SQL.AppendLine("    And X1.PaidInd In ('P', 'F') ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");

            SQL.AppendLine("Update TblDroppingRequestHdr ");
            SQL.AppendLine("Set PaidInd = 'F', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DRQDocNo ");
            SQL.AppendLine("And PRJIDocNo Is Not Null ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDroppingRequestDtl X1 ");
            SQL.AppendLine("    Where X1.DocNo = @DRQDocNo ");
            SQL.AppendLine("    And X1.PaidInd In ('P', 'O') ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");

            SQL.AppendLine("Update TblDroppingRequestHdr ");
            SQL.AppendLine("Set PaidInd = 'O', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DRQDocNo ");
            SQL.AppendLine("And DeptCode Is Not Null ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDroppingRequestDtl2 X1 ");
            SQL.AppendLine("    Where X1.DocNo = @DRQDocNo ");
            SQL.AppendLine("    And X1.PaidInd In ('P', 'F') ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");

            SQL.AppendLine("Update TblDroppingRequestHdr ");
            SQL.AppendLine("Set PaidInd = 'P', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DRQDocNo ");
            SQL.AppendLine("And DeptCode Is Not Null ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDroppingRequestDtl2 X1 ");
            SQL.AppendLine("    Where X1.DocNo = @DRQDocNo ");
            SQL.AppendLine("    And X1.PaidInd In ('P', 'F') ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");

            SQL.AppendLine("Update TblDroppingRequestHdr ");
            SQL.AppendLine("Set PaidInd = 'F', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DRQDocNo ");
            SQL.AppendLine("And DeptCode Is Not Null ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDroppingRequestDtl2 X1 ");
            SQL.AppendLine("    Where X1.DocNo = @DRQDocNo ");
            SQL.AppendLine("    And X1.PaidInd In ('P', 'O') ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists (Select 1 From TblDroppingPaymentHdr Where DocNo = @DocNo And Status = 'A'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DRQDocNo", TxtDRQDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditDroppingPaymentHdr());
            cml.Add(UpdateDroppingRequestPaidInd(TxtDocNo.Text, "E"));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToVoucher()
                ;
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyProcessedToVoucher()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DocNo ");
            SQL.AppendLine("From TblDroppingPaymentHdr A ");
            SQL.AppendLine("Inner Join TblVoucherHdr B On A.VoucherRequestDocNo = B.VoucherRequestDocNo ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            SQL.AppendLine("Where A.DocNo = @Param Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed to voucher #" + Sm.GetValue(SQL.ToString(), TxtDocNo.Text));
                return true;
            }

            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblDroppingPaymentHdr ");
            SQL.AppendLine("Where (CancelInd='Y' Or Status = 'C') And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditDroppingPaymentHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDroppingPaymentHdr Set ");
            SQL.AppendLine("    Amt = @Amt, CancelReason=@CancelReason, CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status <> 'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelReason = @CancelReason, CancelInd = @CancelInd, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @VoucherRequestDocNo And CancelInd = 'N' And Status <> 'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowDroppingPaymentHdr(DocNo);
                ShowDroppingPaymentDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDroppingPaymentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc, ");
            SQL.AppendLine("A.CancelReason, A.CancelInd, A.DRQDocNo, B.PRJIDocNo, B.DeptCode, B.Mth, B.Yr, ");
            SQL.AppendLine("A.BankAcCode, A.BankAcCode2, A.Amt, A.Remark, A.VoucherRequestDocNo, C.VoucherDocNo ");
            SQL.AppendLine("From TblDroppingPaymentHdr A ");
            SQL.AppendLine("Inner Join TblDroppingRequestHdr B On A.DRQDocNo = B.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr C On A.VoucherRequestDocNo = C.DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "StatusDesc", "CancelReason", "CancelInd", "DRQDocNo",
                    
                    //6-10
                    "PRJIDocNo", "DeptCode", "Mth", "Yr", "BankAcCode", 
                    
                    //11-15
                    "BankAcCode2", "Amt", "Remark", "VoucherRequestDocNo", "VoucherDocNo"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                    TxtDRQDocNo.EditValue = Sm.DrStr(dr, c[5]);
                    TxtPRJIDocNo.EditValue = Sm.DrStr(dr, c[6]);
                    Sl.SetLueDeptCode(ref LueDeptCode);
                    Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[7]));
                    Sm.SetLue(LueMth, Sm.DrStr(dr, c[8]));
                    Sm.SetLue(LueYr, Sm.DrStr(dr, c[9]));
                    Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[10]));
                    Sm.SetLue(LueBankAcCode2, Sm.DrStr(dr, c[11]));
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[13]);
                    TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[14]);
                    TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[15]);
                }, true
            );
        }

        private void ShowDroppingPaymentDtl(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select T.* From ( ");
	        SQLDtl.AppendLine("    Select A.DNo, A.DRQDNo, E.ItName ResourceName, (C.DirectCostAmt + C.RemunerationAmt) RequestAmt, A.Amt, A.Remark ");
	        SQLDtl.AppendLine("    From TblDroppingPaymentDtl A ");
	        SQLDtl.AppendLine("    Inner Join TblDroppingRequestHdr B On A.DRQDocNo = B.DocNo And B.PRJIDocNo Is Not Null And A.DocNo = @DocNo ");
	        SQLDtl.AppendLine("    Inner Join TblDroppingRequestDtl C On B.DocNo = C.DocNo And A.DRQDNo = C.DNo ");
            SQLDtl.AppendLine("    Inner Join TblProjectImplementationRBPHdr C1 On C.PRBPDocNo = C1.DocNo ");
	        SQLDtl.AppendLine("    Inner Join TblProjectImplementationDtl2 D On C1.PRJIDocNo = D.DocNo And C1.ResourceItCode = D.ResourceItCode ");
	        SQLDtl.AppendLine("    Inner Join TblItem E On D.ResourceItCode = E.ItCode ");
	        SQLDtl.AppendLine("    Union All ");
	        SQLDtl.AppendLine("    Select X1.DNo, X1.DRQDNo, X4.BCName ResourceName, X3.Amt RequestAmt, X1.Amt, X1.Remark ");
	        SQLDtl.AppendLine("    From TblDroppingPaymentDtl X1 ");
	        SQLDtl.AppendLine("    Inner Join TblDroppingRequestHdr X2 On X1.DRQDocNo = X2.DocNo And X2.DeptCode Is Not Null And X1.DocNo = @DocNo ");
	        SQLDtl.AppendLine("    Inner Join TblDroppingRequestDtl2 X3 On X2.DocNo = X3.DocNo And X1.DRQDNo = X3.DNo ");
	        SQLDtl.AppendLine("    Inner Join TblBudgetCategory X4 On X3.BCCode = X4.BCCode ");
            SQLDtl.AppendLine(") T ");
            SQLDtl.AppendLine("Order By T.DNo; ");

            var cm1 = new MySqlCommand();
            Sm.CmParam<String>(ref cm1, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm1, SQLDtl.ToString(),
                new string[] 
                { 
                    "DRQDNo", 
                    "ResourceName", "RequestAmt", "Amt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });
        }

        #endregion

        #region Additional Method

        private void UpdateExistingOutstandingAmt()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DocNo, DNo, OutstandingAmt ");
            SQL.AppendLine("From TblDroppingRequestDtl ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDRQDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "OutstandingAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        for (int x = 0; x < Grd1.Rows.Count; x++)
                        {
                            if (Sm.GetGrdStr(Grd1, x, 1).Length > 0)
                            {
                                if (Sm.GetGrdStr(Grd1, x, 1) == Sm.DrStr(dr, c[1]))
                                {
                                    Grd1.Cells[x, 3].Value = Sm.DrDec(dr, c[2]);
                                    break;
                                }
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        internal string GetSelectedDRQDNo()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void SetLueMth(ref DXE.LookUpEdit Lue)
        {
            //Sl.SetLookUpEdit(Lue, new string[] { null, "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" });

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Mth As Col1, Mth As Col2 ");
            SQL.AppendLine("From TblDroppingRequestHdr ");
            SQL.AppendLine("Order By Mth; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void SetLueYr(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Yr As Col1, Yr As Col2 ");
            SQL.AppendLine("From TblDroppingRequestHdr ");
            SQL.AppendLine("Order By Yr; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        internal void ComputeTotalAmt()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd1, row, 1).Length != 0)
                {
                    Total += Sm.GetGrdDec(Grd1, row, 4);
                }
            }
            TxtAmt.EditValue = Sm.FormatNum(Total, 0);
        }

        private bool IsNeedApproval()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType = 'DroppingPayment' ");
            if (TxtPRJIDocNo.Text.Length > 0)
            {
                SQL.AppendLine("And SiteCode Is Not Null ");
                SQL.AppendLine("And SiteCode In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select X5.SiteCode ");
                SQL.AppendLine("    From TblProjectImplementationHdr X1 ");
                SQL.AppendLine("    Inner Join TblSOContractRevisionHdr X2 On X1.SOContractDocNo = X2.DocNo ");
                SQL.AppendLine("        And X1.DocNo = @Param1 ");
                SQL.AppendLine("    Inner Join TblSOContractHdr X3 On X2.SOCDocNo = X3.DocNo ");
                SQL.AppendLine("    Inner Join TblBOQHdr X4 On X3.BOQDocNo = X4.DocNo ");
                SQL.AppendLine("    Inner Join TblLOPHdr X5 On X4.LOPDocNo = X5.DocNo ");
                SQL.AppendLine(") ");
            }
            if (Sm.GetLue(LueDeptCode).Length > 0)
            {
                SQL.AppendLine("And DeptCode Is Not Null ");
                SQL.AppendLine("And DeptCode = @Param2 ");
            }
            SQL.AppendLine("Limit 1; ");

            return Sm.IsDataExist(SQL.ToString(), TxtPRJIDocNo.Text, Sm.GetLue(LueDeptCode), string.Empty);
        }

        internal void SetLueBankAcCode(ref DXE.LookUpEdit Lue, string BankAcCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (mBankAccountFormat == "1")
            {
                SQL.AppendLine("Select A.BankAcCode As Col1, ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
                SQL.AppendLine("From TblBankAccount A ");
                SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            }
            else
            {
                SQL.AppendLine("Select A.BankAcCode As Col1, ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                SQL.AppendLine(")) As Col2 ");
                SQL.AppendLine("From TblBankAccount A ");
                SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            }

            if (BankAcCode.Length != 0)
                SQL.AppendLine("Where A.BankAcCode=@BankAcCode ");

            SQL.AppendLine("Order By A.Sequence;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BankAcCode.Length != 0) Sm.SetLue(Lue, BankAcCode);
        }

        private void GetParameter()
        {
            mBankAccountFormat = Sm.GetParameter("BankAccountFormat");
            mDroppingPaymentDeptCode = Sm.GetParameter("DroppingPaymentDeptCode");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsDroppingRequestRemunerationProceedToPayment = Sm.GetParameterBoo("IsDroppingRequestRemunerationProceedToPayment");
            mIsDroppingRequestUseType = Sm.GetParameterBoo("IsDroppingRequestUseType");
        }

        #endregion

        #endregion

        #region Events

        #region Button Events

        private void BtnDRQDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmDroppingPaymentDlg(this));
            }
        }

        private void BtnDRQDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDRQDocNo, "Dropping Request#", false))
            {
                var f = new FrmDroppingRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmDroppingRequest");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDRQDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnPRJIDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project Implementation#", false))
            {
                var f = new FrmProjectImplementation(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmProjectImplementation");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPRJIDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher Request#", false))
            {
                var f = new FrmVoucherRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmVoucherRequest");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmVoucher");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMth, new Sm.RefreshLue1(SetLueMth));
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueYr, new Sm.RefreshLue1(SetLueYr));
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue2(SetLueBankAcCode), string.Empty);
        }

        private void LueBankAcCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankAcCode2, new Sm.RefreshLue2(SetLueBankAcCode), string.Empty);
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmt, 0);
        }

        #endregion

        #endregion

    }
}
