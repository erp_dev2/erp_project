﻿#region Update
/*
 * 29/01/2020 [RF] OT Fleksibel
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPaySlipParticular1Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPaySlipParticular1 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPaySlipParticular1Dlg(FrmPaySlipParticular1 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PayrunCode, A.PayrunName, B.Deptname, C.SiteName, ");
            SQL.AppendLine("D.OptDesc As EmpSystemType, E.OptDesc As PayrunPeriode, A.StartDt, A.EndDt, A.DeptCode ");
            SQL.AppendLine("From TblPayrun A");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode");
            SQL.AppendLine("left Join TblSite C On A.SiteCode = C.SiteCode");
            SQL.AppendLine("Left Join TblOption D On A.SystemType = D.OptCode And D.Optcat = 'EmpSystemType'");
            SQL.AppendLine("Left Join TblOption E On A.PayrunPeriod = E.OptCode And E.OptCat = 'PayrunPeriod' ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='C' ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Payrun Code",
                        "Payrun Name",
                        "Department",
                        "Site",
                        "Employee System"+Environment.NewLine+"Type",
                        
                        //6-9
                        "Payrun"+Environment.NewLine+"Periode",
                        "Start Date",
                        "End Date",
                        "DeptCode"
                    },
                    new int[]
                    {
                        40, 
                        100, 250, 130, 150, 120,
                        120, 80, 80, 0
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.PayrunCode", "A.PayrunName" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By Left(payruncode, 6) Desc, PayrunName, StartDt;",
                        new string[] 
                        { 
                            "PayrunCode",
                            "PayrunName", "Deptname", "SiteName", "EmpSystemType", "PayrunPeriode", 
                            "StartDt", "EndDt", "DeptCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                int Row = Grd1.CurRow.Index;
                mFrmParent.TxtPayrunCode.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtPayrunName.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.TxtDeptName.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                mFrmParent.TxtSiteName.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.TxtPayrunPeriod.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                Sm.SetDte(mFrmParent.DteStartDt, Sm.GetGrdDate(Grd1, Row, 7));
                Sm.SetDte(mFrmParent.DteEndDt, Sm.GetGrdDate(Grd1, Row, 8));
                Sm.ClearGrd(mFrmParent.Grd1, true);
                mFrmParent.mDeptCode = Sm.GetGrdStr(Grd1, Row, 9);
                mFrmParent.SetLueAGCode(ref mFrmParent.LueAGCode, mFrmParent.mDeptCode);
                this.Close();
            }

        }

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender); 
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion
    }
}
