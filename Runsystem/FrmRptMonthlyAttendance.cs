﻿#region Update
/*
    15/04/2018 [TKG] Monthly Attendance Reporting
    18/08/2018 [TKG] tambah total working days
    09/03/2020 [WED/SIER] tambah informasi total keterlambatan (IsRptMonthlyAttendanceShowTotalLate) dan total pulan cepat (IsRptMonthlyAttendanceShowTotalEarlyGoHome)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthlyAttendance : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mWork = "WORK",
            mOff = "OFF";
        private int mCol = 15;
        private bool mIsRptMonthlyAttendanceShowTotalLate = false, mIsRptMonthlyAttendanceShowTotalEarlyGoHome = false;

        #endregion

        #region Constructor

        public FrmRptMonthlyAttendance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sl.SetLuePayrunCode(ref LuePayrunCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        private void SetGrd()
        {
            Grd1.Cols.Count = mCol;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Payrun"+Environment.NewLine+"Code",
                    "Payrun Name",
                    "Employee's"+Environment.NewLine+"Code", 
                    "Employee's Name",
                    "Old"+Environment.NewLine+"Code",

                    //6-10
                    "Position",
                    "Department",
                    "Site",
                    "Leave With"+Environment.NewLine+"Meal+Transport",
                    "Leave Without"+Environment.NewLine+"Meal+Transport",

                    //11-14
                    "Total"+Environment.NewLine+"Leave",
                    "Total"+Environment.NewLine+"Working Days",
                    "Total"+Environment.NewLine+"Late for Work",
                    "Total"+Environment.NewLine+"Early Go Home"
                },
                new int[] 
                {
                    //0
                    50, 

                    //1-5
                    80, 200, 80, 200, 80, 
                    
                    //6-10
                    200, 200, 200, 100, 100,

                    //11-14
                    80, 100, 180, 180
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 12 }, 2);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 5, 6 }, false);
            if (!mIsRptMonthlyAttendanceShowTotalLate) Sm.GrdColInvisible(Grd1, new int[] { 13 });
            if (!mIsRptMonthlyAttendanceShowTotalEarlyGoHome) Sm.GrdColInvisible(Grd1, new int[] { 14 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 5, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            Grd1.Cols.Count = mCol;
            if (Sm.IsLueEmpty(LuePayrunCode, "Payrun")) return;

            var l1 = new List<Data1>();
            var l2 = new List<Data2>();
            var l3 = new List<Data3>();
            var l4 = new List<Data4>();
            var l5 = new List<Data5>();

            try
            {
                string 
                    PayrunCode = Sm.GetLue(LuePayrunCode),
                    PayrunName = string.Empty,
                    StartDt = string.Empty,
                    EndDt = string.Empty,
                    DeptName = string.Empty,
                    SiteName = string.Empty;

                Process1(PayrunCode, ref PayrunName, ref StartDt, ref EndDt, ref DeptName, ref SiteName); // Get Payrun start date and end date
                Process2(PayrunCode, StartDt, EndDt, ref l1); // Set grid column
                Process3(PayrunCode, ref l2); // Get Employee Info
                Process4(ref l3); // Get Leave Info
                Process5(PayrunCode, ref l4); // Get Payroll Process 2 Info
                if (mIsRptMonthlyAttendanceShowTotalEarlyGoHome || mIsRptMonthlyAttendanceShowTotalLate)
                {
                    // Get monthly total late for work and total early go home
                    Process6(ref l5);
                }
                Process7(PayrunCode, PayrunName, DeptName, SiteName, ref l1, ref l2, ref l3, ref l4, ref l5); // Process all info
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l1.Clear();
                l2.Clear();
                l3.Clear();
                l4.Clear();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Methods

        private void GetParameter()
        {
            mIsRptMonthlyAttendanceShowTotalEarlyGoHome = Sm.GetParameterBoo("IsRptMonthlyAttendanceShowTotalEarlyGoHome");
            mIsRptMonthlyAttendanceShowTotalLate = Sm.GetParameterBoo("IsRptMonthlyAttendanceShowTotalLate");
        }

        private void Process1(string PayrunCode, ref string PayrunName, ref string StartDt, ref string EndDt, ref string DeptName, ref string SiteName)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = 
                    "Select A.PayrunName, A.StartDt, A.EndDt, B.DeptName, C.SiteName " +
                    "From TblPayrun A, TblDepartment B, TblSite C " +
                    "Where A.DeptCode=B.DeptCode " +
                    "And A.SiteCode=C.SiteCode " +
                    "And A.PayrunCode=@PayrunCode;";
                Sm.CmParam<string>(ref cm, "@PayrunCode", PayrunCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "PayrunName", "StartDt", "EndDt", "DeptName", "SiteName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        PayrunName = Sm.DrStr(dr, c[0]);
                        StartDt = Sm.DrStr(dr, c[1]);
                        EndDt = Sm.DrStr(dr, c[2]);
                        DeptName = Sm.DrStr(dr, c[3]);
                        SiteName = Sm.DrStr(dr, c[4]);
                    }
                }
                dr.Close();
            }
        }

        private void Process2(string PayrunCode, string StartDt, string EndDt, ref List<Data1> l)
        {
            string ColDt = string.Empty, ColDtDesc = string.Empty;
            DateTime Dt1 = new DateTime(
                   Int32.Parse(StartDt.Substring(0, 4)),
                   Int32.Parse(StartDt.Substring(4, 2)),
                   Int32.Parse(StartDt.Substring(6, 2)),
                   0, 0, 0
                   );
            DateTime Dt2 = new DateTime(
                Int32.Parse(EndDt.Substring(0, 4)),
                Int32.Parse(EndDt.Substring(4, 2)),
                Int32.Parse(EndDt.Substring(6, 2)),
                0, 0, 0
                );

            var TotalDays = (Dt2 - Dt1).Days;
            Grd1.Cols.Count = mCol + TotalDays+1;

            ColDt =
                   Dt1.Year.ToString() +
                   ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                   ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2);

            ColDtDesc =
                ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2) + "/" +
                ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) + "/" +
                Dt1.Year.ToString();

            l.Add(new Data1()
            {
                Col = mCol,
                Dt = ColDt
            });

            Grd1.Cols[mCol].Text = ColDtDesc;
            Grd1.Cols[mCol].Width = 80;
            Grd1.Header.Cells[0, mCol].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Cols[mCol].CellStyle.ValueType = typeof(String);
            Grd1.Cols[mCol].CellStyle.TextAlign = iGContentAlignment.MiddleLeft;

            for (int i = 1; i <= TotalDays; i++)
            {
                Dt1 = Dt1.AddDays(1);
                ColDt =
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2);

                ColDtDesc =
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2) + "/" +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) + "/" +
                    Dt1.Year.ToString();

                l.Add(new Data1()
                {
                    Col = mCol+i,
                    Dt = ColDt
                });

                Grd1.Cols[mCol + i].Text = ColDtDesc;
                Grd1.Cols[mCol + i].Width = 80;
                Grd1.Header.Cells[0, mCol + i].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Cols[mCol + i].CellStyle.ValueType = typeof(String);
                Grd1.Cols[mCol + i].CellStyle.TextAlign = iGContentAlignment.MiddleLeft;
            }
        }

        private void Process3(string PayrunCode, ref List<Data2> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName ");
            SQL.AppendLine("From TblPayrollProcess1 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Where A.PayrunCode=@PayrunCode;");

            Sm.CmParam<string>(ref cm, "@PayrunCode", PayrunCode);
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    "EmpCode", 
                    "EmpName", "EmpCodeOld", "PosName" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Data2()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            EmpName = Sm.DrStr(dr, c[1]),
                            EmpCodeOld = Sm.DrStr(dr, c[2]),
                            PosName = Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process4(ref List<Data3> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.LeaveCode, A.ShortName, ");
            SQL.AppendLine("Case When Find_In_Set(IfNull(A.LeaveCode, ''), IfNull(B.ParValue, '')) Then 'Y' Else 'N' End As MealTransportInd ");
            SQL.AppendLine("From TblLeave A ");
            SQL.AppendLine("Left Join TblParameter B On B.ParCode Is Not Null And B.ParCode='LeaveCodeGetMealTransport' ");
            SQL.AppendLine("Order By LeaveCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "LeaveCode", "ShortName", "MealTransportInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Data3()
                        {
                            LeaveCode = Sm.DrStr(dr, c[0]),
                            ShortName = Sm.DrStr(dr, c[1]),
                            MealTransportInd = Sm.DrStr(dr, c[2])=="Y"
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process5(string PayrunCode, ref List<Data4> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select EmpCode, Dt, ProcessInd, WSHolidayInd, LeaveCode ");
            SQL.AppendLine("From TblPayrollProcess2 ");
            SQL.AppendLine("Where PayrunCode=@PayrunCode ");
            SQL.AppendLine("Order By EmpCode, Dt;");
            
            Sm.CmParam<string>(ref cm, "@PayrunCode", PayrunCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    "EmpCode", 
                    "Dt", "ProcessInd", "WSHolidayInd", "LeaveCode" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Data4()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            ProcessInd = Sm.DrStr(dr, c[2]) == "Y",
                            WSHolidayInd = Sm.DrStr(dr, c[3]) == "Y",
                            LeaveCode = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process6(ref List<Data5> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT T.EmpCode, ");
            //-- If(T.Late = 'OFF', 'OFF', If(T.Late <= 0, '0', T.Late)) Late,
            //-- If(T.EarlyGoHome = 'OFF', 'OFF', If(T.EarlyGoHome >= 0, '0', T.EarlyGoHome * -1)) EarlyGoHome
            SQL.AppendLine("SUM(T.Late) Late, SUM(T.EarlyGoHome) EarlyGoHome ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT EmpCode, ");
            //-- If(ActualIn IS NULL, 'OFF', TIMESTAMPDIFF(minute, DATE_FORMAT(CONCAT(Dt, WSIn1, '00'), '%Y-%m-%d %H:%i'), DATE_FORMAT(CONCAT(ActualIn, '00'), '%Y-%m-%d %H:%i'))) Late,
            //-- If(ActualOut IS NULL, 'OFF', TIMESTAMPDIFF(minute, DATE_FORMAT(CONCAT(Dt, WSOut1, '00'), '%Y-%m-%d %H:%i'), DATE_FORMAT(CONCAT(ActualOut, '00'), '%Y-%m-%d %H:%i'))) EarlyGoHome
            SQL.AppendLine("    If( ");
            SQL.AppendLine("        TIMESTAMPDIFF(minute, DATE_FORMAT(CONCAT(Dt, WSIn1, '00'), '%Y-%m-%d %H:%i'), DATE_FORMAT(CONCAT(ActualIn, '00'), '%Y-%m-%d %H:%i')) <= 0, ");
		    SQL.AppendLine("          0, ");
		    SQL.AppendLine("          TIMESTAMPDIFF(minute, DATE_FORMAT(CONCAT(Dt, WSIn1, '00'), '%Y-%m-%d %H:%i'), DATE_FORMAT(CONCAT(ActualIn, '00'), '%Y-%m-%d %H:%i')) ");
	        SQL.AppendLine("     ) Late, ");
	        SQL.AppendLine("     If( ");
            SQL.AppendLine("        TIMESTAMPDIFF(minute, DATE_FORMAT(CONCAT(Dt, WSOut1, '00'), '%Y-%m-%d %H:%i'), DATE_FORMAT(CONCAT(ActualOut, '00'), '%Y-%m-%d %H:%i')) >= 0, ");
		    SQL.AppendLine("          0, ");
		    SQL.AppendLine("          TIMESTAMPDIFF(minute, DATE_FORMAT(CONCAT(Dt, WSOut1, '00'), '%Y-%m-%d %H:%i'), DATE_FORMAT(CONCAT(ActualOut, '00'), '%Y-%m-%d %H:%i')) * -1 ");
            SQL.AppendLine("    ) EarlyGoHome ");
            SQL.AppendLine("    FROM TblPayrollProcess2 ");
            SQL.AppendLine("    WHERE PayrunCode = @PayrunCode ");
            SQL.AppendLine("    AND WSIn1 IS NOT NULL AND WSOut1 IS NOT NULL ");
            SQL.AppendLine("    AND ActualIn IS NOT NULL AND ActualOut IS NOT NULL ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("GROUP BY T.EmpCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Late", "EarlyGoHome" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Data5()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            TotalLate = string.Concat((Math.Floor(Sm.DrDec(dr, c[1]) / 60m)).ToString(), " hour(s), ", (Sm.DrDec(dr, c[1]) % 60m).ToString(), " minute(s)"),
                            TotalEarlyGoHome = string.Concat((Math.Floor(Sm.DrDec(dr, c[2]) / 60m)).ToString(), " hour(s), ", (Sm.DrDec(dr, c[2]) % 60m).ToString(), " minute(s)")
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process7(string PayrunCode, string PayrunName, string DeptName, string SiteName, 
            ref List<Data1> l1, ref List<Data2> l2, ref List<Data3> l3, ref List<Data4> l4, ref List<Data5> l5)
        {
            int r = 0, Col = -1;
            string EmpCode = string.Empty;
            decimal Total1 = 0; // LeaveWithTransportMeal
            decimal Total2 = 0; // LeaveWithoutTransportMeal
            decimal Total3 = 0; // Working Days
            
            Grd1.BeginUpdate();
            Grd1.Rows.Count = l2.Count;
            for (int i = 0; i < l2.Count; i++)
            {
                Grd1.Cells[r, 0].Value = r+1;
                Grd1.Cells[r, 1].Value = PayrunCode;
                Grd1.Cells[r, 2].Value = PayrunName;
                Grd1.Cells[r, 3].Value = l2[i].EmpCode;
                Grd1.Cells[r, 4].Value = l2[i].EmpName;
                Grd1.Cells[r, 5].Value = l2[i].EmpCodeOld;
                Grd1.Cells[r, 6].Value = l2[i].PosName;
                Grd1.Cells[r, 7].Value = DeptName;
                Grd1.Cells[r, 8].Value = SiteName;
                Grd1.Cells[r, 13].Value = string.Empty;
                Grd1.Cells[r, 14].Value = string.Empty;

                EmpCode = l2[i].EmpCode;
                Col = -1;
                Total1 = 0m;
                Total2 = 0m;
                Total3 = 0m;
                foreach(var j in l4
                    .Where(w=>Sm.CompareStr(w.EmpCode, EmpCode))
                    .OrderBy(o=>o.Dt)
                    )
                {
                    foreach(var k in l1.Where(w=>Sm.CompareStr(w.Dt, j.Dt)))
                        Col = k.Col;
                    if (j.LeaveCode.Length > 0)
                    {
                        foreach (var l in l3.Where(w => Sm.CompareStr(w.LeaveCode, j.LeaveCode)))
                        {
                            Grd1.Cells[r, Col].Value = l.ShortName;
                            if (l.MealTransportInd)
                                Total1 += 1;
                            else
                                Total2 += 1;
                        }
                    }
                    else
                    {
                        if (j.ProcessInd)
                        {
                            if (j.WSHolidayInd)
                                Grd1.Cells[r, Col].Value = mOff;
                            else
                            {
                                Grd1.Cells[r, Col].Value = mWork;
                                Total3 += 1;
                            }
                        }
                    }
                }

                if (l5.Count > 0)
                {
                    foreach (var p in l5)
                    {
                        if (EmpCode == p.EmpCode)
                        {
                            Grd1.Cells[r, 13].Value = p.TotalLate;
                            Grd1.Cells[r, 14].Value = p.TotalEarlyGoHome;
                        }
                    }
                }

                Grd1.Cells[r, 9].Value = Total1;
                Grd1.Cells[r, 10].Value = Total2;
                Grd1.Cells[r, 11].Value = Total1+Total2;
                Grd1.Cells[r, 12].Value = Total3;
                r++;
            }

            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10, 11, 12 });

            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LuePayrunCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrunCode, new Sm.RefreshLue1(Sl.SetLuePayrunCode));
        }

        #endregion

        #endregion

        #region Class

        private class Data1
        {
            public int Col { get; set; }
            public string Dt { get; set; }
        }

        private class Data2
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string PosName { get; set; }
            public string DeptName { get; set; }
            public string SiteName { get; set; }
        }

        private class Data3
        {
            public string LeaveCode { get; set; }
            public string ShortName { get; set; }
            public bool MealTransportInd { get; set; }
        }

        private class Data4
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public bool ProcessInd { get; set; }
            public bool WSHolidayInd { get; set; }
            public string LeaveCode { get; set; }
        }

        private class Data5
        {
            public string EmpCode { get; set; }
            public string TotalLate { get; set; }
            public string TotalEarlyGoHome { get; set; }
        }

        #endregion
    }
}
