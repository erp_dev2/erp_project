﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBonusDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmBonus mFrmParent;
        private string
            mSQL = string.Empty,
            mBonusDt = string.Empty,
            mSiteCode = string.Empty;
        private decimal
            mFunctionalExpenses = 0m,
            mFunctionalExpensesMaxAmt = 0m;
        private List<TI> mlTI = null;
        private List<NTI> mlNTI = null;

        #endregion

        #region Constructor

        public FrmBonusDlg(FrmBonus FrmParent, string BonusDt, string SiteCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mBonusDt = BonusDt;
            mSiteCode = SiteCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                GetData();
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Employee's"+Environment.NewLine+"Code",
                    "Employee's Name",
                    "Position",
                    "Department",                        

                    //6-10
                    "Status",
                    "Permanent Date",
                    "Resign",
                    "Bank",
                    "Bank"+Environment.NewLine+"Account#",
                    
                    //11-15                        
                    "Value",
                    "Tax",
                    "Amount",
                    "PTKP",
                    "NPWP",

                    //16-18
                    "WorkPeriod",
                    "ResignPeriod",
                    "DeptCode"
                  },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    20,  80, 200, 150, 150,   
                    
                    //6-10
                    100, 100, 100, 150, 150, 

                    //11-15
                    120, 120, 130, 120, 120,

                    //16-17
                    0, 0, 0
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13, 16 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 14, 15, 16, 17, 18 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select EmpCode, EmpName, PosName, DeptName, EmploymentStatusDesc, ");
            SQL.AppendLine("LeaveStartDt, ResignDt, BankName, BankAcNo, ");
            SQL.AppendLine("Value, Tax, Amt, ");
            //SQL.AppendLine("Case When Prorate<12.00 Then Prorate/12.00 Else 1.00 End * Amt As Amt, ");
            SQL.AppendLine("PTKP, NPWP, If(WorkPeriod > 12, 12, WorkPeriod) WorkPeriod, ResignPeriod, DeptCode ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, C.DeptName, I.OptDesc As EmploymentStatusDesc, ");
            SQL.AppendLine("A.LeaveStartDt, A.ResignDt, G.BankName, A.BankAcNo, ");
            SQL.AppendLine("IfNull(D.Amt, 0.00)+IfNull(E.Amt, 0.00) As Value, ");
            SQL.AppendLine("0.00 As Tax, ");
            SQL.AppendLine("0.00 As Amt, ");
            SQL.AppendLine("A.PTKP, A.NPWP, ");
            SQL.AppendLine("TIMESTAMPDIFF(Month, ");
            SQL.AppendLine("Concat(Left(A.LeaveStartDt, 4), '-', substring(A.LeaveStartDt, 5, 2), '-', '01') ");
            SQL.AppendLine(", @BonusDt) As WorkPeriod, ");
            SQL.AppendLine("TIMESTAMPDIFF(Month, ");
            SQL.AppendLine("IfNull(Concat(Left(A.ResignDt, 4), '-', substring(A.ResignDt, 5, 2), '-', '01'), '') ");
            SQL.AppendLine(", @BonusDt) As ResignPeriod, ");
            SQL.AppendLine("A.DeptCode ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select T2.EmpCode, T2.Amt ");
	        SQL.AppendLine("    From ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select EmpCode, Max(StartDt) StartDt ");
		    SQL.AppendLine("        From TblEmployeeSalary ");
		    //SQL.AppendLine("        Where ActInd = 'Y' ");
		    SQL.AppendLine("        Where StartDt<=@CurrentDate ");
		    SQL.AppendLine("        Group By EmpCode ");
	        SQL.AppendLine("    ) T1 ");
	        SQL.AppendLine("    Inner Join TblEmployeeSalary T2 On T1.EmpCode = T2.EmpCode And T1.StartDt = T2.StartDt ");
            SQL.AppendLine(") D On A.EmpCode = D.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.EmpCode, Sum(T1.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction T1 ");
            SQL.AppendLine("    Inner Join TblEmployee T2 on T1.EmpCode=T2.EmpCode And T2.SiteCode=@SiteCode ");
            SQL.AppendLine("    Where T1.StartDt<=@CurrentDate ");
            SQL.AppendLine("    And @CurrentDate<=T1.EndDt ");
            SQL.AppendLine("    And Find_In_Set(");
            SQL.AppendLine("    T1.ADCode, ");
            SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='ADCodeBonus'), '') ");
            SQL.AppendLine("    )");
            SQL.AppendLine("    Group By T1.EmpCode ");
            SQL.AppendLine(") E On A.EmpCode=E.EmpCode ");
            SQL.AppendLine("Left Join TblBank G On A.BankCode=G.BankCode ");
            SQL.AppendLine("Left Join TblOption I On A.EmploymentStatus=I.OptCode And I.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Where A.SiteCode = @SiteCode ");
            SQL.AppendLine("And A.LeaveStartDt Is Not Null ");
            SQL.AppendLine("And A.EmploymentStatus = '1' ");
            SQL.AppendLine("And Not Find_In_Set(A.EmpCode, @GetSelectedEmployee) ");
            
            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 14, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@CurrentDate", mBonusDt);
                Sm.CmParam<String>(ref cm, "@BonusDt", 
                    string.Concat(
                        Sm.Left(mBonusDt, 4), "-",
                        mBonusDt.Substring(4, 2), "-",
                        "01"
                        ));
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParam<String>(ref cm, "@GetSelectedEmployee", mFrmParent.GetSelectedEmployee());
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + ") T Order By EmpName;",
                        new string[]
                        {
                            //0
                            "EmpCode",
                            
                            //1-5
                            "EmpName", "PosName", "DeptName", "EmploymentStatusDesc", "LeaveStartDt", 
                            
                            //6-10
                            "ResignDt", "BankName", "BankAcNo", "Value", "Tax", 
                            
                            //11-15
                            "Amt", "PTKP", "NPWP", "WorkPeriod", "ResignPeriod", 
                            
                            //16
                            "DeptCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                        }, true, false, false, false
                    );
                ComputeValueTaxAmt();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 10);
                        //Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 11);
                        mFrmParent.Grd1.Cells[Row1, 9].Value = Decimal.Parse(mFrmParent.TxtCoefficient.Text) * Sm.GetGrdDec(Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 12);
                        //Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 13);
                        mFrmParent.Grd1.Cells[Row1, 11].Value = (Decimal.Parse(mFrmParent.TxtCoefficient.Text) * Sm.GetGrdDec(Grd1, Row2, 11)) + Sm.GetGrdDec(Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 18);
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 12 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }
            mFrmParent.ComputeAmt();
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 employee.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            var EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int r = 0; r < mFrmParent.Grd1.Rows.Count - 1; r++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, r, 1), EmpCode)) return true;
            return false;
        }

        private void ComputeValueTaxAmt()
        {
            var l = new List<Bonus>();
            try
            {
                string EmpCodeTemp = string.Empty;

                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCodeTemp = Sm.GetGrdStr(Grd1, r, 2);
                    if (EmpCodeTemp.Length > 0)
                    {
                        l.Add(new Bonus()
                        {
                            EmpCode = EmpCodeTemp,
                            Value = Sm.GetGrdDec(Grd1, r, 11),
                            Tax = Sm.GetGrdDec(Grd1, r, 12),
                            Amt = Sm.GetGrdDec(Grd1, r, 13),
                            PTKP = Sm.GetGrdStr(Grd1, r, 14),
                            NPWP = Sm.GetGrdStr(Grd1, r, 15),
                            WorkPeriod = Sm.GetGrdDec(Grd1, r, 16),
                            ResignPeriod = Sm.GetGrdStr(Grd1, r, 17)
                        });
                    }
                }

                if (l.Count>0)
                {
                    for(int i =0;i<l.Count;i++)
                    {
                        ProcessValue(ref l, i); // nentuin nilai l[i].Value yang bener dan masukkin nilai l[i].Value ke Grd nya Value
                        for(int x = 0; x < Grd1.Rows.Count; x++)
                        {
                            if (l[i].EmpCode == Sm.GetGrdStr(Grd1, x, 2))
                            {
                                Grd1.Cells[x, 11].Value = l[i].Value;
                                break;
                            }
                        }

                        if (l[i].NPWP.Length != 0 && l[i].PTKP.Length != 0)
                        {
                            ProcessTax(ref l, i, ref mlNTI, ref mlTI);
                            for (int r = 0; r < Grd1.Rows.Count; r++)
                            {
                                if (Sm.CompareStr(l[i].EmpCode, Sm.GetGrdStr(Grd1, r, 2)))
                                {
                                    Grd1.Cells[r, 12].Value = l[i].Tax;
                                    break;
                                }
                            }
                        }

                        for (int r = 0; r < Grd1.Rows.Count; r++)
                        {
                            if(l[i].EmpCode == Sm.GetGrdStr(Grd1, r, 2))
                            {
                                Grd1.Cells[r, 13].Value = Sm.GetGrdDec(Grd1, r, 11) + Sm.GetGrdDec(Grd1, r, 12);
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                l.Clear();
            }
        }

        private void ProcessValue(ref List<Bonus> l, int i)
        {
            bool mIsProrated = false;
            if(l[i].WorkPeriod < 12)
            {
                mIsProrated = true;
            }
            else
            {
                if (l[i].ResignPeriod.Length > 0)
                {
                    if (Decimal.Parse(l[i].ResignPeriod) > 0)
                        mIsProrated = true;
                }
            }

            decimal mResignPeriod = 0m;
            if (l[i].ResignPeriod.Length > 0) mResignPeriod = Decimal.Parse(l[i].ResignPeriod);

            if (mIsProrated) l[i].Value = ((l[i].WorkPeriod - mResignPeriod) / 12) * l[i].Value;
        }

        private void ProcessTax(ref List<Bonus> l, int i, ref List<NTI> lNTI, ref List<TI> lTI)
        {
            //decimal
            //    TotalImbalan = 0m,
            //    TotalPengurangan = 0m,
            //    GajiBersihSebulan = 0m,
            //    //GajiBersihDisetahunkan = 0m,
            //    NTIAmt = 0m,
            //    PKP = 0m,
            //    TaxTemp = PKP,
            //    Amt2Temp = 0m,
            //    //PPH21Setahun = 0m,
            //    PPH21Sebulan = 0m,

            //    TotalImbalan2 = 0m,
            //    TotalPengurangan2 = 0m,
            //    GajiBersihSebulan2 = 0m,
            //    //GajiBersihDisetahunkan = 0m,
            //    NTIAmt2 = 0m,
            //    PKP2 = 0m,
            //    TaxTemp2 = PKP,
            //    Amt2Temp2 = 0m,
            //    //PPH21Setahun = 0m,
            //    PPH21Sebulan2 = 0m;

            decimal
                    Tax = 0m,
                    NTIAmt = 0m,
                    FunctionalExpenses = 0m,
                    Amt = 0m;

            string NTI = l[i].PTKP;

            Amt = l[i].Value;

            FunctionalExpenses = Amt * mFunctionalExpenses * 0.01m;
            if (FunctionalExpenses > mFunctionalExpensesMaxAmt)
                FunctionalExpenses = mFunctionalExpensesMaxAmt;

            Tax = Amt - FunctionalExpenses;

            Tax = Tax * 12m;

            foreach (var x in lNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (Tax > NTIAmt)
                Tax -= NTIAmt;
            else
                Tax = 0m;

            Tax = Tax % 1000 >= 500 ? Tax + 1000 - Tax % 1000 : Tax - Tax % 1000;

            if (Tax > 0 && lTI.Count > 0)
            {
                decimal TaxTemp = Tax, Amt2Temp = 0m;
                Tax = 0m;

                foreach (TI o in lTI.OrderBy(x => x.SeqNo))
                {
                    if (TaxTemp > 0m)
                    {
                        if (TaxTemp <= (o.Amt2 - Amt2Temp))
                        {
                            Tax += (TaxTemp * o.TaxRate / 100);
                            TaxTemp = 0m;
                        }
                        else
                        {
                            Tax += ((o.Amt2 - Amt2Temp) * o.TaxRate / 100);
                            TaxTemp -= (o.Amt2 - Amt2Temp);
                        }
                    }
                    Amt2Temp = o.Amt2;
                }
            }

            Tax = Tax / 12m;
            if (Tax <= 0) Tax = 0m;
            l[i].Tax = decimal.Truncate(Tax);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mFunctionalExpenses = Sm.GetParameterDec("FunctionalExpenses");
            mFunctionalExpensesMaxAmt = Sm.GetParameterDec("FunctionalExpensesMaxAmt");
        }

        private void GetData()
        {
            mlTI = new List<TI>();
            mlNTI = new List<NTI>();

            ProcessTI(ref mlTI);
            ProcessNTI(ref mlNTI);
        }

        private void ProcessNTI(ref List<NTI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Status, B.Amt ");
            SQL.AppendLine("From TblNTIHdr A ");
            SQL.AppendLine("Inner Join TblNTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Status", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new NTI()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            Status = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI(ref List<TI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.TaxRate  ");
            SQL.AppendLine("From TblTIHdr A ");
            SQL.AppendLine("Inner Join TblTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y' And A.UsedFor = '01'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "SeqNo", "Amt1", "Amt2", "TaxRate" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            TaxRate = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion
       
        #endregion

        #region Class

        private class Bonus
        {
            public string EmpCode { get; set; }
            public decimal Value { get; set; }
            public decimal Tax { get; set; }
            public decimal Amt { get; set; }
            public string PTKP { get; set; }
            public string NPWP { get; set; }
            public decimal WorkPeriod { get; set; }
            public string ResignPeriod { get; set; }
        }

        private class NTI
        {
            public string DocNo { get; set; }
            public string Status { get; set; }
            public decimal Amt { get; set; }
        }

        private class TI
        {
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal TaxRate { get; set; }
        }

        #endregion
    }
}
