﻿#region Update
/*
    10/09/2017 [TKG] tambah level
    23/07/2020 [WED/SIER] tambah ActInd berdasarkan parameter IsGradeLevelUseActInd
    12/08/2021 [IBL/PHT] tambah Sequence berdasarkan parameter IsGradeLevelUseSequence
    24/08/2021 [RDA/PHT] tambah Scale berdasarkan parameter IsGradeUseScale
    30/08/2021 [TRI/PHT] tambah Odd Even berdasarkan parameter IsGradeUseOddEven
    12/01/2022 [TKG/IMS] tambah other ss salary
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGradeLevel : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mGrdLvlCode = string.Empty;
        internal FrmGradeLevelFind FrmFind;
        internal bool
            mIsGradeLevelOtherSSSalaryEnabled = false,
            mIsGradeLevelUseActInd = false,
            mIsGradeLevelUseSequence = false,
            mIsGradeUseScale = false,
            mIsGradeUseOddEven = false;
        #endregion

        #region Constructor

        public FrmGradeLevel(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Grade";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (!mIsGradeLevelUseActInd) ChkActInd.Visible = false;
                if (!mIsGradeLevelUseSequence) LblSeqNo.Visible = TxtSeqNo.Visible = false;
                if (!mIsGradeUseScale) LblScale.Visible = TxtScale.Visible = false;
                SetFormControl(mState.View);
                SetGrd();
                Tc1.SelectedTabPage = Tp2;
                Sl.SetLueOption(ref LueOddEven, "GradeOddEven");
                if (!mIsGradeUseOddEven) LblOddEven.Visible = LueOddEven.Visible = false;
                Tc1.SelectedTabPage = Tp1;
                Sl.SetLueGrdLvlGrpCode(ref LueGrdLvlGrpCode, string.Empty);
                Sl.SetLueLevelCode(ref LueLevelCode);
                
                if (mGrdLvlCode.Length != 0)
                {
                    ShowData(mGrdLvlCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Allowance/Deduction"+Environment.NewLine+"Code",
                        "Allowance/Deduction"+Environment.NewLine+"Name",
                        "",
                        "Amount",

                    },
                     new int[] 
                    {
                        60, 
                        20, 150, 250, 20, 130
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3 });
            Sm.GrdColButton(Grd1, new int[] { 1, 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtGrdLvlCode, TxtGrdLvlName, LueGrdLvlGrpCode, LueLevelCode, TxtBasicSalary, 
                        TxtBasicSalary2, TxtHealthSSSalary, TxtEmploymentSSSalary, TxtTaxSalary, ChkActInd,
                        TxtOtherSSSalary, TxtSeqNo, TxtScale, LueOddEven
                    }, true);
                    Grd1.ReadOnly = true;
                    TxtGrdLvlCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtGrdLvlCode, TxtGrdLvlName, LueGrdLvlGrpCode, LueLevelCode, TxtBasicSalary, 
                        TxtBasicSalary2, TxtHealthSSSalary, TxtEmploymentSSSalary, TxtTaxSalary, TxtSeqNo,
                        TxtScale, LueOddEven
                    }, false);
                    if (mIsGradeLevelOtherSSSalaryEnabled) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtOtherSSSalary }, false);
                    ChkActInd.Checked = true;
                    Grd1.ReadOnly = false;
                    TxtGrdLvlCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtGrdLvlName, LueGrdLvlGrpCode, LueLevelCode, TxtBasicSalary, TxtBasicSalary2, 
                        TxtHealthSSSalary, TxtEmploymentSSSalary, TxtTaxSalary, TxtSeqNo, TxtScale, 
                        LueOddEven
                    }, false);
                    if (mIsGradeLevelUseActInd) ChkActInd.Properties.ReadOnly = false;
                    if (mIsGradeLevelOtherSSSalaryEnabled) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtOtherSSSalary }, false);
                    Grd1.ReadOnly = false;
                    TxtGrdLvlName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            { TxtGrdLvlCode, TxtGrdLvlName, LueGrdLvlGrpCode, LueLevelCode, LueOddEven });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { 
                TxtBasicSalary, TxtBasicSalary2, TxtHealthSSSalary, TxtEmploymentSSSalary, TxtTaxSalary,
                TxtOtherSSSalary, TxtSeqNo, TxtScale 
            }, 0);
            ChkActInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmGradeLevelFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtGrdLvlCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;
                Cursor.Current = Cursors.WaitCursor;
                var cml = new List<MySqlCommand>();
                cml.Add(SaveGradeLevelHdr());
                if (Grd1.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) 
                            cml.Add(SaveGradeLevelDtl( Row));
                }
                Sm.ExecCommands(cml);
                ShowData(TxtGrdLvlCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmGradeLevelDlg(this));
                }

                if (Sm.IsGrdColSelected(new int[] { 1, 5 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
                }
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAllowanceDeduction(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mADCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmGradeLevelDlg(this));
            
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmAllowanceDeduction(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mADCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string GrdLvlCode)
        {
            try
            {
                ClearData();
                ShowGradeLevelHdr(GrdLvlCode);
                ShowGradeLevelDtl(GrdLvlCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowGradeLevelHdr(string GrdLvlCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", GrdLvlCode);

            Sm.ShowDataInCtrl(
                 ref cm,
                  "Select GrdLvlCode, GrdLvlName, GrdLvlGrpCode, LevelCode, " +
                  "BasicSalary, BasicSalary2, HealthSSSalary, EmploymentSSSalary, TaxSalary, ActInd, SeqNo, Scale, OddEvenInd, " +
                  (mIsGradeLevelOtherSSSalaryEnabled?"": " Null ") +
                  " OtherSSSalary " +
                  "From TblGradeLevelHdr Where GrdLvlCode=@GrdLvlCode;",
                 new string[] 
                   {
                      //0
                      "GrdLvlCode",

                      //1-5
                      "GrdLvlName", "GrdLvlGrpCode", "LevelCode", "BasicSalary", "BasicSalary2", 
                      
                      //6-10
                      "HealthSSSalary", "EmploymentSSSalary", "TaxSalary", "ActInd", "SeqNo",

                      //11-13
                      "Scale", "OddEvenInd", "OtherSSSalary"
                   },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtGrdLvlCode.EditValue = Sm.DrStr(dr, c[0]);
                     TxtGrdLvlName.EditValue = Sm.DrStr(dr, c[1]);
                     Sm.SetLue(LueGrdLvlGrpCode, Sm.DrStr(dr, c[2]));
                     Sm.SetLue(LueLevelCode, Sm.DrStr(dr, c[3]));
                     TxtBasicSalary.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                     TxtBasicSalary2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                     TxtHealthSSSalary.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                     TxtEmploymentSSSalary.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                     TxtTaxSalary.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                     ChkActInd.Checked = Sm.DrStr(dr, c[9]) == "Y";
                     TxtSeqNo.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                     TxtScale.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                     Sm.SetLue(LueOddEven, Sm.DrStr(dr, c[12]));
                     TxtOtherSSSalary.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                 }, true
             );
        }

        private void ShowGradeLevelDtl(string GrdLvlCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ADCode, B.ADName, A.Amt ");
            SQL.AppendLine("From TblGradeLevelDtl A  ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode ");
            SQL.AppendLine("Where A.GrdLvlCode=@GrdLvlCode Order By B.ADName;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@GrdLvlCode", GrdLvlCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                       { 
                           //0
                           "DNo",

                           //1-3
                           "ADCode","ADName","Amt"

                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count-1, new int[] { 5 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtGrdLvlCode, "Grade's code", false) ||
                Sm.IsTxtEmpty(TxtGrdLvlName, "Grade's name", false) ||
                Sm.IsLueEmpty(LueGrdLvlGrpCode, "Grade's group" ) ||
                (mIsGradeLevelUseSequence && Sm.IsTxtEmpty(TxtSeqNo, "Sequence", true)) ||
                (mIsGradeUseScale && Sm.IsTxtEmpty(TxtScale, "Scale", true)) ||
                IsGrdLvlCodeExisted() ||
                (mIsGradeLevelUseSequence && IsGrdLvlSequenceExisted()) ||
                IsGrdValueNotValid() ||
                IsDataInactive();
        }

        private bool IsDataInactive()
        {
            if (!mIsGradeLevelUseActInd) return false;
            if (!TxtGrdLvlCode.Properties.ReadOnly) return false;

            if (Sm.IsDataExist("Select 1 From TblGradeLevelHdr Where GrdLvlCode = @Param And ActInd = 'N'; ", TxtGrdLvlCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already inactive.");
                ChkActInd.Focus();
                return true;
            }

            return false;
        }

        private bool IsGrdLvlCodeExisted()
        { 
            if (TxtGrdLvlCode.Properties.ReadOnly) return false;
            return Sm.IsDataExist(
                "Select 1 From TblGradeLevelHdr Where GrdLvlCode=@Param",
                TxtGrdLvlCode.Text,
                "This grade code already existed.");
        }

        private bool IsGrdLvlSequenceExisted()
        {
            return Sm.IsDataExist(
                "Select 1 From TblGradeLevelHdr Where SeqNo=@Param",
                TxtSeqNo.Text,
                "This sequence number already existed.");
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int r=0; r<Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 2, false, "Allowance/Deduction is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, r, 5, true, "Amount is empty.")) return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveGradeLevelHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblGradeLevelHdr ");
            SQL.AppendLine("(GrdLvlCode, GrdLvlName, GrdLvlGrpCode, LevelCode, SeqNo, Scale, BasicSalary, BasicSalary2, HealthSSSalary, EmploymentSSSalary, TaxSalary, OddEvenInd, ");
            if (mIsGradeLevelOtherSSSalaryEnabled) SQL.AppendLine("OtherSSSalary, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@GrdLvlCode, @GrdLvlName, @GrdLvlGrpCode, @LevelCode, @SeqNo, @Scale, @BasicSalary, @BasicSalary2, @HealthSSSalary, @EmploymentSSSalary, @TaxSalary, @OddEvenInd, ");
            if (mIsGradeLevelOtherSSSalaryEnabled) SQL.AppendLine("@OtherSSSalary, ");
            SQL.AppendLine("@UserCode, @Dt) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("    ActInd = @ActInd, GrdLvlName=@GrdLvlName, GrdLvlGrpCode=@GrdLvlGrpCode, LevelCode=@LevelCode, ");
            SQL.AppendLine("    SeqNo=@SeqNo, Scale=@Scale, BasicSalary=@BasicSalary, BasicSalary2=@BasicSalary2, ");
            SQL.AppendLine("    HealthSSSalary=@HealthSSSalary, EmploymentSSSalary=@EmploymentSSSalary, TaxSalary=@TaxSalary, OddEvenInd = @OddEvenInd, ");
            if (mIsGradeLevelOtherSSSalaryEnabled) SQL.AppendLine("OtherSSSalary=@OtherSSSalary, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=@Dt; ");

            SQL.AppendLine("Delete From TblGradeLevelDtl Where GrdLvlCode=@GrdLvlCode;");

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", TxtGrdLvlCode.Text);
            Sm.CmParam<String>(ref cm, "@GrdLvlName", TxtGrdLvlName.Text);
            Sm.CmParam<String>(ref cm, "@GrdLvlGrpCode", Sm.GetLue(LueGrdLvlGrpCode));
            Sm.CmParam<String>(ref cm, "@LevelCode", Sm.GetLue(LueLevelCode));
            Sm.CmParam<Decimal>(ref cm, "@SeqNo", decimal.Parse(TxtSeqNo.Text));
            Sm.CmParam<Decimal>(ref cm, "@Scale", decimal.Parse(TxtScale.Text));
            Sm.CmParam<String>(ref cm, "@OddEvenInd", Sm.GetLue(LueOddEven));
            Sm.CmParam<Decimal>(ref cm, "@BasicSalary", decimal.Parse(TxtBasicSalary.Text));
            Sm.CmParam<Decimal>(ref cm, "@BasicSalary2", decimal.Parse(TxtBasicSalary2.Text));
            Sm.CmParam<Decimal>(ref cm, "@HealthSSSalary", decimal.Parse(TxtHealthSSSalary.Text));
            Sm.CmParam<Decimal>(ref cm, "@EmploymentSSSalary", decimal.Parse(TxtEmploymentSSSalary.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxSalary", decimal.Parse(TxtTaxSalary.Text));
            Sm.CmParam<Decimal>(ref cm, "@OtherSSSalary", decimal.Parse(TxtOtherSSSalary.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveGradeLevelDtl(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblGradeLevelDtl(GrdLvlCode, DNo, ADCode, Amt, CreateBy, CreateDt) " +
                    "Values (@GrdLvlCode, @DNo, @ADCode, @Amt, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", TxtGrdLvlCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "ADCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsGradeLevelUseActInd', 'IsGradeLevelUseSequence', 'IsGradeUseScale', 'IsGradeUseOddEven', 'IsGradeLevelOtherSSSalaryEnabled' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsGradeLevelUseActInd": mIsGradeLevelUseActInd = ParValue == "Y"; break;
                            case "IsGradeLevelUseSequence": mIsGradeLevelUseSequence = ParValue == "Y"; break;
                            case "IsGradeUseScale": mIsGradeUseScale = ParValue == "Y"; break;
                            case "IsGradeUseOddEven": mIsGradeUseOddEven = ParValue == "Y"; break;
                            case "IsGradeLevelOtherSSSalaryEnabled": mIsGradeLevelOtherSSSalaryEnabled = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        //private bool IsAllowanceDeductionEmpty(iGRequestEditEventArgs e)
        //{
        //    if (Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0)
        //    {
        //        e.DoDefault = false;
        //        return true;
        //    }
        //    return false;
        //}

        //internal string GetSelectedItem()
        //{
        //    var SQL = string.Empty;
        //    if (Grd1.Rows.Count != 1)
        //    {
        //        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
        //        {
        //            if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
        //            {
        //                if (SQL.Length != 0) SQL += ", ";
        //                SQL +=
        //                    "'" +
        //                    Sm.GetGrdStr(Grd1, Row, 2) +
        //                    "'";
        //            }
        //        }
        //    }
        //    return (SQL.Length == 0 ? "'XXX'" : SQL);
        //}

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtGrdLvlCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGrdLvlCode);
        }

        private void TxtGrdLvlName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGrdLvlName);
        }

        private void LueGrdLvlGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueGrdLvlGrpCode, new Sm.RefreshLue2(Sl.SetLueGrdLvlGrpCode), string.Empty);
        }
        
        private void TxtBasicSalary_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtBasicSalary, 0);
        }

        private void TxtBasicSalary2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtBasicSalary2, 0);
        }

        private void TxtHealthSSSalary_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtHealthSSSalary, 0);
        }

        private void TxtOtherSSSalary_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtOtherSSSalary, 0);
        }

        private void TxtEmploymentSSSalary_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtEmploymentSSSalary, 0);
        }

        private void TxtTaxSalary_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtTaxSalary, 0);
        }

        private void LueLevelCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueLevelCode, new Sm.RefreshLue1(Sl.SetLueLevelCode));
        }

        private void TxtSeqNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtSeqNo, 0);
        }

        private void TxtScale_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtScale, 0);
        }
        
        #endregion

        private void label9_Click(object sender, EventArgs e)
        {

        }


        #endregion

    }
}
