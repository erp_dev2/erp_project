﻿#region Update
/*
    13/12/2021 [BRI/PHT] menu baru  
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBalanceSheetSettingDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmBalanceSheetSetting mFrmParent;
        private string
            mSQL = string.Empty,
            mTypeDlg = string.Empty
            ;
        private Label label6;
        private DXE.TextEdit TxtAcNo;
        private DXE.CheckEdit ChkAcNo;

        #endregion

        #region Constructor

        public FrmBalanceSheetSettingDlg(FrmBalanceSheetSetting FrmParent, string TypeDlg)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mTypeDlg = TypeDlg;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "List of COA's Account#";
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetLueLevel(ref LueLevel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.ReadOnly = false;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    "No.",

                    //1-5
                    "",
                    "Account#",
                    "Description",
                    "Type",
                    "Alias",
                    //6
                    "ProcessInd"
                },
                new int[] { 50, 20, 150, 350, 100, 150, 80 }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1, 6 });
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 6 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AcNo, AcDesc, Case AcType When 'D' Then 'Debit' When 'C' Then 'Credit' End As AcType, Alias ");
            SQL.AppendLine("From TblCoa ");
            SQL.AppendLine("Where ActInd='Y' ");
            if (Sm.GetLue(LueLevel).Length > 0)
                SQL.AppendLine("And Left(AcNo, 1) = '" + Sm.GetLue(LueLevel) + "'");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                if (Sm.IsLueEmpty(LueLevel, "Account Group")) return;
                Cursor.Current = Cursors.WaitCursor;

                SetSQL();
                string Filter = " ";

                var cm = new MySqlCommand();

                for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(mFrmParent.Grd1, r, 2).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " And ";
                        Filter += "(AcNo<>@AssetAcNo" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@AssetAcNo" + r.ToString(), Sm.GetGrdStr(mFrmParent.Grd1, r, 2));
                    }
                }

                for (int r = 0; r < mFrmParent.Grd2.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(mFrmParent.Grd2, r, 2).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " And ";
                        Filter += "(AcNo<>@LiabilityAcNo" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@LiabilityAcNo" + r.ToString(), Sm.GetGrdStr(mFrmParent.Grd2, r, 2));
                    }
                }

                for (int r = 0; r < mFrmParent.Grd3.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(mFrmParent.Grd3, r, 2).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " And ";
                        Filter += "(AcNo<>@EquityAcNo" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EquityAcNo" + r.ToString(), Sm.GetGrdStr(mFrmParent.Grd3, r, 2));
                    }
                }

                Sm.FilterStr(ref Filter, ref cm, TxtAccountNo.Text, new string[] { "AcNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtAcDesc.Text, new string[] { "AcDesc", "Alias" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By AcNo;",
                        new string[]
                        {
                            "AcNo",
                            "AcDesc",
                            "AcType",
                            "Alias"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                if (mTypeDlg == "2")
                {
                    ChooseMultiData(mFrmParent.Grd1);
                }
                if (mTypeDlg == "3")
                {
                    ChooseMultiData(mFrmParent.Grd2);
                }
                if (mTypeDlg == "4")
                {
                    ChooseMultiData(mFrmParent.Grd3);
                }
                this.Close();
            }
        }

        private void ChooseMultiData(iGrid mParentGrid)
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsCOAAlreadyChosen(Row, mParentGrid))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mParentGrid.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mParentGrid, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mParentGrid, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mParentGrid, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mParentGrid, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mParentGrid, Row1, 6, Grd1, Row2, 6);

                        mParentGrid.Rows.Add();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account#.");
        }

        private bool IsCOAAlreadyChosen(int Row, iGrid mParentGrd)
        {
            for (int Index = 0; Index < mParentGrd.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mParentGrd, Index, 2),
                    Sm.GetGrdStr(Grd1, Row, 2)
                    )) return true;
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }
        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, e.RowIndex, 1);
                Grd1.Cells[e.RowIndex, 6].Value = IsSelected;

                string mRow = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                if (ChkChildInd.Checked)
                {
                    for (int xrow = e.RowIndex + 1; xrow < Grd1.Rows.Count; xrow++)
                    {
                        if (Sm.GetGrdStr(Grd1, xrow, 2).Length >= mRow.Length)
                        {
                            string mRow2 = Sm.Left(Sm.GetGrdStr(Grd1, xrow, 2), mRow.Length);
                            if (mRow2 == mRow)
                            {
                                Grd1.Cells[xrow, 1].Value = IsSelected;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }


        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        private void SetLueLevel(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            for (int i = 1; i <= Convert.ToInt32(mFrmParent.mMaxAccountCategory); i++)
            {
                if (i == Convert.ToInt32(mFrmParent.mMaxAccountCategory))
                    SQL.AppendLine("Select " + i + " As Col1, " + i + " As Col2 ");
                else
                {
                    SQL.AppendLine("Select " + i + " As Col1, " + i + " As Col2 ");
                    SQL.AppendLine("UNION ALL ");
                }

            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        private void TxtAccountNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAccountNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account");
        }

        private void TxtAcDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Description");
        }

        private void LueLevel_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel, new Sm.RefreshLue1(SetLueLevel));
        }

        #endregion

    }
}
