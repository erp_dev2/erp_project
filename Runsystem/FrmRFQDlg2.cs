﻿#region Update
/*
    23/11/2021 [WED/RM] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRFQDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmRFQ mFrmParent;
        private string mSQL = string.Empty, mSectorCode = string.Empty, mFieldName = string.Empty;
        private byte mDocType = 0;
        private int mRow = 0, mCodeColIndex = 0;

        #endregion

        #region Constructor

        public FrmRFQDlg2(FrmRFQ FrmParent, byte DocType, int Row, string SectorCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocType = DocType;
            mRow = Row;
            mSectorCode = SectorCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                if (mDocType == 1) mFieldName = "Sector";
                else
                {
                    mFieldName = "Sub Sector";
                    mCodeColIndex = 3;
                    LblCode.Text = mFieldName;
                    this.Text = "List of " + mFieldName;
                }
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No",

                    //1-3
                    "",
                    "Code",
                    mFieldName,
                },
                new int[]
                {
                    40,
                    20, 100, 200
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            if (mDocType == 2) Sm.GrdColInvisible(Grd1, new int[] { 1 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.SetGrdProperty(Grd1, false);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.Code, T.Name ");
            SQL.AppendLine("From ( ");

            if (mDocType == 1)
            {
                SQL.AppendLine("Select SectorCode As Code, SectorName As Name ");
                SQL.AppendLine("From TblSector ");
                SQL.AppendLine("Where ActInd = 'Y' ");
            }
            else
            {
                SQL.AppendLine("Select SubSectorCode As Code, SubSectorName As Name ");
                SQL.AppendLine("From TblSubSector ");
                SQL.AppendLine("Where ActInd = 'Y' ");
                SQL.AppendLine("And SectorCode = @SectorCode ");
            }

            SQL.AppendLine(") T ");
            SQL.AppendLine("Where 0 = 0 ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtCode.Text, new string[] { "T.Code", "T.Name" });

                if (mDocType == 2) Sm.CmParam<String>(ref cm, "@SectorCode", mSectorCode);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T.Name;",
                    new string[] { "Code", "Name" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                if (mDocType == 1)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1))
                        {
                            if (IsChoose == false) IsChoose = true;

                            Row1 = mFrmParent.Grd1.Rows.Count - 1;
                            Row2 = Row;

                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 0, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);

                            mFrmParent.Grd1.Rows.Add();
                        }
                    }

                    if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 " + mFieldName.ToLower() + ".");
                }
                else
                {
                    if (Sm.IsFindGridValid(Grd1, 2))
                    {
                        mFrmParent.Grd1.Cells[mRow, 3].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                        mFrmParent.Grd1.Cells[mRow, 5].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);

                        this.Close();
                    }
                }
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }
        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (mDocType == 1)
            {
                if (Grd1.Rows.Count > 0)
                {
                    bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        Grd1.Cells[Row, 1].Value = !IsSelected;
                }
            }
        }
        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (mDocType == 2)
            {
                ChooseData();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        #endregion

        #endregion

    }
}
