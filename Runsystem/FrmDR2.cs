﻿#region Update
/*
    30/10/2019 [WED/IMS] new apps, ambil data dari SO Contract
    13/12/2019 [DITA+VIN+WED/IMS] Printout DR
    30/01/2020 [TKG/IMS] tambah project code, project name, po#
    20/02/2020 [WED/IMS] update ke revision nya juga
    06/05/2020 [HAR/IMS] bug menampilkan outstanding SO
    11/05/2020 [VIN/IMS] remark detail bisa diedit
    15/05/2020 [IBL/IMS] Penyesuaian Print Out
    10/06/2020 [IBL/IMS] Tambah kolom SO Contract Remark berdasarkan parameter mIsSalesTransactionShowSOContractRemark
    10/06/2020 [IBL/IMS] Tambah kolom Notes berdasarkan parameter mIsSalesTransactionUseItemNotes
    01/07/2020 [HAR/IMS] feedback remark so contract ambil dari detail item SO Contract
    23/09/2020 [WED/IMS] tambah informasi kolom No dari SO Contract berdasarkan parameter IsDetailShowColumnNumber
    24/09/2020 [IBL/IMS] Penyesuaian printout
    24/06/2021 [RDA/IMS] Tambah item dismantle di printout DR for Project
    12/07/2021 [TRI/IMS] bug saat print item double 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmDR2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mCity = string.Empty, mCnt = string.Empty, mExpCity = string.Empty, mDocNo = string.Empty;
        internal FrmDR2Find FrmFind;
        iGCell fCell;
        bool fAccept;
        internal int bal, left, right = 0;
        internal int start, end = 0;
        internal decimal pallet = 0m;
        internal string 
            QtyPallet = string.Empty,
            mIsPrintOutDR=string.Empty,
            mLogisticItemCategoryCode = string.Empty,
            mLogisticDepartmentCode = string.Empty,
             mLocalDocument = "0";
        internal bool 
            mIsDRWithCBD= false,
            mIsSystemUseCostCenter = false,
            mIsCreditLimitValidate = false,
            mIsApprovalBySiteMandatory = false,
            mIsSalesTransactionShowSOContractRemark = false,
            mIsSalesTransactionUseItemNotes = false,
            mIsDetailShowColumnNumber = false
            ;

        private string
           mPortForFTPClient = string.Empty,
           mHostAddrForFTPClient = string.Empty,
           mSharedFolderForFTPClient = string.Empty,
           mUsernameForFTPClient = string.Empty,
           mPasswordForFTPClient = string.Empty,
           mFileSizeMaxUploadFTPClient = string.Empty,
           mDRAutoApprovedByEmpCode = string.Empty,
           mDRVerifiedByEmpCode =string.Empty,
           mDRDocType = "2";
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmDR2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Delivery Request (Project)";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();

                Sl.SetLueTTCode(ref LueTtCode);
                SetLueItCode(ref LueItCode);
                SetLueCtCode(ref LueCtCode);
                SetLueVdCode(ref LueVdCode);
                SetFormControl(mState.View);
                SetLueStatus(ref LueStatus);

                var cm = new MySqlCommand()
                {
                    CommandText = "Select ParValue from TblParameter Where ParCode='FormPrintOutDR' "
                };
                mIsPrintOutDR = Sm.GetValue(cm);
                if (mIsPrintOutDR.Length == 0)
                    mIsPrintOutDR = "DR";
                SetGrd();

                base.FrmLoad(sender, e);
                //if this application is called from other application :)
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            bool mFlag = true;
            string mMenuDRStd = Sm.GetParameter("MenuCodeForDeliveryRequestStandard");
            if (mMenuDRStd.Length > 0)
            {
                string[] s = mMenuDRStd.Split(',');
                foreach (string d in s)
                {
                    if (mMenuCode == d)
                    {
                        mFlag = false;
                        break;
                    }
                }
            }
            //mIsDRWithCBD = !Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForDeliveryRequestStandard"));
            mIsDRWithCBD = mFlag;
            mIsCreditLimitValidate = Sm.GetParameter("IsCreditLimitValidate") == "Y";
            mLogisticDepartmentCode = Sm.GetParameter("LogisticDepartmentCode");
            mLogisticItemCategoryCode = Sm.GetParameter("LogisticItemCategoryCode");
            mIsApprovalBySiteMandatory = Sm.GetParameter("IsApprovalBySiteMandatory") == "Y";
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mLocalDocument = Sm.GetParameter("LocalDocument");
            mDRAutoApprovedByEmpCode = Sm.GetParameter("DRAutoApprovedByEmpCode");
            mDRVerifiedByEmpCode = Sm.GetParameter("DRVerifiedByEmpCode");
            mIsSalesTransactionShowSOContractRemark = Sm.GetParameterBoo("IsSalesTransactionShowSOContractRemark");
            mIsSalesTransactionUseItemNotes = Sm.GetParameterBoo("IsSalesTransactionUseItemNotes");
            mIsDetailShowColumnNumber = Sm.GetParameterBoo("IsDetailShowColumnNumber");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-5
                        "Item's"+Environment.NewLine+"Code",
                        "Item's"+Environment.NewLine+"Name",
                        "",
                        "Local Code",
                        "SO#",

                        //6-10
                        "",
                        "SO DNo",
                        "Packaging"+Environment.NewLine+"Unit",
                        "Outstanding"+Environment.NewLine+"Packaging"+Environment.NewLine+"Quantity",
                        "Packaging"+Environment.NewLine+"Quantity",

                        //11-15
                        "Balance",
                        "Outstanding"+Environment.NewLine+"SO",
                        "Requested"+Environment.NewLine+"Quantity (Sales)",
                        "Balance",
                        "UoM"+Environment.NewLine+"(Sales)",

                        //16-20
                        "Stock",
                        "Requested"+Environment.NewLine+"Quantity (Inventory)",
                        "UoM"+Environment.NewLine+"(Inventory)",
                        "Delivery"+Environment.NewLine+"Date",
                        "Remark",

                        //21-25
                        "Price After Tax",
                        "Project Code",
                        "Project Name",
                        "Customer's PO",
                        "SO Contract's"+Environment.NewLine+"Remark",

                        //26-27
                        "Notes",
                        "SO Contract's"+Environment.NewLine+"No"
                    },
                     new int[] 
                    {
                        20, 
                        80, 200, 20, 150, 150, 
                        20, 50, 100, 100, 100,   
                        80, 100, 100, 80, 80, 
                        40, 130, 80, 100, 150,
                        100, 120, 200, 120, 200,
                        200, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 19 });
            if (mIsDRWithCBD == false)
            {
                Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 4, 5, 7, 8, 9, 11, 12, 13, 14, 15, 17, 18, 19,  21, 22, 23, 24, 25, 27 });
            }
            else
            {
                Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 21, 22, 23, 24, 25, 27 });
            }
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 12, 13, 14, 17,21 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0, 3, 6, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 7, 16, 21 }, false);
            Grd1.Cols[27].Move(1);

            if (!mIsDetailShowColumnNumber)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 27 });
            }

            if(!mIsSalesTransactionShowSOContractRemark)
                Sm.GrdColInvisible(Grd1, new int[] { 25 }, false);

            if (!mIsSalesTransactionUseItemNotes)
                Sm.GrdColInvisible(Grd1, new int[] { 26 }, false);
            
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 16 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtLocalDocNo, LueCtCode, LueShipAdd, 
                        LueStatus, LueShipAdd, TxtAddress, TxtCity, TxtCountry, 
                        TxtPostalCd, TxtPhone, TxtFax, TxtEmail, TxtMobile, 
                        LueVdCode, LueDriver, TxtCityCode, LueTtCode, TxtPlatNo, 
                        TxtMobile2, LueAgtCode, MeeNote, MeeRemark, LueItCode, DteUsageDt, TxtMRDocNo
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = true;
                    Sm.GrdColReadOnly(Grd1, new int[] { 26 }, true);
                    BtnCtShippingAddress.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueCtCode, LueShipAdd, LueVdCode, 
                        LueDriver, TxtCityCode, LueTtCode, TxtPlatNo, TxtMobile2, 
                        LueAgtCode, MeeNote, MeeRemark, LueItCode 
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 26 });
                    Grd1.ReadOnly = false;
                    BtnCtShippingAddress.Enabled = true;
                    DteDocDt.Focus();
                  break;
                case mState.Edit:
                    Sm.SetControlReadOnly(LueStatus, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    BtnCtShippingAddress.Enabled = false;
                    Grd1.ReadOnly = true;
                    LueStatus.Focus();
                    break;
            }
        }

        private void ClearData(int Option)
        {
            switch (Option)
            {
                case 1:
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        TxtDocNo, DteDocDt, TxtLocalDocNo, LueCtCode, LueShipAdd, 
                        LueStatus, TxtAddress, TxtCity, TxtCountry, TxtPostalCd, 
                        TxtPhone, TxtFax, TxtEmail, TxtMobile, LueVdCode, 
                        LueDriver, TxtCityCode, LueTtCode, TxtPlatNo, TxtMobile2, 
                        LueAgtCode, MeeNote, MeeRemark, LueItCode, DteUsageDt
                    });
                    ChkCancelInd.Checked = false;
                    ClearGrd();
                    Sm.FocusGrd(Grd1, 0, 1);
                    break;
                case 2:
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        TxtAddress, TxtCity, TxtCountry, TxtPostalCd, TxtPhone, 
                        TxtFax, TxtEmail, TxtMobile, LueAgtCode
                    });
                    break;
                case 3:
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        LueDriver, TxtMobile2, TxtCityCode, LueTtCode, TxtPlatNo
                    });
                    break;
            }
           
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 9, 10, 13 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDR2Find(this, mIsDRWithCBD);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData(1);
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueStatus, "O");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData(1);
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDR2Dlg(this, Sm.GetLue(LueCtCode), Sm.GetLue(LueAgtCode), mIsDRWithCBD));
                    }

                }
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSOContract2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                if (e.ColIndex == 16) ShowStockInfo(e.RowIndex);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && !Sm.IsLueEmpty(LueCtCode, "Customer") && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmDR2Dlg(this, Sm.GetLue(LueCtCode), Sm.GetLue(LueAgtCode), mIsDRWithCBD));

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmSOContract2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 16) 
                ShowStockInfo(e.RowIndex);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (mIsDRWithCBD == false)
            {
                if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtDocNo.Text.Length == 0)
                {
                    if (Grd1.SelectedRows.Count > 0)
                    {
                        if (Grd1.Rows[Grd1.Rows[Grd1.Rows.Count - 1].Index].Selected)
                            MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        else
                        {
                            if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                for (int Index = Grd1.SelectedRows.Count - 1; Index >= 0; Index--)
                                {
                                    Grd1.Rows.RemoveAt(Grd1.SelectedRows[Index].Index);
                                    if (Grd1.Rows.Count <= 0)
                                    {
                                        Grd1.Rows.Add();
                                    }
                                }
                            }
                        }
                    }
                }
                Sm.GrdEnter(Grd1, e);
                Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 10)
            {
                ComputeUom(e.RowIndex);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DR", "TblDRHdr");

            var cml = new List<MySqlCommand>();

            if (Sm.GetLue(LueItCode).Length > 0)
            {
                if (Sm.StdMsgYN("Question", "Material Request Document is automatically generated based on this data. Do you want to proceed ?") == DialogResult.Yes)
                {
                    if (Sm.IsDteEmpty(DteUsageDt, "Usage Date") || Sm.IsMeeEmpty(MeeRemark, "Remark")) return;
                    else
                    {
                        Cursor.Current = Cursors.WaitCursor;

                        string MRDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr");
                        string ItCode = Sm.GetLue(LueItCode);

                        cml.Add(SaveDRHdr(DocNo, MRDocNo));
                        for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                            if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                                cml.Add(SaveDRDtl(DocNo, Row));

                        cml.Add(SaveMRHdr(MRDocNo));
                        cml.Add(SaveMRDtl(MRDocNo, ItCode, IsDocApprovalSettingNotExisted()));
                    }
                }
                else
                {
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        DteUsageDt
                    });

                    cml.Add(SaveDRHdr(DocNo, string.Empty));
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                            cml.Add(SaveDRDtl(DocNo, Row));
                }
            }
            else
            {
                cml.Add(SaveDRHdr(DocNo, string.Empty));
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveDRDtl(DocNo, Row));
            }

            cml.Add(UpdateSO(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueShipAdd, "Shipping Address") ||
                IsSOCancelledAlready() ||
                IsSOStatusFulfilledAlready() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsDRCityNotValid() ||
                CheckCreditLimit();
        }

        private bool IsDRCityNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.GetGrdStr(Grd1, Row, 5).Length>0 &&
                    IsDRCityNotValid(Sm.GetGrdStr(Grd1, Row, 5)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "SO# : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                        "City : " + TxtCity.Text + Environment.NewLine + Environment.NewLine +
                        "Invalid City." + Environment.NewLine +
                        "Please update quotation."
                        );
                    return true;
                }
            }
            return false;
        }

        private bool IsDRCityNotValid(string SODocNo)
        {
            var SQL = new StringBuilder();

            string ParamDtCode = Sm.GetParameter("DTCodeOnDR");
            string ShpMCode = Sm.GetValue("Select ShpMCode From TblSOContractHdr Where DocNo = @Param; ", SODocNo);

            if (ParamDtCode != ShpMCode)
            {
                SQL.AppendLine("Select CityName From TblCity ");
                SQL.AppendLine("Where CityCode=@CityCode ");
                SQL.AppendLine("And CityCode In ( ");
                SQL.AppendLine("    Select Distinct A.SACityCode ");
                SQL.AppendLine("    From TblSOContractHdr A ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("); ");
            }
            else
            {
                SQL.AppendLine("Select CityName From TblCity ");
                SQL.AppendLine("Where CityCode=@CityCode ");
                SQL.AppendLine("And CityCode In ( ");
                SQL.AppendLine("    Select CityCode From TblCity ");
                SQL.AppendLine("); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CityCode", mCity);
            Sm.CmParam<String>(ref cm, "@DocNo", SODocNo);
            return !Sm.IsDataExist(cm);
        }

        private bool IsSOProcessIndAlreadyFulfilled(string Param)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblSOContractDtl " +
                    "Where ProcessIndForDR='F' " +
                    "And Locate(Concat('##', DocNo, DNo, '##'), @Param)>0; "
            };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Request delivery already processed to DO to customer.");
                return true;
            }
            return false;
        }

        private bool IsSOStatusFulfilledAlready()
        {
            var DocNo = GetValue(
                "Select DocNo From TblSOContractHdr " +
                "Where ProcessIndForDR In ('M', 'F') And Locate(Concat('##', DocNo, '##'), @Param)>0 " +
                "Limit 1; ",
                GetSelectedSODocNo()
                );
            if (DocNo.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "SO# :" + DocNo + Environment.NewLine +
                    "This document already fulfilled.");
                return true;
            }
            return false;
        }

        private bool IsSOCancelledAlready()
        {
           
            var DocNo = GetValue(
                "Select DocNo From TblSOContractHdr " +
                "Where CancelInd='Y' And Locate(Concat('##', DocNo, '##'), @Param)>0 " +
                "Limit 1; ",
                GetSelectedSODocNo()
                );
            if (DocNo.Length!=0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "SO# :" + DocNo + Environment.NewLine +
                    "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            RecomputeBalance();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Msg =
                   "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                   "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                   "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                   "Packaging Unit : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                   "Sales Uom : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                   "SO# : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine + Environment.NewLine;

                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 8, false, "Uom (Packaging Unit) is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 10, true, Msg + "Requested Quantity (Packaging Unit) should not be 0.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 13, true, Msg + "Requested Quantity (Sales) should not be 0.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 17, true, Msg + "Requested Quantity (Inventory) should not be 0.")
                    )
                    return true;

               
                  
                if (Sm.GetGrdDec(Grd1, Row, 10) > Sm.GetGrdDec(Grd1, Row, 9))
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        Msg +
                        "Requested quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 10), 0) + 
                        " ) should not be bigger than outstanding quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 9), 0) + 
                        " ).");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 13) > Sm.GetGrdDec(Grd1, Row, 12))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Requested quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 13), 0) +
                        " ) should not be bigger than outstanding quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 12), 0) +
                        " ).");
                    return true;
                }

                if (IsDataExists(
                        "Select DocNo From TblSOContractDtl " +
                        "Where ProcessIndForDR='F' And Locate(Concat('##', DocNo, DNo, '##'), @Param)>0 " +
                        "Limit 1; ",
                        "##" + Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 7) + "##", 
                        Msg + "This data already fulfilled."
                    )) return true;
                
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 SO#.");
                return true;
            }
            return false;
        }

        private bool IsQtyRequestNotValid()
        {
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 11) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Packaging Uom : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                            "Packaging Quantity is bigger than Outstanding Packaging Quantity.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 14) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Packaging Uom : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                            "Request Quantity is bigger than Outstanding Request Quantity.");
                        return true;
                    }
                }
            }

            return false;
        }
     
        private MySqlCommand SaveDRHdr(string DocNo, string MRDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDRHdr(DocNo, DocDt, DocType, CancelInd, ProcessInd, LocalDocNo, MRDocNo, ItCode, UsageDt, WhsCode, CtCode, CBDInd, SAName, SAAddress, SACityCode, SACntCode, SAPostalCD, SAPhone, SAFax, SAEmail, SAMobile, ExpVdCode, ExpDriver, ExpPlatNo, ExpMobile, ExpCityCode, ExpTTCode, AgtCode, Note, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @DocType, @CancelInd, @ProcessInd, @LocalDocNo, @MRDocNo, @ItCode, @UsageDt, @WhsCode, @CtCode, @CBDInd, @SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCD, @SAPhone, @SAFax, @SAEmail, @SAMobile, @ExpVdCode, @ExpDriver, @ExpPlatNo, @ExpMobile, @ExpCityCode, @ExpTTCode, @AgtCode, @Note, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDRDocType);
            Sm.CmParam<String>(ref cm, "@MRDocNo", MRDocNo);
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetLue(LueItCode));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetDte(DteUsageDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CBDInd", mIsDRWithCBD ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@SAName", Sm.GetLue(LueShipAdd));
            Sm.CmParam<String>(ref cm, "@SAAddress", TxtAddress.Text);
            Sm.CmParam<String>(ref cm, "@SACityCode", mCity);
            Sm.CmParam<String>(ref cm, "@SACntCode", mCnt);
            Sm.CmParam<String>(ref cm, "@SAPostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@SAPhone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@SAFax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@SAEmail", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@SAMobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@ExpVdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@ExpDriver", Sm.GetLue(LueDriver));
            Sm.CmParam<String>(ref cm, "@ExpPlatNo", TxtPlatNo.Text);
            Sm.CmParam<String>(ref cm, "@ExpMobile", TxtMobile2.Text);
            Sm.CmParam<String>(ref cm, "@ExpCityCode", mExpCity);
            Sm.CmParam<String>(ref cm, "@ExpTTCode", Sm.GetLue(LueTtCode));
            Sm.CmParam<String>(ref cm, "@AgtCode", Sm.GetLue(LueAgtCode));
            Sm.CmParam<String>(ref cm, "@Note", MeeNote.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDRDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDRDtl(DocNo, DNo, SODocNo, SODNo, QtyPackagingUnit, Qty, QtyInventory, Remark, Notes, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @SODocNo, @SODNo, @QtyPackagingUnit, @Qty, @QtyInventory, @Remark, @Notes, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SODocNo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@SODNo", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@QtyInventory", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@Notes", Sm.GetGrdStr(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateSO(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractDtl T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Distinct SODocNo, SODNo ");
            SQL.AppendLine("    From TblDrDtl  ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.SODocNo And T1.DNo=T2.SODNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select A.SODocNo, A.SODNo, Sum(IfNull(B.QtyPackagingUnit, 0)) As QtyPackagingUnit ");
	        SQL.AppendLine("    From ( ");
		    SQL.AppendLine("        Select Distinct SODocNo, SODNo "); 
		    SQL.AppendLine("        From TblDrDtl  ");
		    SQL.AppendLine("        Where DocNo=@DocNo ");
	        SQL.AppendLine("    ) A ");
	        SQL.AppendLine("    Inner Join TblDrDtl B On A.SODocNo=B.SODocNo And A.SODNo=B.SODNo ");
	        SQL.AppendLine("    Inner Join TblDrHdr C On B.DocNo=C.DocNo And C.CancelInd='N' ");
	        SQL.AppendLine("    Group By A.SODocNo, A.SODNo ");
            SQL.AppendLine(") T3 On T1.DocNo=T3.SODocNo And T1.DNo=T3.SODNo ");
            SQL.AppendLine("Set T1.ProcessIndForDR =  ");
	        SQL.AppendLine("    Case When IfNull(T3.QtyPackagingUnit, 0)=0 Then 'O' ");
	        SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When IfNull(T3.QtyPackagingUnit, 0)>=T1.QtyPackagingUnit Then 'F' Else 'P' End ");
	        SQL.AppendLine("End ");
            if (mIsDRWithCBD == true)
            {
                SQL.AppendLine(",T1.ProcessIndForDRCBD =  ");
                SQL.AppendLine("    Case When IfNull(T3.QtyPackagingUnit, 0)=0 Then 'O' ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(T3.QtyPackagingUnit, 0)>=T1.QtyPackagingUnit Then 'F' Else 'P' End ");
                SQL.AppendLine("End ");
            }

            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblSOContractRevisionDtl A ");
            SQL.AppendLine("Inner Join TblSOContractDtl B On A.SOCDocNo = B.DocNo And A.DNo = B.DNo ");
            SQL.AppendLine("    And B.DocNo In (Select SODocNo From TblDRDtl Where DocNo = @DocNo) ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.ProcessIndForDR = B.ProcessIndForDR, ");
            SQL.AppendLine("    A.ProcessIndForDRCBD = B.ProcessIndForDRCBD ");
            SQL.AppendLine("Where A.DocNo In (Select Max(DocNo) From TblSOContractRevisionHdr Where SOCDocNo In (Select SODocNo From TblDRDtl Where DocNo = @DocNo)); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private MySqlCommand SaveMRHdr(string MRDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialRequestHdr(DocNo, LocalDocNo, POQtyCancelDocNo, DocDt, SiteCode, DeptCode, ReqType, BCCode, SeqNo, ItScCode, Mth, Yr, Revision, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @LocalDocNo, NULL, @DocDt, NULL, @DeptCode, @ReqType, NULL, NULL, NULL, NULL, NULL, NULL, @Remark, @CreateBy, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", MRDocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", mLogisticDepartmentCode);
            Sm.CmParam<String>(ref cm, "@ReqType", Sm.GetParameter("ReqTypeForNonBudget"));
            //Sm.CmParam<String>(ref cm, "@ItScCode", Sm.GetGrdStr(Grd1, 0, 22));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveMRDtl(string MRDocNo, string ItCode, bool NoNeedApproval)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblMaterialRequestDtl(DocNo, DNo, CancelInd, CancelReason, Status, ItCode, Qty, UsageDt, QtDocNo, QtDNo, UPrice, Remark, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, 'N', NULL, @Status, @ItCode, '1', @UsageDt, NULL, NULL, 0, @Remark,  @CreateBy, CurrentDateTime()); ");

            if (!NoNeedApproval)
            {
                SQLDtl.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQLDtl.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
                SQLDtl.AppendLine("From TblDocApprovalSetting T ");
                SQLDtl.AppendLine("Where T.DeptCode=@DeptCode ");
                //if (mIsApprovalBySiteMandatory)
                //    SQLDtl.AppendLine("And IfNull(T.SiteCode, '')=@SiteCode ");
                SQLDtl.AppendLine("And T.DocType='MaterialRequest'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", MRDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Status", NoNeedApproval ? "A" : "O");
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetDte(DteUsageDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", mLogisticDepartmentCode);
            //Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
            
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelDRHdr());
            cml.Add(UpdateSO(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueStatus, "Status") ||
                IsDataAlreadyCancelled() ||
                IsDataAlreadyFullfiled() ||
                IsDRAlreadyProcessedToDKO() ||
                (!ChkCancelInd.Checked && IsStatusNotValid()) ||
                (ChkCancelInd.Checked && IsDRAlreadyProcessedToDO()) ||
                IsDataProcessedToMR() ||
                IsMREximExisted();
        }

        private bool IsMREximExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.Status In ('O', 'A') ");
            SQL.AppendLine("Where A.DRDocNo Is Not Null And A.DRDocNo=@Param Limit 1;");

            string DocNo = Sm.GetValue(SQL.ToString(), TxtDocNo.Text);
            if (DocNo.Length>0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "You can't cancel this document." + Environment.NewLine + 
                    "You need to cancel it's material request ("+DocNo+") first."
                    );
                return true;
            }
            return false;
        }

        private bool IsDataProcessedToMR()
        {
            if (TxtMRDocNo.Text.Length > 0)
            {
                var SQLMR = new StringBuilder();

                SQLMR.AppendLine("Select T.DocNo From TblMaterialRequestDtl T ");
                SQLMR.AppendLine("Where T.DocNo = ( ");
                SQLMR.AppendLine("  Select A.MRDocNo From TblDRHdr A ");
                SQLMR.AppendLine("  Where A.DocNo = @DocNo ");
                SQLMR.AppendLine(") ");
                SQLMR.AppendLine("And T.CancelInd = 'N' ");

                var cm = new MySqlCommand() { CommandText = SQLMR.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This data is already processed to Material Request.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDRAlreadyProcessedToDKO()
        {
            var DocNo = Sm.GetValue(
                "Select DRDocNo From TblDKO " +
                "Where DRDocno = '" + TxtDocNo.Text + "' And cancelInd = 'N' And DRDocno is not null " +
                "Limit 1; ");
            if (DocNo.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "This document has been processed into DKO. ");
                return true;
            }
            return false;
        }


        private bool IsStatusNotValid()
        {
            if (Sm.GetLue(LueStatus) != "M")
            {
                Sm.StdMsg(mMsgType.Warning, "Delivery Request's status is not valid.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyFullfiled()
        {
            return IsDataExists(
                "Select ProcessInd From TblDrHdr " +
                "Where ProcessInd='F' And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already fulfilled."
                ); 
        }

        private bool IsDataAlreadyCancelled()
        {
            return IsDataExists(
                "Select CancelInd From TblDrHdr " +
                "Where CancelInd='Y' And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already cancelled."
                ); 
        }

        private bool IsDRAlreadyProcessedToDO()
        {
            return IsDataExists(
                "Select A.DRDocNo " +
                "From TblDOCt2Hdr A " +
                "Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' " +
                "Where A.DRDocNo=@Param Limit 1;",
                TxtDocNo.Text,
                "This requested data already processed to DO."
                );
        }

        private MySqlCommand CancelDRHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDRHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, ProcessInd=@ProcessInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N"); 
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData(1);
                ShowDRHdr(DocNo);
                ShowDRDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDRHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.ProcessInd, A.LocalDocNo, A.MRDocNo, E.ItCode, A.UsageDt, ");
            SQL.AppendLine("A.CtCode, A.SAName, A.SAAddress, A.SACityCode, B.CityName, A.SACntCode, D.CntName, A.SAPostalCD, ");
            SQL.AppendLine("A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile, A.ExpVdCode, A.ExpDriver, A.ExpPlatNo, A.ExpMobile, ");
            SQL.AppendLine("A.ExpCityCode, C.CityName As ExpCityName, A.ExpTtCode, A.AgtCode, A.Note, A.Remark, A.ItCode As LogisticItCode ");
            SQL.AppendLine("From TblDRHdr A ");
            SQL.AppendLine("Left Join TblCity B On A.SACityCode = B.CityCode ");
            SQL.AppendLine("Left Join TblCity C On A.ExpCityCode = C.CityCode ");
            SQL.AppendLine("Left Join TblCountry D On A.SACntCode = D.CntCode ");
            SQL.AppendLine("Left Join TblMaterialRequestDtl E On A.MRDocNo = E.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "CancelInd", "ProcessInd", "LocalDocNo", "CtCode", 
                        "SAName", "SAAddress", "SACityCode", "CityName", "SACntCode", 
                        "CntName", "SAPostalCD",  "SAPhone", "SAFax", "SAEmail", 
                        "SAMobile", "ExpVdCode", "ExpDriver", "ExpPlatNo", "ExpMobile", 
                        "ExpCityCode", "ExpCityName", "ExpTtCode", "AgtCode", "Note", 
                        "Remark", "ItCode", "UsageDt", "MRDocNo", "LogisticItCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr("Y", Sm.DrStr(dr, c[2])); 
                        Sm.SetLue(LueStatus, Sm.DrStr(dr, c[3]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5])); 
                        Sm.SetLue(LueShipAdd, Sm.DrStr(dr, c[6]));
                        TxtAddress.EditValue = Sm.DrStr(dr, c[7]);
                        mCity = Sm.DrStr(dr, c[8]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[9]);
                        mCnt = Sm.DrStr(dr, c[10]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[11]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[12]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[13]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[14]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[15]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[16]);
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[17]));
                        Sm.SetLue(LueDriver, Sm.DrStr(dr, c[18]));
                        TxtPlatNo.EditValue = Sm.DrStr(dr, c[19]);
                        TxtMobile2.EditValue = Sm.DrStr(dr, c[20]);
                        mExpCity = Sm.DrStr(dr, c[21]);
                        TxtCityCode.EditValue = Sm.DrStr(dr, c[22]);
                        Sm.SetLue(LueTtCode, Sm.DrStr(dr, c[23]));
                        Sm.SetLue(LueAgtCode, Sm.DrStr(dr, c[24]));
                        MeeNote.EditValue = Sm.DrStr(dr, c[25]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[26]);
                        Sm.SetLue(LueItCode, Sm.DrStr(dr, c[27]));
                        Sm.SetDte(DteUsageDt, Sm.DrStr(dr, c[28]));
                        TxtMRDocNo.EditValue = Sm.DrStr(dr, c[29]);
                        Sm.SetLue(LueItCode, Sm.DrStr(dr, c[30]));
                    }, true
                );
        }

        private void ShowDRDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT D.ItCode, E.ItName, E.ItCodeInternal, E.Specification, C.DocNo, D.DNo, ");
            SQL.AppendLine("D.PackagingUnitUomCode, B.QtyPackagingUnit, ");
            SQL.AppendLine("(IFNULL(D.QtyPackagingUnit, 0) - IFNULL(F.QtyPackagingUnit, 0)) AS OutstandingPackaging, ");
            SQL.AppendLine("B.Qty, D.PackagingUnitUomCode PriceUomCode, (IFNULL(D.Qty, 0) - IFNULL(F.Qty, 0)) AS OutstandingQty, ");
            SQL.AppendLine("B.QtyInventory, E.InventoryUomCode, D.DeliveryDt, B.Remark, D.Amt AS PriceAfterTax, ");
            SQL.AppendLine("I.ProjectCode, I.ProjectName, C.PONo, D.Remark as SOCRemark, B.Notes, D.No ");
            SQL.AppendLine("FROM TblDRHdr A ");
            SQL.AppendLine("INNER JOIN TblDRDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN TblSOContractHdr C ON B.SODocNo = C.DocNo ");
            SQL.AppendLine("INNER JOIN TblSOContractDtl D ON B.SODocNo = D.DocNo AND B.SODNo = D.DNo  ");
            SQL.AppendLine("INNER JOIN TblItem E ON D.ItCode = E.ItCode ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    SELECT T2.SODocNo, T2.SODNo, ");
	        SQL.AppendLine("    SUM(T2.QtyPackagingUnit) AS QtyPackagingUnit, SUM(T2.Qty) AS Qty ");
	        SQL.AppendLine("    FROM TblDRHdr T1 ");
	        SQL.AppendLine("    INNER JOIN TblDRDtl T2 ON T1.DocNo = T2.DocNo ");
	        SQL.AppendLine("    INNER JOIN (SELECT SODocNo, SODNo FROM TblDRDtl WHERE DocNo = @DocNo) T3 ");
	        SQL.AppendLine("        ON T2.SODocNo = T3.SODocNo AND T2.SODNo = T3.SODNo ");
	        SQL.AppendLine("    WHERE T1.CancelInd = 'N' ");
	        SQL.AppendLine("    AND T1.DocNo <> @DocNo ");
	        SQL.AppendLine("    GROUP BY T2.SODocNo, T2.SODNo ");
            SQL.AppendLine(") F ON B.SODocNo = F.SODocNo And B.SODNo = F.SODNo ");
            SQL.AppendLine("Left Join TblBOQHdr G On C.BOQDocNo=G.DocNo ");
            SQL.AppendLine("Left Join TblLOPHdr H On G.LOPDocNo=H.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup I On H.PGCode=I.PGCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            SQL.AppendLine("ORDER BY B.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",
 
                    //1-5
                    "ItName", "ItCodeInternal", "DocNo",  "DNo", "PackagingUnitUomCode", 
                    
                    //6-10
                    "OutstandingPackaging", "QtyPackagingUnit", "OutstandingQty", "Qty", "PriceUomCode", 
                    
                    //11-15
                    "QtyInventory", "InventoryUomCode", "DeliveryDt","Remark", "PriceAfterTax",
 
                    //16-20
                    "ProjectCode", "ProjectName", "PONo", "SOCRemark", "Notes",

                    //21
                    "No"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Grd.Cells[Row, 11].Value = Sm.GetGrdDec(Grd, Row, 9) - Sm.GetGrdDec(Grd, Row, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    Grd.Cells[Row, 14].Value = Sm.GetGrdDec(Grd, Row, 12) - Sm.GetGrdDec(Grd, Row, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 21);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 12, 13, 14, 17, 21 });
            Sm.FocusGrd(Grd1, 0, 1);
            //ComputeUom();
        }

        public void ShowDataSO(string VoucherDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.SAName, B.ItCode, C.ItCodeInternal, C.Specification, C.ItName, B.PackagingUnitUomCode, ");
            SQL.AppendLine("A.DocNo, A.DocDt, B.DNo, B.PackagingUnitUomCode PriceUomCode, C.InventoryUomCode, B.DeliveryDt, ");
            SQL.AppendLine("B.QtyPackagingUnit, B.QtyPackagingUnit As OutStandingPackaging, ");
            SQL.AppendLine("B.Qty, B.Qty As OutstandingQty, G.DocNo AS VoucherDocNo, B.Remark, B.Amt AS PriceAfterTax, B.No ");
            SQL.AppendLine("FROM TblSOContractHdr A ");
            SQL.AppendLine("INNER JOIN TblSOContractDtl B ON A.DocNo = B.DocNo AND A.ProcessIndForDR <> 'F' ");
            SQL.AppendLine("    AND A.CancelInd = 'N' AND A.DocType = '2' ");
            SQL.AppendLine("INNER JOIN TblItem C ON B.ItCode = C.ItCode ");
            SQL.AppendLine("INNER JOIN TblSalesInvoiceHdr D ON A.DocNo = D.SODocNo ");
            SQL.AppendLine("INNER JOIN TblIncomingPaymentDtl E ON D.DocNo = E.InvoiceDocNo ");
            SQL.AppendLine("INNER JOIN TblIncomingPaymentHdr F ON E.DocNo = F.DocNo AND F.CancelInd = 'N' ");
            SQL.AppendLine("INNER JOIN TblVoucherHdr G ON F.VoucherRequestDocNo = G.VoucherRequestDocNo AND G.DocNo = @VoucherDocNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", VoucherDocNo);
            
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",
                    //1-5
                    "ItName", "ItCodeInternal", "DocNo",  "DNo", "PackagingUnitUomCode",                     
                    //6-10
                    "OutstandingPackaging", "QtyPackagingUnit", "OutstandingQty", "Qty", "PriceUomCode", 
                    //11-15
                    "Qty", "InventoryUomCode", "DeliveryDt","Remark", "PriceAftertax", 
                    //16
                    "No"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Grd.Cells[Row, 11].Value = Sm.GetGrdDec(Grd, Row, 9) - Sm.GetGrdDec(Grd, Row, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    Grd.Cells[Row, 14].Value = Sm.GetGrdDec(Grd, Row, 12) - Sm.GetGrdDec(Grd, Row, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 16);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 12, 13, 14, 17 });
            Sm.FocusGrd(Grd1, 0, 1);

            if (Sm.GetLue(LueCtCode) == Sm.GetParameter("OnlineCtCode"))
                ShowShippingAddressData2(Sm.GetGrdStr(Grd1, 0, 5));
        }

        private void ShowShippingAddressData(string CtCode, string ShipName)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@ShipName", ShipName);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.Address, A.CityCode, A.CntCode, B.CityName, C.CntName, A.PostalCd, A.Phone, A.Fax, A.Email, A.Mobile " +
                    "From TblCustomerShipAddress A " +
                    "Left Join TblCity B On A.CityCode = B.CityCode " +
                    "Left Join TblCountry C On A.CntCode = C.CntCode Where A.CtCode=@CtCode And A.Name=@ShipName ",
                    new string[] 
                    { 
                        "Address", 
                        "CityCode", "CityName", "CntCode", "CntName", "PostalCd",  
                        "Phone", "Fax", "Email", "Mobile", 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtAddress.EditValue = Sm.DrStr(dr, c[0]);
                        mCity = Sm.DrStr(dr, c[1]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[2]);
                        mCnt = Sm.DrStr(dr, c[3]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[4]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[6]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[7]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[8]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[9]);
                    }, false
                );
        }

        private void ShowExpeditionData(string VdCode, string ContactName)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.CmParam<String>(ref cm, "@ContactPersonName", ContactName);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.ContactPersonName, A.Position, A.ContactNumber, B.CityCode, C.CityName " +
                    "From TblVendorContactPerson A " +
                    "Left Join TblVendor B On A.VdCode = B.VdCode " +
                    "Left Join TblCity C On B.CityCode = C.CityCode Where A.VdCode=@VdCode And A.ContactPersonName=@ContactPersonName ",
                    new string[] 
                    { 
                        "ContactNumber", 
                        "CityCode", "CityName", 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtMobile2.EditValue = Sm.DrStr(dr, c[0]);
                        mExpCity = Sm.DrStr(dr, c[1]);
                        TxtCityCode.EditValue = Sm.DrStr(dr, c[2]);
                    }, false
                );
        }

        private void ShowShippingAddressData2(string SODocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SODocNo", SODocNo);

            Sm.ShowDataInCtrl( ref cm,
                    "Select A.SAName, SAAddress, A.SACityCode, B.CityName, A.SACntCode, C.CntName, SAPostalCd "+ 
                    "From TblSOContractHdr  A "+
                    "Left Join TblCity B On A.SACityCode = B.CityCode " +
                    "Left Join TblCountry C On A.SACntCode = C.CntCode "+
                    "Where A.DocNo=@SODocNo ",
                    new string[] 
                    { 
                        "SAName", 
                        "SAAddress", "SACityCode", "CityName", "SACntCode", "CntName", 
                        "SAPostalCd"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Sm.SetLue(LueShipAdd, Sm.DrStr(dr, c[0]));
                        TxtAddress.EditValue = Sm.DrStr(dr, c[1]);
                        mCity = Sm.DrStr(dr, c[2]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[3]);
                        mCnt = Sm.DrStr(dr, c[4]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[6]);
                    }, false
                );
        }


        #endregion

        #region Additional Method

        #region credit limit 

        private bool CheckCreditLimit()
        {
            if (mIsCreditLimitValidate && !mIsDRWithCBD)
            {
                decimal DOSI = 0m; 
                decimal SIIP = 0m;
                decimal IPVC = 0m;
                decimal RIDO =0m;
                decimal CL = 0m;
                decimal Outs = 0m;
                decimal DRNow = 0;
                decimal DRDO = 0;
                //string CTQT = string.Empty;

                //nentuin customer code
                string CtCode = string.Empty;
                CtCode = Sm.GetLue(LueCtCode);

                var mCLSQL = new StringBuilder();
                //var mBOQSQL = new StringBuilder();
                string mSODocNo = string.Empty;

                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 5).Length > 0)
                    {
                        if (mSODocNo.Length > 0) mSODocNo += ",";
                        mSODocNo += Sm.GetGrdStr(Grd1, i, 5);
                    }
                }

                mCLSQL.AppendLine("Select Sum(B.CreditLimit) ");
                mCLSQL.AppendLine("From TblSOContractHdr A ");
                mCLSQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
                mCLSQL.AppendLine("Where Find_In_Set(A.DocNo, @Param); ");

                //mBOQSQL.AppendLine("Select Group_Concat(Distinct B.DocNo) ");
                //mBOQSQL.AppendLine("From TblSOContractHdr A ");
                //mBOQSQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
                //mBOQSQL.AppendLine("    And B.ActInd = 'Y' ");
                //mBOQSQL.AppendLine("    And B.ProcessInd = 'F' ");
                //mBOQSQL.AppendLine("Where Find_In_Set(A.SODocNo, @Param); ");

                //nentuin nilai credit limit
                CL = Decimal.Parse(Sm.GetValue(mCLSQL.ToString(), mSODocNo));

                //nentuin customer quotation yang aktif
                //CTQT = Sm.GetValue(mBOQSQL.ToString(), mSODocNo);

                if (GetDOSI(CtCode) == 0)
                {
                    DOSI = 0;
                }
                else
                {
                    DOSI = GetDOSI(CtCode);
                }


                //nentuin amount DR yang sdg dibuat
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    DRNow += (Sm.GetGrdDec(Grd1, Row, 17) * Sm.GetGrdDec(Grd1, Row, 21));
                }

                SIIP = GetSIIP(CtCode);
                IPVC = GetIPVC(CtCode);
                RIDO = GetRIDO(CtCode, mSODocNo);
                DRDO = GetDRDO(CtCode);

                Outs = DOSI + SIIP + IPVC + DRNow + DRDO - RIDO;

                if (CL < Outs)
                {
                    Sm.StdMsg(
                    mMsgType.Warning,
                    "Credit Limit : " + Sm.FormatNum(CL, 0) + Environment.NewLine +
                    "Outstanding : " + Sm.FormatNum(Outs, 0) + Environment.NewLine +
                    "Balance : " + Sm.FormatNum(CL - Outs, 0) + Environment.NewLine +
                    "Total amount should not be greater than Credit Limit."
                    );
                    return true;
                }
                return false;
            }
            return false;
        }

        //Sales Retur  terhadap Quotation aktif
        private decimal GetRIDO(string CtCode, string SODocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ifnull(SUM(T.Amt), 0) As RIDOAMT From ( ");
            SQL.AppendLine("   Select B.ItCode, (A.Qty* C.UPrice) As AMt ");
            SQL.AppendLine("   from TblRecvCtDtl A ");
            SQL.AppendLine("   Inner Join TblDoCt2Dtl B On A.DOCtDocNo = B.DocNo And A.DOCtDno = B.Dno ");
            SQL.AppendLine("   Inner Join ( ");
            SQL.AppendLine("       Select A.DocNo, B.DNo, C.UPrice ");
            SQL.AppendLine("       From TblDOCt2Hdr A ");
            SQL.AppendLine("       Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("       Left Join ");
            SQL.AppendLine("       ( ");
            SQL.AppendLine("           Select Distinct A.DocNo, G.ItCode, C.CurCode, D.Amt As UPrice ");
            SQL.AppendLine("           From TblDrhdr A ");
            SQL.AppendLine("           Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("           Inner Join TblSOContractHdr C On B.SODocNo = C.DocNo And C.cancelInd = 'N' ");
            SQL.AppendLine("                 And Find_In_Set(C.DocNo, @SODocNo) ");
            SQL.AppendLine("           Inner Join TblSOContractDtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("           Inner Join TblItem I On D.ItCode=I.ItCode ");
            SQL.AppendLine("           Where A.cancelInd = 'N' And A.CtCode = @CtCode ");
            SQL.AppendLine("        ) C On A.DrDocno = C.DocNo And B.ItCode=C.ItCode ");
            SQL.AppendLine("   )C On B.DocNo = C.DocNo And B.Dno = C.Dno ");
            SQL.AppendLine(")T; ");

            var cm = new MySqlCommand { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@SODocNo", SODocNo);
            return Sm.GetValueDec(cm);
        }

        //Incoming payment yang belum divoucherkan
        private decimal GetIPVC(string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ifnull(SUM(A.Amt), 0) As AmtIPVC From TblIncomingpaymentHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo = B.DocNo ");
            SQL.AppendLine("Where A.cancelInd = 'N' And A.CtCode = @CtCode And ");
            SQL.AppendLine("B.DocNo Not In ( ");
            SQL.AppendLine("    Select A.VoucherRequestDocNo ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocno = B.Docno ");
            SQL.AppendLine("    Where A.CancelInd = 'N' ); ");

            var cm = new MySqlCommand { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        //SalesInvoice yang belum di incoming paymentkan
        private decimal GetSIIP(string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ifnull(Sum(Amt), 0) As AmtSIIP from tblsalesinvoicehdr t where ctcode = @CtCode and cancelind='N' and ");
            SQL.AppendLine("not exists( ");
            SQL.AppendLine("    select a.docno ");
            SQL.AppendLine("    from tblincomingpaymenthdr a, tblincomingpaymentdtl b ");
            SQL.AppendLine("    where a.docno=b.docno ");
            SQL.AppendLine("    and status<>'C' ");
            SQL.AppendLine("    and cancelind='N' ");
            SQL.AppendLine("    and b.InvoiceDocNo=t.docno and b.InvoiceType='1' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        //delivery order yang belum di sales invoicekan
        private decimal GetDOSI(string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IFNULL(SUM(T.Qty * T.PriceAfterTax), 0) AmtDOSI ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("   Select '1' As DocType, ");
            SQL.AppendLine("   If(C.QtyInventory=0, 0, (B.Qty/C.QtyInventory)*C.Qty) As Qty, ");
            SQL.AppendLine("   E.Amt As PriceAfterTax ");
            SQL.AppendLine("   From TblDOCt2Hdr A ");
            SQL.AppendLine("   Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' ");
            SQL.AppendLine("   Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo ");
            SQL.AppendLine("   Inner Join TblSOContractHdr D On C.SODocNo=D.DocNo And D.CancelInd = 'N'  ");
            SQL.AppendLine("   Inner Join TblSOContractDtl E On C.SODocNo=E.DocNo And C.SODNo=E.DNo ");
            SQL.AppendLine("   Inner Join TblDRHdr M On A.DRDocNo=M.DocNo ");
            SQL.AppendLine("   Where A.CtCode=@CtCode ");
            SQL.AppendLine(" )T; ");

            var cm = new MySqlCommand { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        //Delivery request yang belum di DO kan
        private decimal GetDRDO(string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select  ifnull(SUM((B.Qty* E.Amt)), 0)  PriceAfterTax ");
            SQL.AppendLine("From TblDrhdr A ");
            SQL.AppendLine("Inner Join TblDRDtl B On A.DocNo = B.DocNO ");
            SQL.AppendLine("Inner Join TblSOContractHdr D On B.SODocNo=D.DocNo And D.CancelInd = 'N'  ");
            SQL.AppendLine("Inner Join TblSOContractDtl E On B.SODocNo=E.DocNo And B.SODNo=E.DNo ");
            SQL.AppendLine("Where A.ProcessInd = 'O' And A.Cancelind = 'N' And A.CtCode = @CtCode ");

            var cm = new MySqlCommand { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        #endregion

        private bool IsDocApprovalSettingNotExisted()
        {
            if (!Sm.IsDataExist(
                    "Select DocType From TblDocApprovalSetting " +
                    "Where UserCode Is not Null " +
                    "And DocType='MaterialRequest' " +
                    "And DeptCode='" + mLogisticDepartmentCode + "' Limit 1"
                ))
                //Sm.StdMsg(mMsgType.Warning, "Nobody will approve this request.");
                return true;
            else
                return false;
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'O' As Col1, 'Outstanding' As Col2 Union All ");
            SQL.AppendLine("Select 'F', 'Fulfilled' Union All ");
            SQL.AppendLine("Select 'M', 'Manual Fulfilled';");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void SetLueItCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.ItCode As Col1, T.ItName As Col2 ");
            SQL.AppendLine("From TblItem T ");
            SQL.AppendLine("Where T.ActInd = 'Y' And T.ItCtCode = @ItCtCode ");
            SQL.AppendLine("Order By T.ItName Asc ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@ItCtCode", mLogisticItemCategoryCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void ShowStockInfo(int Row)
        {
            var SQL = new StringBuilder();
            string Msg = string.Empty;
                
            SQL.AppendLine("Select IfNull(( ");
            SQL.AppendLine("    Select Sum(Qty) ");
            SQL.AppendLine("    From TblStockSummary ");
            SQL.AppendLine("    Where ItCode=@ItCode And Qty>0 ");
            SQL.AppendLine("    ), 0) -");
            SQL.AppendLine("IfNull(( ");
	        SQL.AppendLine("    Select Sum(B.QtyInventory) ");
		    SQL.AppendLine("    From TblDrhdr A ");
		    SQL.AppendLine("    Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
		    SQL.AppendLine("    Inner Join TblSOContractHdr C On B.SODocNo = C.DocNo ");
            SQL.AppendLine("    Inner Join TblSOContractDtl D On B.SODocNo = D.DocNo And B.SODNo = D.DNo And D.ItCode = @ItCode ");
	        SQL.AppendLine("    Where A.CancelInd = 'N' And A.ProcessInd = 'O' ");
		    SQL.AppendLine("), 0) As Qty; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, new string[]{ "Qty" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                            Msg =
                                "Item Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                                "Item Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                                "Uom (Inventory) : " + Sm.GetGrdStr(Grd1, Row, 18) + Environment.NewLine +
                                "Available Stock : " + ((Sm.DrDec(dr, 0) < 0) ? "0" : Sm.FormatNum(Sm.DrDec(dr, 0), 0));
                    }
                    dr.Close();
                    dr.Dispose();
                }
                cm.Dispose();
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No available stock.");
        }

        //internal string GetSelectedSONumber()
        //{
        //    var SQL = string.Empty;
        //    if (Grd1.Rows.Count != 1)
        //    {
        //        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
        //        {
        //            if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
        //            {
        //                if (SQL.Length != 0) SQL += ", ";
        //                SQL +=
        //                    "##" +
        //                    Sm.GetGrdStr(Grd1, Row, 5) +
        //                    Sm.GetGrdStr(Grd1, Row, 7) +
        //                    "##";
        //            }
        //        }
        //    }
        //    return (SQL.Length == 0 ? "##XXX##" : SQL);
        //}

        private void SetLueCtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CtCode As Col1, CtName As Col2 " +
                "From TblCustomer " +
                "Where ActInd = 'Y' And CtCode In " +
                "(Select Distinct CtCode From TblSOContractHdr Where ProcessIndForDR In ('O', 'P') And OverSeaInd = 'N' ) Order By CtName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueVdCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select VdCode As Col1, VdName As Col2 From TblVendor " +
                "Where ActInd='Y' " +
                "And VdCtCode In (Select ParValue From TblParameter Where ParCode='VdCtCodeExpedition') " +
                "Order By VdName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueAgtCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue2(
                ref Lue,
                "Select Distinct B.AgtCode As Col1, C.Agtname As Col2  "+  
                "From TblSOContractHdr A "+
                "Inner Join TblSOContractDtl B On A.DocNo = B.DocNo "+
                "Inner Join TblAgent C On B.AgtCode = C.AgtCode " +
                "Where C.ActInd = 'Y' And A.CtCode = '" + CtCode + "' Order By AgtName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueDriver(ref LookUpEdit Lue, string VdCode)
        {
            Sm.SetLue3(
                ref Lue,
                "Select ContactPersonName As Col1, Position As Col2, ContactNumber As Col3 From TblVendorContactPerson Where VdCode = '"+VdCode+"'" +
                "Order By ContactPersonName",
                30, 35, 35, true, true, true, "Name", "Position", "Phone", "Col1", "Col1");
        }

        private void SetLueShippingAddress(ref DXE.LookUpEdit Lue, string CtCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Col1, Col2, Col3, Col4 From ( ");
                SQL.AppendLine("Select A.Dno As Col1, A.Name As Col2, Concat( A.Address, ' ',B.CityName) As Col3, A.Phone As Col4 From TblCustomershipaddress A ");
                SQL.AppendLine("Left Join tblcity B on A.CityCode = B.CityCode ");
                SQL.AppendLine("Where A.CtCode='" + CtCode + "' ");
                if (TxtDocNo.Text.Length != 0)
                {
                    SQL.AppendLine("Union All ");
                    SQL.AppendLine("Select Distinct '000' As Col1, A.SAName As Col2, Concat(A.SAAddress, ' ', 'B.CityName') As Col3, A.SAPhone As Col4 ");
                    SQL.AppendLine("From TblDRHdr A ");
                    SQL.AppendLine("Left Join TblCity B On A.SACityCode = B.CityCode ");
                    SQL.AppendLine("Where A.CtCode='" + CtCode + "' ");
                }
                if (TxtDocNo.Text.Length == 0 && Sm.GetLue(LueCtCode) == Sm.GetParameter("OnlineCtCode"))
                {
                    SQL.AppendLine("Union All ");
                    SQL.AppendLine("Select '' As Col1, A.SAName As Col2, A.SAAddress As Col3, '' As Col4 ");
                    SQL.AppendLine("From TblSOContractHdr  A ");
                    SQL.AppendLine("Left Join TblCity B On A.SACityCode = B.CityCode ");
                    SQL.AppendLine("Left Join TblCountry C On A.SACntCode = C.CntCode ");
                    SQL.AppendLine("Where A.CtCode='" + CtCode + "' ");
                }
                SQL.AppendLine(") Tbl Order By Col2");

                Sm.SetLue4(
                    ref Lue,
                    SQL.ToString(),
                    0, 100, 250, 50, false, true, true, true, "Code", "Name", "Address", "Phone", "Col2", "Col2");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal void ComputeUom(int index)
        {
            decimal QtyUomConvert12 = 0m, QtyUomConvert21 = 0m, QtyPackaging = 0m;
            string UomSales, UomSales1, UomInv;


            if (Sm.GetGrdDec(Grd1, index, 10) != 0 && Sm.GetGrdStr(Grd1, index, 1).Length != 0)
            {
                try
                {
                    QtyPackaging = Sm.GetGrdDec(Grd1, index, 10);
                    UomSales1 = Sm.GetValue("Select SalesUomCode From TblItem Where ItCode = '" + Sm.GetGrdStr(Grd1, index, 1) + "' ");

                    string SQtyUomConvert12 = Sm.GetValue("Select Qty From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, index, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, index, 8) + "' ");
                    string SQtyUomConvert21 = Sm.GetValue("Select Qty2 From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, index, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, index, 8) + "' ");

                    UomSales = Sm.GetGrdStr(Grd1, index, 15);
                    UomInv = Sm.GetGrdStr(Grd1, index, 18);

                    if (SQtyUomConvert12.Length > 0)
                    {
                        QtyUomConvert12 = Decimal.Parse(SQtyUomConvert12);
                    }
                    else
                    {
                        QtyUomConvert12 = 0;
                    }

                    if (SQtyUomConvert21.Length > 0)
                    {
                        QtyUomConvert21 = Decimal.Parse(SQtyUomConvert21);
                    }
                    else
                    {
                        QtyUomConvert21 = 0;
                    }


                    if (UomSales1 == UomSales)
                    {
                        Grd1.Cells[index, 13].Value = QtyPackaging * QtyUomConvert12;
                        Grd1.Cells[index, 11].Value = Sm.GetGrdDec(Grd1, index, 9) - Sm.GetGrdDec(Grd1, index, 10);
                        Grd1.Cells[index, 14].Value = Sm.GetGrdDec(Grd1, index, 12) - Sm.GetGrdDec(Grd1, index, 13);

                        if (UomSales == UomInv)
                        {
                            Grd1.Cells[index, 17].Value = Sm.GetGrdDec(Grd1, index, 13);
                        }
                        else
                        {
                            if (UomInv == UomSales1)
                            {
                                Grd1.Cells[index, 17].Value = QtyPackaging * QtyUomConvert12;
                            }
                            else
                            {
                                Grd1.Cells[index, 17].Value = QtyPackaging * QtyUomConvert21;
                            }
                        }
                    }
                    else
                    {
                        Grd1.Cells[index, 13].Value = QtyPackaging * QtyUomConvert21;
                        Grd1.Cells[index, 11].Value = Sm.GetGrdDec(Grd1, index, 9) - Sm.GetGrdDec(Grd1, index, 10);
                        Grd1.Cells[index, 14].Value = Sm.GetGrdDec(Grd1, index, 12) - Sm.GetGrdDec(Grd1, index, 13);

                        if (UomSales == UomInv)
                        {
                            Grd1.Cells[index, 17].Value = Sm.GetGrdDec(Grd1, index, 13);
                        }
                        else
                        {
                            if (UomInv == UomSales1)
                            {
                                Grd1.Cells[index, 17].Value = QtyPackaging * QtyUomConvert12;
                            }
                            else
                            {
                                Grd1.Cells[index, 17].Value = QtyPackaging * QtyUomConvert21;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

        public void ComputeUom()
        {
            decimal QtyPackaging = 0m, QtyUomConvert12 = 0m, QtyUomConvert21 = 0m;
            string UomSales, UomSales1, UomInv;


            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdDec(Grd1, Row, 10) != 0 && Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                {
                    try
                    {
                        QtyPackaging = Sm.GetGrdDec(Grd1, Row, 10);
                        UomSales = Sm.GetGrdStr(Grd1, Row, 15);
                        UomInv = Sm.GetGrdStr(Grd1, Row, 18);
                        UomSales1 = Sm.GetValue("Select SalesUomCode From TblItem Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' ");

                        string SQtyUomConvert12 = Sm.GetValue("Select ifnull(Qty, 0) As Qty From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, Row, 8) + "' ");
                        string SQtyUomConvert21 = Sm.GetValue("Select ifnull(Qty2, 0) As Qty2 From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, Row, 8) + "' ");

                        if (SQtyUomConvert12.Length > 0)
                        {
                            QtyUomConvert12 = Decimal.Parse(SQtyUomConvert12);
                        }
                        else
                        {
                            QtyUomConvert12 = 0;
                        }

                        if (SQtyUomConvert21.Length > 0)
                        {
                            QtyUomConvert21 = Decimal.Parse(SQtyUomConvert21);
                        }
                        else
                        {
                            QtyUomConvert21 = 0;
                        }

                        if (UomSales1 == UomSales)
                        {
                            Grd1.Cells[Row, 13].Value = QtyPackaging * QtyUomConvert12;
                            Grd1.Cells[Row, 11].Value = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 10);
                            Grd1.Cells[Row, 14].Value = Sm.GetGrdDec(Grd1, Row, 12) - Sm.GetGrdDec(Grd1, Row, 13);

                            if (UomSales == UomInv)
                            {
                                Grd1.Cells[Row, 17].Value = Sm.GetGrdDec(Grd1, Row, 13);
                            }
                            else
                            {
                                if (UomInv == UomSales1)
                                {
                                    Grd1.Cells[Row, 17].Value = QtyPackaging * QtyUomConvert12;
                                }
                                else
                                {
                                    Grd1.Cells[Row, 17].Value = QtyPackaging * QtyUomConvert21;
                                }
                            }
                        }
                        else
                        {
                            Grd1.Cells[Row, 13].Value = QtyPackaging * QtyUomConvert21;
                            Grd1.Cells[Row, 11].Value = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 10);
                            Grd1.Cells[Row, 14].Value = Sm.GetGrdDec(Grd1, Row, 12) - Sm.GetGrdDec(Grd1, Row, 13);

                            if (UomSales == UomInv)
                            {
                                Grd1.Cells[Row, 17].Value = Sm.GetGrdDec(Grd1, Row, 13);
                            }
                            else
                            {
                                if (UomInv == UomSales1)
                                {
                                    Grd1.Cells[Row, 17].Value = QtyPackaging * QtyUomConvert12;
                                }
                                else
                                {
                                    Grd1.Cells[Row, 17].Value = QtyPackaging * QtyUomConvert21;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("" + ex + "");
                    }
                }
            }
        }
        
        internal string GetSelectedItemSO()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            ("'" +
                            Sm.GetGrdStr(Grd1, Row, 1) +
                            Sm.GetGrdStr(Grd1, Row, 5) +
                            Sm.GetGrdStr(Grd1, Row, 7) +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 15) +
                            "'");
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedItemSO2()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            ("'" +
                            Sm.GetGrdStr(Grd1, Row, 5) +
                            "'");
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void RecomputeBalance()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, ");
            SQL.AppendLine("(B.QtyPackagingUnit-IfNull(C.QtyPackagingUnit, 0)) As OutstandingQtyPackagingUnit,  ");
            SQL.AppendLine("(B.Qty-IfNull(C.Qty, 0)) As OutstandingQty ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Inner Join TblSOContractDtl B On A.DocNo=B.DocNo and B.ProcessIndForDR <> 'F' ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.SODocNo As DocNo, T2.SODNo As DNo, ");
	        SQL.AppendLine("    Sum(IfNull(T2.QtyPackagingUnit, 0)) As QtyPackagingUnit, Sum(IfNull(T2.Qty, 0)) As Qty  ");
	        SQL.AppendLine("    From TblDRHdr T1  ");
	        SQL.AppendLine("    Inner Join TblDRDtl T2 On T1.DocNo=T2.DocNo ");
	        SQL.AppendLine("    Inner Join TblSOContractHdr T3 On T2.SODocNo = T3.DocNo And T3.CancelInd = 'N' And T3.ProcessIndForDR Not In ('M', 'F') ");
	        SQL.AppendLine("    Inner Join TblSOContractDtl T4 On T2.SODocNo = T4.DocNo And T2.SODNo = T4.DNo and T4.ProcessIndForDR <> 'F' ");
            SQL.AppendLine("    Inner Join TblBOQHdr T5 On T3.BOQDocNo = T5.DocNo ");
            SQL.AppendLine("    Inner Join TblLOPHdr T6 On T5.LOPDocNo = T6.DocNo And T6.CtCode = @CtCode ");
	        SQL.AppendLine("    Where T1.CancelInd='N' ");
	        SQL.AppendLine("    Group By T2.SODocNo, T2.SODNo ");
            SQL.AppendLine(") C On B.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("Inner Join TblBOQHdr D On A.BOQDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo = E.DocNo And E.CtCode = @CtCode ");
            SQL.AppendLine("Where A.CancelInd='N'  ");
            SQL.AppendLine("And A.ProcessIndForDR Not In ('M', 'F') ");
            SQL.AppendLine("And Locate(Concat('##', A.DocNo, B.DNo, '##'), @SelectedSO) >0 ");
            SQL.AppendLine("Order By A.DocNo, B.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SelectedSO", GetSelectedSO());
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                        new string[] 
                        { 
                            //0
                           "DocNo",  

                            //1-3
                           "DNo", "OutstandingQtyPackagingUnit", "OutstandingQty"
                        }
                        );
                    if (dr.HasRows)
                    {
                        Grd1.ProcessTab = true;
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                            {
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5), Sm.DrStr(dr, 0)) &&
                                    Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 7), Sm.DrStr(dr, 1)))
                                {
                                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 2);
                                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 3);
                                    break;
                                }
                            }
                        }
                        Grd1.EndUpdate();
                    }
                    dr.Close();
                    ComputeUom();
                }
            }
        }

        private bool IsDataExists(string SQL, string Param, string Warning)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, Warning);
                return true;
            }
            return false;
        }

        private string GetValue(string SQL, string Param)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            return Sm.GetValue(cm);
        }

        private string GetSelectedSODocNo()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 5) + "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GetSelectedSO()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 7) + "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        public static string GetNumber(string palet)
        {
            string number = string.Empty;
            for (int ind = 0; ind < palet.Length; ind++)
            {
                if (Char.IsNumber(palet[ind]) == true)
                {
                    number = number + palet[ind];
                }

            }
            return number;
        }

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<DRHdr>();
            var ldtl = new List<DRDtl>();
            var ldtl2 = new List<DRDtl2>();
            var lDtlS = new List<DRSign>();
            var lDtlS2 = new List<DRSign2>();
            var lDtlS3 = new List<DRSign2>();


            string[] TableName = { "DRHdr", "DRDtl", "DRDtl2", "DRSign", "DRSign2", "DRSign3" };
            string mDocTitle = Sm.GetParameter("DocTitle");
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();
            
            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
            SQL.AppendLine("A.LocalDocNo As DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.Ctname As Customer, A.LocalDocNo, ");
            SQL.AppendLine("A.ExpDriver, A.ExpPlatNo, C.TTName, D.VdName, ");
            SQL.AppendLine("A.DocNo As DocNoDR, H.ItName, DATE_FORMAT(A.UsageDt,'%d %M %Y')As UsageDt, A.SAName, A.SAAddress,"); // tambahan
            SQL.AppendLine("E.CityName, F.CntName, A.SAPostalCd, A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile, G.UserName, ");
            if (mDocTitle == "IMS")
                SQL.AppendLine("I.ProjectCode, I.ProjectName, I.PONo, I.Remark ");
            else
                SQL.AppendLine(" NULL As ProjectCode,NULL As ProjectName,NULL As PONo, A.Remark ");
            SQL.AppendLine("From TblDRHdr A  ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL.AppendLine("left Join Tbltransporttype C On A.ExpTTCode = C.TTCode ");
            SQL.AppendLine("left Join TblVendor D On A.ExpVdCode = D.VdCode ");
            SQL.AppendLine("Left Join TblCity E On A.SACityCode=E.CityCode ");
            SQL.AppendLine("Left Join TblCountry F On A.SACntCode = F.CntCode ");
            SQL.AppendLine("Left Join TblUser G On A.Createby = G.UserCode ");
            SQL.AppendLine("Left Join TblItem H On A.ItCode = H.ItCode ");
            if (mDocTitle == "IMS")
            {
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT T1.DocNo, GROUP_CONCAT(DISTINCT IFNULL(T5.ProjectCode, T2.ProjectCode2)) ProjectCode, ");
                SQL.AppendLine("    GROUP_CONCAT(DISTINCT IFNULL(T5.ProjectName, T4.ProjectName)) ProjectName, ");
                SQL.AppendLine("    GROUP_CONCAT(DISTINCT T2.PONo) PONo, Group_Concat(T1.Remark Separator '\n') Remark ");
                SQL.AppendLine("    FROM TblDRDtl T1 ");
                SQL.AppendLine("    INNER JOIN TblSOContractHdr T2 ON T1.SODocNo = T2.DocNo ");
                SQL.AppendLine("        AND T1.DocNo = @DocNo ");
                SQL.AppendLine("    INNER JOIN TblBOQHdr T3 ON T2.BOQDocNo = T3.DocNo ");
                SQL.AppendLine("    INNER JOIN TblLOPHdr T4 ON T3.LOPDocNo = T4.DocNo ");
                SQL.AppendLine("    LEFT JOIN TblProjectGroup T5 ON T4.PGCode = T5.PGCode ");
                SQL.AppendLine("    GROUP BY T1.DocNo ");
                SQL.AppendLine(") I ON A.DocNo = I.DocNo ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo And A.cancelInd ='N'  ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "DocNo",
                         //1
                         "DocDt",
                         "Customer",
                         "ExpDriver", 
                         "ExpPlatNo",
                         "TTName",
                         //6-10
                         "VdName",
                         "Remark",
                         "DocNoDR",
                         "ItName",
                         "UsageDt",
                         //11-15
                         "SAName",
                         "SAAddress",
                         "CityName",
                         "CntName",
                         "SAPostalCd",
                         //16-20
                         "SAPhone",
                         "SAFax",
                         "SAEmail",
                         "SAMobile",
                         "UserName",
                         //21-25
                         "ProjectCode",
                         "ProjectName",
                         "PONo",
                         "CompanyLogo",
                         "LocalDocNo"


                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DRHdr()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            Customer = Sm.DrStr(dr, c[2]),
                            ExpDriver = Sm.DrStr(dr, c[3]),
                            ExpPlatNo = Sm.DrStr(dr, c[4]),
                            ExpTT = Sm.DrStr(dr, c[5]),

                            ExpVd = Sm.DrStr(dr, c[6]),
                            Remark = Sm.DrStr(dr, c[7]),
                            DocNoDR = Sm.DrStr(dr, c[8]),
                            ItCode = Sm.DrStr(dr, c[9]),
                            UsageDt = Sm.DrStr(dr, c[10]),

                            SAName = Sm.DrStr(dr, c[11]),
                            SAAddress = Sm.DrStr(dr, c[12]),
                            CityName = Sm.DrStr(dr, c[13]),
                            CntName = Sm.DrStr(dr, c[14]),
                            SAPostalCd = Sm.DrStr(dr, c[15]),

                            SAPhone = Sm.DrStr(dr, c[16]),
                            SAFax = Sm.DrStr(dr, c[17]),
                            SAEmail = Sm.DrStr(dr, c[18]),
                            SAMobile = Sm.DrStr(dr, c[19]),
                            UserName = Sm.DrStr(dr, c[20]),

                            ProjectCode = Sm.DrStr(dr, c[21]),
                            ProjectName = Sm.DrStr(dr, c[22]),
                            PONo = Sm.DrStr(dr, c[23]),
                            CompanyLogo = Sm.DrStr(dr, c[24]),
                            LocalDocNo  = Sm.DrStr(dr, c[25]),


                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select I.ItCode, I.ItName, I.ItCodeInternal, DATE_FORMAT(C.DeliveryDt,'%d %M %Y') As DeliveryDt, ");
                SQLDtl.AppendLine("ifnull(C.PackagingUnitUomCode, '0') As PackagingUnitUomCode, B.QtyPackagingUnit, B.Qty,    ");
                SQLDtl.AppendLine("B.QtyInventory, I.InventoryUomCode, C.DeliveryDt, C.Remark, B.DNo, C.No, IfNull(G.ItName, H.ItName) ItNameDismantle ");
                SQLDtl.AppendLine("From TblDrHdr A  ");
                SQLDtl.AppendLine("Inner join TblDrDtl B On A.DocNo = B.DocNo  ");
                SQLDtl.AppendLine("Inner Join TblSOContractDtl C On B.SODocNo = C.DocNo And B.SODNo = C.DNo  ");
                SQLDtl.AppendLine("Inner Join TblSOContractHdr D On C.DocNo = D.DocNo And B.SODocNo = D.DocNo  ");
                SQLDtl.AppendLine("Left Join TblBOQDtl2 E On D.BOQDocNo = E.DocNo And C.No = E.No AND C.ItCode = E.ItCode ");
                SQLDtl.AppendLine("Left Join TblBOQDtl3 F On D.BOQDocNo = F.DocNo And C.No = F.No ");
                SQLDtl.AppendLine("Left Join TblItem G On E.ItCodeDismantle = G.ItCode ");
                SQLDtl.AppendLine("Left Join TblItem H On F.ItCodeDismantle = H.ItCode ");
                SQLDtl.AppendLine("Inner Join TblItem I On C.ItCode = I.ItCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo And A.cancelInd = 'N' ");
                SQLDtl.AppendLine("Order by B.DNo ");


                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "ItCode" ,
                     //1-5
                     "ItName",
                     "PackagingUnitUomCode",
                     "QtyPackagingUnit",
                     "QtyInventory",
                     "InventoryUomCode",

                     //6-10
                     "Remark",
                     "DNo",
                     "ItCodeInternal",
                     "Qty",
                     "DeliveryDt",

                     //11-12
                     "No",
                     "ItNameDismantle"
                    });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                            nomor = nomor + 1;
                            ldtl.Add(new DRDtl()
                            {
                                nomor = nomor,
                                QtyPackagingUnit = Sm.DrDec(drDtl, cDtl[3]),
                                ItCode = Sm.DrStr(drDtl, cDtl[0]),
                                ItName = Sm.DrStr(drDtl, cDtl[1]),
                                PackagingUnitUomCode = Sm.DrStr(drDtl, cDtl[2]),
                                QtyInventory = Sm.DrDec(drDtl, cDtl[4]),
                                InventoryUomCode = Sm.DrStr(drDtl, cDtl[5]),
                                Remark = Sm.DrStr(drDtl, cDtl[6]),
                                DNo = Sm.DrStr(drDtl, cDtl[7]),
                                ItCodeInternal = Sm.DrStr(drDtl, cDtl[8]),
                                Qty = Sm.DrDec(drDtl, cDtl[9]),
                                DeliveryDt = Sm.DrStr(drDtl, cDtl[10]),
                                SOCNo = Sm.DrStr(drDtl, cDtl[11]),
                                ItNameDismantle = Sm.DrStr(drDtl, cDtl[12]),
                            });

                    }
                    
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail2
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select I.ItCode, I.ItName, B.QtyPackagingUnit, ");
                SQLDtl2.AppendLine("B.Remark, B.Dno, C.PackagingUnitUomCode  ");
                SQLDtl2.AppendLine("From TblDrHdr A  ");
                SQLDtl2.AppendLine("Inner join TblDrDtl B On A.DocNo = B.DocNo  ");
                SQLDtl2.AppendLine("Inner Join TblSOContractDtl C On B.SODocNo = C.DocNo And B.SODNo = C.DNo  ");
                SQLDtl2.AppendLine("Inner Join TblSOContractHdr D On C.DocNo = D.DocNo And B.SODocNo = D.DocNo  ");
                SQLDtl2.AppendLine("Inner Join TblItem I On C.ItCode = I.ItCode  ");
                SQLDtl2.AppendLine("Where A.DocNo=@DocNo And A.cancelInd = 'N' ");
                SQLDtl2.AppendLine("Order by B.Dno ");


                cmDtl2.CommandText = SQLDtl2.ToString();

                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);

                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "ItCode" ,
                     //1-5
                     "ItName",
                     "QtyPackagingUnit",
                     "Remark",
                     "DNo",
                     "PackagingUnitUomCode"
                     
                    });
                if (drDtl2.HasRows)
                {
                    int nomor = 0;
                    while (drDtl2.Read())
                    {
                        nomor = nomor + 1;
                        ldtl2.Add(new DRDtl2()
                        {
                            nomor = nomor,
                            ItCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            ItName = Sm.DrStr(drDtl2, cDtl2[1]),
                            QtyPackagingUnit = Sm.DrDec(drDtl2, cDtl2[2]),
                            Remark = Sm.DrStr(drDtl2, cDtl2[3]),
                            DNo = Sm.DrStr(drDtl2, cDtl2[4]),
                            PackagingUnitUomCode = Sm.DrStr(drDtl2, cDtl2[5]),
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail Signature IMS

            //Dibuat Oleh
            var cmDtlS = new MySqlCommand();

            var SQLDtlS = new StringBuilder();
            using (var cnDtlS = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS.Open();
                cmDtlS.Connection = cnDtlS;

                SQLDtlS.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtlS.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtlS.AppendLine("From ( ");
                SQLDtlS.AppendLine("    Select Distinct ");
                SQLDtlS.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, '1' As DNo, 0 As Level, 'Prepared By,' As Title, Left(A.CreateDt, 8) As LastUpDt  ");
                SQLDtlS.AppendLine("    From TblDRHdr A ");
                SQLDtlS.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtlS.AppendLine("    Where  A.DocNo=@DocNo ");
                SQLDtlS.AppendLine(") T1 ");
                SQLDtlS.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtlS.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtlS.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtlS.AppendLine("Group By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Title ");
                SQLDtlS.AppendLine("Order By T1.Level; ");

                cmDtlS.CommandText = SQLDtlS.ToString();
                Sm.CmParam<String>(ref cmDtlS, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS, "@DocNo", TxtDocNo.Text);
                var drDtlS = cmDtlS.ExecuteReader();
                var cDtlS = Sm.GetOrdinal(drDtlS, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",

                         //6
                         "LastupDt"
                        });
                if (drDtlS.HasRows)
                {
                    while (drDtlS.Read())
                    {
                        lDtlS.Add(new DRSign()
                        {
                            Signature = Sm.DrStr(drDtlS, cDtlS[0]),
                            UserName = Sm.DrStr(drDtlS, cDtlS[1]),
                            PosName = Sm.DrStr(drDtlS, cDtlS[2]),
                            Space = Sm.DrStr(drDtlS, cDtlS[3]),
                            DNo = Sm.DrStr(drDtlS, cDtlS[4]),
                            Title = Sm.DrStr(drDtlS, cDtlS[5]),
                            LastUpDt = Sm.DrStr(drDtlS, cDtlS[6])
                        });
                    }
                }
                drDtlS.Close();
            }
            myLists.Add(lDtlS);

            //Disetujui Oleh
            var cmDtlS2 = new MySqlCommand();

            var SQLDtlS2 = new StringBuilder();
            using (var cnDtlS2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS2.Open();
                cmDtlS2.Connection = cnDtlS2;

                SQLDtlS2.AppendLine("Select A.EmpName, B.PosName  ");
                SQLDtlS2.AppendLine("From TblEmployee A  ");
                SQLDtlS2.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode  ");
                SQLDtlS2.AppendLine("Where A.EmpCode = @EmpCode  ");

                cmDtlS2.CommandText = SQLDtlS2.ToString();
                Sm.CmParam<String>(ref cmDtlS2, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS2, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmDtlS2, "@EmpCode", mDRAutoApprovedByEmpCode);
                var drDtlS2 = cmDtlS2.ExecuteReader();
                var cDtlS2 = Sm.GetOrdinal(drDtlS2, new string[] 
                        {
                         //0
                         "EmpName" ,

                         //1
                         "PosName" 
                        });
                if (drDtlS2.HasRows)
                {
                    while (drDtlS2.Read())
                    {

                        lDtlS2.Add(new DRSign2()
                        {
                            EmpName = Sm.DrStr(drDtlS2, cDtlS2[0]),
                            PosName = Sm.DrStr(drDtlS2, cDtlS2[1]),
                        });
                    }
                }
                drDtlS2.Close();
            }
            myLists.Add(lDtlS2);

            //Diperiksa Oleh
            var cmDtlS3 = new MySqlCommand();

            var SQLDtlS3 = new StringBuilder();
            using (var cnDtlS3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS3.Open();
                cmDtlS3.Connection = cnDtlS3;

                SQLDtlS3.AppendLine("Select A.EmpName, B.PosName  ");
                SQLDtlS3.AppendLine("From TblEmployee A  ");
                SQLDtlS3.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode  ");
                SQLDtlS3.AppendLine("Where A.EmpCode = @EmpCode  ");

                cmDtlS3.CommandText = SQLDtlS3.ToString();
                Sm.CmParam<String>(ref cmDtlS3, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS3, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmDtlS3, "@EmpCode", mDRVerifiedByEmpCode);
                var drDtlS3 = cmDtlS3.ExecuteReader();
                var cDtlS3 = Sm.GetOrdinal(drDtlS3, new string[] 
                        {
                         //0
                         "EmpName" ,

                         //1
                         "PosName" 
                        });
                if (drDtlS3.HasRows)
                {
                    while (drDtlS3.Read())
                    {

                        lDtlS3.Add(new DRSign2()
                        {
                            EmpName = Sm.DrStr(drDtlS3, cDtlS3[0]),
                            PosName = Sm.DrStr(drDtlS3, cDtlS3[1]),
                        });
                    }
                }
                drDtlS3.Close();
            }
            myLists.Add(lDtlS3);


            #endregion

            Sm.PrintReport("DRIMS", myLists, TableName, false);
        }


        private void DownloadFileKu(string TxtFile)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD1.FileName = TxtFile;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (SFD1.ShowDialog() == DialogResult.OK)
            {
                Application.DoEvents();

                //Write the bytes to a file
                FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                newFile.Write(downloadedData, 0, downloadedData.Length);
                newFile.Close();
                MessageBox.Show("Saved Successfully");
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;


                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnCtShippingAddress_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(SetLueVdCode));
            ClearData(3);
            if (Sm.GetLue(LueVdCode) != null)
                SetLueDriver(ref LueDriver, Sm.GetLue(LueVdCode));
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
            ClearData(2);
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(SetLueCtCode));
            if (Sm.GetLue(LueCtCode) != null)
            {
                if (Sm.GetLue(LueCtCode) != Sm.GetParameter("OnlineCtCode"))
                {
                    SetLueShippingAddress(ref LueShipAdd, Sm.GetLue(LueCtCode));
                    SetLueAgtCode(ref LueAgtCode, Sm.GetLue(LueCtCode));
                }
                else
                {
                    SetLueShippingAddress(ref LueShipAdd, Sm.GetLue(LueCtCode));
                    SetLueAgtCode(ref LueAgtCode, Sm.GetLue(LueCtCode));
                    //Lock online customer
                    //LueShipAdd.Properties.ReadOnly = true;
                }
            }

        }

        private void LueAgtCode_EditValueChanged(object sender, EventArgs e)
        {

            Sm.ClearGrd(Grd1, true);
            Sm.RefreshLookUpEdit(LueAgtCode, new Sm.RefreshLue2(SetLueAgtCode), Sm.GetLue(LueCtCode));
        }


        private void LueShipAdd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShipAdd, new Sm.RefreshLue2(SetLueShippingAddress), Sm.GetLue(LueCtCode));
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData(2);
                ShowShippingAddressData(Sm.GetLue(LueCtCode), Sm.GetLue(LueShipAdd));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueDriver_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDriver, new Sm.RefreshLue2(SetLueDriver), Sm.GetLue(LueVdCode));
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ShowExpeditionData(Sm.GetLue(LueVdCode), Sm.GetLue(LueDriver));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }     

        private void LueTtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTtCode, new Sm.RefreshLue1(Sl.SetLueTTCode));
        }

        private void LueShipAdd_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        {
            (LueShipAdd.Properties.DataSource as List<Lue1>).Add
              (
                  new Lue1
                  {
                      Col1 = e.DisplayValue.ToString()
                  }
              );

            e.Handled = true;
        }

        private void LueDriver_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        {
            (LueDriver.Properties.DataSource as List<Lue1>).Add
             (
                 new Lue1
                 {
                     Col1 = e.DisplayValue.ToString()
                 }
             );

            e.Handled = true;
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LueItCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode, new Sm.RefreshLue1(SetLueItCode));

            if (Sm.GetLue(LueItCode).Length > 0)
            {
                Sm.SetControlReadOnly(DteUsageDt, false);
                return;
            }
        }

        #endregion

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            List<string> FileName = new List<string>();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.LocalDocNo, A.FileName1, A.FileName2, A.FileName3, A.FileName4, A.FileName5, ");
            SQL.AppendLine("A.FileName6, A.FileName7, A.FileName8, A.FileName9, A.FileName10, ");
            SQL.AppendLine("A.FileName11, A.FileName12, A.FileName13, A.FileName14, A.FileName15, ");
            SQL.AppendLine("A.FileName16, A.FileName17, A.FileName18, A.FileName19, A.FileName20 ");
            SQL.AppendLine("From TblAttachmentFile A ");
            SQL.AppendLine("Where A.LocalDocNo=@LocalDocNo;");

            Sm.CmParam<String>(ref cm, "@localDocNo", TxtLocalDocNo.Text);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "FileName1", 
                        //1-5
                        "FileName2", "FileName3", "FileName4", "FileName5", "FileName6",  
                        //6-10
                        "FileName7", "FileName8", "FileName9", "FileName10", "FileName11", 
                        //11-15
                        "FileName12", "FileName13", "FileName14", "FileName15", "FileName16", 
                        //16-19
                        "FileName17", "FileName18", "FileName19", "FileName20"                         
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (Sm.DrStr(dr, c[0]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[0]));
                        if (Sm.DrStr(dr, c[1]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[1]));
                        if (Sm.DrStr(dr, c[2]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[2]));
                        if (Sm.DrStr(dr, c[3]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[3]));
                        if (Sm.DrStr(dr, c[4]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[4]));
                        if (Sm.DrStr(dr, c[5]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[5]));
                        if (Sm.DrStr(dr, c[6]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[6]));
                        if (Sm.DrStr(dr, c[7]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[7]));
                        if (Sm.DrStr(dr, c[8]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[8]));
                        if (Sm.DrStr(dr, c[9]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[9]));
                        if (Sm.DrStr(dr, c[10]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[10]));
                        if (Sm.DrStr(dr, c[11]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[11]));
                        if (Sm.DrStr(dr, c[12]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[12]));
                        if (Sm.DrStr(dr, c[13]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[13]));
                        if (Sm.DrStr(dr, c[14]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[14]));
                        if (Sm.DrStr(dr, c[15]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[15]));
                        if (Sm.DrStr(dr, c[16]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[16]));
                        if (Sm.DrStr(dr, c[17]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[17]));
                        if (Sm.DrStr(dr, c[18]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[18]));
                        if (Sm.DrStr(dr, c[19]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[19]));
                    }, false
                );
            if (FileName.Count > 0)
            {
                for (int i = 0; i < FileName.Count; i++)
                {
                    DownloadFileKu(FileName[i]);
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "No Attachment File");
            }
        }

        #endregion

        #region Report Class

        class DRHdr
        {
            public string CompanyLogo { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }
            public string Customer { get; set; }
            public string ExpVd { get; set; }
            public string ExpDriver { get; set; }
            public string ExpPlatNo { get; set; }
            public string ExpTT { get; set; }
            public string DocNoDR { get; set; }
            public string ItCode { get; set; }
            public string UsageDt { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string CityName { get; set; }
            public string CntName { get; set; }
            public string SAPostalCd { get; set; }
            public string SAPhone { get; set; }
            public string SAFax { get; set; }
            public string SAEmail { get; set; }
            public string SAMobile { get; set; }
            public string UserName { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string PONo { get; set; }
            public string LocalDocNo { get; set; }
        }

        class DRDtl
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string PackagingUnitUomCode { get; set; }
            public decimal QtyPackagingUnit { get; set; }
            public decimal QtyInventory { get; set; }
            public string InventoryUomCode { get; set; }
            public string Remark { get; set; }
            public string DNo { get; set; }
            public string ItCodeInternal { get; set; }
            public decimal Qty{ get; set; }
            public string DeliveryDt { get; set; }
            public string SOCNo { get; set; }
            public string ItNameDismantle { get; set; }

        }

        class DRDtl2
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string PackagingUnitUomCode { get; set; }
            public decimal QtyPackagingUnit { get; set; }
            public decimal QtyInventory { get; set; }
            public string InventoryUomCode { get; set; }
            public string Remark { get; set; }
            public string DNo { get; set; }

        }

        private class DRSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class DRSign2
        {
            public string EmpName { get; set; }
            public string PosName { get; set; }
        }

        private class DRSign3
        {
            public string EmpName { get; set; }
            public string PosName { get; set; }
        }


        #endregion
    }
}
