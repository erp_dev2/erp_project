﻿#region Update
/*
    18/08/2020 [ICA/MGI] new apps
    24/08/2020 [WED/MGI] DO diubah ke detail semua
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmCustomerPickupItemFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmCustomerPickupItem mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmCustomerPickupItemFind(FrmCustomerPickupItem FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                //GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch(Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT * FROM (");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT A.DocNo, A.DocDt, B.DOCtDocNo, E.WhsCode, E.WhsName, F.CtCode, F.CtName, ");
            SQL.AppendLine("    G.ItCode, G.ItName, B.CancelInd, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("    FROM tblcustomerpickupitemhdr A ");
            SQL.AppendLine("    INNER JOIN tblcustomerpickupitemdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN tbldocthdr C ON C.DocNo = B.DOCtDocNo ");
            SQL.AppendLine("    INNER JOIN tbldoctdtl D ON D.DocNo = C.DocNo And B.DOCtDNo = D.DNo ");
            SQL.AppendLine("    INNER JOIN tblwarehouse E ON E.WhsCode = C.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=E.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    INNER JOIN tblcustomer F ON F.CtCode = C.CtCode ");
            SQL.AppendLine("    INNER JOIN tblitem G ON G.ItCode = D.ItCode ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT A.DocNo, A.DocDt, B.DOCtDocNo, E.WhsCode, E.WhsName, F.CtCode, F.CtName, ");
            SQL.AppendLine("    G.ItCode, G.ItName, B.CancelInd, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("    FROM tblcustomerpickupitemhdr A ");
            SQL.AppendLine("    INNER JOIN tblcustomerpickupitemdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN tbldoct2hdr C ON C.DocNo = B.DOCtDocNo ");
            SQL.AppendLine("    INNER JOIN tbldoct2dtl D ON D.DocNo = C.DocNo And B.DOCtDNo = D.DNo ");
            SQL.AppendLine("    INNER JOIN tblwarehouse E ON E.WhsCode = C.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=E.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    INNER JOIN tblcustomer F ON F.CtCode = C.CtCode ");
            SQL.AppendLine("    INNER JOIN tblitem G ON G.ItCode = D.ItCode ");
            SQL.AppendLine(") ");
            SQL.AppendLine(")Tbl ");
            SQL.AppendLine("WHERE Tbl.DocDt BETWEEN @DocDt1 AND @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "Date",
                    "DO#",
                    "Warehouse's"+Environment.NewLine+"Code",
                    "Warehouse's"+Environment.NewLine+"Name",

                    //6-10
                    "Customer's"+Environment.NewLine+"Code",
                    "Customer's"+Environment.NewLine+"Name",
                    "Item's"+Environment.NewLine+"Code",
                    "Item's"+Environment.NewLine+"Name",
                    "Cancel",

                    //11-15
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date", 

                    //16
                    "Last"+Environment.NewLine+"Update Time"
                }, new int[]
                {
                    //0
                    50,

                    //1-5
                    130, 80, 130, 130, 200, 

                    //6-10
                    130, 200, 130, 200, 50,

                    //11-15
                    120, 120, 120, 120, 120, 

                    //16
                    120
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 10 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 12, 15 });
            Sm.GrdFormatTime(Grd1, new int[] { 13, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 8, 11, 12, 13, 14, 15, 16 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 8, 11, 12, 13, 14, 15, 16 }, !ChkHideInfoInGrd.Checked);
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "Tbl.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtDONo.Text, new string[] { "Tbl.DOCtDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "Tbl.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "Tbl.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "Tbl.ItCode", "Tbl.ItName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter,
                    new string[]
                    {
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocDt", "DOCtDocNo", "WhsCode", "WhsName", "CtCode",

                        //6-10
                        "CtName", "ItCode", "ItName", "CancelInd", "CreateBy",

                        //11-13
                        "CreateDt", "LastUpBy", "LastUpDt"
                    }, (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 13);
                    },true, false, false, false
                );

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        //protected override void GetParameter()
        //{

        //}
        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO#");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChlWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
