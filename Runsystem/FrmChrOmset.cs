﻿#region Update
/*
    08/09/2017 [wed] rubah tampilan nilai omset
    03/01/2018 [Ari] koma dibelakang nilai chart dihilangkan
    03/01/2018 [Ari] menggunakan time range di dalam pie chart(date)
    05/01/2018 [ARI] tambah filter subcategory, itemgroup
    09/01/2018 [WED] ubah nama menu
    19/01/2018 [WED] filter item group dan item sub category, isinya menyesuaikan item yang muncul di pie nya
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using MySql.Data.MySqlClient;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using Syncfusion.Windows;
using Syncfusion.Windows.Forms.Chart;
using SyXL = Syncfusion.XlsIO;
using SyDoc = Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmChrOmset : RunSystem.FrmBase10
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            exportFileName = string.Empty,
            file = string.Empty;

        #endregion

        #region Constructor

        public FrmChrOmset(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            BtnExcel.Visible = BtnPDF.Visible = BtnWord.Visible = false;
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
            SetLueCategoryCode(ref LueCategoryCode);
            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTotalOmset }, true);
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtTotalOmset }, 0);

            this.Chart.Title.Text = Sm.GetValue("Select IfNull(MenuDesc, '') as MenuDesc From TblMenu Where Param = 'FrmChrOmset' Limit 1;");

            base.FrmLoad(sender, e);
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            try
            {
            if (
                    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                    Sm.IsLueEmpty(LueCategoryCode, "Category")
                    ) return;

                ChartAppearance.ApplyChartStyles(this.Chart);
               // LoadData(string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)), Sm.GetLue(LueCategoryCode));
                LoadData(Sm.GetLue(LueCategoryCode));
                //ComputeTotalOmset();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Info, Exc.Message);
            }
        }

        private void LoadData(string CategoryCode)
        {
            var l = new List<Omset>();
            var cm = new MySqlCommand();

            if (CategoryCode == "1")
            {
                GetSQLItemSubCategory();
            }
            else if (CategoryCode == "2")
            {
                GetSQLItemGroup();
            }
            else if (CategoryCode == "3")
            {
                GetSQLItem();
            }

            l.Clear();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = mSQL;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]

                {
                    //0
                    "Item",

                    //1
                    "TotalOmset"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Omset()
                        {
                            Item = Sm.DrStr(dr, c[0]),
                            TotalOmset = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                else
                    Sm.StdMsg(mMsgType.NoData, "");

                dr.Close();
            }

            BindChart(ref l);

            decimal totomset = 0m;

            for (int x = 0; x < l.Count; x++)
            {
                totomset += decimal.Parse(l[x].TotalOmset);
            }

            TxtTotalOmset.EditValue = Sm.FormatNum(totomset, 0);
        }

        private void BindChart(ref List<Omset> l)
        {
            this.Chart.Series.Clear();
            ChartSeries series = new ChartSeries(this.Chart.Title.Text, ChartSeriesType.Pie);

            //series.Points.Add("ffs", 1);
            //series.Points.Add("dd", 3);
            //series.Points.Add("aa", 4);
            //series.Points.Add("bb", 14);
            //series.Points.Add("vv", 4);
            //series.Points.Add("ww", 3);
            //series.Points.Add("tt", 5);
            //series.Points.Add("yy", 5);
            //series.Points.Add("uu", 2);

            l.ForEach(i =>
            { series.Points.Add(i.Item, Convert.ToDouble(i.TotalOmset)); });

            series.Style.DisplayText = true;
            series.Style.TextOffset = 5;
            //series.ConfigItems.PieItem.LabelStyle = ChartAccumulationLabelStyle.Inside;
            for (int i = 0; i < l.Count; i++)
            {
                series.Styles[i].TextFormat = series.Points[i].Category.ToString();
            }
            
            //Chart.Legend.Visible = true;

            Chart.Series.Add(series);
        }

        #endregion

        #region Additional Method

        protected void OpenFile(string filetype, string exportFileName)
        {
            try
            {
                //if (filetype == "Grid")
                //    gridForm.ShowDialog();
                //else
                System.Diagnostics.Process.Start(exportFileName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void SetLueCategoryCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.OptCode As Col1, T.OptDesc As Col2 ");
            SQL.AppendLine("From TblOption T ");
            SQL.AppendLine("Where T.OptCat = 'DashboardOmsetCategory' ");
            SQL.AppendLine("Order By T.OptCode; ");

            cm.CommandText = SQL.ToString();

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private string SubQuery1()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("    Select A.DocNo, B.DocDt, A.ItCode, D.ItName, D.ItScCode, E.ItScName, D.ItGrpCode, F.ItGrpName, ");
            SQL.AppendLine("    A.Qty, IfNull(C1.UPrice, C2.UPrice) UPriceAfterDiscount, ");
            SQL.AppendLine("    (A.Qty * IfNull(C1.UPrice, C2.UPrice)) TotalOmset ");
            SQL.AppendLine("    From tbldoct2dtl A ");
            SQL.AppendLine("    Inner Join tbldoct2hdr B On A.DocNo = B.DocNo ");

            SQL.AppendLine("    -- DR ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select T1.DocNo, T5.ItCode, T4.UPrice ");
            SQL.AppendLine("        From TblDRDtl T1 ");
            SQL.AppendLine("        Inner Join TblSOHdr T2 On T1.SODocNo = T2.DocNo ");
            SQL.AppendLine("        Inner Join TblSODtl T3 On T1.SODocNo = T3.DocNo And T1.SODNo = T3.DNo ");
            SQL.AppendLine("        Inner Join TblCtQtDtl T4 On T2.CtQtDocNo = T4.DocNo And T3.CtQtDNo = T4.DNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl T5 On T4.ItemPriceDocNo = T5.DocNo And T4.ItemPriceDNo = T5.DNo ");
            SQL.AppendLine("    ) C1 On B.DRDocNo = C1.DocNo And C1.ItCode = A.ItCode ");

            SQL.AppendLine("    -- PL ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select T1.DocNo, T1.SectionNo, T1.ItCode, T4.UPrice ");
            SQL.AppendLine("        From TblPLDtl T1 ");
            SQL.AppendLine("        Inner Join TblSOHdr T2 On T1.SODocNo = T2.DocNo ");
            SQL.AppendLine("        Inner Join TblSODtl T3 On T1.SODocNo = T3.DocNo And T1.SODNo = T3.DNo ");
            SQL.AppendLine("        Inner Join TblCtQtDtl T4 On T2.CtQtDocNo = T4.DocNo And T3.CtQtDNo = T4.DNo ");
            SQL.AppendLine("    ) C2 On B.PLDocNo = C2.DocNo And B.SectionNo = C2.SectionNo And A.ItCode = C2.ItCode ");

            SQL.AppendLine("    Inner Join TblItem D On A.ItCode = D.ItCode ");
            SQL.AppendLine("    Left Join TblItemSubCategory E On D.ItScCode = E.ItScCode ");
            SQL.AppendLine("    Left Join TblItemGroup F On D.ItGrpCode = F.ItGrpCode ");
            SQL.AppendLine("    Where (B.DocDt Between '" + Sm.Left(Sm.GetDte(DteDocDt1), 8) + "' And '" + Sm.Left(Sm.GetDte(DteDocDt2), 8) + "') ");

            return SQL.ToString();
        }

        private string SubQuery2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.TrnNo As DocNo, A.BsDate As DocDt, B.ItCode, C.ItName, C.ItScCode, D.ItScName, C.ItGrpCode, ");
            SQL.AppendLine("    E.ItGrpName, B.Qty, (B.UPrice + B.DiscAmt) As UPriceAfterDiscount, ");
            SQL.AppendLine("    (B.Qty * (B.UPrice + B.DiscAmt)) As TotalOmset ");
            SQL.AppendLine("    From TblPosTrnHdr A ");
            SQL.AppendLine("    Inner Join TblPosTrnDtl B On A.TrnNo = B.TrnNo And A.BsDate = B.BsDate And A.PosNo = B.PosNo ");
            SQL.AppendLine("    Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("    Left Join TblItemSubCategory D On C.ItScCode = D.ItScCode ");
            SQL.AppendLine("    Left Join TblItemGroup E On C.ItGrpCode = E.ItGrpCode ");
            SQL.AppendLine("    Where (A.BsDate Between '" + Sm.Left(Sm.GetDte(DteDocDt1), 8) + "' And '" + Sm.Left(Sm.GetDte(DteDocDt2), 8) + "') ");

            return SQL.ToString();
        }

        private string SubQueryOther1()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("    Select A.ItCode, D.ItGrpCode, ");
            SQL.AppendLine("    (A.Qty * IfNull(C1.UPrice, C2.UPrice)) TotalOmset ");
            SQL.AppendLine("    From tbldoct2dtl A ");
            SQL.AppendLine("    Inner Join tbldoct2hdr B On A.DocNo = B.DocNo ");

            SQL.AppendLine("    -- DR ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select T1.DocNo, T5.ItCode, T4.UPrice ");
            SQL.AppendLine("        From TblDRDtl T1 ");
            SQL.AppendLine("        Inner Join TblSOHdr T2 On T1.SODocNo = T2.DocNo ");
            SQL.AppendLine("        Inner Join TblSODtl T3 On T1.SODocNo = T3.DocNo And T1.SODNo = T3.DNo ");
            SQL.AppendLine("        Inner Join TblCtQtDtl T4 On T2.CtQtDocNo = T4.DocNo And T3.CtQtDNo = T4.DNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl T5 On T4.ItemPriceDocNo = T5.DocNo And T4.ItemPriceDNo = T5.DNo ");
            SQL.AppendLine("    ) C1 On B.DRDocNo = C1.DocNo And C1.ItCode = A.ItCode ");

            SQL.AppendLine("    -- PL ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select T1.DocNo, T1.SectionNo, T1.ItCode, T4.UPrice ");
            SQL.AppendLine("        From TblPLDtl T1 ");
            SQL.AppendLine("        Inner Join TblSOHdr T2 On T1.SODocNo = T2.DocNo ");
            SQL.AppendLine("        Inner Join TblSODtl T3 On T1.SODocNo = T3.DocNo And T1.SODNo = T3.DNo ");
            SQL.AppendLine("        Inner Join TblCtQtDtl T4 On T2.CtQtDocNo = T4.DocNo And T3.CtQtDNo = T4.DNo ");
            SQL.AppendLine("    ) C2 On B.PLDocNo = C2.DocNo And B.SectionNo = C2.SectionNo And A.ItCode = C2.ItCode ");

            SQL.AppendLine("    Inner Join TblItem D On A.ItCode = D.ItCode ");
            SQL.AppendLine("    Left Join TblItemSubCategory E On D.ItScCode = E.ItScCode ");
            SQL.AppendLine("    Left Join TblItemGroup F On D.ItGrpCode = F.ItGrpCode ");
            SQL.AppendLine("    Where (B.DocDt Between '" + Sm.Left(Sm.GetDte(DteDocDt1), 8) + "' And '" + Sm.Left(Sm.GetDte(DteDocDt2), 8) + "') ");

            return SQL.ToString();
        }

        private string SubQueryOther2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select B.ItCode, C.ItGrpCode, ");
            SQL.AppendLine("    (B.Qty * (B.UPrice + B.DiscAmt)) As TotalOmset ");
            SQL.AppendLine("    From TblPosTrnHdr A ");
            SQL.AppendLine("    Inner Join TblPosTrnDtl B On A.TrnNo = B.TrnNo And A.BsDate = B.BsDate And A.PosNo = B.PosNo ");
            SQL.AppendLine("    Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("    Left Join TblItemSubCategory D On C.ItScCode = D.ItScCode ");
            SQL.AppendLine("    Left Join TblItemGroup E On C.ItGrpCode = E.ItGrpCode ");
            SQL.AppendLine("    Where (A.BsDate Between '" + Sm.Left(Sm.GetDte(DteDocDt1), 8) + "' And '" + Sm.Left(Sm.GetDte(DteDocDt2), 8) + "') ");

            return SQL.ToString();
        }

        private string GetSQLItem()
        {
            string DocDt1 = Sm.Left(Sm.GetDte(DteDocDt1), 8);
            string DocDt2 = Sm.Left(Sm.GetDte(DteDocDt2), 8);
            var SQL = new StringBuilder();
            var TopTen = Sm.GetValue(GetTopTen());
            string[] Tops = TopTen.Split(',');

            SQL.AppendLine("Select X.ItCode, Concat(X.Item, ' : ', Convert(Format(X.TotalOmset, 0) using utf8)) As Item, Convert(Format(X.TotalOmset, 4) using utf8) TotalOmset From ( ");
            SQL.AppendLine("Select T.ItCode, T.ItName As Item, Sum(T.TotalOmset) TotalOmset From ( ");
            SQL.AppendLine(SubQuery1());
            SQL.AppendLine(SubQuery2());
            SQL.AppendLine(")T ");
            SQL.AppendLine("Group By T.ItCode ");
            SQL.AppendLine("Order By Sum(T.TotalOmset) Desc ");
            SQL.AppendLine("Limit 10 ");
            SQL.AppendLine(")X ");
            if (TopTen.Length > 0 && Tops.Length >= 10)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select 'Other' As ItCode, Concat('Other : ', Convert(Format(Sum(T.TotalOmset), 0) using utf8)) As Item, Convert(Format(Sum(T.TotalOmset), 4) using utf8) TotalOmset From ( ");
                SQL.AppendLine(SubQueryOther1());
                SQL.AppendLine(SubQueryOther2());
                SQL.AppendLine(")T ");
                SQL.AppendLine("Where Not Find_In_Set(T.ItCode, '" + TopTen + "') ");
            }

            mSQL = SQL.ToString();
            return mSQL;
        }

        private string GetSQLItemSubCategory()
        {
            string DocDt1 = Sm.Left(Sm.GetDte(DteDocDt1), 8);
            string DocDt2 = Sm.Left(Sm.GetDte(DteDocDt2), 8);
            var SQL = new StringBuilder();
            
            if (Sm.GetLue(LueFilterCategory).Length > 0)
                SQL.AppendLine("Select T.ItScCode, Concat(T.ItName, ' : ', Convert(Format(Sum(T.TotalOmset), 0) using utf8)) As Item, Sum(T.TotalOmset) TotalOmset From ( ");
            else
            SQL.AppendLine("Select T.ItScCode, Concat(T.ItScName, ' : ', Convert(Format(Sum(T.TotalOmset), 0) using utf8)) As Item, Sum(T.TotalOmset) TotalOmset From ( ");

            SQL.AppendLine(SubQuery1());

            if(Sm.GetLue(LueFilterCategory).Length>0)
                SQL.AppendLine("    And D.ItScCode='" + Sm.GetLue(LueFilterCategory) + "'  ");

            SQL.AppendLine(SubQuery2());
            
            if (Sm.GetLue(LueFilterCategory).Length > 0)
                SQL.AppendLine("    And D.ItScCode='"+Sm.GetLue(LueFilterCategory)+"'  ");
            
            SQL.AppendLine(")T ");
            
            if (Sm.GetLue(LueFilterCategory).Length > 0)
                SQL.AppendLine("Group By T.ItCode ");
            else
            SQL.AppendLine("Group By T.ItScCode ");
            
            SQL.AppendLine("Order By Sum(T.TotalOmset) Desc ");
            SQL.AppendLine("Limit 11 ");

            mSQL = SQL.ToString();
            return mSQL;
        }

        private string GetSQLItemGroup()
        {
            string DocDt1 = Sm.Left(Sm.GetDte(DteDocDt1), 8);
            string DocDt2 = Sm.Left(Sm.GetDte(DteDocDt2), 8);
            var SQL = new StringBuilder();
            var TopTen = Sm.GetValue(GetTopTen());

            string[] Tops = TopTen.Split(',');

            SQL.AppendLine("Select X.ItGrpCode, Concat(X.Item, ' : ', Convert(Format(X.TotalOmset, 0) using utf8)) As Item, Convert(Format(X.TotalOmset, 4) using utf8) TotalOmset From ( ");
            if (Sm.GetLue(LueFilterCategory).Length > 0)//Ari
                SQL.AppendLine("Select T.ItGrpCode, T.ItName As Item, Sum(T.TotalOmset) TotalOmset From ( ");
            else
            SQL.AppendLine("Select T.ItGrpCode, T.ItGrpName As Item, Sum(T.TotalOmset) TotalOmset From ( ");

            SQL.AppendLine(SubQuery1());
            
            if (Sm.GetLue(LueFilterCategory).Length > 0)
                SQL.AppendLine("    And D.ItGrpCode='" + Sm.GetLue(LueFilterCategory) + "'  ");

            SQL.AppendLine(SubQuery2());
            
            if (Sm.GetLue(LueFilterCategory).Length > 0)
                SQL.AppendLine("    And C.ItGrpCode='" + Sm.GetLue(LueFilterCategory) + "'  ");
            
            SQL.AppendLine(")T ");
            
            if (Sm.GetLue(LueFilterCategory).Length > 0)
                SQL.AppendLine("Group By T.ItCode ");
            else
            SQL.AppendLine("Group By T.ItGrpCode ");

            SQL.AppendLine("Order By Sum(T.TotalOmset) Desc ");
            SQL.AppendLine("Limit 10 ");
            SQL.AppendLine(")X ");
            if (TopTen.Length > 0 && Tops.Length == 10)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select 'Other' As ItGrpCode, Concat('Other : ', Convert(Format(Sum(T.TotalOmset), 0) using utf8)) As Item, Convert(Format(Sum(T.TotalOmset), 4) using utf8) TotalOmset From ( ");
                SQL.AppendLine(SubQueryOther1());

                if (Sm.GetLue(LueFilterCategory).Length > 0)
                    SQL.AppendLine("    And D.ItGrpCode='" + Sm.GetLue(LueFilterCategory) + "'  ");

                SQL.AppendLine(SubQueryOther2());

                if (Sm.GetLue(LueFilterCategory).Length > 0)
                    SQL.AppendLine("    And C.ItGrpCode='" + Sm.GetLue(LueFilterCategory) + "'  ");
                
                SQL.AppendLine(")T ");

                if ((Sm.GetLue(LueFilterCategory).Length > 0 && Sm.GetLue(LueCategoryCode) == "2") || Sm.GetLue(LueCategoryCode) == "3")
                    SQL.AppendLine("Where Not Find_In_Set(T.ItCode, '" + TopTen + "') ");
                else
                    SQL.AppendLine("Where Not Find_In_Set(T.ItGrpCode, '" + TopTen + "') ");
            }

            mSQL = SQL.ToString();
            return mSQL;
        }

        private string GetTopTen()
        {
            string DocDt1 = Sm.Left(Sm.GetDte(DteDocDt1),8);
            string DocDt2 = Sm.Left(Sm.GetDte(DteDocDt2),8);
            var SQL = new StringBuilder();

            if (Sm.GetLue(LueCategoryCode) == "2" || Sm.GetLue(LueCategoryCode) == "3")
            {
                if ((Sm.GetLue(LueCategoryCode) == "2" && Sm.GetLue(LueFilterCategory).Length > 0) || Sm.GetLue(LueCategoryCode) == "3")
                {
                    SQL.AppendLine("Select Group_Concat(X.ItCode) ItCode From ( ");
                }
                else
                {
                    SQL.AppendLine("Select Group_Concat(X.ItGrpCode) ItGrpCode From ( ");
                }

                SQL.AppendLine("    Select T.ItCode, T.ItGrpCode, T.ItScCode, T.ItName as Item, Sum(T.TotalOmset) TotalOmset From ( ");
                
                SQL.AppendLine(SubQuery1());

                if (Sm.GetLue(LueFilterCategory).Length > 0 && Sm.GetLue(LueCategoryCode) == "2")
                    SQL.AppendLine("    And D.ItGrpCode='" + Sm.GetLue(LueFilterCategory) + "'  ");

                SQL.AppendLine(SubQuery2());
                
                if (Sm.GetLue(LueFilterCategory).Length > 0 && Sm.GetLue(LueCategoryCode) == "2")
                    SQL.AppendLine("    And C.ItGrpCode='" + Sm.GetLue(LueFilterCategory) + "'  ");
                
                SQL.AppendLine("    )T ");

                if (Sm.GetLue(LueCategoryCode) == "3" || (Sm.GetLue(LueCategoryCode) == "2" && Sm.GetLue(LueFilterCategory).Length > 0))
                    SQL.AppendLine("    Group By T.ItCode ");
                else
                    SQL.AppendLine("    Group By T.ItGrpCode ");

                SQL.AppendLine("    Order By Sum(T.TotalOmset) Desc ");
                SQL.AppendLine("    Limit 10 ");
                SQL.AppendLine(")X ");
            }

            return SQL.ToString();

        }

        private void ComputeTotalOmset()

        {
            string DocDt1 = Sm.Left(Sm.GetDte(DteDocDt1), 8);
            string DocDt2 = Sm.Left(Sm.GetDte(DteDocDt2), 8);

            var SQL = new StringBuilder();

            SQL.AppendLine("    Select Sum(T.TotalOmset) TotalOmset From ( ");
            SQL.AppendLine(SubQuery1());

            if (Sm.GetLue(LueFilterCategory).Length > 0)
            {
                if (Sm.GetLue(LueCategoryCode) == "1")
                {
                    SQL.AppendLine("    And D.ItScCode='" + Sm.GetLue(LueFilterCategory) + "'  ");
                }
                if (Sm.GetLue(LueCategoryCode) == "2")
                {
                    SQL.AppendLine("    And D.ItGrpCode='" + Sm.GetLue(LueFilterCategory) + "'  ");
                }
            }

            SQL.AppendLine(SubQuery2());

            if (Sm.GetLue(LueFilterCategory).Length > 0)
            {
                if (Sm.GetLue(LueCategoryCode) == "1")
                {
                    SQL.AppendLine("    And C.ItScCode='" + Sm.GetLue(LueFilterCategory) + "'  ");
                }
                if (Sm.GetLue(LueCategoryCode) == "2")
                {
                    SQL.AppendLine("    And C.ItGrpCode='" + Sm.GetLue(LueFilterCategory) + "'  ");
                }
            }
            
            SQL.AppendLine("    )T ");

            var cm = new MySqlCommand();

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "TotalOmset"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtTotalOmset.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[0]), 0);
                }, true
            );
        }

        private void SetLueItScCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.ItScCode As Col1, T.ItScName As Col2 From ( ");
            SQL.AppendLine(SubQuery1());
            SQL.AppendLine(SubQuery2());
            SQL.AppendLine(")T ");
            SQL.AppendLine("Where T.ItScCode Is Not Null ");
            SQL.AppendLine("Group By T.ItScCode ");
            SQL.AppendLine("Order By T.ItScName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueNull(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue, "Select null As Col1, null as Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueItGrpCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.ItGrpCode As Col1, T.ItGrpName As Col2 From ( ");
            SQL.AppendLine(SubQuery1());
            SQL.AppendLine(SubQuery2());
            SQL.AppendLine(")T ");
            SQL.AppendLine("Where T.ItGrpCode Is Not Null ");
            SQL.AppendLine("Group By T.ItGrpCode ");
            SQL.AppendLine("Order By T.ItGrpName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Button Click

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            PrintDialog pr = new PrintDialog();
            PrintPreviewDialog ppd = new PrintPreviewDialog();

            if (Chart.Series.Count == 0)
                Sm.StdMsg(mMsgType.Info, "No Data");
            else
            {
                if (Chart.Series[0].Points.Count == 0)
                    Sm.StdMsg(mMsgType.Warning, "The chart is empty");
                else
                {
                    //var a = Chart.Series[0].Points[4].X;
                    //var b = Chart.Series[0].Points[4].YValues[0];

                    pr.AllowSomePages = true;
                    pr.AllowSelection = true;
                    pr.PrinterSettings.Clone();
                    pr.Document = Chart.PrintDocument;
                    if (pr.ShowDialog() == DialogResult.OK)
                        pr.Document.Print();
                }
            }

            //if (pr.ShowDialog() == DialogResult.OK)
            //{
            //    pr.Document = Chart.PrintDocument;
            //    ppd.Document = Chart.PrintDocument;
            //    if (ppd.ShowDialog() == DialogResult.OK)
            //        //pr.Document.Print();
            //        ppd.Document.Print();
            //}
        }

        private void BtnWord_Click(object sender, EventArgs e)
        {
            try
            {
                if (Chart.Series.Count == 0)
                    Sm.StdMsg(mMsgType.NoData, "");
                else
                {
                    if (Chart.Series[0].Points.Count == 0)
                        Sm.StdMsg(mMsgType.Info, "The chart is empty");
                    else
                    {
                        exportFileName = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".doc";
                        file = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".gif";

                        //Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + "_" + Sm.ConvertDate(Sm.GetDte(DteDocDt1)) + "_TO_" + Sm.ConvertDate(Sm.GetDte(DteDocDt2)) + ".gif";
                        //if (!System.IO.File.Exists(file))
                        Chart.SaveImage(file);

                        //Create a new document
                        WordDocument document = new WordDocument();
                        //Adding a new section to the document.
                        IWSection section = document.AddSection();
                        //Adding a paragraph to the section
                        IWParagraph paragraph = section.AddParagraph();
                        //Writing text.
                        paragraph.AppendText("Top Selling Product");
                        //Adding a new paragraph		
                        paragraph = section.AddParagraph();
                        paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                        //Inserting chart.
                        paragraph.AppendPicture(Image.FromFile(file));
                        //Save the Document to disk.
                        document.Save(exportFileName, Syncfusion.DocIO.FormatType.Doc);
                        System.Diagnostics.Process.Start(exportFileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            if (Chart.Series.Count == 0)
                Sm.StdMsg(mMsgType.NoData, "");
            else
            {
                if (Chart.Series[0].Points.Count == 0)
                    Sm.StdMsg(mMsgType.Info, "The chart is empty.");
                else
                {
                    exportFileName = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".xls";

                    // A new workbook with a worksheet should be created. 
                    SyXL.IWorkbook chartBook = SyXL.ExcelUtils.CreateWorkbook(1);
                    SyXL.IWorksheet sheet = chartBook.Worksheets[0];

                    //if chart is not empty.
                    // Fill the worksheet with chart data. 
                    for (int i = 1; i <= Chart.Series[0].Points.Count; i++)
                    {
                        sheet.Range[i, 1].Number = Chart.Series[0].Points[i - 1].X;
                        sheet.Range[i, 2].Number = Chart.Series[0].Points[i - 1].YValues[0];
                    }

                    // Create a chart worksheet. 
                    SyXL.IChart chart = chartBook.Charts.Add(Chart.Title.Text);

                    // Specify the title of the Chart.
                    chart.ChartTitle = Chart.Title.Text;

                    // Initialize a new series instance and add it to the series collection of the chart. 
                    SyXL.IChartSerie series = chart.Series.Add("Top Selling Product");

                    // Specify the chart type of the series. 
                    series.SerieType = SyXL.ExcelChartType.Column_Clustered;

                    // Specify the name of the series. This will be displayed as the text of the legend. 
                    //series.Name = Chart.Name;

                    // Specify the value ranges for the series. 
                    series.Values = sheet.Range["B1:B10"];

                    // Specify the Category labels for the series. 
                    series.CategoryLabels = sheet.Range["A1:A10"];

                    // Make the chart as active sheet. 
                    chart.Activate();

                    // Save the Chart book. 
                    chartBook.SaveAs(exportFileName); chartBook.Close();
                    SyXL.ExcelUtils.Close();

                    // Launches the file. 
                    System.Diagnostics.Process.Start(exportFileName);
                }
            }
        }

        private void BtnPDF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Chart.Series.Count == 0)
                    Sm.StdMsg(mMsgType.NoData, "");
                else
                {
                    if (Chart.Series[0].Points.Count == 0)
                        Sm.StdMsg(mMsgType.Info, "The chart is empty");
                    else
                    {
                        exportFileName = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".pdf";
                        file = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".gif";

                        //if (!System.IO.File.Exists(file))
                        this.Chart.SaveImage(file);

                        //Create a new PDF Document. The pdfDoc object represents the PDF document.
                        //This document has one page by default and additional pages have to be added.
                        PdfDocument pdfDoc = new PdfDocument();

                        pdfDoc.Pages.Add();

                        pdfDoc.Pages[0].Graphics.DrawImage(PdfImage.FromFile(file), new PointF(10, 30));

                        //Save the PDF Document to disk.
                        pdfDoc.Save(exportFileName);
                        OpenFile("Pdf", exportFileName);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
            
            if(Sm.GetDte(DteDocDt1).Length > 0)
            {
                if (Sm.GetLue(LueCategoryCode).Length > 0)
                {
                    if (Sm.GetLue(LueCategoryCode) == "1")
                    {
                        SetLueItScCode(ref LueFilterCategory);
                    }
                    if (Sm.GetLue(LueCategoryCode) == "2")
                    {
                        SetLueItGrpCode(ref LueFilterCategory);
                    }
                }
            }
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;

            if (Sm.GetDte(DteDocDt2).Length > 0)
            {
                if (Sm.GetLue(LueCategoryCode).Length > 0)
                {
                    if (Sm.GetLue(LueCategoryCode) == "1")
                    {
                        SetLueItScCode(ref LueFilterCategory);
                    }
                    if (Sm.GetLue(LueCategoryCode) == "2")
                    {
                        SetLueItGrpCode(ref LueFilterCategory);
                    }
                }
            }
        }

        private void LueCategoryCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCategoryCode, new Sm.RefreshLue1(SetLueCategoryCode));
            var ItCtCode = Sm.GetLue(LueCategoryCode);
            if (ItCtCode.Length <= 0)
            {
                LblSubCategory.Text = "Filter";
                SetLueNull(ref LueFilterCategory);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueFilterCategory }, false);
            }
            if (ItCtCode == "1")
            {
                LblSubCategory.Text = "Sub Category";
                SetLueItScCode(ref LueFilterCategory);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueFilterCategory }, false);
            }
            if (ItCtCode == "2")
            {
                LblSubCategory.Text = "Item Group";
                SetLueItGrpCode(ref LueFilterCategory);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueFilterCategory }, false);
            }
            if (ItCtCode == "3")
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueFilterCategory });
                LblSubCategory.Text = "Item Name";
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueFilterCategory }, true);
            }
        }
   
        private void TxtTotalOmset_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTotalOmset, 0);
        }

        private void LueFilterCategory_EditValueChanged(object sender, EventArgs e)
        {
            var ItCtCode = Sm.GetLue(LueCategoryCode);
            if (ItCtCode == "1")
                Sm.RefreshLookUpEdit(LueFilterCategory, new Sm.RefreshLue1(SetLueItScCode));
            if (ItCtCode == "2")
                Sm.RefreshLookUpEdit(LueFilterCategory, new Sm.RefreshLue1(SetLueItGrpCode));
        }

        #endregion

        #endregion

        #region Class

        class Omset
        {
            public string Item { get; set; }
            public string TotalOmset { get; set; }
        }

        #endregion

    }
}
