﻿namespace RunSystem
{
    partial class FrmSalesInvoice6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDueDt = new DevExpress.XtraEditors.DateEdit();
            this.LueTaxCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TcSalesInvoice3 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.LueOption = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel9 = new System.Windows.Forms.Panel();
            this.LueAdvanceChargeCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.AdvanceCharge3 = new System.Windows.Forms.Label();
            this.LueAdvanceChargeCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.AdvanceCharge2 = new System.Windows.Forms.Label();
            this.LueAdvanceChargeCode = new DevExpress.XtraEditors.LookUpEdit();
            this.AdvanceCharge = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcSalesInvoice3)).BeginInit();
            this.TcSalesInvoice3.SuspendLayout();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.Tp3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueOption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueAdvanceChargeCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAdvanceChargeCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAdvanceChargeCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // LueFontSize
            // 
            this.LueFontSize.EditValue = "9";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LueTaxCode1);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DteDueDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Size = new System.Drawing.Size(772, 77);
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 77);
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(772, 396);
            this.Grd1.TabIndex = 9;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(14, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 14);
            this.label2.TabIndex = 5;
            this.label2.Text = "Sales Invoice\'s Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(133, 5);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(139, 20);
            this.DteDocDt.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(70, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 14);
            this.label1.TabIndex = 7;
            this.label1.Text = "Due Date";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDueDt
            // 
            this.DteDueDt.EditValue = null;
            this.DteDueDt.EnterMoveNextControl = true;
            this.DteDueDt.Location = new System.Drawing.Point(133, 27);
            this.DteDueDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDt.Name = "DteDueDt";
            this.DteDueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.Appearance.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDt.Size = new System.Drawing.Size(139, 20);
            this.DteDueDt.TabIndex = 8;
            // 
            // LueTaxCode1
            // 
            this.LueTaxCode1.EnterMoveNextControl = true;
            this.LueTaxCode1.Location = new System.Drawing.Point(133, 49);
            this.LueTaxCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode1.Name = "LueTaxCode1";
            this.LueTaxCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode1.Properties.DropDownRows = 30;
            this.LueTaxCode1.Properties.NullText = "[Empty]";
            this.LueTaxCode1.Properties.PopupWidth = 350;
            this.LueTaxCode1.Size = new System.Drawing.Size(306, 20);
            this.LueTaxCode1.TabIndex = 10;
            this.LueTaxCode1.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode1.EditValueChanged += new System.EventHandler(this.LueTaxCode1_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(101, 52);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 14);
            this.label4.TabIndex = 9;
            this.label4.Text = "Tax";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TcSalesInvoice3
            // 
            this.TcSalesInvoice3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcSalesInvoice3.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcSalesInvoice3.Location = new System.Drawing.Point(0, 77);
            this.TcSalesInvoice3.Name = "TcSalesInvoice3";
            this.TcSalesInvoice3.SelectedTabPage = this.Tp2;
            this.TcSalesInvoice3.Size = new System.Drawing.Size(772, 396);
            this.TcSalesInvoice3.TabIndex = 11;
            this.TcSalesInvoice3.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp2,
            this.Tp3});
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.Grd2);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 368);
            this.Tp2.Text = "General";
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 368);
            this.Grd2.TabIndex = 12;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.GrdEllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.GrdRequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GrdKeyDown);
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.LueOption);
            this.Tp3.Controls.Add(this.Grd3);
            this.Tp3.Controls.Add(this.panel9);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(766, 368);
            this.Tp3.Text = "Discount, Additional Cost, Etc";
            // 
            // LueOption
            // 
            this.LueOption.EnterMoveNextControl = true;
            this.LueOption.Location = new System.Drawing.Point(128, 97);
            this.LueOption.Margin = new System.Windows.Forms.Padding(5);
            this.LueOption.Name = "LueOption";
            this.LueOption.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.Appearance.Options.UseFont = true;
            this.LueOption.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueOption.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueOption.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueOption.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueOption.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueOption.Properties.DropDownRows = 20;
            this.LueOption.Properties.NullText = "[Empty]";
            this.LueOption.Properties.PopupWidth = 500;
            this.LueOption.Size = new System.Drawing.Size(281, 20);
            this.LueOption.TabIndex = 21;
            this.LueOption.ToolTip = "F4 : Show/hide list";
            this.LueOption.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueOption.EditValueChanged += new System.EventHandler(this.LueOption_EditValueChanged);
            this.LueOption.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueOption_KeyDown);
            this.LueOption.Leave += new System.EventHandler(this.LueOption_Leave);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 72);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(766, 296);
            this.Grd3.TabIndex = 20;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.LueAdvanceChargeCode3);
            this.panel9.Controls.Add(this.AdvanceCharge3);
            this.panel9.Controls.Add(this.LueAdvanceChargeCode2);
            this.panel9.Controls.Add(this.AdvanceCharge2);
            this.panel9.Controls.Add(this.LueAdvanceChargeCode);
            this.panel9.Controls.Add(this.AdvanceCharge);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(766, 72);
            this.panel9.TabIndex = 13;
            // 
            // LueAdvanceChargeCode3
            // 
            this.LueAdvanceChargeCode3.EnterMoveNextControl = true;
            this.LueAdvanceChargeCode3.Location = new System.Drawing.Point(103, 47);
            this.LueAdvanceChargeCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueAdvanceChargeCode3.Name = "LueAdvanceChargeCode3";
            this.LueAdvanceChargeCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode3.Properties.Appearance.Options.UseFont = true;
            this.LueAdvanceChargeCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAdvanceChargeCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAdvanceChargeCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAdvanceChargeCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAdvanceChargeCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAdvanceChargeCode3.Properties.DropDownRows = 30;
            this.LueAdvanceChargeCode3.Properties.NullText = "[Empty]";
            this.LueAdvanceChargeCode3.Properties.PopupWidth = 350;
            this.LueAdvanceChargeCode3.Size = new System.Drawing.Size(306, 20);
            this.LueAdvanceChargeCode3.TabIndex = 19;
            this.LueAdvanceChargeCode3.ToolTip = "F4 : Show/hide list";
            this.LueAdvanceChargeCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAdvanceChargeCode3.EditValueChanged += new System.EventHandler(this.LueAdvanceChargeCode3_EditValueChanged);
            // 
            // AdvanceCharge3
            // 
            this.AdvanceCharge3.AutoSize = true;
            this.AdvanceCharge3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdvanceCharge3.ForeColor = System.Drawing.Color.Black;
            this.AdvanceCharge3.Location = new System.Drawing.Point(5, 50);
            this.AdvanceCharge3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.AdvanceCharge3.Name = "AdvanceCharge3";
            this.AdvanceCharge3.Size = new System.Drawing.Size(96, 14);
            this.AdvanceCharge3.TabIndex = 18;
            this.AdvanceCharge3.Text = "Advance Charge";
            this.AdvanceCharge3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueAdvanceChargeCode2
            // 
            this.LueAdvanceChargeCode2.EnterMoveNextControl = true;
            this.LueAdvanceChargeCode2.Location = new System.Drawing.Point(103, 26);
            this.LueAdvanceChargeCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueAdvanceChargeCode2.Name = "LueAdvanceChargeCode2";
            this.LueAdvanceChargeCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode2.Properties.Appearance.Options.UseFont = true;
            this.LueAdvanceChargeCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAdvanceChargeCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAdvanceChargeCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAdvanceChargeCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAdvanceChargeCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAdvanceChargeCode2.Properties.DropDownRows = 30;
            this.LueAdvanceChargeCode2.Properties.NullText = "[Empty]";
            this.LueAdvanceChargeCode2.Properties.PopupWidth = 350;
            this.LueAdvanceChargeCode2.Size = new System.Drawing.Size(306, 20);
            this.LueAdvanceChargeCode2.TabIndex = 17;
            this.LueAdvanceChargeCode2.ToolTip = "F4 : Show/hide list";
            this.LueAdvanceChargeCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAdvanceChargeCode2.EditValueChanged += new System.EventHandler(this.LueAdvanceChargeCode2_EditValueChanged);
            // 
            // AdvanceCharge2
            // 
            this.AdvanceCharge2.AutoSize = true;
            this.AdvanceCharge2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdvanceCharge2.ForeColor = System.Drawing.Color.Black;
            this.AdvanceCharge2.Location = new System.Drawing.Point(5, 29);
            this.AdvanceCharge2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.AdvanceCharge2.Name = "AdvanceCharge2";
            this.AdvanceCharge2.Size = new System.Drawing.Size(96, 14);
            this.AdvanceCharge2.TabIndex = 16;
            this.AdvanceCharge2.Text = "Advance Charge";
            this.AdvanceCharge2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueAdvanceChargeCode
            // 
            this.LueAdvanceChargeCode.EnterMoveNextControl = true;
            this.LueAdvanceChargeCode.Location = new System.Drawing.Point(103, 5);
            this.LueAdvanceChargeCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueAdvanceChargeCode.Name = "LueAdvanceChargeCode";
            this.LueAdvanceChargeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode.Properties.Appearance.Options.UseFont = true;
            this.LueAdvanceChargeCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAdvanceChargeCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAdvanceChargeCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAdvanceChargeCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAdvanceChargeCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAdvanceChargeCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAdvanceChargeCode.Properties.DropDownRows = 30;
            this.LueAdvanceChargeCode.Properties.NullText = "[Empty]";
            this.LueAdvanceChargeCode.Properties.PopupWidth = 350;
            this.LueAdvanceChargeCode.Size = new System.Drawing.Size(306, 20);
            this.LueAdvanceChargeCode.TabIndex = 15;
            this.LueAdvanceChargeCode.ToolTip = "F4 : Show/hide list";
            this.LueAdvanceChargeCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAdvanceChargeCode.EditValueChanged += new System.EventHandler(this.LueAdvanceChargeCode_EditValueChanged);
            // 
            // AdvanceCharge
            // 
            this.AdvanceCharge.AutoSize = true;
            this.AdvanceCharge.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdvanceCharge.ForeColor = System.Drawing.Color.Black;
            this.AdvanceCharge.Location = new System.Drawing.Point(5, 8);
            this.AdvanceCharge.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.AdvanceCharge.Name = "AdvanceCharge";
            this.AdvanceCharge.Size = new System.Drawing.Size(96, 14);
            this.AdvanceCharge.TabIndex = 14;
            this.AdvanceCharge.Text = "Advance Charge";
            this.AdvanceCharge.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmSalesInvoice6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Controls.Add(this.TcSalesInvoice3);
            this.Name = "FrmSalesInvoice6";
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.Grd1, 0);
            this.Controls.SetChildIndex(this.TcSalesInvoice3, 0);
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcSalesInvoice3)).EndInit();
            this.TcSalesInvoice3.ResumeLayout(false);
            this.Tp2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.Tp3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueOption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueAdvanceChargeCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAdvanceChargeCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAdvanceChargeCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDueDt;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode1;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraTab.XtraTabControl TcSalesInvoice3;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.Panel panel9;
        private DevExpress.XtraEditors.LookUpEdit LueAdvanceChargeCode;
        private System.Windows.Forms.Label AdvanceCharge;
        private DevExpress.XtraEditors.LookUpEdit LueAdvanceChargeCode3;
        private System.Windows.Forms.Label AdvanceCharge3;
        private DevExpress.XtraEditors.LookUpEdit LueAdvanceChargeCode2;
        private System.Windows.Forms.Label AdvanceCharge2;
        private DevExpress.XtraEditors.LookUpEdit LueOption;
    }
}