﻿#region Update
/*
    12/11/2019 [WED/VIR] new apps
1. SettledAmt periode
2. VC Dropping Payment periode DocDt
3. VC ARDP
4. DP SLI
5. Amt SLI Periode
6. Amt Tax PPH SLI
7. Amt PPH Tax ARDP

 */
#endregion

#region Rule
/*
    Kode : Kode Proyek SO Contract
    Nama Project : Prooject Name
    Periode : DocDt SOContract s.d Delivery Dt SOContractDtl (paling akhir)
    Total Kontrak : SOCR terakhir (Amt)
    Kontrak Carry Over 2019 : (nilai SO - project delivery sebelum tahun berjalan)
	    if(LOP Resource != Loan (param baru untuk resource LOAN)) * 1.1 (param baru 1.1)
    Kontrak Luncuran 2019 : Total Project Delivery di tahun berjalan
	    if(LOP Resource != Loan (param baru untuk resource LOAN)) * 1.1 (param baru 1.1)
    Kontrak Baru 2019 : Total Project Delivery di tahun berjalan, yg SO Contract nya tahun berjalan
	    if(LOP Resource != Loan (param baru untuk resource LOAN)) * 1.1 (param baru 1.1)
    Kontrak Baru : SOCR terakhir yg DOcDt SOC nyadi tahun berjalan (Amt)
    Kontrak TW I : SOCR terakhir yg DOcDt SOC nya di (201901 s.d 201903) (Amt)
    Kontrak TW II :SOCR terakhir yg DOcDt SOC nya di (201904 s.d 201906) (Amt)
    Kontrak TW III :SOCR terakhir yg DOcDt SOC nya di (201907 s.d 201909) (Amt)
    Prognosa Kontrak TW IV : Nilai di LOP (EstAmt) yg Confident Level = (3, 4)
    Total Penjualan : Total Kontrak / 1.1 *******
    Penjualan s.d 2018 : Project DElivery sebelum 2019
	    if(LOP Resource != Loan (param baru untuk resource LOAN)) / 1.1 (param baru 1.1)
    % : Penjualan s.d 2018 / Total Penjualan
    Penjualan Carry Over 2019 : Kontrak Carry Over 2019
	    if(LOP Resource != Loan (param baru untuk resource LOAN)) / 1.1 (param baru 1.1)
    Penjualan Luncuran 2019 : Kontrak Luncuran 2019
	    if(LOP Resource != Loan (param baru untuk resource LOAN)) / 1.1 (param baru 1.1)
    Penjualan Baru 2019 : Kontrak Baru 2019
	    if(LOP Resource != Loan (param baru untuk resource LOAN)) / 1.1 (param baru 1.1)
    Penjualan baru yg dikelola 2019 : ********
    Penjualan Baru 2019 : Penjualan Baru 2019

    PROGRES PRESTASI PER BULAN
    TW I 2019 : Project Delivery setteledt (201901 s.d 201903) yg DocDt SO Contract tahun berjalan
    TW II 2019 : Project Delivery setteledt (201904 s.d 201906) yg DocDt SO Contract tahun berjalan
    TW III 2019 : Project Delivery setteledt (201907 s.d 201909) yg DocDt SO Contract tahun berjalan
    TW IV 2019 : Project Delivery setteledt (201910 s.d 201912) yg DocDt SO Contract tahun berjalan
    Penjualan s.d Sept 2019 : Project Delivery settledt < 201910 yg DocDt SO Contract tahun berjalan

    Penjualan Luncuran 2019 : Penjualan Luncuran 2019

    PROGRES PRESTASI PER BULAN 2
    TW I 2019 : Project Delivery setteledt (201901 s.d 201903) yg DocDt SO Contract sebelum tahun berjalan
    TW II 2019 : Project Delivery setteledt (201904 s.d 201906) yg DocDt SO Contract sebelum tahun berjalan
    TW III 2019 : Project Delivery setteledt (201907 s.d 201909) yg DocDt SO Contract sebelum tahun berjalan
    TW IV 2019 : Project Delivery setteledt (201910 s.d 201912) yg DocDt SO Contract sebelum tahun berjalan
    Penjualan s.d Sept 2019 : Project Delivery settledt < 201910 yg DocDt SO Contract sebelum tahun berjalan

    Total Penjualan 2019 : Penjualan Baru 2019 + Penjualan Luncuran 2019

    Total RAPP : Total REsource PRJI
	    if(LOP Resource != Loan (param baru untuk resource LOAN)) / 1.1 (param baru 1.1)
    RAPP s.d 2018 : VC Dropping Payment, DocDt Voucher nya < 2019
    RAPP TW I : Voucher Dropping payment, DocDt (201901 s.d 201903)
    RAPP TW II : Voucher Dropping payment, DocDt (201904 s.d 201906)
    RAPP TW III : Voucher Dropping payment, DocDt (201907 s.d 201909)
    RAPP TW IV : Voucher Dropping payment, DocDt (201910 s.d 201912)
    RAPP s.d 2019 : VC Dropping Payment, DocDt Voucher nya < 2020
    % : RAPP s.d 2019 / (Total Kontrak / 1.1 tergantung resource LOP)
    Control RAPP : Total amt Project DElivery - Total Amt VC Dropping Payment
    Uang Muka : VC ARDP
    Potongan Uang Muka : DP SLI Project
    Sisa Uang Muka : Uang Muka - Potongan Uang Muka

    Termin s.d 2018 : Amt SLI Project sebelum 2019
    Termin 2019 : Amt SLI Project 2019
    Termin s.d 2019 : Amt SLI Project sebelum 2020
    PPH Termin : Amt Tax PPH SLI Project
    Piutang : ********

    Tagihan Brutto Total : Amt Project Delivery - Amt SLI Project - VC ARDP
    Realisasi s.d 2018 : VC Dropping Payment < 2019
    Realisasi TW I : VC Dropping PAyment 201901 s.d 201903
    Realisasi TW II : VC Dropping PAyment 201904 s.d 201906
    Realisasi TW III : VC Dropping PAyment 201907 s.d 201909
    Realisasi TW IV : VC Dropping PAyment 201910 s.d 201912
    REalisasi s.d Sept 2019 : VC Dropping Payment sebelum 201910
    BMHD s.d Sept 2019 : Total Resource - REalisasi s.d Sept 2019 
 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectAchievement : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mOneYearAgo = string.Empty, mThisYear = string.Empty,
            mThisMth = string.Empty, mNextMth = string.Empty,
            mMthDesc = string.Empty, mTerminDesc = string.Empty,
            mProjectResourceCodeForLOAN = string.Empty, mConfidentLvlLOPForProgonosa4 = string.Empty,
            mPPHTaxCode = string.Empty;

        private string[] mConfidentLvl = { };

        private decimal mAchievementDeviderForOperationCashflow = 0m;
        private bool mIsFilterBySite = false;

        private string[] mArrMthDesc = { "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" };

        private int[] mGrdFormatDec = { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 
                                        21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
                                        41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58 };
        private int[] mGrdColReadOnly = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 
                                        21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
                                        41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58 };
        #endregion

        #region Constructor

        public FrmRptProjectAchievement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueSiteCode(ref LueSiteCode);
                SetTimeStampVariable();
                SetGrd();
                SetGrdColumnHdr();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void SetGrd()
        {
            Grd1.ReadOnly = false;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 59;
            Grd1.FrozenArea.ColCount = 3;

            Grd1.Cols[0].Width = 50;
            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[1].Width = 120;
            Grd1.Header.Cells[0, 1].Value = "Kode Proyek";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[2].Width = 250;
            Grd1.Header.Cells[0, 2].Value = "Nama Proyek";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[3].Width = 200;
            Grd1.Header.Cells[0, 3].Value = "Wilayah";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;            

            Grd1.Cols[4].Width = 180;
            Grd1.Header.Cells[0, 4].Value = "Periode";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;            

            Grd1.Cols[5].Width = 150;
            Grd1.Header.Cells[0, 5].Value = "Total Kontrak";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;            

            Grd1.Cols[6].Width = 150;
            //Grd1.Header.Cells[0, 6].Value = "Kontrak"+Environment.NewLine+"Carry Over " + mThisYear;
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;            

            Grd1.Cols[7].Width = 150;
            //Grd1.Header.Cells[0, 7].Value = "Kontrak Luncuran"+Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;            

            Grd1.Cols[8].Width = 150;
            //Grd1.Header.Cells[0, 8].Value = "Kontrak Baru" + mThisYear;
            Grd1.Header.Cells[0, 8].TextAlign = iGContentAlignment.MiddleCenter;            

            Grd1.Cols[9].Width = 150;
            Grd1.Header.Cells[0, 9].Value = "Kontrak Baru";
            Grd1.Header.Cells[0, 9].TextAlign = iGContentAlignment.MiddleCenter;
            
            Grd1.Cols[10].Width = 150;
            Grd1.Header.Cells[0, 10].Value = "Kontrak"+Environment.NewLine+"TW I";
            Grd1.Header.Cells[0, 10].TextAlign = iGContentAlignment.MiddleCenter;            

            Grd1.Cols[11].Width = 150;
            Grd1.Header.Cells[0, 11].Value = "Kontrak" + Environment.NewLine + "TW II";
            Grd1.Header.Cells[0, 11].TextAlign = iGContentAlignment.MiddleCenter;
            
            Grd1.Cols[12].Width = 150;
            Grd1.Header.Cells[0, 12].Value = "Kontrak" + Environment.NewLine + "TW III";
            Grd1.Header.Cells[0, 12].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[13].Width = 150;
            Grd1.Header.Cells[0, 13].Value = "Prognosa Kontrak" + Environment.NewLine + "TW IV"; ;
            Grd1.Header.Cells[0, 13].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[14].Width = 150;
            Grd1.Header.Cells[0, 14].Value = "Total Penjualan";
            Grd1.Header.Cells[0, 14].TextAlign = iGContentAlignment.TopCenter;

            Grd1.Cols[15].Width = 150;
            //Grd1.Header.Cells[0, 15].Value = "Penjualan"+Environment.NewLine+"s.d " + mOneYearAgo;
            Grd1.Header.Cells[0, 15].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[16].Width = 150;
            Grd1.Header.Cells[0, 16].Value = " % ";
            Grd1.Header.Cells[0, 16].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[17].Width = 150;
            //Grd1.Header.Cells[0, 17].Value = "Penjualan" + Environment.NewLine + "Carry Over " + mThisYear;
            Grd1.Header.Cells[0, 17].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[18].Width = 150;
            //Grd1.Header.Cells[0, 18].Value = "Penjualan Luncuran" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 18].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[19].Width = 150;
            //Grd1.Header.Cells[0, 19].Value = "Penjualan Baru" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 19].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[20].Width = 150;
            //Grd1.Header.Cells[0, 20].Value = "Penjualan Baru" + Environment.NewLine + "yang dikelola " + mThisYear;
            Grd1.Header.Cells[0, 20].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[21].Width = 150;
            //Grd1.Header.Cells[0, 21].Value = "Penjualan Baru" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 21].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[22].Width = 150;
            //Grd1.Header.Cells[0, 22].Value = "Progress TW I" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 22].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[23].Width = 150;
            //Grd1.Header.Cells[0, 23].Value = "Progress TW II" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 23].TextAlign = iGContentAlignment.MiddleCenter;            

            Grd1.Cols[24].Width = 150;
            //Grd1.Header.Cells[0, 24].Value = "Progress TW III" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 24].TextAlign = iGContentAlignment.MiddleCenter;            

            Grd1.Cols[25].Width = 150;
            //Grd1.Header.Cells[0, 25].Value = "Progress TW IV" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 25].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[26].Width = 150;
            //Grd1.Header.Cells[0, 26].Value = "Penjualan s.d" + Environment.NewLine + mMthDesc;
            Grd1.Header.Cells[0, 26].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[27].Width = 150;
            //Grd1.Header.Cells[0, 27].Value = "Penjualan Luncuran" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 27].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[28].Width = 150;
            //Grd1.Header.Cells[0, 28].Value = "Progress TW I" + Environment.NewLine + "sebelum " +mThisYear;
            Grd1.Header.Cells[0, 28].TextAlign = iGContentAlignment.MiddleCenter;            

            Grd1.Cols[29].Width = 150;
            //Grd1.Header.Cells[0, 29].Value = "Progress TW II" + Environment.NewLine + "sebelum " + mThisYear;
            Grd1.Header.Cells[0, 29].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[30].Width = 150;
            //Grd1.Header.Cells[0, 30].Value = "Progress TW III" + Environment.NewLine + "sebelum " + mThisYear;
            Grd1.Header.Cells[0, 30].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[31].Width = 150;
            //Grd1.Header.Cells[0, 31].Value = "Progress TW IV" + Environment.NewLine + "sebelum " + mThisYear;
            Grd1.Header.Cells[0, 31].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[32].Width = 150;
            //Grd1.Header.Cells[0, 32].Value = "Penjualan s.d" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 32].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[33].Width = 150;
            //Grd1.Header.Cells[0, 33].Value = "Total Penjualan" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 33].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[34].Width = 150;
            Grd1.Header.Cells[0, 34].Value = "Total RAPP";
            Grd1.Header.Cells[0, 34].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[35].Width = 150;
            //Grd1.Header.Cells[0, 35].Value = "RAPP s.d" + Environment.NewLine + mOneYearAgo;
            Grd1.Header.Cells[0, 35].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[36].Width = 150;
            Grd1.Header.Cells[0, 36].Value = "RAPP TW I";
            Grd1.Header.Cells[0, 36].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[37].Width = 150;
            Grd1.Header.Cells[0, 37].Value = "RAPP TW II";
            Grd1.Header.Cells[0, 37].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[38].Width = 150;
            Grd1.Header.Cells[0, 38].Value = "RAPP TW III";
            Grd1.Header.Cells[0, 38].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[39].Width = 150;
            Grd1.Header.Cells[0, 39].Value = "RAPP TW IV";
            Grd1.Header.Cells[0, 39].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[40].Width = 150;
            //Grd1.Header.Cells[0, 40].Value = "RAPP s.d" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 40].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[41].Width = 150;
            Grd1.Header.Cells[0, 41].Value = " % ";
            Grd1.Header.Cells[0, 41].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[42].Width = 150;
            Grd1.Header.Cells[0, 42].Value = "Control RAPP";
            Grd1.Header.Cells[0, 42].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[43].Width = 150;
            Grd1.Header.Cells[0, 43].Value = "Uang Muka";
            Grd1.Header.Cells[0, 43].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[44].Width = 150;
            Grd1.Header.Cells[0, 44].Value = "Potongan Uang Muka";
            Grd1.Header.Cells[0, 44].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[45].Width = 150;
            Grd1.Header.Cells[0, 45].Value = "Sisa Uang Muka";
            Grd1.Header.Cells[0, 45].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[46].Width = 150;
            //Grd1.Header.Cells[0, 46].Value = "Termin s.d" + Environment.NewLine + mOneYearAgo;
            Grd1.Header.Cells[0, 46].TextAlign = iGContentAlignment.MiddleCenter;            

            Grd1.Cols[47].Width = 150;
            //Grd1.Header.Cells[0, 47].Value = "Termin" + mThisYear;
            Grd1.Header.Cells[0, 47].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[48].Width = 150;
            //Grd1.Header.Cells[0, 48].Value = "Termin s.d" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 48].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[49].Width = 200;
            Grd1.Header.Cells[0, 49].Value = "PPH Termin + PPH uang muka +"+Environment.NewLine+"PPH Potongan Uang muka";
            Grd1.Header.Cells[0, 49].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[50].Width = 150;
            Grd1.Header.Cells[0, 50].Value = "Piutang";
            Grd1.Header.Cells[0, 50].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[51].Width = 150;
            Grd1.Header.Cells[0, 51].Value = "Tagihan Brutto" +Environment.NewLine+ "Total";
            Grd1.Header.Cells[0, 51].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[52].Width = 150;
            //Grd1.Header.Cells[0, 52].Value = "Realisasi s.d" + Environment.NewLine + mOneYearAgo;
            Grd1.Header.Cells[0, 52].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[53].Width = 150;
            Grd1.Header.Cells[0, 53].Value = "Realisasi" +Environment.NewLine+ "TW I";
            Grd1.Header.Cells[0, 53].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[54].Width = 150;
            Grd1.Header.Cells[0, 54].Value = "Realisasi" + Environment.NewLine + "TW II";
            Grd1.Header.Cells[0, 54].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[55].Width = 150;
            Grd1.Header.Cells[0, 55].Value = "Realisasi" + Environment.NewLine + "TW III";
            Grd1.Header.Cells[0, 55].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[56].Width = 150;
            Grd1.Header.Cells[0, 56].Value = "Realisasi" + Environment.NewLine + "TW IV";
            Grd1.Header.Cells[0, 56].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[57].Width = 150;
            //Grd1.Header.Cells[0, 57].Value = "Realisasi s.d" +Environment.NewLine+ mMthDesc;
            Grd1.Header.Cells[0, 57].TextAlign = iGContentAlignment.MiddleCenter;

            Grd1.Cols[58].Width = 150;
            //Grd1.Header.Cells[0, 58].Value = "BMHD s.d" +Environment.NewLine+ mMthDesc;
            Grd1.Header.Cells[0, 58].TextAlign = iGContentAlignment.MiddleCenter;

            Sm.GrdFormatDec(Grd1, mGrdFormatDec, 0);
            Sm.GrdColReadOnly(true, false, Grd1, mGrdColReadOnly);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
            {
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
                Grd1.Header.Cells[0, Index].SpanRows = 2;
            }
        }

        #region SQL

        private string SQL1()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT IFNULL(C.ProjectCode2, C.ProjectCode) ProjectCode, A.ProjectName, A.ProjectResource, IFNULL((G.ParValue / 100), 1.1) Tax, F.SiteName,  ");
            SQL.AppendLine("CONCAT( ");
            SQL.AppendLine("DATE_FORMAT(C.DocDt, '%d %b %Y'),  ");
            SQL.AppendLine("IFNULL(CONCAT(' - ', DATE_FORMAT(C1.DeliveryDt, '%d %b %Y')), ''),  ");
            SQL.AppendLine("IF(C1.DeliveryDt IS NULL, '', CONCAT(' (', TIMESTAMPDIFF(MONTH, C.DocDt, C1.DeliveryDt), ' bulan)')) ");
            SQL.AppendLine(") AS Period, ");
            SQL.AppendLine("D.Amt ContractAmt, E.DocNo PRJIDocNo, IFNULL(E.TotalResource, 0.00) TotalResource, A.ConfidentLvl, ");
            SQL.AppendLine("If(H.ParValue IS NULL, 0.00, If(FIND_IN_SET(A.ConfidentLvl, H.ParValue), A.EstValue, 0.00)) EstValue, ");
            SQL.AppendLine("Left(C.DocDt, 6) SOCDocDt ");
            SQL.AppendLine("FROM TblLOPHdr A  ");
            SQL.AppendLine("INNER JOIN TblBOQHdr B ON A.DocNo = B.LOPDocNo  ");
            SQL.AppendLine("    AND A.CancelInd = 'N' AND A.Status = 'A'  ");
            SQL.AppendLine("    AND B.ActInd = 'Y' AND B.Status = 'A'  ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("    And A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=A.SiteCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (Sm.GetLue(LueSiteCode).Length > 0)
                SQL.AppendLine("    And A.SiteCode = @SiteCode ");
            SQL.AppendLine("INNER JOIN TblSOContractHdr C ON B.DocNo = C.BOQDocNo  ");
            SQL.AppendLine("    AND C.CancelInd = 'N' AND C.Status = 'A'  ");
            if (TxtProjectCode.Text.Length > 0)
                SQL.AppendLine("    And (C.ProjectCode2 Like @ProjectCode Or C.ProjectCode Like @ProjectCode Or A.ProjectName Like @ProjectCode) ");
            SQL.AppendLine("LEFT JOIN (SELECT DocNo, MAX(DeliveryDt) DeliveryDt FROM TblSOContractDtl GROUP BY DocNo) C1 ON C.DocNo = C1.DocNo ");
            SQL.AppendLine("INNER JOIN  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    SELECT T1.SOCRDocNo, T1.SOCDocNo, T3.Amt  ");
            SQL.AppendLine("    FROM  ");
            SQL.AppendLine("    (  ");
            SQL.AppendLine("        SELECT SOCDocNo, MAX(DocNo) SOCRDocNo   ");
            SQL.AppendLine("         FROM TblSOContractRevisionHdr   ");
            SQL.AppendLine("         GROUP BY SOCDocNo  ");
            SQL.AppendLine("     ) T1  ");
            SQL.AppendLine("     INNER JOIN TblSOContractHdr T2 ON T1.SOCDocNo = T2.DocNo AND T2.CancelInd = 'N' AND T2.Status = 'A'  ");
            SQL.AppendLine("     INNER JOIN TblSOContractRevisionHdr T3 ON T1.SOCRDocNo = T3.DocNo  ");
            SQL.AppendLine(") D ON C.DocNo = D.SOCDocNo  ");
            SQL.AppendLine("LEFT JOIN TblProjectImplementationHdr E ON D.SOCRDocNo = E.SOContractDocNo  ");
            SQL.AppendLine("    AND E.CancelInd = 'N' AND E.Status = 'A'  ");
            SQL.AppendLine("LEFT JOIN TblSite F ON A.SiteCode = F.SiteCode  ");
            SQL.AppendLine("LEFT JOIN TblParameter G ON G.ParCode = 'AchievementDeviderForOperationCashflow' ");
            SQL.AppendLine("LEFT JOIN TblParameter H ON H.ParCode = 'ConfidentLvlLOPForProgonosa4' ");

            return SQL.ToString();
        }

        private string SQL2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, LEFT(A.SettleDt, 6) SettleDt, SUM(A.SettledAmt) SettledAmt ");
            SQL.AppendLine("FROM TblProjectImplementationDtl A ");
            SQL.AppendLine("WHERE FIND_IN_SET(A.DocNo, @PRJIDocNo) ");
            SQL.AppendLine("AND A.SettledInd = 'Y' AND A.SettleDt IS NOT NULL ");
            SQL.AppendLine("GROUP BY A.DocNo, LEFT(A.SettleDt, 6) ");
            SQL.AppendLine("ORDER BY A.DocNo, LEFT(A.SettleDt, 6); ");

            return SQL.ToString();
        }

        private string SQL3()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.PRJIDocNo, LEFT(C.DocDt, 6) DocDt, SUM(C.Amt) Amt ");
            SQL.AppendLine("FROM TblDroppingRequestHdr A ");
            SQL.AppendLine("INNER JOIN TblDroppingPaymentHdr B ON A.DocNo = B.DRQDocNo ");
            SQL.AppendLine("    AND FIND_IN_SET(A.PRJIDocNo, @PRJIDocNo) ");
            SQL.AppendLine("INNER JOIN TblVoucherHdr C ON B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
            SQL.AppendLine("    AND C.CancelInd = 'N' ");
            SQL.AppendLine("GROUP BY A.PRJIDocNo, LEFT(C.DocDt, 6); ");

            return SQL.ToString();
        }

        private string SQL4()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo PRJIDocNo, SUM(D.Amt) Amt ");
            SQL.AppendLine("FROM TblProjectImplementationHdr A ");
            SQL.AppendLine("INNER JOIN TblSOContractRevisionHdr B ON A.SOContractDocNo = B.DocNo ");
            SQL.AppendLine("    AND FIND_IN_SET(A.DocNo, @PRJIDocNo) ");
            SQL.AppendLine("INNER JOIN TblARDownpayment C ON B.SOCDocNo = C.SODocNo ");
            SQL.AppendLine("INNER JOIN TblVoucherHdr D ON C.VoucherRequestDocNo = D.VoucherRequestDocNo ");
            SQL.AppendLine("    AND D.CancelInd = 'N' ");
            SQL.AppendLine("GROUP BY A.DocNo; ");

            return SQL.ToString();
        }

        private string SQL5()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT T.PRJIDocNo, T.DocDt, SUM(T.Amt) Amt, SUM(T.Downpayment) Downpayment, SUM(T.TaxAmt) TaxAmt ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    SELECT B.PRJIDocNo, LEFT(A.DocDt, 6) DocDt, A.Amt, A.Downpayment, ");
	        SQL.AppendLine("    ( ");
	        SQL.AppendLine("    If(FIND_IN_SET(A.TaxCode1, C.ParValue), A.TaxAmt1, 0.00) + ");
	        SQL.AppendLine("    If(FIND_IN_SET(A.TaxCode2, C.ParValue), A.TaxAmt2, 0.00) + ");
	        SQL.AppendLine("    If(FIND_IN_SET(A.TaxCode3, C.ParValue), A.TaxAmt3, 0.00) ");
	        SQL.AppendLine("    ) AS TaxAmt ");
	        SQL.AppendLine("    FROM TblSalesInvoice5Hdr A ");
	        SQL.AppendLine("    INNER JOIN  ");
	        SQL.AppendLine("    ( ");
	        SQL.AppendLine("        SELECT DISTINCT DocNo, ProjectImplementationDocNo PRJIDocNo ");
		    SQL.AppendLine("         FROM TblSalesInvoice5Dtl ");
		    SQL.AppendLine("         WHERE FIND_IN_SET(ProjectImplementationDocNo, @PRJIDocNo) ");
	        SQL.AppendLine("    ) B ON A.DocNo = B.DocNo ");
	        SQL.AppendLine("        AND A.CancelInd = 'N' ");
	        SQL.AppendLine("    LEFT JOIN TblParameter C ON C.ParCode = 'PPHTaxCode' ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("GROUP BY T.PRJIDocNo, T.DocDt; ");

            return SQL.ToString();
        }

        #endregion

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;
            Sm.ClearGrd(Grd1, false);
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                Cursor.Current = Cursors.WaitCursor;
                SetTimeStampVariable();
                SetGrdColumnHdr();
                string mPRJIDocNo = string.Empty;

                var l = new List<ProjectAchievement>();
                var l2 = new List<ProjectDelivery>();
                var l3 = new List<DroppingPayment>();
                var l4 = new List<ARDP>();
                var l5 = new List<SalesInvoice>();

                Process1(ref l);
                if (l.Count > 0)
                {
                    for (int i = 0; i < l.Count; ++i)
                    {
                        if (l[i].PRJIDocNo.Length > 0)
                        {
                            if (mPRJIDocNo.Length > 0) mPRJIDocNo += ",";
                            mPRJIDocNo += l[i].PRJIDocNo;
                        }
                    }

                    if (mPRJIDocNo.Length > 0)
                    {
                        Process2(ref l2, mPRJIDocNo);
                        Process3(ref l3, mPRJIDocNo);
                        Process4(ref l4, mPRJIDocNo);
                        Process5(ref l5, mPRJIDocNo);
                    }

                    if (l2.Count > 0) Process2_1(ref l, ref l2);
                    if (l3.Count > 0) Process3_1(ref l, ref l3);
                    if (l4.Count > 0) Process4_1(ref l, ref l4);
                    if (l5.Count > 0) Process5_1(ref l, ref l5);
                    ProcessBruttoTotal(ref l);

                    Process99(ref l);
                }
                else Sm.StdMsg(mMsgType.NoData, string.Empty);

                Grd1.GroupObject.Add(3);
                Grd1.Group();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ForeColor = Color.Black;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, mGrdFormatDec);

                l.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mAchievementDeviderForOperationCashflow = Sm.GetParameterDec("AchievementDeviderForOperationCashflow");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mProjectResourceCodeForLOAN = Sm.GetParameter("ProjectResourceCodeForLOAN");
            mConfidentLvlLOPForProgonosa4 = Sm.GetParameter("ConfidentLvlLOPForProgonosa4");
            mPPHTaxCode = Sm.GetParameter("PPHTaxCode");

            if (mConfidentLvlLOPForProgonosa4.Length > 0)
            {
                mConfidentLvl = mConfidentLvlLOPForProgonosa4.Split(',');
            }
        }

        private void SetTimeStampVariable()
        {
            mThisMth = string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth));
            if (Sm.GetLue(LueMth) == "12") mNextMth = string.Concat((Int32.Parse(Sm.GetLue(LueYr)) + 1).ToString(), "01");
            else mNextMth = string.Concat(Sm.GetLue(LueYr), Sm.Right(string.Concat("00", (Int32.Parse(Sm.GetLue(LueMth)) + 1).ToString()), 2));
            mThisYear = Sm.GetLue(LueYr);
            mOneYearAgo = (Int32.Parse(mThisYear) - 1).ToString();
            mMthDesc = mArrMthDesc[Int32.Parse(Sm.GetLue(LueMth)) - 1];
            if (Sm.GetLue(LueMth) == "01") mTerminDesc = string.Concat(mArrMthDesc[11], " ", mOneYearAgo);
            else mTerminDesc = string.Concat(mArrMthDesc[Int32.Parse(Sm.GetLue(LueMth)) - 2], " ", mThisYear);
        }

        private void SetGrdColumnHdr()
        {
            Grd1.Header.Cells[0, 6].Value = "Kontrak" + Environment.NewLine + "Carry Over " + mThisYear;
            Grd1.Header.Cells[0, 7].Value = "Kontrak Luncuran" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 8].Value = "Kontrak Baru " + mThisYear;
            Grd1.Header.Cells[0, 15].Value = "Penjualan" + Environment.NewLine + "s.d " + mOneYearAgo;
            Grd1.Header.Cells[0, 17].Value = "Penjualan" + Environment.NewLine + "Carry Over " + mThisYear;
            Grd1.Header.Cells[0, 18].Value = "Penjualan Luncuran" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 19].Value = "Penjualan Baru" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 20].Value = "Penjualan Baru" + Environment.NewLine + "yang dikelola " + mThisYear;
            Grd1.Header.Cells[0, 21].Value = "Penjualan Baru" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 22].Value = "Progress TW I" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 23].Value = "Progress TW II" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 24].Value = "Progress TW III" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 25].Value = "Progress TW IV" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 26].Value = "Penjualan s.d" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 27].Value = "Penjualan Luncuran" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 28].Value = "Progress TW I" + Environment.NewLine + "sebelum " + mThisYear;
            Grd1.Header.Cells[0, 29].Value = "Progress TW II" + Environment.NewLine + "sebelum " + mThisYear;
            Grd1.Header.Cells[0, 30].Value = "Progress TW III" + Environment.NewLine + "sebelum " + mThisYear;
            Grd1.Header.Cells[0, 31].Value = "Progress TW IV" + Environment.NewLine + "sebelum " + mThisYear;
            Grd1.Header.Cells[0, 32].Value = "Penjualan s.d" + Environment.NewLine + string.Concat(mMthDesc, " ", mThisYear);
            Grd1.Header.Cells[0, 33].Value = "Total Penjualan" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 35].Value = "RAPP s.d" + Environment.NewLine + mOneYearAgo;
            Grd1.Header.Cells[0, 40].Value = "RAPP s.d" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 46].Value = "Termin s.d" + Environment.NewLine + mOneYearAgo;
            Grd1.Header.Cells[0, 47].Value = "Termin" + mThisYear;
            Grd1.Header.Cells[0, 48].Value = "Termin s.d" + Environment.NewLine + mThisYear;
            Grd1.Header.Cells[0, 52].Value = "Realisasi s.d" + Environment.NewLine + mOneYearAgo;
            Grd1.Header.Cells[0, 57].Value = "Realisasi s.d" + Environment.NewLine + mMthDesc;
            Grd1.Header.Cells[0, 58].Value = "BMHD s.d" + Environment.NewLine + mMthDesc;
        }

        #region Process

        private void Process1(ref List<ProjectAchievement> l)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@ProjectCode", string.Concat("%", TxtProjectCode.Text, "%"));
                Sm.CmParam<Decimal>(ref cm, "@AchievementDeviderForOperationCashflow", mAchievementDeviderForOperationCashflow);

                cm.CommandText = SQL1();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "ProjectCode", 
                    "ProjectName", "ProjectResource", "Tax", "SiteName", "Period", 
                    "ContractAmt", "PRJIDocNo", "TotalResource", "EstValue", "ConfidentLvl",
                    "SOCDocDt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProjectAchievement()
                        {
                            ProjectCode = Sm.DrStr(dr, c[0]),
                            ProjectName = Sm.DrStr(dr, c[1]),
                            ProjectResource = Sm.DrStr(dr, c[2]),
                            Tax = Sm.DrDec(dr, c[3]),
                            SiteName = Sm.DrStr(dr, c[4]),
                            Period = Sm.DrStr(dr, c[5]),
                            Contract = Sm.DrDec(dr, c[6]),
                            PRJIDocNo = Sm.DrStr(dr, c[7]),
                            TotalResource = Sm.DrDec(dr, c[8]),
                            EstValue = Sm.DrDec(dr, c[9]),
                            ConfidentLvl = Sm.DrStr(dr, c[10]),
                            SOCDocDt = Sm.DrStr(dr, c[11]),
                            ContractCarryOverNow = Sm.DrDec(dr, c[6]),
                            ContractGlideNow = 0m,
                            NewContractNow = 0m,
                            NewContract = (Sm.Left(Sm.DrStr(dr, c[11]), 4) == mThisYear) ? Sm.DrDec(dr, c[6]) : 0m,
                            ContractTW1 = (Sm.DrStr(dr, c[11]) == string.Concat(mThisYear, "01") ||
                                           Sm.DrStr(dr, c[11]) == string.Concat(mThisYear, "02") ||
                                           Sm.DrStr(dr, c[11]) == string.Concat(mThisYear, "03")) ? Sm.DrDec(dr, c[6]) : 0m,
                            ContractTW2 = (Sm.DrStr(dr, c[11]) == string.Concat(mThisYear, "04") ||
                                           Sm.DrStr(dr, c[11]) == string.Concat(mThisYear, "05") ||
                                           Sm.DrStr(dr, c[11]) == string.Concat(mThisYear, "06")) ? Sm.DrDec(dr, c[6]) : 0m,
                            ContractTW3 = (Sm.DrStr(dr, c[11]) == string.Concat(mThisYear, "07") ||
                                           Sm.DrStr(dr, c[11]) == string.Concat(mThisYear, "08") ||
                                           Sm.DrStr(dr, c[11]) == string.Concat(mThisYear, "09")) ? Sm.DrDec(dr, c[6]) : 0m,
                            ContractTW4 = Sm.DrDec(dr, c[9]),
                            TotalSelling = 0m,
                            SellingTo1 = 0m,
                            SellingContractPercentage = 0m,
                            SellingCarryOverNow = 0m,
                            SellingGlideNow = 0m,
                            NewSellingNow = 0m,
                            NewSellingManageNow = 0m,
                            ProgressTW1Now = 0m,
                            ProgressTW2Now = 0m,
                            ProgressTW3Now = 0m,
                            ProgressTW4Now = 0m,
                            SellingToMth = 0m,
                            ProgressTW11 = 0m,
                            ProgressTW21 = 0m,
                            ProgressTW31 = 0m,
                            ProgressTW41 = 0m,
                            SellingToMth2 = 0m,
                            TotalSellingNow = 0m,
                            TotalRAPP = 0m,
                            RAPPTo1 = 0m,
                            RAPPTW1 = 0m,
                            RAPPTW2 = 0m,
                            RAPPTW3 = 0m,
                            RAPPTW4 = 0m,
                            RAPPToNow = 0m,
                            RAPPToNowPercentage = 0m,
                            ControlRAPP = 0m,
                            DPAmt = 0m,
                            DPDiscountAmt = 0m,
                            OutstandingDP = 0m,
                            TerminTo1 = 0m,
                            TerminNow = 0m,
                            TerminToNow = 0m,
                            PPHTermin = 0m,
                            ARAmt = 0m,
                            BruttoTotal = 0m,
                            PaymentTo1 = 0m,
                            PaymentTW1 = 0m,
                            PaymentTW2 = 0m,
                            PaymentTW3 = 0m,
                            PaymentTW4 = 0m,
                            PaymentToMth = 0m,
                            BMHD = 0m,
                            PDAmt = 0m,
                            SLIAmt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<ProjectDelivery> l2, string PRJIDocNo)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParam<String>(ref cm, "@PRJIDocNo", PRJIDocNo);

                cm.CommandText = SQL2();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DocNo", "SettleDt", "SettledAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new ProjectDelivery()
                        {
                            PRJIDocNo = Sm.DrStr(dr, c[0]),
                            SettleDt = Sm.DrStr(dr, c[1]),
                            SettledAmt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2_1(ref List<ProjectAchievement> l, ref List<ProjectDelivery> l2)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                for (int j = 0; j < l2.Count; ++j)
                {
                    if (l[i].PRJIDocNo == l2[j].PRJIDocNo)
                    {
                        l[i].PDAmt += l2[j].SettledAmt;

                        l[i].ControlRAPP += l2[j].SettledAmt;

                        if (Int32.Parse(Sm.Left(l2[j].SettleDt, 4)) < Int32.Parse(mThisYear))
                        {
                            l[i].ContractCarryOverNow -= l2[j].SettledAmt;
                            l[i].SellingTo1 += l2[j].SettledAmt;
                        }

                        if (Sm.Left(l2[j].SettleDt, 4) == mThisYear)
                        {
                            l[i].ContractGlideNow += l2[j].SettledAmt;
                            if (Sm.Left(l[i].SOCDocDt, 4) == mThisYear)
                                l[i].NewContractNow += l2[j].SettledAmt;
                        }

                        if (Sm.Left(l[i].SOCDocDt, 4) == mThisYear &&
                            (
                                l2[j].SettleDt == string.Concat(mThisYear, "01") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "02") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "03")
                            )
                            )
                            l[i].ProgressTW1Now += l2[j].SettledAmt;

                        if (Sm.Left(l[i].SOCDocDt, 4) == mThisYear &&
                            (
                                l2[j].SettleDt == string.Concat(mThisYear, "04") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "05") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "06")
                            )
                            )
                            l[i].ProgressTW2Now += l2[j].SettledAmt;

                        if (Sm.Left(l[i].SOCDocDt, 4) == mThisYear &&
                            (
                                l2[j].SettleDt == string.Concat(mThisYear, "07") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "08") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "09")
                            )
                            )
                            l[i].ProgressTW3Now += l2[j].SettledAmt;

                        if (Sm.Left(l[i].SOCDocDt, 4) == mThisYear &&
                            (
                                l2[j].SettleDt == string.Concat(mThisYear, "10") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "11") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "12")
                            )
                            )
                            l[i].ProgressTW4Now += l2[j].SettledAmt;

                        if (Sm.Left(l[i].SOCDocDt, 4) == mThisYear && Int32.Parse(l2[j].SettleDt) < Int32.Parse(mNextMth))
                            l[i].SellingToMth += l2[j].SettledAmt;

                        if (Int32.Parse(Sm.Left(l[i].SOCDocDt, 4)) < Int32.Parse(mThisYear) &&
                            (
                                l2[j].SettleDt == string.Concat(mThisYear, "01") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "02") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "03")
                            )
                            )
                            l[i].ProgressTW11 += l2[j].SettledAmt;

                        if (Int32.Parse(Sm.Left(l[i].SOCDocDt, 4)) < Int32.Parse(mThisYear) &&
                            (
                                l2[j].SettleDt == string.Concat(mThisYear, "04") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "05") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "06")
                            )
                            )
                            l[i].ProgressTW21 += l2[j].SettledAmt;

                        if (Int32.Parse(Sm.Left(l[i].SOCDocDt, 4)) < Int32.Parse(mThisYear) &&
                            (
                                l2[j].SettleDt == string.Concat(mThisYear, "07") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "08") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "09")
                            )
                            )
                            l[i].ProgressTW31 += l2[j].SettledAmt;

                        if (Int32.Parse(Sm.Left(l[i].SOCDocDt, 4)) < Int32.Parse(mThisYear) &&
                            (
                                l2[j].SettleDt == string.Concat(mThisYear, "10") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "11") ||
                                l2[j].SettleDt == string.Concat(mThisYear, "12")
                            )
                            )
                            l[i].ProgressTW41 += l2[j].SettledAmt;

                        if (Int32.Parse(Sm.Left(l[i].SOCDocDt, 4)) < Int32.Parse(mThisYear) && Int32.Parse(l2[j].SettleDt) < Int32.Parse(mNextMth))
                            l[i].SellingToMth2 += l2[j].SettledAmt;
                    }
                }

                if (l[i].PRJIDocNo.Length > 0)
                {
                    if (l[i].ProjectResource != mProjectResourceCodeForLOAN)
                    {
                        l[i].ContractCarryOverNow = l[i].ContractCarryOverNow * l[i].Tax;
                        l[i].ContractGlideNow = l[i].ContractGlideNow * l[i].Tax;
                        l[i].NewContractNow = l[i].NewContractNow * l[i].Tax;
                        l[i].SellingTo1 = l[i].SellingTo1 / l[i].Tax;
                    }
                }

                if (l[i].TotalSelling != 0) l[i].SellingContractPercentage = l[i].SellingTo1 / l[i].TotalSelling;

                if(l[i].ContractCarryOverNow != 0)
                {
                    l[i].SellingCarryOverNow = l[i].ContractCarryOverNow;
                    if (l[i].ProjectResource != mProjectResourceCodeForLOAN)
                        l[i].SellingCarryOverNow = l[i].SellingCarryOverNow / l[i].Tax;
                }

                if (l[i].ContractGlideNow != 0)
                {
                    l[i].SellingGlideNow = l[i].ContractGlideNow;
                    if (l[i].ProjectResource != mProjectResourceCodeForLOAN)
                        l[i].SellingGlideNow = l[i].SellingGlideNow / l[i].Tax;
                }

                if (l[i].NewContractNow != 0)
                {
                    l[i].NewSellingNow = l[i].NewContractNow;
                    if (l[i].ProjectResource != mProjectResourceCodeForLOAN)
                        l[i].NewSellingNow = l[i].NewSellingNow / l[i].Tax;
                }

                l[i].TotalSellingNow = l[i].NewSellingNow + l[i].SellingGlideNow;

                l[i].TotalRAPP = l[i].TotalResource;
                l[i].TotalSelling = l[i].Contract;
                if (l[i].ProjectResource != mProjectResourceCodeForLOAN)
                {
                    l[i].TotalRAPP = l[i].TotalRAPP / l[i].Tax;
                    l[i].TotalSelling = l[i].TotalSelling / l[i].Tax;
                }
            }
        }

        private void Process3(ref List<DroppingPayment> l3, string PRJIDocNo)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParam<String>(ref cm, "@PRJIDocNo", PRJIDocNo);

                cm.CommandText = SQL3();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "PRJIDocNo", "DocDt", "Amt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new DroppingPayment()
                        {
                            PRJIDocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3_1(ref List<ProjectAchievement> l, ref List<DroppingPayment> l3)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                for (int j = 0; j < l3.Count; ++j)
                {
                    if (l[i].PRJIDocNo == l3[j].PRJIDocNo)
                    {
                        l[i].ControlRAPP -= l3[j].Amt;

                        if (Int32.Parse(Sm.Left(l3[j].DocDt, 4)) < Int32.Parse(mThisYear))
                        {
                            l[i].RAPPTo1 += l3[j].Amt;
                            l[i].PaymentTo1 += l3[j].Amt;
                        }

                        if (Int32.Parse(l3[j].DocDt) < Int32.Parse(mNextMth))
                            l[i].PaymentToMth += l3[j].Amt;

                        if (Int32.Parse(Sm.Left(l3[j].DocDt, 4)) < (Int32.Parse(mThisYear) + 1))
                            l[i].RAPPToNow += l3[j].Amt;

                        if (l3[j].DocDt == string.Concat(mThisYear, "01") ||
                            l3[j].DocDt == string.Concat(mThisYear, "02") ||
                            l3[j].DocDt == string.Concat(mThisYear, "03"))
                        {
                            l[i].RAPPTW1 += l3[j].Amt;
                            l[i].PaymentTW1 += l3[j].Amt;
                        }

                        if (l3[j].DocDt == string.Concat(mThisYear, "04") ||
                            l3[j].DocDt == string.Concat(mThisYear, "05") ||
                            l3[j].DocDt == string.Concat(mThisYear, "06"))
                        {
                            l[i].RAPPTW2 += l3[j].Amt;
                            l[i].PaymentTW2 += l3[j].Amt;
                        }

                        if (l3[j].DocDt == string.Concat(mThisYear, "07") ||
                            l3[j].DocDt == string.Concat(mThisYear, "08") ||
                            l3[j].DocDt == string.Concat(mThisYear, "09"))
                        {
                            l[i].RAPPTW3 += l3[j].Amt;
                            l[i].PaymentTW3 += l3[j].Amt;
                        }

                        if (l3[j].DocDt == string.Concat(mThisYear, "10") ||
                            l3[j].DocDt == string.Concat(mThisYear, "11") ||
                            l3[j].DocDt == string.Concat(mThisYear, "12"))
                        {
                            l[i].RAPPTW4 += l3[j].Amt;
                            l[i].PaymentTW4 += l3[j].Amt;
                        }
                    }
                }

                l[i].BMHD = l[i].TotalResource - l[i].PaymentToMth;

                if (l[i].Contract != 0)
                {
                    l[i].RAPPToNowPercentage = l[i].RAPPToNow;
                    if (l[i].ProjectResource != mProjectResourceCodeForLOAN)
                        l[i].RAPPToNowPercentage = (l[i].RAPPToNowPercentage / l[i].Contract) * 100m;
                    else
                        l[i].RAPPToNowPercentage = (l[i].RAPPToNowPercentage / (l[i].Contract));
                }
            }
        }

        private void Process4(ref List<ARDP> l4, string PRJIDocNo)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParam<String>(ref cm, "@PRJIDocNo", PRJIDocNo);

                cm.CommandText = SQL4();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "PRJIDocNo", "Amt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new ARDP()
                        {
                            PRJIDocNo = Sm.DrStr(dr, c[0]),
                            Amt = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process4_1(ref List<ProjectAchievement> l, ref List<ARDP> l4)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                for (int j = 0; j < l4.Count; ++j)
                {
                    if (l[i].PRJIDocNo == l4[j].PRJIDocNo)
                    {
                        l[i].DPAmt += l4[j].Amt;
                    }
                }
            }
        }

        private void Process5(ref List<SalesInvoice> l5, string PRJIDocNo)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                Sm.CmParam<String>(ref cm, "@PRJIDocNo", PRJIDocNo);

                cm.CommandText = SQL5();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "PRJIDocNo", "DocDt", "Amt", "Downpayment", "TaxAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l5.Add(new SalesInvoice()
                        {
                            PRJIDocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            Downpayment = Sm.DrDec(dr, c[3]),
                            TaxAmt = Sm.DrDec(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process5_1(ref List<ProjectAchievement> l, ref List<SalesInvoice> l5)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                for (int j = 0; j < l5.Count; ++j)
                {
                    if (l[i].PRJIDocNo == l5[j].PRJIDocNo)
                    {
                        l[i].SLIAmt += l5[j].Amt;

                        l[i].DPDiscountAmt += l5[j].Downpayment;

                        l[i].PPHTermin += l5[j].TaxAmt;

                        if (Int32.Parse(Sm.Left(l5[j].DocDt, 4)) < Int32.Parse(mThisYear))
                            l[i].TerminTo1 += l5[j].Amt;

                        if (Sm.Left(l5[j].DocDt, 4) == mThisYear)
                            l[i].TerminNow += l5[j].Amt;

                        if (Int32.Parse(Sm.Left(l5[j].DocDt, 4)) < (Int32.Parse(mThisYear) + 1))
                            l[i].TerminToNow += l5[j].Amt;
                    }
                }

                l[i].OutstandingDP = l[i].DPAmt - l[i].DPDiscountAmt;
            }
        }

        private void ProcessBruttoTotal(ref List<ProjectAchievement> l)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                l[i].BruttoTotal = l[i].PDAmt - l[i].SLIAmt - l[i].DPAmt;
            }
        }

        private void Process99(ref List<ProjectAchievement> l)
        {
            Sm.ClearGrd(Grd1, false);
            int No = 0;
            for (int i = 0; i < l.Count; i++)
            {
                Grd1.Rows.Add();
                Grd1.Cells[No, 0].Value = No + 1;
                Grd1.Cells[No, 1].Value = l[i].ProjectCode;
                Grd1.Cells[No, 2].Value = l[i].ProjectName;
                Grd1.Cells[No, 3].Value = l[i].SiteName;
                Grd1.Cells[No, 4].Value = l[i].Period;
                Grd1.Cells[No, 5].Value = l[i].Contract;
                Grd1.Cells[No, 6].Value = l[i].ContractCarryOverNow;
                Grd1.Cells[No, 7].Value = l[i].ContractGlideNow;
                Grd1.Cells[No, 8].Value = l[i].NewContractNow;
                Grd1.Cells[No, 9].Value = l[i].NewContract;
                Grd1.Cells[No, 10].Value = l[i].ContractTW1;
                Grd1.Cells[No, 11].Value = l[i].ContractTW2;
                Grd1.Cells[No, 12].Value = l[i].ContractTW3;
                Grd1.Cells[No, 13].Value = l[i].ContractTW4;
                Grd1.Cells[No, 14].Value = l[i].TotalSelling;
                Grd1.Cells[No, 15].Value = l[i].SellingTo1;
                Grd1.Cells[No, 16].Value = l[i].SellingContractPercentage;
                Grd1.Cells[No, 17].Value = l[i].SellingCarryOverNow;
                Grd1.Cells[No, 18].Value = l[i].SellingGlideNow;
                Grd1.Cells[No, 19].Value = l[i].NewSellingNow;
                Grd1.Cells[No, 20].Value = l[i].NewSellingManageNow;
                Grd1.Cells[No, 21].Value = l[i].NewSellingNow;
                Grd1.Cells[No, 22].Value = l[i].ProgressTW1Now;
                Grd1.Cells[No, 23].Value = l[i].ProgressTW2Now;
                Grd1.Cells[No, 24].Value = l[i].ProgressTW3Now;
                Grd1.Cells[No, 25].Value = l[i].ProgressTW4Now;
                Grd1.Cells[No, 26].Value = l[i].SellingToMth;
                Grd1.Cells[No, 27].Value = l[i].SellingGlideNow;
                Grd1.Cells[No, 28].Value = l[i].ProgressTW11;
                Grd1.Cells[No, 29].Value = l[i].ProgressTW21;
                Grd1.Cells[No, 30].Value = l[i].ProgressTW31;
                Grd1.Cells[No, 31].Value = l[i].ProgressTW41;
                Grd1.Cells[No, 32].Value = l[i].SellingToMth2;
                Grd1.Cells[No, 33].Value = l[i].TotalSellingNow;
                Grd1.Cells[No, 34].Value = l[i].TotalRAPP;
                Grd1.Cells[No, 35].Value = l[i].RAPPTo1;
                Grd1.Cells[No, 36].Value = l[i].RAPPTW1;
                Grd1.Cells[No, 37].Value = l[i].RAPPTW2;
                Grd1.Cells[No, 38].Value = l[i].RAPPTW3;
                Grd1.Cells[No, 39].Value = l[i].RAPPTW4;
                Grd1.Cells[No, 40].Value = l[i].RAPPToNow;
                Grd1.Cells[No, 41].Value = l[i].RAPPToNowPercentage;
                Grd1.Cells[No, 42].Value = l[i].ControlRAPP;
                Grd1.Cells[No, 43].Value = l[i].DPAmt;
                Grd1.Cells[No, 44].Value = l[i].DPDiscountAmt;
                Grd1.Cells[No, 45].Value = l[i].OutstandingDP;
                Grd1.Cells[No, 46].Value = l[i].TerminTo1;
                Grd1.Cells[No, 47].Value = l[i].TerminNow;
                Grd1.Cells[No, 48].Value = l[i].TerminToNow;
                Grd1.Cells[No, 49].Value = l[i].PPHTermin;
                Grd1.Cells[No, 50].Value = l[i].ARAmt;
                Grd1.Cells[No, 51].Value = l[i].BruttoTotal;
                Grd1.Cells[No, 52].Value = l[i].PaymentTo1;
                Grd1.Cells[No, 53].Value = l[i].PaymentTW1;
                Grd1.Cells[No, 54].Value = l[i].PaymentTW2;
                Grd1.Cells[No, 55].Value = l[i].PaymentTW3;
                Grd1.Cells[No, 56].Value = l[i].PaymentTW4;
                Grd1.Cells[No, 57].Value = l[i].PaymentToMth;
                Grd1.Cells[No, 58].Value = l[i].BMHD;
                No += 1;
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        #endregion

        #endregion

        #region Class

        private class ProjectAchievement
        {
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string ProjectResource { get; set; }
            public decimal Tax { get; set; }
            public string Period { get; set; }
            public string SiteName { get; set; }
            public string PRJIDocNo { get; set; }
            public decimal Contract { get; set; }
            public decimal TotalResource { get; set; }
            public decimal EstValue { get; set; }
            public string ConfidentLvl { get; set; }
            public string SOCDocDt { get; set; }
            public decimal ContractCarryOverNow { get; set; }
            public decimal ContractGlideNow { get; set; }
            public decimal NewContractNow { get; set; }
            public decimal NewContract { get; set; }
            public decimal ContractTW1 { get; set; }
            public decimal ContractTW2 { get; set; }
            public decimal ContractTW3 { get; set; }
            public decimal ContractTW4 { get; set; }
            public decimal TotalSelling { get; set; }
            public decimal SellingTo1 { get; set; }
            public decimal SellingContractPercentage { get; set; }
            public decimal SellingCarryOverNow { get; set; }
            public decimal SellingGlideNow { get; set; }
            public decimal NewSellingNow { get; set; }
            public decimal NewSellingManageNow { get; set; }
            public decimal ProgressTW1Now { get; set; }
            public decimal ProgressTW2Now { get; set; }
            public decimal ProgressTW3Now { get; set; }
            public decimal ProgressTW4Now { get; set; }
            public decimal SellingToMth { get; set; }
            public decimal ProgressTW11 { get; set; }
            public decimal ProgressTW21 { get; set; }
            public decimal ProgressTW31 { get; set; }
            public decimal ProgressTW41 { get; set; }
            public decimal SellingToMth2 { get; set; }
            public decimal TotalSellingNow { get; set; }
            public decimal TotalRAPP { get; set; }
            public decimal RAPPTo1 { get; set; }
            public decimal RAPPTW1 { get; set; }
            public decimal RAPPTW2 { get; set; }
            public decimal RAPPTW3 { get; set; }
            public decimal RAPPTW4 { get; set; }
            public decimal RAPPToNow { get; set; }
            public decimal RAPPToNowPercentage { get; set; }
            public decimal ControlRAPP { get; set; }
            public decimal DPAmt { get; set; }
            public decimal DPDiscountAmt { get; set; }
            public decimal OutstandingDP { get; set; }
            public decimal TerminTo1 { get; set; }
            public decimal TerminNow { get; set; }
            public decimal TerminToNow { get; set; }
            public decimal PPHTermin { get; set; }
            public decimal ARAmt { get; set; }
            public decimal BruttoTotal { get; set; }
            public decimal PaymentTo1 { get; set; }
            public decimal PaymentTW1 { get; set; }
            public decimal PaymentTW2 { get; set; }
            public decimal PaymentTW3 { get; set; }
            public decimal PaymentTW4 { get; set; }
            public decimal PaymentToMth { get; set; }
            public decimal BMHD { get; set; }
            public decimal PDAmt { get; set; }
            public decimal SLIAmt { get; set; }
        }

        private class ProjectDelivery
        {
            public string PRJIDocNo { get; set; }
            public string SettleDt { get; set; }
            public decimal SettledAmt { get; set; }
        }

        private class DroppingPayment
        {
            public string PRJIDocNo { get; set; }
            public string DocDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class ARDP
        {
            public string PRJIDocNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class SalesInvoice
        {
            public string PRJIDocNo { get; set; }
            public string DocDt { get; set; }
            public decimal Amt { get; set; }
            public decimal Downpayment { get; set; }
            public decimal TaxAmt { get; set; }
        }

        #endregion
    }
}
