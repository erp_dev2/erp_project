﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptPurchaserPerformance2Dlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptPurchaserPerformance2 mFrmParent;
        private string 
            mSQL = string.Empty, 
            mPODocNo = string.Empty, 
            mPODNo = string.Empty,
            mItCode = string.Empty,
            mItName = string.Empty;
        private decimal 
            mUPriceBeforeTax = 0m,
            mUPriceAfterTax = 0m;

        #endregion

        #region Constructor

        public FrmRptPurchaserPerformance2Dlg(
            FrmRptPurchaserPerformance2 FrmParent, 
            string PODocNo, 
            string PODNo,
            string ItCode,
            string ItName,
            decimal UPriceBeforeTax,
            decimal UPriceAfterTax)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPODocNo = PODocNo;
            mPODNo = PODNo;
            mItCode = ItCode;
            mItName = ItName;
            mUPriceBeforeTax = UPriceBeforeTax;
            mUPriceAfterTax = UPriceAfterTax;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                base.FrmLoad(sender, e);
                ShowDataHeader();
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.RowHeader.Visible = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "PI#", 
                    "", 
                    "Unit Price"+Environment.NewLine+"After Tax",
                    "PO#",
                    "Item Code",

                    //6-8
                    "Item Name",
                    "Unit Price"+Environment.NewLine+"Before Tax",
                    "Unit Price"+Environment.NewLine+"After Tax"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 20, 180, 120, 100,

                    //6-8
                    180, 120, 120
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 7, 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    F.UPrice + ");
	        SQL.AppendLine("    IFNULL((F.UPrice * H.TaxRate * 0.01), 0.00) + ");
	        SQL.AppendLine("    IFNULL((F.UPrice * I.TaxRate * 0.01), 0.00) + ");
	        SQL.AppendLine("    IFNULL((F.UPrice * J.TaxRate * 0.01), 0.00) ");
            SQL.AppendLine(") AS UPriceAfterTax ");
            SQL.AppendLine("FROM TblPurchaseInvoiceDtl A ");
            SQL.AppendLine("INNER JOIN TblPurchaseInvoiceHdr B ON A.DocNo = B.DocNo AND B.CancelInd = 'N' ");
            SQL.AppendLine("INNER JOIN TblRecvVdDtl C ON A.RecvVdDocNo = C.DocNo AND A.RecvVdDNo = C.DNo ");
            SQL.AppendLine("INNER JOIN TblPODtl D ON C.PODocNo = D.DocNo AND C.PODNo = D.DNo ");
            SQL.AppendLine("     AND D.DocNo = @PODocNo AND D.DNo = @PODNo ");
            SQL.AppendLine("INNER JOIN TblPORequestDtl E ON D.PORequestDocNo = E.DocNo AND D.PORequestDNo = E.DNo ");
            SQL.AppendLine("INNER JOIN TblQtDtl F ON E.QtDocNo = F.DocNo AND E.QtDNo = F.DNo AND F.ActInd = 'Y' ");
            SQL.AppendLine("INNER JOIN TblQtHdr G ON F.DocNo = G.DocNo AND G.Status = 'A' ");
            SQL.AppendLine("LEFT JOIN TblTax H ON B.TaxCode1 = H.TaxCode ");
            SQL.AppendLine("LEFT JOIN TblTax I ON B.TaxCode2 = I.TaxCode ");
            SQL.AppendLine("LEFT JOIN TblTax J ON B.TaxCode3 = J.TaxCode; ");

            return SQL.ToString();
        }

        private void ShowData()
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@PODocNo", mPODocNo);
            Sm.CmParam<String>(ref cm, "@PODNo", mPODNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, GetSQL(), new string[] { "DocNo", "UPriceAfterTax" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 1);
                    Grd1.Cells[Row, 4].Value = mPODocNo;
                    Grd1.Cells[Row, 5].Value = mItCode;
                    Grd1.Cells[Row, 6].Value = mItName;
                    Grd1.Cells[Row, 7].Value = mUPriceBeforeTax;
                    Grd1.Cells[Row, 8].Value = mUPriceAfterTax;
                }, false, false, false, false
            );
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3 });
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, r, 1).Length != 0)
            {
                e.DoDefault = false;
                var f1 = new FrmPurchaseInvoice(mFrmParent.mMenuCode);
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, r, 1);
                f1.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, r, 1).Length != 0)
            {
                var f1 = new FrmPurchaseInvoice(mFrmParent.mMenuCode);
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, r, 1);
                f1.ShowDialog();
            }
        }

        #endregion

        #region Additional Method

        private void ShowDataHeader()
        {
            TxtDocNo.EditValue = mPODocNo;
            TxtItCode.EditValue = mItCode;
            TxtItName.EditValue = mItName;
            TxtUPriceBeforeTax.EditValue = Sm.FormatNum(mUPriceBeforeTax, 0);
            TxtUPriceAfterTax.EditValue = Sm.FormatNum(mUPriceAfterTax, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Button Click

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            Sm.ExportToExcel(Grd1);
        }

        #endregion

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocNo);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItCode);
        }

        private void TxtItName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItName);
        }

        private void TxtUPriceBeforeTax_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtUPriceBeforeTax, 0);
        }

        private void TxtUPriceAfterTax_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtUPriceAfterTax, 0);
        }

        #endregion

        #endregion
    }
}
