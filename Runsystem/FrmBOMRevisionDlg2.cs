﻿﻿#region Update
/*
    22/11/2019 [DITA/IMS] new apps
    27/11/2019 [WED/IMS] tambah specification
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBOMRevisionDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmBOMRevision mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmBOMRevisionDlg2(FrmBOMRevision FrmParent) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5 
                    "",
                    "BOM#", 
                    "BOMDNo",
                    "Date",
                    "Material Result Indicator",
                    
                    //6-10
                    "Material / Result",
                    "Item Code", 
                    "Item Local Code",
                    "Item Name",
                    "Quantity",

                    //11
                    "Specification"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 150, 0, 100, 50,
                    
                    //6-10
                    120, 120, 120, 180, 100,

                    //11
                    120
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select '1' As MaterialResultInd, 'Material' As MaterialResultDesc, A.DocNo BOMDocNo, B.DNo BOMDNo, B.DocCode ItCode, C.ItName, C.ItCodeInternal, B.Qty, A.DocDt, C.Specification ");
            SQL.AppendLine("From TblBOMHdr A ");
            SQL.AppendLine("Inner Join TblBOMDtl B On A.DocNo = B.DocNo And A.ActiveInd = 'Y' ");
            SQL.AppendLine("Inner Join TblItem C On B.DocCode = C.ItCode And (C.InventoryItemInd = 'Y' Or C.ServiceItemInd = 'Y') ");
            SQL.AppendLine("Where Locate(Concat('##', '1', A.DocNo, B.DNo, '##'), @SelectedBOMData) < 1 And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As MaterialResultInd, 'Result' As MaterialResultDesc, A.DocNo BOMDocNo, B.DNo BOMDNo, B.ItCode, C.ItName, C.ItCodeInternal, B.Qty, A.DocDt, C.Specification ");
            SQL.AppendLine("From TblBOMHdr A ");
            SQL.AppendLine("Inner Join TblBOMDtl2 B On A.DocNo = B.DocNo And A.ActiveInd = 'Y' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode And (C.InventoryItemInd = 'Y' Or C.ServiceItemInd = 'Y') ");
            SQL.AppendLine("Where Locate(Concat('##', '2', A.DocNo, B.DNo, '##'), @SelectedBOMData) < 1 And A.DocDt Between @DocDt1 And @DocDt2  ");
            SQL.AppendLine(")T ");


            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@SelectedBOMData", mFrmParent.GetSelectedBOMData());

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.BOMDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItName", "T.ItCodeInternal" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL.ToString() + Filter + " Order By T.BOMDocNo, T.BOMDNo;",
                    new string[] 
                    { 
                        //0
                        "BOMDocNo", 
                        //1-5
                        "BOMDNo", "DocDt", "MaterialResultInd", "MaterialResultDesc", "ItCode",
                        //6-9
                        "ItCodeInternal", "ItName", "Qty", "Specification"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsBomDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 11);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 6 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsBomDataAlreadyChosen(int Row)
        {
            string BomData = string.Concat(Sm.GetGrdStr(Grd1, Row, 5), Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 3));
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(string.Concat(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), Sm.GetGrdStr(mFrmParent.Grd1, Index, 3)), BomData)) return true;
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

    

        #endregion

    }
}
