﻿#region Update
/*
    [WED/VIR] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptWeeklyProject : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmRptWeeklyProject(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);
                string CurrentDateTime = Sm.ServerCurrentDate();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDateTime);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDateTime).AddDays(6);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetControlNumValueZero(new List<DXE.TextEdit>
                {
                    TxtRKAPOld, TxtRKAPNew, TxtRKAPTotal, TxtRKAPPercentage, TxtRKAPDeviation,
                    TxtPercentageRKAPOld, TxtPercentageRKAPNew, TxtPercentageRKAPTotal,
                    TxtPercentageManagementOld, TxtPercentageManagementNew, TxtPercentageManagementTotal
                }, 0);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT T.SiteCode, T.SiteName, T.OutstandingSLIAmt, T.TargetSOCAmt, T.SumTargetAmt, ");
            SQL.AppendLine("T.RevisionAmt, T.SumRealizationAmt, ");
            SQL.AppendLine("If((T.SumTargetAmt - T.OutstandingSLIAmt) = 0, 0, (T.RevisionAmt / (T.SumTargetAmt - T.OutstandingSLIAmt)) * 100) Percentage, ");
            SQL.AppendLine("(T.SumRealizationAmt - T.SumTargetAmt) Deviation, T.Potension1, T.Potension2, T.Potension3, T.Potension4, ");
            SQL.AppendLine("T.AddendumAmt ");
            SQL.AppendLine("FROM  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT A.SiteCode, A.SiteName, IFNULL(B.OutstandingSLIAmt, 0.00) OutstandingSLIAmt, ");
            SQL.AppendLine("    IFNULL(C.Amt, 0.00) TargetSOCAmt, (IFNULL(B.OutstandingSLIAmt, 0.00) + IFNULL(C.Amt, 0.00)) SumTargetAmt, ");
            SQL.AppendLine("    IFNULL(D.RevisionAmt, 0.00) RevisionAmt, (IFNULL(B.OutstandingSLIAmt, 0.00) + IFNULL(D.RevisionAmt, 0.00)) SumRealizationAmt, ");
            SQL.AppendLine("    IFNULL(E.Potension1, 0.00) Potension1, IFNULL(F.Potension2, 0.00) Potension2, ");
	        SQL.AppendLine("    IFNULL(G.Potension3, 0.00) Potension3, IFNULL(H.Potension4, 0.00) Potension4, ");
	        SQL.AppendLine("    IFNULL(I.AddendumAmt, 0.00) AddendumAmt ");
            SQL.AppendLine("    FROM TblSite A ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("    Inner Join TblGroupSite A1 On A.SiteCode = A1.SiteCode ");
                SQL.AppendLine("        And A1.GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("    LEFT JOIN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T1.SiteCode, SUM(T6.EstimatedAmt) OutstandingSLIAmt ");
            SQL.AppendLine("        FROM TblLOPHdr T1 ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("    Inner Join TblGroupSite A1 On T1.SiteCode = A1.SiteCode ");
                SQL.AppendLine("        And A1.GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("        INNER JOIN TblBOQHdr T2 ON T1.DocNo = T2.LOPDocNo ");
            SQL.AppendLine("        INNER JOIN TblSOContractHdr T3 ON T2.DocNo = T3.BOQDocNo ");
            //SQL.AppendLine("            AND Left(T3.DocDt, 4) < @Yr ");
            SQL.AppendLine("              And (T3.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        INNER JOIN TblSOContractRevisionHdr T4 ON T3.DocNo = T4.SOCDocNo ");
            SQL.AppendLine("        INNER JOIN TblProjectImplementationHdr T5 ON T4.DocNo = T5.SOContractDocNo  ");
    	    SQL.AppendLine("             AND T5.CancelInd = 'N' ");
    	    SQL.AppendLine("             AND T5.ProcessInd = 'F' ");
            SQL.AppendLine("        INNER JOIN TblProjectImplementationDtl T6 ON T5.DocNo = T6.DocNo ");
            SQL.AppendLine("            AND T6.InvoicedInd = 'N' ");
            SQL.AppendLine("        GROUP BY T1.SiteCode ");
            SQL.AppendLine("    ) B ON A.SiteCode = B.SiteCode ");
            SQL.AppendLine("    LEFT JOIN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T1.SiteCode, T1.Amt ");
            SQL.AppendLine("        FROM TblSiteTargetSOC T1 ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("    Inner Join TblGroupSite A1 On T1.SiteCode = A1.SiteCode ");
                SQL.AppendLine("        And A1.GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("        WHERE T1.Yr = @Yr ");
            SQL.AppendLine("    ) C ON A.SiteCode = C.SiteCode ");
            SQL.AppendLine("    LEFT JOIN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T1.SiteCode, SUM(T5.Amt) RevisionAmt ");
            SQL.AppendLine("        FROM TblLOPHdr T1 ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("    Inner Join TblGroupSite A1 On T1.SiteCode = A1.SiteCode ");
                SQL.AppendLine("        And A1.GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("        INNER JOIN TblBOQHdr T2 ON T1.DocNo = T2.LOPDocNo ");
            SQL.AppendLine("        INNER JOIN TblSOContractHdr T3 ON T2.DocNo = T3.BOQDocNo ");
            //SQL.AppendLine("            AND Left(T3.DocDt, 4) = @Yr ");
            SQL.AppendLine("              And (T3.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        INNER JOIN  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            SELECT X1.SOCDocNo, MAX(X1.DocNo) DocNo ");
            SQL.AppendLine("            FROM TblSOContractRevisionHdr X1 ");
            SQL.AppendLine("            INNER JOIN TblSOContractHdr X2 ON X1.SOCDocNo = X2.DocNo ");
            SQL.AppendLine("                AND LEFT(X1.DocDt, 4) = @Yr ");
            SQL.AppendLine("                And (X2.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("            GROUP BY X1.SOCDocNo ");
            SQL.AppendLine("        ) T4 ON T3.DocNo = T4.SOCDocNo ");
            SQL.AppendLine("        INNER JOIN TblSOContractRevisionHdr T5 ON T4.DocNo = T5.DocNo ");
            SQL.AppendLine("        GROUP BY T1.SiteCode ");
            SQL.AppendLine("    ) D ON A.SiteCode = D.SiteCode ");
            SQL.AppendLine("    LEFT JOIN  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T1.SiteCode, SUM(T1.EstValue) Potension1 ");
            SQL.AppendLine("        FROM TblLOPHdr T1 ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("    Inner Join TblGroupSite A1 On T1.SiteCode = A1.SiteCode ");
                SQL.AppendLine("        And A1.GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("        Inner Join TblBOQHdr T2 On T1.DocNo = T2.LOPDocNo ");
            SQL.AppendLine("        Inner Join TblSOContractHdr T3 On T2.DocNo = T3.BOQDocNo ");
            SQL.AppendLine("            And (T3.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        WHERE T1.ProcessInd IN ('P', 'L') ");
            SQL.AppendLine("        AND T1.CancelInd = 'N' ");
            SQL.AppendLine("        AND T1.`Status` = 'A' ");
            SQL.AppendLine("        AND T1.ConfidentLvl = '1' ");
            SQL.AppendLine("        GROUP BY T1.SiteCode ");
            SQL.AppendLine("    ) E ON A.SiteCode = E.SiteCode ");
            SQL.AppendLine("    LEFT JOIN  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T1.SiteCode, SUM(T1.EstValue) Potension2 ");
            SQL.AppendLine("        FROM TblLOPHdr T1 ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("    Inner Join TblGroupSite A1 On T1.SiteCode = A1.SiteCode ");
                SQL.AppendLine("        And A1.GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("        Inner Join TblBOQHdr T2 On T1.DocNo = T2.LOPDocNo ");
            SQL.AppendLine("        Inner Join TblSOContractHdr T3 On T2.DocNo = T3.BOQDocNo ");
            SQL.AppendLine("            And (T3.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        WHERE T1.ProcessInd IN ('P', 'L') ");
            SQL.AppendLine("        AND T1.CancelInd = 'N' ");
            SQL.AppendLine("        AND T1.`Status` = 'A' ");
            SQL.AppendLine("        AND T1.ConfidentLvl = '2' ");
            SQL.AppendLine("        GROUP BY T1.SiteCode ");
            SQL.AppendLine("    ) F ON A.SiteCode = F.SiteCode ");
            SQL.AppendLine("    LEFT JOIN  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T1.SiteCode, SUM(T1.EstValue) Potension3 ");
            SQL.AppendLine("        FROM TblLOPHdr T1 ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("    Inner Join TblGroupSite A1 On T1.SiteCode = A1.SiteCode ");
                SQL.AppendLine("        And A1.GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("        Inner Join TblBOQHdr T2 On T1.DocNo = T2.LOPDocNo ");
            SQL.AppendLine("        Inner Join TblSOContractHdr T3 On T2.DocNo = T3.BOQDocNo ");
            SQL.AppendLine("            And (T3.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        WHERE T1.ProcessInd IN ('P', 'L') ");
            SQL.AppendLine("        AND T1.CancelInd = 'N' ");
            SQL.AppendLine("        AND T1.`Status` = 'A' ");
            SQL.AppendLine("        AND T1.ConfidentLvl = '3' ");
            SQL.AppendLine("        GROUP BY T1.SiteCode ");
            SQL.AppendLine("    ) G ON A.SiteCode = G.SiteCode ");
            SQL.AppendLine("    LEFT JOIN  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T1.SiteCode, SUM(T1.EstValue) Potension4 ");
            SQL.AppendLine("        FROM TblLOPHdr T1 ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("    Inner Join TblGroupSite A1 On T1.SiteCode = A1.SiteCode ");
                SQL.AppendLine("        And A1.GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("        Inner Join TblBOQHdr T2 On T1.DocNo = T2.LOPDocNo ");
            SQL.AppendLine("        Inner Join TblSOContractHdr T3 On T2.DocNo = T3.BOQDocNo ");
            SQL.AppendLine("            And (T3.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        WHERE T1.ProcessInd IN ('P', 'L') ");
            SQL.AppendLine("        AND T1.CancelInd = 'N' ");
            SQL.AppendLine("        AND T1.`Status` = 'A' ");
            SQL.AppendLine("        AND T1.ConfidentLvl = '4' ");            
            SQL.AppendLine("        GROUP BY T1.SiteCode ");
            SQL.AppendLine("    ) H ON A.SiteCode = H.SiteCode ");
            SQL.AppendLine("    LEFT JOIN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT T1.SiteCode, SUM(T5.Amt) AddendumAmt ");
            SQL.AppendLine("        FROM TblLOPHdr T1 ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("    Inner Join TblGroupSite A1 On T1.SiteCode = A1.SiteCode ");
                SQL.AppendLine("        And A1.GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("        INNER JOIN TblBOQHdr T2 ON T1.DocNo = T2.LOPDocNo ");
            SQL.AppendLine("        INNER JOIN TblSOContractHdr T3 ON T2.DocNo = T3.BOQDocNo ");
            SQL.AppendLine("            AND T3.DocDt BETWEEN @DocDt1 AND @DocDt2 ");
            SQL.AppendLine("        INNER JOIN  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            SELECT X1.SOCDocNo, MAX(X1.DocNo) DocNo ");
            SQL.AppendLine("            FROM TblSOContractRevisionHdr X1 ");
            SQL.AppendLine("            INNER JOIN TblSOContractHdr X2 ON X1.SOCDocNo = X2.DocNo ");
            SQL.AppendLine("            GROUP BY X1.SOCDocNo ");
            SQL.AppendLine("        ) T4 ON T3.DocNo = T4.SOCDocNo ");
            SQL.AppendLine("        INNER JOIN TblSOContractRevisionHdr T5 ON T4.DocNo = T5.DocNo ");
            SQL.AppendLine("        GROUP BY T1.SiteCode ");
            SQL.AppendLine("    ) I ON A.SiteCode = I.SiteCode ");
            SQL.AppendLine("    WHERE A.HOInd = 'N' ");
            SQL.AppendLine("    ORDER BY A.SiteCode ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Site Code",
                    "Site Name",
                    "Management Target"+Environment.NewLine+"Old Contract", 
                    "Management Target"+Environment.NewLine+"New Contract", 
                    "Management Target"+Environment.NewLine+"Total", 

                    //6-10
                    "Realization"+Environment.NewLine+"Old Contract", 
                    "Realization"+Environment.NewLine+"New Contract", 
                    "Realization"+Environment.NewLine+"Total", 
                    "Percentage"+Environment.NewLine+"(Realization of New Contract)", 
                    "Deviation Towards"+Environment.NewLine+"Management Target",  

                    //11-15
                    "Potency"+Environment.NewLine+"25%", 
                    "Potency"+Environment.NewLine+"50%", 
                    "Potency"+Environment.NewLine+"75%", 
                    "Potency"+Environment.NewLine+"90%", 
                    "Addendum", 
                },
                new int[] 
                {
                    //0
                    20, 

                    //1-5
                    100, 250, 150, 150, 150, 
                    
                    //6-10
                    150, 150, 150, 150, 150, 

                    //11-15
                    150, 150, 150, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)) return;

            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T.SiteCode; ",
                    new string[]
                    {
                        //0
                        "SiteCode",  
                        //1-5
                        "SiteName", "OutstandingSLIAmt", "TargetSOCAmt", "SumTargetAmt", "RevisionAmt", 
                        //6-10
                        "SumRealizationAmt", "Percentage", "Deviation", "Potension1", "Potension2",
                        //11-13
                        "Potension3", "Potension4", "AddendumAmt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });

                CalculateData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        private void CalculateData()
        {
            decimal mRKAPOld = 0m, mRKAPNew = 0m, mRKAPTotal = 0m,
                mRKAPPercentage = 0m, mRKAPDeviation = 0m,
                mPercentageRKAPOld = 0m, mPercentageRKAPNew = 0m, mPercentageRKAPTotal = 0m,
                mPercentageManagementOld = 0m, mPercentageManagementNew = 0m, mPercentageManagementTotal = 0m,
                mTotalManagementOld = 0m, mTotalManagementNew = 0m, mTotalManagementTotal = 0m,
                mTotalRealizationOld = 0m, mTotalRealizationNew = 0m, mTotalRealizationTotal = 0m;

            if(Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    {
                        mTotalManagementOld += Sm.GetGrdDec(Grd1, i, 3);
                        mTotalManagementNew += Sm.GetGrdDec(Grd1, i, 4);
                        mTotalManagementTotal += Sm.GetGrdDec(Grd1, i, 5);
                        mTotalRealizationOld += Sm.GetGrdDec(Grd1, i, 6);
                        mTotalRealizationNew += Sm.GetGrdDec(Grd1, i, 7);
                        mTotalRealizationTotal += Sm.GetGrdDec(Grd1, i, 8);
                    }
                }

                CalculateRKAP(ref mRKAPOld, ref mRKAPNew, ref mRKAPTotal, ref mRKAPPercentage, ref mRKAPDeviation,
                    mTotalRealizationOld, mTotalRealizationNew, mTotalRealizationTotal);

                CalculatePercentageRKAP(ref mPercentageRKAPOld, ref mPercentageRKAPNew, ref mPercentageRKAPTotal,
                    ref mRKAPOld, ref mRKAPTotal,
                    mTotalRealizationOld, mTotalRealizationNew, mTotalRealizationTotal);

                CalculatePercentageManagement(ref mPercentageManagementOld, ref mPercentageManagementNew, ref mPercentageManagementTotal,
                    mTotalManagementOld, mTotalManagementTotal,
                    mTotalRealizationOld, mTotalRealizationNew, mTotalRealizationTotal);

            }

            TxtRKAPOld.EditValue = Sm.FormatNum(mRKAPOld, 0);
            TxtRKAPNew.EditValue = Sm.FormatNum(mRKAPNew, 0);
            TxtRKAPTotal.EditValue = Sm.FormatNum(mRKAPTotal, 0);
            TxtRKAPPercentage.EditValue = Sm.FormatNum(mRKAPPercentage, 0);
            TxtRKAPDeviation.EditValue = Sm.FormatNum(mRKAPDeviation, 0);
            TxtPercentageRKAPOld.EditValue = Sm.FormatNum(mPercentageRKAPOld, 0);
            TxtPercentageRKAPNew.EditValue = Sm.FormatNum(mPercentageRKAPNew, 0);
            TxtPercentageRKAPTotal.EditValue = Sm.FormatNum(mPercentageRKAPTotal, 0);
            TxtPercentageManagementOld.EditValue = Sm.FormatNum(mPercentageManagementOld, 0);
            TxtPercentageManagementNew.EditValue = Sm.FormatNum(mPercentageManagementNew, 0);
            TxtPercentageManagementTotal.EditValue = Sm.FormatNum(mPercentageManagementTotal, 0);
        }

        private void CalculateRKAP(ref decimal RKAPOld,
            ref decimal RKAPNew,
            ref decimal RKAPTotal,
            ref decimal RKAPPercentage,
            ref decimal RKAPDeviation,
            decimal RealizationOld,
            decimal RealizationNew,
            decimal RealizationTotal)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT SUM(T.RKAPOld) RKAPOld, SUM(T.RKAPNew) RKAPNew ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT IFNULL(B.Amt, 0.00) RKAPOld, 0.00 RKAPNew ");
            SQL.AppendLine("    FROM TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        AND A.CancelInd = 'N' ");
            SQL.AppendLine("        AND A.Yr = (@Yr - 1) ");
            SQL.AppendLine("        AND B.Amt != 0 ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("        AND C.Parent IS NULL ");
            SQL.AppendLine("        And C.ActInd = 'Y' ");
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    SELECT 0.00 RKAPOld, IFNULL(B.Amt, 0.00) RKAPNew ");
            SQL.AppendLine("    FROM TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        AND A.CancelInd = 'N' ");
            SQL.AppendLine("        AND A.Yr = @Yr ");
            SQL.AppendLine("        AND B.Amt != 0 ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("        AND C.Parent IS NULL ");
            SQL.AppendLine("        And C.ActInd = 'Y' ");
            SQL.AppendLine(") T; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RKAPOld", "RKAPNew" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        RKAPOld = Sm.DrDec(dr, c[0]);
                        RKAPNew = Sm.DrDec(dr, c[1]);
                    }
                }
                dr.Close();
            }

            RKAPTotal = RKAPOld + RKAPNew;

            if (RKAPTotal - RealizationOld != 0) 
                RKAPPercentage = (RealizationNew / (RKAPTotal - RealizationOld)) * 100m;

            RKAPDeviation = RealizationTotal - RKAPTotal;
        }

        private void CalculatePercentageRKAP(ref decimal PercentageRKAPOld,
            ref decimal PercentageRKAPNew,
            ref decimal PercentageRKAPTotal,
            ref decimal RKAPOld,
            ref decimal RKAPTotal,
            decimal RealizationOld,
            decimal RealizationNew,
            decimal RealizationTotal)
        {
            if (RKAPOld != 0) PercentageRKAPOld = (RealizationOld / RKAPOld) * 100m;
            if (RKAPTotal - RealizationOld != 0) PercentageRKAPNew = (RealizationNew / (RKAPTotal - RealizationOld)) * 100m;
            if (RKAPTotal != 0) PercentageRKAPTotal = (RealizationTotal / RKAPTotal) * 100m;
        }

        private void CalculatePercentageManagement(ref decimal PercentageManagementOld,
            ref decimal PercentageManagementNew,
            ref decimal PercentageManagementTotal,
            decimal ManagementOld,
            decimal ManagementTotal,
            decimal RealizationOld,
            decimal RealizationNew,
            decimal RealizationTotal)
        {
            if (ManagementOld != 0) PercentageManagementOld = (RealizationOld / ManagementOld) * 100m;
            if (ManagementTotal - RealizationOld != 0) PercentageManagementNew = (RealizationNew / (ManagementTotal - RealizationOld)) * 100m;
            if (ManagementTotal != 0) PercentageManagementTotal = (RealizationTotal / ManagementTotal) * 100m;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 LueYr, LueMth, DteDocDt2
            });

            if (Sm.GetDte(DteDocDt1).Length > 0)
            {
                string mMth = Sm.GetDte(DteDocDt1).Substring(4, 2);
                string mYr = Sm.GetDte(DteDocDt1).Substring(0, 4);

                Sm.SetLue(LueYr, mYr);
                Sm.SetLue(LueMth, mMth);
                DteDocDt2.DateTime = Sm.ConvertDate(Sm.GetDte(DteDocDt1)).AddDays(6);
            }
        }

        #endregion

        #endregion
    }
}
