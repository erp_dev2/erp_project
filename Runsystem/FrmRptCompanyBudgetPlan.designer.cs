﻿namespace RunSystem
{
    partial class FrmRptCompanyBudgetPlan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptCompanyBudgetPlan));
            this.LueYr = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LblMultiCCCode = new System.Windows.Forms.Label();
            this.LblMultiProfitCenterCode = new System.Windows.Forms.Label();
            this.CcbProfitCenterCode = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.CcbCCCode = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.BtnRefreshCostCenter = new DevExpress.XtraEditors.SimpleButton();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtDirectorateGroup = new DevExpress.XtraEditors.TextEdit();
            this.BtnDirectorateGroup = new DevExpress.XtraEditors.SimpleButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.ChkDirectorateGroup = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbCCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectorateGroup.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDirectorateGroup.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(905, 0);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.BtnRefreshCostCenter);
            this.panel2.Controls.Add(this.CcbCCCode);
            this.panel2.Controls.Add(this.CcbProfitCenterCode);
            this.panel2.Controls.Add(this.LblMultiProfitCenterCode);
            this.panel2.Controls.Add(this.LblMultiCCCode);
            this.panel2.Controls.Add(this.LueYr);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Size = new System.Drawing.Size(905, 74);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ReadOnly = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(905, 399);
            this.Grd1.TabIndex = 19;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Size = new System.Drawing.Size(905, 399);
            // 
            // LueYr
            // 
            this.LueYr.EnterMoveNextControl = true;
            this.LueYr.Location = new System.Drawing.Point(81, 3);
            this.LueYr.Margin = new System.Windows.Forms.Padding(5);
            this.LueYr.Name = "LueYr";
            this.LueYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.Appearance.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueYr.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueYr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueYr.Properties.DropDownRows = 12;
            this.LueYr.Properties.NullText = "[Empty]";
            this.LueYr.Properties.PopupWidth = 100;
            this.LueYr.Size = new System.Drawing.Size(75, 20);
            this.LueYr.TabIndex = 9;
            this.LueYr.ToolTip = "F4 : Show/hide list";
            this.LueYr.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(46, 6);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 14);
            this.label4.TabIndex = 8;
            this.label4.Text = "Year";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblMultiCCCode
            // 
            this.LblMultiCCCode.AutoSize = true;
            this.LblMultiCCCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMultiCCCode.ForeColor = System.Drawing.Color.Black;
            this.LblMultiCCCode.Location = new System.Drawing.Point(6, 48);
            this.LblMultiCCCode.Name = "LblMultiCCCode";
            this.LblMultiCCCode.Size = new System.Drawing.Size(72, 14);
            this.LblMultiCCCode.TabIndex = 12;
            this.LblMultiCCCode.Text = "Cost Center";
            // 
            // LblMultiProfitCenterCode
            // 
            this.LblMultiProfitCenterCode.AutoSize = true;
            this.LblMultiProfitCenterCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMultiProfitCenterCode.ForeColor = System.Drawing.Color.Black;
            this.LblMultiProfitCenterCode.Location = new System.Drawing.Point(1, 27);
            this.LblMultiProfitCenterCode.Name = "LblMultiProfitCenterCode";
            this.LblMultiProfitCenterCode.Size = new System.Drawing.Size(77, 14);
            this.LblMultiProfitCenterCode.TabIndex = 10;
            this.LblMultiProfitCenterCode.Text = "Profit Center";
            // 
            // CcbProfitCenterCode
            // 
            this.CcbProfitCenterCode.Location = new System.Drawing.Point(81, 24);
            this.CcbProfitCenterCode.Name = "CcbProfitCenterCode";
            this.CcbProfitCenterCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CcbProfitCenterCode.Properties.DropDownRows = 30;
            this.CcbProfitCenterCode.Properties.PopupFormSize = new System.Drawing.Size(600, 300);
            this.CcbProfitCenterCode.Size = new System.Drawing.Size(394, 20);
            this.CcbProfitCenterCode.TabIndex = 11;
            // 
            // CcbCCCode
            // 
            this.CcbCCCode.Location = new System.Drawing.Point(81, 45);
            this.CcbCCCode.Name = "CcbCCCode";
            this.CcbCCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CcbCCCode.Properties.DropDownRows = 30;
            this.CcbCCCode.Properties.PopupFormSize = new System.Drawing.Size(600, 300);
            this.CcbCCCode.Size = new System.Drawing.Size(394, 20);
            this.CcbCCCode.TabIndex = 13;
            // 
            // BtnRefreshCostCenter
            // 
            this.BtnRefreshCostCenter.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRefreshCostCenter.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefreshCostCenter.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefreshCostCenter.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefreshCostCenter.Appearance.Options.UseBackColor = true;
            this.BtnRefreshCostCenter.Appearance.Options.UseFont = true;
            this.BtnRefreshCostCenter.Appearance.Options.UseForeColor = true;
            this.BtnRefreshCostCenter.Appearance.Options.UseTextOptions = true;
            this.BtnRefreshCostCenter.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRefreshCostCenter.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRefreshCostCenter.Image = ((System.Drawing.Image)(resources.GetObject("BtnRefreshCostCenter.Image")));
            this.BtnRefreshCostCenter.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRefreshCostCenter.Location = new System.Drawing.Point(477, 43);
            this.BtnRefreshCostCenter.Name = "BtnRefreshCostCenter";
            this.BtnRefreshCostCenter.Size = new System.Drawing.Size(24, 21);
            this.BtnRefreshCostCenter.TabIndex = 14;
            this.BtnRefreshCostCenter.ToolTip = "Refresh Cost Center";
            this.BtnRefreshCostCenter.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRefreshCostCenter.ToolTipTitle = "Run System";
            this.BtnRefreshCostCenter.Click += new System.EventHandler(this.BtnRefreshCostCenter_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(10, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 14);
            this.label6.TabIndex = 16;
            this.label6.Text = "Directorate Group";
            // 
            // TxtDirectorateGroup
            // 
            this.TxtDirectorateGroup.EnterMoveNextControl = true;
            this.TxtDirectorateGroup.Location = new System.Drawing.Point(121, 3);
            this.TxtDirectorateGroup.Name = "TxtDirectorateGroup";
            this.TxtDirectorateGroup.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtDirectorateGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDirectorateGroup.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDirectorateGroup.Properties.Appearance.Options.UseFont = true;
            this.TxtDirectorateGroup.Properties.MaxLength = 30;
            this.TxtDirectorateGroup.Properties.ReadOnly = true;
            this.TxtDirectorateGroup.Size = new System.Drawing.Size(229, 20);
            this.TxtDirectorateGroup.TabIndex = 17;
            this.TxtDirectorateGroup.EditValueChanged += new System.EventHandler(this.TxtDirectorateGroup_EditValueChanged);
            // 
            // BtnDirectorateGroup
            // 
            this.BtnDirectorateGroup.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDirectorateGroup.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDirectorateGroup.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDirectorateGroup.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDirectorateGroup.Appearance.Options.UseBackColor = true;
            this.BtnDirectorateGroup.Appearance.Options.UseFont = true;
            this.BtnDirectorateGroup.Appearance.Options.UseForeColor = true;
            this.BtnDirectorateGroup.Appearance.Options.UseTextOptions = true;
            this.BtnDirectorateGroup.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDirectorateGroup.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDirectorateGroup.Image = ((System.Drawing.Image)(resources.GetObject("BtnDirectorateGroup.Image")));
            this.BtnDirectorateGroup.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDirectorateGroup.Location = new System.Drawing.Point(350, 2);
            this.BtnDirectorateGroup.Name = "BtnDirectorateGroup";
            this.BtnDirectorateGroup.Size = new System.Drawing.Size(24, 21);
            this.BtnDirectorateGroup.TabIndex = 18;
            this.BtnDirectorateGroup.ToolTip = "Refresh Directorate Group";
            this.BtnDirectorateGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDirectorateGroup.ToolTipTitle = "Run System";
            this.BtnDirectorateGroup.Click += new System.EventHandler(this.BtnDirectorateGroup_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.ChkDirectorateGroup);
            this.panel5.Controls.Add(this.TxtDirectorateGroup);
            this.panel5.Controls.Add(this.BtnDirectorateGroup);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(507, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(394, 70);
            this.panel5.TabIndex = 15;
            this.panel5.Visible = false;
            // 
            // ChkDirectorateGroup
            // 
            this.ChkDirectorateGroup.Location = new System.Drawing.Point(374, 2);
            this.ChkDirectorateGroup.Name = "ChkDirectorateGroup";
            this.ChkDirectorateGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDirectorateGroup.Properties.Appearance.Options.UseFont = true;
            this.ChkDirectorateGroup.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDirectorateGroup.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDirectorateGroup.Properties.Caption = " ";
            this.ChkDirectorateGroup.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDirectorateGroup.Size = new System.Drawing.Size(18, 22);
            this.ChkDirectorateGroup.TabIndex = 19;
            this.ChkDirectorateGroup.ToolTip = "Remove filter";
            this.ChkDirectorateGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDirectorateGroup.ToolTipTitle = "Run System";
            this.ChkDirectorateGroup.CheckedChanged += new System.EventHandler(this.ChkDirectorateGroup_CheckedChanged);
            // 
            // FrmRptCompanyBudgetPlan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 473);
            this.Name = "FrmRptCompanyBudgetPlan";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbCCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectorateGroup.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDirectorateGroup.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit LueYr;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LblMultiProfitCenterCode;
        private System.Windows.Forms.Label LblMultiCCCode;
        private DevExpress.XtraEditors.CheckedComboBoxEdit CcbCCCode;
        private DevExpress.XtraEditors.CheckedComboBoxEdit CcbProfitCenterCode;
        public DevExpress.XtraEditors.SimpleButton BtnRefreshCostCenter;
        public DevExpress.XtraEditors.SimpleButton BtnDirectorateGroup;
        private System.Windows.Forms.Label label6;
        protected System.Windows.Forms.Panel panel5;
        internal DevExpress.XtraEditors.TextEdit TxtDirectorateGroup;
        private DevExpress.XtraEditors.CheckEdit ChkDirectorateGroup;
    }
}