﻿#region Update
/*
    05/04/2023 [WED/PHT] new apps, to display employee spouse
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmployeeDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmEmployee mFrmParent;

        #endregion

        #region Constructor

        public FrmEmployeeDlg3(FrmEmployee FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
                SetLueStatus(ref LueStatus);
                SetLueSSInd(ref LueSSInd);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
                Sm.SetLue(LueStatus, "1");
                Sl.SetLueSectionCode(ref LueSection);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL(string Filter, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.EmpCode, A.EmpName, B.UserName, A.EmpCodeOld, O.DivisionName, A.DeptCode,  C.DeptName, D.PosName, ");
            if (mFrmParent.mIsPltCheckboxNotBasedOnPosition)
                SQL.AppendLine(" A.ActingOfficialInd, ");
            else
                SQL.AppendLine(" D.ActingOfficialInd, ");
            SQL.AppendLine(" Z.PositionStatusName, ");

            if (mFrmParent.mIsEmployeeUseGradeSalary)
                SQL.AppendLine("AG.GrdSalaryName As GrdLvlName, ");
            else
                SQL.AppendLine("N.GrdLvlName,  ");

            if (mFrmParent.mIsEmployeeLevelEnabled)
                SQL.AppendLine("    AF.LevelName, ");
            else
                SQL.AppendLine("    V.LevelName, ");
            SQL.AppendLine("    A.SiteCode, M.SiteName, A.JoinDt, A.ResignDt, ");
            SQL.AppendLine("    A.IdNumber, E.OptDesc As Gender, F.OptDesc As Religion, A.ConductDtTm, ");
            SQL.AppendLine("    X.LastAssessment, A.BirthPlace, A.BirthDt, ");
            SQL.AppendLine("    Case When A.BirthDt Is Null Then 0.00 Else TimeStampDiff(Year, Str_To_Date(A.BirthDt, '%Y%m%d'), Str_To_Date(Replace(CurDate(), '-', ''), '%Y%m%d')) End As Age, ");
            SQL.AppendLine("    A.Address, G.CityName, A.PostalCode, A.Phone, A.Mobile, A.Email, A.NPWP, Q.PGName, H.OptDesc As PayrollType, R.OptDesc As PayrunPeriod, A.PTKP, ");
            SQL.AppendLine("    I.BankName As BankName, A.BankAcNo As BankAcNo, A.BankBranch, K.OptDesc As EmploymentStatusDesc, L.OptDesc As SystemTypeDesc, P.EntName, W.EntName As RegEntName, S.Level, S.Major, ");
            SQL.AppendLine("    if(length(T.EmpCode)>0, 'Y', 'N') As SSind, if(length(U.EmpCode)>0, 'Y', 'N') As SSind2, A.LeaveStartDt, Y.SectionName, A.UserCode, ");
            if (mFrmParent.mIsEmployeeFindShowFamily)
                SQL.AppendLine("    AA.FamilyInfo, ");
            else
                SQL.AppendLine("    Null As FamilyInfo, ");
            SQL.AppendLine("    AB.OptDesc As ClothesSizeDesc, AC.OptDesc As ShoeSizeDesc, A.CostGroup, AE.OptDesc As MaritalStatusDesc, ");
            SQL.AppendLine("    A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, A.TGDt, A.ContractDt, ");
            SQL.AppendLine("    TimeStampdiff(Year, A.JoinDt, left(Replace(CurDate(), '-', ''), 8)) As YearsOfService, ");
            if (mFrmParent.mIsEmpMeritIncreaseExists)
                SQL.AppendLine("    AH.EmpMeritIncreaseStartDt, ");
            else
                SQL.AppendLine("    Null as EmpMeritIncreaseStartDt, ");
            if (mFrmParent.mIsEmployeeVaccineEnabled)
                SQL.AppendLine("    AI.VaccineCount ");
            else
                SQL.AppendLine("    0.00 As VaccineCount ");
            SQL.AppendLine("    , AJ.OptDesc as BloodType, AK.OptDesc as RhesusType, AL.GrdLvlName FirstGrdLvlName, A.Mother  ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    Left Join TblUser B On A.UserCode=B.UserCode ");
            SQL.AppendLine("    left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("    Left Join TblPosition D On A.PosCode=D.PosCode ");
            SQL.AppendLine("    Left Join TblOption E On A.Gender=E.OptCode And E.OptCat='Gender' ");
            SQL.AppendLine("    Left Join TblOption F On A.Religion=F.OptCode And F.OptCat='Religion' ");
            SQL.AppendLine("    Left Join TblCity G On A.CityCode=G.CityCode ");
            SQL.AppendLine("    Left Join tblOption H On A.PayrollType=H.OptCode And H.OptCat='EmployeePayrollType' ");
            SQL.AppendLine("    Left Join TblBank I On A.BankCode=I.BankCode ");
            //SQL.AppendLine("    Left Join TblBankAccount J On A.BankAcNo=J.BankAcNo ");
            SQL.AppendLine("    Left Join TblOption K On A.EmploymentStatus=K.OptCode And K.OptCat='EmploymentStatus' ");
            SQL.AppendLine("    Left Join TblOption L On A.SystemType=L.OptCode And L.OptCat='EmpSystemType' ");
            SQL.AppendLine("    Left Join TblSite M On A.SiteCode=M.SiteCode ");
            SQL.AppendLine("    left Join TblGradeLevelHdr N On A.GrdLvlCode=N.GrdLvlCode ");
            SQL.AppendLine("    Left join TblDivision O On A.DivisionCode = O.DivisionCode ");
            SQL.AppendLine("    Left Join TblEntity P On A.EntCode = P.EntCode ");
            SQL.AppendLine("    Left Join TblPayrollGrpHdr Q On A.PGCode=Q.PGCode ");
            SQL.AppendLine("    Left Join TblOption R On A.PayrunPeriod=R.OptCode And R.OptCat='PayrunPeriod' ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Distinct T1.EmpCode, group_concat(T2.OptDesc) As Level, group_concat(T3.MajorName) As major  ");
            SQL.AppendLine("        From TblEmployeeEducation T1  ");
            SQL.AppendLine("        Left Join TblOption T2 On T1.Level=T2.OptCode And T2.OptCat='EmployeeEducationLevel'  ");
            SQL.AppendLine("        Left Join Tblmajor T3 On T1.major = T3.MajorCode ");
            SQL.AppendLine("        Where T1.HighestInd = 'Y'  ");
            SQL.AppendLine("        And T1.EmpCode In (Select EmpCode From TblEmployee Where 1=1 " + Filter + ") ");
            SQL.AppendLine("        Group by T1.EmpCode ");
            SQL.AppendLine("    ) S On A.EmpCode=S.EmpCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Distinct A.EmpCode From TblEmployeeSS A ");
            SQL.AppendLine("        Inner Join TblSS B On A.SSCode = B.SSCode And B.SSPCode Is Not Null ");
            SQL.AppendLine("        Inner Join TblSSProgram C On B.SSPCode = C.SSpCode ");
            SQL.AppendLine("        And Find_In_Set( ");
            SQL.AppendLine("            IfNull(C.SSpCode, ''), ");
            SQL.AppendLine("            IfNull((Select ParValue From TblParameter Where ParCode='SSProgramForEmployment' And ParValue Is Not Null), '') ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And A.SSCode Is Not Null ");
            SQL.AppendLine("        And (EndDt Is Null Or (EndDt is Not Null And EndDt>Left(CurrentDateTime(), 8))) ");
            SQL.AppendLine("        And A.EmpCode In (Select EmpCode From TblEmployee Where 1=1 " + Filter + ") ");
            SQL.AppendLine("    ) T On A.EmpCode = T.EmpCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Distinct A.EmpCode From TblEmployeeSS A ");
            SQL.AppendLine("        Inner Join Tblss B On A.SSCode = B.SSCode ");
            SQL.AppendLine("        Inner Join TblSSProgram C On B.SSpCode = C.SSpCode ");
            SQL.AppendLine("        Where (EndDt Is Null Or (EndDt is Not Null And EndDt>Left(CurrentDateTime(), 8))) ");
            SQL.AppendLine("        And C.SSpCode = (Select Parvalue From TblParameter Where ParCode = 'SSProgramForHealth') ");
            SQL.AppendLine("        And A.EmpCode In (Select EmpCode From TblEmployee Where 1=1 " + Filter + ") ");
            SQL.AppendLine("    ) U On A.EmpCode=U.EmpCode ");
            SQL.AppendLine("    Left Join TblLevelHdr V On N.LevelCode=V.LevelCode ");
            SQL.AppendLine("    Left Join TblEntity W On A.RegEntCode = W.EntCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.EmpCode, T1.DocDt As LastAssessment ");
            SQL.AppendLine("        From TblAssesmentProcessHdr T1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select EmpCode, Max(Concat(DocDt, DocNo)) Key1 ");
            SQL.AppendLine("            From TblAssesmentProcessHdr ");
            SQL.AppendLine("            Where CancelInd='N' ");
            SQL.AppendLine("            And EmpCode In (Select EmpCode From TblEmployee Where 1=1 " + Filter + ") ");
            SQL.AppendLine("            Group By EmpCode ");
            SQL.AppendLine("        ) T2 On T1.EmpCode=T2.EmpCode And Concat(T1.DocDt, T1.DocNo)=key1 ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.EmpCode In (Select EmpCode From TblEmployee Where 1=1 " + Filter + ") ");
            SQL.AppendLine("    ) X On A.EmpCode=X.EmpCode ");
            SQL.AppendLine("    Left Join TblSection Y On A.SectionCode = Y.SectionCode ");
            SQL.AppendLine("    Left Join TblPositionStatus Z On A.PositionStatusCode=Z.PositionStatusCode ");
            if (mFrmParent.mIsEmployeeFindShowFamily)
            {
                SQL.AppendLine("    Left Join (");
                SQL.AppendLine("        Select EmpCode, ");
                SQL.AppendLine("        Group_Concat(Concat(FamilyName, Case When BirthDt Is Null Then '' Else Concat(' (', Date_Format(BirthDt, '%d/%m/%Y'), ')') End) Order By FamilyName Separator ', ') As FamilyInfo ");
                SQL.AppendLine("        From TblEmployeeFamily ");
                SQL.AppendLine("        Where EmpCode In (Select EmpCode From TblEmployee Where 1=1 " + Filter + ") ");
                SQL.AppendLine("        Group By EmpCode ");
                SQL.AppendLine("    ) AA On A.EmpCode=AA.EmpCode ");
            }
            SQL.AppendLine("    Left Join TblOption AB On A.ClothesSize=AB.OptCode And AB.OptCat='EmployeeClothesSize' ");
            SQL.AppendLine("    Left Join TblOption AC On A.ShoeSize=AC.OptCode And AC.OptCat='EmployeeShoeSize' ");
            SQL.AppendLine("    Left Join TblOption AD On A.CostGroup=AD.OptCode And AD.OptCat='EmpCostGroup' ");
            SQL.AppendLine("    Left Join TblOption AE On A.MaritalStatus=AE.OptCode And AE.OptCat='MaritalStatus' ");
            SQL.AppendLine("    Left Join TblLevelHdr AF On A.LevelCode=AF.LevelCode ");
            if (mFrmParent.mIsEmployeeUseGradeSalary)
                SQL.AppendLine("    Left Join TblGradeSalaryHdr AG On A.GrdLvlCode = AG.GrdSalaryCode ");

            if (mFrmParent.mIsEmpMeritIncreaseExists)
            {
                SQL.AppendLine("    Left Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select T2.EmpCode, Max(T1.StartDt) EmpMeritIncreaseStartDt ");
                SQL.AppendLine("        From TblEmpMeritIncreaseHdr T1 ");
                SQL.AppendLine("        Inner Join TblEmpMeritIncreaseDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("            And T1.CancelInd = 'N' ");
                SQL.AppendLine("            And T1.Status = 'A' ");
                SQL.AppendLine("        Where T2.EmpCode In (Select EmpCode From TblEmployee Where 1=1 " + Filter + ") ");
                SQL.AppendLine("        Group By T2.EmpCode ");
                SQL.AppendLine("    ) AH On A.EmpCode = AH.EmpCode ");
            }
            if (mFrmParent.mIsEmployeeVaccineEnabled)
            {
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select EmpCode, Count(1) VaccineCount ");
                SQL.AppendLine("        From TblEmployeeVaccine ");
                SQL.AppendLine("        Where EmpCode In (Select EmpCode From TblEmployee Where 1=1 " + Filter + ") ");
                SQL.AppendLine("        Group By EmpCode ");
                SQL.AppendLine("    ) AI On A.EmpCode=AI.EmpCode ");
            }
            SQL.AppendLine("    Left Join TblOption AJ On A.BloodType=AJ.OptCode And AJ.OptCat='BloodType' ");
            SQL.AppendLine("    Left Join TblOption AK On A.RhesusType=AK.OptCode And AK.OptCat='BloodRhesusType' ");
            SQL.AppendLine("    Left Join TblGradeLevelHdr AL On A.FirstGrdLvlCode = AL.GrdLvlCode ");
            SQL.AppendLine("Where A.EmpCode In (Select EmpCode From TblEmployee Where 1=1 " + Filter + ") ");
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ");
                SQL.AppendLine("(A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("And (A.DeptCode Is Null Or ");
                SQL.AppendLine("(A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine(") T ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine(" Order By DeptName, EmpName;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 68;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Division",
                        "Department",

                        //6-10
                        "Position",
                        "Position Status",
                        "Grade",
                        "Level",
                        "Site",
                        
                        //11-15
                        "Join",
                        "Resign",
                        "Identity#",
                        "Gender",
                        "Religion",
                        
                        //16-20
                        "Employment Status",
                        "Last"+Environment.NewLine+"Assessment",
                        "Birth"+Environment.NewLine+"Date",
                        "Age",
                        "Address",
                        
                        //21-25
                        "City",
                        "Postal"+Environment.NewLine+"Code",
                        "Phone",
                        "Mobile",
                        "Email",
                        
                        //26-30
                        "NPWP",
                        "System Type",
                        "Payroll Group",
                        "Payroll Type",
                        "Payrun Period",
                        
                        //31-35
                        "PTKP",
                        "Bank",
                        "Bank Account#",
                        "Bank"+Environment.NewLine+"Branch",
                        "SS Employement",
                        
                        //36-40
                        "SS Health",
                        "Entity",
                        "Entity (SS)",
                        "Education Level",
                        "Education Major",
                        
                        //41-45
                        "Permanent Date",
                        "Section",
                        "User Code",
                        "Family",
                        "Clothes Size",

                        //46-50
                        "Shoe Size",
                        "Created By",
                        "Created Date",
                        "Created Time",
                        "Last Updated By",
                        
                        //51-55
                        "Last Updated Date",
                        "Last Updated Time",
                        "Training Graduation",
                        "Contract Date",
                        "Cost Group",

                        //56-60
                        "Marital Status",
                        "Acting Official",
                        "Last Seen Code"+Environment.NewLine+"of Conduct Date",
                        "Last Seen Code"+Environment.NewLine+"of Conduct Time",
                        "Years of Service",

                        //61-65
                        "Merit Increase"+Environment.NewLine+"Start Date",
                        "Birth Place",
                        "Vaccine",
                        "Blood Type",
                        "Blood Rhesus",

                        //66-67
                        "First Grade",
                        "Biological Mother"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        130, 250, 100, 150, 150, 
                        
                        //6-10
                        200, 100, 130, 150, 150, 
                        
                        //11-15
                        80, 80, 130, 80, 80, 

                        //16-20
                        120, 100, 80, 80, 300, 

                        //21-25
                        150, 80, 80, 80, 80, 

                        //26-30
                        130, 80, 100, 80, 100, 

                        //31-35
                        80, 200, 200, 130, 80, 
                      
                        //36-40
                        80, 150, 150, 250, 250, 

                        //41-45
                        100, 180, 100, 200, 120, 
                        
                        //46-50
                        120, 130, 130, 130, 130, 
                        
                        //51-55
                        130, 130, 130, 100, 100,

                        //56-60
                        150, 150, 130, 130, 130,

                        //61-65
                        120, 100, 80, 120, 120,

                        //66-67
                        130, 150,
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 35, 36, 57 });
            Sm.GrdFormatDate(Grd1, new int[] { 11, 12, 17, 18, 41, 48, 51, 53, 54, 58, 61 });
            Sm.GrdFormatTime(Grd1, new int[] { 49, 52, 59 });
            Sm.GrdFormatDec(Grd1, new int[] { 19, 60, 63 }, 11);
            Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 35, 36, 43, 45, 46, 47, 48, 49, 50, 51, 52 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 44 }, mFrmParent.mIsEmployeeFindShowFamily);
            Sm.GrdColInvisible(Grd1, new int[] { 66 }, mFrmParent.mIsEmployeeUseFirstGrade);
            if (!mFrmParent.mIsEmployeeUseCodeOfConduct) Sm.GrdColInvisible(Grd1, new int[] { 58, 59 });
            if (!mFrmParent.mIsEmpContractDtMandatory) Sm.GrdColInvisible(Grd1, new int[] { 54, 55 });
            if (!mFrmParent.mIsEmployeeVaccineEnabled) Sm.GrdColInvisible(Grd1, new int[] { 63 });
            if (mFrmParent.mIsEmpMeritIncreaseExists)
                Grd1.Cols[61].Move(42);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 61 });
            Grd1.Cols[60].Move(12);
            Grd1.Cols[53].Move(14);
            Grd1.Cols[54].Move(11);
            Grd1.Cols[55].Move(12);
            Grd1.Cols[56].Move(23);
            Grd1.Cols[57].Move(7);
            Grd1.Cols[58].Move(52);
            Grd1.Cols[59].Move(53);
            Grd1.Cols[62].Move(23);
            Grd1.Cols[63].Move(47);
            Grd1.Cols[64].Move(25);
            Grd1.Cols[65].Move(26);
            Grd1.Cols[66].Move(52);
            Grd1.Cols[67].Move(27);
            if (!mFrmParent.mIsEmployeeUseBiologicalMother) Sm.GrdColInvisible(Grd1, new int[] { 67 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 35, 36, 43, 45, 46, 47, 48, 49, 50, 51, 52 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ", Filter2 = string.Empty;
                var cm = new MySqlCommand();

                switch (Sm.GetLue(LueStatus))
                {
                    case "1":
                        Filter = " And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>@CurrentDate)) ";
                        break;
                    case "2":
                        Filter = " And ResignDt Is Not Null And ResignDt<=@CurrentDate ";
                        break;
                }

                switch (Sm.GetLue(LueSSInd))
                {
                    case "1":
                        Filter2 = " Where SSInd='Y' ";
                        break;
                    case "2":
                        Filter2 = " Where SSInd='N' ";
                        break;
                }

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@BirthDt", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());

                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "BirthDt");
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteLeaveStartDt1), Sm.GetDte(DteLeaveStartDt2), "LeaveStartDt");
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "EmpCode", "EmpCodeOld", "EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSection), "SectionCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter, Filter2),
                        new string[]
                        {
                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName","EmpCodeOld", "DivisionName", "DeptName","PosName", 

                            //6-10
                            "PositionStatusName", "GrdLvlName", "LevelName", "SiteName", "JoinDt",
                            
                            //11-15
                            "ResignDt", "IdNumber", "Gender", "Religion", "EmploymentStatusDesc", 
                            
                            //16-20
                            "LastAssessment", "BirthDt", "Age", "Address", "CityName", 
                            
                            //21-25
                            "PostalCode", "Phone", "Mobile", "Email", "NPWP", 
                            
                            //26-30
                            "SystemTypeDesc", "PGName", "PayrollType", "PayrunPeriod", "PTKP", 
                            
                            //31-35
                            "BankName", "BankAcNo", "BankBranch", "SSInd", "SSInd2", 
                            
                            //36-40
                            "EntName", "RegEntName", "Level", "Major", "LeaveStartDt", 
                            
                            //41-45
                            "SectionName",  "UserCode", "FamilyInfo", "ClothesSizeDesc", "ShoeSizeDesc", 
                            
                            //46-50
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt", "TGDt",

                            //51-55
                            "ContractDt", "CostGroup", "MaritalStatusDesc", "ActingOfficialInd", "ConductDtTm",

                            //56-60
                            "YearsOfService", "EmpMeritIncreaseStartDt", "BirthPlace", "VaccineCount", "BloodType", 
                            
                            //61-63
                            "RhesusType", "FirstGrdLvlName", "Mother"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 32);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 33);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 35, 34);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 36, 35);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 36);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 37);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 38);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 39);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 41, 40);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 41);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 42);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 44, 43);
                            if (mFrmParent.mIsEmployeeFindShowFamily)
                                Grd.Cells[Row, 44].Value = Sm.GetGrdStr(Grd, Row, 44).Replace(",", "," + Environment.NewLine);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 45, 44);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 46, 45);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 47, 46);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 48, 47);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 49, 47);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 50, 48);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 51, 49);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 52, 49);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 53, 50);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 54, 51);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 55, 52);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 56, 53);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 57, 54);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 58, 55);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 59, 55);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 60, 56);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 61, 57);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 62, 58);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 63, 59);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 64, 60);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 65, 61);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 66, 62);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 67, 63);
                        }, true, false, false, false
                    );
                if (mFrmParent.mIsEmployeeFindShowFamily)
                {
                    Grd1.BeginUpdate();
                    Grd1.Rows.AutoHeight();
                    Grd1.EndUpdate();
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtSpouseEmpCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                this.Hide();
            }
        }

        #endregion

        #region Grid Methods

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                var f = new FrmEmployee("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.DoDefault = false;
                var f = new FrmEmployee("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Active Employee' As Col2 Union All " +
                "Select '2' As Col1, 'Resignee' As Col2; ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void SetLueSSInd(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Participant' As Col2 Union All " +
                "Select '2' As Col1, 'Not Participant' As Col2;",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employee's status");
        }

        private void LueSSInd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSSInd, new Sm.RefreshLue1(SetLueSSInd));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSSInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Social security");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Birth date");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void DteLeaveStartDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteLeaveStartDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkLeaveStartDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Permanent date");
        }

        private void ChkSection_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Section");
        }

        private void LueSection_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSection, new Sm.RefreshLue1(Sl.SetLueSectionCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion

        #endregion

    }
}
