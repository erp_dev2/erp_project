﻿#region Update
/*
    22/06/2021 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBudget2Dlg2 : RunSystem.FrmBase3
    {
        #region Field, Property

        private FrmBudget2 mFrmParent;
        private string mPrevDocNo = string.Empty;
        //internal FrmFind FrmFind;

        #endregion

        #region Constructor

        public FrmBudget2Dlg2(FrmBudget2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPrevDocNo = mFrmParent.TxtDocNo.Text;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            this.Text = "New Budget";

            try
            {
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnDelete.Visible = BtnPrint.Visible = false;
                SetGrd();
                LueEntCode.Visible = mFrmParent.mIsBudgetRequestUseItemAndQty;
                label9.Visible = mFrmParent.mIsBudgetRequestUseItemAndQty;
                SetFormControl(mState.Insert);
                ShowPrevData(mPrevDocNo, true);
                ComputeTotalAmount();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "Budget Request DNo",

                    //1-5
                    "Budget"+Environment.NewLine+"Category",
                    "Budget"+Environment.NewLine+"Category",
                    "Requested"+Environment.NewLine+"Amount",
                    "Previous" + Environment.NewLine + "Budget"+Environment.NewLine+"Amount",
                    "Budget"+Environment.NewLine+"Amount",

                    //6-10
                    "Usage",
                    "Remark",
                    "Local"+Environment.NewLine+"Code",
                    "Item's Code",
                    "Item's Name",

                    //11-12
                    "",
                    "Quantity",
                },
                new int[] 
                {
                    //0
                    0,

                    //1-5
                    0, 200, 130, 130, 130,

                    //6-10
                    130, 300, 100, 120, 150,

                    //11-12
                    20, 100
                }
            );
            Grd1.Cols[8].Move(0);
            Grd1.Cols[7].Move(12);

            Sm.GrdColButton(Grd1, new int[] { 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 12 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 4, 6, 9 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 8, 9, 10, 12 });

            if (!mFrmParent.mIsBudgetRequestUseItemAndQty)
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12 }, false);
        }

        override protected void HideInfoInGrd()
        {
            if (BtnSave.Enabled) Sm.GrdColInvisible(Grd1, new int[] { 4, 6 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mFrmParent.mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeRemark, LueEntCode }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 5, 7, 11 });
                    Sm.GrdColInvisible(Grd1, new int[] { 4, 6 }, true);
                    TxtDocNo.Focus();
                    BtnSave.Visible = BtnCancel.Visible = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 5, 7, 11 });
                    Sm.GrdColInvisible(Grd1, new int[] { 4, 6 }, !ChkHideInfoInGrd.Checked);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, 
                DteDocDt, TxtBudgetRequestDocNo, TxtYr, TxtMth, TxtDeptCode,TxtStatus, 
                MeeRemark, LueEntCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtBudgetRequestAmt, TxtAmt }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 6 });
            Sm.FocusGrd(Grd1, 0, 2);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0) InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!(BtnSave.Enabled && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0 && Sm.IsGrdColSelected(new int[] { 5, 7 }, e.ColIndex)))
                e.DoDefault = false;
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 5 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 7 }, e);
            if (e.ColIndex == 5) ComputeTotalAmount();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Budget", "TblBudgetHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(CancelPreviousBudget(mPrevDocNo));
            cml.Add(SaveBudgetHdr(DocNo));
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) cml.Add(SaveBudgetDtl(DocNo, r));

            Sm.ExecCommands(cml);

            ShowPrevData(DocNo, false);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtBudgetRequestDocNo, "Budget request#", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 budget category.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            decimal BudgetRequestAmt = 0m, BudgetAmt = 0m, UsageAmt = 0m;

            RecomputeUsage();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Budget category is empty.")) return true;
                Msg = "Budget category : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine;

                BudgetRequestAmt = Sm.GetGrdDec(Grd1, Row, 3);
                BudgetAmt = Sm.GetGrdDec(Grd1, Row, 5);
                UsageAmt = Sm.GetGrdDec(Grd1, Row, 6);

                //if (BudgetAmt > BudgetRequestAmt)
                //{
                //    Sm.StdMsg(mMsgType.Warning, 
                //        Msg +
                //        "Budget category : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                //        "Budget amount : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                //        "Requested amount : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine + Environment.NewLine +
                //        "Budget amount is bigger than requested amount."
                //        );
                //}

                if (BudgetAmt < UsageAmt)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Budget category : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Budget amount : " + Sm.FormatNum(BudgetAmt, 0) + Environment.NewLine +
                        "Used amount : " + Sm.FormatNum(UsageAmt, 0) + Environment.NewLine + Environment.NewLine +
                        "Budget amount is smaller than used amount."
                        );
                    return true;
                }

            }
            return false;
        }

        private bool IsBudgetRequestAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblBudgetRequestHdr Where DocNo=@Param And CancelInd='Y';",
                TxtBudgetRequestDocNo.Text,
                "Budget Request# : " + TxtBudgetRequestDocNo.Text + Environment.NewLine +
                "This budget request already cancelled."
                );
        }

        private bool IsBudgetRequestAlreadyProcessed()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblBudgetRequestHdr Where DocNo=@Param And BudgetDocNo is Not Null;",
                TxtBudgetRequestDocNo.Text,
                "Budget Request# : " + TxtBudgetRequestDocNo.Text + Environment.NewLine +
                "This budget request already processed."
                );
        }

        private MySqlCommand CancelPreviousBudget(string mPrevDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBudgetHdr Set ");
            SQL.AppendLine("    CancelInd = 'Y', CancelReason = @CancelReason, ");
            SQL.AppendLine("    LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", mPrevDocNo);
            Sm.CmParam<String>(ref cm, "@CancelReason", mFrmParent.MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBudgetHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudgetHdr(DocNo, DocDt, Status, BudgetRequestDocNo, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @BudgetRequestDocNo, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='Budget2' ");
                SQL.AppendLine("And T.DeptCode Is Not Null And T.DeptCode = (Select DeptCode From TblBudgetRequestHdr Where DocNo = @BudgetRequestDocNo) ");
                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select A.Amt*1 ");
                SQL.AppendLine("    From TblBudgetHdr A ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("), 0)); ");
            }

            SQL.AppendLine("Update TblBudgetHdr Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType = 'Budget2' ");
            SQL.AppendLine("    And DocNo = @DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblBudgetRequestHdr Set BudgetDocNo = @DocNo ");
            SQL.AppendLine("Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType = 'Budget2' ");
            SQL.AppendLine("    And DocNo = @DocNo ");
            SQL.AppendLine("); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", TxtBudgetRequestDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBudgetDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudgetDtl(DocNo, DNo, BudgetRequestDocNo, BudgetRequestDNo, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @BudgetRequestDocNo, @BudgetRequestDNo, @Amt, @Remark, @UserCode, CurrentDateTime()); ");

            if (!mFrmParent.mIsBudgetMaintenanceYearlyActived)
            {
                SQL.AppendLine("Insert Into TblBudgetSummary(Yr, Mth, DeptCode, BCCode, Amt1, Amt2, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@Yr, @Mth, @DeptCode, @BCCode, @Amt1, @Amt, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("    Update Amt1=@Amt1, Amt2=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", TxtBudgetRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@BudgetRequestDNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.CmParam<String>(ref cm, "@Yr", TxtYr.Text);
            Sm.CmParam<String>(ref cm, "@Mth", (mFrmParent.mIsBudget2YearlyFormat) ? "00" : TxtMth.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", mFrmParent.mDeptCode);
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Sm.GetGrdDec(Grd1, Row, 3));

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        private void ShowPrevData(string PrevDocNo, bool IsInsert)
        {
            Cursor.Current = Cursors.WaitCursor;
            ClearData();
            ShowBudgetHdr(PrevDocNo, IsInsert);
            ShowBudgetDtl(PrevDocNo);

            if (!IsInsert)
            {
                SetFormControl(mState.View);
            }
        }

        private void ShowBudgetHdr(string DocNo, bool IsInsert)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.BudgetRequestDocNo, B.Yr, B.Mth, ");
            SQL.AppendLine("B.DeptCode, C.DeptName, B.Amt As BudgetRequestAmt, A.Amt, A.Remark, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As Status, B.EntCode, ");
            if (mFrmParent.mIsBudget2Cancellable)
                SQL.AppendLine("A.CancelReason, A.CancelInd ");
            else
                SQL.AppendLine("Null As CancelReason, 'N' As CancelInd ");
            SQL.AppendLine("From TblBudgetHdr A ");
            SQL.AppendLine("Inner Join TblBudgetRequestHdr B On A.BudgetRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");


            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 
                    //1-5
                    "DocDt", "BudgetRequestDocNo", "Yr", "Mth", "DeptCode", 
                    //6-10
                    "DeptName", "BudgetRequestAmt", "Amt", "Remark","Status",

                    //11-13
                    "EntCode", "CancelReason", "CancelInd"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    if (!IsInsert) TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    if (IsInsert) Sm.SetDteCurrentDate(DteDocDt);
                    else Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtBudgetRequestDocNo.EditValue = Sm.DrStr(dr, c[2]);
                    TxtYr.EditValue = Sm.DrStr(dr, c[3]);
                    TxtMth.EditValue = Sm.DrStr(dr, c[4]);
                    TxtDeptCode.EditValue = Sm.DrStr(dr, c[6]);
                    TxtBudgetRequestAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                    TxtStatus.EditValue = Sm.DrStr(dr, c[10]);
                    Sl.SetLueEntCode(ref LueEntCode, Sm.DrStr(dr, c[11]));
                }, true
            );
        }

        private void ShowBudgetDtl(string DocNo)
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BudgetRequestDNo, B.BCCode, C.LocalCode, C.BCName, B.Amt As BudgetRequestAmt, A.Amt, A.Remark, IfNull(E.Amt, 0) Amt2, IfNull(F.Amt, 0.00) PrevAmt,  ");
            SQL.AppendLine("G.ItCode, G.ItName, B.Qty ");
            SQL.AppendLine("From TblBudgetDtl A ");
            SQL.AppendLine("Inner Join TblBudgetRequestDtl B On A.BudgetRequestDocNo=B.DocNo And A.BudgetRequestDNo=B.DNo ");
            SQL.AppendLine("Inner Join TblBudgetCategory C On B.BCCode=C.BCCode ");

            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select T.Dno, T.BcCode, SUM(T.Amt) As Amt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("    Select F.DNo, D.BCCode, Case when A.AcType = 'D' Then IFNULL(A.Amt*-1, 0.00) ELSE IFNULL(A.Amt, 0.00) END Amt ");
            SQL.AppendLine("    From tblvoucherhdr A ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT X1.DocNo, X1.VoucherRequestDocNo ");
            SQL.AppendLine("        From tblvoucherhdr X1 ");
            SQL.AppendLine("        INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
            SQL.AppendLine("        AND X1.DocType = '58' ");
            SQL.AppendLine("        AND X1.CancelInd = 'N' ");
            SQL.AppendLine("        AND X2.Status In ('O', 'A') ");
            SQL.AppendLine("     ) B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
            SQL.AppendLine("        From tblvoucherhdr X1 ");
            SQL.AppendLine("        Inner Join tblcashadvancesettlementdtl X2 ON X1.DocNo = X2.VoucherDocNo ");
            SQL.AppendLine("        INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
            SQL.AppendLine("        AND X1.DocType = '56' ");
            SQL.AppendLine("        AND X1.CancelInd = 'N' ");
            SQL.AppendLine("       AND X3.Status In ('O', 'A') ");
            SQL.AppendLine("    ) D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("    Inner Join TblBudgetRequestHdr E On E.DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("        And D.DeptCode = @DeptCode ");
            if (mFrmParent.mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
            else
                SQL.AppendLine("        And Left(A.DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("    Inner Join TblBudgetRequestDtl F On E.DocNo = F.DocNo ");
            SQL.AppendLine("        And F.BCCode = D.BCCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select E.DNo, C.BCCode, Sum( ");
            if (mFrmParent.mIsMRShowEstimatedPrice && mFrmParent.mIsBudgetCalculateFromEstimatedPrice)
                SQL.AppendLine("    B.Qty * B.EstPrice ");
            else
                SQL.AppendLine("    B.Qty * B.UPrice ");
            SQL.AppendLine("    ) Amt ");
            SQL.AppendLine("    From TblMaterialRequestHdr A ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        And B.`Status` In ('A', 'O') And B.CancelInd = 'N' ");
            if (mFrmParent.mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
            else
                SQL.AppendLine("        And Left(A.DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("    Inner Join TblBudgetCategory C On A.BCCode = C.BCCode ");
            SQL.AppendLine("    Inner Join TblBudgetRequestHdr D On D.DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("    Inner Join TblBudgetRequestDtl E On D.DocNo = E.DocNo And C.BCCode = E.BCCode ");
            SQL.AppendLine("    Group By E.DNo, C.BCCode ");

            if (mFrmParent.mIsCASUsedForBudget)
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select E.DNo, C.BCCode, Sum(B.Amt) Amt ");
                SQL.AppendLine("    From TblCashAdvanceSettlementHdr A ");
                SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        And A.`Status` = 'A' And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.DocStatus = 'F' ");
                SQL.AppendLine("        And B.CCtCode Is Not Null ");
                if (mFrmParent.mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
                else
                    SQL.AppendLine("        And Left(A.DocDt, 6) = Concat(@Yr, @Mth) ");
                SQL.AppendLine("    Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                SQL.AppendLine("        And C.CCtCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblBudgetRequestHdr D On D.DocNo = @BudgetRequestDocNo And D.DeptCode = C.DeptCode ");
                SQL.AppendLine("    Inner Join TblBudgetRequestDtl E On D.DocNo = E.DocNo And C.BCCode = E.BCCode ");
                SQL.AppendLine("    Group By E.DNo, C.BCCode ");
            }

            SQL.AppendLine("        )T ");
            SQL.AppendLine("Group By T.DNo, T.BCCode ");
            SQL.AppendLine(") E On  B.BCCode = E.BCCode ");

            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T3.BudgetRequestDocNo, T3.BudgetRequestDNo, T3.Amt ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Max(Concat(X3.DocDt, Left(X3.DocNo, 4))) DocNo ");
            SQL.AppendLine("        From TblBudgetDtl X1 ");
            SQL.AppendLine("        Inner Join TblBudgetRequestDtl X2 On X1.BudgetRequestDocNo = X2.DocNo And X1.BudgetRequestDNo = X2.DNo ");
            SQL.AppendLine("            And X1.DocNo != @DocNo ");
            SQL.AppendLine("            And X1.CreateDt <= (Select CreateDt From TblBudgetHdr Where DocNo = @DocNo) ");
            SQL.AppendLine("        Inner Join TblBudgetHdr X3 On X1.DocNo = X3.DocNo And X3.Status In ('O', 'A') ");
            if (mFrmParent.mIsBudget2Cancellable)
                SQL.AppendLine("            And X3.CancelInd = 'N' ");
            SQL.AppendLine("    ) T1 ");
            SQL.AppendLine("    Inner Join TblBudgetHdr T2 On T1.DocNo = Concat(T2.DocDt, Left(T2.DocNo, 4)) ");
            if (mFrmParent.mIsBudget2Cancellable)
                SQL.AppendLine("        And T2.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblBudgetDtl T3 On T2.DocNo = T3.DocNo ");
            SQL.AppendLine(") F On A.BudgetRequestDocNo=F.BudgetRequestDocNo And A.BudgetRequestDNo=F.BudgetRequestDNo ");
            SQL.AppendLine("Left Join TblItem G On B.ItCode = G.ItCode ");
            if (mFrmParent.mIsBudgetShowPreviousAmt)
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");
            else
            {
                SQL.AppendLine("Where B.DocNo=@BudgetRequestDocNo ");
                if (mFrmParent.mIsBudget2Cancellable)
                    SQL.AppendLine("And A.DocNo In (Select DocNo From TblBudgetHdr Where CancelInd = 'N') ");
                SQL.AppendLine("Order By A.DNo; ");
            }

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", TxtBudgetRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Yr", TxtYr.Text);
            Sm.CmParam<String>(ref cm, "@Mth", TxtMth.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", mFrmParent.mDeptCode);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "BudgetRequestDNo", 
                    "BCCode", "BCName", "BudgetRequestAmt", "Amt", "Remark",
                    "LocalCode", "Amt2", "PrevAmt", "ItCode", "ItName",
                    "Qty",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    if (mFrmParent.mIsBudgetShowPreviousAmt) Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 8);
                    else Grd.Cells[Row, 4].Value = 0m;

                    //Grd.Cells[Row, 6].Value = 0m;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6, 12 });
            Sm.FocusGrd(Grd1, 0, 2);
        }

        #endregion

        #region Additional Method

        private bool IsNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where Doctype = 'Budget2' And DeptCode Is Not Null Limit 1; ");
        }

        private void ComputeTotalAmount()
        {
            decimal Amt = 0m;
            decimal Amt2 = 0m;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd1, r, 3).Length != 0)
                    Amt += Sm.GetGrdDec(Grd1, r, 3);
            TxtBudgetRequestAmt.Text = Sm.FormatNum(Amt, 0);

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd1, r, 5).Length != 0)
                    Amt2 += Sm.GetGrdDec(Grd1, r, 5);
            TxtAmt.Text = Sm.FormatNum(Amt2, 0);
        }

        internal void ShowBudgetAmt()
        {
            if (BtnSave.Enabled && Grd1.Rows.Count > 1)
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    Sm.CopyGrdValue(Grd1, row, 5, Grd1, row, 3);
                }
                ComputeTotalAmount();
            }
        }

        internal void ShowBudgetRequestInfo()
        {
            TxtAmt.EditValue = Sm.FormatNum(0m, 0);
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.BCCode, B.BCName, A.Amt, B.LocalCode, IfNull(C.Amt2, 0) As Amt2, (IfNull(D.Amt, 0)+ IfNull(E.Amt, 0) ");
            if (mFrmParent.mIsCASUsedForBudget)
                SQL.AppendLine("+ IfNull(G.Amt, 0.00) ");
            SQL.AppendLine(") As Used, ");
            SQL.AppendLine("IfNull(A.Qty, 0) Qty, F.ItCode, F.ItName ");
            SQL.AppendLine("From TblBudgetRequestDtl A ");
            SQL.AppendLine("Inner Join TblBudgetCategory B On A.BCCode=B.BCCode ");
            SQL.AppendLine("Left Join TblBudgetSummary C On A.BCCode=C.BCCode And C.DeptCode=@DeptCode And C.Yr=@Yr ");
            if (!mFrmParent.mIsBudget2YearlyFormat)
                SQL.AppendLine("    And C.Mth=@Mth ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.BCCode, Sum(T.Amt) Amt ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select BCCode, Amt ");
            //SQL.AppendLine("        From TblMaterialRequestHdr ");
            //SQL.AppendLine("        Where Left(DocDt, 4)=@Yr ");
            //if (!mIsBudget2YearlyFormat)
            //    SQL.AppendLine("        And Substring(DocDt, 5, 2)=@Mth ");
            //SQL.AppendLine("        And DeptCode=@DeptCode ");
            SQL.AppendLine("    Select BCCode,   ");
            if (mFrmParent.mIsMRShowEstimatedPrice && mFrmParent.mIsBudgetCalculateFromEstimatedPrice)
                SQL.AppendLine("    (B.Qty * B.EstPrice) As Amt ");
            else
                SQL.AppendLine("    (B.Qty*B.UPrice) As Amt ");
            SQL.AppendLine("    From TblMaterialRequestHdr A  ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("    Where B.cancelind = 'N' And  Left(A.DocDt, 4)=@Yr  ");
            if (!mFrmParent.mIsBudget2YearlyFormat)
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2)=@Mth  ");
            SQL.AppendLine("        And A.DeptCode=@DeptCode ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select BCCode, Amt ");
            SQL.AppendLine("        From TblVoucherRequestHdr ");
            SQL.AppendLine("        Where ReqType Is Not null ");
            SQL.AppendLine("        And ReqType <> @ReqTypeForNonBudget ");
            SQL.AppendLine("        And CancelInd = 'N' ");
            SQL.AppendLine("        And Status In ('O', 'A') ");
            if (mFrmParent.mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(DocDt, 4) = Concat(@Yr) ");
            else
                SQL.AppendLine("        And Left(DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("        And Find_In_Set(DocType, ");
            SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");

            SQL.AppendLine("        And DeptCode = @DeptCode ");

            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select T1.BCCode, (T1.Amt1 + T1.Amt2 + T1.Amt3 + T1.Amt4 + T1.Amt5 + T1.Amt6 + T1.Detasering) Amt ");
            SQL.AppendLine("        From TblTravelRequestDtl7 T1 ");
            SQL.AppendLine("        Inner Join TblTravelRequestHdr T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And T2.CancelInd = 'N' ");
            SQL.AppendLine("            And T2.Status In ('O', 'A') ");
            if (mFrmParent.mIsBudget2YearlyFormat)
                SQL.AppendLine("            And Left(T2.DocDt, 4) = Concat(@Yr) ");
            else
                SQL.AppendLine("            And Left(T2.DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("            And T1.BCCode Is Not Null ");
            SQL.AppendLine("        Inner Join TblBudgetCategory T3 On T1.BCCode = T3.BCCode ");
            SQL.AppendLine("            And T3.DeptCode = @DeptCode ");

            SQL.AppendLine("    ) T Group By T.BCCode ");
            SQL.AppendLine(") D On B.BCCode=D.BCCode ");


            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select F.DNo, D.BCCode, Case when A.AcType = 'D' Then IFNULL(SUM(A.Amt)*-1, 0.00) ELSE IFNULL(SUM(A.Amt), 0.00) END Amt ");
            SQL.AppendLine("    From tblvoucherhdr A ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT X1.DocNo, X1.VoucherRequestDocNo ");
            SQL.AppendLine("        From tblvoucherhdr X1 ");
            SQL.AppendLine("        INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
            SQL.AppendLine("        AND X1.DocType = '58' ");
            SQL.AppendLine("        AND X1.CancelInd = 'N' ");
            SQL.AppendLine("        AND X2.Status In ('O', 'A') ");
            SQL.AppendLine("     ) B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
            SQL.AppendLine("        From tblvoucherhdr X1 ");
            SQL.AppendLine("        Inner Join tblcashadvancesettlementdtl X2 ON X1.DocNo = X2.VoucherDocNo ");
            SQL.AppendLine("        INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
            SQL.AppendLine("        AND X1.DocType = '56' ");
            SQL.AppendLine("        AND X1.CancelInd = 'N' ");
            SQL.AppendLine("       AND X3.Status In ('O', 'A') ");
            SQL.AppendLine("    ) D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("    Inner Join TblBudgetRequestHdr E On E.DocNo = @DocNo ");
            SQL.AppendLine("        And D.DeptCode = E.DeptCode ");
            if (mFrmParent.mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(A.DocDt, 4) = E.Yr ");
            else
                SQL.AppendLine("        And Left(A.DocDt, 6) = Concat(E.Yr, E.Mth) ");
            SQL.AppendLine("    Inner Join TblBudgetRequestDtl F On E.DocNo = F.DocNo ");
            SQL.AppendLine("        And F.BCCode = D.BCCode ");
            SQL.AppendLine("    Group By F.DNo, D.BCCode ");
            SQL.AppendLine(") E On A.DNo = E.DNo And A.BCCode = E.BCCode ");
            SQL.AppendLine("Left Join TblItem F On A.ItCode = F.ItCode ");
            if (mFrmParent.mIsCASUsedForBudget)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select E.DNo, C.BCCode, Sum(B.Amt) Amt ");
                SQL.AppendLine("    From TblCashAdvanceSettlementHdr A ");
                SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        And A.`Status` = 'A' And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.DocStatus = 'F' ");
                SQL.AppendLine("        And B.CCtCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                SQL.AppendLine("        And C.CCtCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblBudgetRequestHdr D On D.DocNo = @DocNo And D.DeptCode = C.DeptCode ");
                SQL.AppendLine("    Inner Join TblBudgetRequestDtl E On D.DocNo = E.DocNo And C.BCCode = E.BCCode ");
                SQL.AppendLine("    Group By E.DNo, C.BCCode ");
                SQL.AppendLine(") G On A.DNo = G.DNo And A.BCCode = G.BCCode ");
            }

            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", TxtBudgetRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Yr", TxtYr.Text);
            Sm.CmParam<String>(ref cm, "@Mth", TxtMth.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", mFrmParent.mDeptCode);
            Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mFrmParent.mReqTypeForNonBudget);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    
                    //1-5
                    "BCCode", "BCName", "Amt", "Amt2", "Used",

                    //6-9
                    "LocalCode", "ItCode", "ItName", "Qty"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Grd.Cells[Row, 7].Value = null;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6, 12 });
            Sm.FocusGrd(Grd1, 0, 2);
            ComputeTotalAmount();
        }

        private void RecomputeUsage()
        {
            var BCCode = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.BCCode, Sum(T.Amt) Amt ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            //SQL.AppendLine("    Select BCCode, Amt ");
            //SQL.AppendLine("    From TblMaterialRequestHdr ");
            //SQL.AppendLine("    Where Left(DocDt, 4)=@Yr ");
            //if (!mIsBudget2YearlyFormat)
            //    SQL.AppendLine("    And Substring(DocDt, 5, 2)=@Mth ");
            //SQL.AppendLine("    And DeptCode=@DeptCode ");

            SQL.AppendLine("    Select BCCode,   ");
            if (mFrmParent.mIsMRShowEstimatedPrice && mFrmParent.mIsBudgetCalculateFromEstimatedPrice)
                SQL.AppendLine("    (B.Qty * B.EstPrice) As Amt ");
            else
                SQL.AppendLine("    (B.Qty*B.UPrice) As Amt ");
            SQL.AppendLine("    From TblMaterialRequestHdr A  ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("    Where B.cancelind = 'N' And  Left(A.DocDt, 4)=@Yr  ");
            if (!mFrmParent.mIsBudget2YearlyFormat)
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2)=@Mth  ");
            SQL.AppendLine("    And DeptCode=@DeptCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select BCCode, Amt ");
            SQL.AppendLine("    From TblVoucherRequestHdr ");
            SQL.AppendLine("    Where ReqType Is Not null ");
            SQL.AppendLine("    And ReqType <> @ReqTypeForNonBudget ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    And Status In ('O', 'A') ");
            if (mFrmParent.mIsBudget2YearlyFormat)
                SQL.AppendLine("    And Left(DocDt, 4) = Concat(@Yr) ");
            else
                SQL.AppendLine("    And Left(DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("        And Find_In_Set(DocType, ");
            SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
            SQL.AppendLine("    And DeptCode = @DeptCode ");

            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select T1.BCCode, (T1.Amt1 + T1.Amt2 + T1.Amt3 + T1.Amt4 + T1.Amt5 + T1.Amt6 + T1.Detasering) Amt ");
            SQL.AppendLine("    From TblTravelRequestDtl7 T1 ");
            SQL.AppendLine("    Inner Join TblTravelRequestHdr T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd = 'N' ");
            SQL.AppendLine("        And T2.Status In ('O', 'A') ");
            if (mFrmParent.mIsBudget2YearlyFormat)
                SQL.AppendLine("        And Left(T2.DocDt, 4) = Concat(@Yr) ");
            else
                SQL.AppendLine("        And Left(T2.DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("        And T1.BCCode Is Not Null ");
            SQL.AppendLine("    Inner Join TblBudgetCategory T3 On T1.BCCode = T3.BCCode ");
            SQL.AppendLine("        And T3.DeptCode = @DeptCode ");

            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    SELECT D.BCCode AS BCCode,  Case when A.AcType = 'D' Then IFNULL(SUM(A.Amt)*-1, 0.00) ELSE IFNULL(SUM(A.Amt), 0.00) END As Amt ");
            SQL.AppendLine("    From tblvoucherhdr A  ");
            SQL.AppendLine("    Inner Join  ");
            SQL.AppendLine("      ( ");
            SQL.AppendLine("       SELECT X1.DocNo, X1.VoucherRequestDocNo   ");
            SQL.AppendLine("       From tblvoucherhdr X1  ");
            SQL.AppendLine("	    INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo  ");
            SQL.AppendLine("	    AND X1.DocType = '58' ");
            SQL.AppendLine("	    AND X1.CancelInd = 'N'  ");
            SQL.AppendLine("	    AND X2.Status In ('O', 'A')  ");
            SQL.AppendLine("      ) B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("      ( ");
            SQL.AppendLine("       SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
            SQL.AppendLine("       From tblvoucherhdr X1  ");
            SQL.AppendLine("       Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
            SQL.AppendLine("       INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
            SQL.AppendLine("       AND X1.DocType = '56' ");
            SQL.AppendLine("       AND X1.CancelInd = 'N'  ");
            SQL.AppendLine("       AND X3.Status In ('O', 'A')  ");
            SQL.AppendLine("      ) D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("    And D.DeptCode = @DeptCode  ");
            if (!mFrmParent.mIsBudget2YearlyFormat)
            {
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr  ");
                SQL.AppendLine("    And SUBSTRING(A.DocDt, 5, 2) = @Mth ");
            }
            else
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr  ");

            if (mFrmParent.mIsCASUsedForBudget)
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select C.BCCode, B.Amt ");
                SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                SQL.AppendLine("        Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            And A.`Status` = 'A' And A.CancelInd = 'N' ");
                SQL.AppendLine("            And A.DocStatus = 'F' ");
                SQL.AppendLine("            And B.CCtCode Is Not Null ");
                SQL.AppendLine("            And Left(A.DocDt, 4) = @Yr ");
                if (!mFrmParent.mIsBudget2YearlyFormat)
                    SQL.AppendLine("            And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("        Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                SQL.AppendLine("            And C.CCtCode Is Not Null ");
                SQL.AppendLine("            And C.DeptCode = @DeptCode ");
            }

            SQL.AppendLine(") T Group By T.BCCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand() { Connection = cn, CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@Yr", TxtYr.Text);
                Sm.CmParam<String>(ref cm, "@Mth", TxtMth.Text);
                Sm.CmParam<String>(ref cm, "@DeptCode", mFrmParent.mDeptCode);
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mFrmParent.mReqTypeForNonBudget);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BCCode", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        BCCode = Sm.DrStr(dr, 0);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), BCCode))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, r, 6, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnBudgetRequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtBudgetRequestDocNo, "Budget request#", false))
            {
                var f1 = new FrmBudgetRequest2(mFrmParent.mMenuCode);
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtBudgetRequestDocNo.Text;
                f1.ShowDialog();
            }
        }

        #endregion

        #region Grid Event Control

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #endregion
    }
}
