﻿#region Update
/*
    13/02/2018 [TKG] New application
    21/03/2018 [HAR] tambah input remark diheader 
    21/03/2018 [HAR] tambah message info jika asset kosong 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt4 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
            mCityCode = string.Empty, mCntCode = string.Empty;
        internal FrmDOCt4Find FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmDOCt4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion
 
        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "DO To Customer For Sold/Rented Item/Asset";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                //GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");

                TcDOCt4.SelectedTabPage = TpItem;
                Sl.SetLueUomCode(ref LuePackagingUnitUomCode);
                LuePackagingUnitUomCode.Visible = false;

                TcDOCt4.SelectedTabPage = TpGeneral;
                
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Quotation#",
                        "DNo",

                        //6-10
                        "",
                        "Start"+Environment.NewLine+"Date",
                        "End"+Environment.NewLine+"Date",
                        "Item/Asset"+Environment.NewLine+"Code",
                        "Item/Asset Name",
                        
                        //11-15
                        "Asset",
                        "Packaging"+Environment.NewLine+"Unit",
                        "Quantity"+Environment.NewLine+"(Packaging Unit)",
                        "Quantity",
                        "UoM",
                        
                        //16-17
                        "Remark",
                        "Journal#"
                    },
                     new int[] 
                    {
                        //0
                        0,
                        
                        //1-5
                        60, 0, 20, 130, 0, 
                        
                        //6-10
                        0, 100, 100, 100, 200, 
                        
                        //11-15
                        80, 0, 0, 100, 100, 

                        //16-17
                        200, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2, 11 });
            Sm.GrdColButton(Grd1, new int[] { 6 });
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 14 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 7, 8, 9, 10, 11, 15, 17 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5, 6, 7, 8, 11, 12, 13,  17 }, false);
            
            #endregion
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 11, 17 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueCtCode, MeeRemark }, true);
                    BtnCustomerShipAddress.Enabled = false;
                    BtnSAName.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 12, 13, 14, 16 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueCtCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 12, 13, 14, 16 });
                    BtnCustomerShipAddress.Enabled = true;
                    BtnSAName.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mCityCode = string.Empty;
            mCntCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueCtCode, TxtSAName, MeeSAAddress, 
                TxtCity, TxtCountry, TxtPostalCd, TxtPhone, TxtFax, 
                TxtEmail, TxtMobile, MeeRemark, TxtJournalDocNo
            });
            ClearGrd();
        }

        internal void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtSAName, MeeSAAddress, TxtCity, TxtCountry, TxtPostalCd, 
                TxtPhone, TxtFax, TxtEmail, TxtMobile
            });
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2, 11 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 13, 14 });
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOCt4Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TcDOCt4.SelectedTabPage = TpGeneral;
                SetLueCtCode(ref LueCtCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt4", "TblDOCt4Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDOCt4Hdr(DocNo));

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0) cml.Add(SaveDOCt4Dtl(DocNo, r));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid()||
                IsAssetDifferentLocation();
        }

        private bool IsAssetDifferentLocation()
        {
            int val = 0;
            string loc1 = string.Empty;
            string loc2 = string.Empty;
            string CtCode = Sm.GetLue(LueCtCode);

            loc1 = Sm.GetValue("Select B.AssetLocation from tblAsset A " +
                                       "inner Join tblCustomerAsset B On A.AssetCode = b.AssetCode " +
                                       "Where B.CtCode = " + CtCode + " And A.AssetCode = '"+Sm.GetGrdStr(Grd1, 0, 9)+"'  ");

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 11) == true)
                {
                    val += 1; 
                }
            }

            if (val > 0)
            {
                if (loc1.Length > 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 11) == true)
                        {
                            loc2 = Sm.GetValue("Select B.AssetLocation from tblAsset A " +
                                               "inner Join tblCustomerAsset B On A.AssetCode = b.AssetCode " +
                                               "Where B.CtCode = " + CtCode + " And A.AssetCode = '" + Sm.GetGrdStr(Grd1, Row, 9) + "' ");
                        }
                        if (loc1 != loc2)
                        {
                            if (Sm.StdMsgYN("Question", "Asset have different location, want to save? ") == DialogResult.No)
                                return true;
                            else
                                return false;
                        }
                    }
                }
                else
                {
                    if (Sm.StdMsgYN("Question", "Asset location is empty, want to save?") == DialogResult.No)
                        return true;
                    else
                        return false;
                }
            }
            return false;
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 quotation document.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO's quotation data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "quotation# is empty.")) return true;
                if (IsCtQt2AlreadyNotActived(Row)) return true;
                if (IsDocDtInvalid(Row)) return true;

                Msg =
                   "Item/Asset Code : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                   "Item/Asset Name : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdStr(Grd1, Row, 12).Length>0 && Sm.GetGrdDec(Grd1, Row, 13) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (Packaging unit) should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 14) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }
            }
            return false;
        }

        private bool IsCtQt2AlreadyNotActived(int r)
        {
            return Sm.IsDataExist(
                "Select 1 From TblCtQt2Hdr Where DocNo=@Param And ActInd='N';",
                Sm.GetGrdStr(Grd1, r, 4),
                "Quotation# : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                "This quotation already not actived.");
        }

        private bool IsDocDtInvalid(int r)
        {
            var cm = new MySqlCommand() 
            { 
                CommandText = 
                    "Select 1 From TblCtQt2Hdr " +
                    "Where DocNo=@DocNo And @DocDt Between QtStartDt And QtEndDt;"
            };
            Sm.CmParam<string>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 4));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));

            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Quotation# : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                    "Invalid DO date.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveDOCt4Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOCt4Hdr ");
            SQL.AppendLine("(DocNo, DocDt, CtCode, ");
            SQL.AppendLine("SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, SAPhone, SAFax, SAEmail, SAMobile, SARemark, ");
            SQL.AppendLine("JournalDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @CtCode, ");
            SQL.AppendLine("@SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCd, @SAPhone, @SAFax, @SAEmail, @SAMobile, @SARemark,");
            SQL.AppendLine("Null, @Remark, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@SAName", TxtSAName.Text);
            Sm.CmParam<String>(ref cm, "@SAAddress", MeeSAAddress.Text);
            Sm.CmParam<String>(ref cm, "@SACityCode", mCityCode);
            Sm.CmParam<String>(ref cm, "@SACntCode", mCntCode);
            Sm.CmParam<String>(ref cm, "@SAPostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@SAPhone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@SAFax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@SAEmail", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@SAMobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@SARemark", MeeRemark2.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDOCt4Dtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOCt4Dtl ");
            SQL.AppendLine("(DocNo, DNo, CancelInd, CtQt2DocNo, CtQt2DNo, ItCode, AssetInd, PackagingUnitUomCode, QtyPackagingUnit, Qty, PriceUomCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, 'N', @CtQt2DocNo, @CtQt2DNo, @ItCode, @AssetInd, @PackagingUnitUomCode, @QtyPackagingUnit, @Qty, @PriceUomCode, @Remark, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@CtQt2DocNo", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@CtQt2DNo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@AssetInd", Sm.GetGrdBool(Grd1, Row, 11)?"Y":"N");
            Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@PriceUomCode", Sm.GetGrdStr(Grd1, Row, 15));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 16));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelDOCt4Dtl());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = "Select DNo From TblDOCt4Dtl Where DocNo=@DocNo And CancelInd='Y' Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo" });
                var DNo = string.Empty;
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        DNo = Sm.DrStr(dr, 0);
                        for (int r= 0; r< Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(DNo, Sm.GetGrdStr(Grd1, r, 0)))
                            {
                                Grd1.Cells[r, 1].Value = true;
                                Grd1.Cells[r, 2].Value = true;
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid()
        {
            return
                IsCancelledItemValidationNotValid();
        }

        private bool IsCancelledItemValidationNotValid()
        {
            UpdateCancelledItem();

            bool IsProcessed = false;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    IsProcessed = true;
                    if (IsDOCt4AlreadyProcessed(r)) return true;
                }
            }
            if (!IsProcessed)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 quotation.");
                return true;
            }
            return false;
        }

        private bool IsDOCt4AlreadyProcessed(int r)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 From TblDOCt4Dtl " +
                    "Where DocNo=@DocNo And DNo=@DNo And SalesInvoice2DocNo Is Not Null;"
            };
            Sm.CmParam<string>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<string>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, r, 0));

            return Sm.IsDataExist(cm,
                "Quotation# : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                "Item/Asset Code : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                "Item/Asset Name : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine + Environment.NewLine +
                "This quotation already processed to invoice."
                );
        }


        private MySqlCommand CancelDOCt4Dtl()
        {
            var Filter = string.Empty;
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(DNo=@DNo0" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo0" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));                    
                }
            }

            if (Filter.Length > 0)
                Filter = " And ( " + Filter + " );";
            else
                Filter = " And 0=1;";

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCt4Dtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine(Filter);

            cm.CommandText = SQL.ToString();

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowDOCt4Hdr(DocNo);
                ShowDOCt4Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDOCt4Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CtCode, ");
            SQL.AppendLine("A.SAName, A.SAAddress, A.SACityCode, B.CityName, A.SACntCode, C.CntName, ");
            SQL.AppendLine("A.SAPostalCd, A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile, A.SARemark, A.Remark, A.JournalDocNo ");
            SQL.AppendLine("From TblDOCt4Hdr A  ");
            SQL.AppendLine("Left Join TblCity B On A.SACityCode=B.CityCode ");
            SQL.AppendLine("Left Join TblCountry C On A.SACntCode=C.CntCode ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "CtCode", "SAName",  "SAAddress", "SACityCode", 
                        //6-10
                        "CityName", "SACntCode", "CntName", "SAPostalCd", "SAPhone", 
                        //11-15
                        "SAFax", "SAEmail", "SAMobile", "SARemark", "Remark", 
                        //16
                        "JournalDocNo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TcDOCt4.SelectedTabPage = TpGeneral;
                        SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[2]));
                        TxtSAName.EditValue = Sm.DrStr(dr, c[3]);
                        MeeSAAddress.EditValue = Sm.DrStr(dr, c[4]);
                        mCityCode = Sm.DrStr(dr, c[5]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[6]);
                        mCntCode = Sm.DrStr(dr, c[7]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[8]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[9]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[10]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[11]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[12]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[13]);
                        MeeRemark2.EditValue = Sm.DrStr(dr, c[14]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[16]);
                    }, true
                );
        }

        private void ShowDOCt4Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.CtQt2DocNo, A.CtQt2DNo, D.QtStartDt, D.QtEndDt, ");
            SQL.AppendLine("A.ItCode, ");
            SQL.AppendLine("Case A.AssetInd When 'Y' Then C.AssetName Else B.ItName End As ItName, ");
            SQL.AppendLine("A.AssetInd, A.QtyPackagingUnit, A.PackagingUnitUomCode, A.Qty, A.PriceUomCode, ");
            SQL.AppendLine("A.Remark, A.JournalDocNo ");
            SQL.AppendLine("From TblDOCt4Dtl A ");
            SQL.AppendLine("Left Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblAsset C On A.ItCode=C.AssetCode ");
            SQL.AppendLine("Inner Join TblCtQt2Hdr D On A.CtQt2DocNo=D.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "CtQt2DocNo", "CtQt2DNo", "QtStartDt", "QtEndDt", 

                    //6-10
                    "ItCode", "ItName", "AssetInd", "PackagingUnitUomCode", "QtyPackagingUnit", 

                    //11-14
                    "Qty", "PriceUomCode", "Remark", "JournalDocNo"
                    
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                }, false, false, false, false
            );
            Grd1.Rows.Add();
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2, 11 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 13, 14 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void SetLueCtCode(ref DXE.LookUpEdit Lue, string CtCode)
        {
            try
            {
                var SQL = new StringBuilder();

                if (CtCode.Length == 0)
                {
                    SQL.AppendLine("Select CtCode As Col1, CtName As Col2 ");
                    SQL.AppendLine("From TblCustomer ");
                    SQL.AppendLine("Where CtCode In (Select CtCode From TblCtQt2Hdr Where ActInd='Y' And Status In ('O', 'A')) ");
                    SQL.AppendLine("Order By CtName; ");
                }
                else
                {
                    SQL.AppendLine("Select CtCode As Col1, CtName As Col2 ");
                    SQL.AppendLine("From TblCustomer Where CtCode=@CtCode;");
                }

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (CtCode.Length > 0) Sm.SetLue(Lue, CtCode);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
                Grd1.Font = new Font(Grd1.Font.FontFamily.Name.ToString(), int.Parse(Sm.GetLue(LueFontSize)));
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearGrd();
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(SetLueCtCode), string.Empty);
                ClearGrd();
            }
        }

        private void LuePackagingUnitUomCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePackagingUnitUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LuePackagingUnitUomCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LuePackagingUnitUomCode_Leave(object sender, EventArgs e)
        {
            if (LuePackagingUnitUomCode.Visible && fAccept && fCell.ColIndex == 12)
            {
                if (Sm.GetLue(LuePackagingUnitUomCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 12].Value = null;
                else
                    Grd1.Cells[fCell.RowIndex, 12].Value = Sm.GetLue(LuePackagingUnitUomCode);
            }
        }

        private void LuePackagingUnitUomCode_Validated(object sender, EventArgs e)
        {
            LuePackagingUnitUomCode.Visible = false;
        }

        #endregion

        #region Button Event

        private void BtnCustomerShipAddress_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmDOCt4Dlg2(this, Sm.GetLue(LueCtCode)));
        }

        private void BtnSAName_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueCtCode, "Customer")) return;
            try
            {
                var MenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param='FrmCustomer';");
                var f = new FrmCustomer(MenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mCtCode = Sm.GetLue(LueCtCode);
                f.ShowDialog();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmCtQt3("x");
                f.Tag = "x";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")
                            && !Sm.IsLueEmpty(LueCtCode, "Customer"))
                            Sm.FormShowDialog(new FrmDOCt4Dlg(this, Sm.GetLue(LueCtCode), Sm.GetDte(DteDocDt)));
                    }
                    if (e.ColIndex == 12) LueRequestEdit(Grd1, LuePackagingUnitUomCode, ref fCell, ref fAccept, e);
                    if (TxtDocNo.Text.Length == 0 &&
                        Sm.IsGrdColSelected(new int[] { 3, 12, 13, 14, 16 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3
               && TxtDocNo.Text.Length == 0
               && !Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmDOCt4Dlg(this, Sm.GetLue(LueCtCode), Sm.GetDte(DteDocDt)));

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmCtQt3("x");
                f.Tag = "x";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 28 }, e);
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 14, 19, 24 }, e);

            if (e.ColIndex == 12 || e.ColIndex == 13)
            {
                int r = e.RowIndex;
                if (!Sm.GetGrdBool(Grd1, r, 11))
                {
                    Grd1.Cells[r, 14].Value = 0m;
                    ComputeQty(r);
                }
            }
        }

        internal void ComputeQty(int r)
        {
            decimal QtyPackagingUnit = 0m, Qty = 0m, Qty2 = 0m;
            string
                PriceUomCode = string.Empty,
                PackagingUnitUomCode = string.Empty,
                ItCode = string.Empty,
                SalesUomCode = string.Empty;

            try
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 9);
                PackagingUnitUomCode = Sm.GetGrdStr(Grd1, r, 12);
                QtyPackagingUnit = Sm.GetGrdDec(Grd1, r, 13);
                PriceUomCode = Sm.GetGrdStr(Grd1, r, 15);
                
                if (PackagingUnitUomCode.Length>0)
                {
                    Qty = GetItemPackagingUnitQty("Qty", ItCode, PackagingUnitUomCode);
                    Qty2 = GetItemPackagingUnitQty("Qty2", ItCode, PackagingUnitUomCode);
                    SalesUomCode = Sm.GetValue("Select SalesUomCode From TblItem Where ItCode=@Param;", ItCode);

                    if (Sm.CompareStr(SalesUomCode, PriceUomCode))
                        Grd1.Cells[r, 14].Value = QtyPackagingUnit * Qty;
                    else
                        Grd1.Cells[r, 14].Value = QtyPackagingUnit * Qty2;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private decimal GetItemPackagingUnitQty(string Qty, string ItCode, string UomCode)
        { 
            var cm = new MySqlCommand(){CommandText = "Select " + Qty + " From TblItemPackagingUnit Where ItCode=@ItCode And UomCode=@UomCode;"};
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.CmParam<String>(ref cm, "@UomCode", UomCode);
            return Sm.GetValueDec(cm);
        }

        #endregion

        #endregion
    }
}
