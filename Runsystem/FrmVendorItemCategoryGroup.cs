﻿#region Update
/*
    28/12/2017 [ARI] tambah printout 
    01/02/2018 [ARI] tambah printout kim 
    02/03/2018 [HAR] item terhadap vendor item category yang sudah expired dan document date lbh dari document date transaksi masih muncul 
    19/03/2018 [HAR] feedback : item vendor category yang sudah di letter of offer kan, harusnya tidak muncul lagi
    18/07/2018 [HAR] BUG : validasi item berdasarkan exipred date
    31/10/2019 [DITA/IMS] tambah informasi Specifications
    28/11/2019 [WED/IMS] tambah kolom project name dan project code, berdasarkan parameter IsBOMShowSpecifications
    11/12/2019 [WED/IMS] tambah lup berdasarkan parameter IsVendorItemCategoryGroupCouldChooseItem
    13/12/2019 [WED/IMS] printout
    06/01/2020 [WED/IMS] qty bisa diedit, berdasarkan parameter IsVendorItemCategoryGroupCouldEditQty
    23/04/2020 [IBL/IMS] Menampilkan item berdasarkan parameter IsLOVShowingAllItem
    23/04/2020 [IBL/IMS] Tambah kolom remark
    24/04/2020 [TKG/IMS] ubah query untuk menampilkan informasi project dan Customer's PO#
    18/09/2020 [DITA/IMS] perubahan design printout LOV
    21/09/2020 [WED/IMS] parameter tanda tangan VendorItemCategorySignatureSource. jabatan penandatangan di parameter IsVendorItemCategorySignaturePositionUseDefault. IsVendorItemCategoryPrintOutUseRemarkDoc
    22/09/2020 [DITA/IMS] feedback printout LOV
    05/10/2020 [ICA/IMS] feedback printout LOV
    23/02/2021 [WED/IMS] LOV mandatory di remark nya berdasarkan parameter IsRemarkLOVMandatory
    31/03/2021 [VIN/IMS] project dari MR
    07/06/2021 [VIN/IMS] item yg sudah PO tidak muncul 
    10/06/2021 [VIN/IMS] tambah order by itname 
    11/06/2021 [VIN/IMS] bug item tidak muncul
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVendorItemCategoryGroup : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmVendorItemCategoryGroupFind FrmFind;
        internal string mVdCode = string.Empty, mDocNo = string.Empty,
            mEmpCodeVdItemCategory = string.Empty;
        internal bool 
            mIsBOMShowSpecifications = false, 
            mIsVendorItemCategoryGroupCouldChooseItem = false,
            mIsVendorItemCategoryGroupCouldEditQty = false,
            mIsLOVShowingAllItem = false
            ;
        private string mVendorItemCategorySignatureSource = string.Empty,
            mVendorItemCategorySignatureDefaultPosName = string.Empty;
        private bool mIsVendorItemCategorySignaturePositionUseDefault = false,
            mIsVendorItemCategoryPrintOutUseRemarkDoc = false,
            mIsRemarkLOVMandatory = false
            ;

        #endregion

        #region Constructor

        public FrmVendorItemCategoryGroup(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                if (mIsRemarkLOVMandatory) LblRemark.ForeColor = Color.Red;

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "DNo",
                    //1-5
                    "Item's Code",
                    "Local Code",
                    "Item's Name",
                    "Foreign Name",
                    "Vendor Collection#",

                    //6-10
                    "Vendor Item Colection Dno",
                    "Vendor Code Colection Dno",
                    "Specification",
                    "Project's Code",
                    "Project's Name",

                    //11-14
                    "",
                    "Quantity",
                    "UoM",
                    "Remark"
                },
                new int[] 
                {
                    80, 
                    120, 120, 250, 200, 180, 
                    80, 80, 300, 100, 200,
                    20, 100, 100, 150
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 13 });
            if (!mIsVendorItemCategoryGroupCouldEditQty) Sm.GrdColReadOnly(Grd1, new int[] { 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 12 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 7 });
            Sm.GrdColButton(Grd1, new int[] { 11 });
            Grd1.Cols[11].Move(0);
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10 });
            if (!mIsVendorItemCategoryGroupCouldChooseItem) Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 13 });
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo, DteDocDt, TxtVdName, MeeCancelReason, MeeRemark }, true);
                    BtnVdCode.Enabled = false;
                    TxtDocNo.Focus();
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeRemark }, false);
                    BtnVdCode.Enabled = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    if (mIsVendorItemCategoryGroupCouldChooseItem) Grd1.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    BtnVdCode.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = false;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mVdCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtVdName, MeeCancelReason, MeeRemark
            });
            Sm.ClearGrd(Grd1, mIsVendorItemCategoryGroupCouldChooseItem);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, !ChkHideInfoInGrd.Checked);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVendorItemCategoryGroupFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.FormShowDialog(new FrmVendorItemCategoryGroupDlg(this));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData();
                }
                else
                {
                    CancelData();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            ParPrint(TxtDocNo.Text);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VendorItemCategoryGroup", "TblVendorItCtGroupHdr");

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(SaveVendorItCtGroupHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveVendorItCtGroupDtl(DocNo, Row));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtVdName, "Vendor", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                (mIsRemarkLOVMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark"))
                ;
        }

        private bool IsGrdValueNotValid()
        {
            if (mIsVendorItemCategoryGroupCouldEditQty)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    {
                        if (Sm.IsGrdValueEmpty(Grd1, i, 12, true, "Quantity should not be zero.")) return true;
                    }
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Item was empty.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveVendorItCtGroupHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVendorItCtGroupHdr " +
                    "(DocNo, DocDt, VdCode, CancelInd, CancelReason, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @VdCode, 'N', @CancelReason, @Remark, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveVendorItCtGroupDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVendorItCtGroupDtl");
            SQL.AppendLine("(DocNo, DNo, ItemVendorCollectionDocNo, ItemVendorCollectionDNo, ItemVendorCollectionDNo2, Qty, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ItemVendorCollectionDocNo, @ItemVendorCollectionDNo, @ItemVendorCollectionDNo2, @Qty, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItemVendorCollectionDocNo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@ItemVendorCollectionDNo", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@ItemVendorCollectionDNo2", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelVendorItCtGroup());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation");
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblVendorItCtGroupHdr " +
                    "Where CancelInd='Y'  And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelVendorItCtGroup()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVendorItCtGroupHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowItemCategoryGroupHdr(DocNo);
                ShowItemCategoryGroupDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowItemCategoryGroupHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.VdCode, B.VdName, A.CancelInd, A.CancelReason, A.Remark " +
                    "From TblVendorItCtGroupHdr A, TblVendor B " +
                    "Where A.VdCode=B.VdCode And A.DocNo=@DocNo;",
                    new string[]
                    { 
                        "DocNo", 
                        "DocDt", "VdCode", "VdName", "CancelReason", "CancelInd", 
                        "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        mVdCode = Sm.DrStr(dr, c[2]);
                        TxtVdName.EditValue = Sm.DrStr(dr, c[3]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[4]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[5]), "Y") ? true : false;
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        private void ShowItemCategoryGroupDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select B.Dno, B.Remark, C.ItCode, E.ItCodeInternal, E.itName, E.ForeignName, ");
                SQL.AppendLine("B.ItemVendorCollectionDocNo, B.itemVendorCollectionDNo, E.Specification,  B.itemVendorCollectionDNo2, ");
                SQL.AppendLine("M.ProjectCode, M.ProjectName, I.PONo, ");
                SQL.AppendLine("If(B.Qty>0.00, B.Qty, G.Qty) As Qty, E.InventoryUomCode Uom ");
                SQL.AppendLine("From TblVendorItCtGroupHdr A ");
                SQL.AppendLine("Inner Join TblVendorItCtGroupDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("Inner Join TblItemVendorCollectionDtl C On B.ItemVendorCollectionDocNo=C.DocNo And B.ItemVendorCollectionDNo=C.DNo ");
                SQL.AppendLine("Inner Join TblItemVendorCollectionDtl2 D ");
                SQL.AppendLine("    On B.ItemVendorCollectionDocNo=D.DocNo  ");
                SQL.AppendLine("    And B.ItemVendorCollectionDNo=D.DNo  ");
                SQL.AppendLine("    And B.ItemVendorCollectionDNo2=D.DNo2 ");
                SQL.AppendLine("Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl G On C.MRDocNo=G.DocNo And C.MRDNo=G.DNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestHdr H On G.DocNo=H.DocNo ");
                SQL.AppendLine("Left Join TblSOContractHdr I On H.SOCDocNo=I.DocNo ");
                SQL.AppendLine("Left Join TblBOQHdr J On I.BoqDocNo=J.DocNo ");
                SQL.AppendLine("Left Join TblLOPHdr K On J.LopDocNo=K.DocNo ");
                SQL.AppendLine("Left Join TblProjectGroup L on K.PGCode=L.PGCode ");
                SQL.AppendLine("Left Join TblProjectGroup M on H.PGCode=M.PGCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo;");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo", 
                        //1-5
                        "ItCode", "ItCodeInternal", "ItName", "ForeignName", "itemVendorCollectionDocNo",  
                        //6-10
                        "ItemVendorCollectionDNo", "ItemVendorCollectionDNo2", "Specification", "ProjectCode", "ProjectName",
                        //11-13
                        "Qty", "Uom", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                    }, false, false, true, false
                );
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowDataItemDtl()
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Dno, C.Dno2, A.DocDt, C.VdCode, B.ItCode, D.ItName, E.ItCtname, D.ItCodeInternal, D.Foreignname, D.Specification, ");
            SQL.AppendLine("L.ProjectCode, L.ProjectName, I.PONo, G.Qty, D.InventoryUomCode Uom ");
            SQL.AppendLine("From TblItemVendorCollectionhdr A ");
            SQL.AppendLine("Inner Join TblItemVendorCollectionDtl B On A.Docno = B.Docno ");
            if (mIsLOVShowingAllItem)
            {
                if (Sm.StdMsgYN("Question", "Do you want to show item based on outstanding request ?") == DialogResult.Yes)
                {
                    SQL.AppendLine("And B.ItCode In ( ");
                    SQL.AppendLine("    Select Distinct ItCode From (");
                    SQL.AppendLine("        Select X2.ItCode, X2.Qty-IfNull(X3.Qty, 0)-IfNull(X4.Qty, 0) As Qty ");
                    SQL.AppendLine("        From TblMaterialRequestHdr X1 ");
                    SQL.AppendLine("        Inner Join TblMaterialRequestDtl X2 ");
                    SQL.AppendLine("            On X1.DocNo=X2.DocNo ");
                    SQL.AppendLine("            And IfNull(X2.Status,'X')='A' ");
                    SQL.AppendLine("            And X2.CancelInd='N' ");
                    SQL.AppendLine("            And IfNull(X2.ProcessInd, '')<>'F' ");
                    SQL.AppendLine("            And X2.TenderDocNo Is Null ");
                    SQL.AppendLine("        Left Join ( ");
                    SQL.AppendLine("            Select T1.MRDocNo As DocNo, T1.MRDNo As DNo, Sum(T1.Qty) Qty ");
                    SQL.AppendLine("            From TblMRQtyCancel T1 ");
                    SQL.AppendLine("            Inner Join TblMaterialRequestDtl T2 ");
                    SQL.AppendLine("                On T1.MRDocNo=T2.DocNo ");
                    SQL.AppendLine("                And T1.MRDNo=T2.DNo ");
                    SQL.AppendLine("                And IfNull(T2.ProcessInd, '')<>'F' ");
                    SQL.AppendLine("                And T2.TenderDocNo Is Null ");
                    SQL.AppendLine("            Where T1.CancelInd = 'N' ");
                    SQL.AppendLine("            Group By T1.MRDocNo, T1.MRDNo ");
                    SQL.AppendLine("        ) X3 On X2.DocNo=X3.DocNo And X2.DNo=X3.DNo ");
                    SQL.AppendLine("        Left Join ( ");
                    SQL.AppendLine("            Select MaterialRequestDocNo As DocNo, T1.MaterialRequestDNo As DNo, Sum(T1.Qty) As Qty ");
                    SQL.AppendLine("            From TblPORequestDtl T1 ");
                    SQL.AppendLine("            Inner Join TblMaterialRequestDtl T2 ");
                    SQL.AppendLine("                On T1.MaterialRequestDocNo=T2.DocNo ");
                    SQL.AppendLine("                And T1.MaterialRequestDNo=T2.DNo ");
                    SQL.AppendLine("                And IfNull(T2.ProcessInd, '')<>'F' ");
                    SQL.AppendLine("                And T2.TenderDocNo Is Null ");
                    SQL.AppendLine("                And T2.CancelInd='N' ");
                    SQL.AppendLine("            Where T1.CancelInd='N' And T1.Status<>'C' ");
                    SQL.AppendLine("            Group By T1.MaterialRequestDocNo, T1.MaterialRequestDNo ");
                    SQL.AppendLine("        ) X4 On X2.DocNo=X4.DocNo And X2.DNo=X4.DNo ");
                    SQL.AppendLine("    ) X Where Qty>0.00 ");
                    SQL.AppendLine(") ");
                }
            }
            SQL.AppendLine("Inner Join TblItemVendorCollectionDtl2 C on B.Docno = C.DocNo And B.Dno = C.Dno  ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode = D.itCode ");
            SQL.AppendLine("Inner Join TblItemCategory E On D.ItCtCode = E.itCtCode ");
            SQL.AppendLine("Inner Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select B.ItCode, C.VdCode, ");
            SQL.AppendLine("    Max(Concat(A.DocDt, A.DocNo, B.DNo, C.Dno2)) As Key1   ");
            SQL.AppendLine("    From TblItemVendorCollectionhdr A ");
            SQL.AppendLine("    Inner Join TblItemVendorCollectionDtl B On A.Docno = B.Docno And B.Cancelind = 'N' ");
            SQL.AppendLine("    Inner Join TblItemVendorCollectionDtl2 C on B.Docno = C.DocNo And B.Dno = C.Dno And C.VdCode = @VdCode ");
            SQL.AppendLine("    Where A.DocDt <= @DocDt And ((A.ExpiredDt is not null and expiredDt>=@DocDt) Or A.ExpiredDt is null)   ");
            SQL.AppendLine("    Group By B.ItCode, C.VdCode ");
            SQL.AppendLine(")F On Concat(A.DocDt, B.DocNo, B.DNo, C.Dno2)=F.Key1 And C.VdCode=F.VdCode And B.ItCode=F.ItCode  ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl G On B.MRDocNo = G.DocNo And B.MRDNo = G.DNo ");

            SQL.AppendLine("Inner Join TblMaterialRequestHdr H On G.DocNo=H.DocNo ");
            SQL.AppendLine("Left Join TblSOContractHdr I On H.SOCDocNo=I.DocNo ");
            SQL.AppendLine("Left Join TblBOQHdr J On I.BoqDocNo=J.DocNo ");
            SQL.AppendLine("Left Join TblLOPHdr K On J.LopDocNo=K.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup L on K.PGCode=L.PGCode ");
            SQL.AppendLine("Where C.VdCode=@VdCode ");
            SQL.AppendLine("And Concat(C.VdCode, C.DocNo, C.Dno, C.Dno2) not in ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Concat(A.VdCode, B.itemVendorCollectionDocNo, B.itemVendorCollectionDno, B.ItemVendorCollectionDno2) As KeyWord ");
            SQL.AppendLine("    From tblvendoritctgroupHdr A ");
            SQL.AppendLine("    Inner Join TblVendorItCtGroupDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Where A.cancelInd = 'N' ");
            SQL.AppendLine(")  ");
            SQL.AppendLine("AND CONCAT(B.ItCode, G.DocNo, G.DNo) NOT IN  "); 
            SQL.AppendLine("(  ");
	        SQL.AppendLine("    SELECT CONCAT(E.ItCode, D.DocNo, D.DNo) FROM TblPOHdr A ");
	        SQL.AppendLine("    Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo   ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");  
	        SQL.AppendLine("    Inner Join TblItem E On D.ItCode=E.ItCode  ");
	        SQL.AppendLine("    WHERE A.VdCode=@VdCode ");
            SQL.AppendLine(") ");
            SQL.AppendLine(" Order By D.ItName;");


            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "ItCode", 
                    "ItCodeInternal", "ItName", "ForeignName", "DocNo", "Dno",  
                    "Dno2", "Specification", "ProjectCode", "ProjectName", "PONo",
                    "Qty", "Uom"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 12);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Grid Methods

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 11)
                {
                    Sm.FormShowDialog(new FrmVendorItemCategoryGroupDlg2(this));
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && mIsVendorItemCategoryGroupCouldChooseItem)
            {
                if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdEnter(Grd1, e);
                Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #region Additional Method

        private void ParPrint(string DocNo)
        {
            string[] TableName = { "VendorICHdr", "VendorICDtl", "VendorICHdr2", "Employee" };

            var l = new List<VendorICHdr>();
            var ldtl = new List<VendorICDtl>();
            var l2 = new List<VendorICHdr2>();
            var l3 = new List<Employee>();
            string mDocTitle = Sm.GetParameter("DocTitle");

            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyCity, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyPhone, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle5')As CompanyFax, ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.VdCode, B.VdName, B.Phone, B.Fax, B.Address, A.CancelInd, ");
            SQL.AppendLine("C.ContactPersonName, B.Email, D.MRDocNo, D.ProjectName, A.Remark, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode = 'VendorItemCategoryGroupNotes') VendorItemCategoryGroupNotes, ");
            SQL.AppendLine("E.EmpName, E.PosName ");
            SQL.AppendLine("From tblvendoritctgrouphdr A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, T2.ContactPersonName ");
            SQL.AppendLine("    From TblVendorItCtGroupHdr T1 ");
            SQL.AppendLine("    Inner Join TblVendorContactPerson T2 On T1.VdCode = T2.VdCode ");
            SQL.AppendLine("        And T1.DocNo = @DocNo ");
            SQL.AppendLine("        And T2.DNo = '001' ");
            SQL.AppendLine(") C On A.DocNo = C.DocNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, Group_Concat(Distinct T3.DocNo) MRDocNo, Group_Concat(Distinct IfNull(T7.ProjectName, T6.ProjectName)) ProjectName ");
            SQL.AppendLine("    From TblVendorItCtGroupDtl T1 ");
            SQL.AppendLine("    Inner Join TblItemVendorCollectionDtl T2 On T1.ItemVendorCollectionDocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.ItemVendorCollectionDNo = T2.DNo ");
            SQL.AppendLine("        And T1.DocNo = @DocNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr T3 On T2.MRDocNo = T3.DocNo ");
            SQL.AppendLine("    Left Join TblSOContractHdr T4 On T3.SOCDocNo = T4.DocNo ");
            SQL.AppendLine("    Left Join TblBOQHdr T5 On T4.BOQDocNo = T5.DocNo ");
            SQL.AppendLine("    Left Join TblLOPHdr T6 On T5.LOPDocNo = T6.DocNo ");
            SQL.AppendLine("    Left Join TblProjectGroup T7 On T6.PGCode = T7.PGCode ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") D On A.DocNo = D.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpName, T3.PosName ");
            SQL.AppendLine("    From TblParameter T1 ");
            SQL.AppendLine("    Inner Join TblEmployee T2 On T1.ParValue = T2.EmpCode ");
            SQL.AppendLine("        And T1.ParCode = 'VendorItemCategoryGroupAutoApprovedByEmpCode' ");
            SQL.AppendLine("    Left Join TblPosition T3 On T2.PosCode = T3.PosCode ");
            SQL.AppendLine(") E On 0 = 0 ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",
                        
                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyCity",
                         "CompanyPhone",
                         "DocNo", 
 
                         //6-10
                         "DocDt",
                         "VdCode",
                         "VdName",
                         "Phone", 
                         "Fax",

                         //11-15
                         "Address", 
                         "CancelInd",
                         "CompanyFax",
                         "Remark",
                         "ContactPersonName",

                         //16-20
                         "Email",
                         "MRDocNo",
                         "ProjectName",
                         "VendorItemCategoryGroupNotes",
                         "EmpName",

                         //21
                         "PosName"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new VendorICHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),

                            DocDt = Sm.DrStr(dr, c[6]),
                            VdCode = Sm.DrStr(dr, c[7]),
                            VdName = Sm.DrStr(dr, c[8]),
                            Phone = Sm.DrStr(dr, c[9]),
                            Fax = Sm.DrStr(dr, c[10]),

                            Address = Sm.DrStr(dr, c[11]),
                            CancelInd = Sm.DrStr(dr, c[12]),
                            CompanyFax = Sm.DrStr(dr, c[13]),
                            Remark = mIsVendorItemCategoryPrintOutUseRemarkDoc ? (Sm.DrStr(dr, c[14]).Length == 0 ? "-" : Sm.DrStr(dr, c[14])) : Sm.DrStr(dr, c[14]),
                            ContactPersonName = Sm.DrStr(dr, c[15]),

                            Email = Sm.DrStr(dr, c[16]),
                            MRDocNo = Sm.DrStr(dr, c[17]),
                            ProjectName = Sm.DrStr(dr, c[18]),
                            VendorItemCategoryGroupNotes = Sm.DrStr(dr, c[19]),
                            EmpName = mVendorItemCategorySignatureSource == "1" ? Sm.DrStr(dr, c[20]) : "",
                            PosName = mIsVendorItemCategorySignaturePositionUseDefault ? mVendorItemCategorySignatureDefaultPosName : Sm.DrStr(dr, c[21]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.DocNo, C.ItCode, E.itName, E.ForeignName, E.InventoryUomCode2, Date_Format(F.UsageDt,'%d/%b/%Y')As UsageDt, If(B.Qty > 0, B.Qty, F.Qty) Qty, F.Remark, B.Remark RemarkVIC, E.PurchaseUomCode, ");
                SQLDtl.AppendLine("E.ItCodeInternal, E.Specification ");
                SQLDtl.AppendLine("From tblvendoritctgrouphdr A ");
                SQLDtl.AppendLine("Inner Join TblVendorItCtGroupDtl B On A.DocNo = B.DocNo ");
                SQLDtl.AppendLine("Inner Join tblitemvendorcollectiondtl C On B.itemVendorCollectionDocNo = C.DocNo And B.itemVendorCollectionDNo = C.DNo ");
                SQLDtl.AppendLine("Inner Join tblitemvendorcollectiondtl2 D On B.itemVendorCollectionDocNo = D.DocNo ");
                SQLDtl.AppendLine("And B.itemVendorCollectionDNo = D.DNo ");
                SQLDtl.AppendLine("And B.itemVendorCollectionDNo2 = D.DNo2 ");
                SQLDtl.AppendLine("Inner Join tblItem E On C.itCode = E.ItCode ");
                SQLDtl.AppendLine("Inner Join tblMaterialRequestDtl F On C.MRDocNo=F.DocNo And C.MRDNo=F.DNo ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo; ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "DocNo",

                     //1-5
                     "ItCode",
                     "ItName",
                     "ForeignName",
                     "InventoryUomCode2",
                     "UsageDt",

                     //6-10
                     "Qty",
                     "Remark",
                     "PurchaseUomCode",
                     "ItCodeInternal",
                     "Specification",

                     //11
                     "RemarkVIC"

                    });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new VendorICDtl()
                        {
                            nomor = nomor,
                            DocNo = Sm.DrStr(drDtl, cDtl[0]),
                            ItCode = Sm.DrStr(drDtl, cDtl[1]),
                            ItName = Sm.DrStr(drDtl, cDtl[2]),
                            ForeignName = Sm.DrStr(drDtl, cDtl[3]),
                            Uom = Sm.DrStr(drDtl, cDtl[4]),
                            UsageDt = Sm.DrStr(drDtl, cDtl[5]),

                            Qty = Sm.DrDec(drDtl, cDtl[6]),
                            Remark = Sm.DrStr(drDtl, cDtl[7]),
                            PurchaseUomCode = Sm.DrStr(drDtl, cDtl[8]),
                            ItCodeInternal = Sm.DrStr(drDtl, cDtl[9]),
                            Specification = Sm.DrStr(drDtl, cDtl[10]),
                            RemarkVIC = Sm.DrStr(drDtl, cDtl[11]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Header2

            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL2.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL2.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyCity, ");
            SQL2.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyPhone, ");
            SQL2.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle5')As CompanyFax, ");
            SQL2.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.VdCode, B.VdName, B.Phone, B.Fax, B.Address, A.CancelInd, ");
            SQL2.AppendLine("Group_Concat(Distinct E.DocNo Order By E.DocNo Separator ', ') As DocNoMR, ");
            SQL2.AppendLine("Group_Concat(Distinct Date_Format(F.DocDt, '%d %M %Y') Order By F.DocDt Separator ', ') As DocDtMR, ");
            SQL2.AppendLine("Date_Format(A.DocDt + interval 7 day,'%d %M %Y') As DocDt2 ");
            SQL2.AppendLine("From tblvendoritctgrouphdr A ");
            SQL2.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
            SQL2.AppendLine("Inner Join TblVendorItCtGroupDtl C On A.DocNo = C.DocNo ");
            SQL2.AppendLine("Inner Join tblitemvendorcollectiondtl D On C.itemVendorCollectionDocNo = D.DocNo And C.itemVendorCollectionDNo = D.DNo ");
            SQL2.AppendLine("Inner Join tblMaterialRequestDtl E On D.MRDocNo=E.DocNo And D.MRDNo=E.DNo ");
            SQL2.AppendLine("Inner Join tblMaterialRequestHdr F On E.DocNo=F.DocNo ");
            SQL2.AppendLine("Where A.DocNo=@DocNo ");
            SQL2.AppendLine("Group By A.DocNo ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "CompanyLogo",
                        
                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyCity",
                         "CompanyPhone",
                         "DocNo", 
 
                         //6-10
                         "DocDt",
                         "VdCode",
                         "VdName",
                         "Phone", 
                         "Fax",

                         //11-15
                         "Address", 
                         "CancelInd",
                         "CompanyFax",
                         "DocNoMR",
                         "DocDtMR",

                         "DocDt2"

                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new VendorICHdr2()
                        {
                            CompanyLogo = Sm.DrStr(dr2, c2[0]),

                            CompanyName = Sm.DrStr(dr2, c2[1]),
                            CompanyAddress = Sm.DrStr(dr2, c2[2]),
                            CompanyCity = Sm.DrStr(dr2, c2[3]),
                            CompanyPhone = Sm.DrStr(dr2, c2[4]),
                            DocNo = Sm.DrStr(dr2, c2[5]),

                            DocDt = Sm.DrStr(dr2, c2[6]),
                            VdCode = Sm.DrStr(dr2, c2[7]),
                            VdName = Sm.DrStr(dr2, c2[8]),
                            Phone = Sm.DrStr(dr2, c2[9]),
                            Fax = Sm.DrStr(dr2, c2[10]),

                            Address = Sm.DrStr(dr2, c2[11]),
                            CancelInd = Sm.DrStr(dr2, c2[12]),
                            CompanyFax = Sm.DrStr(dr2, c2[13]),
                            DocNoMR = Sm.DrStr(dr2, c2[14]),
                            DocDtMR = Sm.DrStr(dr2, c2[15]),
                            DocDt2 = Sm.DrStr(dr2, c2[16]),
                        });
                    }
                }
                dr2.Close();
            }

            myLists.Add(l2);
            #endregion

            #region Signature KIM
            var cm3 = new MySqlCommand();
            var SQL3 = new StringBuilder();

            SQL3.AppendLine("Select A.EmpCode, A.EmpName, B.PosName From TblEmployee A ");
            SQL3.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
            SQL3.AppendLine("Where A.EmpCode=@EmpCode ");

            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cm3.Connection = cn3;
                cm3.CommandText = SQL3.ToString();
                Sm.CmParam<String>(ref cm3, "@EmpCode", mEmpCodeVdItemCategory);
                var dr3 = cm3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[] 
                        {
                         //0-2
                         "EmpCode",
                         "EmpName",
                         "PosName",
                        
                        });
                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        l3.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr3, c3[0]),

                            EmpName = Sm.DrStr(dr3, c3[1]),
                            Position = Sm.DrStr(dr3, c3[2]),
                        });
                    }
                }
                dr3.Close();
            }
            myLists.Add(l3);

            #endregion

            if (mDocTitle=="KIM")
                Sm.PrintReport(string.Concat("VendorItemCategory2"), myLists, TableName, false);
            else if (mDocTitle == "IMS")
                Sm.PrintReport(string.Concat("VendorItemCategoryIMS"), myLists, TableName, false);
            else
                Sm.PrintReport(string.Concat("VendorItemCategory"), myLists, TableName, false);
        }

        private void GetParameter()
        {
            mEmpCodeVdItemCategory = Sm.GetParameter("EmpCodeVdItemCategory");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsVendorItemCategoryGroupCouldChooseItem = Sm.GetParameterBoo("IsVendorItemCategoryGroupCouldChooseItem");
            mIsVendorItemCategoryGroupCouldEditQty = Sm.GetParameterBoo("IsVendorItemCategoryGroupCouldEditQty");
            mIsLOVShowingAllItem = Sm.GetParameterBoo("IsLOVShowingAllItem");
            mVendorItemCategorySignatureSource = Sm.GetParameter("VendorItemCategorySignatureSource");
            mIsVendorItemCategorySignaturePositionUseDefault = Sm.GetParameterBoo("IsVendorItemCategorySignaturePositionUseDefault");
            mVendorItemCategorySignatureDefaultPosName = Sm.GetParameter("VendorItemCategorySignatureDefaultPosName");
            mIsVendorItemCategoryPrintOutUseRemarkDoc = Sm.GetParameterBoo("IsVendorItemCategoryPrintOutUseRemarkDoc");
            mIsRemarkLOVMandatory = Sm.GetParameterBoo("IsRemarkLOVMandatory");

            if (mVendorItemCategorySignatureSource.Length == 0) mVendorItemCategorySignatureSource = "1";
            if (mVendorItemCategorySignatureDefaultPosName.Length == 0) mVendorItemCategorySignatureDefaultPosName = "KADEP PENGADAAN";
        }

        internal string GetSelectedItemVendorCollection()
        {
            string SQL = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if(Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (SQL.Length > 0) SQL += ",";
                    SQL += string.Concat(Sm.GetGrdStr(Grd1, i, 5), Sm.GetGrdStr(Grd1, i, 6), Sm.GetGrdStr(Grd1, i, 7));
                }
            }

            return SQL.Length > 0 ? SQL : "XXX";
        }

        #endregion
      
        #endregion

        #region Event
        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void BtnVdCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVendorItemCategoryGroupDlg(this));
        }

        #endregion

        #region Class

        private class VendorICHdr
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyCity { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdCode { get; set; }
            public string VdName { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Address { get; set; }
            public string CancelInd { get; set; }
            public string PrintBy { get; set; }
            public string Remark { get; set; }
            public string ContactPersonName { get; set; }
            public string Email { get; set; }
            public string MRDocNo { get; set; }
            public string ProjectName { get; set; }
            public string VendorItemCategoryGroupNotes { get; set; }
            public string EmpName { get; set; }
            public string PosName { get; set; }
        }

        private class VendorICHdr2
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyCity { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdCode { get; set; }
            public string VdName { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Address { get; set; }
            public string CancelInd { get; set; }
            public string DocNoMR { get; set; }
            public string DocDtMR { get; set; }
            public string DocDt2 { get; set; }
        }

        private class VendorICDtl
        {
            public int nomor { get; set; }
            public string DocNo { get; set; }
            public string ItCode { get; set; }
            public string ItCodeInternal { get; set; }
            public string Specification { get; set; }
            public string ItName { get; set; }
            public string ForeignName { get; set; }
            public string Uom { get; set; }
            public string UsageDt { get; set; }
            public decimal Qty { get; set; }
            public string Remark { get; set; }
            public string RemarkVIC { get; set; }
            public string PurchaseUomCode { get; set; }
        }

        private class Employee
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
        }

        #endregion         

    }
}
