﻿namespace RunSystem
{
    partial class FrmDocApprovalSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtDocType = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtDNo = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtLevel = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtUserCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtStartAmt = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.LuePaymentType = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.LueDAGCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtLeaveCode = new DevExpress.XtraEditors.TextEdit();
            this.LblLeaveCode = new System.Windows.Forms.Label();
            this.TxtEndAmt = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.LblLevelCode = new System.Windows.Forms.Label();
            this.TxtLevelCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtItCtCode = new DevExpress.XtraEditors.TextEdit();
            this.LblItCtCode = new System.Windows.Forms.Label();
            this.TxtItCtName = new DevExpress.XtraEditors.TextEdit();
            this.BtnItCtCode = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStartAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDAGCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLeaveCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEndAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevelCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCtName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(764, 0);
            this.panel1.Size = new System.Drawing.Size(70, 311);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Location = new System.Drawing.Point(0, 160);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.BtnCancel.Size = new System.Drawing.Size(70, 32);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Location = new System.Drawing.Point(0, 128);
            this.BtnSave.Margin = new System.Windows.Forms.Padding(2);
            this.BtnSave.Size = new System.Drawing.Size(70, 32);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Location = new System.Drawing.Point(0, 96);
            this.BtnDelete.Margin = new System.Windows.Forms.Padding(2);
            this.BtnDelete.Size = new System.Drawing.Size(70, 32);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Location = new System.Drawing.Point(0, 64);
            this.BtnEdit.Margin = new System.Windows.Forms.Padding(2);
            this.BtnEdit.Size = new System.Drawing.Size(70, 32);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Location = new System.Drawing.Point(0, 32);
            this.BtnInsert.Margin = new System.Windows.Forms.Padding(2);
            this.BtnInsert.Size = new System.Drawing.Size(70, 32);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Margin = new System.Windows.Forms.Padding(2);
            this.BtnFind.Size = new System.Drawing.Size(70, 32);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Location = new System.Drawing.Point(0, 192);
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(2);
            this.BtnPrint.Size = new System.Drawing.Size(70, 32);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnItCtCode);
            this.panel2.Controls.Add(this.TxtItCtName);
            this.panel2.Controls.Add(this.TxtItCtCode);
            this.panel2.Controls.Add(this.LblItCtCode);
            this.panel2.Controls.Add(this.TxtLevelCode);
            this.panel2.Controls.Add(this.LblLevelCode);
            this.panel2.Controls.Add(this.TxtEndAmt);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.TxtLeaveCode);
            this.panel2.Controls.Add(this.LblLeaveCode);
            this.panel2.Controls.Add(this.LueDAGCode);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LueWhsCode);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.LueSiteCode);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.LuePaymentType);
            this.panel2.Controls.Add(this.TxtStartAmt);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtUserCode);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.LueDeptCode);
            this.panel2.Controls.Add(this.TxtLevel);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtDNo);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocType);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(764, 311);
            // 
            // TxtDocType
            // 
            this.TxtDocType.EnterMoveNextControl = true;
            this.TxtDocType.Location = new System.Drawing.Point(165, 9);
            this.TxtDocType.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocType.Name = "TxtDocType";
            this.TxtDocType.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocType.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocType.Properties.Appearance.Options.UseFont = true;
            this.TxtDocType.Properties.MaxLength = 80;
            this.TxtDocType.Size = new System.Drawing.Size(329, 20);
            this.TxtDocType.TabIndex = 10;
            this.TxtDocType.Validated += new System.EventHandler(this.TxtDocType_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(65, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Document Type";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDNo
            // 
            this.TxtDNo.EnterMoveNextControl = true;
            this.TxtDNo.Location = new System.Drawing.Point(165, 30);
            this.TxtDNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDNo.Name = "TxtDNo";
            this.TxtDNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDNo.Properties.MaxLength = 8;
            this.TxtDNo.Size = new System.Drawing.Size(66, 20);
            this.TxtDNo.TabIndex = 12;
            this.TxtDNo.Validated += new System.EventHandler(this.TxtDNo_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(111, 33);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Number";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(98, 54);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 14);
            this.label4.TabIndex = 13;
            this.label4.Text = "User Code";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel
            // 
            this.TxtLevel.EnterMoveNextControl = true;
            this.TxtLevel.Location = new System.Drawing.Point(165, 72);
            this.TxtLevel.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel.Name = "TxtLevel";
            this.TxtLevel.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLevel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLevel.Size = new System.Drawing.Size(66, 20);
            this.TxtLevel.TabIndex = 16;
            this.TxtLevel.EditValueChanged += new System.EventHandler(this.TxtLevel_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(74, 75);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "Setting\'s Level";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(81, 96);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 14);
            this.label5.TabIndex = 17;
            this.label5.Text = "Departement";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(165, 93);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 25;
            this.LueDeptCode.Properties.MaxLength = 16;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 500;
            this.LueDeptCode.Size = new System.Drawing.Size(330, 20);
            this.LueDeptCode.TabIndex = 18;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // TxtUserCode
            // 
            this.TxtUserCode.EnterMoveNextControl = true;
            this.TxtUserCode.Location = new System.Drawing.Point(165, 51);
            this.TxtUserCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUserCode.Name = "TxtUserCode";
            this.TxtUserCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUserCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUserCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUserCode.Properties.Appearance.Options.UseFont = true;
            this.TxtUserCode.Properties.MaxLength = 255;
            this.TxtUserCode.Size = new System.Drawing.Size(329, 20);
            this.TxtUserCode.TabIndex = 14;
            this.TxtUserCode.Validated += new System.EventHandler(this.TxtUserCode_Validated);
            // 
            // TxtStartAmt
            // 
            this.TxtStartAmt.EnterMoveNextControl = true;
            this.TxtStartAmt.Location = new System.Drawing.Point(165, 156);
            this.TxtStartAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStartAmt.Name = "TxtStartAmt";
            this.TxtStartAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtStartAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStartAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStartAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtStartAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtStartAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtStartAmt.Size = new System.Drawing.Size(146, 20);
            this.TxtStartAmt.TabIndex = 24;
            this.TxtStartAmt.Validated += new System.EventHandler(this.TxtStartAmt_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(79, 159);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 14);
            this.label6.TabIndex = 23;
            this.label6.Text = "Start Amount";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(74, 201);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(87, 14);
            this.label17.TabIndex = 27;
            this.label17.Text = "Payment Type";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePaymentType
            // 
            this.LuePaymentType.EnterMoveNextControl = true;
            this.LuePaymentType.Location = new System.Drawing.Point(165, 198);
            this.LuePaymentType.Margin = new System.Windows.Forms.Padding(5);
            this.LuePaymentType.Name = "LuePaymentType";
            this.LuePaymentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.Appearance.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePaymentType.Properties.DropDownRows = 25;
            this.LuePaymentType.Properties.NullText = "[Empty]";
            this.LuePaymentType.Properties.PopupWidth = 250;
            this.LuePaymentType.Size = new System.Drawing.Size(330, 20);
            this.LuePaymentType.TabIndex = 28;
            this.LuePaymentType.ToolTip = "F4 : Show/hide list";
            this.LuePaymentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePaymentType.EditValueChanged += new System.EventHandler(this.LuePaymentType_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(133, 117);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 14);
            this.label7.TabIndex = 19;
            this.label7.Text = "Site";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(165, 114);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 25;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 500;
            this.LueSiteCode.Size = new System.Drawing.Size(330, 20);
            this.LueSiteCode.TabIndex = 20;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(92, 138);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 14);
            this.label8.TabIndex = 21;
            this.label8.Text = "Warehouse";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(165, 135);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 25;
            this.LueWhsCode.Properties.MaxLength = 16;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 500;
            this.LueWhsCode.Size = new System.Drawing.Size(330, 20);
            this.LueWhsCode.TabIndex = 22;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(9, 221);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(152, 14);
            this.label9.TabIndex = 29;
            this.label9.Text = "Document Approval Group";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDAGCode
            // 
            this.LueDAGCode.EnterMoveNextControl = true;
            this.LueDAGCode.Location = new System.Drawing.Point(165, 219);
            this.LueDAGCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDAGCode.Name = "LueDAGCode";
            this.LueDAGCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDAGCode.Properties.Appearance.Options.UseFont = true;
            this.LueDAGCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDAGCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDAGCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDAGCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDAGCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDAGCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDAGCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDAGCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDAGCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDAGCode.Properties.DropDownRows = 25;
            this.LueDAGCode.Properties.NullText = "[Empty]";
            this.LueDAGCode.Properties.PopupWidth = 250;
            this.LueDAGCode.Size = new System.Drawing.Size(330, 20);
            this.LueDAGCode.TabIndex = 30;
            this.LueDAGCode.ToolTip = "F4 : Show/hide list";
            this.LueDAGCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDAGCode.EditValueChanged += new System.EventHandler(this.LueDAGCode_EditValueChanged);
            // 
            // TxtLeaveCode
            // 
            this.TxtLeaveCode.EnterMoveNextControl = true;
            this.TxtLeaveCode.Location = new System.Drawing.Point(165, 261);
            this.TxtLeaveCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLeaveCode.Name = "TxtLeaveCode";
            this.TxtLeaveCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLeaveCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLeaveCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLeaveCode.Properties.Appearance.Options.UseFont = true;
            this.TxtLeaveCode.Properties.MaxLength = 255;
            this.TxtLeaveCode.Size = new System.Drawing.Size(330, 20);
            this.TxtLeaveCode.TabIndex = 32;
            // 
            // LblLeaveCode
            // 
            this.LblLeaveCode.AutoSize = true;
            this.LblLeaveCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLeaveCode.ForeColor = System.Drawing.Color.Black;
            this.LblLeaveCode.Location = new System.Drawing.Point(90, 264);
            this.LblLeaveCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblLeaveCode.Name = "LblLeaveCode";
            this.LblLeaveCode.Size = new System.Drawing.Size(71, 14);
            this.LblLeaveCode.TabIndex = 31;
            this.LblLeaveCode.Text = "Leave Code";
            this.LblLeaveCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEndAmt
            // 
            this.TxtEndAmt.EnterMoveNextControl = true;
            this.TxtEndAmt.Location = new System.Drawing.Point(165, 177);
            this.TxtEndAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEndAmt.Name = "TxtEndAmt";
            this.TxtEndAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEndAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEndAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEndAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtEndAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEndAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEndAmt.Size = new System.Drawing.Size(146, 20);
            this.TxtEndAmt.TabIndex = 26;
            this.TxtEndAmt.Validated += new System.EventHandler(this.TxtEndAmt_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(85, 180);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 14);
            this.label10.TabIndex = 25;
            this.label10.Text = "End Amount";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblLevelCode
            // 
            this.LblLevelCode.AutoSize = true;
            this.LblLevelCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLevelCode.ForeColor = System.Drawing.Color.Black;
            this.LblLevelCode.Location = new System.Drawing.Point(61, 243);
            this.LblLevelCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblLevelCode.Name = "LblLevelCode";
            this.LblLevelCode.Size = new System.Drawing.Size(100, 14);
            this.LblLevelCode.TabIndex = 31;
            this.LblLevelCode.Text = "Employee\'s Level";
            this.LblLevelCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevelCode
            // 
            this.TxtLevelCode.EnterMoveNextControl = true;
            this.TxtLevelCode.Location = new System.Drawing.Point(165, 240);
            this.TxtLevelCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevelCode.Name = "TxtLevelCode";
            this.TxtLevelCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevelCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevelCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevelCode.Properties.Appearance.Options.UseFont = true;
            this.TxtLevelCode.Properties.MaxLength = 255;
            this.TxtLevelCode.Size = new System.Drawing.Size(330, 20);
            this.TxtLevelCode.TabIndex = 32;
            this.TxtLevelCode.Validated += new System.EventHandler(this.TxtLevelCode_Validated);
            // 
            // TxtItCtCode
            // 
            this.TxtItCtCode.EnterMoveNextControl = true;
            this.TxtItCtCode.Location = new System.Drawing.Point(165, 283);
            this.TxtItCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCtCode.Name = "TxtItCtCode";
            this.TxtItCtCode.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtItCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCtCode.Properties.MaxLength = 255;
            this.TxtItCtCode.Properties.ReadOnly = true;
            this.TxtItCtCode.Size = new System.Drawing.Size(97, 20);
            this.TxtItCtCode.TabIndex = 34;
            // 
            // LblItCtCode
            // 
            this.LblItCtCode.AutoSize = true;
            this.LblItCtCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblItCtCode.ForeColor = System.Drawing.Color.Black;
            this.LblItCtCode.Location = new System.Drawing.Point(67, 286);
            this.LblItCtCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblItCtCode.Name = "LblItCtCode";
            this.LblItCtCode.Size = new System.Drawing.Size(94, 14);
            this.LblItCtCode.TabIndex = 33;
            this.LblItCtCode.Text = "Item\'s Category";
            this.LblItCtCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItCtName
            // 
            this.TxtItCtName.EnterMoveNextControl = true;
            this.TxtItCtName.Location = new System.Drawing.Point(267, 283);
            this.TxtItCtName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCtName.Name = "TxtItCtName";
            this.TxtItCtName.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtItCtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCtName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCtName.Properties.Appearance.Options.UseFont = true;
            this.TxtItCtName.Properties.MaxLength = 255;
            this.TxtItCtName.Properties.ReadOnly = true;
            this.TxtItCtName.Size = new System.Drawing.Size(228, 20);
            this.TxtItCtName.TabIndex = 35;
            // 
            // BtnItCtCode
            // 
            this.BtnItCtCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnItCtCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnItCtCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnItCtCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnItCtCode.Appearance.Options.UseBackColor = true;
            this.BtnItCtCode.Appearance.Options.UseFont = true;
            this.BtnItCtCode.Appearance.Options.UseForeColor = true;
            this.BtnItCtCode.Appearance.Options.UseTextOptions = true;
            this.BtnItCtCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnItCtCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnItCtCode.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnItCtCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnItCtCode.Location = new System.Drawing.Point(498, 281);
            this.BtnItCtCode.Name = "BtnItCtCode";
            this.BtnItCtCode.Size = new System.Drawing.Size(27, 20);
            this.BtnItCtCode.TabIndex = 36;
            this.BtnItCtCode.ToolTip = "Show Voucher Request Information";
            this.BtnItCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnItCtCode.ToolTipTitle = "Run System";
            this.BtnItCtCode.Click += new System.EventHandler(this.BtnItCtCode_Click);
            // 
            // FrmDocApprovalSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 311);
            this.Name = "FrmDocApprovalSetting";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStartAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDAGCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLeaveCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEndAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevelCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCtName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtDocType;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtDNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private DevExpress.XtraEditors.TextEdit TxtLevel;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtUserCode;
        private DevExpress.XtraEditors.TextEdit TxtStartAmt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.LookUpEdit LuePaymentType;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        private DevExpress.XtraEditors.LookUpEdit LueDAGCode;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtLeaveCode;
        private System.Windows.Forms.Label LblLeaveCode;
        private DevExpress.XtraEditors.TextEdit TxtEndAmt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label LblLevelCode;
        internal DevExpress.XtraEditors.TextEdit TxtLevelCode;
        internal DevExpress.XtraEditors.TextEdit TxtItCtName;
        internal DevExpress.XtraEditors.TextEdit TxtItCtCode;
        private System.Windows.Forms.Label LblItCtCode;
        public DevExpress.XtraEditors.SimpleButton BtnItCtCode;
    }
}