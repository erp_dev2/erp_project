﻿#region Update
/*
 *  29/06/2022 [ICA/PHT] New Apps
 * */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptGradeSalaryAdvancement : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptGradeSalaryAdvancement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueDeptCode(ref LueDeptCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Tbl.* ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    SELECT A.DNo, A.EmpCode, B.EmpName, 'Grade Advancement' AdvancementType, CONCAT(GrdLvlIncreaseYr, GrdLvlIncreaseMth, '21') DocDt, ");
            SQL.AppendLine("    C.DocDt DocDtPPS, C.DocNo PPSDocNo, C.StartDt, G.OptDesc JobTransfer, D.GrdLvlName GrdLvlOld, E.GrdLvlName GrdLvlNew, C.GrdLvlCodeNew, ");
            SQL.AppendLine("    F.LevelName, B.JoinDt, B.LeaveStartDt, CONCAT(Convert(C.WorkPeriodYr, INT), '|', Right(CONCAT('00', Convert(C.WorkPeriodMth, INT)), 2)) Duration, K.SectionName, ");
            SQL.AppendLine("    H.PosName, J.DeptName, I.SiteName, B.DeptCode, B.SiteCode ");
            SQL.AppendLine("    FROM TblPeriodicGradeAdvancement A ");
            SQL.AppendLine("    INNER JOIN TblEmployee B ON A.EmpCode = B.EmpCode ");
            SQL.AppendLine("    LEFT JOIN TblPPS C ON A.PPSDocNo = C.DocNo AND C.CancelInd = 'N' ");
            SQL.AppendLine("    LEFT JOIN TblGradeLevelHdr D ON C.GrdLvlCodeOld = D.GrdLvlCode ");
            SQL.AppendLine("    LEFT JOIN TblGradeLevelHdr E ON C.GrdLvlCodeNew = E.GrdLvlCode ");
            SQL.AppendLine("    LEFT JOIN TblLevelHdr F ON C.LevelCodeNew = F.LevelCode ");
            SQL.AppendLine("    LEFT JOIN TblOption G ON G.OptCat = 'EmpJobTransfer' AND G.OptCode = C.JobTransfer ");
            SQL.AppendLine("    LEFT JOIN TblPosition H ON B.PosCode = H.PosCode ");
            SQL.AppendLine("    LEFT JOIN TblSite I ON B.SiteCode = I.SiteCode ");
            SQL.AppendLine("    LEFT JOIN TblDepartment J ON B.DeptCode = J.DeptCode ");
            SQL.AppendLine("    LEFT JOIN TblSection K ON B.SectionCode = K.SectionCode ");
            SQL.AppendLine("    Where CONCAT(GrdLvlIncreaseYr, GrdLvlIncreaseMth, '21') Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    SELECT A.DNo, A.EmpCode, B.EmpName, 'Salary Advancement' AdvancementType, CONCAT(SalaryIncreaseYr, SalaryIncreaseMth, '21') DocDt, ");
            SQL.AppendLine("    C.DocDt DocDtPPS, C.DocNo PPSDocNo, C.StartDt, G.OptDesc JobTransfer, D.GrdLvlName GrdLvlOld, E.GrdLvlName GrdLvlNew, C.GrdLvlCodeNew, ");
            SQL.AppendLine("    F.LevelName, B.JoinDt, B.LeaveStartDt, CONCAT(Convert(C.WorkPeriodYr, INT), '|', Right(CONCAT('00', Convert(C.WorkPeriodMth, INT)), 2)) Duration, K.SectionName, ");
            SQL.AppendLine("    H.PosName, J.DeptName, I.SiteName, B.DeptCode, B.SiteCode ");
            SQL.AppendLine("    FROM TblPeriodicSalaryAdvancement A ");
            SQL.AppendLine("    INNER JOIN TblEmployee B ON A.EmpCode = B.EmpCode ");
            SQL.AppendLine("    LEFT JOIN TblPPS C ON A.PPSDocNo = C.DocNo AND C.CancelInd = 'N' ");
            SQL.AppendLine("    LEFT JOIN TblGradeLevelHdr D ON C.GrdLvlCodeOld = D.GrdLvlCode ");
            SQL.AppendLine("    LEFT JOIN TblGradeLevelHdr E ON C.GrdLvlCodeNew = E.GrdLvlCode ");
            SQL.AppendLine("    LEFT JOIN TblLevelHdr F ON C.LevelCodeNew = F.LevelCode ");
            SQL.AppendLine("    LEFT JOIN TblOption G ON G.OptCat = 'EmpJobTransfer' AND G.OptCode = C.JobTransfer ");
            SQL.AppendLine("    LEFT JOIN TblPosition H ON B.PosCode = H.PosCode ");
            SQL.AppendLine("    LEFT JOIN TblSite I ON B.SiteCode = I.SiteCode ");
            SQL.AppendLine("    LEFT JOIN TblDepartment J ON B.DeptCode = J.DeptCode ");
            SQL.AppendLine("    LEFT JOIN TblSection K ON B.SectionCode = K.SectionCode ");
            SQL.AppendLine("    Where CONCAT(SalaryIncreaseYr, SalaryIncreaseMth, '21') Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(") Tbl ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee Code",
                        "Employee Name",
                        "Advancement Type",
                        "Advancement Date",
                        "Date",

                        //6-10
                        "Document",
                        "",
                        "Start Date",
                        "Job Transfer",
                        "Grade Old",
                        
                        //11-15
                        "Grade New",
                        "Level",
                        "Join Date",
                        "Permanent Date",
                        "Duration",
                        
                        //16-19
                        "Section",
                        "Position",
                        "Department",
                        "Site",
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        120, 200, 150, 120, 120, 
                        
                        //6-10
                        150, 20, 120, 150, 100, 

                        //11-15
                        100, 100, 120, 120, 120, 
                        
                        //16-19
                        150, 200, 200, 200
                    }
                ) ;
            Sm.GrdColButton(Grd1, new int[] { 7 });
            Sm.GrdFormatDate(Grd1, new int[] { 4, 5, 8, 13, 14 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "Tbl.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "Tbl.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By Tbl.EmpName;",
                        new string[]
                        {

                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName", "AdvancementType", "DocDt", "DocDtPPS", "PPSDocNo", 
                            
                            //6-10
                            "StartDt", "JobTransfer", "GrdLvlOld", "GrdLvlNew", "LevelName", 
                            
                            //11-15
                            "JoinDt", "LeaveStartDt", "Duration", "SectionName", "PosName",

                            //16-17
                            "DeptName", "SiteName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPPS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmPPS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #endregion
    }
}
