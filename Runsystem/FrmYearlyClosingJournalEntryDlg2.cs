﻿#region Update
/*
    01/02/2023 [IBL/BBT] Menu baru
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmYearlyClosingJournalEntryDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmYearlyClosingJournalEntry mFrmParent;
        private string mSQL = string.Empty;
        #endregion

        #region Constructor

        public FrmYearlyClosingJournalEntryDlg2(FrmYearlyClosingJournalEntry FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "List of Yearly Closing Journal Entry";
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "Date",
                    "Cancel",
                    "Approval"+Environment.NewLine+"Status",
                    "Year",
                        
                    //6-9                     
                    "Current Earning Acc No",
                    "Current Earning Acc Desc.",
                    "Current Earning Amount",
                    "Remark",
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    150, 80, 80, 100, 80,
                        
                    //6-9
                    200, 200, 150, 200,

                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("	Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("	Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As Status, ");
            SQL.AppendLine("	A.Year, A.CurrentEarningAcNo, B.AcDesc, IfNull(A.CurrentEarningAmt, 0.00) As Amt, A.Remark ");
            SQL.AppendLine("	From TblYearlyClosingJournalEntryHdr A ");
            SQL.AppendLine("	Inner Join TblCOA B On A.CurrentEarningAcNo = B.AcNo ");
            SQL.AppendLine("	Where A.CancelInd = 'Y' Or A.Status = 'C' ");
            SQL.AppendLine("	Union All ");
            SQL.AppendLine("	Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("	Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As Status, ");
            SQL.AppendLine("	A.Year, A.CurrentEarningAcNo, B.AcDesc, IfNull(A.CurrentEarningAmt, 0.00) As Amt, A.Remark ");
            SQL.AppendLine("	From TblYearlyClosingJournalEntryHdr A ");
            SQL.AppendLine("	Inner Join TblCOA B On A.CurrentEarningAcNo = B.AcNo ");
            SQL.AppendLine("	Where A.CancelInd = 'N' And A.Status <> 'C' ");
            SQL.AppendLine("	And A.Year = @Yr-1 ");
            SQL.AppendLine(")T ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "T.DocNo"});
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "T.DocDt");
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(mFrmParent.LueYr));
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T.DocNo;",
                        new string[]
                       {
                            "DocNo",

                            "DocDt",
                            "CancelInd",
                            "Status",
                            "Year",
                            "CurrentEarningAcNo",

                            "AcDesc",
                            "Amt",
                            "Remark"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowCopyData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        #endregion

        #region Grid Event
        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion
    }
}
