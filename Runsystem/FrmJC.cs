﻿#region Update
/*
    20/03/2018 [TKG] tambah start date, end date, process group
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmJC : RunSystem.FrmBase3
    {
        #region Field, Property

        internal bool mIsProductionOrderUseProcessGroup = false;
        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mMainCurCode= string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        internal FrmJCFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmJC(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Job Costing";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Sl.SetLueCtCode(ref LueCtCode, "");
                Sl.SetLueCurCode(ref LueCurCode);
                SetLuePGCode(ref LuePGCode);
                SetGrd();
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtItCode, TxtItName, TxtPlanningUomCode, TxtProductionRoutingDocNo, 
                        TxtAmt1, TxtAmt2
                    }, true);
                SetFormControl(mState.View);
                LuePGCode.Visible = false;
                DteStartDt.Visible = false;
                DteEndDt.Visible = false;

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsProductionOrderUseProcessGroup = Sm.GetParameterBoo("IsProductionOrderUseProcessGroup");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Routing DNo",

                        //1-5
                        "Work Center#",
                        "",
                        "Work Center Name",
                        "Location",
                        "Capacity",
                        
                        //6-10
                        "UoM",
                        "Sequence",
                        "",
                        "Bill of Material#",
                        "",
                        
                        //11-15
                        "Bill of Material Name",
                        "Total Values",
                        "Start Date",
                        "End Date",
                        "Process Group Code",

                        //16
                        "Process Group"
                    },
                     new int[] 
                    {
                        20, 
                        130, 20, 300, 150, 80, 
                        80, 80, 20, 130, 20, 
                        300, 150, 100, 100, 0, 
                        200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 8, 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 12 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 2 );
            Sm.GrdFormatDate(Grd1, new int[] { 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 9, 10 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 9, 11, 12, 15 });
            if (mIsProductionOrderUseProcessGroup)
                Grd1.Cols[16].Move(1);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 6, 9, 10 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LueCtCode, LueCurCode, TxtQty, 
                        MeeRemark
                    }, true);
                    BtnItCode.Enabled = BtnProductionRoutingDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 8, 13, 14, 16 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, LueCurCode, TxtQty, MeeRemark
                    }, false);
                    BtnItCode.Enabled = BtnProductionRoutingDocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 8, 13, 14, 16 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueCtCode, LueCurCode, TxtItCode, 
                TxtItName, TxtPlanningUomCode, TxtProductionRoutingDocNo, MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtQty, TxtAmt1, TxtAmt2 }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 7, 12 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmJCFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, mMainCurCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmBom2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && TxtDocNo.Text.Length == 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmJCDlg3(this, e.RowIndex, Sm.GetGrdStr(Grd1, e.RowIndex, 1)));
            }

            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && Sm.IsGrdColSelected(new int[] { 13, 14, 16 }, e.ColIndex))
            {
                if (e.ColIndex == 13) Sm.DteRequestEdit(Grd1, DteStartDt, ref fCell, ref fAccept, e);
                if (e.ColIndex == 14) Sm.DteRequestEdit(Grd1, DteEndDt, ref fCell, ref fAccept, e);
                if (e.ColIndex == 16) LueRequestEdit(Grd1, LuePGCode, ref fCell, ref fAccept, e, 14);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            ComputeAmt();
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                Sm.FormShowDialog(new FrmJCDlg3(this, e.RowIndex, Sm.GetGrdStr(Grd1, e.RowIndex, 1)));

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmBom2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "JC", "TblJCHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveJCHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveJCDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsTxtEmpty(TxtItCode, "Item", false) ||
                Sm.IsTxtEmpty(TxtQty, "Quantity", true) ||
                Sm.IsTxtEmpty(TxtProductionRoutingDocNo, "Routing#", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 work center.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Work center# is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 9, false, "Bill of Material# is empty.")
                    ) return true;
            }
            return false;
        }

        private MySqlCommand SaveJCHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblJCHdr(DocNo, DocDt, CancelInd, CtCode, CurCode, ItCode, Qty, ProductionRoutingDocNo, Amt1, Amt2, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'N', @CtCode, @CurCode, @ItCode, @Qty, @ProductionRoutingDocNo, @Amt1, @Amt2, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@Qty", decimal.Parse(TxtQty.Text));
            Sm.CmParam<String>(ref cm, "@ProductionRoutingDocNo", TxtProductionRoutingDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt1", decimal.Parse(TxtAmt1.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJCDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblJCDtl(DocNo, DNo, ProductionRoutingDNo, BomDocNo, Amt, StartDt, EndDt, PGCode, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @ProductionRoutingDNo, @BomDocNo, @Amt, @StartDt, @EndDt, @PGCode, @CreateBy, CurrentDateTime()); " 
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ProductionRoutingDNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@BomDocNo", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetGrdStr(Grd1, Row, 15));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd1, Row, 13));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetGrdDate(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditJCHdr());
            
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                IsEditedDataNotCancelled() ||
                IsEditedDataAlreadyCancelled();
        }

        private bool IsEditedDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "This job costing is not cancelled.");
                return true;
            }
            return false;
        }

        private bool IsEditedDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select CancelInd From TblJCHdr Where DocNo=@Param And CancelInd='Y' Limit 1;", 
                TxtDocNo.Text, 
                "This job costing already cancelled.");
        }

        private MySqlCommand EditJCHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblJCHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowJCHdr(DocNo);
                ShowJCDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowJCHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CtCode, A.CurCode, ");
            SQL.AppendLine("A.ItCode, B.ItName, A.Qty, B.PlanningUomCode, A.ProductionRoutingDocNo, ");
            SQL.AppendLine("A.Amt1, A.Amt2, A.Remark ");
            SQL.AppendLine("From TblJCHdr A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "CtCode", "CurCode", "ItCode",     
                        
                        //6-10
                        "ItName", "Qty", "PlanningUomCode", "ProductionRoutingDocNo", "Amt1",  
                        
                        //11-12
                        "Amt2", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[4]));
                        TxtItCode.EditValue = Sm.DrStr(dr, c[5]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[6]);
                        TxtQty.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[7]), 0);
                        TxtPlanningUomCode.EditValue = Sm.DrStr(dr, c[8]);
                        TxtProductionRoutingDocNo.EditValue = Sm.DrStr(dr, c[9]);
                        TxtAmt1.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[10]), 0);
                        TxtAmt2.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[11]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                    }, true
                );
        }

        private void ShowJCDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ProductionRoutingDNo, C.WorkCenterDocNo, D.DocName As WorkCenterDocName, ");
            SQL.AppendLine("D.Location, D.Capacity, D.UomCode, C.Sequence, B.BomDocNo, E.DocName As BomDocName, B.Amt, ");
            SQL.AppendLine("B.StartDt, B.EndDt, B.PGCode, Concat(F.PGCode, ' : ', F.PGName) As PGName ");
            SQL.AppendLine("From TblJCHdr A ");
            SQL.AppendLine("Inner Join TblJCDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblProductionRoutingDtl C On A.ProductionRoutingDocNo=C.DocNo And B.ProductionRoutingDNo=C.DNo ");
            SQL.AppendLine("Left Join TblWorkCenterHdr D On C.WorkCenterDocNo=D.DocNo ");
            SQL.AppendLine("Left Join TblBomHdr E On B.BomDocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblProcessGroup F On B.PGCode=F.PGCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ProductionRoutingDNo", 

                    //1-5
                    "WorkCenterDocNo", "WorkCenterDocName", "Location", "Capacity", "UomCode", 

                    //6-10
                    "Sequence", "BomDocNo", "BomDocName", "Amt", "StartDt",

                    //11-13
                    "EndDt", "PGCode", "PGName"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 7, 12 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void SetLuePGCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PGCode As Col1, Concat(PGCode, ' : ', PGName) As Col2 From TblProcessGroup " +
                "Where PGCode Not In (Select Parent From TblProcessGroup Where Parent Is Not Null) " +
                "Order By PGName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e, int Col)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, Col));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal void ShowProductionRoutingInfo(string DocNo)
        {
            var SQL = new StringBuilder();

            ClearGrd();

            SQL.AppendLine("Select A.DNo, A.WorkCenterDocNo, B.DocName, B.Location, B.Capacity, B.UomCode, A.Sequence ");
            SQL.AppendLine("From TblProductionRoutingDtl A ");
            SQL.AppendLine("Left Join TblWorkCenterHdr B On A.WorkCenterDocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.Sequence, B.DocName;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                       { 
                           //0
                           "DNo",

                           //1-5
                           "WorkCenterDocNo", "DocName", "Location", "Capacity", "UomCode",

                           //6
                           "Sequence"
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValueNull(ref Grd, Row, new int[] { 9, 11 });
                        Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 12 });
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 7 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ComputeAmt()
        {
            decimal Amt1 = 0m, Amt2 = 0, Qty = 0m;

            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 12).Length>0)                
                    Amt1 += Sm.GetGrdDec(Grd1, Row, 12);
            
            if (TxtQty.Text.Length > 0) Qty = decimal.Parse(TxtQty.Text);
            if (Qty != 0m) Amt2 = Math.Round(Amt1 / Qty, 2);

            TxtAmt1.Text = Sm.FormatNum(Amt1, 0);
            TxtAmt2.Text = Sm.FormatNum(Amt2, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(Sl.SetLueCtCode), "");
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void TxtQty_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtQty, 0);
            ComputeAmt();
        }

        private void DteStartDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteStartDt, ref fCell, ref fAccept);
        }

        private void DteStartDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteEndDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteEndDt, ref fCell, ref fAccept);
        }

        private void DteEndDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void LuePGCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LuePGCode_Leave(object sender, EventArgs e)
        {
            if (LuePGCode.Visible && fAccept)
            {
                if (Sm.GetLue(LuePGCode).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = null;
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex - 1].Value = Sm.GetLue(LuePGCode);
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = LuePGCode.GetColumnValue("Col2");
                }
                LuePGCode.Visible = false;
            }
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue1(SetLuePGCode));
        }

        #endregion

        #region Button Event

        private void BtnItCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmJCDlg(this));
        }

        private void BtnItCode2_Click(object sender, EventArgs e)
        {
            Sm.ShowItemInfo("XXX", TxtItCode.Text);
        }

        private void BtnProductionRoutingDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmJCDlg2(this));
        }

        private void BtnTxtProductionRoutingDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtProductionRoutingDocNo, "Routing#", false))
            {
                var f1 = new FrmProductionRouting(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtProductionRoutingDocNo.Text;
                f1.ShowDialog();
            }
        }

        #endregion

        #endregion
    }
}
