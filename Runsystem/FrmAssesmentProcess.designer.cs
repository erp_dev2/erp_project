﻿namespace RunSystem
{
    partial class FrmAssesmentProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAssesmentProcess));
            this.panel3 = new System.Windows.Forms.Panel();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteJoinDt = new DevExpress.XtraEditors.DateEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtPosition = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtDeptName = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnEmpCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtPosition2 = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtDeptName2 = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtEmpName2 = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.BtnEmpCode2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtEmpCode2 = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtCompetenceLevelName = new DevExpress.XtraEditors.TextEdit();
            this.TxtAsmCode = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnAssesment2 = new DevExpress.XtraEditors.SimpleButton();
            this.label18 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnAssement = new DevExpress.XtraEditors.SimpleButton();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtAsmName = new DevExpress.XtraEditors.TextEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.Grd15 = new TenTec.Windows.iGridLib.iGrid();
            this.panel21 = new System.Windows.Forms.Panel();
            this.TxtScore15 = new DevExpress.XtraEditors.TextEdit();
            this.label61 = new System.Windows.Forms.Label();
            this.TxtLevel15 = new DevExpress.XtraEditors.TextEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.LueComp15 = new DevExpress.XtraEditors.LookUpEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.Grd14 = new TenTec.Windows.iGridLib.iGrid();
            this.panel20 = new System.Windows.Forms.Panel();
            this.TxtScore14 = new DevExpress.XtraEditors.TextEdit();
            this.label60 = new System.Windows.Forms.Label();
            this.TxtLevel14 = new DevExpress.XtraEditors.TextEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.LueComp14 = new DevExpress.XtraEditors.LookUpEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.Grd13 = new TenTec.Windows.iGridLib.iGrid();
            this.panel19 = new System.Windows.Forms.Panel();
            this.TxtScore13 = new DevExpress.XtraEditors.TextEdit();
            this.label59 = new System.Windows.Forms.Label();
            this.TxtLevel13 = new DevExpress.XtraEditors.TextEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.LueComp13 = new DevExpress.XtraEditors.LookUpEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.Grd12 = new TenTec.Windows.iGridLib.iGrid();
            this.panel18 = new System.Windows.Forms.Panel();
            this.TxtScore12 = new DevExpress.XtraEditors.TextEdit();
            this.label58 = new System.Windows.Forms.Label();
            this.TxtLevel12 = new DevExpress.XtraEditors.TextEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.LueComp12 = new DevExpress.XtraEditors.LookUpEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.Grd11 = new TenTec.Windows.iGridLib.iGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TxtScore11 = new DevExpress.XtraEditors.TextEdit();
            this.label57 = new System.Windows.Forms.Label();
            this.TxtLevel11 = new DevExpress.XtraEditors.TextEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.LueComp11 = new DevExpress.XtraEditors.LookUpEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.Grd10 = new TenTec.Windows.iGridLib.iGrid();
            this.panel17 = new System.Windows.Forms.Panel();
            this.TxtScore10 = new DevExpress.XtraEditors.TextEdit();
            this.label56 = new System.Windows.Forms.Label();
            this.TxtLevel10 = new DevExpress.XtraEditors.TextEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.LueComp10 = new DevExpress.XtraEditors.LookUpEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.Grd9 = new TenTec.Windows.iGridLib.iGrid();
            this.panel16 = new System.Windows.Forms.Panel();
            this.TxtScore9 = new DevExpress.XtraEditors.TextEdit();
            this.label55 = new System.Windows.Forms.Label();
            this.TxtLevel9 = new DevExpress.XtraEditors.TextEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.LueComp9 = new DevExpress.XtraEditors.LookUpEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.Grd8 = new TenTec.Windows.iGridLib.iGrid();
            this.panel15 = new System.Windows.Forms.Panel();
            this.TxtScore8 = new DevExpress.XtraEditors.TextEdit();
            this.label54 = new System.Windows.Forms.Label();
            this.TxtLevel8 = new DevExpress.XtraEditors.TextEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.LueComp8 = new DevExpress.XtraEditors.LookUpEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.panel14 = new System.Windows.Forms.Panel();
            this.TxtScore7 = new DevExpress.XtraEditors.TextEdit();
            this.label53 = new System.Windows.Forms.Label();
            this.TxtLevel7 = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.LueComp7 = new DevExpress.XtraEditors.LookUpEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.panel13 = new System.Windows.Forms.Panel();
            this.TxtScore6 = new DevExpress.XtraEditors.TextEdit();
            this.label52 = new System.Windows.Forms.Label();
            this.TxtLevel6 = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.LueComp6 = new DevExpress.XtraEditors.LookUpEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.panel12 = new System.Windows.Forms.Panel();
            this.TxtScore5 = new DevExpress.XtraEditors.TextEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.TxtLevel5 = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.LueComp5 = new DevExpress.XtraEditors.LookUpEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.panel11 = new System.Windows.Forms.Panel();
            this.TxtScore4 = new DevExpress.XtraEditors.TextEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.TxtLevel4 = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.LueComp4 = new DevExpress.XtraEditors.LookUpEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel10 = new System.Windows.Forms.Panel();
            this.TxtScore3 = new DevExpress.XtraEditors.TextEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.TxtLevel3 = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.LueComp3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel9 = new System.Windows.Forms.Panel();
            this.TxtScore2 = new DevExpress.XtraEditors.TextEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.TxtLevel2 = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.LueComp2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel8 = new System.Windows.Forms.Panel();
            this.TxtScore1 = new DevExpress.XtraEditors.TextEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.TxtLevel1 = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.LueComp1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.Grd16 = new TenTec.Windows.iGridLib.iGrid();
            this.panel6 = new System.Windows.Forms.Panel();
            this.TxtScore16 = new DevExpress.XtraEditors.TextEdit();
            this.label62 = new System.Windows.Forms.Label();
            this.TxtLevel16 = new DevExpress.XtraEditors.TextEdit();
            this.label63 = new System.Windows.Forms.Label();
            this.LueComp16 = new DevExpress.XtraEditors.LookUpEdit();
            this.label64 = new System.Windows.Forms.Label();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.Grd17 = new TenTec.Windows.iGridLib.iGrid();
            this.panel7 = new System.Windows.Forms.Panel();
            this.TxtScore17 = new DevExpress.XtraEditors.TextEdit();
            this.label65 = new System.Windows.Forms.Label();
            this.TxtLevel17 = new DevExpress.XtraEditors.TextEdit();
            this.label66 = new System.Windows.Forms.Label();
            this.LueComp17 = new DevExpress.XtraEditors.LookUpEdit();
            this.label67 = new System.Windows.Forms.Label();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this.Grd18 = new TenTec.Windows.iGridLib.iGrid();
            this.panel22 = new System.Windows.Forms.Panel();
            this.TxtScore18 = new DevExpress.XtraEditors.TextEdit();
            this.label68 = new System.Windows.Forms.Label();
            this.TxtLevel18 = new DevExpress.XtraEditors.TextEdit();
            this.label69 = new System.Windows.Forms.Label();
            this.LueComp18 = new DevExpress.XtraEditors.LookUpEdit();
            this.label70 = new System.Windows.Forms.Label();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.Grd19 = new TenTec.Windows.iGridLib.iGrid();
            this.panel23 = new System.Windows.Forms.Panel();
            this.TxtScore19 = new DevExpress.XtraEditors.TextEdit();
            this.label71 = new System.Windows.Forms.Label();
            this.TxtLevel19 = new DevExpress.XtraEditors.TextEdit();
            this.label72 = new System.Windows.Forms.Label();
            this.LueComp19 = new DevExpress.XtraEditors.LookUpEdit();
            this.label73 = new System.Windows.Forms.Label();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this.Grd20 = new TenTec.Windows.iGridLib.iGrid();
            this.panel24 = new System.Windows.Forms.Panel();
            this.TxtScore20 = new DevExpress.XtraEditors.TextEdit();
            this.label74 = new System.Windows.Forms.Label();
            this.TxtLevel20 = new DevExpress.XtraEditors.TextEdit();
            this.label75 = new System.Windows.Forms.Label();
            this.LueComp20 = new DevExpress.XtraEditors.LookUpEdit();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.BtnTraining = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptName2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompetenceLevelName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsmCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsmName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.tabPage15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).BeginInit();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp15.Properties)).BeginInit();
            this.tabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).BeginInit();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp14.Properties)).BeginInit();
            this.tabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).BeginInit();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp13.Properties)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).BeginInit();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp12.Properties)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp11.Properties)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd10)).BeginInit();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp10.Properties)).BeginInit();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).BeginInit();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp9.Properties)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp8.Properties)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp7.Properties)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp6.Properties)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp5.Properties)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp4.Properties)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp3.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp2.Properties)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp1.Properties)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp16.Properties)).BeginInit();
            this.tabPage17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp17.Properties)).BeginInit();
            this.tabPage18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd18)).BeginInit();
            this.panel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp18.Properties)).BeginInit();
            this.tabPage19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd19)).BeginInit();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp19.Properties)).BeginInit();
            this.tabPage20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd20)).BeginInit();
            this.panel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp20.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(811, 0);
            this.panel1.Size = new System.Drawing.Size(70, 461);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(811, 461);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.MeeCancelReason);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.DteJoinDt);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.TxtPosition);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.TxtDeptName);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.TxtEmpName);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.BtnEmpCode);
            this.panel3.Controls.Add(this.TxtEmpCode);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(811, 204);
            this.panel3.TabIndex = 11;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(147, 49);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(195, 20);
            this.MeeCancelReason.TabIndex = 17;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(6, 51);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(135, 14);
            this.label20.TabIndex = 16;
            this.label20.Text = "Reason For Cancellation";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(344, 49);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 18;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // DteJoinDt
            // 
            this.DteJoinDt.EditValue = null;
            this.DteJoinDt.EnterMoveNextControl = true;
            this.DteJoinDt.Location = new System.Drawing.Point(147, 159);
            this.DteJoinDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteJoinDt.Name = "DteJoinDt";
            this.DteJoinDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJoinDt.Properties.Appearance.Options.UseFont = true;
            this.DteJoinDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJoinDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteJoinDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteJoinDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteJoinDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJoinDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteJoinDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJoinDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteJoinDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteJoinDt.Size = new System.Drawing.Size(111, 20);
            this.DteJoinDt.TabIndex = 29;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(83, 162);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 14);
            this.label9.TabIndex = 28;
            this.label9.Text = "Join Date";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPosition
            // 
            this.TxtPosition.EnterMoveNextControl = true;
            this.TxtPosition.Location = new System.Drawing.Point(147, 137);
            this.TxtPosition.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosition.Name = "TxtPosition";
            this.TxtPosition.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPosition.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosition.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosition.Properties.Appearance.Options.UseFont = true;
            this.TxtPosition.Properties.MaxLength = 16;
            this.TxtPosition.Size = new System.Drawing.Size(258, 20);
            this.TxtPosition.TabIndex = 27;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(92, 139);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 14);
            this.label7.TabIndex = 26;
            this.label7.Text = "Position";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDeptName
            // 
            this.TxtDeptName.EnterMoveNextControl = true;
            this.TxtDeptName.Location = new System.Drawing.Point(147, 115);
            this.TxtDeptName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptName.Name = "TxtDeptName";
            this.TxtDeptName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDeptName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptName.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptName.Properties.MaxLength = 16;
            this.TxtDeptName.Size = new System.Drawing.Size(258, 20);
            this.TxtDeptName.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(68, 117);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 14);
            this.label8.TabIndex = 24;
            this.label8.Text = "Department";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(147, 93);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 16;
            this.TxtEmpName.Size = new System.Drawing.Size(258, 20);
            this.TxtEmpName.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(46, 94);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 14);
            this.label1.TabIndex = 22;
            this.label1.Text = "Employee Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnEmpCode
            // 
            this.BtnEmpCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode.Appearance.Options.UseFont = true;
            this.BtnEmpCode.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode.Image")));
            this.BtnEmpCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode.Location = new System.Drawing.Point(346, 70);
            this.BtnEmpCode.Name = "BtnEmpCode";
            this.BtnEmpCode.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpCode.TabIndex = 21;
            this.BtnEmpCode.ToolTip = "List Of Employee";
            this.BtnEmpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode.ToolTipTitle = "Run System";
            this.BtnEmpCode.Click += new System.EventHandler(this.BtnEmpCode_Click);
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.EnterMoveNextControl = true;
            this.TxtEmpCode.Location = new System.Drawing.Point(147, 71);
            this.TxtEmpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode.Properties.MaxLength = 16;
            this.TxtEmpCode.Size = new System.Drawing.Size(195, 20);
            this.TxtEmpCode.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(49, 72);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 14);
            this.label6.TabIndex = 19;
            this.label6.Text = "Employee Code";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.BtnTraining);
            this.panel4.Controls.Add(this.label77);
            this.panel4.Controls.Add(this.TxtPosition2);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.TxtDeptName2);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.TxtEmpName2);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.BtnEmpCode2);
            this.panel4.Controls.Add(this.TxtEmpCode2);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.TxtCompetenceLevelName);
            this.panel4.Controls.Add(this.TxtAsmCode);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.BtnAssesment2);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.BtnAssement);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.TxtAsmName);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(429, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(382, 204);
            this.panel4.TabIndex = 35;
            // 
            // TxtPosition2
            // 
            this.TxtPosition2.EnterMoveNextControl = true;
            this.TxtPosition2.Location = new System.Drawing.Point(117, 137);
            this.TxtPosition2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosition2.Name = "TxtPosition2";
            this.TxtPosition2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPosition2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosition2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosition2.Properties.Appearance.Options.UseFont = true;
            this.TxtPosition2.Properties.MaxLength = 16;
            this.TxtPosition2.Size = new System.Drawing.Size(258, 20);
            this.TxtPosition2.TabIndex = 46;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(64, 139);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 14);
            this.label10.TabIndex = 45;
            this.label10.Text = "Position";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDeptName2
            // 
            this.TxtDeptName2.EnterMoveNextControl = true;
            this.TxtDeptName2.Location = new System.Drawing.Point(117, 115);
            this.TxtDeptName2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptName2.Name = "TxtDeptName2";
            this.TxtDeptName2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDeptName2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptName2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptName2.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptName2.Properties.MaxLength = 16;
            this.TxtDeptName2.Size = new System.Drawing.Size(258, 20);
            this.TxtDeptName2.TabIndex = 44;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(40, 117);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 14);
            this.label12.TabIndex = 43;
            this.label12.Text = "Department";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName2
            // 
            this.TxtEmpName2.EnterMoveNextControl = true;
            this.TxtEmpName2.Location = new System.Drawing.Point(117, 93);
            this.TxtEmpName2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName2.Name = "TxtEmpName2";
            this.TxtEmpName2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpName2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName2.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName2.Properties.MaxLength = 16;
            this.TxtEmpName2.Size = new System.Drawing.Size(258, 20);
            this.TxtEmpName2.TabIndex = 42;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(30, 94);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 14);
            this.label14.TabIndex = 41;
            this.label14.Text = "Assesor Name";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnEmpCode2
            // 
            this.BtnEmpCode2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode2.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode2.Appearance.Options.UseFont = true;
            this.BtnEmpCode2.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode2.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode2.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode2.Image")));
            this.BtnEmpCode2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode2.Location = new System.Drawing.Point(287, 71);
            this.BtnEmpCode2.Name = "BtnEmpCode2";
            this.BtnEmpCode2.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpCode2.TabIndex = 40;
            this.BtnEmpCode2.ToolTip = "List Of Employee";
            this.BtnEmpCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode2.ToolTipTitle = "Run System";
            this.BtnEmpCode2.Click += new System.EventHandler(this.BtnEmpCode2_Click);
            // 
            // TxtEmpCode2
            // 
            this.TxtEmpCode2.EnterMoveNextControl = true;
            this.TxtEmpCode2.Location = new System.Drawing.Point(117, 71);
            this.TxtEmpCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode2.Name = "TxtEmpCode2";
            this.TxtEmpCode2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode2.Properties.MaxLength = 16;
            this.TxtEmpCode2.Size = new System.Drawing.Size(167, 20);
            this.TxtEmpCode2.TabIndex = 39;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(33, 72);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 14);
            this.label16.TabIndex = 38;
            this.label16.Text = "Assesor Code";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCompetenceLevelName
            // 
            this.TxtCompetenceLevelName.EnterMoveNextControl = true;
            this.TxtCompetenceLevelName.Location = new System.Drawing.Point(116, 49);
            this.TxtCompetenceLevelName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCompetenceLevelName.Name = "TxtCompetenceLevelName";
            this.TxtCompetenceLevelName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCompetenceLevelName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCompetenceLevelName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCompetenceLevelName.Properties.Appearance.Options.UseFont = true;
            this.TxtCompetenceLevelName.Properties.MaxLength = 16;
            this.TxtCompetenceLevelName.Size = new System.Drawing.Size(248, 20);
            this.TxtCompetenceLevelName.TabIndex = 37;
            // 
            // TxtAsmCode
            // 
            this.TxtAsmCode.EnterMoveNextControl = true;
            this.TxtAsmCode.Location = new System.Drawing.Point(116, 5);
            this.TxtAsmCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAsmCode.Name = "TxtAsmCode";
            this.TxtAsmCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAsmCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAsmCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAsmCode.Properties.Appearance.Options.UseFont = true;
            this.TxtAsmCode.Properties.MaxLength = 16;
            this.TxtAsmCode.Size = new System.Drawing.Size(166, 20);
            this.TxtAsmCode.TabIndex = 31;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(4, 51);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 14);
            this.label4.TabIndex = 36;
            this.label4.Text = "Competence Level";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAssesment2
            // 
            this.BtnAssesment2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAssesment2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAssesment2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAssesment2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAssesment2.Appearance.Options.UseBackColor = true;
            this.BtnAssesment2.Appearance.Options.UseFont = true;
            this.BtnAssesment2.Appearance.Options.UseForeColor = true;
            this.BtnAssesment2.Appearance.Options.UseTextOptions = true;
            this.BtnAssesment2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAssesment2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAssesment2.Image = ((System.Drawing.Image)(resources.GetObject("BtnAssesment2.Image")));
            this.BtnAssesment2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAssesment2.Location = new System.Drawing.Point(321, 5);
            this.BtnAssesment2.Name = "BtnAssesment2";
            this.BtnAssesment2.Size = new System.Drawing.Size(24, 21);
            this.BtnAssesment2.TabIndex = 33;
            this.BtnAssesment2.ToolTip = "Show Assesment";
            this.BtnAssesment2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAssesment2.ToolTipTitle = "Run System";
            this.BtnAssesment2.Click += new System.EventHandler(this.BtnAssesment2_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(24, 8);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 14);
            this.label18.TabIndex = 30;
            this.label18.Text = "Assesment No.";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(116, 159);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(258, 20);
            this.MeeRemark.TabIndex = 48;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(66, 161);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 47;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAssement
            // 
            this.BtnAssement.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAssement.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAssement.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAssement.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAssement.Appearance.Options.UseBackColor = true;
            this.BtnAssement.Appearance.Options.UseFont = true;
            this.BtnAssement.Appearance.Options.UseForeColor = true;
            this.BtnAssement.Appearance.Options.UseTextOptions = true;
            this.BtnAssement.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAssement.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAssement.Image = ((System.Drawing.Image)(resources.GetObject("BtnAssement.Image")));
            this.BtnAssement.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAssement.Location = new System.Drawing.Point(288, 5);
            this.BtnAssement.Name = "BtnAssement";
            this.BtnAssement.Size = new System.Drawing.Size(24, 21);
            this.BtnAssement.TabIndex = 32;
            this.BtnAssement.ToolTip = "List Of Assesment";
            this.BtnAssement.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAssement.ToolTipTitle = "Run System";
            this.BtnAssement.Click += new System.EventHandler(this.BtnAssement_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(12, 29);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(101, 14);
            this.label17.TabIndex = 34;
            this.label17.Text = "Assesment Name";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAsmName
            // 
            this.TxtAsmName.EnterMoveNextControl = true;
            this.TxtAsmName.Location = new System.Drawing.Point(116, 27);
            this.TxtAsmName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAsmName.Name = "TxtAsmName";
            this.TxtAsmName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAsmName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAsmName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAsmName.Properties.Appearance.Options.UseFont = true;
            this.TxtAsmName.Properties.MaxLength = 16;
            this.TxtAsmName.Size = new System.Drawing.Size(248, 20);
            this.TxtAsmName.TabIndex = 35;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(147, 27);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(111, 20);
            this.DteDocDt.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(108, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(147, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Size = new System.Drawing.Size(258, 20);
            this.TxtDocNo.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(68, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 14);
            this.label3.TabIndex = 12;
            this.label3.Text = "Document#";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.Grd15);
            this.tabPage15.Controls.Add(this.panel21);
            this.tabPage15.Location = new System.Drawing.Point(4, 23);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(764, 20);
            this.tabPage15.TabIndex = 14;
            this.tabPage15.Text = "No.15";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // Grd15
            // 
            this.Grd15.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd15.DefaultRow.Height = 20;
            this.Grd15.DefaultRow.Sortable = false;
            this.Grd15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd15.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd15.Header.Height = 21;
            this.Grd15.Location = new System.Drawing.Point(0, 33);
            this.Grd15.Name = "Grd15";
            this.Grd15.RowHeader.Visible = true;
            this.Grd15.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd15.SingleClickEdit = true;
            this.Grd15.Size = new System.Drawing.Size(764, 0);
            this.Grd15.TabIndex = 51;
            this.Grd15.TreeCol = null;
            this.Grd15.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd15.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd15_RequestEdit);
            this.Grd15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd15_KeyDown);
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel21.Controls.Add(this.TxtScore15);
            this.panel21.Controls.Add(this.label61);
            this.panel21.Controls.Add(this.TxtLevel15);
            this.panel21.Controls.Add(this.label46);
            this.panel21.Controls.Add(this.LueComp15);
            this.panel21.Controls.Add(this.label40);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(764, 33);
            this.panel21.TabIndex = 44;
            // 
            // TxtScore15
            // 
            this.TxtScore15.EnterMoveNextControl = true;
            this.TxtScore15.Location = new System.Drawing.Point(603, 4);
            this.TxtScore15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore15.Name = "TxtScore15";
            this.TxtScore15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore15.Properties.Appearance.Options.UseFont = true;
            this.TxtScore15.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore15.Properties.MaxLength = 12;
            this.TxtScore15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore15.Size = new System.Drawing.Size(86, 20);
            this.TxtScore15.TabIndex = 71;
            this.TxtScore15.Validated += new System.EventHandler(this.TxtScore15_Validated);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(561, 8);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(38, 14);
            this.label61.TabIndex = 60;
            this.label61.Text = "Score";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel15
            // 
            this.TxtLevel15.EnterMoveNextControl = true;
            this.TxtLevel15.Location = new System.Drawing.Point(481, 5);
            this.TxtLevel15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel15.Name = "TxtLevel15";
            this.TxtLevel15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel15.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel15.Properties.MaxLength = 16;
            this.TxtLevel15.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel15.TabIndex = 57;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Red;
            this.label46.Location = new System.Drawing.Point(435, 9);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(35, 14);
            this.label46.TabIndex = 56;
            this.label46.Text = "Level";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp15
            // 
            this.LueComp15.EnterMoveNextControl = true;
            this.LueComp15.Location = new System.Drawing.Point(90, 6);
            this.LueComp15.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp15.Name = "LueComp15";
            this.LueComp15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp15.Properties.Appearance.Options.UseFont = true;
            this.LueComp15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp15.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp15.Properties.DropDownRows = 20;
            this.LueComp15.Properties.NullText = "[Empty]";
            this.LueComp15.Properties.PopupWidth = 500;
            this.LueComp15.Size = new System.Drawing.Size(330, 20);
            this.LueComp15.TabIndex = 46;
            this.LueComp15.ToolTip = "F4 : Show/hide list";
            this.LueComp15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp15.EditValueChanged += new System.EventHandler(this.LueComp15_EditValueChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Red;
            this.label40.Location = new System.Drawing.Point(8, 9);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(77, 14);
            this.label40.TabIndex = 45;
            this.label40.Text = "Competence";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.Grd14);
            this.tabPage14.Controls.Add(this.panel20);
            this.tabPage14.Location = new System.Drawing.Point(4, 23);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(764, 20);
            this.tabPage14.TabIndex = 13;
            this.tabPage14.Text = "No.14";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // Grd14
            // 
            this.Grd14.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd14.DefaultRow.Height = 20;
            this.Grd14.DefaultRow.Sortable = false;
            this.Grd14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd14.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd14.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd14.Header.Height = 21;
            this.Grd14.Location = new System.Drawing.Point(0, 33);
            this.Grd14.Name = "Grd14";
            this.Grd14.RowHeader.Visible = true;
            this.Grd14.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd14.SingleClickEdit = true;
            this.Grd14.Size = new System.Drawing.Size(764, 0);
            this.Grd14.TabIndex = 51;
            this.Grd14.TreeCol = null;
            this.Grd14.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd14.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd14_RequestEdit);
            this.Grd14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd14_KeyDown);
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel20.Controls.Add(this.TxtScore14);
            this.panel20.Controls.Add(this.label60);
            this.panel20.Controls.Add(this.TxtLevel14);
            this.panel20.Controls.Add(this.label45);
            this.panel20.Controls.Add(this.LueComp14);
            this.panel20.Controls.Add(this.label38);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(764, 33);
            this.panel20.TabIndex = 44;
            // 
            // TxtScore14
            // 
            this.TxtScore14.EnterMoveNextControl = true;
            this.TxtScore14.Location = new System.Drawing.Point(609, 6);
            this.TxtScore14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore14.Name = "TxtScore14";
            this.TxtScore14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore14.Properties.Appearance.Options.UseFont = true;
            this.TxtScore14.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore14.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore14.Properties.MaxLength = 12;
            this.TxtScore14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore14.Size = new System.Drawing.Size(86, 20);
            this.TxtScore14.TabIndex = 71;
            this.TxtScore14.Validated += new System.EventHandler(this.TxtScore14_Validated);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(566, 9);
            this.label60.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(38, 14);
            this.label60.TabIndex = 60;
            this.label60.Text = "Score";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel14
            // 
            this.TxtLevel14.EnterMoveNextControl = true;
            this.TxtLevel14.Location = new System.Drawing.Point(482, 7);
            this.TxtLevel14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel14.Name = "TxtLevel14";
            this.TxtLevel14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel14.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel14.Properties.MaxLength = 16;
            this.TxtLevel14.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel14.TabIndex = 57;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Red;
            this.label45.Location = new System.Drawing.Point(436, 10);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(35, 14);
            this.label45.TabIndex = 56;
            this.label45.Text = "Level";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp14
            // 
            this.LueComp14.EnterMoveNextControl = true;
            this.LueComp14.Location = new System.Drawing.Point(96, 6);
            this.LueComp14.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp14.Name = "LueComp14";
            this.LueComp14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp14.Properties.Appearance.Options.UseFont = true;
            this.LueComp14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp14.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp14.Properties.DropDownRows = 20;
            this.LueComp14.Properties.NullText = "[Empty]";
            this.LueComp14.Properties.PopupWidth = 500;
            this.LueComp14.Size = new System.Drawing.Size(330, 20);
            this.LueComp14.TabIndex = 46;
            this.LueComp14.ToolTip = "F4 : Show/hide list";
            this.LueComp14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp14.EditValueChanged += new System.EventHandler(this.LueComp14_EditValueChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Location = new System.Drawing.Point(8, 9);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(77, 14);
            this.label38.TabIndex = 45;
            this.label38.Text = "Competence";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.Grd13);
            this.tabPage13.Controls.Add(this.panel19);
            this.tabPage13.Location = new System.Drawing.Point(4, 23);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Size = new System.Drawing.Size(764, 20);
            this.tabPage13.TabIndex = 12;
            this.tabPage13.Text = "No.13";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // Grd13
            // 
            this.Grd13.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd13.DefaultRow.Height = 20;
            this.Grd13.DefaultRow.Sortable = false;
            this.Grd13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd13.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd13.Header.Height = 21;
            this.Grd13.Location = new System.Drawing.Point(0, 33);
            this.Grd13.Name = "Grd13";
            this.Grd13.RowHeader.Visible = true;
            this.Grd13.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd13.SingleClickEdit = true;
            this.Grd13.Size = new System.Drawing.Size(764, 0);
            this.Grd13.TabIndex = 51;
            this.Grd13.TreeCol = null;
            this.Grd13.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd13.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd13_RequestEdit);
            this.Grd13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd13_KeyDown);
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel19.Controls.Add(this.TxtScore13);
            this.panel19.Controls.Add(this.label59);
            this.panel19.Controls.Add(this.TxtLevel13);
            this.panel19.Controls.Add(this.label44);
            this.panel19.Controls.Add(this.LueComp13);
            this.panel19.Controls.Add(this.label36);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(764, 33);
            this.panel19.TabIndex = 44;
            // 
            // TxtScore13
            // 
            this.TxtScore13.EnterMoveNextControl = true;
            this.TxtScore13.Location = new System.Drawing.Point(606, 7);
            this.TxtScore13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore13.Name = "TxtScore13";
            this.TxtScore13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore13.Properties.Appearance.Options.UseFont = true;
            this.TxtScore13.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore13.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore13.Properties.MaxLength = 12;
            this.TxtScore13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore13.Size = new System.Drawing.Size(86, 20);
            this.TxtScore13.TabIndex = 71;
            this.TxtScore13.Validated += new System.EventHandler(this.TxtScore13_Validated);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(564, 9);
            this.label59.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(38, 14);
            this.label59.TabIndex = 60;
            this.label59.Text = "Score";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel13
            // 
            this.TxtLevel13.EnterMoveNextControl = true;
            this.TxtLevel13.Location = new System.Drawing.Point(479, 6);
            this.TxtLevel13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel13.Name = "TxtLevel13";
            this.TxtLevel13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel13.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel13.Properties.MaxLength = 16;
            this.TxtLevel13.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel13.TabIndex = 57;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Red;
            this.label44.Location = new System.Drawing.Point(436, 11);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(35, 14);
            this.label44.TabIndex = 56;
            this.label44.Text = "Level";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp13
            // 
            this.LueComp13.EnterMoveNextControl = true;
            this.LueComp13.Location = new System.Drawing.Point(91, 6);
            this.LueComp13.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp13.Name = "LueComp13";
            this.LueComp13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp13.Properties.Appearance.Options.UseFont = true;
            this.LueComp13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp13.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp13.Properties.DropDownRows = 20;
            this.LueComp13.Properties.NullText = "[Empty]";
            this.LueComp13.Properties.PopupWidth = 500;
            this.LueComp13.Size = new System.Drawing.Size(330, 20);
            this.LueComp13.TabIndex = 46;
            this.LueComp13.ToolTip = "F4 : Show/hide list";
            this.LueComp13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp13.EditValueChanged += new System.EventHandler(this.LueComp13_EditValueChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Red;
            this.label36.Location = new System.Drawing.Point(8, 9);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(77, 14);
            this.label36.TabIndex = 45;
            this.label36.Text = "Competence";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.Grd12);
            this.tabPage12.Controls.Add(this.panel18);
            this.tabPage12.Location = new System.Drawing.Point(4, 23);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(764, 20);
            this.tabPage12.TabIndex = 11;
            this.tabPage12.Text = "No.12";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // Grd12
            // 
            this.Grd12.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd12.DefaultRow.Height = 20;
            this.Grd12.DefaultRow.Sortable = false;
            this.Grd12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd12.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd12.Header.Height = 21;
            this.Grd12.Location = new System.Drawing.Point(0, 33);
            this.Grd12.Name = "Grd12";
            this.Grd12.RowHeader.Visible = true;
            this.Grd12.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd12.SingleClickEdit = true;
            this.Grd12.Size = new System.Drawing.Size(764, 0);
            this.Grd12.TabIndex = 51;
            this.Grd12.TreeCol = null;
            this.Grd12.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd12.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd12_RequestEdit);
            this.Grd12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd12_KeyDown);
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel18.Controls.Add(this.TxtScore12);
            this.panel18.Controls.Add(this.label58);
            this.panel18.Controls.Add(this.TxtLevel12);
            this.panel18.Controls.Add(this.label43);
            this.panel18.Controls.Add(this.LueComp12);
            this.panel18.Controls.Add(this.label34);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(764, 33);
            this.panel18.TabIndex = 44;
            // 
            // TxtScore12
            // 
            this.TxtScore12.EnterMoveNextControl = true;
            this.TxtScore12.Location = new System.Drawing.Point(608, 5);
            this.TxtScore12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore12.Name = "TxtScore12";
            this.TxtScore12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore12.Properties.Appearance.Options.UseFont = true;
            this.TxtScore12.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore12.Properties.MaxLength = 12;
            this.TxtScore12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore12.Size = new System.Drawing.Size(86, 20);
            this.TxtScore12.TabIndex = 71;
            this.TxtScore12.Validated += new System.EventHandler(this.TxtScore12_Validated);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(563, 9);
            this.label58.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(38, 14);
            this.label58.TabIndex = 60;
            this.label58.Text = "Score";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel12
            // 
            this.TxtLevel12.EnterMoveNextControl = true;
            this.TxtLevel12.Location = new System.Drawing.Point(485, 6);
            this.TxtLevel12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel12.Name = "TxtLevel12";
            this.TxtLevel12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel12.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel12.Properties.MaxLength = 16;
            this.TxtLevel12.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel12.TabIndex = 57;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Red;
            this.label43.Location = new System.Drawing.Point(439, 9);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(35, 14);
            this.label43.TabIndex = 56;
            this.label43.Text = "Level";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp12
            // 
            this.LueComp12.EnterMoveNextControl = true;
            this.LueComp12.Location = new System.Drawing.Point(90, 6);
            this.LueComp12.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp12.Name = "LueComp12";
            this.LueComp12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp12.Properties.Appearance.Options.UseFont = true;
            this.LueComp12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp12.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp12.Properties.DropDownRows = 20;
            this.LueComp12.Properties.NullText = "[Empty]";
            this.LueComp12.Properties.PopupWidth = 500;
            this.LueComp12.Size = new System.Drawing.Size(330, 20);
            this.LueComp12.TabIndex = 46;
            this.LueComp12.ToolTip = "F4 : Show/hide list";
            this.LueComp12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp12.EditValueChanged += new System.EventHandler(this.LueComp12_EditValueChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Red;
            this.label34.Location = new System.Drawing.Point(8, 9);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(77, 14);
            this.label34.TabIndex = 45;
            this.label34.Text = "Competence";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.Grd11);
            this.tabPage11.Controls.Add(this.panel5);
            this.tabPage11.Location = new System.Drawing.Point(4, 23);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(764, 20);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "No.11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // Grd11
            // 
            this.Grd11.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd11.DefaultRow.Height = 20;
            this.Grd11.DefaultRow.Sortable = false;
            this.Grd11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd11.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd11.Header.Height = 21;
            this.Grd11.Location = new System.Drawing.Point(0, 33);
            this.Grd11.Name = "Grd11";
            this.Grd11.RowHeader.Visible = true;
            this.Grd11.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd11.SingleClickEdit = true;
            this.Grd11.Size = new System.Drawing.Size(764, 0);
            this.Grd11.TabIndex = 51;
            this.Grd11.TreeCol = null;
            this.Grd11.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd11.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd11_RequestEdit);
            this.Grd11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd11_KeyDown);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.TxtScore11);
            this.panel5.Controls.Add(this.label57);
            this.panel5.Controls.Add(this.TxtLevel11);
            this.panel5.Controls.Add(this.label42);
            this.panel5.Controls.Add(this.LueComp11);
            this.panel5.Controls.Add(this.label32);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(764, 33);
            this.panel5.TabIndex = 44;
            // 
            // TxtScore11
            // 
            this.TxtScore11.EnterMoveNextControl = true;
            this.TxtScore11.Location = new System.Drawing.Point(601, 7);
            this.TxtScore11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore11.Name = "TxtScore11";
            this.TxtScore11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore11.Properties.Appearance.Options.UseFont = true;
            this.TxtScore11.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore11.Properties.MaxLength = 12;
            this.TxtScore11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore11.Size = new System.Drawing.Size(86, 20);
            this.TxtScore11.TabIndex = 71;
            this.TxtScore11.Validated += new System.EventHandler(this.TxtScore11_Validated);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(558, 11);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(38, 14);
            this.label57.TabIndex = 60;
            this.label57.Text = "Score";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel11
            // 
            this.TxtLevel11.EnterMoveNextControl = true;
            this.TxtLevel11.Location = new System.Drawing.Point(471, 7);
            this.TxtLevel11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel11.Name = "TxtLevel11";
            this.TxtLevel11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel11.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel11.Properties.MaxLength = 16;
            this.TxtLevel11.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel11.TabIndex = 57;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Red;
            this.label42.Location = new System.Drawing.Point(428, 9);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(35, 14);
            this.label42.TabIndex = 56;
            this.label42.Text = "Level";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp11
            // 
            this.LueComp11.EnterMoveNextControl = true;
            this.LueComp11.Location = new System.Drawing.Point(93, 6);
            this.LueComp11.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp11.Name = "LueComp11";
            this.LueComp11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp11.Properties.Appearance.Options.UseFont = true;
            this.LueComp11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp11.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp11.Properties.DropDownRows = 20;
            this.LueComp11.Properties.NullText = "[Empty]";
            this.LueComp11.Properties.PopupWidth = 500;
            this.LueComp11.Size = new System.Drawing.Size(330, 20);
            this.LueComp11.TabIndex = 46;
            this.LueComp11.ToolTip = "F4 : Show/hide list";
            this.LueComp11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp11.EditValueChanged += new System.EventHandler(this.LueComp11_EditValueChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Location = new System.Drawing.Point(8, 9);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(77, 14);
            this.label32.TabIndex = 45;
            this.label32.Text = "Competence";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.Grd10);
            this.tabPage10.Controls.Add(this.panel17);
            this.tabPage10.Location = new System.Drawing.Point(4, 23);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(764, 20);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "No.10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // Grd10
            // 
            this.Grd10.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd10.DefaultRow.Height = 20;
            this.Grd10.DefaultRow.Sortable = false;
            this.Grd10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd10.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd10.Header.Height = 21;
            this.Grd10.Location = new System.Drawing.Point(0, 33);
            this.Grd10.Name = "Grd10";
            this.Grd10.RowHeader.Visible = true;
            this.Grd10.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd10.SingleClickEdit = true;
            this.Grd10.Size = new System.Drawing.Size(764, 0);
            this.Grd10.TabIndex = 51;
            this.Grd10.TreeCol = null;
            this.Grd10.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd10.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd10_RequestEdit);
            this.Grd10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd10_KeyDown);
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel17.Controls.Add(this.TxtScore10);
            this.panel17.Controls.Add(this.label56);
            this.panel17.Controls.Add(this.TxtLevel10);
            this.panel17.Controls.Add(this.label41);
            this.panel17.Controls.Add(this.LueComp10);
            this.panel17.Controls.Add(this.label31);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(764, 33);
            this.panel17.TabIndex = 44;
            // 
            // TxtScore10
            // 
            this.TxtScore10.EnterMoveNextControl = true;
            this.TxtScore10.Location = new System.Drawing.Point(600, 5);
            this.TxtScore10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore10.Name = "TxtScore10";
            this.TxtScore10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore10.Properties.Appearance.Options.UseFont = true;
            this.TxtScore10.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore10.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore10.Properties.MaxLength = 12;
            this.TxtScore10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore10.Size = new System.Drawing.Size(86, 20);
            this.TxtScore10.TabIndex = 71;
            this.TxtScore10.Validated += new System.EventHandler(this.TxtScore10_Validated);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(557, 9);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(38, 14);
            this.label56.TabIndex = 60;
            this.label56.Text = "Score";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel10
            // 
            this.TxtLevel10.EnterMoveNextControl = true;
            this.TxtLevel10.Location = new System.Drawing.Point(472, 6);
            this.TxtLevel10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel10.Name = "TxtLevel10";
            this.TxtLevel10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel10.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel10.Properties.MaxLength = 16;
            this.TxtLevel10.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel10.TabIndex = 57;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Red;
            this.label41.Location = new System.Drawing.Point(431, 8);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(35, 14);
            this.label41.TabIndex = 56;
            this.label41.Text = "Level";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp10
            // 
            this.LueComp10.EnterMoveNextControl = true;
            this.LueComp10.Location = new System.Drawing.Point(94, 6);
            this.LueComp10.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp10.Name = "LueComp10";
            this.LueComp10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp10.Properties.Appearance.Options.UseFont = true;
            this.LueComp10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp10.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp10.Properties.DropDownRows = 20;
            this.LueComp10.Properties.NullText = "[Empty]";
            this.LueComp10.Properties.PopupWidth = 500;
            this.LueComp10.Size = new System.Drawing.Size(330, 20);
            this.LueComp10.TabIndex = 46;
            this.LueComp10.ToolTip = "F4 : Show/hide list";
            this.LueComp10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp10.EditValueChanged += new System.EventHandler(this.LueComp10_EditValueChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(8, 9);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(77, 14);
            this.label31.TabIndex = 45;
            this.label31.Text = "Competence";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.Grd9);
            this.tabPage9.Controls.Add(this.panel16);
            this.tabPage9.Location = new System.Drawing.Point(4, 23);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(764, 20);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "No.9";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // Grd9
            // 
            this.Grd9.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd9.DefaultRow.Height = 20;
            this.Grd9.DefaultRow.Sortable = false;
            this.Grd9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd9.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd9.Header.Height = 21;
            this.Grd9.Location = new System.Drawing.Point(0, 32);
            this.Grd9.Name = "Grd9";
            this.Grd9.RowHeader.Visible = true;
            this.Grd9.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd9.SingleClickEdit = true;
            this.Grd9.Size = new System.Drawing.Size(764, 0);
            this.Grd9.TabIndex = 51;
            this.Grd9.TreeCol = null;
            this.Grd9.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd9.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd9_RequestEdit);
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel16.Controls.Add(this.TxtScore9);
            this.panel16.Controls.Add(this.label55);
            this.panel16.Controls.Add(this.TxtLevel9);
            this.panel16.Controls.Add(this.label39);
            this.panel16.Controls.Add(this.LueComp9);
            this.panel16.Controls.Add(this.label29);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(764, 32);
            this.panel16.TabIndex = 44;
            // 
            // TxtScore9
            // 
            this.TxtScore9.EnterMoveNextControl = true;
            this.TxtScore9.Location = new System.Drawing.Point(606, 5);
            this.TxtScore9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore9.Name = "TxtScore9";
            this.TxtScore9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore9.Properties.Appearance.Options.UseFont = true;
            this.TxtScore9.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore9.Properties.MaxLength = 12;
            this.TxtScore9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore9.Size = new System.Drawing.Size(86, 20);
            this.TxtScore9.TabIndex = 71;
            this.TxtScore9.Validated += new System.EventHandler(this.TxtScore9_Validated);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(566, 8);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(38, 14);
            this.label55.TabIndex = 60;
            this.label55.Text = "Score";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel9
            // 
            this.TxtLevel9.EnterMoveNextControl = true;
            this.TxtLevel9.Location = new System.Drawing.Point(483, 6);
            this.TxtLevel9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel9.Name = "TxtLevel9";
            this.TxtLevel9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel9.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel9.Properties.MaxLength = 16;
            this.TxtLevel9.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel9.TabIndex = 57;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Location = new System.Drawing.Point(436, 10);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(35, 14);
            this.label39.TabIndex = 56;
            this.label39.Text = "Level";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp9
            // 
            this.LueComp9.EnterMoveNextControl = true;
            this.LueComp9.Location = new System.Drawing.Point(98, 6);
            this.LueComp9.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp9.Name = "LueComp9";
            this.LueComp9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp9.Properties.Appearance.Options.UseFont = true;
            this.LueComp9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp9.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp9.Properties.DropDownRows = 20;
            this.LueComp9.Properties.NullText = "[Empty]";
            this.LueComp9.Properties.PopupWidth = 500;
            this.LueComp9.Size = new System.Drawing.Size(330, 20);
            this.LueComp9.TabIndex = 46;
            this.LueComp9.ToolTip = "F4 : Show/hide list";
            this.LueComp9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp9.EditValueChanged += new System.EventHandler(this.LueComp9_EditValueChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(8, 9);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(77, 14);
            this.label29.TabIndex = 45;
            this.label29.Text = "Competence";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.Grd8);
            this.tabPage8.Controls.Add(this.panel15);
            this.tabPage8.Location = new System.Drawing.Point(4, 23);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(764, 20);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "No.8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // Grd8
            // 
            this.Grd8.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd8.DefaultRow.Height = 20;
            this.Grd8.DefaultRow.Sortable = false;
            this.Grd8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd8.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd8.Header.Height = 21;
            this.Grd8.Location = new System.Drawing.Point(0, 32);
            this.Grd8.Name = "Grd8";
            this.Grd8.RowHeader.Visible = true;
            this.Grd8.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd8.SingleClickEdit = true;
            this.Grd8.Size = new System.Drawing.Size(764, 0);
            this.Grd8.TabIndex = 51;
            this.Grd8.TreeCol = null;
            this.Grd8.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd8.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd8_RequestEdit);
            this.Grd8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd8_KeyDown);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel15.Controls.Add(this.TxtScore8);
            this.panel15.Controls.Add(this.label54);
            this.panel15.Controls.Add(this.TxtLevel8);
            this.panel15.Controls.Add(this.label37);
            this.panel15.Controls.Add(this.LueComp8);
            this.panel15.Controls.Add(this.label27);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(764, 32);
            this.panel15.TabIndex = 44;
            // 
            // TxtScore8
            // 
            this.TxtScore8.EnterMoveNextControl = true;
            this.TxtScore8.Location = new System.Drawing.Point(594, 7);
            this.TxtScore8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore8.Name = "TxtScore8";
            this.TxtScore8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore8.Properties.Appearance.Options.UseFont = true;
            this.TxtScore8.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore8.Properties.MaxLength = 12;
            this.TxtScore8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore8.Size = new System.Drawing.Size(86, 20);
            this.TxtScore8.TabIndex = 71;
            this.TxtScore8.Validated += new System.EventHandler(this.TxtScore8_Validated);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(549, 10);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(38, 14);
            this.label54.TabIndex = 60;
            this.label54.Text = "Score";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel8
            // 
            this.TxtLevel8.EnterMoveNextControl = true;
            this.TxtLevel8.Location = new System.Drawing.Point(466, 7);
            this.TxtLevel8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel8.Name = "TxtLevel8";
            this.TxtLevel8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel8.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel8.Properties.MaxLength = 16;
            this.TxtLevel8.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel8.TabIndex = 57;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(423, 9);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(35, 14);
            this.label37.TabIndex = 56;
            this.label37.Text = "Level";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp8
            // 
            this.LueComp8.EnterMoveNextControl = true;
            this.LueComp8.Location = new System.Drawing.Point(89, 6);
            this.LueComp8.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp8.Name = "LueComp8";
            this.LueComp8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp8.Properties.Appearance.Options.UseFont = true;
            this.LueComp8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp8.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp8.Properties.DropDownRows = 20;
            this.LueComp8.Properties.NullText = "[Empty]";
            this.LueComp8.Properties.PopupWidth = 500;
            this.LueComp8.Size = new System.Drawing.Size(330, 20);
            this.LueComp8.TabIndex = 46;
            this.LueComp8.ToolTip = "F4 : Show/hide list";
            this.LueComp8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp8.EditValueChanged += new System.EventHandler(this.LueComp8_EditValueChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(8, 9);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(77, 14);
            this.label27.TabIndex = 45;
            this.label27.Text = "Competence";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.Grd7);
            this.tabPage7.Controls.Add(this.panel14);
            this.tabPage7.Location = new System.Drawing.Point(4, 23);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(764, 20);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "No.7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(0, 32);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(764, 0);
            this.Grd7.TabIndex = 51;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd7_RequestEdit);
            this.Grd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd7_KeyDown);
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel14.Controls.Add(this.TxtScore7);
            this.panel14.Controls.Add(this.label53);
            this.panel14.Controls.Add(this.TxtLevel7);
            this.panel14.Controls.Add(this.label35);
            this.panel14.Controls.Add(this.LueComp7);
            this.panel14.Controls.Add(this.label25);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(764, 32);
            this.panel14.TabIndex = 44;
            // 
            // TxtScore7
            // 
            this.TxtScore7.EnterMoveNextControl = true;
            this.TxtScore7.Location = new System.Drawing.Point(601, 6);
            this.TxtScore7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore7.Name = "TxtScore7";
            this.TxtScore7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore7.Properties.Appearance.Options.UseFont = true;
            this.TxtScore7.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore7.Properties.MaxLength = 12;
            this.TxtScore7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore7.Size = new System.Drawing.Size(86, 20);
            this.TxtScore7.TabIndex = 71;
            this.TxtScore7.Validated += new System.EventHandler(this.TxtScore7_Validated);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(560, 9);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(38, 14);
            this.label53.TabIndex = 60;
            this.label53.Text = "Score";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel7
            // 
            this.TxtLevel7.EnterMoveNextControl = true;
            this.TxtLevel7.Location = new System.Drawing.Point(479, 6);
            this.TxtLevel7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel7.Name = "TxtLevel7";
            this.TxtLevel7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel7.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel7.Properties.MaxLength = 16;
            this.TxtLevel7.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel7.TabIndex = 57;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Red;
            this.label35.Location = new System.Drawing.Point(434, 9);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(35, 14);
            this.label35.TabIndex = 56;
            this.label35.Text = "Level";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp7
            // 
            this.LueComp7.EnterMoveNextControl = true;
            this.LueComp7.Location = new System.Drawing.Point(97, 6);
            this.LueComp7.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp7.Name = "LueComp7";
            this.LueComp7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp7.Properties.Appearance.Options.UseFont = true;
            this.LueComp7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp7.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp7.Properties.DropDownRows = 20;
            this.LueComp7.Properties.NullText = "[Empty]";
            this.LueComp7.Properties.PopupWidth = 500;
            this.LueComp7.Size = new System.Drawing.Size(330, 20);
            this.LueComp7.TabIndex = 46;
            this.LueComp7.ToolTip = "F4 : Show/hide list";
            this.LueComp7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp7.EditValueChanged += new System.EventHandler(this.LueComp7_EditValueChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(8, 9);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(77, 14);
            this.label25.TabIndex = 45;
            this.label25.Text = "Competence";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.Grd6);
            this.tabPage6.Controls.Add(this.panel13);
            this.tabPage6.Location = new System.Drawing.Point(4, 23);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(764, 20);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "No.6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(0, 33);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(764, 0);
            this.Grd6.TabIndex = 51;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd6_RequestEdit);
            this.Grd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd6_KeyDown);
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel13.Controls.Add(this.TxtScore6);
            this.panel13.Controls.Add(this.label52);
            this.panel13.Controls.Add(this.TxtLevel6);
            this.panel13.Controls.Add(this.label33);
            this.panel13.Controls.Add(this.LueComp6);
            this.panel13.Controls.Add(this.label23);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(764, 33);
            this.panel13.TabIndex = 44;
            // 
            // TxtScore6
            // 
            this.TxtScore6.EnterMoveNextControl = true;
            this.TxtScore6.Location = new System.Drawing.Point(598, 6);
            this.TxtScore6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore6.Name = "TxtScore6";
            this.TxtScore6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore6.Properties.Appearance.Options.UseFont = true;
            this.TxtScore6.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore6.Properties.MaxLength = 12;
            this.TxtScore6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore6.Size = new System.Drawing.Size(86, 20);
            this.TxtScore6.TabIndex = 71;
            this.TxtScore6.Validated += new System.EventHandler(this.TxtScore6_Validated);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(557, 9);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(38, 14);
            this.label52.TabIndex = 60;
            this.label52.Text = "Score";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel6
            // 
            this.TxtLevel6.EnterMoveNextControl = true;
            this.TxtLevel6.Location = new System.Drawing.Point(470, 6);
            this.TxtLevel6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel6.Name = "TxtLevel6";
            this.TxtLevel6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel6.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel6.Properties.MaxLength = 16;
            this.TxtLevel6.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel6.TabIndex = 57;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(431, 10);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(35, 14);
            this.label33.TabIndex = 56;
            this.label33.Text = "Level";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp6
            // 
            this.LueComp6.EnterMoveNextControl = true;
            this.LueComp6.Location = new System.Drawing.Point(93, 7);
            this.LueComp6.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp6.Name = "LueComp6";
            this.LueComp6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp6.Properties.Appearance.Options.UseFont = true;
            this.LueComp6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp6.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp6.Properties.DropDownRows = 20;
            this.LueComp6.Properties.NullText = "[Empty]";
            this.LueComp6.Properties.PopupWidth = 500;
            this.LueComp6.Size = new System.Drawing.Size(330, 20);
            this.LueComp6.TabIndex = 46;
            this.LueComp6.ToolTip = "F4 : Show/hide list";
            this.LueComp6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp6.EditValueChanged += new System.EventHandler(this.LueComp6_EditValueChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(8, 9);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(77, 14);
            this.label23.TabIndex = 45;
            this.label23.Text = "Competence";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.Grd5);
            this.tabPage5.Controls.Add(this.panel12);
            this.tabPage5.Location = new System.Drawing.Point(4, 23);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(764, 20);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "No.5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 32);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(764, 0);
            this.Grd5.TabIndex = 51;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel12.Controls.Add(this.TxtScore5);
            this.panel12.Controls.Add(this.label51);
            this.panel12.Controls.Add(this.TxtLevel5);
            this.panel12.Controls.Add(this.label30);
            this.panel12.Controls.Add(this.LueComp5);
            this.panel12.Controls.Add(this.label21);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(764, 32);
            this.panel12.TabIndex = 44;
            // 
            // TxtScore5
            // 
            this.TxtScore5.EnterMoveNextControl = true;
            this.TxtScore5.Location = new System.Drawing.Point(608, 4);
            this.TxtScore5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore5.Name = "TxtScore5";
            this.TxtScore5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore5.Properties.Appearance.Options.UseFont = true;
            this.TxtScore5.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore5.Properties.MaxLength = 12;
            this.TxtScore5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore5.Size = new System.Drawing.Size(86, 20);
            this.TxtScore5.TabIndex = 71;
            this.TxtScore5.Validated += new System.EventHandler(this.TxtScore5_Validated);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(564, 8);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(38, 14);
            this.label51.TabIndex = 60;
            this.label51.Text = "Score";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel5
            // 
            this.TxtLevel5.EnterMoveNextControl = true;
            this.TxtLevel5.Location = new System.Drawing.Point(476, 6);
            this.TxtLevel5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel5.Name = "TxtLevel5";
            this.TxtLevel5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel5.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel5.Properties.MaxLength = 16;
            this.TxtLevel5.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel5.TabIndex = 57;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(434, 9);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(35, 14);
            this.label30.TabIndex = 56;
            this.label30.Text = "Level";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp5
            // 
            this.LueComp5.EnterMoveNextControl = true;
            this.LueComp5.Location = new System.Drawing.Point(94, 6);
            this.LueComp5.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp5.Name = "LueComp5";
            this.LueComp5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp5.Properties.Appearance.Options.UseFont = true;
            this.LueComp5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp5.Properties.DropDownRows = 20;
            this.LueComp5.Properties.NullText = "[Empty]";
            this.LueComp5.Properties.PopupWidth = 500;
            this.LueComp5.Size = new System.Drawing.Size(330, 20);
            this.LueComp5.TabIndex = 46;
            this.LueComp5.ToolTip = "F4 : Show/hide list";
            this.LueComp5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp5.EditValueChanged += new System.EventHandler(this.LueComp5_EditValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(8, 9);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 14);
            this.label21.TabIndex = 45;
            this.label21.Text = "Competence";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.Grd4);
            this.tabPage4.Controls.Add(this.panel11);
            this.tabPage4.Location = new System.Drawing.Point(4, 23);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(764, 20);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "No.4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 32);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(764, 0);
            this.Grd4.TabIndex = 51;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel11.Controls.Add(this.TxtScore4);
            this.panel11.Controls.Add(this.label50);
            this.panel11.Controls.Add(this.TxtLevel4);
            this.panel11.Controls.Add(this.label28);
            this.panel11.Controls.Add(this.LueComp4);
            this.panel11.Controls.Add(this.label19);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(764, 32);
            this.panel11.TabIndex = 44;
            // 
            // TxtScore4
            // 
            this.TxtScore4.EnterMoveNextControl = true;
            this.TxtScore4.Location = new System.Drawing.Point(598, 5);
            this.TxtScore4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore4.Name = "TxtScore4";
            this.TxtScore4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore4.Properties.Appearance.Options.UseFont = true;
            this.TxtScore4.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore4.Properties.MaxLength = 12;
            this.TxtScore4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore4.Size = new System.Drawing.Size(86, 20);
            this.TxtScore4.TabIndex = 71;
            this.TxtScore4.Validated += new System.EventHandler(this.TxtScore4_Validated);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(555, 9);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(38, 14);
            this.label50.TabIndex = 60;
            this.label50.Text = "Score";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel4
            // 
            this.TxtLevel4.EnterMoveNextControl = true;
            this.TxtLevel4.Location = new System.Drawing.Point(471, 5);
            this.TxtLevel4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel4.Name = "TxtLevel4";
            this.TxtLevel4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel4.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel4.Properties.MaxLength = 16;
            this.TxtLevel4.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel4.TabIndex = 57;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(429, 9);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(35, 14);
            this.label28.TabIndex = 56;
            this.label28.Text = "Level";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp4
            // 
            this.LueComp4.EnterMoveNextControl = true;
            this.LueComp4.Location = new System.Drawing.Point(89, 6);
            this.LueComp4.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp4.Name = "LueComp4";
            this.LueComp4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp4.Properties.Appearance.Options.UseFont = true;
            this.LueComp4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp4.Properties.DropDownRows = 20;
            this.LueComp4.Properties.NullText = "[Empty]";
            this.LueComp4.Properties.PopupWidth = 500;
            this.LueComp4.Size = new System.Drawing.Size(330, 20);
            this.LueComp4.TabIndex = 46;
            this.LueComp4.ToolTip = "F4 : Show/hide list";
            this.LueComp4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp4.EditValueChanged += new System.EventHandler(this.LueComp4_EditValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(8, 9);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 14);
            this.label19.TabIndex = 45;
            this.label19.Text = "Competence";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.Grd3);
            this.tabPage3.Controls.Add(this.panel10);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(764, 20);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "No.3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 34);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(764, 0);
            this.Grd3.TabIndex = 51;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.Controls.Add(this.TxtScore3);
            this.panel10.Controls.Add(this.label49);
            this.panel10.Controls.Add(this.TxtLevel3);
            this.panel10.Controls.Add(this.label26);
            this.panel10.Controls.Add(this.LueComp3);
            this.panel10.Controls.Add(this.label15);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(764, 34);
            this.panel10.TabIndex = 44;
            // 
            // TxtScore3
            // 
            this.TxtScore3.EnterMoveNextControl = true;
            this.TxtScore3.Location = new System.Drawing.Point(616, 7);
            this.TxtScore3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore3.Name = "TxtScore3";
            this.TxtScore3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore3.Properties.Appearance.Options.UseFont = true;
            this.TxtScore3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore3.Properties.MaxLength = 12;
            this.TxtScore3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore3.Size = new System.Drawing.Size(86, 20);
            this.TxtScore3.TabIndex = 71;
            this.TxtScore3.Validated += new System.EventHandler(this.TxtScore3_Validated);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(570, 10);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(38, 14);
            this.label49.TabIndex = 60;
            this.label49.Text = "Score";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel3
            // 
            this.TxtLevel3.EnterMoveNextControl = true;
            this.TxtLevel3.Location = new System.Drawing.Point(475, 9);
            this.TxtLevel3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel3.Name = "TxtLevel3";
            this.TxtLevel3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel3.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel3.Properties.MaxLength = 16;
            this.TxtLevel3.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel3.TabIndex = 57;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(433, 11);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 14);
            this.label26.TabIndex = 56;
            this.label26.Text = "Level";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp3
            // 
            this.LueComp3.EnterMoveNextControl = true;
            this.LueComp3.Location = new System.Drawing.Point(89, 8);
            this.LueComp3.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp3.Name = "LueComp3";
            this.LueComp3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp3.Properties.Appearance.Options.UseFont = true;
            this.LueComp3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp3.Properties.DropDownRows = 20;
            this.LueComp3.Properties.NullText = "[Empty]";
            this.LueComp3.Properties.PopupWidth = 500;
            this.LueComp3.Size = new System.Drawing.Size(330, 20);
            this.LueComp3.TabIndex = 46;
            this.LueComp3.ToolTip = "F4 : Show/hide list";
            this.LueComp3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp3.EditValueChanged += new System.EventHandler(this.LueComp3_EditValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(8, 9);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 14);
            this.label15.TabIndex = 45;
            this.label15.Text = "Competence";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.Grd2);
            this.tabPage2.Controls.Add(this.panel9);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(764, 20);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "No.2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(3, 35);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(758, 0);
            this.Grd2.TabIndex = 51;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.TxtScore2);
            this.panel9.Controls.Add(this.label48);
            this.panel9.Controls.Add(this.TxtLevel2);
            this.panel9.Controls.Add(this.label24);
            this.panel9.Controls.Add(this.LueComp2);
            this.panel9.Controls.Add(this.label13);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(758, 32);
            this.panel9.TabIndex = 44;
            // 
            // TxtScore2
            // 
            this.TxtScore2.EnterMoveNextControl = true;
            this.TxtScore2.Location = new System.Drawing.Point(606, 5);
            this.TxtScore2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore2.Name = "TxtScore2";
            this.TxtScore2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore2.Properties.Appearance.Options.UseFont = true;
            this.TxtScore2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore2.Properties.MaxLength = 12;
            this.TxtScore2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore2.Size = new System.Drawing.Size(86, 20);
            this.TxtScore2.TabIndex = 71;
            this.TxtScore2.Validated += new System.EventHandler(this.TxtScore2_Validated);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(564, 9);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(38, 14);
            this.label48.TabIndex = 58;
            this.label48.Text = "Score";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel2
            // 
            this.TxtLevel2.EnterMoveNextControl = true;
            this.TxtLevel2.Location = new System.Drawing.Point(475, 6);
            this.TxtLevel2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel2.Name = "TxtLevel2";
            this.TxtLevel2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel2.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel2.Properties.MaxLength = 16;
            this.TxtLevel2.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel2.TabIndex = 57;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(433, 9);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 14);
            this.label24.TabIndex = 56;
            this.label24.Text = "Level";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp2
            // 
            this.LueComp2.EnterMoveNextControl = true;
            this.LueComp2.Location = new System.Drawing.Point(91, 6);
            this.LueComp2.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp2.Name = "LueComp2";
            this.LueComp2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp2.Properties.Appearance.Options.UseFont = true;
            this.LueComp2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp2.Properties.DropDownRows = 20;
            this.LueComp2.Properties.NullText = "[Empty]";
            this.LueComp2.Properties.PopupWidth = 500;
            this.LueComp2.Size = new System.Drawing.Size(330, 20);
            this.LueComp2.TabIndex = 46;
            this.LueComp2.ToolTip = "F4 : Show/hide list";
            this.LueComp2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp2.EditValueChanged += new System.EventHandler(this.LueComp2_EditValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(9, 9);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 14);
            this.label13.TabIndex = 45;
            this.label13.Text = "Competence";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.Grd1);
            this.tabPage1.Controls.Add(this.panel8);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(803, 230);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "No.1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(3, 33);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(797, 194);
            this.Grd1.TabIndex = 45;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.TxtScore1);
            this.panel8.Controls.Add(this.label47);
            this.panel8.Controls.Add(this.TxtLevel1);
            this.panel8.Controls.Add(this.label22);
            this.panel8.Controls.Add(this.LueComp1);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(797, 30);
            this.panel8.TabIndex = 44;
            // 
            // TxtScore1
            // 
            this.TxtScore1.EnterMoveNextControl = true;
            this.TxtScore1.Location = new System.Drawing.Point(608, 6);
            this.TxtScore1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore1.Name = "TxtScore1";
            this.TxtScore1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore1.Properties.Appearance.Options.UseFont = true;
            this.TxtScore1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore1.Properties.MaxLength = 12;
            this.TxtScore1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore1.Size = new System.Drawing.Size(86, 20);
            this.TxtScore1.TabIndex = 70;
            this.TxtScore1.Validated += new System.EventHandler(this.TxtScore1_Validated);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(568, 8);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(38, 14);
            this.label47.TabIndex = 56;
            this.label47.Text = "Score";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel1
            // 
            this.TxtLevel1.EnterMoveNextControl = true;
            this.TxtLevel1.Location = new System.Drawing.Point(478, 4);
            this.TxtLevel1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel1.Name = "TxtLevel1";
            this.TxtLevel1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel1.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel1.Properties.MaxLength = 16;
            this.TxtLevel1.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel1.TabIndex = 55;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(439, 7);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 14);
            this.label22.TabIndex = 54;
            this.label22.Text = "Level";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp1
            // 
            this.LueComp1.EnterMoveNextControl = true;
            this.LueComp1.Location = new System.Drawing.Point(97, 4);
            this.LueComp1.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp1.Name = "LueComp1";
            this.LueComp1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp1.Properties.Appearance.Options.UseFont = true;
            this.LueComp1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp1.Properties.DropDownRows = 20;
            this.LueComp1.Properties.NullText = "[Empty]";
            this.LueComp1.Properties.PopupWidth = 500;
            this.LueComp1.Size = new System.Drawing.Size(330, 20);
            this.LueComp1.TabIndex = 51;
            this.LueComp1.ToolTip = "F4 : Show/hide list";
            this.LueComp1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp1.EditValueChanged += new System.EventHandler(this.LueComp1_EditValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(13, 7);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 14);
            this.label11.TabIndex = 45;
            this.label11.Text = "Competence";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Controls.Add(this.tabPage13);
            this.tabControl1.Controls.Add(this.tabPage14);
            this.tabControl1.Controls.Add(this.tabPage15);
            this.tabControl1.Controls.Add(this.tabPage16);
            this.tabControl1.Controls.Add(this.tabPage17);
            this.tabControl1.Controls.Add(this.tabPage18);
            this.tabControl1.Controls.Add(this.tabPage19);
            this.tabControl1.Controls.Add(this.tabPage20);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 204);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(811, 257);
            this.tabControl1.TabIndex = 44;
            this.tabControl1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tabControl1_KeyDown);
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this.Grd16);
            this.tabPage16.Controls.Add(this.panel6);
            this.tabPage16.Location = new System.Drawing.Point(4, 23);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage16.Size = new System.Drawing.Size(764, 20);
            this.tabPage16.TabIndex = 15;
            this.tabPage16.Text = "No.16";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // Grd16
            // 
            this.Grd16.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd16.DefaultRow.Height = 20;
            this.Grd16.DefaultRow.Sortable = false;
            this.Grd16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd16.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd16.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd16.Header.Height = 21;
            this.Grd16.Location = new System.Drawing.Point(3, 36);
            this.Grd16.Name = "Grd16";
            this.Grd16.RowHeader.Visible = true;
            this.Grd16.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd16.SingleClickEdit = true;
            this.Grd16.Size = new System.Drawing.Size(758, 0);
            this.Grd16.TabIndex = 52;
            this.Grd16.TreeCol = null;
            this.Grd16.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd16.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd16_RequestEdit);
            this.Grd16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd16_KeyDown);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.TxtScore16);
            this.panel6.Controls.Add(this.label62);
            this.panel6.Controls.Add(this.TxtLevel16);
            this.panel6.Controls.Add(this.label63);
            this.panel6.Controls.Add(this.LueComp16);
            this.panel6.Controls.Add(this.label64);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(758, 33);
            this.panel6.TabIndex = 45;
            // 
            // TxtScore16
            // 
            this.TxtScore16.EnterMoveNextControl = true;
            this.TxtScore16.Location = new System.Drawing.Point(603, 4);
            this.TxtScore16.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore16.Name = "TxtScore16";
            this.TxtScore16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore16.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore16.Properties.Appearance.Options.UseFont = true;
            this.TxtScore16.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore16.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore16.Properties.MaxLength = 12;
            this.TxtScore16.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore16.Size = new System.Drawing.Size(86, 20);
            this.TxtScore16.TabIndex = 71;
            this.TxtScore16.Validated += new System.EventHandler(this.TxtScore16_Validated);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(561, 8);
            this.label62.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(38, 14);
            this.label62.TabIndex = 60;
            this.label62.Text = "Score";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel16
            // 
            this.TxtLevel16.EnterMoveNextControl = true;
            this.TxtLevel16.Location = new System.Drawing.Point(481, 5);
            this.TxtLevel16.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel16.Name = "TxtLevel16";
            this.TxtLevel16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel16.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel16.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel16.Properties.MaxLength = 16;
            this.TxtLevel16.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel16.TabIndex = 57;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Red;
            this.label63.Location = new System.Drawing.Point(435, 9);
            this.label63.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(35, 14);
            this.label63.TabIndex = 56;
            this.label63.Text = "Level";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp16
            // 
            this.LueComp16.EnterMoveNextControl = true;
            this.LueComp16.Location = new System.Drawing.Point(90, 6);
            this.LueComp16.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp16.Name = "LueComp16";
            this.LueComp16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp16.Properties.Appearance.Options.UseFont = true;
            this.LueComp16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp16.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp16.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp16.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp16.Properties.DropDownRows = 20;
            this.LueComp16.Properties.NullText = "[Empty]";
            this.LueComp16.Properties.PopupWidth = 500;
            this.LueComp16.Size = new System.Drawing.Size(330, 20);
            this.LueComp16.TabIndex = 46;
            this.LueComp16.ToolTip = "F4 : Show/hide list";
            this.LueComp16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Red;
            this.label64.Location = new System.Drawing.Point(8, 9);
            this.label64.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(77, 14);
            this.label64.TabIndex = 45;
            this.label64.Text = "Competence";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage17
            // 
            this.tabPage17.Controls.Add(this.Grd17);
            this.tabPage17.Controls.Add(this.panel7);
            this.tabPage17.Location = new System.Drawing.Point(4, 23);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage17.Size = new System.Drawing.Size(764, 20);
            this.tabPage17.TabIndex = 16;
            this.tabPage17.Text = "No.17";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // Grd17
            // 
            this.Grd17.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd17.DefaultRow.Height = 20;
            this.Grd17.DefaultRow.Sortable = false;
            this.Grd17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd17.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd17.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd17.Header.Height = 21;
            this.Grd17.Location = new System.Drawing.Point(3, 36);
            this.Grd17.Name = "Grd17";
            this.Grd17.RowHeader.Visible = true;
            this.Grd17.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd17.SingleClickEdit = true;
            this.Grd17.Size = new System.Drawing.Size(758, 0);
            this.Grd17.TabIndex = 52;
            this.Grd17.TreeCol = null;
            this.Grd17.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd17.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd17_RequestEdit);
            this.Grd17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd17_KeyDown);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.TxtScore17);
            this.panel7.Controls.Add(this.label65);
            this.panel7.Controls.Add(this.TxtLevel17);
            this.panel7.Controls.Add(this.label66);
            this.panel7.Controls.Add(this.LueComp17);
            this.panel7.Controls.Add(this.label67);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(758, 33);
            this.panel7.TabIndex = 45;
            // 
            // TxtScore17
            // 
            this.TxtScore17.EnterMoveNextControl = true;
            this.TxtScore17.Location = new System.Drawing.Point(603, 4);
            this.TxtScore17.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore17.Name = "TxtScore17";
            this.TxtScore17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore17.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore17.Properties.Appearance.Options.UseFont = true;
            this.TxtScore17.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore17.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore17.Properties.MaxLength = 12;
            this.TxtScore17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore17.Size = new System.Drawing.Size(86, 20);
            this.TxtScore17.TabIndex = 71;
            this.TxtScore17.Validated += new System.EventHandler(this.TxtScore17_Validated);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(561, 8);
            this.label65.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(38, 14);
            this.label65.TabIndex = 60;
            this.label65.Text = "Score";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel17
            // 
            this.TxtLevel17.EnterMoveNextControl = true;
            this.TxtLevel17.Location = new System.Drawing.Point(481, 5);
            this.TxtLevel17.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel17.Name = "TxtLevel17";
            this.TxtLevel17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel17.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel17.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel17.Properties.MaxLength = 16;
            this.TxtLevel17.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel17.TabIndex = 57;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Red;
            this.label66.Location = new System.Drawing.Point(435, 9);
            this.label66.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(35, 14);
            this.label66.TabIndex = 56;
            this.label66.Text = "Level";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp17
            // 
            this.LueComp17.EnterMoveNextControl = true;
            this.LueComp17.Location = new System.Drawing.Point(90, 6);
            this.LueComp17.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp17.Name = "LueComp17";
            this.LueComp17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp17.Properties.Appearance.Options.UseFont = true;
            this.LueComp17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp17.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp17.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp17.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp17.Properties.DropDownRows = 20;
            this.LueComp17.Properties.NullText = "[Empty]";
            this.LueComp17.Properties.PopupWidth = 500;
            this.LueComp17.Size = new System.Drawing.Size(330, 20);
            this.LueComp17.TabIndex = 46;
            this.LueComp17.ToolTip = "F4 : Show/hide list";
            this.LueComp17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Red;
            this.label67.Location = new System.Drawing.Point(8, 9);
            this.label67.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(77, 14);
            this.label67.TabIndex = 45;
            this.label67.Text = "Competence";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage18
            // 
            this.tabPage18.Controls.Add(this.Grd18);
            this.tabPage18.Controls.Add(this.panel22);
            this.tabPage18.Location = new System.Drawing.Point(4, 23);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage18.Size = new System.Drawing.Size(764, 20);
            this.tabPage18.TabIndex = 17;
            this.tabPage18.Text = "No.18";
            this.tabPage18.UseVisualStyleBackColor = true;
            // 
            // Grd18
            // 
            this.Grd18.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd18.DefaultRow.Height = 20;
            this.Grd18.DefaultRow.Sortable = false;
            this.Grd18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd18.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd18.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd18.Header.Height = 21;
            this.Grd18.Location = new System.Drawing.Point(3, 36);
            this.Grd18.Name = "Grd18";
            this.Grd18.RowHeader.Visible = true;
            this.Grd18.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd18.SingleClickEdit = true;
            this.Grd18.Size = new System.Drawing.Size(758, 0);
            this.Grd18.TabIndex = 52;
            this.Grd18.TreeCol = null;
            this.Grd18.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd18.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd18_RequestEdit);
            this.Grd18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd18_KeyDown);
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel22.Controls.Add(this.TxtScore18);
            this.panel22.Controls.Add(this.label68);
            this.panel22.Controls.Add(this.TxtLevel18);
            this.panel22.Controls.Add(this.label69);
            this.panel22.Controls.Add(this.LueComp18);
            this.panel22.Controls.Add(this.label70);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(3, 3);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(758, 33);
            this.panel22.TabIndex = 45;
            // 
            // TxtScore18
            // 
            this.TxtScore18.EnterMoveNextControl = true;
            this.TxtScore18.Location = new System.Drawing.Point(603, 4);
            this.TxtScore18.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore18.Name = "TxtScore18";
            this.TxtScore18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore18.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore18.Properties.Appearance.Options.UseFont = true;
            this.TxtScore18.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore18.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore18.Properties.MaxLength = 12;
            this.TxtScore18.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore18.Size = new System.Drawing.Size(86, 20);
            this.TxtScore18.TabIndex = 71;
            this.TxtScore18.Validated += new System.EventHandler(this.TxtScore18_Validated);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(561, 8);
            this.label68.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(38, 14);
            this.label68.TabIndex = 60;
            this.label68.Text = "Score";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel18
            // 
            this.TxtLevel18.EnterMoveNextControl = true;
            this.TxtLevel18.Location = new System.Drawing.Point(481, 5);
            this.TxtLevel18.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel18.Name = "TxtLevel18";
            this.TxtLevel18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel18.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel18.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel18.Properties.MaxLength = 16;
            this.TxtLevel18.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel18.TabIndex = 57;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Red;
            this.label69.Location = new System.Drawing.Point(435, 9);
            this.label69.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(35, 14);
            this.label69.TabIndex = 56;
            this.label69.Text = "Level";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp18
            // 
            this.LueComp18.EnterMoveNextControl = true;
            this.LueComp18.Location = new System.Drawing.Point(90, 6);
            this.LueComp18.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp18.Name = "LueComp18";
            this.LueComp18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp18.Properties.Appearance.Options.UseFont = true;
            this.LueComp18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp18.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp18.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp18.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp18.Properties.DropDownRows = 20;
            this.LueComp18.Properties.NullText = "[Empty]";
            this.LueComp18.Properties.PopupWidth = 500;
            this.LueComp18.Size = new System.Drawing.Size(330, 20);
            this.LueComp18.TabIndex = 46;
            this.LueComp18.ToolTip = "F4 : Show/hide list";
            this.LueComp18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Red;
            this.label70.Location = new System.Drawing.Point(8, 9);
            this.label70.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(77, 14);
            this.label70.TabIndex = 45;
            this.label70.Text = "Competence";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage19
            // 
            this.tabPage19.Controls.Add(this.Grd19);
            this.tabPage19.Controls.Add(this.panel23);
            this.tabPage19.Location = new System.Drawing.Point(4, 23);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage19.Size = new System.Drawing.Size(764, 20);
            this.tabPage19.TabIndex = 18;
            this.tabPage19.Text = "No.19";
            this.tabPage19.UseVisualStyleBackColor = true;
            // 
            // Grd19
            // 
            this.Grd19.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd19.DefaultRow.Height = 20;
            this.Grd19.DefaultRow.Sortable = false;
            this.Grd19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd19.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd19.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd19.Header.Height = 21;
            this.Grd19.Location = new System.Drawing.Point(3, 36);
            this.Grd19.Name = "Grd19";
            this.Grd19.RowHeader.Visible = true;
            this.Grd19.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd19.SingleClickEdit = true;
            this.Grd19.Size = new System.Drawing.Size(758, 0);
            this.Grd19.TabIndex = 52;
            this.Grd19.TreeCol = null;
            this.Grd19.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd19.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd19_RequestEdit);
            this.Grd19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd19_KeyDown);
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel23.Controls.Add(this.TxtScore19);
            this.panel23.Controls.Add(this.label71);
            this.panel23.Controls.Add(this.TxtLevel19);
            this.panel23.Controls.Add(this.label72);
            this.panel23.Controls.Add(this.LueComp19);
            this.panel23.Controls.Add(this.label73);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(3, 3);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(758, 33);
            this.panel23.TabIndex = 45;
            // 
            // TxtScore19
            // 
            this.TxtScore19.EnterMoveNextControl = true;
            this.TxtScore19.Location = new System.Drawing.Point(603, 4);
            this.TxtScore19.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore19.Name = "TxtScore19";
            this.TxtScore19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore19.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore19.Properties.Appearance.Options.UseFont = true;
            this.TxtScore19.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore19.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore19.Properties.MaxLength = 12;
            this.TxtScore19.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore19.Size = new System.Drawing.Size(86, 20);
            this.TxtScore19.TabIndex = 71;
            this.TxtScore19.Validated += new System.EventHandler(this.TxtScore19_Validated);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(561, 8);
            this.label71.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(38, 14);
            this.label71.TabIndex = 60;
            this.label71.Text = "Score";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel19
            // 
            this.TxtLevel19.EnterMoveNextControl = true;
            this.TxtLevel19.Location = new System.Drawing.Point(481, 5);
            this.TxtLevel19.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel19.Name = "TxtLevel19";
            this.TxtLevel19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel19.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel19.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel19.Properties.MaxLength = 16;
            this.TxtLevel19.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel19.TabIndex = 57;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Red;
            this.label72.Location = new System.Drawing.Point(435, 9);
            this.label72.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(35, 14);
            this.label72.TabIndex = 56;
            this.label72.Text = "Level";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp19
            // 
            this.LueComp19.EnterMoveNextControl = true;
            this.LueComp19.Location = new System.Drawing.Point(90, 6);
            this.LueComp19.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp19.Name = "LueComp19";
            this.LueComp19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp19.Properties.Appearance.Options.UseFont = true;
            this.LueComp19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp19.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp19.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp19.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp19.Properties.DropDownRows = 20;
            this.LueComp19.Properties.NullText = "[Empty]";
            this.LueComp19.Properties.PopupWidth = 500;
            this.LueComp19.Size = new System.Drawing.Size(330, 20);
            this.LueComp19.TabIndex = 46;
            this.LueComp19.ToolTip = "F4 : Show/hide list";
            this.LueComp19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Red;
            this.label73.Location = new System.Drawing.Point(8, 9);
            this.label73.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(77, 14);
            this.label73.TabIndex = 45;
            this.label73.Text = "Competence";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage20
            // 
            this.tabPage20.Controls.Add(this.Grd20);
            this.tabPage20.Controls.Add(this.panel24);
            this.tabPage20.Location = new System.Drawing.Point(4, 23);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage20.Size = new System.Drawing.Size(764, 20);
            this.tabPage20.TabIndex = 19;
            this.tabPage20.Text = "No.20";
            this.tabPage20.UseVisualStyleBackColor = true;
            // 
            // Grd20
            // 
            this.Grd20.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd20.DefaultRow.Height = 20;
            this.Grd20.DefaultRow.Sortable = false;
            this.Grd20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd20.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd20.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd20.Header.Height = 21;
            this.Grd20.Location = new System.Drawing.Point(3, 36);
            this.Grd20.Name = "Grd20";
            this.Grd20.RowHeader.Visible = true;
            this.Grd20.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd20.SingleClickEdit = true;
            this.Grd20.Size = new System.Drawing.Size(758, 0);
            this.Grd20.TabIndex = 52;
            this.Grd20.TreeCol = null;
            this.Grd20.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd20.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd20_RequestEdit);
            this.Grd20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd20_KeyDown);
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel24.Controls.Add(this.TxtScore20);
            this.panel24.Controls.Add(this.label74);
            this.panel24.Controls.Add(this.TxtLevel20);
            this.panel24.Controls.Add(this.label75);
            this.panel24.Controls.Add(this.LueComp20);
            this.panel24.Controls.Add(this.label76);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(3, 3);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(758, 33);
            this.panel24.TabIndex = 45;
            // 
            // TxtScore20
            // 
            this.TxtScore20.EnterMoveNextControl = true;
            this.TxtScore20.Location = new System.Drawing.Point(603, 4);
            this.TxtScore20.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScore20.Name = "TxtScore20";
            this.TxtScore20.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScore20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScore20.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScore20.Properties.Appearance.Options.UseFont = true;
            this.TxtScore20.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScore20.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScore20.Properties.MaxLength = 12;
            this.TxtScore20.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtScore20.Size = new System.Drawing.Size(86, 20);
            this.TxtScore20.TabIndex = 71;
            this.TxtScore20.Validated += new System.EventHandler(this.TxtScore20_Validated);
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(561, 8);
            this.label74.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(38, 14);
            this.label74.TabIndex = 60;
            this.label74.Text = "Score";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevel20
            // 
            this.TxtLevel20.EnterMoveNextControl = true;
            this.TxtLevel20.Location = new System.Drawing.Point(481, 5);
            this.TxtLevel20.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel20.Name = "TxtLevel20";
            this.TxtLevel20.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel20.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel20.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel20.Properties.MaxLength = 16;
            this.TxtLevel20.Size = new System.Drawing.Size(67, 20);
            this.TxtLevel20.TabIndex = 57;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Red;
            this.label75.Location = new System.Drawing.Point(435, 9);
            this.label75.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(35, 14);
            this.label75.TabIndex = 56;
            this.label75.Text = "Level";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp20
            // 
            this.LueComp20.EnterMoveNextControl = true;
            this.LueComp20.Location = new System.Drawing.Point(90, 6);
            this.LueComp20.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp20.Name = "LueComp20";
            this.LueComp20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp20.Properties.Appearance.Options.UseFont = true;
            this.LueComp20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp20.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp20.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp20.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp20.Properties.DropDownRows = 20;
            this.LueComp20.Properties.NullText = "[Empty]";
            this.LueComp20.Properties.PopupWidth = 500;
            this.LueComp20.Size = new System.Drawing.Size(330, 20);
            this.LueComp20.TabIndex = 46;
            this.LueComp20.ToolTip = "F4 : Show/hide list";
            this.LueComp20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Red;
            this.label76.Location = new System.Drawing.Point(8, 9);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(77, 14);
            this.label76.TabIndex = 45;
            this.label76.Text = "Competence";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(14, 183);
            this.label77.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(99, 14);
            this.label77.TabIndex = 49;
            this.label77.Text = "Training Request";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnTraining
            // 
            this.BtnTraining.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnTraining.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnTraining.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTraining.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnTraining.Appearance.Options.UseBackColor = true;
            this.BtnTraining.Appearance.Options.UseFont = true;
            this.BtnTraining.Appearance.Options.UseForeColor = true;
            this.BtnTraining.Appearance.Options.UseTextOptions = true;
            this.BtnTraining.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnTraining.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnTraining.Image = ((System.Drawing.Image)(resources.GetObject("BtnTraining.Image")));
            this.BtnTraining.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnTraining.Location = new System.Drawing.Point(117, 181);
            this.BtnTraining.Name = "BtnTraining";
            this.BtnTraining.Size = new System.Drawing.Size(24, 21);
            this.BtnTraining.TabIndex = 50;
            this.BtnTraining.ToolTip = "Create Training Request";
            this.BtnTraining.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnTraining.ToolTipTitle = "Run System";
            this.BtnTraining.Click += new System.EventHandler(this.BtnTraining_Click);
            // 
            // FrmAssesmentProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 461);
            this.Name = "FrmAssesmentProcess";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptName2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompetenceLevelName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsmCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsmName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.tabPage15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).EndInit();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp15.Properties)).EndInit();
            this.tabPage14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).EndInit();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp14.Properties)).EndInit();
            this.tabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp13.Properties)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp12.Properties)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp11.Properties)).EndInit();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd10)).EndInit();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp10.Properties)).EndInit();
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp9.Properties)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp8.Properties)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp7.Properties)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp6.Properties)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp5.Properties)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp4.Properties)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp3.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp2.Properties)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp1.Properties)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp16.Properties)).EndInit();
            this.tabPage17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp17.Properties)).EndInit();
            this.tabPage18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd18)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp18.Properties)).EndInit();
            this.tabPage19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd19)).EndInit();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp19.Properties)).EndInit();
            this.tabPage20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd20)).EndInit();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScore20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp20.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtAsmCode;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        protected System.Windows.Forms.Panel panel8;
        private DevExpress.XtraEditors.LookUpEdit LueComp1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tabPage2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected System.Windows.Forms.Panel panel9;
        private DevExpress.XtraEditors.LookUpEdit LueComp2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabPage tabPage3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        protected System.Windows.Forms.Panel panel10;
        private DevExpress.XtraEditors.LookUpEdit LueComp3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage tabPage4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        protected System.Windows.Forms.Panel panel11;
        private DevExpress.XtraEditors.LookUpEdit LueComp4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TabPage tabPage5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        protected System.Windows.Forms.Panel panel12;
        private DevExpress.XtraEditors.LookUpEdit LueComp5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TabPage tabPage6;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        protected System.Windows.Forms.Panel panel13;
        private DevExpress.XtraEditors.LookUpEdit LueComp6;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TabPage tabPage7;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        protected System.Windows.Forms.Panel panel14;
        private DevExpress.XtraEditors.LookUpEdit LueComp7;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TabPage tabPage8;
        protected internal TenTec.Windows.iGridLib.iGrid Grd8;
        protected System.Windows.Forms.Panel panel15;
        private DevExpress.XtraEditors.LookUpEdit LueComp8;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TabPage tabPage9;
        protected internal TenTec.Windows.iGridLib.iGrid Grd9;
        protected System.Windows.Forms.Panel panel16;
        private DevExpress.XtraEditors.LookUpEdit LueComp9;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TabPage tabPage10;
        protected internal TenTec.Windows.iGridLib.iGrid Grd10;
        protected System.Windows.Forms.Panel panel17;
        private DevExpress.XtraEditors.LookUpEdit LueComp10;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TabPage tabPage11;
        protected internal TenTec.Windows.iGridLib.iGrid Grd11;
        protected System.Windows.Forms.Panel panel5;
        private DevExpress.XtraEditors.LookUpEdit LueComp11;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabPage tabPage12;
        protected internal TenTec.Windows.iGridLib.iGrid Grd12;
        protected System.Windows.Forms.Panel panel18;
        private DevExpress.XtraEditors.LookUpEdit LueComp12;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TabPage tabPage13;
        protected internal TenTec.Windows.iGridLib.iGrid Grd13;
        protected System.Windows.Forms.Panel panel19;
        private DevExpress.XtraEditors.LookUpEdit LueComp13;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TabPage tabPage14;
        protected internal TenTec.Windows.iGridLib.iGrid Grd14;
        protected System.Windows.Forms.Panel panel20;
        private DevExpress.XtraEditors.LookUpEdit LueComp14;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TabPage tabPage15;
        protected internal TenTec.Windows.iGridLib.iGrid Grd15;
        protected System.Windows.Forms.Panel panel21;
        private DevExpress.XtraEditors.LookUpEdit LueComp15;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtAsmName;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.SimpleButton BtnAssement;
        public DevExpress.XtraEditors.SimpleButton BtnAssesment2;
        internal DevExpress.XtraEditors.TextEdit TxtCompetenceLevelName;
        protected System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label9;
        public DevExpress.XtraEditors.TextEdit TxtPosition;
        private System.Windows.Forms.Label label7;
        public DevExpress.XtraEditors.TextEdit TxtDeptName;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.TextEdit TxtEmpName;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode;
        public DevExpress.XtraEditors.TextEdit TxtEmpCode;
        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.TextEdit TxtPosition2;
        private System.Windows.Forms.Label label10;
        public DevExpress.XtraEditors.TextEdit TxtDeptName2;
        private System.Windows.Forms.Label label12;
        public DevExpress.XtraEditors.TextEdit TxtEmpName2;
        private System.Windows.Forms.Label label14;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode2;
        public DevExpress.XtraEditors.TextEdit TxtEmpCode2;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        public DevExpress.XtraEditors.DateEdit DteJoinDt;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        public DevExpress.XtraEditors.TextEdit TxtLevel1;
        public DevExpress.XtraEditors.TextEdit TxtLevel2;
        public DevExpress.XtraEditors.TextEdit TxtLevel3;
        public DevExpress.XtraEditors.TextEdit TxtLevel4;
        public DevExpress.XtraEditors.TextEdit TxtLevel5;
        public DevExpress.XtraEditors.TextEdit TxtLevel6;
        public DevExpress.XtraEditors.TextEdit TxtLevel7;
        public DevExpress.XtraEditors.TextEdit TxtLevel8;
        public DevExpress.XtraEditors.TextEdit TxtLevel9;
        public DevExpress.XtraEditors.TextEdit TxtLevel10;
        public DevExpress.XtraEditors.TextEdit TxtLevel11;
        public DevExpress.XtraEditors.TextEdit TxtLevel12;
        public DevExpress.XtraEditors.TextEdit TxtLevel13;
        public DevExpress.XtraEditors.TextEdit TxtLevel14;
        public DevExpress.XtraEditors.TextEdit TxtLevel15;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        internal DevExpress.XtraEditors.TextEdit TxtScore1;
        internal DevExpress.XtraEditors.TextEdit TxtScore2;
        internal DevExpress.XtraEditors.TextEdit TxtScore3;
        internal DevExpress.XtraEditors.TextEdit TxtScore4;
        internal DevExpress.XtraEditors.TextEdit TxtScore5;
        internal DevExpress.XtraEditors.TextEdit TxtScore6;
        internal DevExpress.XtraEditors.TextEdit TxtScore7;
        internal DevExpress.XtraEditors.TextEdit TxtScore8;
        internal DevExpress.XtraEditors.TextEdit TxtScore9;
        internal DevExpress.XtraEditors.TextEdit TxtScore10;
        internal DevExpress.XtraEditors.TextEdit TxtScore11;
        internal DevExpress.XtraEditors.TextEdit TxtScore12;
        internal DevExpress.XtraEditors.TextEdit TxtScore13;
        internal DevExpress.XtraEditors.TextEdit TxtScore14;
        internal DevExpress.XtraEditors.TextEdit TxtScore15;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.TabPage tabPage18;
        private System.Windows.Forms.TabPage tabPage19;
        private System.Windows.Forms.TabPage tabPage20;
        protected internal TenTec.Windows.iGridLib.iGrid Grd16;
        protected System.Windows.Forms.Panel panel6;
        internal DevExpress.XtraEditors.TextEdit TxtScore16;
        private System.Windows.Forms.Label label62;
        public DevExpress.XtraEditors.TextEdit TxtLevel16;
        private System.Windows.Forms.Label label63;
        private DevExpress.XtraEditors.LookUpEdit LueComp16;
        private System.Windows.Forms.Label label64;
        protected internal TenTec.Windows.iGridLib.iGrid Grd17;
        protected System.Windows.Forms.Panel panel7;
        internal DevExpress.XtraEditors.TextEdit TxtScore17;
        private System.Windows.Forms.Label label65;
        public DevExpress.XtraEditors.TextEdit TxtLevel17;
        private System.Windows.Forms.Label label66;
        private DevExpress.XtraEditors.LookUpEdit LueComp17;
        private System.Windows.Forms.Label label67;
        protected internal TenTec.Windows.iGridLib.iGrid Grd18;
        protected System.Windows.Forms.Panel panel22;
        internal DevExpress.XtraEditors.TextEdit TxtScore18;
        private System.Windows.Forms.Label label68;
        public DevExpress.XtraEditors.TextEdit TxtLevel18;
        private System.Windows.Forms.Label label69;
        private DevExpress.XtraEditors.LookUpEdit LueComp18;
        private System.Windows.Forms.Label label70;
        protected internal TenTec.Windows.iGridLib.iGrid Grd19;
        protected System.Windows.Forms.Panel panel23;
        internal DevExpress.XtraEditors.TextEdit TxtScore19;
        private System.Windows.Forms.Label label71;
        public DevExpress.XtraEditors.TextEdit TxtLevel19;
        private System.Windows.Forms.Label label72;
        private DevExpress.XtraEditors.LookUpEdit LueComp19;
        private System.Windows.Forms.Label label73;
        protected internal TenTec.Windows.iGridLib.iGrid Grd20;
        protected System.Windows.Forms.Panel panel24;
        internal DevExpress.XtraEditors.TextEdit TxtScore20;
        private System.Windows.Forms.Label label74;
        public DevExpress.XtraEditors.TextEdit TxtLevel20;
        private System.Windows.Forms.Label label75;
        private DevExpress.XtraEditors.LookUpEdit LueComp20;
        private System.Windows.Forms.Label label76;
        public DevExpress.XtraEditors.SimpleButton BtnTraining;
        private System.Windows.Forms.Label label77;
    }
}