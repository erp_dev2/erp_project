﻿namespace RunSystem
{
    partial class FrmBase6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBase6));
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnChart = new DevExpress.XtraEditors.SimpleButton();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkAutoWidth = new DevExpress.XtraEditors.CheckEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.BtnExcel = new DevExpress.XtraEditors.SimpleButton();
            this.BtnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.BtnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.PM1 = new TenTec.Windows.iGridLib.Printing.iGPrintManager();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAutoWidth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SteelBlue;
            this.panel1.Controls.Add(this.BtnChart);
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkAutoWidth);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Controls.Add(this.BtnExcel);
            this.panel1.Controls.Add(this.BtnPrint);
            this.panel1.Controls.Add(this.BtnRefresh);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(772, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(70, 473);
            this.panel1.TabIndex = 0;
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnChart.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnChart.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnChart.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnChart.Location = new System.Drawing.Point(0, 93);
            this.BtnChart.Name = "BtnChart";
            this.BtnChart.Size = new System.Drawing.Size(70, 31);
            this.BtnChart.TabIndex = 7;
            this.BtnChart.Text = "  C&hart";
            this.BtnChart.ToolTip = "Generate Chart (Alt-H)";
            this.BtnChart.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnChart.ToolTipTitle = "Run System";
            this.BtnChart.Click += new System.EventHandler(this.BtnChart_Click);
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 409);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 4;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            this.LueFontSize.EditValueChanged += new System.EventHandler(this.LueFontSize_EditValueChanged);
            // 
            // ChkAutoWidth
            // 
            this.ChkAutoWidth.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkAutoWidth.Location = new System.Drawing.Point(0, 429);
            this.ChkAutoWidth.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkAutoWidth.Name = "ChkAutoWidth";
            this.ChkAutoWidth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAutoWidth.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkAutoWidth.Properties.Appearance.Options.UseFont = true;
            this.ChkAutoWidth.Properties.Appearance.Options.UseForeColor = true;
            this.ChkAutoWidth.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAutoWidth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkAutoWidth.Properties.Caption = "Resize";
            this.ChkAutoWidth.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAutoWidth.Size = new System.Drawing.Size(70, 22);
            this.ChkAutoWidth.TabIndex = 5;
            this.ChkAutoWidth.ToolTip = "Set Automatic Report\'s Column Width";
            this.ChkAutoWidth.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAutoWidth.ToolTipTitle = "Run System";
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 451);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 6;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnExcel.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnExcel.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnExcel.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnExcel.Location = new System.Drawing.Point(0, 62);
            this.BtnExcel.Name = "BtnExcel";
            this.BtnExcel.Size = new System.Drawing.Size(70, 31);
            this.BtnExcel.TabIndex = 3;
            this.BtnExcel.Text = "  &Excel";
            this.BtnExcel.ToolTip = "Export To Excel (Alt-E)";
            this.BtnExcel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnExcel.ToolTipTitle = "Run System";
            this.BtnExcel.Click += new System.EventHandler(this.BtnExcel_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPrint.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnPrint.Location = new System.Drawing.Point(0, 31);
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(70, 31);
            this.BtnPrint.TabIndex = 2;
            this.BtnPrint.Text = "  &Print";
            this.BtnPrint.ToolTip = "Print Data - Preview (Alt-P)";
            this.BtnPrint.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPrint.ToolTipTitle = "Run System";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRefresh.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRefresh.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnRefresh.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnRefresh.Location = new System.Drawing.Point(0, 0);
            this.BtnRefresh.Name = "BtnRefresh";
            this.BtnRefresh.Size = new System.Drawing.Size(70, 31);
            this.BtnRefresh.TabIndex = 1;
            this.BtnRefresh.Text = "  &Refresh";
            this.BtnRefresh.ToolTip = "Refresh Data (Alt-R)";
            this.BtnRefresh.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRefresh.ToolTipTitle = "Run System";
            this.BtnRefresh.Click += new System.EventHandler(this.BtnRefresh_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(772, 74);
            this.panel2.TabIndex = 7;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.Grd1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 74);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 399);
            this.panel3.TabIndex = 8;
            // 
            // Grd1
            // 
            this.Grd1.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd1.BackColorOddRows = System.Drawing.Color.White;
            this.Grd1.CurCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 19;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd1.Name = "Grd1";
            this.Grd1.ProcessTab = false;
            this.Grd1.ReadOnly = true;
            this.Grd1.RowMode = true;
            this.Grd1.RowModeHasCurCell = true;
            this.Grd1.RowTextStartColNear = 3;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(772, 399);
            this.Grd1.TabIndex = 9;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterRowStateChanged += new TenTec.Windows.iGridLib.iGAfterRowStateChangedEventHandler(this.Grd1_AfterRowStateChanged);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd1_ColHdrDoubleClick);
            this.Grd1.AfterContentsSorted += new System.EventHandler(this.Grd1_AfterContentsSorted);
            this.Grd1.AfterContentsGrouped += new System.EventHandler(this.Grd1_AfterContentsGrouped);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // PM1
            // 
            this.PM1.PrintPreviewSettings.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.PM1.PrintPreviewSettings.Zoom = TenTec.Windows.iGridLib.Printing.iGPrintPreviewZoom.Percent100;
            this.PM1.TreeLineColor = System.Drawing.Color.Empty;
            this.PM1.CustomDrawPageHeaderGetBounds += new TenTec.Windows.iGridLib.Printing.iGCustomDrawPageBandGetBoundsEventHandler(this.PM1_CustomDrawPageHeaderGetBounds);
            this.PM1.CustomDrawPageHeader += new TenTec.Windows.iGridLib.Printing.iGCustomDrawPageBandEventHandler(this.PM1_CustomDrawPageHeader);
            // 
            // FrmBase6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmBase6";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.FrmBase6_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAutoWidth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel1;
        protected DevExpress.XtraEditors.SimpleButton BtnExcel;
        protected DevExpress.XtraEditors.SimpleButton BtnPrint;
        protected DevExpress.XtraEditors.SimpleButton BtnRefresh;
        protected System.Windows.Forms.Panel panel2;
        internal DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected DevExpress.XtraEditors.SimpleButton BtnChart;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        protected internal System.Windows.Forms.Panel panel3;
        internal TenTec.Windows.iGridLib.Printing.iGPrintManager PM1;
        internal DevExpress.XtraEditors.CheckEdit ChkAutoWidth;
    }
}