﻿#region Update
/*
    17/02/2021 [WED/IMS] new apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherSpecialDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherSpecial mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherSpecialDlg(FrmVoucherSpecial FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                Sl.SetLueVoucherDocType(ref LueDocType);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Document#",
                        "", 
                        "Date",
                        "Local#",
                        "Type",
                        
                        //6-10
                        "Amount",
                        "Department",
                        "Person In Charge",
                        "Bank Account",
                        "Remark",

                        //11-12
                        "Due Date",
                        "DocType"
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 12 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, C.LocalDocNo, C.DocType, D.OptDesc As DocTypeDesc, Sum(C.Amt) Amt, C.DueDt, A.Remark, ");
            SQL.AppendLine("    Null As VdName, Group_Concat(Distinct E.DeptName) DeptName, F.UserName, ");
            SQL.AppendLine("    Concat( ");
            SQL.AppendLine("        Case When H.BankName Is Not Null Then Concat(H.BankName, ' : ') Else '' End, ");
            SQL.AppendLine("        Case When G.BankAcNo Is Not Null ");
            SQL.AppendLine("            Then Concat(G.BankAcNo, ' [', IfNull(G.BankAcNm, ''), ']') ");
            SQL.AppendLine("            Else IfNull(G.BankAcNm, '') End ");
            SQL.AppendLine("    ) As BankAcDesc, A.CreateDt ");
            SQL.AppendLine("    From TblVoucherRequestSpecialHdr A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestSpecialDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        And A.CancelInd = 'N' ");
            SQL.AppendLine("        And A.`Status` = 'A' ");
            SQL.AppendLine("        And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        And B.VoucherRequestDocNo Not In (Select VoucherRequestDocNo From TblVoucherHdr Where CancelInd = 'N') ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr C On B.VoucherRequestDocNo = C.DocNo ");
            SQL.AppendLine("        And C.CancelInd = 'N' ");
            if (mFrmParent.mIsVoucherFilteredByDept)
            {
                SQL.AppendLine("        And (C.DeptCode Is Null Or (C.DeptCode Is Not Null ");
                SQL.AppendLine("        And Exists( ");
                SQL.AppendLine("            Select 1 From TblGroupDepartment ");
                SQL.AppendLine("            Where DeptCode=C.DeptCode ");
                SQL.AppendLine("            And GrpCode In ( ");
                SQL.AppendLine("                Select GrpCode From TblUser ");
                SQL.AppendLine("                Where UserCode=@UserCode ");
                SQL.AppendLine("        )))) ");
            }
            if (mFrmParent.mIsVoucherBankAccountFilteredByGrp)
            {
                SQL.AppendLine("        And (C.BankAcCode Is Null ");
                SQL.AppendLine("        Or (C.BankAcCode Is Not Null And Exists( ");
                SQL.AppendLine("            Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("            Where BankAcCode=C.BankAcCode ");
                SQL.AppendLine("            And GrpCode In ( ");
                SQL.AppendLine("                Select GrpCode From TblUser ");
                SQL.AppendLine("                Where UserCode=@UserCode ");
                SQL.AppendLine("        )))) ");
            }
            SQL.AppendLine("    Inner Join TblOption D On C.DocType = D.OptCode And D.OptCat = 'VoucherDocType' ");
            SQL.AppendLine("    Left Join TblDepartment E On C.DeptCode = E.DeptCode ");
            SQL.AppendLine("    Left Join TblUser F On C.PIC = F.UserCode ");
            SQL.AppendLine("    Left Join TblBankAccount G On C.BankAcCode = G.BankAcCode ");
            SQL.AppendLine("    Left Join TblBank H On G.BankCode = H.BankCode ");
            SQL.AppendLine("    Group By A.DocNo, A.DocDt, C.LocalDocNo, C.DocType, D.OptDesc, C.DueDt, A.Remark, ");
            SQL.AppendLine("    F.UserName, H.BankName, G.BankAcNo, G.BankAcNm, A.CreateDt ");

            SQL.AppendLine(") T ");
          
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                if (mFrmParent.mIsVoucherFilteredByDept)
                {
                    Sm.CmParam<String>(ref cm, "@VoucherDocTypeManual", mFrmParent.mVoucherDocTypeManual);
                }
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "DocType", true);
                Sm.FilterStr(ref Filter, ref cm, TxtVdCode.Text, "VdName", false);
                Sm.FilterStr(ref Filter, ref cm, TxtRemark.Text, "Remark", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By CreateDt Desc;",
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "LocalDocNo", "DocTypeDesc", "Amt", "DeptName",

                        //6-10
                        "UserName", "BankAcDesc", "Remark", "DueDt", "DocType"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    }, true, false, false, false
                );
                Grd1.Cols.AutoWidth();
                Grd1.Rows.AutoHeight();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowVoucherRequestInfo(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
            else
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least one voucher request special.");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                string MenuCode = string.Empty;

                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12) == "65") MenuCode = Sm.GetParameter("MenuCodeForVRSOT");
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12) == "66") MenuCode = Sm.GetParameter("MenuCodeForVRSRHA");
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12) == "67") MenuCode = Sm.GetParameter("MenuCodeForVRSIncentive");

                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length == 0) MenuCode = Sm.GetParameter("MenuCodeForVRSRHA");

                var f = new FrmVoucherRequestSpecial(MenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
                
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                string MenuCode = string.Empty;

                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12) == "65") MenuCode = Sm.GetParameter("MenuCodeForVRSOT");
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12) == "66") MenuCode = Sm.GetParameter("MenuCodeForVRSRHA");
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12) == "67") MenuCode = Sm.GetParameter("MenuCodeForVRSIncentive");

                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length == 0) MenuCode = Sm.GetParameter("MenuCodeForVRSRHA");

                var f = new FrmVoucherRequestSpecial(MenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }
        
        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(Sl.SetLueVoucherDocType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Document's type");
        }


        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor");
        }

        private void TxtVdCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtRemark_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkRemark_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Remark");
        }

        #endregion

        #endregion
    }
}
