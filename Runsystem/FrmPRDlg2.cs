﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPRDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPR mFrmParent;
        private string mSQL = string.Empty, mMakeToStockDocNo = string.Empty;
        private bool IsFirst = true;

        #endregion

        #region Constructor

        public FrmPRDlg2(FrmPR FrmParent, string MakeToStockDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mMakeToStockDocNo = MakeToStockDocNo;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.ItCode, C.ItName, C.ItCodeInternal, ");
            SQL.AppendLine("B.Qty-IfNull(D.Qty1, 0) As OutstandingQty, C.PlanningUomCode2, ");
            SQL.AppendLine("IfNull(E.Stock, 0) As Stock, C.InventoryUomCode, ");
            SQL.AppendLine("C.PurchaseUomCode, B.UsageDt ");
            SQL.AppendLine("From TblMakeToStockHdr A ");
            SQL.AppendLine("Inner Join TblMakeToStockDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And IfNull(B.ProcessInd2, 'O')<>'F' ");
            SQL.AppendLine("    And Locate(Concat('##', B.DNo, '##'), @SelectedMakeToStockDNo)<1 ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.MakeToStockDNo As DNo, Sum(T2.Qty1) As Qty1 ");
            SQL.AppendLine("    From TblPRHdr T1 ");
            SQL.AppendLine("    Inner Join TblPRDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("    Where T1.MakeToStockDocNo=@MakeToStockDocNo ");
            SQL.AppendLine("    Group By T2.MakeToStockDNo ");
            SQL.AppendLine(") D On B.DNo=D.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.ItCode, Sum(A.Qty) As Stock ");
            SQL.AppendLine("    From TblStockSummary A ");
            SQL.AppendLine("    Inner Join TblMakeToStockDtl B On A.ItCode=B.ItCode And B.DocNo=@MakeToStockDocNo ");
            SQL.AppendLine("    Where A.Qty<>0 ");
            SQL.AppendLine("    Group By A.ItCode ");
            SQL.AppendLine(") E On B.ItCode=E.ItCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@MakeToStockDocNo ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "DNo",
                        "Item's Code",
                        "", 
                        "Item's Name",
 
                        //6-10
                        "Local Code", 
                        "Outstanding" + Environment.NewLine + "MTS Quantity",
                        "Planning" + Environment.NewLine + "UoM",
                        "Stock",
                        "Inventory" + Environment.NewLine + "UoM",
                        
                        //11-12
                        "Purchase" + Environment.NewLine + "UoM",
                        "Usage" + Environment.NewLine + "Date"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 0, 100, 20, 250, 
                        
                        //6-10
                        100, 100, 80, 100, 80,  
                        
                        //11-12
                        0, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 12 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 11 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@MakeToStockDocNo", mMakeToStockDocNo);
                Sm.CmParam<String>(ref cm, "@SelectedMakeToStockDNo", mFrmParent.GetSelectedMakeToStockDNo());
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "C.ItCodeInternal", "C.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By B.DNo;",
                        new string[] 
                        { 
                            //0
                            "DNo", 

                            //1-5
                            "ItCode", "ItName", "ItCodeInternal", "OutstandingQty", "PlanningUomCode2", 
                            
                            //6-9
                            "Stock", "InventoryUomCode", "PurchaseUomCode", "UsageDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = true;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 9);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            mFrmParent.Grd1.BeginUpdate();
            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDNoAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 6);

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 12);
                        if (Sm.CompareGrdStr(Grd1, Row2, 8, Grd1, Row2, 11))
                            mFrmParent.Grd1.Cells[Row1, 14].Value = Sm.GetGrdDec(mFrmParent.Grd1, Row1, 10);
                        else
                            Sm.ComputeQtyBasedOnConvertionFormula("12", mFrmParent.Grd1, Row1, 5, 10, 14, 11, 15);
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 9, 10, 12, 14 });
                    }
                }
            }
            mFrmParent.Grd1.EndUpdate();
            if (!IsChoose)
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
            else
                IsFirst = false;

        }

        private bool IsDNoAlreadyChosen(int Row)
        {
            if (IsFirst) return false;
            var Key = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (Sm.CompareStr(Key, Sm.GetGrdStr(mFrmParent.Grd1, Index, 4)))
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 3));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 3));
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
