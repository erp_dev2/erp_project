﻿#region Update
/*
    24/05/2017 [WED] tambah filter berdasarkan Item Category
    24/05/2017 [WED] fixing price amount
    27/10/2017 [WED] tambah group by source
    30/10/2017 [WED] disamakan nilainya dengan Stock Summary With Price
    10/11/2017 [WED] disamakan nilai In dan Out dengan Stock Movement
    02/07/2017 [TKG] tambah filter berdasarkan item category
    07/02/2019 [WED] tambah condition Qty <> 0 di query A
 *  29/05/2019 [DITA] command condition Qty <> 0 di query A
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthlyStockMovementAccumulated : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        int mNumberOfInventoryUomCode = 1;
        private bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptMonthlyStockMovementAccumulated(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            GetParameter();
            SetGrd();
            string CurrentDateTime = Sm.ServerCurrentDateTime();
            Sl.SetLueMth(LueMth);
            Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
            Sl.SetLueYr(LueYr, "");
            Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
            Sl.SetLueWhsCode(ref LueWhsCode);
            Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt ? "Y" : "N");

            base.FrmLoad(sender, e);
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode(); 
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.ItCode, T.ItName, ");
            SQL.AppendLine("T.InventoryUomCode, T.InventoryUomCode2, T.InventoryUomCode3, ");
            SQL.AppendLine("Sum(T.Init) As QtyInit1, ");
            SQL.AppendLine("Sum(T.Init2) As QtyInit2, ");
            SQL.AppendLine("Sum(T.Init3) As QtyInit3, ");
            SQL.AppendLine("Sum(T.TInitPrice) As InitPrice, ");
            SQL.AppendLine("Sum(T.I) As QtyI1, ");
            SQL.AppendLine("Sum(T.I2) As QtyI2, ");
            SQL.AppendLine("Sum(T.I3) As QtyI3, ");
            SQL.AppendLine("Sum(T.TIPrice) As IPrice, ");
            SQL.AppendLine("Sum(T.O) QtyO1, ");
            SQL.AppendLine("Sum(T.O2) QtyO2, ");
            SQL.AppendLine("Sum(T.O3) QtyO3, ");
            SQL.AppendLine("Sum(T.TOPrice) OPrice ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.ItCode, E.ItName, ");
            SQL.AppendLine("    E.InventoryUomCode, E.InventoryUomCode2, E.InventoryUomCode3, ");
            SQL.AppendLine("    IfNull(B.Init, 0.00) As Init, ");
            SQL.AppendLine("    IfNull(B.Init2, 0.00) As Init2, ");
            SQL.AppendLine("    IfNull(B.Init3, 0.00) As Init3, ");
            SQL.AppendLine("    IfNull(B.TInitPrice, 0.00) As TInitPrice, ");
            SQL.AppendLine("    IfNull(C.I, 0.00) As I, ");
            SQL.AppendLine("    IfNull(C.I2, 0.00) As I2, ");
            SQL.AppendLine("    IfNull(C.I3, 0.00) As I3, ");
            SQL.AppendLine("    IfNull(C.TIPrice, 0.00) As TIPrice, ");
            SQL.AppendLine("    IfNull(D.O, 0.00) As O, ");
            SQL.AppendLine("    IfNull(D.O2, 0.00) As O2, ");
            SQL.AppendLine("    IfNull(D.O3, 0.00) As O3, ");
            SQL.AppendLine("    IfNull(D.TOPrice, 0.00) As TOPrice ");
            SQL.AppendLine("    From ( ");
	        SQL.AppendLine("        Select Distinct ItCode ");
	        SQL.AppendLine("        From TblStockSummary ");
	        SQL.AppendLine("        Where WhsCode=@WhsCode ");
            //SQL.AppendLine("        And Qty <> 0 "); // tambahan karna ada item yang tidak cocok di Stock Summary, 07/02/2019
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("    And ItCode In (");
                SQL.AppendLine("        Select T.ItCode From TblItem T ");
                SQL.AppendLine("        Where Exists( ");
                SQL.AppendLine("            Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("            Where ItCtCode=T.ItCtCode ");
                SQL.AppendLine("            And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("    ) ");
            }
	        SQL.AppendLine("    ) A -- dapatkan item dari StockMovement selama satu bulan ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Tbl.ItCode, Sum(Tbl.Qty) As Init, Sum(Tbl.Qty2) As Init2, Sum(Tbl.Qty3) As Init3, Sum(Tbl.Total) As TInitPrice ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select T1.ItCode, T1.Qty, T1.Qty2, T1.Qty3, (T1.Qty * T2.ExcRate * T2.UPrice) As Total ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select A.ItCode, A.Source, ");
            SQL.AppendLine("                Sum(A.Qty) Qty, Sum(A.Qty2) Qty2, Sum(A.Qty3) Qty3 ");
            SQL.AppendLine("                From TblStockMovement A ");
            SQL.AppendLine("                Where Left(A.DocDt, 6)<Concat(@Yr, @Mth) ");
            SQL.AppendLine("                And A.WhsCode=@WhsCode ");
            SQL.AppendLine("                Group By A.ItCode, A.Source ");
            SQL.AppendLine("                Having Sum(A.Qty)<>0.00 ");
            SQL.AppendLine("            ) T1 ");
            SQL.AppendLine("            Inner Join TblStockPrice T2 On T1.Source = T2.Source ");
            SQL.AppendLine("        ) Tbl ");
            SQL.AppendLine("        Group By Tbl.ItCode ");
            SQL.AppendLine("    ) B On A.ItCode = B.ItCode -- initial stock ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.ItCode, ");
            SQL.AppendLine("        Sum(T1.Qty) As I, Sum(T1.Qty2) As I2, Sum(T1.Qty3) As I3, ");
            SQL.AppendLine("        Sum(T1.Qty*T2.ExcRate*T2.UPrice) As TIPrice ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select DocType, DocNo, DNo, Source, ItCode, ");
            SQL.AppendLine("            Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3 ");
            SQL.AppendLine("            From TblStockMovement ");
            SQL.AppendLine("            Where Left(DocDt, 6)=Concat(@Yr, @Mth) ");
            SQL.AppendLine("            And WhsCode=@WhsCode ");
            SQL.AppendLine("            Group By DocType, DocNo, DNo, Source, ItCode ");
            SQL.AppendLine("            Having Sum(Qty)>0.00 ");
            SQL.AppendLine("        ) T1 ");
            SQL.AppendLine("        Inner Join TblStockPrice T2 On T1.Source=T2.Source ");
            SQL.AppendLine("        Group By T1.ItCode ");
            SQL.AppendLine("    ) C On A.ItCode = C.ItCode -- stock in ");
	        SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.ItCode, ");
            SQL.AppendLine("        Sum(T1.Qty) As O, Sum(T1.Qty2) As O2, Sum(T1.Qty3) As O3, ");
            SQL.AppendLine("        Sum(T1.Qty*T2.ExcRate*T2.UPrice) As TOPrice ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select DocType, DocNo, DNo, Source, ItCode, ");
            SQL.AppendLine("            Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3 ");
            SQL.AppendLine("            From TblStockMovement ");
            SQL.AppendLine("            Where Left(DocDt, 6)=Concat(@Yr, @Mth) ");
            SQL.AppendLine("            And WhsCode=@WhsCode ");
            SQL.AppendLine("            Group By DocType, DocNo, DNo, Source, ItCode ");
            SQL.AppendLine("            Having Sum(Qty)<0.00 ");
            SQL.AppendLine("        ) T1 ");
            SQL.AppendLine("        Inner Join TblStockPrice T2 On T1.Source=T2.Source ");
            SQL.AppendLine("        Group By T1.ItCode ");
            SQL.AppendLine("    ) D On A.ItCode=D.ItCode -- stock out ");
	        SQL.AppendLine("    Inner Join TblItem E On A.ItCode = E.ItCode ");
	        SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode = F.ItCtCode ");
	        SQL.AppendLine("    Where (IfNull(B.Init, 0.00)<>0.00 Or IfNull(C.I, 0.00)<>0.00 Or IfNull(D.O, 0.00)<>0.00) ");

            if(Sm.GetLue(LueItCtCode).Length > 0)
                SQL.AppendLine("    And E.ItCtCode=@ItCtCode ");

            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.ItCode, T.ItName, ");
            SQL.AppendLine("T.InventoryUomCode, T.InventoryUomCode2, T.InventoryUomCode3 ");
            SQL.AppendLine("Order By T.ItName ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 3;

            Grd1.Cols[0].Width = 50;
            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 100;
            Grd1.Header.Cells[0, 1].Value = "Item Code";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;
            
            Grd1.Cols[2].Width = 250;
            Grd1.Header.Cells[0, 2].Value = "Item Name";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Header.Cells[1, 3].Value = "Inventory";
            Grd1.Header.Cells[1, 3].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 3].SpanCols = 3;
            Grd1.Header.Cells[0, 3].Value = "UoM 1";
            Grd1.Header.Cells[0, 4].Value = "UoM 2";
            Grd1.Header.Cells[0, 5].Value = "UoM 3";
            Grd1.Cols[3].Width = 60;
            Grd1.Cols[4].Width = 60;
            Grd1.Cols[5].Width = 60;
            
            Grd1.Header.Cells[1, 6].Value = "Initial";
            Grd1.Header.Cells[1, 6].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 6].SpanCols = 4;
            Grd1.Header.Cells[0, 6].Value = "Quantity 1";
            Grd1.Header.Cells[0, 7].Value = "Quantity 2";
            Grd1.Header.Cells[0, 8].Value = "Quantity 3";
            Grd1.Header.Cells[0, 9].Value = "Amount";
            Grd1.Cols[6].Width = 80;
            Grd1.Cols[7].Width = 80;
            Grd1.Cols[8].Width = 80;
            Grd1.Cols[9].Width = 150;

            Grd1.Header.Cells[1, 10].Value = "In";
            Grd1.Header.Cells[1, 10].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 10].SpanCols = 4;
            Grd1.Header.Cells[0, 10].Value = "Quantity 1";
            Grd1.Header.Cells[0, 11].Value = "Quantity 2";
            Grd1.Header.Cells[0, 12].Value = "Quantity 3";
            Grd1.Header.Cells[0, 13].Value = "Amount";
            Grd1.Cols[10].Width = 80;
            Grd1.Cols[11].Width = 80;
            Grd1.Cols[12].Width = 80;
            Grd1.Cols[13].Width = 150;

            Grd1.Header.Cells[1, 14].Value = "Out";
            Grd1.Header.Cells[1, 14].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 14].SpanCols = 4;
            Grd1.Header.Cells[0, 14].Value = "Quantity 1";
            Grd1.Header.Cells[0, 15].Value = "Quantity 2";
            Grd1.Header.Cells[0, 16].Value = "Quantity 3";
            Grd1.Header.Cells[0, 17].Value = "Amount";
            Grd1.Cols[14].Width = 80;
            Grd1.Cols[15].Width = 80;
            Grd1.Cols[16].Width = 80;
            Grd1.Cols[17].Width = 150;

            Grd1.Header.Cells[1, 18].Value = "Last";
            Grd1.Header.Cells[1, 18].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 18].SpanCols = 4;
            Grd1.Header.Cells[0, 18].Value = "Quantity 1";
            Grd1.Header.Cells[0, 19].Value = "Quantity 2";
            Grd1.Header.Cells[0, 20].Value = "Quantity 3";
            Grd1.Header.Cells[0, 21].Value = "Amount";
            Grd1.Cols[18].Width = 80;
            Grd1.Cols[19].Width = 80;
            Grd1.Cols[20].Width = 80;
            Grd1.Cols[21].Width = 150;

            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 }, 0);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;

            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 5, 7, 8, 11, 12, 15, 16, 19, 20 });
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 4, 7, 11, 15, 19 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 7, 8, 11, 12, 15, 16, 19, 20 }, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode='NumberOfInventoryUomCode'");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueMth, "Month") || Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueWhsCode, "Warehouse")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCtCode));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SetSQL(),
                        new string[]
                        { 
                            //0
                            "ItCode", 
                            
                            //1-5
                            "ItName", "InventoryUomCode", "InventoryUomCode2", "InventoryUomCode3", "QtyInit1", 
                            
                            //6-10
                            "QtyInit2", "QtyInit3", "InitPrice", "QtyI1", "QtyI2", 
                            
                            //11-15
                            "QtyI3", "IPrice", "QtyO1", "QtyO2", "QtyO3", 

                            //16
                            "OPrice"
                        },

                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Grd.Cells[Row, 18].Value = Sm.GetGrdDec(Grd, Row, 6) + Sm.GetGrdDec(Grd, Row, 10) +Sm.GetGrdDec(Grd, Row, 14);
                            Grd.Cells[Row, 19].Value = Sm.GetGrdDec(Grd, Row, 7) + Sm.GetGrdDec(Grd, Row, 11) +Sm.GetGrdDec(Grd, Row, 15);
                            Grd.Cells[Row, 20].Value = Sm.GetGrdDec(Grd, Row, 8) + Sm.GetGrdDec(Grd, Row, 12) +Sm.GetGrdDec(Grd, Row, 16);
                            Grd.Cells[Row, 21].Value = Sm.GetGrdDec(Grd, Row, 9) + Sm.GetGrdDec(Grd, Row, 13) +Sm.GetGrdDec(Grd, Row, 17);
                        }, true, false, false, false
                    );

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                {  
                    6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21
                });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item Category");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        #endregion

        #endregion

    }
}
