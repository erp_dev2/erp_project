﻿#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptCashFlowForecast : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mYr = string.Empty, mMth = string.Empty;
        
        #endregion

        #region Constructor

        public FrmRptCashFlowForecast(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueCurCode(ref LueCurCode);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Type, A.BankAcNm As Description, A.CurCode, Sum(B.Amt) As Amt ");
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T2.BankAcCode, T2.Amt ");
            SQL.AppendLine("    From TblClosingBalanceInCashHdr T1 ");
            SQL.AppendLine("    Inner Join TblClosingBalanceInCashDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.Yr=@Yr And T1.Mth=@Mth ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select BankAcCode, Case AcType When 'C' Then -1 Else 1 End * Amt As Amt ");
            SQL.AppendLine("    From TblVoucherHdr  ");
            SQL.AppendLine("    Where CancelInd='N' And Left(DocDt, 6)>Concat(@Yr, @Mth) ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select BankAcCode2 As bankAcCode, Case AcType2 When 'C' Then -1 Else 1 End *Amt*ExcRate As Amt ");
            SQL.AppendLine("    From TblVoucherHdr  ");
            SQL.AppendLine("    Where CancelInd='N' And Left(DocDt, 6)>Concat(@Yr, @Mth) And AcType2 Is Not Null  ");
            SQL.AppendLine(") B On A.BankAcCode=B.BankAcCode  ");
            SQL.AppendLine("Group By A.BankAcNm, A.CurCode ");
            SQL.AppendLine("Having Sum(B.Amt)<>0 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Type, X2.VdName As Description, X1.CurCode, Sum(X1.Amt) As Amt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.VdCode, A.CurCode, (A.Amt+A.TaxAmt-A.DownPayment-IfNull(B.Amt, 0)) As Amt ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select T.PurchaseInvoiceDocNo, Sum(T.Amt) Amt ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select T2.InvoiceDocNo As PurchaseInvoiceDocNo, T2.Amt ");
            SQL.AppendLine("            From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("            Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' "); 
            SQL.AppendLine("            Inner Join TblPurchaseInvoiceHdr T3  ");
            SQL.AppendLine("                On T2.InvoiceDocNo=T3.DocNo "); 
            SQL.AppendLine("                And Left(T3.DocDt, 6)>Concat(@Yr, @Mth) ");
            SQL.AppendLine("            Inner Join TblVoucherRequestHdr T4 "); 
            SQL.AppendLine("                On T1.VoucherRequestDocNo=T4.DocNo "); 
            SQL.AppendLine("                And T4.VoucherDocNo Is Null "); 
            SQL.AppendLine("                And T4.CancelInd='N' ");
            SQL.AppendLine("            Where T1.CancelInd='N' "); 
            SQL.AppendLine("            And IfNull(T1.Status, 'O') in ('O', 'A') ");
            SQL.AppendLine("            Union All ");
            SQL.AppendLine("            Select T1.PurchaseInvoiceDocNo, T1.Amt ");
            SQL.AppendLine("            From TblApsHdr T1 ");
            SQL.AppendLine("            Inner Join TblPurchaseInvoiceHdr T2 "); 
            SQL.AppendLine("                On T1.PurchaseInvoiceDocNo=T2.DocNo "); 
            SQL.AppendLine("                And Left(T2.DocDt, 6)>Concat(@Yr, @Mth) ");
            SQL.AppendLine("            Where T1.CancelInd='N' ");
            SQL.AppendLine("        ) T Group By T.PurchaseInvoiceDocNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.PurchaseInvoiceDocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And Left(A.DocDt, 6)>Concat(@Yr, @Mth) ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.VdCode, A.CurCode, A.Amt-IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("    From TblPurchaseReturnInvoiceHdr A ");
            SQL.AppendLine("    Left Join ( "); 
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) As Amt "); 
            SQL.AppendLine("        From TblOutgoingPaymentHdr T1 "); 
            SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' "); 
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T3 "); 
            SQL.AppendLine("            On T2.InvoiceDocNo=T3.DocNo "); 
            SQL.AppendLine("            And Left(T3.DocDt, 6)>Concat(@Yr, @Mth) ");
            SQL.AppendLine("        Inner Join TblVoucherRequestHdr T4 "); 
            SQL.AppendLine("            On T1.VoucherRequestDocNo=T4.DocNo "); 
            SQL.AppendLine("            And T4.VoucherDocNo Is Null "); 
            SQL.AppendLine("            And T4.CancelInd='N' ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");  
            SQL.AppendLine("        And IfNull(T1.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("        Group By T2.InvoiceDocNo "); 
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo "); 
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And Left(A.DocDt, 6)>Concat(@Yr, @Mth) ");
            SQL.AppendLine(") X1, TblVendor X2 "); 
            SQL.AppendLine("Where X1.VdCode=X2.VdCode ");
            SQL.AppendLine("Group By X2.VdName, X1.CurCode ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '3' As Type, X2.CtName As Description, X1.CurCode, Sum(X1.Amt) As Amt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.CtCode, A.CurCode, A.Amt-IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("    From TblSalesInvoiceHdr A  ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) Amt  ");
            SQL.AppendLine("        From TblIncomingPaymentHdr T1  ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1'  ");
            SQL.AppendLine("        Inner Join TblSalesInvoiceHdr T3  ");
            SQL.AppendLine("            On T2.InvoiceDocNo=T3.DocNo "); 
            SQL.AppendLine("            And Left(T3.DocDt, 6)>Concat(@Yr, @Mth) ");
            SQL.AppendLine("        Inner Join TblVoucherRequestHdr T4 "); 
            SQL.AppendLine("            On T1.VoucherRequestDocNo=T4.DocNo "); 
            SQL.AppendLine("            And T4.VoucherDocNo Is Null "); 
            SQL.AppendLine("            And T4.CancelInd='N' ");
            SQL.AppendLine("        Where T1.CancelInd='N' "); 
            SQL.AppendLine("        And IfNull(T1.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("        Group By T2.InvoiceDocNo  ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("    Union All  ");
            SQL.AppendLine("    Select A.CtCode, A.CurCode, A.TotalAmt-IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("    From TblSalesReturnInvoiceHdr A  ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) As Amt  ");
            SQL.AppendLine("        From TblIncomingPaymentHdr T1  ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2'  ");
            SQL.AppendLine("        Inner Join TblSalesReturnInvoiceHdr T3  ");
            SQL.AppendLine("        On T2.InvoiceDocNo=T3.DocNo  ");
            SQL.AppendLine("        And Left(T3.DocDt, 6)>Concat(@Yr, @Mth) ");
            SQL.AppendLine("        Inner Join TblVoucherRequestHdr T4  ");
            SQL.AppendLine("            On T1.VoucherRequestDocNo=T4.DocNo  ");
            SQL.AppendLine("            And T4.VoucherDocNo Is Null  ");
            SQL.AppendLine("            And T4.CancelInd='N' ");
            SQL.AppendLine("        Where T1.CancelInd='N'  ");
            SQL.AppendLine("        And IfNull(T1.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("        Group By T2.InvoiceDocNo  ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo "); 
            SQL.AppendLine("    Where A.CancelInd='N' "); 
            SQL.AppendLine(") X1, TblCustomer X2 Where X1.CtCode=X2.CtCode ");
            SQL.AppendLine("Group By X2.CtName, X1.CurCode ");            

            mSQL = SQL.ToString();
        }

        private void GetCashBookYrMth()
        {
            mYr = "0000";
            mMth = "00";
            var YrMth = Sm.GetValue(
                "Select Concat(Yr, Mth) From TblClosingBalanceInCashHdr " +
                "Order By Yr Desc, Mth Desc Limit 1;"
                );

            if (YrMth.Length == 6)
            {
                mYr = Sm.Left(YrMth, 4);
                mMth = Sm.Right(YrMth, 2);
            }
        }


        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-4
                        "Type",
                        "Description",
                        "Currency", 
                        "Amount"
                    },
                    new int[] 
                    { 50, 100, 300, 60, 130 }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.SetGrdProperty(Grd1, false);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 5;
            Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd2, new string[] 
                    {
                        //0
                        "No.",

                        //1-4
                        "Currency", 
                        "In",
                        "Out",
                        "Balance"
                    },
                    new int[] { 50, 80, 180, 180, 180 }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 2, 3, 4 }, 0);
            Sm.SetGrdProperty(Grd2, false);

            #endregion
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            Sm.ClearGrd(Grd2, false);

            var r1 = new List<Result1>();
            var r2 = new List<Result2>();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref r1);
                if (r1.Count > 0)
                {
                    Process2(ref r2);
                    Process3(ref r1, ref r2);
                    Process4(ref r1);
                    Process5(ref r2);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Sm.FocusGrd(Grd2, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1(ref List<Result1> r1)
        {
            string Filter = string.Empty;

            GetCashBookYrMth();

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Yr", mYr);
            Sm.CmParam<String>(ref cm, "@Mth", mMth);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCurCode), "CurCode", true);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select * From ( " + mSQL + ") Tbl " + Filter + " Order By Type, Description, CurCode;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Type", "Description", "CurCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        r1.Add(new Result1()
                        {
                            Type = Sm.DrStr(dr, c[0]),
                            Desciption = Sm.DrStr(dr, c[1]),
                            CurCode = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Result2> r2)
        {
            string Filter = string.Empty;
            var cm = new MySqlCommand();

            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCurCode), "CurCode", true);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select CurCode From TblCurrency " + Filter + " Order By CurCode;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "CurCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        r2.Add(new Result2()
                        { 
                            CurCode = Sm.DrStr(dr, c[0]),
                            In = 0m,
                            Out = 0m,
                            Balance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<Result1> r1, ref List<Result2> r2)
        {
            var CurCode = string.Empty;
            for (var i = 0; i < r1.Count; i++)
            {
                CurCode = r1[i].CurCode;
                foreach (var r in r2.Where(x => x.CurCode == CurCode))
                {
                    if (r1[i].Type == "2")
                        r.Out += r1[i].Amt;
                    else
                        r.In += r1[i].Amt;
                }
            }

            for (var i = 0; i < r2.Count; i++)
                r2[i].Balance = r2[i].In - r2[i].Out;
        }

        private void Process4(ref List<Result1> r1)
        {
            iGRow r;
            Grd1.BeginUpdate();
            for (var i = 0; i < r1.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                if (r1[i].Type== "1") r.Cells[1].Value = "Cash/Bank";
                if (r1[i].Type == "2") r.Cells[1].Value = "Vendor";
                if (r1[i].Type == "3") r.Cells[1].Value = "Customer";
                r.Cells[2].Value = r1[i].Desciption;
                r.Cells[3].Value = r1[i].CurCode;
                r.Cells[4].Value = r1[i].Amt;
            }
            Grd1.EndUpdate();
        }

        private void Process5(ref List<Result2> r2)
        {
            int No = 0;
            iGRow r;
            Grd2.BeginUpdate();
            for (var i = 0; i < r2.Count; i++)
            {
                if (r2[i].In > 0 || r2[i].Out > 0 || r2[i].Balance > 0)
                {
                    No++;
                    r = Grd2.Rows.Add();
                    r.Cells[0].Value = No;
                    r.Cells[1].Value = r2[i].CurCode;
                    r.Cells[2].Value = r2[i].In;
                    r.Cells[3].Value = r2[i].Out;
                    r.Cells[4].Value = r2[i].Balance;
                }
            }
            Grd2.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCurCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Currency");
        }

        #endregion

        #endregion

        #region Class

        private class Result1
        {
            public string Type { get; set; }
            public string Desciption { get; set; }
            public string CurCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class Result2
        {
            public string CurCode { get; set; }
            public decimal In { get; set; }
            public decimal Out { get; set; }
            public decimal Balance { get; set; }
        }

        #endregion
    }
}
