﻿namespace RunSystem
{
    partial class FrmRptProfitLossProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptProfitLossProject));
            this.LueMth = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LueYr = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtLOPDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkLOPDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtProjectCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkProjectCode = new DevExpress.XtraEditors.CheckEdit();
            this.BtnLOPDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnLOPDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.MeeProjectName = new DevExpress.XtraEditors.MemoExEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtPLCBAmt = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtPLMAmt = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtPLOBAmt = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueMth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLOPDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLOPDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProjectCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeProjectName.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPLCBAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPLMAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPLOBAmt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(838, 0);
            this.panel1.Size = new System.Drawing.Size(70, 504);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.MeeProjectName);
            this.panel2.Controls.Add(this.BtnLOPDocNo2);
            this.panel2.Controls.Add(this.BtnLOPDocNo);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtProjectCode);
            this.panel2.Controls.Add(this.ChkProjectCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtLOPDocNo);
            this.panel2.Controls.Add(this.ChkLOPDocNo);
            this.panel2.Controls.Add(this.LueMth);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.LueYr);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Size = new System.Drawing.Size(838, 92);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(838, 412);
            this.Grd1.TabIndex = 29;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 482);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 92);
            this.panel3.Size = new System.Drawing.Size(838, 412);
            // 
            // LueMth
            // 
            this.LueMth.EnterMoveNextControl = true;
            this.LueMth.Location = new System.Drawing.Point(210, 65);
            this.LueMth.Margin = new System.Windows.Forms.Padding(5);
            this.LueMth.Name = "LueMth";
            this.LueMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.Appearance.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMth.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMth.Properties.DropDownRows = 13;
            this.LueMth.Properties.NullText = "[Empty]";
            this.LueMth.Properties.PopupWidth = 500;
            this.LueMth.Size = new System.Drawing.Size(75, 20);
            this.LueMth.TabIndex = 20;
            this.LueMth.ToolTip = "F4 : Show/hide list";
            this.LueMth.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(167, 68);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 14);
            this.label1.TabIndex = 19;
            this.label1.Text = "Month";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueYr
            // 
            this.LueYr.EnterMoveNextControl = true;
            this.LueYr.Location = new System.Drawing.Point(88, 65);
            this.LueYr.Margin = new System.Windows.Forms.Padding(5);
            this.LueYr.Name = "LueYr";
            this.LueYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.Appearance.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueYr.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueYr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueYr.Properties.DropDownRows = 12;
            this.LueYr.Properties.NullText = "[Empty]";
            this.LueYr.Properties.PopupWidth = 500;
            this.LueYr.Size = new System.Drawing.Size(75, 20);
            this.LueYr.TabIndex = 18;
            this.LueYr.ToolTip = "F4 : Show/hide list";
            this.LueYr.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(52, 68);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 14);
            this.label4.TabIndex = 17;
            this.label4.Text = "Year";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DarkOrange;
            this.label6.Location = new System.Drawing.Point(38, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 14);
            this.label6.TabIndex = 8;
            this.label6.Text = "Project";
            // 
            // TxtLOPDocNo
            // 
            this.TxtLOPDocNo.EnterMoveNextControl = true;
            this.TxtLOPDocNo.Location = new System.Drawing.Point(88, 2);
            this.TxtLOPDocNo.Name = "TxtLOPDocNo";
            this.TxtLOPDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtLOPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLOPDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLOPDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLOPDocNo.Properties.MaxLength = 250;
            this.TxtLOPDocNo.Properties.ReadOnly = true;
            this.TxtLOPDocNo.Size = new System.Drawing.Size(219, 20);
            this.TxtLOPDocNo.TabIndex = 9;
            this.TxtLOPDocNo.Validated += new System.EventHandler(this.TxtLOPDocNo_Validated);
            // 
            // ChkLOPDocNo
            // 
            this.ChkLOPDocNo.Location = new System.Drawing.Point(310, 1);
            this.ChkLOPDocNo.Name = "ChkLOPDocNo";
            this.ChkLOPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkLOPDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkLOPDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkLOPDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkLOPDocNo.Properties.Caption = " ";
            this.ChkLOPDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkLOPDocNo.Size = new System.Drawing.Size(20, 22);
            this.ChkLOPDocNo.TabIndex = 10;
            this.ChkLOPDocNo.ToolTip = "Remove filter";
            this.ChkLOPDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkLOPDocNo.ToolTipTitle = "Run System";
            this.ChkLOPDocNo.CheckedChanged += new System.EventHandler(this.ChkLOPDocNo_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkOrange;
            this.label2.Location = new System.Drawing.Point(6, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Project Code";
            // 
            // TxtProjectCode
            // 
            this.TxtProjectCode.EnterMoveNextControl = true;
            this.TxtProjectCode.Location = new System.Drawing.Point(88, 44);
            this.TxtProjectCode.Name = "TxtProjectCode";
            this.TxtProjectCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectCode.Properties.MaxLength = 250;
            this.TxtProjectCode.Size = new System.Drawing.Size(219, 20);
            this.TxtProjectCode.TabIndex = 15;
            this.TxtProjectCode.Validated += new System.EventHandler(this.TxtProjectCode_Validated);
            // 
            // ChkProjectCode
            // 
            this.ChkProjectCode.Location = new System.Drawing.Point(310, 44);
            this.ChkProjectCode.Name = "ChkProjectCode";
            this.ChkProjectCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProjectCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProjectCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProjectCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProjectCode.Properties.Caption = " ";
            this.ChkProjectCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProjectCode.Size = new System.Drawing.Size(20, 22);
            this.ChkProjectCode.TabIndex = 16;
            this.ChkProjectCode.ToolTip = "Remove filter";
            this.ChkProjectCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProjectCode.ToolTipTitle = "Run System";
            this.ChkProjectCode.CheckedChanged += new System.EventHandler(this.ChkProjectCode_CheckedChanged);
            // 
            // BtnLOPDocNo
            // 
            this.BtnLOPDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnLOPDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnLOPDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLOPDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnLOPDocNo.Appearance.Options.UseBackColor = true;
            this.BtnLOPDocNo.Appearance.Options.UseFont = true;
            this.BtnLOPDocNo.Appearance.Options.UseForeColor = true;
            this.BtnLOPDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnLOPDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnLOPDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnLOPDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnLOPDocNo.Image")));
            this.BtnLOPDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnLOPDocNo.Location = new System.Drawing.Point(331, 3);
            this.BtnLOPDocNo.Name = "BtnLOPDocNo";
            this.BtnLOPDocNo.Size = new System.Drawing.Size(16, 18);
            this.BtnLOPDocNo.TabIndex = 11;
            this.BtnLOPDocNo.ToolTip = "Find List of Project";
            this.BtnLOPDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnLOPDocNo.ToolTipTitle = "Run System";
            this.BtnLOPDocNo.Click += new System.EventHandler(this.BtnLOPDocNo_Click);
            // 
            // BtnLOPDocNo2
            // 
            this.BtnLOPDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnLOPDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnLOPDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLOPDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnLOPDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnLOPDocNo2.Appearance.Options.UseFont = true;
            this.BtnLOPDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnLOPDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnLOPDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnLOPDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnLOPDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnLOPDocNo2.Image")));
            this.BtnLOPDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnLOPDocNo2.Location = new System.Drawing.Point(351, 3);
            this.BtnLOPDocNo2.Name = "BtnLOPDocNo2";
            this.BtnLOPDocNo2.Size = new System.Drawing.Size(16, 18);
            this.BtnLOPDocNo2.TabIndex = 12;
            this.BtnLOPDocNo2.ToolTip = "Show List of Project Data";
            this.BtnLOPDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnLOPDocNo2.ToolTipTitle = "Run System";
            this.BtnLOPDocNo2.Click += new System.EventHandler(this.BtnLOPDocNo2_Click);
            // 
            // MeeProjectName
            // 
            this.MeeProjectName.EnterMoveNextControl = true;
            this.MeeProjectName.Location = new System.Drawing.Point(88, 23);
            this.MeeProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.MeeProjectName.Name = "MeeProjectName";
            this.MeeProjectName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.MeeProjectName.Properties.Appearance.Options.UseFont = true;
            this.MeeProjectName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeProjectName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeProjectName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectName.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeProjectName.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectName.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeProjectName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeProjectName.Properties.MaxLength = 800;
            this.MeeProjectName.Properties.PopupFormSize = new System.Drawing.Size(450, 20);
            this.MeeProjectName.Properties.ReadOnly = true;
            this.MeeProjectName.Properties.ShowIcon = false;
            this.MeeProjectName.Size = new System.Drawing.Size(409, 20);
            this.MeeProjectName.TabIndex = 13;
            this.MeeProjectName.ToolTip = "F4 : Show/hide text";
            this.MeeProjectName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeProjectName.ToolTipTitle = "Run System";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.TxtPLCBAmt);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.TxtPLMAmt);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.TxtPLOBAmt);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(519, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(315, 88);
            this.panel4.TabIndex = 21;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(16, 68);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 14);
            this.label8.TabIndex = 27;
            this.label8.Text = "Closing Balance";
            // 
            // TxtPLCBAmt
            // 
            this.TxtPLCBAmt.EnterMoveNextControl = true;
            this.TxtPLCBAmt.Location = new System.Drawing.Point(109, 66);
            this.TxtPLCBAmt.Name = "TxtPLCBAmt";
            this.TxtPLCBAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPLCBAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPLCBAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPLCBAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtPLCBAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPLCBAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPLCBAmt.Properties.MaxLength = 250;
            this.TxtPLCBAmt.Properties.ReadOnly = true;
            this.TxtPLCBAmt.Size = new System.Drawing.Size(200, 20);
            this.TxtPLCBAmt.TabIndex = 28;
            this.TxtPLCBAmt.Validated += new System.EventHandler(this.TxtPLCBAmt_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(50, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 14);
            this.label7.TabIndex = 25;
            this.label7.Text = "Mutation";
            // 
            // TxtPLMAmt
            // 
            this.TxtPLMAmt.EnterMoveNextControl = true;
            this.TxtPLMAmt.Location = new System.Drawing.Point(109, 45);
            this.TxtPLMAmt.Name = "TxtPLMAmt";
            this.TxtPLMAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPLMAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPLMAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPLMAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtPLMAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPLMAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPLMAmt.Properties.MaxLength = 250;
            this.TxtPLMAmt.Properties.ReadOnly = true;
            this.TxtPLMAmt.Size = new System.Drawing.Size(200, 20);
            this.TxtPLMAmt.TabIndex = 26;
            this.TxtPLMAmt.Validated += new System.EventHandler(this.TxtPLMAmt_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(7, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 14);
            this.label5.TabIndex = 23;
            this.label5.Text = "Opening Balance";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(6, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 14);
            this.label3.TabIndex = 22;
            this.label3.Text = "Profit/Loss Amount";
            // 
            // TxtPLOBAmt
            // 
            this.TxtPLOBAmt.EnterMoveNextControl = true;
            this.TxtPLOBAmt.Location = new System.Drawing.Point(109, 24);
            this.TxtPLOBAmt.Name = "TxtPLOBAmt";
            this.TxtPLOBAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPLOBAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPLOBAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPLOBAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtPLOBAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPLOBAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPLOBAmt.Properties.MaxLength = 250;
            this.TxtPLOBAmt.Properties.ReadOnly = true;
            this.TxtPLOBAmt.Size = new System.Drawing.Size(200, 20);
            this.TxtPLOBAmt.TabIndex = 24;
            this.TxtPLOBAmt.Validated += new System.EventHandler(this.TxtPLOBAmt_Validated);
            // 
            // FrmRptProfitLossProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 504);
            this.Name = "FrmRptProfitLossProject";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueMth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLOPDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLOPDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProjectCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeProjectName.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPLCBAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPLMAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPLOBAmt.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit LueMth;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueYr;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TxtProjectCode;
        private DevExpress.XtraEditors.CheckEdit ChkProjectCode;
        public DevExpress.XtraEditors.SimpleButton BtnLOPDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnLOPDocNo;
        protected internal DevExpress.XtraEditors.TextEdit TxtLOPDocNo;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeProjectName;
        protected internal DevExpress.XtraEditors.CheckEdit ChkLOPDocNo;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        protected internal DevExpress.XtraEditors.TextEdit TxtPLOBAmt;
        private System.Windows.Forms.Label label8;
        protected internal DevExpress.XtraEditors.TextEdit TxtPLCBAmt;
        private System.Windows.Forms.Label label7;
        protected internal DevExpress.XtraEditors.TextEdit TxtPLMAmt;
    }
}