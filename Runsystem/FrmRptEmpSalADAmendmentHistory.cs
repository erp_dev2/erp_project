﻿#region Update
/*
    05/08/2022 [IBL/PHT] Mempercepat proses refresh data
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpSalADAmendmentHistory : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false,
            mIsUseEmpSalarySS = false,
            mIsUseEmpPerformance = false
            ;
        private List<Amendment> l;
        private List<AmendmentAfter> lA;

        #endregion

        #region Constructor

        public FrmRptEmpSalADAmendmentHistory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                SetLueStatus(ref LueStatus);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sm.SetLue(LueStatus, "1");

                l = new List<Amendment>();
                lA = new List<AmendmentAfter>();
                SetAmendment(ref l);
                SetAmendmentAfter(ref lA);
                SetGrd();
                SetLueEmpStatus(ref LueEmpStatus);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsUseEmpPerformance = Sm.GetParameterBoo("IsUseEmpPerformance");
            mIsUseEmpSalarySS = Sm.GetParameterBoo("IsUseEmpSalarySS");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22 + l.Count + lA.Count;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Employee's"+Environment.NewLine+"Code",
                    "Employee's Name",
                    "Old Code",
                    "Position",
                    "Grade",
                    
                    //6-10
                    "Level",
                    "Department",
                    "Site",
                    "Status",
                    "Join"+Environment.NewLine+"Date",
                    
                    //11-15
                    "Resign"+Environment.NewLine+"Date",
                    "Monthly Salary"+Environment.NewLine+"[Before]",
                    "Monthly Salary"+Environment.NewLine+"[After]",
                    "Daily Salary"+Environment.NewLine+"[Before]",
                    "Daily Salary"+Environment.NewLine+"[After]",

                    //16-20
                    "Performance Grade"+Environment.NewLine+"[Before]",
                    "Performance Grade"+Environment.NewLine+"[After]",
                    "Performance Status"+Environment.NewLine+"[Before]",
                    "Performance Status"+Environment.NewLine+"[After]",
                    "Performance Value"+Environment.NewLine+"[Before]",

                    //21
                    "Performance Value"+Environment.NewLine+"[After]"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    100, 200, 100, 200, 150, 

                    //6-10
                    150, 200, 200, 130, 100, 
                    
                    //11-15
                    100, 130, 130, 130, 130,

                    //16-20
                    130, 130, 130, 130, 130,

                    //21
                    130
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 10, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14, 15, 20, 21 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 9, 10, 11 }, false);

            if (!mIsUseEmpPerformance)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 20, 21 });
            }

            for (int i = 0; i < l.Count; i++)
            {
                Grd1.Cols[l[i].Col].Text = l[i].Name.Trim().Replace(" ", Environment.NewLine) + Environment.NewLine + "[Before]";
                Grd1.Cols[l[i].Col].Width = 150;
                Grd1.Header.Cells[0, l[i].Col].TextAlign = iGContentAlignment.TopCenter;
                //Grd1.Header.Cells[0, l[i].Col].BackColor = Color.LightCoral;
                Grd1.Cols[l[i].Col].CellStyle.ValueType = typeof(Decimal);
                Grd1.Cols[l[i].Col].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd1.Cols[l[i].Col].CellStyle.FormatString = (Gv.FormatNum0.Length != 0) ? Gv.FormatNum0 : "{0:#,##0.00##}";
            }

            for (int x = 0; x < lA.Count; x++)
            {
                Grd1.Cols[lA[x].Col].Text = lA[x].Name.Trim().Replace(" ", Environment.NewLine) + Environment.NewLine + "[After]";
                Grd1.Cols[lA[x].Col].Width = 150;
                Grd1.Header.Cells[0, lA[x].Col].TextAlign = iGContentAlignment.TopCenter;
                //Grd1.Header.Cells[0, lA[x].Col].BackColor = Color.LightGreen;
                Grd1.Cols[lA[x].Col].CellStyle.ValueType = typeof(Decimal);
                Grd1.Cols[lA[x].Col].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd1.Cols[lA[x].Col].CellStyle.FormatString = (Gv.FormatNum0.Length != 0) ? Gv.FormatNum0 : "{0:#,##0.00##}";
            }
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1();
                if (Grd1.Rows.Count > 0)
                {
                    Process2();
                    ProcessSalary();
                    ProcessPerformanceGrade();
                    ProcessAllowanceDeduction();
                    ProcessSSP();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Grd1.Cols.AutoWidth();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " ";

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, F.GrdLvlName, G.LevelName, C.DeptName, D.SiteName, ");
            SQL.AppendLine("H.OptDesc As EmploymentStatusDesc, A.JoinDt, A.ResignDt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblSite D On A.SiteCode=D.SiteCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr F On A.GrdLvlCode=F.GrdLvlCode ");
            SQL.AppendLine("Left Join TblLevelHdr G On F.LevelCode=G.LevelCode ");
            SQL.AppendLine("Left Join TblOption H On A.EmploymentStatus=H.OptCode And H.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Where 1=1 ");

            switch (Sm.GetLue(LueStatus))
            {
                case "1":
                    Filter = " And (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ";
                    break;
                case "2":
                    Filter = " And A.ResignDt Is Not Null And A.ResignDt<=@CurrentDate ";
                    break;
            }

            Sm.CmParamDt(ref cm, "@CurrentDate", Sm.Left(Sm.ServerCurrentDateTime(), 8));
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpCodeOld", "A.EmpName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueEmpStatus), "H.OptCode", true);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString() + Filter + " Order By A.EmpName;",
                new string[] { 
                    "EmpCode", 
                    "EmpName", "EmpCodeOld", "PosName", "GrdLvlName", "LevelName", 
                    "DeptName", "SiteName", "EmploymentSTatusDesc", "JoinDt", "ResignDt", 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                }, true, false, false, false
            );
        }

        private void Process2()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                for (int c = 12; c < Grd1.Cols.Count; c++)
                {
                    if (c == 18 || c == 19)
                    {
                        Grd1.Cells[r, c].Value = string.Empty;
                    }
                    else
                    {
                        Grd1.Cells[r, c].Value = 0m;
                    }
                }
            }
        }

        #region Salary

        private void ProcessSalary()
        {
            var lEmpSalAf = new List<SalaryAfter>();
            var lEmpSalB = new List<SalaryBefore>();

            ProcessSalaryAfter(ref lEmpSalAf);
            ProcessSalaryBefore(ref lEmpSalAf, ref lEmpSalB);

            if (lEmpSalAf.Count > 0 || lEmpSalB.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (lEmpSalAf.Count > 0)
                    {
                        for (int j = 0; j < lEmpSalAf.Count; j++)
                        {
                            if (string.Compare(Sm.GetGrdStr(Grd1, i, 1), lEmpSalAf[j].EmpCode) == 0)
                            {
                                Grd1.Cells[i, 13].Value = Sm.GetGrdDec(Grd1, i, 13) + lEmpSalAf[j].Amt;
                                Grd1.Cells[i, 15].Value = Sm.GetGrdDec(Grd1, i, 15) + lEmpSalAf[j].Amt2;
                                break;
                            }
                        }
                    }

                    if (lEmpSalB.Count > 0)
                    {
                        for (int j = 0; j < lEmpSalB.Count; j++)
                        {
                            if (string.Compare(Sm.GetGrdStr(Grd1, i, 1), lEmpSalB[j].EmpCode) == 0)
                            {
                                Grd1.Cells[i, 12].Value = Sm.GetGrdDec(Grd1, i, 12) + lEmpSalB[j].Amt;
                                Grd1.Cells[i, 14].Value = Sm.GetGrdDec(Grd1, i, 14) + lEmpSalB[j].Amt2;
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void ProcessSalaryAfter(ref List<SalaryAfter> lEmpSalAf)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("select A.EmpCode, A.StartDt, A.Amt, A.Amt2, 'A' As AfterInd ");
            SQL.AppendLine("from tblemployeesalary A ");
            SQL.AppendLine("inner join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select EmpCode, Max(StartDt) StartDt ");
            SQL.AppendLine("    From TblEmployeeSalary ");
            SQL.AppendLine("    Where ActInd = 'Y' ");
            SQL.AppendLine("    Group By EmpCode ");
            SQL.AppendLine(") B ON A.EmpCode = B.EmpCode And A.StartDt = B.StartDt ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");

            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "StartDt", "Amt", "Amt2", "AfterInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEmpSalAf.Add(new SalaryAfter()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            AfterInd = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessSalaryBefore(ref List<SalaryAfter> lEmpSalAf, ref List<SalaryBefore> lEmpSalB)
        {
            var SQL = new StringBuilder();

            string Filter = string.Empty;

            //if (lEmpSalAf.Count > 0)
            //{
            //    for (int i = 0; i < lEmpSalAf.Count; i++)
            //    {
            //        if (Filter.Length != 0) Filter += " Or ";
            //        Filter += " (EmpCode = '" + lEmpSalAf[i].EmpCode + "' And StartDt = '" + lEmpSalAf[i].StartDt + "') ";
            //    }
            //}

            SQL.AppendLine("select A.EmpCode, A.StartDt, A.Amt, A.Amt2, 'B' As AfterInd ");
            SQL.AppendLine("from tblemployeesalary A ");
            SQL.AppendLine("inner join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T.EmpCode, Max(T.StartDt) StartDt ");
            SQL.AppendLine("    From TblEmployeeSalary T ");
            SQL.AppendLine("    Where T.ActInd = 'Y' ");
            SQL.AppendLine("    And Concat(T.EmpCode, T.StartDt) Not In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Concat(X1.EmpCode, X1.StartDt) ");
            SQL.AppendLine("        from TblEmployeeSalary X1 ");
            SQL.AppendLine("        inner join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select EmpCode, Max(StartDt) StartDt ");
            SQL.AppendLine("            From TblEmployeeSalary ");
            SQL.AppendLine("            Where ActInd = 'Y' ");
            SQL.AppendLine("            Group By EmpCode ");
            SQL.AppendLine("        ) X2 ON X1.EmpCode = X2.EmpCode And X1.StartDt = X2.StartDt ");
            SQL.AppendLine("        Where X1.ActInd = 'Y' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By T.EmpCode ");
            SQL.AppendLine(") B ON A.EmpCode = B.EmpCode And A.StartDt = B.StartDt ");
            SQL.AppendLine("Where A.ActInd = 'Y'; ");

            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "StartDt", "Amt", "Amt2", "AfterInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEmpSalB.Add(new SalaryBefore()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            AfterInd = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Performance

        private void ProcessPerformanceGrade()
        {
            var lEmpPerformanceAf = new List<PerformanceAfter>();
            var lEmpPerformanceB = new List<PerformanceBefore>();

            ProcessPerformanceAfter(ref lEmpPerformanceAf);
            ProcessPerformanceBefore(ref lEmpPerformanceAf, ref lEmpPerformanceB);

            if (lEmpPerformanceAf.Count > 0 || lEmpPerformanceB.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (lEmpPerformanceAf.Count > 0)
                    {
                        for (int j = 0; j < lEmpPerformanceAf.Count; j++)
                        {
                            if (string.Compare(Sm.GetGrdStr(Grd1, i, 1), lEmpPerformanceAf[j].EmpCode) == 0)
                            {
                                Grd1.Cells[i, 17].Value = lEmpPerformanceAf[j].GradePerformance;
                                Grd1.Cells[i, 19].Value = lEmpPerformanceAf[j].PerformanceStatus;
                                Grd1.Cells[i, 21].Value = Sm.GetGrdDec(Grd1, i, 21) + lEmpPerformanceAf[j].Value;
                                break;
                            }
                        }
                    }

                    if (lEmpPerformanceB.Count > 0)
                    {
                        for (int j = 0; j < lEmpPerformanceB.Count; j++)
                        {
                            if (string.Compare(Sm.GetGrdStr(Grd1, i, 1), lEmpPerformanceB[j].EmpCode) == 0)
                            {
                                Grd1.Cells[i, 16].Value = lEmpPerformanceB[j].GradePerformance;
                                Grd1.Cells[i, 18].Value = lEmpPerformanceB[j].PerformanceStatus;
                                Grd1.Cells[i, 20].Value = Sm.GetGrdDec(Grd1, i, 20) + lEmpPerformanceB[j].Value;
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void ProcessPerformanceAfter(ref List<PerformanceAfter> lEmpPerformanceAf)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.EmpCode, D.GradePerformance, C.OptDesc As PerformanceStatus, D.Value, A.CreateDt ");
            SQL.AppendLine("from tblempsalaryhdr A ");
            SQL.AppendLine("inner join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select EmpCode, Max(CreateDt) CreateDt ");
            SQL.AppendLine("    From TblEmpSalaryHdr ");
            SQL.AppendLine("    Where Status = 'A' ");
            SQL.AppendLine("    Group By EmpCode ");
            SQL.AppendLine(") B On A.EmpCode = B.EmpCode And A.CreateDt = B.CreateDt ");
            SQL.AppendLine("left join tbloption C On A.PerformanceStatus = C.OptCode And C.OptCat = 'PerformanceStatus' ");
            SQL.AppendLine("left join tblempsalary5dtl D On A.DocNo = D.DocNo ");
            SQL.AppendLine("Where A.Status = 'A'; ");

            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "EmpCode", "GradePerformance", "PerformanceStatus", "Value", "CreateDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEmpPerformanceAf.Add(new PerformanceAfter()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            GradePerformance = Sm.DrStr(dr, c[2]),
                            PerformanceStatus = Sm.DrStr(dr, c[3]),
                            Value = Sm.DrDec(dr, c[4]),
                            CreateDt = Sm.DrStr(dr, c[5])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessPerformanceBefore(ref List<PerformanceAfter> lEmpPerformanceAf, ref List<PerformanceBefore> lEmpPerformanceB)
        {
            var SQL = new StringBuilder();

            string EmpCodes = string.Empty, CreateDts = string.Empty,
            DocNos = string.Empty, Filter = string.Empty;

            if (lEmpPerformanceAf.Count > 0)
            {
                for (int i = 0; i < lEmpPerformanceAf.Count; i++)
                {
                    //if (EmpCodes.Length != 0) EmpCodes += ",";
                    //EmpCodes += lEmpPerformanceAf[i].EmpCode;

                    //if (CreateDts.Length != 0) CreateDts += ",";
                    //CreateDts += lEmpPerformanceAf[i].CreateDt;

                    //if (DocNos.Length != 0) DocNos += " Or ";
                    //DocNos += " (DocNo = '" + lEmpPerformanceAf[i].DocNo + "') ";

                    //if (Filter.Length != 0) Filter += " Or ";
                    //Filter += " (A.EmpCode = '" + lEmpPerformanceAf[i].EmpCode + "' And A.CreateDt = '" + lEmpPerformanceAf[i].CreateDt + "' ) ";
                }
            }

            #region Old Code

            //SQL.AppendLine("Select A.DocNo, A.EmpCode, D.GradePerformance, C.OptDesc As PerformanceStatus, D.Value, A.CreateDt ");
            //SQL.AppendLine("from tblempsalaryhdr A ");
            //SQL.AppendLine("inner join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select EmpCode, Max(CreateDt) CreateDt ");
            //SQL.AppendLine("    From TblEmpSalaryHdr ");
            //SQL.AppendLine("    Where Status = 'A' ");
            //SQL.AppendLine("    And Concat(EmpCode, CreateDt) Not In ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("         Select Concat(EmpCode, CreateDt) ");
            //SQL.AppendLine("         From TblEmpSalaryHdr ");
            //SQL.AppendLine("         Where ( " + Filter + " ) ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine("    Group By EmpCode ");
            //SQL.AppendLine(") B On A.EmpCode = B.EmpCode And A.CreateDt = B.CreateDt ");
            //SQL.AppendLine("left join tbloption C On A.PerformanceStatus = C.OptCode And C.OptCat = 'PerformanceStatus' ");
            //SQL.AppendLine("left join tblempsalary5dtl D On A.DocNo = D.DocNo ");
            //SQL.AppendLine("Where A.Status = 'A'; ");

            #endregion

            SQL.AppendLine("Select X1.EmpCode, X3.OptDesc As PerformanceStatus, X1.GradePerformance, X1.Value, X1.CreateDt ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.EmpCode, A.PerformanceStatus, B.GradePerformance, B.Value, A.CreateDt ");
            SQL.AppendLine("    From TblEmpSalaryHdr A ");
            SQL.AppendLine("    Inner Join TblEmpSalary5Dtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Where A.Status = 'A' ");
            SQL.AppendLine(") X1 ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.EmpCode, Max(T1.CreateDt) CreateDt ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.EmpCode, A.PerformanceStatus, B.GradePerformance, B.Value, A.CreateDt ");
            SQL.AppendLine("        From TblEmpSalaryHdr A ");
            SQL.AppendLine("        Inner Join TblEmpSalary5Dtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Where Concat(A.EmpCode,A.CreateDt) Not In ( ");
            SQL.AppendLine("            Select Concat(A.EmpCode,A.CreateDt) ");
            SQL.AppendLine("            from tblempsalaryhdr A ");
            SQL.AppendLine("            inner join ");
            SQL.AppendLine("            ( ");
            SQL.AppendLine("                Select EmpCode, Max(CreateDt) CreateDt ");
            SQL.AppendLine("                From TblEmpSalaryHdr ");
            SQL.AppendLine("                Where Status = 'A' ");
            SQL.AppendLine("                Group By EmpCode ");
            SQL.AppendLine("            ) B On A.EmpCode = B.EmpCode And A.CreateDt = B.CreateDt ");
            SQL.AppendLine("            left join tbloption C On A.PerformanceStatus = C.OptCode And C.OptCat = 'PerformanceStatus' ");
            SQL.AppendLine("            left join tblempsalary5dtl D On A.DocNo = D.DocNo ");
            SQL.AppendLine("            Where A.Status = 'A' ");
            SQL.AppendLine("       ) ");
            SQL.AppendLine("    ) T1 ");
            SQL.AppendLine("    Group By T1.EmpCode ");
            SQL.AppendLine(")X2 On X1.EmpCode = X2.EmpCode And X1.CreateDt = X2.CreateDt ");
            SQL.AppendLine("Left Join TblOption X3 On X1.PerformanceStatus = X3.OptCode And X3.OptCat = 'PerformanceStatus' ");

            var cm = new MySqlCommand();

            //Sm.CmParam<String>(ref cm, "@EmpCodes", EmpCodes);
            //Sm.CmParam<String>(ref cm, "@CreateDts", CreateDts);
            //Sm.CmParam<String>(ref cm, "@DocNos", DocNos);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "GradePerformance", "PerformanceStatus", "Value", "CreateDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEmpPerformanceB.Add(new PerformanceBefore()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            GradePerformance = Sm.DrStr(dr, c[1]),
                            PerformanceStatus = Sm.DrStr(dr, c[2]),
                            Value = Sm.DrDec(dr, c[3]),
                            CreateDt = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Allowance Deduction

        private void ProcessAllowanceDeduction()
        {
            var lADAf = new List<ADAfter>();
            var lADB = new List<ADBefore>();

            ProcessADAfter(ref lADAf);
            ProcessADBefore(ref lADAf, ref lADB);

            if (lADAf.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    for (int j = 0; j < lADAf.Count; j++)
                    {
                        for (int k = 0; k < lA.Count; k++)
                        {
                            if (string.Compare(Sm.GetGrdStr(Grd1, i, 1), lADAf[j].EmpCode) == 0)
                            {
                                if (lADAf[j].ADCode == lA[k].Code)
                                {
                                    Grd1.Cells[i, lA[k].Col].Value = Sm.GetGrdDec(Grd1, i, lA[k].Col) + lADAf[j].Amt;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }

            if (lADB.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    for (int j = 0; j < lADB.Count; j++)
                    {
                        for (int k = 0; k < l.Count; k++)
                        {
                            if (string.Compare(Sm.GetGrdStr(Grd1, i, 1), lADB[j].EmpCode) == 0)
                            {
                                if (lADB[j].ADCode == l[k].Code)
                                {
                                    Grd1.Cells[i, l[k].Col].Value = Sm.GetGrdDec(Grd1, i, l[k].Col) + lADB[j].Amt;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void ProcessADAfter(ref List<ADAfter> lADAf)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("select A.EmpCode, A.DNo, A.ADCode, A.StartDt, A.Amt ");
            SQL.AppendLine("from TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("Inner join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select EmpCode, ADCode, Max(StartDt) StartDt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction ");
            SQL.AppendLine("    Group By EmpCode, ADCode ");
            SQL.AppendLine(") B On A.EmpCode = B.EmpCode And A.ADCOde = B.AdCode And A.StartDt = B.StartDt ");

            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "DNo", "ADCode", "StartDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lADAf.Add(new ADAfter()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            DNo = Sm.DrStr(dr, c[1]),
                            ADCode = "AD" + Sm.DrStr(dr, c[2]),
                            StartDt = Sm.DrStr(dr, c[3]),
                            Amt = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessADBefore(ref List<ADAfter> lADAf, ref List<ADBefore> lADB)
        {
            var SQL = new StringBuilder();
            var SQL1 = new StringBuilder();
            string Filter = string.Empty;

            //if (lADAf.Count > 0)
            //{
            //    for (int i = 0; i < lADAf.Count; i++)
            //    {
            //        Console.WriteLine("(EmpCode = '" + lADAf[i].EmpCode + "' And StartDt = '" + lADAf[i].StartDt + "' And ADCode = '" + Sm.Right(lADAf[i].ADCode, (lADAf[i].ADCode.Length - 2)) + "' )");
            //        if (Filter.Length != 0) Filter += " Or ";
            //        Filter += " (EmpCode = '" + lADAf[i].EmpCode + "' And StartDt = '" + lADAf[i].StartDt + "' And ADCode = '" + Sm.Right(lADAf[i].ADCode, (lADAf[i].ADCode.Length - 2)) + "' ) ";
            //    }
            //}

            #region Old Code - Source dari document document EmpSalary

            //SQL.AppendLine("Select X1.EmpCode, '001' As DNo, X1.ADCode, X1.StartDt, X1.Amt ");
            //SQL.AppendLine("From ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select A.EmpCode, B.ADCode, B.StartDt, B.Amt, A.CreateDt ");
            //SQL.AppendLine("    From TblEmpSalaryHdr A ");
            //SQL.AppendLine("    Inner Join TblEmpSalary2Dtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("    Where A.Status = 'A' ");
            //SQL.AppendLine("    And B.StartDt Is Not Null ");
            //SQL.AppendLine(") X1 ");
            //SQL.AppendLine("Inner Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select T1.EmpCode, T1.ADCode, Max(T1.StartDt) StartDt, Max(CreateDt) CreateDt ");
            //SQL.AppendLine("    From ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select A.DocNo, A.EmpCode, B.ADCode, B.StartDt, A.CreateDt ");
            //SQL.AppendLine("        From TblEmpSalaryHdr A ");
            //SQL.AppendLine("        Inner Join TblEmpSalary2Dtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("        Where StartDt Is Not Null ");
            //SQL.AppendLine("        And Not ( " + Filter + " )  ");
            //SQL.AppendLine("    ) T1 ");
            //SQL.AppendLine("    Group By T1.EmpCode,  T1.ADCode ");
            //SQL.AppendLine(")X2 On X1.EmpCode = X2.EmpCode And X1.ADCode = X2.ADCode And X1.StartDt = X2.StartDt And X1.CreateDt = X2.CreateDt ");

            #endregion

            SQL.AppendLine("select A.EmpCode, A.ADCode, A.StartDt, A.Amt ");
            SQL.AppendLine("from tblemployeeallowancededuction A ");
            SQL.AppendLine("inner join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T.EmpCode, Max(T.StartDt) StartDt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction T ");
            SQL.AppendLine("    Where Concat(EmpCode, StartDt, ADCode) Not In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Concat(X1.EmpCode, X1.StartDt, X1.ADCode) ");
            SQL.AppendLine("        From TblEmployeeAllowanceDeduction X1 ");
            SQL.AppendLine("        Inner join  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select EmpCode, ADCode, Max(StartDt) StartDt ");
            SQL.AppendLine("            From TblEmployeeAllowanceDeduction ");
            SQL.AppendLine("            Group By EmpCode, ADCode ");
            SQL.AppendLine("        ) X2 On X1.EmpCode = X2.EmpCode And X1.ADCOde = X2.AdCode And X1.StartDt = X2.StartDt ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By T.EmpCode ");
            SQL.AppendLine(") B ON A.EmpCode = B.EmpCode And A.StartDt = B.StartDt; ");

            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "ADCode", "StartDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lADB.Add(new ADBefore()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            ADCode = "AD" + Sm.DrStr(dr, c[1]),
                            StartDt = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Social Security Program

        private void ProcessSSP()
        {
            var lSSPAf = new List<SSPAfter>();
            var lSSPB = new List<SSPBefore>();
            
            ProcessSSPAfter(ref lSSPAf);
            ProcessSSPBefore(ref lSSPAf, ref lSSPB);

            if (lSSPAf.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    for (int j = 0; j < lSSPAf.Count; j++)
                    {
                        for (int k = 0; k < lA.Count; k++)
                        {
                            if (string.Compare(Sm.GetGrdStr(Grd1, i, 1), lSSPAf[j].EmpCode) == 0)
                            {
                                if (lSSPAf[j].SSPCode == lA[k].Code)
                                {
                                    Grd1.Cells[i, lA[k].Col].Value = Sm.GetGrdDec(Grd1, i, lA[k].Col) + lSSPAf[j].Amt;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }

            if (lSSPB.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    for (int j = 0; j < lSSPB.Count; j++)
                    {
                        for (int k = 0; k < l.Count; k++)
                        {
                            if (string.Compare(Sm.GetGrdStr(Grd1, i, 1), lSSPB[j].EmpCode) == 0)
                            {
                                if (lSSPB[j].SSPCode == l[k].Code)
                                {
                                    Grd1.Cells[i, l[k].Col].Value = Sm.GetGrdDec(Grd1, i, l[k].Col) + lSSPB[j].Amt;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void ProcessSSPAfter(ref List<SSPAfter> lSSPAf)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("select A.EmpCode, A.DNo, A.SSPCode, A.StartDt, A.Amt ");
            SQL.AppendLine("from TblEmployeeSalarySS A ");
            SQL.AppendLine("Inner join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select EmpCode, SSPCode, Max(StartDt) StartDt ");
            SQL.AppendLine("    From TblEmployeeSalarySS ");
            SQL.AppendLine("    Group By EmpCode, SSPCode ");
            SQL.AppendLine(") B On A.EmpCode = B.EmpCode And A.SSPCode = B.SSPCode And A.StartDt = B.StartDt ");

            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "SSPCode", "StartDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lSSPAf.Add(new SSPAfter()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            SSPCode = "SSP" + Sm.DrStr(dr, c[1]),
                            StartDt = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessSSPBefore(ref List<SSPAfter> lSSPAf, ref List<SSPBefore> lSSPB)
        {
            var SQL = new StringBuilder();
            string Filter = string.Empty;

            //if (lSSPAf.Count > 0)
            //{
            //    for (int i = 0; i < lSSPAf.Count; i++)
            //    {
            //        if (Filter.Length != 0) Filter += " Or ";
            //        Filter += " (EmpCode = '" + lSSPAf[i].EmpCode + "' And StartDt = '" + lSSPAf[i].StartDt + "' And SSPCode = '" + Sm.Right(lSSPAf[i].SSPCode, (lSSPAf[i].SSPCode.Length - 3)) + "' ) ";
            //    }
            //}

            #region Old Code - Source dari document document EmpSalary

            //SQL.AppendLine("Select X1.EmpCode, X1.SSPCode, X1.StartDt, X1.Amt ");
            //SQL.AppendLine("From ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select A.EmpCode, B.SSPCode, B.StartDt, B.Amt, A.CreateDt ");
            //SQL.AppendLine("    From TblEmpSalaryHdr A ");
            //SQL.AppendLine("    Inner Join TblEmpSalary3Dtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("    Where A.Status = 'A' ");
            //SQL.AppendLine("    And B.StartDt Is Not Null ");
            //SQL.AppendLine(") X1 ");
            //SQL.AppendLine("Inner Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select T1.EmpCode, T1.SSPCode, Max(T1.StartDt) StartDt, Max(CreateDt) CreateDt ");
            //SQL.AppendLine("    From ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select A.DocNo, A.EmpCode, B.SSPCode, B.StartDt, A.CreateDt ");
            //SQL.AppendLine("        From TblEmpSalaryHdr A ");
            //SQL.AppendLine("        Inner Join TblEmpSalary3Dtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("        Where StartDt Is Not Null ");
            //SQL.AppendLine("        And Not ( " + Filter + " )  ");
            //SQL.AppendLine("    ) T1 ");
            //SQL.AppendLine("    Group By T1.EmpCode,  T1.SSPCode ");
            //SQL.AppendLine(")X2 On X1.EmpCode = X2.EmpCode And X1.SSPCode = X2.SSPCode And X1.StartDt = X2.StartDt And X1.CreateDt = X2.CreateDt ");

            #endregion

            SQL.AppendLine("select A.EmpCode, A.SSPCode, A.StartDt, A.Amt ");
            SQL.AppendLine("from tblemployeesalaryss A ");
            SQL.AppendLine("inner join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T.EmpCode, Max(T.StartDt) StartDt ");
            SQL.AppendLine("    From tblemployeesalaryss T ");
            SQL.AppendLine("    Where Concat(EmpCode, StartDt, SSPCode) Not In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Concat(X1.EmpCode, X1.StartDt, X1.SSPCode) ");
            SQL.AppendLine("        from TblEmployeeSalarySS X1 ");
            SQL.AppendLine("        Inner join  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select EmpCode, SSPCode, Max(StartDt) StartDt ");
            SQL.AppendLine("            From TblEmployeeSalarySS ");
            SQL.AppendLine("            Group By EmpCode, SSPCode ");
            SQL.AppendLine("        ) X2 On X1.EmpCode = X2.EmpCode And X1.SSPCode = X2.SSPCode And X1.StartDt = X2.StartDt ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By T.EmpCode ");
            SQL.AppendLine(") B ON A.EmpCode = B.EmpCode And A.StartDt = B.StartDt; ");

            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "SSPCode", "StartDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lSSPB.Add(new SSPBefore()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            SSPCode = "SSP" + Sm.DrStr(dr, c[1]),
                            StartDt = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }

        }

        #endregion

        private void SetAmendment(ref List<Amendment> l)
        {
            var GrdCol = 20;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.Code, T.Name From ( ");
            SQL.AppendLine("   Select Concat('AD', AdCode) As Code, AdName As Name ");
            SQL.AppendLine("   From tblallowancededuction ");
            SQL.AppendLine("   Where ADType = 'A' ");
            SQL.AppendLine("   Union All ");
            SQL.AppendLine("   Select Concat('AD', AdCode) As Code, AdName As Name ");
            SQL.AppendLine("   From tblallowancededuction ");
            SQL.AppendLine("   Where ADType = 'D' ");

            if (mIsUseEmpSalarySS)
            {
                SQL.AppendLine("   Union All ");
                SQL.AppendLine("   Select Concat('SSP', SSPCode) As Code, SSPName As Name ");
                SQL.AppendLine("   From tblssprogram ");
            }
            SQL.AppendLine(")T ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Code", "Name" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        GrdCol += 2;
                        l.Add(new Amendment()
                        {
                            Code = Sm.DrStr(dr, c[0]),
                            Name = Sm.DrStr(dr, c[1]),
                            Col = GrdCol
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SetAmendmentAfter(ref List<AmendmentAfter> lA)
        {
            int GrdCol = 21;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.Code, T.Name From ( ");
            SQL.AppendLine("   Select Concat('AD', AdCode) As Code, AdName As Name ");
            SQL.AppendLine("   From tblallowancededuction ");
            SQL.AppendLine("   Where ADType = 'A' ");
            SQL.AppendLine("   Union All ");
            SQL.AppendLine("   Select Concat('AD', AdCode) As Code, AdName As Name ");
            SQL.AppendLine("   From tblallowancededuction ");
            SQL.AppendLine("   Where ADType = 'D' ");

            if (mIsUseEmpSalarySS)
            {
                SQL.AppendLine("   Union All ");
                SQL.AppendLine("   Select Concat('SSP', SSPCode) As Code, SSPName As Name ");
                SQL.AppendLine("   From tblssprogram ");
            }
            SQL.AppendLine(")T ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Code", "Name" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        GrdCol += 2;
                        lA.Add(new AmendmentAfter()
                        {
                            Code = Sm.DrStr(dr, c[0]),
                            Name = Sm.DrStr(dr, c[1]),
                            Col = GrdCol
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Active Employee' As Col2 Union All " +
                "Select '2' As Col1, 'Resignee' As Col2; ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void SetLueEmpStatus(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
                SQL.AppendLine("From TblOption ");
                SQL.AppendLine("Where OptCat='EmploymentStatus' ");
                SQL.AppendLine("Order By OptDesc; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };


                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueEmpStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmpStatus, new Sm.RefreshLue1(SetLueEmpStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEmpStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employee status");
        }

        #endregion

        #endregion

        #region Class

        private class Amendment
        {
            public string Code { get; set; }
            public string Name { get; set; }
            public int Col { get; set; }
        }

        private class AmendmentAfter
        {
            public string Code { get; set; }
            public string Name { get; set; }
            public int Col { get; set; }
        }

        private class SalaryAfter
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
            public string AfterInd { get; set; }
        }

        private class SalaryBefore
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
            public string AfterInd { get; set; }
        }

        private class PerformanceAfter
        {
            public string DocNo { get; set; }
            public string EmpCode { get; set; }
            public string GradePerformance { get; set; }
            public string PerformanceStatus { get; set; }
            public decimal Value { get; set; }
            public string CreateDt { get; set; }
        }

        private class PerformanceBefore
        {
            public string DocNo { get; set; }
            public string EmpCode { get; set; }
            public string GradePerformance { get; set; }
            public string PerformanceStatus { get; set; }
            public decimal Value { get; set; }
            public string CreateDt { get; set; }
        }

        private class Performance
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CreateDt { get; set; }
            public string EmpCode { get; set; }
            public string PerformanceStatus { get; set; }
            public string GradePerformance { get; set; }
            public decimal Value { get; set; }
        }

        private class ADAfter
        {
            public string EmpCode { get; set; }
            public string DNo { get; set; }
            public string ADCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class ADBefore
        {
            public string EmpCode { get; set; }
            public string DNo { get; set; }
            public string ADCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class SSPAfter
        {
            public string EmpCode { get; set; }
            public string SSPCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class SSPBefore
        {
            public string EmpCode { get; set; }
            public string SSPCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion
    }
}
