﻿#region Update
/*
    07/02/2023 [WED/MNET] new apps. semacam DOCt, tapi item gak masuk stock, dan hanya item yang sales dan service
                          https://docs.google.com/spreadsheets/d/1lB6wtZRlw7ltgvXjWOAsXHXAOM6MqoTj/edit#gid=1709999113
    10/02/2023 [WED/MNET] validasi tidak bisa edit kalau sudah diproses di Sales Invoice aktif
    20/02/2023 [WED/MNET] validasi nomor Service Delivery dirubah ke TblSalesInvoiceDtl (tadinya TblSalesInvoiceDtl5)
    23/03/2023 [RDA/MNET] tambah tab coa account list berdasarkan parameter IsSDUseCOAAccountList & IsCOAAccountListSDMandatory
    03/04/2023 [WED/MNET] salah set LueCCCode saat refresh menjadi LueCtCode
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmServiceDelivery : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mCityCode = string.Empty,
            mCntCode = string.Empty,
            mMainCurCode = string.Empty
            ;

        internal int
            mNumberOfInventoryUomCode = 2,
            mQty1Cols = 8,
            mQty2Cols = 10,
            mQty3Cols = 12,
            mServicePriceCols = 14,
            mAmtDtlCols = 15,
            mItCodeCols = 5,
            mUom1Cols = 9,
            mUom2Cols = 11,
            mUom3Cols = 13
            ;

        internal bool
            mIsFilterByCtCt = false,
            IsInsert = false,
            mIsAutoJournalActived = false,
            mIsCheckCOAJournalNotExists = false,
            mIsFilterByCC = false
            ;
        internal FrmServiceDeliveryFind FrmFind;

        private bool
            mIsSDUseCOAAccountList = false,
            mIsCOAAccountListSDMandatory = false
            ;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmServiceDelivery(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Service Delivery";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                ExecQuery();
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCtCode(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueCCCode(ref LueCCCode, string.Empty, mIsFilterByCC ? "Y" : "N");

                if (!mIsSDUseCOAAccountList) Tp3.PageVisible = false;

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsFilterByCC', 'IsCheckCOAJournalNotExists', 'IsFilterByCtCt', 'IsAutoJournalActived', ");
            SQL.AppendLine("'MainCurCode', 'NumberOfInventoryUomCode', 'IsSDUseCOAAccountList', 'IsCOAAccountListSDMandatory' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsFilterByCC": mIsFilterByCC = ParValue == "Y"; break;
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsFilterByCtCt": mIsFilterByCtCt = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsSDUseCOAAccountList": mIsSDUseCOAAccountList = ParValue == "Y"; break;
                            case "IsCOAAccountListSDMandatory": mIsCOAAccountListSDMandatory = ParValue == "Y"; break;

                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;

                            //Integer
                            case "NumberOfInventoryUomCode":
                                if (ParValue.Length == 0)
                                    mNumberOfInventoryUomCode = 1;
                                else
                                    mNumberOfInventoryUomCode = int.Parse(ParValue);
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "DNo",
                        
                    //1-5
                    "Cancel",
                    "Old Cancel",
                    "Reason for Cancellation",
                    "",
                    "Item's Code",
                        
                    //6-10
                    "",
                    "Item's Name",
                    "Quantity",
                    "UoM",
                    "Quantity 2",

                    //11-15
                    "UoM 2",
                    "Quantity 3",
                    "UoM 3",
                    "Service's Price",
                    "Amount",

                    //16
                    "Remark",
                }, new int[]
                {
                    //0
                    0,
                        
                    //1-5
                    50, 50, 200, 20, 180, 
                        
                    //6-10
                    20, 250, 100, 200, 100,
                        
                    //11-15
                    200, 100, 200, 180, 180, 
                        
                    //16
                    180
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { mQty1Cols, mQty2Cols, mQty3Cols, mServicePriceCols, mAmtDtlCols }, 0);
            Sm.GrdColButton(Grd1, new int[] { 4, 6 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 5, 7, 9, 11, 13, 16, mQty1Cols, mQty2Cols, mQty3Cols, mServicePriceCols, mAmtDtlCols });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5, 6, mQty2Cols, 11, mQty3Cols, 13 });
            ShowInventoryUomCode();

            #endregion

            #region Grid 2 - Approval Info

            Grd2.Cols.Count = 5;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "No",

                    //1-4
                    "User",
                    "Status",
                    "Date",
                    "Remark"
                },
                new int[] { 50, 150, 200, 100, 200 }
            );
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 2, 3, 4 });

            #endregion

            #region Grid 3 - COA's Account List

            Grd3.Cols.Count = 6;
            Grd3.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit Amount",
                        "Credit Amount",
                        "Remark"
                    },
                     new int[]
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 150, 150, 150
                    }
                );
            
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColReadOnly(Grd3, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd3, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 2 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6 }, !ChkHideInfoInGrd.Checked);
            ShowInventoryUomCode();
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { mQty2Cols, 11 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { mQty2Cols, 11, mQty3Cols, 13 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, LueCtCode, TxtDocNoInternal, 
                        TxtResiNo, TxtDocNoInternal, LueCurCode, MeeRemark, LueCCCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, mQty1Cols, mQty2Cols, mQty3Cols, mServicePriceCols, 16 });
                    BtnCustomerShipAddress.Enabled = false;
                    BtnSAName.Enabled = false;
                    BtnDOCtDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 3, 4, 5 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, LueCtCode, TxtDocNoInternal, LueCurCode,
                        TxtResiNo, MeeRemark, LueCCCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4, mQty1Cols, mQty2Cols, mQty3Cols, mServicePriceCols, 16 });
                    BtnCustomerShipAddress.Enabled = true;
                    BtnSAName.Enabled = true;
                    BtnDOCtDocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5 });
                    DteDocDt.Focus();
                    break;

                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mCityCode = mCntCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, TxtStatus, LueCtCode,
                TxtDocNoInternal, MeeRemark,
                LueCurCode, TxtSAName, MeeSAAddress, TxtCity, TxtCountry,
                TxtPostalCd, TxtPhone, TxtFax, TxtEmail, TxtResiNo,
                TxtMobile, TxtDOCtDocNo, LueCCCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { mQty1Cols, mQty2Cols, mQty3Cols, mServicePriceCols, mAmtDtlCols });

            Sm.ClearGrd(Grd2, false);

            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 3, 4 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmServiceDeliveryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                Sl.SetLueCtCode(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                IsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                //if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                //    Sm.StdMsgYN("Print", "") == DialogResult.No
                //    ) return;
                //ParPrint((int)mNumberOfInventoryUomCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (Sm.IsGrdColSelected(new int[] { 4, 6, mQty1Cols, mQty2Cols, mQty3Cols, mServicePriceCols, mAmtDtlCols }, e.ColIndex))
                    {
                        if (e.ColIndex == 4)
                        {
                            e.DoDefault = false;
                            if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmServiceDeliveryDlg(this));
                        }

                        if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, mItCodeCols).Length != 0)
                        {
                            e.DoDefault = false;
                            var f = new FrmItem(mMenuCode);
                            f.Tag = mMenuCode;
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, mItCodeCols);
                            f.ShowDialog();
                        }
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { mQty1Cols, mQty2Cols, mQty3Cols, mServicePriceCols, mAmtDtlCols });
                    }
                }
                else
                {
                    if (!(
                        (e.ColIndex == 1 || e.ColIndex == 3) &&
                        !Sm.GetGrdBool(Grd1, e.RowIndex, 2) &&
                        Sm.GetGrdStr(Grd1, e.RowIndex, mItCodeCols).Length != 0
                        ))
                        e.DoDefault = false;
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmt();
            }
            
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4)
            {
                Sm.FormShowDialog(new FrmServiceDeliveryDlg(this));
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, mItCodeCols).Length != 0)
            { 
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, mItCodeCols);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { mQty1Cols, mQty2Cols, mQty3Cols, mServicePriceCols, mAmtDtlCols }, e);

                if (e.ColIndex == mQty1Cols)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, mItCodeCols, mQty1Cols, mQty2Cols, mQty3Cols, mUom1Cols, mUom2Cols, mUom3Cols);
                    Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, mItCodeCols, mQty1Cols, mQty3Cols, mQty2Cols, mUom1Cols, mUom3Cols, mUom2Cols);
                }

                if (e.ColIndex == mQty2Cols)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, mItCodeCols, mQty2Cols, mQty1Cols, mQty3Cols, mUom2Cols, mUom1Cols, mUom3Cols);
                    Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, mItCodeCols, mQty2Cols, mQty3Cols, mQty1Cols, mUom2Cols, mUom3Cols, mUom1Cols);
                }

                if (e.ColIndex == mQty3Cols)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, mItCodeCols, mQty3Cols, mQty1Cols, mQty2Cols, mUom3Cols, mUom1Cols, mUom2Cols);
                    Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, mItCodeCols, mQty3Cols, mQty2Cols, mQty1Cols, mUom3Cols, mUom2Cols, mUom1Cols);
                }

                if (e.ColIndex == mQty1Cols && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, mUom1Cols), Sm.GetGrdStr(Grd1, e.RowIndex, mUom2Cols)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, mQty2Cols, Grd1, e.RowIndex, mQty1Cols);

                if (e.ColIndex == mQty1Cols && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, mUom1Cols), Sm.GetGrdStr(Grd1, e.RowIndex, mUom3Cols)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, mQty3Cols, Grd1, e.RowIndex, mQty1Cols);

                if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, mUom2Cols), Sm.GetGrdStr(Grd1, e.RowIndex, mUom3Cols)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, mQty3Cols, Grd1, e.RowIndex, mQty2Cols);                

                if (Sm.IsGrdColSelected(new int[] { 1, 3, mQty1Cols, mQty2Cols, mQty3Cols, mServicePriceCols, mAmtDtlCols }, e.ColIndex))
                {
                    if (e.ColIndex == 3)
                    {
                        if (Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length > 0)
                        {
                            Grd1.Cells[e.RowIndex, 1].Value = true;
                        }
                    }

                    if (e.ColIndex == 1)
                    {
                        if (!Sm.GetGrdBool(Grd1, e.RowIndex, 1))
                        {
                            Grd1.Cells[e.RowIndex, 3].Value = string.Empty;
                        }
                    }

                    ComputeTotal(e.RowIndex);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { mQty1Cols, mQty2Cols, mQty3Cols, mAmtDtlCols }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ServiceDelivery", "TblServiceDeliveryHdr");
            var cml = new List<MySqlCommand>();

            cml.Add(SaveServiceDeliveryHdr(DocNo));
            cml.Add(SaveServiceDeliveryDtl(DocNo));
            if (mIsSDUseCOAAccountList && Grd3.Rows.Count > 1) cml.Add(SaveServiceDeliveryDtl2(DocNo));

            if (mIsAutoJournalActived)
            {
                string JNDocNo = Sm.GetValue(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));

                cml.Add(SaveJournal(DocNo, mMenuCode, JNDocNo));
            }

            IsInsert = false;

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueCCCode, "Cost Center") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, false, Sm.GetDte(DteDocDt), GetProfitCenterCode()) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt)) ||
                IsJournalSettingInvalid() ||
                IsSelectedDOCtInvalid();
        }

        private bool IsSelectedDOCtInvalid()
        {
            if (TxtDOCtDocNo.Text.Trim().Length == 0) return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Group_Concat(Distinct A.DocNo) ");
            SQL.AppendLine("From TblServiceDeliveryHdr A ");
            SQL.AppendLine("Where A.Status In ('O', 'A') ");
            SQL.AppendLine("And A.DOCtDocNo = @Param ");
            SQL.AppendLine("And Not Exists ( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblServiceDeliveryHdr T1 ");
            SQL.AppendLine("    Inner Join TblServiceDeliveryDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    And T1.DocNo = A.DocNo ");
            SQL.AppendLine("    And T2.CancelInd = 'N' ");
            SQL.AppendLine("); ");

            string result = Sm.GetValue(SQL.ToString(), TxtDOCtDocNo.Text);

            if (result.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "This DO# is being used by another Service Delivery document : " + result);
                TxtDOCtDocNo.Focus();
                return true;
            }

            return false;
        }

        private string GetProfitCenterCode()
        {
            var Value = Sm.GetLue(LueCCCode);
            if (Value.Length == 0) return string.Empty;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select ProfitCenterCode ");
            SQL.AppendLine("From TblCostCenter ");
            SQL.AppendLine("Where CCCode = @Param ");
            SQL.AppendLine("And ProfitCenterCode Is Not Null; ");

            return Sm.GetValue(SQL.ToString(), Value);
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }

            if (Grd3.Rows.Count == 1 && mIsCOAAccountListSDMandatory)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 COA Account List.");
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, mItCodeCols, false, "Item is empty.")) return true;

                Msg =
                   "Item's Code : " + Sm.GetGrdStr(Grd1, Row, mItCodeCols) + Environment.NewLine +
                   "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, mQty1Cols) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    Sm.FocusGrd(Grd1, Row, mQty1Cols);
                    return true;
                }

                if (Grd1.Cols[mQty2Cols].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, mQty2Cols) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        Sm.FocusGrd(Grd1, Row, mQty2Cols);
                        return true;
                    }
                }

                if (Grd1.Cols[mQty3Cols].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, mQty3Cols) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        Sm.FocusGrd(Grd1, Row, mQty3Cols);
                        return true;
                    }
                }
            }

            if (mIsSDUseCOAAccountList) 
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.IsGrdValueEmpty(Grd3, Row, 5, false, "Remark is empty."))
                    {
                        Sm.FocusGrd(Grd3, Row, 5);
                        return true; 
                    }
                }

                //check balance
                decimal DAmtTotal = 0m, CAmtTotal = 0m;
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                    {
                        if (Sm.GetGrdDec(Grd3, Row, 3) > 0)
                        {
                            DAmtTotal += Sm.GetGrdDec(Grd3, Row, 3);
                        }

                        if (Sm.GetGrdDec(Grd3, Row, 4) > 0)
                        {
                            CAmtTotal += Sm.GetGrdDec(Grd3, Row, 4);
                        }
                    }
                }
                if (DAmtTotal - CAmtTotal != 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "COA's Account List is not balance");
                    return true;
                } 

            }

            return false;
        }

        private bool IsJournalSettingInvalid()
        {
            if (mIsAutoJournalActived)
            {
                var Msg =
                        "Journal's setting is invalid." + Environment.NewLine +
                        "Please contact Finance/Accounting department." + Environment.NewLine;
                
                if (!mIsCheckCOAJournalNotExists) return false;

                string mCustomerAcNoNonInvoice = Sm.GetValue("Select ParValue From TblParameter Where Parcode='CustomerAcNoNonInvoice'");

                // Table
                if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo4")) return true;
                // Parameter
                if (mCustomerAcNoNonInvoice.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoNonInvoice is empty.");
                    return true;
                }
            }
            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg, string COA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;
            string AcName = string.Empty;
            if (COA == "AcNo") AcName = "(Stock)";
            if (COA == "AcNo4") AcName = "(Sales)";
            if (COA == "AcNo5") AcName = "(COGS)";
            if (COA == "AcNo11") AcName = "(AR Uninvoiced)";

            SQL.AppendLine("Select B.ItCtName ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode = B.ItCtCode ");
            SQL.AppendLine("Where B." + COA + " Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, mItCodeCols);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                    r++;
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account" + AcName + "# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveServiceDeliveryHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* SaveServiceDeliveryHdr */ ");

            SQL.AppendLine("Insert Into TblServiceDeliveryHdr(DocNo, DocDt, Status, CtCode, DocNoInternal, ");
            SQL.AppendLine("DOCtDocNo, CCCode, CurCode, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, ");
            SQL.AppendLine("SAPhone, SAFax, SAEmail, SAMobile, ResiNo, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @CtCode, @DocNoInternal, ");
            SQL.AppendLine("@DOCtDocNo, @CCCode, @CurCode, @SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCd, ");
            SQL.AppendLine("@SAPhone, @SAFax, @SAEmail, @SAMobile, @ResiNo, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='ServiceDelivery' ");
            SQL.AppendLine("And T.DeptCode In ( ");
            SQL.AppendLine("    Select DeptCode From TblCostCenter Where CCCode=@CCCode ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblServiceDeliveryHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select D1.CurCode1, D1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate D1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblServiceDeliveryHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='ServiceDelivery' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@DocNoInternal", TxtDocNoInternal.Text);
            Sm.CmParam<String>(ref cm, "@DOCtDocNo", TxtDOCtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@SAName", TxtSAName.Text);
            Sm.CmParam<String>(ref cm, "@SAAddress", MeeSAAddress.Text);
            Sm.CmParam<String>(ref cm, "@SACityCode", mCityCode);
            Sm.CmParam<String>(ref cm, "@SACntCode", mCntCode);
            Sm.CmParam<String>(ref cm, "@SAPostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@SAPhone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@SAFax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@SAEmail", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@SAMobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@ResiNo", TxtResiNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveServiceDeliveryDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* SaveServiceDeliveryDtl */ ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, mItCodeCols).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblServiceDeliveryDtl(DocNo, DNo, CancelInd, ItCode, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                    {
                        SQL.AppendLine(", ");
                    }

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", 'N', @ItCode_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @Qty2_" + r.ToString() +
                        ", @Qty3_" + r.ToString() +
                        ", @UPrice_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @CreateBy, CurrentDateTime()) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, mItCodeCols));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, mQty1Cols));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, mQty2Cols));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, mQty3Cols));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, mServicePriceCols));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 16));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveServiceDeliveryDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Service Delivery - Dtl2 */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblServiceDeliveryDtl2 ");
                        SQL.AppendLine("(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt ");
                    SQL.AppendLine(") ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 4));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 5));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        public MySqlCommand SaveJournal(
            string DocNo, string MenuCode, string JNDocNo
            )
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblServiceDeliveryHdr Set ");
            SQL.AppendLine("    JournalDocNo = @JournalDocNo ");
            SQL.AppendLine(" Where DocNo=@DocNo And Status='A'; ");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, A.DocDt, ");
            SQL.AppendLine("Concat('Service Delivery : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("A.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblServiceDeliveryHdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status='A'; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('000', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, E.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            SQL.AppendLine(JournalQuery(false, string.Empty, false));
            SQL.AppendLine(JournalCOAAccountList(false));
            
            SQL.AppendLine("    ) Tbl Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Inner Join TblServiceDeliveryHdr C On C.DocNo = @DocNo ");
            SQL.AppendLine("Left Join TblCostCenter D On C.CCCode = D.CCCode ");
            SQL.AppendLine("Left Join TblProfitCenter E On D.ProfitCenterCode = E.ProfitCenterCode ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblServiceDeliveryHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", MenuCode);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JNDocNo);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            UpdateCancelledItem();

            string DNo = "##XXX##";
            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, mItCodeCols).Length > 0)
                {
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

                    cml.Add(EditServiceDeliveryDtl(Row));
                }
            }

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsEditedDataNotValid(DNo))
            {
                cml.Clear();
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            cml.Add(UpdateServiceDeliveryHdr(TxtDocNo.Text));

            if (mIsAutoJournalActived)
            {
                var CurrentDt = Sm.ServerCurrentDate();
                var DocDt = Sm.GetDte(DteDocDt);
                var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);
                var SelectedDt = DocDt;
                if (IsClosingJournalUseCurrentDt) SelectedDt = CurrentDt;
                string Filter = string.Empty;
                var cm = new MySqlCommand();

                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, mItCodeCols).Length > 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (B.DNo=@DNo00" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                    }
                }

                if (Filter.Length > 0) Filter = " And (" + Filter + ") ";

                string JNDocNo = Sm.GenerateDocNoJournal(SelectedDt, 1);
                cml.Add(SaveJournalCancel(TxtDocNo.Text, JNDocNo, Filter, ref cm, IsClosingJournalUseCurrentDt));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);

            cml.Clear();
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblServiceDeliveryDtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                {
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                }
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsEditedDataNotValid(string DNo)
        {
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode()) ||
                IsServiceDeliveryStatusCancelled() ||
                IsCancelledItemNotExisted(DNo) ||
                IsItemCancelledInvalid() ||
                IsCancelReasonEmpty(DNo) ||
                (mIsAutoJournalActived && mIsCheckCOAJournalNotExists && IsCOAJournalEditNotValid()) ||
                IsDataAlreadyUsedInRecvCt(DNo) || IsDataAlreadyUsedInSalesInvoice(DNo);
        }

        private bool IsItemCancelledInvalid()
        {
            bool IsCancelled = Sm.GetGrdBool(Grd1, 0, 1);
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, mItCodeCols).Length > 0)
                {
                    if (Sm.GetGrdBool(Grd1, r, 1) != IsCancelled)
                    {
                        Sm.StdMsg(mMsgType.Warning, "You need to cancel ALL items.");
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsCancelReasonEmpty(string DNo)
        {
            for (int Row = 0; Row < Grd1.Rows.Count; ++Row)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 3).Length == 0)
                {
                    Sm.FocusGrd(Grd1, Row, 3);
                    Sm.StdMsg(mMsgType.Warning, "Reason for cancellation could not be empty.");
                    return true;
                }
            }

            return false;
        }

        private bool IsServiceDeliveryStatusCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 from TblServiceDeliveryHdr Where DocNo=@Param And Status='C';",
                TxtDocNo.Text,
                "Document is cancelled by approval process.");
        }

        private bool IsCOAJournalEditNotValid()
        {
            var l = new List<COA>();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            var cm = new MySqlCommand();

            //for (int r = 0; r < Grd1.Rows.Count; r++)
            //{
            //    if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
            //    {
            //        if (Filter.Length > 0) Filter += " Or ";
            //        Filter += " (B.DNo=@DNo00" + r.ToString() + ") ";
            //        Sm.CmParam<String>(ref cm, "@DNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
            //    }
            //}

            //if (Filter.Length > 0) Filter = " And (" + Filter + ") ";


            SQL.AppendLine("Select AcNo From ");
            SQL.AppendLine("( ");

            SQL.AppendLine(JournalQuery(true, string.Empty, false));

            SQL.AppendLine(")T ; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();

                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0])

                        });
                    }
                }
                dr.Close();
            }

            foreach (var x in l.Where(w => w.AcNo.Length <= 0))
            {
                Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for creating journal transaction.");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyUsedInRecvCt(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo From TblRecvCtDtl ");
            SQL.AppendLine("Where DOCtDocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Position(Concat('##', DOCtDNo, '##') In @DNo)>0 Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);

            var key = Sm.GetValue(cm);
            if (key.Length != 0)
            {
                string Msg = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0) == key)
                    {
                        Msg =
                           "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                           "Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                           "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                           "Property : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                           "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                           "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                           "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                           "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;
                        break;
                    }
                }
                Sm.StdMsg(mMsgType.Warning, Msg + Environment.NewLine + "Data already used in receiving from customer transactions.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyUsedInSalesInvoice(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DOCtDNo As KeyWord ");
            SQL.AppendLine("from tblSalesInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl B On A.DocNo = B.Docno ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And B.DOCtDocNo=@DocNo ");
            SQL.AppendLine("And Position(Concat('##', B.DOCtDNo, '##') In @DNo)>0 Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);

            var key = Sm.GetValue(cm);
            if (key.Length != 0)
            {
                string Msg = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0) == key)
                    {
                        Msg =
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, mItCodeCols) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine;
                        break;
                    }
                }
                Sm.StdMsg(mMsgType.Warning, Msg + Environment.NewLine + "Data already used in sales invoice transactions.");
                return true;
            }
            
            return false;
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "##XXX##"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel minimum 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand UpdateServiceDeliveryHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblServiceDeliveryHdr Set ");
            SQL.AppendLine("    Amt = @Amt, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));

            return cm;
        }

        private MySqlCommand EditServiceDeliveryDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblServiceDeliveryDtl Set CancelInd='Y', CancelReason = @CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo = @DNo ");
            SQL.AppendLine("And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        public MySqlCommand SaveJournalCancel(
            string DocNo, string JNDocNo, string Filter,
            ref MySqlCommand cm, bool IsClosingJournalUseCurrentDt
            )
        {
            var SQL = new StringBuilder();            

            if (Filter.Length > 0)
            {
                SQL.AppendLine("Update TblServiceDeliveryDtl Set JournalDocNo=@JournalDocNo ");
                SQL.AppendLine("Where DocNo=@DocNo " + Filter.Replace("B.", string.Empty));
                SQL.AppendLine("And Exists(Select 1 From TblServiceDeliveryHdr Where DocNo=@DocNo And JournalDocNo Is Not Null) ");
                SQL.AppendLine("And JournalDocNo Is Null; ");

                SQL.AppendLine("Insert Into TblJournalHdr ");
                SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @JournalDocNo, ");
                if (IsClosingJournalUseCurrentDt)
                    SQL.AppendLine("Replace(CurDate(), '-', ''), ");
                else
                    SQL.AppendLine("DocDt, ");
                SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
                SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblJournalHdr ");
                SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblServiceDeliveryHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

                SQL.AppendLine("Set @Row:=0; ");

                SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, E.EntCode, A.CreateBy, A.CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

                SQL.AppendLine(JournalQuery(false, Filter, true));
                SQL.AppendLine(JournalCOAAccountList(true));

                SQL.AppendLine("    ) Tbl Where AcNo Is Not Null Group By AcNo ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Inner Join TblServiceDeliveryHdr C On C.DocNo = @DocNo ");
                SQL.AppendLine("Left Join TblCostCenter D On C.CCCode = D.CCCode ");
                SQL.AppendLine("Left Join TblProfitCenter E On D.ProfitCenterCode = E.ProfitCenterCode ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo; ");
            }
            else
            {
                SQL.AppendLine("Select 1; ");
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JNDocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowServiceDeliveryHdr(DocNo);
                ShowServiceDeliveryDtl(DocNo);
                ShowServiceDeliveryDtl2(DocNo);
                Sm.ShowDocApproval(DocNo, "ServiceDelivery", ref Grd2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowServiceDeliveryHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' Else '' End As StatusDesc, ");
            SQL.AppendLine("A.CtCode, A.DocNoInternal, A.DOCtDocNo, A.CCCode, A.CurCode, A.SAName, A.SAAddress, A.SACityCode, B.CityName, A.SACntCode, C.CntName, ");
            SQL.AppendLine("A.SAPostalCd, A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile, A.ResiNo, A.JournalDocNo, A.Amt, A.Remark ");
            SQL.AppendLine("From TblServiceDeliveryHdr A ");
            SQL.AppendLine("Left Join TblCity B On A.SACityCode = B.CityCode ");
            SQL.AppendLine("Left Join TblCountry C On A.SACntCode = C.CntCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "StatusDesc", "CtCode", "DocNoInternal", "SAName",  

                    //6-10
                    "SAAddress", "SACityCode", "CityName", "SACntCode", "CntName",  

                    //11-15
                    "SAPostalCd", "SAPhone", "SAFax", "SAEmail", "SAMobile",

                    //16-20
                    "CurCode", "ResiNo", "Remark", "DOCtDocNo", "CCCode",

                    //21
                    "Amt"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                    Sl.SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[3]));
                    TxtDocNoInternal.EditValue = Sm.DrStr(dr, c[4]);
                    TxtSAName.EditValue = Sm.DrStr(dr, c[5]);
                    MeeSAAddress.EditValue = Sm.DrStr(dr, c[6]);
                    mCityCode = Sm.DrStr(dr, c[7]);
                    TxtCity.EditValue = Sm.DrStr(dr, c[8]);
                    mCntCode = Sm.DrStr(dr, c[9]);
                    TxtCountry.EditValue = Sm.DrStr(dr, c[10]);
                    TxtPostalCd.EditValue = Sm.DrStr(dr, c[11]);
                    TxtPhone.EditValue = Sm.DrStr(dr, c[12]);
                    TxtFax.EditValue = Sm.DrStr(dr, c[13]);
                    TxtEmail.EditValue = Sm.DrStr(dr, c[14]);
                    TxtMobile.EditValue = Sm.DrStr(dr, c[15]);
                    Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[16]));
                    TxtResiNo.EditValue = Sm.DrStr(dr, c[17]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[18]);
                    TxtDOCtDocNo.EditValue = Sm.DrStr(dr, c[19]);
                    Sm.SetLue(LueCCCode, Sm.DrStr(dr, c[20]));
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[21]), 0);
                }, true
            );
        }

        private void ShowServiceDeliveryDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.CancelInd, B.CancelReason, B.ItCode, C.ItName, B.Qty, C.InventoryUOMCode Uom, ");
            SQL.AppendLine("B.Qty2, C.InventoryUOMCode2 Uom2, B.Qty3, C.InventoryUOMCode3 Uom3, B.UPrice, (B.UPrice * B.Qty) Amt, ");
            SQL.AppendLine("B.Remark ");
            SQL.AppendLine("From TblServiceDeliveryHdr A ");
            SQL.AppendLine("Inner Join TblServiceDeliveryDtl B On A.DocNo = B.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Order By B.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "CancelReason", "ItCode", "ItName", "Qty", 
                    
                    //6-10
                    "Uom", "Qty2", "Uom2", "Qty3", "Uom3", 
                    
                    //11-13
                    "UPrice", "Amt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { mQty1Cols, mQty2Cols, mQty3Cols, mServicePriceCols, mAmtDtlCols });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowServiceDeliveryDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark, Null as LocalName ");
            SQL.AppendLine("From TblServiceDeliveryDtl2 A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] { "AcNo", "AcDesc", "DAmt", "CAmt", "Remark", "LocalName" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        #endregion

        #region Additional Method

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT IGNORE INTO `tbldocabbreviation` (`DocType`, `DocAbbr`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('ServiceDelivery', 'SD', 'WEDHA', '202308070000', NULL, NULL); ");

            Sm.ExecQuery(SQL.ToString());
        }

        private string JournalQuery(bool IsForValidation, string Filter, bool IsCancel)
        {
            var SQL = new StringBuilder();
            string CancelInd = IsCancel ? "Y" : "N";

            SQL.AppendLine("        Select D.AcNo4 As AcNo ");
            if (!IsForValidation)
            {
                if (IsCancel)
                {
                    SQL.AppendLine("        ,0.00 As CAmt, ");
                    SQL.AppendLine("        Sum(IfNull(B.UPrice, 0)*B.Qty) As DAmt ");
                }
                else
                {
                    SQL.AppendLine("        ,0.00 As DAmt, ");
                    SQL.AppendLine("        Sum(IfNull(B.UPrice, 0)*B.Qty) As CAmt ");
                }
            }
            SQL.AppendLine("        From TblServiceDeliveryHdr A ");
            SQL.AppendLine("        Inner Join TblServiceDeliveryDtl B On A.DocNo=B.DocNo And B.CancelInd = '" + CancelInd + "' " + Filter);
            SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode ");
            if (!IsForValidation)
                SQL.AppendLine("        And D.AcNo4 Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            if (!IsForValidation)
                SQL.AppendLine("        Group By D.AcNo4 ");

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Concat(C.ParValue, A.CtCode) As AcNo ");
            if (!IsForValidation)
            {
                if (IsCancel)
                {
                    SQL.AppendLine("        , Sum(IfNull(B.UPrice, 0)*B.Qty) As CAmt, ");
                    SQL.AppendLine("        0.00 As DAmt ");
                }
                else
                {
                    SQL.AppendLine("        , Sum(IfNull(B.UPrice, 0)*B.Qty) As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                }
            }
            SQL.AppendLine("        From TblServiceDeliveryHdr A ");
            SQL.AppendLine("        Inner Join TblServiceDeliveryDtl B On A.DocNo = B.DocNo And B.CancelInd = '" + CancelInd + "' " + Filter);
            SQL.AppendLine("        Inner Join TblParameter C On C.ParCode = 'CustomerAcNoNonInvoice' ");
            if (!IsForValidation)
                SQL.AppendLine("        And C.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");

            return SQL.ToString();
        }

        private string JournalCOAAccountList(bool IsCancel)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("UNION ALL ");
            if (!IsCancel)
                SQL.AppendLine("SELECT B.AcNo as AcNo, B.DAmt as DAmt, B.CAmt as CAmt ");
            else
                SQL.AppendLine("SELECT B.AcNo as AcNo, B.DAmt as CAmt, B.CAmt as DAmt ");
            SQL.AppendLine("FROM tblservicedeliveryhdr A ");
            SQL.AppendLine("INNER JOIN tblservicedeliverydtl2 B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo ");

            return SQL.ToString();
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, mItCodeCols).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, mItCodeCols) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ComputeTotal(int row)
        {
            decimal Qty = 0m, UPrice = 0m;

            if (Sm.GetGrdStr(Grd1, row, mQty1Cols).Length != 0) Qty = Sm.GetGrdDec(Grd1, row, mQty1Cols);
            if (Sm.GetGrdStr(Grd1, row, mServicePriceCols).Length != 0) UPrice = Sm.GetGrdDec(Grd1, row, mServicePriceCols);
            Grd1.Cells[row, mAmtDtlCols].Value = Sm.FormatNum(Qty * UPrice, 0);
            ComputeAmt();
        }

        private void ComputeAmt()
        {
            decimal Amt = 0m;
            for (int row = 0; row <= Grd1.Rows.Count - 1; row++)
                if (Sm.GetGrdStr(Grd1, row, mAmtDtlCols).Length != 0 && !Sm.GetGrdBool(Grd1, row, 1))
                    Amt += Sm.GetGrdDec(Grd1, row, mAmtDtlCols);
            
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void SetLueOptionCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.OptCode As Col1, A.OptDesc As Col2 From TblOption A Where A.OptCat='SDDefaultCOA' ");

                //SQL.AppendLine("And Exists( ");
                //SQL.AppendLine("    Select 1 From TblGroupCOA ");
                //SQL.AppendLine("    Where AcNo=A.Property1 ");
                //SQL.AppendLine("    And GrpCode In ( ");
                //SQL.AppendLine("        Select GrpCode From TblUser ");
                //SQL.AppendLine("        Where UserCode=@UserCode ");
                //SQL.AppendLine("    ) ");
                //SQL.AppendLine(") ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue3(Sl.SetLueCtCode), string.Empty, mIsFilterByCtCt ? "Y" : "N");
            }
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mIsFilterByCC ? "Y" : "N");
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
            }
        }


       

        #endregion

        #region Button Click

        private void BtnCustomerShipAddress_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    Sm.FormShowDialog(new FrmServiceDeliveryDlg3(this, Sm.GetLue(LueCtCode)));
                }
            }
        }

        private void BtnSAName_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnDOCtDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    Sm.FormShowDialog(new FrmServiceDeliveryDlg2(this, Sm.GetLue(LueCtCode)));
                }
            }
        }

        private void BtnDOCtDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDOCtDocNo, "DO#", false))
            {
                var f = new FrmDOCt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDOCtDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid Events

        #region Grd3

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {

        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmServiceDeliveryDlg4(this, Grd3, true, Sm.GetLue(LueCtCode), string.Empty));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #endregion
    }
}
