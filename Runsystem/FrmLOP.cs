﻿#region Update
/*
    22/05/2018 [WED] BUG di validasi kode fase, bukan numeric
    17/07/2018 [HAR] tambah inputan site, costcenter, scope work, resource
    26/07/2018 [HAR] bug saat show status detail
    14/08/2018 [WED] tambah approval LOP
    14/08/2018 [WED] kalau belum di approve, ngga bisa ubah Process
    31/08/2018 [TKG] menyembunyikan target 
    06/11/2018 [DITA] tambah StartDt dan EndDt di LOPHdr
    08/11/2018 [DITA] tambah StartDt dan EndDt di LOPDtl
    22/02/2019 [TKG] tambah validasi start date dan end date
    01/03/2019 [WED] Bug Cost Center yang sudah tidak aktif masih muncul di combo box
    11/03/2019 [WED] Site yang sudah tidak aktif masih muncul di combo box
    12/03/2019 [MEY] Estimasi value dibuat mandatory atau wajib diisi
    12/03/2019 [MEY] fasilitas copy data detail LOP dari LOP lain yang sudah jadi
    13/03/2019 [DITA] Settingan aproval berdasarkan nominal angka. Misal Aproval diatas 10 Milyar
    18/03/2019 [DITA] Penambahan field Confident Level
    01/07/2019 [DITA] LOP yg sudah dibuat BOQ. tidak bisa di "Process: Close"
    16/07/2019 [DITA] tambah ProcessInd = Lose (L), dan validasi tidak bisa Lose ProcessInd saat SO Contract masih aktif
    08/08/2019 [TKG] tambah filter berdasarkan site. 
    24/09/2019 [TKG/IMS] tambah project group
    24/09/2019 [DITA/IMS] Scope of Work, Cost Center, Estimated Value mandatory berdasarkan parameter
    21/10/2019 [DITA/IMS] Lampiran pada phase
    24/10/2019 [VIN/IMS]  Penambahan kolom QtLetterNo (Quotation Letter No)
    25/10/2019 [DITA/IMS] Site tidak mandatory
    25/10/2019 [DITA/IMS] Resource dibuat tidak mandatory
    30/10/2019 [WED/IMS] Site tidak mandatory berdasarkan parameter IsFilterBySite dan IsSiteMandatory
    06/11/2019 [DITA/IMS] fitur edit dan delete untuk phase berdasar parameter 
    11/12/2019 [VIN+DITA/IMS] fitur printout LOP 
    02/01/2019 [HAR/IMS] BUG : LOP tidak bisa remove baris
    18/01/2020 [TKG/IMS] berdasarkan parameter IsLOPQtLetterNoMandatory, quotation letter# harus diinput atau tidak.
    18/01/2020 [TKG/IMS] berdasarkan parameter IsLOPRouteMandatory, route harus diinput atau tidak.
    18/01/2020 [TKG/IMS] bisa upload pdf maupun excel
    06/02/2020 [VIN/YK] Printout LOP YK
    11/02/2020 [VIN/YK] tambah parameter phasecodetodisplay
    05/03/2020 [HAR/YK] printout YK feedback
    27/03/2020 [VIN/IMS] detail bisa hapus
    24/04/2020 [IBL/IMS] Ubah Project Group menjadi mandatory
 *  17/06/2020 [HAR/YK] BUG : Ubah Project Group menjadi mandatory validasi blm dihilangkan param IsProjectGroupEnabled
 *  24/06/2020 [HAR/IMS] Length quotation letter ditambah jadi 50
 *  02/06/2021 [MYA/IMS] Project yang internalnya terceklist maka tidak bisa ditarik di LOP.
 *  30/09/2021 [VIN/YK] Bug: aktifin kembali IsDocumentAlreadyProcessToBOQ
 *  29/11/2021 [TYO/ALL] membuat kolom Target menjadi deffault tidak hiden
 *  21/12/2021 [TYO/YK] Menambahkan field "Approval's Remark" berdasarkan param IsPSModuleShowApproverRemarkInfo
 *  22/12/2021 [MYA/VIR] bug menu List of project dimana saat disave bagian detail hilang
 *  08/03/2022 [TYO/GSS] set parameter IsLOPUseRouteAndPhase
    22/03/2022 [VIN/YK] BUG show hdr belum di group by
    14/07/2022 [RIS/PRODUCT] Menambahkan tab approval information
    16/12/2022 [BRI/MNET] route ambil dari system option berdasarkan param LoPRouteType
    23/12/2022 [IBL/MNET] BUG: ketika insert -> saat sudah pilih route dr dropdown, nama route hilang.
                          BUG: ketika show data route yg muncul adl code nya. Harusnya route name nya
    05/01/2023 [ICA/MNET] menambah field PIC Sales, source ambil dari user. berdasarkan parameter IsLOPShowPICSales
                          menambah field BOQ#. 
    19/01/2023 [DITA/MNET] Mengubah field Scope of Work pada menu List of Project menjadi free teks based on param = IsProjectScopeNotUseOption 
    21/02/2023 [MAU/MNET] penyesuaian button BOQDOcNo 
    02/03/2023 [WED/MNET] letak pemanggilan dari Frm lain di FrmLoad nya dipindah ke paling bawah
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Net;
using System.Threading;


#endregion

namespace RunSystem
{
    public partial class FrmLOP : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal string mDocNo = String.Empty;
        internal FrmLOPFind FrmFind;
        private bool 
            IsInsert = false,
            mIsLOPQtLetterNoMandatory = false,
            mIsLOPRouteMandatory = false,
            mIsPSModuleShowApproverRemarkInfo = false;

        internal bool mIsFilterBySite = false, 
            mIsFilterBySiteHR = false, 
            mIsFilterByDeptHR = false, 
            mIsProjectGroupEnabled = false,
            mIsEstimatedValueMandatory = false,
            mIsCostCenterMandatory = false,
            mIsProjectScopeMandatory= false,
            mIsPhaseAllowToUploadFile = false,
            mIsSiteMandatory=false,
            mIsResourceMandatory=false,
            mIsPhaseLOPEditable = false,
            mIsProjectGroupUseInternalInd = false,
            mIsLOPUseRouteAndPhase = false,
            mIsLOPShowPICSales = false,
            mIsProjectScopeNotUseOption = false;
        iGCell fCell;
        bool fAccept;
    
        internal string mPhaseCodetoDisplay = string.Empty;

        private string
          mPortForFTPClient = string.Empty,
          mHostAddrForFTPClient = string.Empty,
          mSharedFolderForFTPClient = string.Empty,
          mUsernameForFTPClient = string.Empty,
          mPasswordForFTPClient = string.Empty,
          mFileSizeMaxUploadFTPClient = string.Empty,
          mFormatFTPClient = string.Empty,
          mLoPRouteType = string.Empty,
          mPICSalesGroup = string.Empty;

        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmLOP(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "List of Project";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCityCode(ref LueCityCode);
                Sl.SetLueOption(ref LueTypeCode, "ProjectType");
                Sl.SetLueOption(ref LueScope, "ProjectScope");
                Sl.SetLueOption(ref LueResource, "ProjectResource");
                Sl.SetLueOption(ref LueConfidentLvl, "ConfidentLvl");
                if (mLoPRouteType == "2") Sl.SetLueOption(ref LueRouteCode, "ProjectRoute");
                LueRouteCode.Visible = false;
                if (TxtDocNo.Text.Length > 0)
                {
                    string mCCCode = Sm.GetValue("Select CCCode From TblLOPHdr Where DocNo = @Param; ", TxtDocNo.Text);
                    string mSiteCode = Sm.GetValue("Select SiteCode From TblLOPHdr Where DocNo = @Param; ", TxtDocNo.Text);
                    string mCtCode = Sm.GetValue("Select CtCode From TblLOPHdr Where DocNo = @Param; ", TxtDocNo.Text);
                    SetLueCCCode(ref LueCostCenter, mCCCode);
                    SetLueSiteCode(ref LueSiteCode, mSiteCode);
                    SetLueCtCode(ref LueCtCode, mCtCode);
                }
                else
                {
                    SetLueCCCode(ref LueCostCenter, string.Empty);
                    SetLueSiteCode(ref LueSiteCode, string.Empty);
                    SetLueCtCode(ref LueCtCode, string.Empty);
                }
                if (mIsProjectScopeNotUseOption)
                {
                    MeeScope.Visible = true;
                    LueScope.Visible = false;
                }
                SetLueStatus(ref LueProcessInd);
                LuePhaseCode.Visible = false;

                LblPGCode.ForeColor = mIsProjectGroupEnabled?Color.Red:Color.Black;
                if (mIsProjectScopeMandatory) lblProjectScope.ForeColor = Color.Red;
                if (mIsCostCenterMandatory) lblCostCenter.ForeColor = Color.Red;
                if (mIsEstimatedValueMandatory) lblEstimatedValue.ForeColor = Color.Red;
                
                if (mIsResourceMandatory) lblResource.ForeColor = Color.Red;
                if (!mIsFilterBySite && !mIsSiteMandatory) lblSite.ForeColor = Color.Black;
                if (mIsLOPQtLetterNoMandatory) LblQtLetterNo.ForeColor = Color.Red;

                if (!mIsPSModuleShowApproverRemarkInfo)
                {
                    int ypoint = 21;
                    LblApprovalRemark.Visible = MeeApprovalRemark.Visible = false;
                    label8.Top = label8.Top - ypoint;
                    LueProcessInd.Top = LueProcessInd.Top - ypoint;
                    label76.Top = label76.Top - ypoint;
                    TxtProjectName.Top = TxtProjectName.Top - ypoint;
                    LblPGCode.Top = LblPGCode.Top - ypoint;
                    LuePGCode.Top = LuePGCode.Top - ypoint;
                    label3.Top = label3.Top - ypoint;
                    LueCtCode.Top = LueCtCode.Top - ypoint;
                    lblSite.Top = lblSite.Top - ypoint;
                    LueSiteCode.Top = LueSiteCode.Top - ypoint;
                    lblCostCenter.Top = lblCostCenter.Top - ypoint;
                    LueCostCenter.Top = LueCostCenter.Top - ypoint;
                    Grd2.Height = Grd2.Height + ypoint;
                    label9.Top -= ypoint;
                    TxtBOQDocNo.Top -= ypoint;
                    BtnBOQDocNo.Top -= ypoint;
                }

                SetLuePICSales(ref LuePICSales, "");
                if(!mIsLOPShowPICSales)
                {
                    LblPICSales.Visible = false;
                    LuePICSales.Visible = false;
                }
                else
                {
                    label6.Visible = label7.Visible = TxtPICCode.Visible = TxtPICName.Visible = BtnPICCode.Visible = false;
                    lblEstimatedValue.Top -= 21;
                    TxtEstimated.Top -= 21;
                    label19.Top -= 21;
                    LueConfidentLvl.Top -= 21;
                    LblQtLetterNo.Top -= 21;
                    TxtQtLetterNo.Top -= 21;
                    label10.Top -= 21;
                    MeeRemark.Top -= 21;
                    BtnCopyData.Top -= 21;
                    LblPICSales.Top -= 162;
                    LuePICSales.Top -= 162;
                }

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 2

            Grd2.Cols.Count = 13;
            Grd2.FrozenArea.ColCount = 1;
            Grd2.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Dno",

                        //1-5
                        "Route",
                        "Target",
                        "Start Date",
                        "End Date",
                        "Phase Code",
                        
                        //6-10
                        "Phase",
                        "Status", 
                        "File Name", 
                        "", 
                        "",

                        //11-12
                        "File Name 2",
                        "Route Code"
                    },
                    new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        150, 130, 120,120, 100, 
                        //6-10
                        150,150, 200,20,20,

                        //11-12
                        200, 120
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 2 }, 0);
            Sm.GrdFormatDate(Grd2, new int[] { 3,4 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 5, 11, 12 });
            Sm.GrdColButton(Grd2, new int[] { 9, 10 });
            if (!mIsPhaseAllowToUploadFile) Sm.GrdColInvisible(Grd2, new int[] { 8, 9, 10 });

            #endregion

            #region Grid 3

            Grd3.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "No",

                        //1-4
                        "User",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd3, new int[] { 3 });
            Sm.GrdColReadOnly(Grd3, new int[] { 0, 1, 2, 3, 4 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd2, new int[] {  5 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, MeeCancelReason, DteDocDt, TxtProjectName, LuePGCode,
                        LueProcessInd, LueCtCode, LueTypeCode, LueCityCode, TxtPICCode,
                        TxtPICName, TxtEstimated, LueSiteCode, LueCostCenter, LueResource, 
                        LueScope, TxtQtLetterNo, MeeRemark, DteStartDt,DteEndDt, LueConfidentLvl, 
                        MeeApprovalRemark, LueRouteCode, TxtBOQDocNo, LuePICSales, MeeScope
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 12 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnCopyData.Enabled = false;
                    BtnPICCode.Enabled = false;
                    BtnBOQDocNo.Enabled = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtProjectName, LueCtCode, LueTypeCode, LuePGCode,
                        LueCityCode, TxtEstimated,  LueSiteCode, LueCostCenter, LueResource, 
                        LueScope, TxtQtLetterNo, MeeRemark, DteStartDt, DteEndDt, LueConfidentLvl,
                        LueRouteCode, LuePICSales, MeeScope
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 2, 3, 4, 6, 9 });
                    BtnPICCode.Enabled = true;
                    BtnCopyData.Enabled = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, LueConfidentLvl }, false);
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 3,4 });
                    if (mIsPhaseLOPEditable)
                    {
                        for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                        {
                            if (Sm.GetGrdStr(Grd2, Row, 7).ToString() == "Progress")
                            {
                                Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 2, 6, 9 });
                            }
                        }

                    }
                    
                    if(Sm.IsDataExist("Select DocNo From TblLOPHdr Where Status = 'A' And DocNo = @Param; ", TxtDocNo.Text))
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProcessInd }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            IsInsert = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, MeeCancelReason, DteDocDt, TxtProjectName, LuePGCode,
                 LueProcessInd, LueCtCode, LueTypeCode, LueCityCode, TxtPICCode,
                 TxtPICName, TxtEstimated, TxtQtLetterNo, MeeRemark, LueSiteCode, LueCostCenter, 
                 LueResource, LueScope, TxtStatus,DteStartDt,DteEndDt, LueConfidentLvl, MeeApprovalRemark,
                 LueRouteCode, LuePICSales, TxtBOQDocNo, MeeScope
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtEstimated }, 0);
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 2 });
            Sm.FocusGrd(Grd2, 0, 0);
           
        }

        #endregion

        #region Button Method

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
           
            ParPrint();
            
        }

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmLOPFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetDteCurrentDate(DteDocDt);
                ChkCancelInd.Checked = false;
                Sm.SetLue(LueProcessInd, "P");
                //Sl.SetLuePGCode(ref LuePGCode, "PGCode", "Y");
                SetLuePGCode(ref LuePGCode, "PGCode", "Y");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySite ? "Y" : "N");
                IsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnCopyData_Click(object sender, EventArgs e)
        {
            var f = new FrmLOPDlg2(this);
            f.Tag = mMenuCode;
            f.WindowState = FormWindowState.Normal;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.ShowDialog();
        }

        private void BtnBOQDocNo_Click(object sender, EventArgs e)
        {
            if (Sm.GetParameter("DocTitle") == "MNET")
            {
                var f = new FrmBOQ3(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtBOQDocNo.Text;
                f.ShowDialog();
            }

        }


        #endregion

        #region Grid Method


        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && mLoPRouteType == "2" && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                LueRequestEdit(Grd2, LueRouteCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
                Sl.SetLueOption(ref LueRouteCode, "ProjectRoute");
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                LueRequestEdit(Grd2, LuePhaseCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
                SetLuePhaseCode(ref LuePhaseCode);
            }

            if (Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                if (e.ColIndex == 3) Sm.DteRequestEdit(Grd2, DteStartDt1, ref fCell, ref fAccept, e);
            }
            if (Sm.IsGrdColSelected(new int[] { 4 }, e.ColIndex))
            {
                if (e.ColIndex == 4) Sm.DteRequestEdit(Grd2, DteEndDt1, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 2 });
            }

           
                if (e.ColIndex == 10)
                {
                    if (Sm.GetGrdStr(Grd2, e.RowIndex, 8).Length > 0)
                    {
                        DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd2, e.RowIndex, 8), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                        SFD.FileName = Sm.GetGrdStr(Grd2, e.RowIndex, 8);
                        SFD.DefaultExt = "pdf";
                        SFD.AddExtension = true;

                        if (!Sm.IsGrdValueEmpty(Grd2, e.RowIndex, 8, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                        {
                            if (SFD.ShowDialog() == DialogResult.OK)
                            {
                                Application.DoEvents();

                                //Write the bytes to a file
                                FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                                newFile.Write(downloadedData, 0, downloadedData.Length);
                                newFile.Close();
                                Sm.StdMsg(mMsgType.Info, "File Downloaded");
                            }
                        }
                        else
                            Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                    }
                }
            

        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (mIsPhaseLOPEditable)
            {
                if (BtnSave.Enabled && e.KeyCode == Keys.Delete)
                {
                    if (Grd2.SelectedRows.Count > 0)
                    {
                        if (Grd2.Rows[Grd2.Rows[Grd2.Rows.Count - 1].Index].Selected)
                            MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        else
                        {
                            if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                for (int Index = Grd2.SelectedRows.Count - 1; Index >= 0; Index--)
                                {
                                    if ((Sm.GetGrdStr(Grd2, Grd2.SelectedRows[Index].Index, 7).Length>0 && Sm.GetGrdStr(Grd2, Grd2.SelectedRows[Index].Index, 7) != "Progress") /*|| Sm.GetGrdStr(Grd2, Grd2.SelectedRows[Index].Index, 8).Length > 0*/)
                                    {
                                        Sm.StdMsg(mMsgType.Warning, "You can't remove this selected data.");
                                        return;
                                    }
                                    Grd2.Rows.RemoveAt(Grd2.SelectedRows[Index].Index);
                                }
                                if (Grd2.Rows.Count <= 0) Grd2.Rows.Add();
                            }
                        }
                    }
                }

            }
            else Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }


        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {

                if (e.ColIndex == 10)
                {
                    if (Sm.GetGrdStr(Grd2, e.RowIndex, 8).Length > 0)
                    {
                        DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd2, e.RowIndex, 8), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                        SFD.FileName = Sm.GetGrdStr(Grd2, e.RowIndex, 8);
                        SFD.DefaultExt = "pdf";
                        SFD.AddExtension = true;

                        if (!Sm.IsGrdValueEmpty(Grd2, e.RowIndex, 8, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                        {
                            if (SFD.ShowDialog() == DialogResult.OK)
                            {
                                Application.DoEvents();

                                //Write the bytes to a file
                                FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                                newFile.Write(downloadedData, 0, downloadedData.Length);
                                newFile.Close();
                                Sm.StdMsg(mMsgType.Info, "File Downloaded");
                            }
                        }
                        else
                            Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                    }
                }

                if (BtnSave.Enabled)
                {

                if (e.ColIndex == 9)
                {
                    try
                    {
                        OD.InitialDirectory = "c:";
                        OD.Filter = "Pdf files (*.pdf)|*.pdf|Office Files|*.xls;*.xlsx";
                        OD.FilterIndex = 2;
                        OD.ShowDialog();
                        Grd2.Cells[e.RowIndex, 8].Value = OD.FileName;
                    }
                    catch (Exception Exc)
                    {
                        Sm.ShowErrorMsg(Exc);
                    }
                }
            }
        }
        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "LOP", "TblLOPHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveLOPHdr(DocNo));

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0 && mIsLOPRouteMandatory)
                {
                    cml.Add(SaveLOPDtl(DocNo, Row));
                }
                else
                {
                    cml.Add(SaveLOPDtl(DocNo, Row));
                }
            }
       
            Sm.ExecCommands(cml);
            // bool IsFile = false;

            if (mIsPhaseAllowToUploadFile)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                    {
                        if (mIsPhaseAllowToUploadFile && Sm.GetGrdStr(Grd2, Row, 8).Length > 0 && Sm.GetGrdStr(Grd2, Row, 8) != "openFileDialog1")
                        {
                            UploadFile(DocNo, Row, Sm.GetGrdStr(Grd2, Row, 8));
                            // IsFile = true;
                        }
                    }
                }
            }
            // if (IsFile) Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            ShowData(DocNo);            
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueProcessInd, "Status") ||
                Sm.IsTxtEmpty(TxtProjectName, "Project Name", false) ||
                (mIsProjectGroupEnabled && Sm.IsLueEmpty(LuePGCode, "Project's group")) ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                (mIsSiteMandatory && mIsFilterBySite && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                (mIsCostCenterMandatory && Sm.IsLueEmpty(LueCostCenter, "Cost Center")) ||
                (mIsProjectScopeMandatory && (!mIsProjectScopeNotUseOption ? Sm.IsLueEmpty(LueScope, "Scope of Work") : Sm.IsMeeEmpty(MeeScope, "Scope of Work"))) ||
                (mIsResourceMandatory && Sm.IsLueEmpty(LueResource, "Resource")) ||
                Sm.IsLueEmpty(LueTypeCode, "Type") ||
                (!mIsLOPShowPICSales && Sm.IsTxtEmpty(TxtPICCode, "PIC", false)) ||
                (mIsLOPShowPICSales && Sm.IsLueEmpty(LuePICSales, "PIC Sales")) ||
                Sm.IsLueEmpty(LueConfidentLvl, "Confident Level") ||
                (mIsEstimatedValueMandatory && Sm.IsTxtEmpty(TxtEstimated, "Estimated Value", true)) ||
                (mIsLOPQtLetterNoMandatory && Sm.IsTxtEmpty(TxtQtLetterNo, "Quotation letter#", false)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                (mIsPhaseAllowToUploadFile && IsUploadFileNotValid());
        }

        private bool IsUploadFileNotValid()
        {
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                {
                    if (mIsPhaseAllowToUploadFile && Sm.GetGrdStr(Grd2, Row, 8).Length > 0 && Sm.GetGrdStr(Grd2, Row, 8) != "openFileDialog1")
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd2, Row, 8))) return true;
                    }
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string StartDt = string.Empty, EndDt = string.Empty;

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (
                    (mIsLOPRouteMandatory && Sm.IsGrdValueEmpty(Grd2, Row, 1, false, "Route is empty.")) ||
                    Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Target is empty.") ||
                    Sm.IsGrdValueEmpty(Grd2, Row, 6, false,
                        "Route# : " + Sm.GetGrdStr(Grd2, Row, 1) + Environment.NewLine +
                        "Phase is Empty.") ||
                     Sm.IsGrdValueEmpty(Grd2, Row, 3, false, "Start date is empty.") ||
                     Sm.IsGrdValueEmpty(Grd2, Row, 4, false, "End date is empty.")
                    ) return true;

                StartDt = Sm.GetGrdDate(Grd2, Row, 3).Substring(0, 8);
                EndDt = Sm.GetGrdDate(Grd2, Row, 4).Substring(0, 8);
                if (Sm.CompareDtTm(EndDt, StartDt) < 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Route : " + Sm.GetGrdStr(Grd2, Row, 1) + Environment.NewLine +
                        "Start Date : " + 
                            Sm.Right(StartDt.ToString(), 2) + "/" +
                            StartDt.ToString().Substring(4, 2) + "/" +
                            Sm.Left(StartDt.ToString(), 4) + Environment.NewLine +
                        "End Date : " + Sm.Right(EndDt.ToString(), 2) + "/" +
                            EndDt.ToString().Substring(4, 2) + "/" +
                            Sm.Left(EndDt.ToString(), 4) + Environment.NewLine + Environment.NewLine +
                        "End date should not be earlier than start date.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveLOPHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLOPHdr(DocNo, CancelInd, CancelReason, Status, DocDt, ProcessInd, ");
            SQL.AppendLine("ProjectName, PGCode, CtCode, ProjectType, CityCode, PicCode, ");
            SQL.AppendLine("EstValue, SiteCode, CCCode, ProjectScope, ProjectResource, StartDt, EndDt, ConfidentLvl, QtLetterNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, 'N', @CancelReason, 'O', @DocDt, @ProcessInd, ");
            SQL.AppendLine("@ProjectName, @PGCode, @CtCode, @ProjectType, @CityCode, @PicCode, ");
            SQL.AppendLine("@EstValue, @SiteCode, @CCCode, @ProjectScope, @ProjectResource, @StartDt,@EndDt, @ConfidentLvl, @QtLetterNo, @Remark, @CreateBy, CurrentDateTime()); ");

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='LOP' ");
                if (IsNeedApprovalBySite())
                    SQL.AppendLine("And T.SiteCode Is Not Null And T.SiteCode=@SiteCode ");
                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select A.EstValue*1 ");
                SQL.AppendLine("    From TblLOPHdr A ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("), 0)); ");
            }

            SQL.AppendLine("Update TblLOPHdr Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType = 'LOP' ");
            SQL.AppendLine("    And DocNo = @DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<String>(ref cm, "@ProjectName", TxtProjectName.Text);
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetLue(LuePGCode));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@ProjectType", Sm.GetLue(LueTypeCode));
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCityCode));
            Sm.CmParam<String>(ref cm, "@PICCode", (mIsLOPShowPICSales ? Sm.GetLue(LuePICSales) : TxtPICCode.Text));
            Sm.CmParam<Decimal>(ref cm, "@EstValue", Decimal.Parse(TxtEstimated.Text));
            Sm.CmParam<String>(ref cm, "@SiteCode",  Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCostCenter));
            Sm.CmParam<String>(ref cm, "@ProjectScope", !mIsProjectScopeNotUseOption ? Sm.GetLue(LueScope) : MeeScope.Text);
            Sm.CmParam<String>(ref cm, "@ProjectResource", Sm.GetLue(LueResource));
            Sm.CmParam<String>(ref cm, "@ConfidentLvl", Sm.GetLue(LueConfidentLvl));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            Sm.CmParam<String>(ref cm, "@QtLetterNo", TxtQtLetterNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveLOPDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblLOPDtl(DocNo, DNo, Route, Target, StartDt,EndDt, PhaseCode, Status, FileName, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @Route, @Target,@StartDt,@EndDt, @PhaseCode, 'P', @FileName, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            if (mLoPRouteType == "2")
                Sm.CmParam<String>(ref cm, "@Route", Sm.GetGrdStr(Grd2, Row, 12));
            else
                Sm.CmParam<String>(ref cm, "@Route", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Target", Sm.GetGrdDec(Grd2, Row, 2));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd2, Row, 3));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetGrdDate(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@PhaseCode", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@FileName", Sm.GetGrdStr(Grd2, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdatePhaseFile(string DocNo,int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblLOPDtl Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }



        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            if (mIsPhaseLOPEditable)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 8) != Sm.GetGrdStr(Grd2, Row, 11))
                    {
                        if (mIsPhaseAllowToUploadFile && Sm.GetGrdStr(Grd2, Row, 8).Length > 0 && Sm.GetGrdStr(Grd2, Row, 8) != "openFileDialog1")
                        {
                            UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd2, Row, 8));
                        }
                    }
                }
            }

            cml.Add(EditLOPHdr());

            if (mIsPhaseLOPEditable)
            {
                cml.Add(DeleteLOPDtl());

                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                    {
                        cml.Add(SaveLOPDtl(TxtDocNo.Text, Row));
                    }
                }
            }
            else
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(EditLOPDtl(Row));
            }
           
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "List Of Project#", false) ||
                Sm.IsLueEmpty(LueConfidentLvl, "Confident Level") ||
                (mIsLOPQtLetterNoMandatory && Sm.IsTxtEmpty(TxtQtLetterNo, "Quotation letter#", false)) ||
                IsGrdValueNotValid() ||
                IsDocumentAlreadyCancel() ||
                IsDocumentAlreadyClose() ||
                IsDocumentSOContractIsActive() ||
                IsDocumentAlreadyProcessToBOQ();
        }

        private bool IsDocumentAlreadyProcessToBOQ()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblBOQHdr ");
            SQL.AppendLine("Where LOPDocNo = @Param ");
            SQL.AppendLine("And Status = 'A' ");
            SQL.AppendLine("And ActInd = 'Y' ");
            SQL.AppendLine("Limit 1; ");

            if(Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already processed to BOQ.");
                return true;
            }

            return false;
        }

        private bool IsDocumentSOContractIsActive()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo From TblSOContractHdr A ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr C On B.LOPDocNo = C.DocNo ");
            SQL.AppendLine("Where B.LOPDocNo = @Param And A.CancelInd = 'N' and A.Status In ('A','O') limit 1 ");
  
            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                if (Sm.GetLue(LueProcessInd).ToString() == "S")
                {
                    Sm.StdMsg(mMsgType.Warning, "SO Contract still active. SOContract# " + Sm.GetValue(SQL.ToString(), TxtDocNo.Text));
                    return true;
                }
            }

            return false;
        }

        private bool IsDocumentAlreadyCancel()
        {           
            var cm = new MySqlCommand()
            {
                CommandText = "Select 1 From TblLOPHdr Where DocNo=@DocNo And (CancelInd='Y' Or Status = 'C') "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancel");
                return true;
            }
            return false;
        }

        private bool IsDocumentAlreadyClose()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select 1 From TblLOPHdr Where DocNo=@DocNo And CancelInd='N' And ProcessInd = 'L' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already Closed");
                return true;
            }
            return false;
        }

        private MySqlCommand EditLOPHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblLOPHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason = @CancelReason, ConfidentLvl = @ConfidentLvl, ");
            SQL.AppendLine("    ProcessInd=@ProcessInd, QtLetterNo=@QtLetterNo, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo; ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<String>(ref cm, "@ConfidentLvl", Sm.GetLue(LueConfidentLvl));
            Sm.CmParam<String>(ref cm, "@QtLetterNo", TxtQtLetterNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand DeleteLOPDtl()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Delete From TblLOPDtl ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            cm.CommandText = SQL.ToString();

            return cm;

        }

        private MySqlCommand EditLOPDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblLOPDtl Set ");
            SQL.AppendLine("    StartDt=@StartDt, EndDt = @EndDt,LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo and DNo=@Dno; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd2, Row, 0));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd2, Row, 3));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetGrdDate(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowLOPHdr(DocNo);
                ShowLOPDtl(DocNo);
                Sm.ShowDocApproval(DocNo, "LOP", ref Grd3);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowLOPDetail(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Sm.ClearGrd(Grd2, true);
                Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 2 });
                Sm.FocusGrd(Grd2, 0, 0);
                ShowLOPDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowLOPHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.Cancelind, A.ProjectName, A.CtCode, ");
            SQL.AppendLine("A.ProjectType, A.CityCode, A.PICCode, A.EstValue, A.ProcessInd, A.SiteCode, ");
            if (mIsLOPShowPICSales)
                SQL.AppendLine(" B.UserCode EmpName, ");
            else
                SQL.AppendLine(" B.EmpName, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.CCCode,  A.projectScope, A.ProjectResource, A.ConfidentLvl, A.QtLetterNo, A.Remark, A.StartDt, A.EndDt, A.PGCode, E.DocNo BOQDocNo ");
            if (mIsPSModuleShowApproverRemarkInfo)
                SQL.AppendLine(", D.Remark ApprovalRemark ");
            else
                SQL.AppendLine(", NULL as ApprovalRemark ");
            SQL.AppendLine("From TblLOPHdr A ");
            if (mIsLOPShowPICSales)
                SQL.AppendLine("Inner Join TblUser B On A.PICCode = B.UserCode ");
            else
                SQL.AppendLine("Inner Join TblEmployee B On A.PICCode = B.EmpCode ");
            if (mIsPSModuleShowApproverRemarkInfo)
            {
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("(   ");
                SQL.AppendLine("	Select Max(T2.`Level`) LV, T1.DocNo   ");
                SQL.AppendLine("	From TblDocApproval T1   ");
                SQL.AppendLine("	Inner Join TblDocApprovalSetting T2 On T1.DocType = T2.DocType And T1.ApprovalDNo = T2.DNo   ");
                SQL.AppendLine("	Where T1.DocType = 'LOP' And T1.DocNo = @DocNo ");
                SQL.AppendLine("	And (T1.UserCode Is Not Null And T1.UserCode <> '') ");
                SQL.AppendLine("	And T1.Status Is Not Null ");
                SQL.AppendLine("	Group By T1.DocNo ");

                SQL.AppendLine(") C ON A.DocNo = C.DocNo ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("(   ");
                SQL.AppendLine("   Select T2.`Level` As LV, T1.DocNo, T1.Remark, T3.EmpName, T1.UserCode ");
                SQL.AppendLine("   From TblDocApproval T1   ");
                SQL.AppendLine("   Inner Join TblDocApprovalSetting T2 On T1.DocType = T2.DocType And T1.ApprovalDNo = T2.DNo ");
                SQL.AppendLine("	Inner Join TblEmployee T3 On T1.UserCode = T3.UserCode  ");
                SQL.AppendLine("   Where T1.DocType = 'LOP' And T1.DocNo = @DocNo ");
                SQL.AppendLine("	And (T1.UserCode Is Not Null And T1.UserCode <> '')   ");
                SQL.AppendLine("	And T1.Status Is Not Null ");
                SQL.AppendLine(") D On C.DocNo = D.DocNo And C.LV = D.LV ");
            }

            SQL.AppendLine("Left Join TblBOQHdr E On A.DocNo = E.LOPDocNo And E.ActInd = 'Y' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "ProjectName", "CtCode", 
                        
                        //6-10
                        "Projecttype", "CityCode", "PiCCode", "EmpName", "EstValue",
 
                        //11-15
                        "ProcessInd", "SiteCode", "CCCode", "projectScope", "ProjectResource", 
                        //16-20
                        "QtLetterNo", "Remark", "StatusDesc", "StartDt","EndDt", 
                        //21-23
                        "ConfidentLvl", "PGCode", "ApprovalRemark", "BOQDocNo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[4]);
                        SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueTypeCode, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueCityCode, Sm.DrStr(dr, c[7]));
                        TxtPICCode.EditValue = Sm.DrStr(dr, c[8]);
                        TxtPICName.EditValue = Sm.DrStr(dr, c[9]);
                        if (mIsLOPShowPICSales) Sm.SetLue(LuePICSales, Sm.DrStr(dr, c[9]));
                        TxtEstimated.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                        Sm.SetLue(LueProcessInd,  Sm.DrStr(dr, c[11]));
                        Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[12]), "N");            
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[12]));
                        SetLueCCCode(ref LueCostCenter, Sm.DrStr(dr, c[13]));
                        Sm.SetLue(LueCostCenter, Sm.DrStr(dr, c[13]));
                        if (!mIsProjectScopeNotUseOption) Sm.SetLue(LueScope, Sm.DrStr(dr, c[14]));
                        else MeeScope.EditValue = Sm.DrStr(dr, c[14]);
                        Sm.SetLue(LueResource, Sm.DrStr(dr, c[15]));
                        TxtQtLetterNo.EditValue = Sm.DrStr(dr, c[16]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[17]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[18]);
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[19]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[20]));
                        Sm.SetLue(LueConfidentLvl, Sm.DrStr(dr, c[21]));
                        Sl.SetLuePGCode(ref LuePGCode, Sm.DrStr(dr, c[22]), "N");
                        if (mIsPSModuleShowApproverRemarkInfo)
                            MeeApprovalRemark.EditValue = Sm.DrStr(dr, c[23]);
                        TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[24]);
                    }, true
                );
        }

        private void ShowLOPDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, ");
            if (mLoPRouteType == "1")
                SQL.AppendLine("A.Route, ");
            else
                SQL.AppendLine("C.OptDesc As Route, ");
            SQL.AppendLine("A.Target, A.StartDt, A.EndDt, A.PhaseCode, B.Phasename, Case When A.Status = 'P' Then 'Progress' When A.status = 'F' Then 'Finished' End As Status, A.FileName ");
            SQL.AppendLine("From TblLOPDtl A ");
            SQL.AppendLine("Inner Join TblPhase B On A.phaseCode = B.PhaseCode");
            if(mLoPRouteType == "2")
                SQL.AppendLine("Left Join TblOption C On A.Route = C.OptCode And C.OptCat = 'ProjectRoute' ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "Dno", 

                    //1-5
                    "Route", "Target", "StartDt","EndDt", "PhaseCode", 
                    
                    //6-7
                    "PhaseName", "Status", "FileName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowLOPDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.Route, A.Target, A.StartDt, A.EndDt, A.PhaseCode, B.Phasename, Case When A.Status = 'P' Then 'Progress' When A.status = 'F' Then 'Finished' End As Status  ");
            SQL.AppendLine("From TblLOPDtl A ");
            SQL.AppendLine("Inner Join TblPhase B On A.phaseCode = B.PhaseCode");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "Dno", 

                    //1-5
                    "Route", "Target", "StartDt","EndDt", "PhaseCode", 
                    
                    //6-7
                    "PhaseName", "Status"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd2, 0, 1);
        }
        #endregion

        #region Additional Method

        private void ParPrint()
        {
            if (Sm.StdMsgYN("Print", "") == DialogResult.No || Sm.IsTxtEmpty(TxtDocNo, "Document#", false)) return;

            string[] TableName = { "LOP", "LOPDtl", "LOPYK", "LOPYKSign", "LOPYKSign2", "LOPYKSign3" };

            var l = new List<LOP>();
            var l2 = new List<LOPDtl>();
            var ldtl = new List<LOPYK>();
            var lsign = new List<LOPYKSign>();
            var lsign2 = new List<LOPYKSign2>();
            var lsign3 = new List<LOPYKSign3>();

            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

           SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
           SQL.AppendLine("A.DocNo, Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' End As Status, Date_Format(A.DocDt,'%d %M %Y')As DocDt, A.CancelReason, ");
		   SQL.AppendLine("IfNull (B.ProjectName, A.ProjectName) ProjectName, B.ProjectCode, C.CtName, D.OptDesc ProjectType, ");
		   SQL.AppendLine("E.OptDesc ConfidentLvl, F.EmpName ");
           SQL.AppendLine("From TblLOPHdr A    ");
           SQL.AppendLine("Left Join TblProjectGroup B ON B.PGCode = A.PGCode ");
           SQL.AppendLine("Inner Join TblCustomer C ON A.CtCode = C.CtCode ");
           SQL.AppendLine("Inner Join TblOption D ON A.ProjectType = D.OptCode AND D.OptCat = 'ProjectType' ");
           SQL.AppendLine("Left Join TblOption E ON A.ConfidentLvl = E.OptCode AND E.OptCat = 'ConfidentLvl' ");
           SQL.AppendLine("Inner Join TblEmployee F On A.PICCode = F.EmpCode ");
           SQL.AppendLine("Where A.DocNo=@DocNo  ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "DocNo",
                         "DocDt",
                         "ProjectCode",
                         "ProjectName",
                         "CtName",

                         //6-10
                         "ProjectType",
                         "Status",
                         "ConfidentLvl",
                         "CancelReason",
                         "EmpName"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new LOP()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            DocNo = Sm.DrStr(dr, c[1]),
                            DocDt = Sm.DrStr(dr, c[2]),
                            ProjectCode = Sm.DrStr(dr, c[3]),
                            ProjectName = Sm.DrStr(dr, c[4]),
                            CtName = Sm.DrStr(dr, c[5]),
                            ProjectType = Sm.DrStr(dr, c[6]),
                            Status = Sm.DrStr(dr, c[7]),
                            ConfidentLvl = Sm.DrStr(dr, c[8]),
                            CancelReason = Sm.DrStr(dr, c[9]),
                            PICName = Sm.DrStr(dr, c[10]),
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);
            #endregion

            #region LOP Detail 

            int nomor = 0;
            if (Grd2.Rows.Count > 0)
            {
                for (int Row1 = 0; Row1 < Grd2.Rows.Count; Row1++)
                {
                    if (Sm.GetGrdStr(Grd2, Row1, 1).Length > 0)
                    {
                        nomor = nomor + 1;
                        l2.Add(new LOPDtl()
                        {
                            No = nomor,
                            StartDt = Sm.GetGrdText(Grd2, Row1, 3),
                            EndDt = Sm.GetGrdText(Grd2, Row1, 4),
                            Phase = Sm.GetGrdStr(Grd2, Row1, 6),
                            Status = Sm.GetGrdStr(Grd2, Row1, 7),
                            FileName = Sm.GetGrdStr(Grd2, Row1, 8),
                           
                        });
                    }
                }
            }

            myLists.Add(l2);
            #endregion

            #region Detail YK

            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("SELECT IFNULL (A.ProjectName, B.ProjectName) ProjectName, C.OptDesc AS ProjectType, ");
                SQLDtl.AppendLine("D.CtName, E.OptDesc AS ProjectResource, A.EstValue, DATE_FORMAT(A.StartDt, '%d %M %Y') StartDt, DATE_FORMAT(A.EndDt, '%d %M %Y') EndDt, F.OptDesc AS ConfidentLvl, ");
                SQLDtl.AppendLine("Case A.Status When 'A' Then 'Go' ELSE 'No Go' END AS STATUS  ");
                SQLDtl.AppendLine("FROM TblLOPHdr A ");
                SQLDtl.AppendLine("LEFT JOIN TblProjectGroup B ON A.PGCode = B.PGCode ");
                SQLDtl.AppendLine("INNER JOIN TblOption C ON A.ProjectType = C.OptCode AND C.OptCat = 'ProjectType' ");
                SQLDtl.AppendLine("INNER JOIN TblCustomer D ON A.CtCode = D.CtCode ");
                SQLDtl.AppendLine("Left JOIN TblOption E ON A.ProjectResource = E.OptCode AND E.OptCat = 'ProjectResource' ");
                SQLDtl.AppendLine("LEFT JOIN TblOption F ON A.ConfidentLvl = F.OptCode AND F.OptCat = 'ConfidentLvl' ");
                SQLDtl.AppendLine("WHERE A.DocNo = @DocNo ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                            {

                             //0
                             "ProjectName" ,

                             //1-5
                             "ProjectType" ,
                             "CtName",
                             "ProjectResource",
                             "EstValue",
                             "StartDt",

                             //6-8
                             "EndDt",
                             "ConfidentLvl",
                             "Status"
                             
            
                            });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new LOPYK()
                        {
                            ProjectName = Sm.DrStr(drDtl, cDtl[0]),
                            ProjectType = Sm.DrStr(drDtl, cDtl[1]),
                            CtName = Sm.DrStr(drDtl, cDtl[2]),
                            ProjectResource = Sm.DrStr(drDtl, cDtl[3]),
                            EstValue = Sm.DrDec(drDtl, cDtl[4]),
                            StartDt = Sm.DrStr(drDtl, cDtl[5]),
                            EndDt = Sm.DrStr(drDtl, cDtl[6]),
                            ConfidentLvl = Sm.DrStr(drDtl, cDtl[7]),
                            Status = Sm.DrStr(drDtl, cDtl[8])

                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region LOPYK Sign

            //sign kolom paling bawah
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtl2.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtl2.AppendLine("From ( ");
                SQLDtl2.AppendLine("Select Distinct ");
                SQLDtl2.AppendLine("B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, B.ApprovalDNo As DNo, D.Level, Left(B.LastUpDt, 8) As LastUpDt");
                SQLDtl2.AppendLine("From TblLOPHdr A ");
                SQLDtl2.AppendLine("Inner Join TblDocApproval B On B.DocType='LOP' And A.DocNo=B.DocNo   ");
                SQLDtl2.AppendLine("Inner Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl2.AppendLine("Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'LOP' ");
                SQLDtl2.AppendLine("INNER JOIN (SELECT DocType, SiteCode, MAX(LEVEL) MaxLvl FROM TblDocApprovalSetting WHERE Doctype = 'LOP' GROUP BY DocType, SiteCode) E ");
                SQLDtl2.AppendLine("ON D.Doctype = E.DocType AND D.SiteCode AND E.SiteCode AND D.Level = E.MaxLvl ");
                SQLDtl2.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtl2.AppendLine(") T1 ");
                SQLDtl2.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl2.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl2.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtl2.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                SQLDtl2.AppendLine("Order By T1.DNo DESC; ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                            {

                             //0
                             "Signature" ,

                             //1-5
                             "UserName" ,
                             "LastUpDt"
                             
            
                            });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        lsign.Add(new LOPYKSign()
                        {
                            Signature = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            LastUpDt = Sm.DrStr(drDtl2, cDtl2[2])

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(lsign);

            //sign 2 yk disetujui oleh 
            var cmDtl3 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;

                SQLDtl3.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtl3.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Min(T1.Level) Level, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt, T1.Remark ");
                SQLDtl3.AppendLine("From ( ");
                SQLDtl3.AppendLine("Select Distinct ");
                SQLDtl3.AppendLine("B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, B.ApprovalDNo As DNo, D.Level, Left(B.LastUpDt, 8) As LastUpDt, ");
                SQLDtl3.AppendLine("IfNull(B.Remark, A.Remark) Remark");
                SQLDtl3.AppendLine("From TblLOPHdr A ");
                SQLDtl3.AppendLine("Inner Join TblDocApproval B On B.DocType='LOP' And A.DocNo=B.DocNo   ");
                SQLDtl3.AppendLine("Inner Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl3.AppendLine("Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'LOP' ");
                SQLDtl3.AppendLine("INNER JOIN (SELECT DocType, SiteCode, MIN(LEVEL) MinLvl FROM TblDocApprovalSetting WHERE DocType = 'LOP' GROUP BY DocType, SiteCode) E ");
                SQLDtl3.AppendLine("ON D.DocType = E.DocType AND D.SiteCode = E.SiteCode AND D.Level = E.MinLvl ");
                SQLDtl3.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtl3.AppendLine(") T1 ");
                SQLDtl3.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl3.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl3.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                SQLDtl3.AppendLine("Order By T1.DNo DESC; ");

                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);
                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                            {

                             //0
                             "Signature" ,

                             //1-5
                             "UserName" ,
                             "LastUpDt",
                             "Remark"
                             
            
                            });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        lsign2.Add(new LOPYKSign2()
                        {
                            Signature = Sm.DrStr(drDtl3, cDtl3[0]),
                            UserName = Sm.DrStr(drDtl3, cDtl3[1]),
                            LastUpDt = Sm.DrStr(drDtl3, cDtl3[2]),
                            Remark = Sm.DrStr(drDtl3, cDtl3[3]),

                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(lsign2);

            //sign 3 yk dibuat oleh 
            var cmDtl4 = new MySqlCommand();

            var SQLDtl4 = new StringBuilder();
            using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl4.Open();
                cmDtl4.Connection = cnDtl4;

                SQLDtl4.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtl4.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtl4.AppendLine("From ( ");
                SQLDtl4.AppendLine("Select Distinct ");
                SQLDtl4.AppendLine("A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, '1' As DNo, 0 As Level, 'Dibuat oleh,' As Title, Left(A.CreateDt, 8) As LastUpDt   ");
                SQLDtl4.AppendLine("From TblLOPHdr A ");
                SQLDtl4.AppendLine("Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtl4.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtl4.AppendLine(") T1 ");
                SQLDtl4.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl4.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl4.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtl4.AppendLine("Group By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Title ");
                SQLDtl4.AppendLine("Order By T1.Level;  ");

                cmDtl4.CommandText = SQLDtl4.ToString();
                Sm.CmParam<String>(ref cmDtl4, "@DocNo", TxtDocNo.Text);
                var drDtl4 = cmDtl4.ExecuteReader();
                var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                            {

                             //0
                             "Signature" ,

                             //1-5
                             "UserName" ,
                             "LastUpDt"
                             
            
                            });
                if (drDtl4.HasRows)
                {
                    while (drDtl4.Read())
                    {
                        lsign3.Add(new LOPYKSign3()
                        {
                            Signature = Sm.DrStr(drDtl4, cDtl4[0]),
                            UserName = Sm.DrStr(drDtl4, cDtl4[1]),
                            LastUpDt = Sm.DrStr(drDtl4, cDtl4[2])

                        });
                    }
                }
                drDtl4.Close();
            }
            myLists.Add(lsign3);

            #endregion

            if (Sm.GetParameter("DocTitle") == "YK")
                Sm.PrintReport("LOPYK", myLists, TableName, false);
            if (Sm.GetParameter("DocTitle") == "IMS")
                Sm.PrintReport("LOP", myLists, TableName, false);
            
        }

        private void SetLueCCCode(ref DXE.LookUpEdit Lue, string CCCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode As Col1, CCName As Col2 ");
            SQL.AppendLine("From TblCostCenter ");
            if (CCCode.Length <= 0)
                SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By CCName; ");

            Sm.SetLue2(
               ref Lue,
               SQL.ToString(),
               0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueCtCode(ref DXE.LookUpEdit Lue, string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CtCode As Col1, CtName As Col2 ");
            SQL.AppendLine("From TblCustomer ");
            if (CtCode.Length <= 0)
                SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By CtName; ");

            Sm.SetLue2(
               ref Lue,
               SQL.ToString(),
               0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue, string SiteCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SiteCode As Col1, SiteName As Col2 ");
            SQL.AppendLine("From TblSite ");
            if (SiteCode.Length <= 0)
                SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By SiteName; ");

            Sm.SetLue2(
               ref Lue,
               SQL.ToString(),
               0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLuePICSales(ref DXE.LookUpEdit Lue, string PICSales)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select UserCode As Col1, UserName As Col2 ");
            SQL.AppendLine("From TblUser ");
            SQL.AppendLine("Where Find_In_Set(GrpCode, '"+mPICSalesGroup+"') ");
            SQL.AppendLine("Order By UserName; ");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private bool IsNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where Doctype = 'LOP' Limit 1; ");
        }

        private bool IsNeedApprovalBySite()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where Doctype = 'LOP' And SiteCode Is Not Null Limit 1; ");
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsProjectGroupEnabled = Sm.GetParameterBoo("IsProjectGroupEnabled");
            mIsEstimatedValueMandatory = Sm.GetParameterBoo("IsEstimatedValueMandatory");
            mIsCostCenterMandatory = Sm.GetParameterBoo("IsCostCenterMandatory");
            mIsProjectScopeMandatory = Sm.GetParameterBoo("IsProjectScopeMandatory");
            mIsPhaseAllowToUploadFile = Sm.GetParameterBoo("IsPhaseAllowToUploadFile");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsResourceMandatory = Sm.GetParameterBoo("IsResourceMandatory");
            mIsPhaseLOPEditable = Sm.GetParameterBoo("IsPhaseLOPEditable");
            mIsLOPQtLetterNoMandatory = Sm.GetParameterBoo("IsLOPQtLetterNoMandatory");
            mIsLOPRouteMandatory = Sm.GetParameterBoo("IsLOPRouteMandatory");
            mPhaseCodetoDisplay = Sm.GetParameter("PhaseCodetoDisplay");
            mIsProjectGroupUseInternalInd = Sm.GetParameterBoo("IsProjectGroupUseInternalInd");
            mIsPSModuleShowApproverRemarkInfo = Sm.GetParameterBoo("IsPSModuleShowApproverRemarkInfo");
            mIsLOPUseRouteAndPhase = Sm.GetParameterBoo("IsLOPUseRouteAndPhase");
            mLoPRouteType = Sm.GetParameter("LoPRouteType");
            mPICSalesGroup = Sm.GetParameter("PICSalesGroup");
            mIsLOPShowPICSales = Sm.GetParameterBoo("IsLOPShowPICSales");
            mIsProjectScopeNotUseOption = Sm.GetParameterBoo("IsProjectScopeNotUseOption");
            if (mLoPRouteType.Length == 0) mLoPRouteType = "1";
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select 'P' As Col1, 'On Progress' As Col2 Union All " +
                "Select 'L' As Col1, 'Close' As Col2 Union All " +
                "Select 'C' As Col1, 'Cancelled' As Col2 Union All " +
                "Select 'S' As Col1, 'Lose' As Col2; ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        public void SetLuePhaseCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select PhaseCode As Col1, PhaseName As Col2  ");
            SQL.AppendLine("From TblPhase ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By Col2 ");

            cm.CommandText = SQL.ToString();
            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

               
                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                   
                }
            }
            while (bytesRead != 0);

           

            file.Close();
            ftpStream.Close();

            if (!IsInsert) Grd2.Cells[Row, 8].Value = toUpload.Name;
            if (IsInsert)
            {
                var cml = new List<MySqlCommand>();
                cml.Add(UpdatePhaseFile(DocNo, Row, toUpload.Name));
                Sm.ExecCommands(cml);
            }
        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsPhaseAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsPhaseAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsPhaseAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsPhaseAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsPhaseAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row "+(Row+1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsPhaseAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPOHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        public void SetLuePGCode(ref LookUpEdit Lue, string PGCode, string IsActive)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select PGCode As Col1, PGName As Col2 ");
            SQL.AppendLine("From TblProjectGroup ");
            SQL.AppendLine("Where 0=0");
            if (PGCode.Length > 0)
                SQL.AppendLine("And PGCode=@PGCode ");
            else
            {
                if (IsActive == "Y") SQL.AppendLine("And ActInd='Y' ");
            }
            if (mIsProjectGroupUseInternalInd)
            {
                SQL.AppendLine("And InternalInd = 'N'");      
            }
            SQL.AppendLine("Order By PGName;");
            cm.CommandText = SQL.ToString();
            if (PGCode.Length > 0) Sm.CmParam<String>(ref cm, "@PGCode", PGCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (PGCode.Length > 0) Sm.SetLue(Lue, PGCode);
        }

        #endregion        

        #endregion

        #region Event
        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void BtnPICCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmLOPDlg(this));
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(SetLueCtCode), string.Empty);
        }

        private void LueTypeCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTypeCode, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectType");
        }

        private void LueCityCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue1(Sl.SetLueCityCode));
        }

        private void LueRouteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mLoPRouteType == "2")
                Sm.RefreshLookUpEdit(LueRouteCode, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectRoute");
        }

        private void LueRouteCode_Leave(object sender, EventArgs e)
        {
            if (LueRouteCode.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueRouteCode).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 12].Value =
                    Grd2.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 12].Value = Sm.GetLue(LueRouteCode);
                    Grd2.Cells[fCell.RowIndex, 1].Value = LueRouteCode.GetColumnValue("Col2");
                }
                LueRouteCode.Visible = false;
            }
        }

        private void LuePhaseCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePhaseCode, new Sm.RefreshLue1(SetLuePhaseCode));
        }

        private void LuePhaseCode_Leave(object sender, EventArgs e)
        {
            if (LuePhaseCode.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (Sm.GetLue(LuePhaseCode).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 5].Value =
                    Grd2.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LuePhaseCode);
                    Grd2.Cells[fCell.RowIndex, 6].Value = LuePhaseCode.GetColumnValue("Col2");
                }
                LuePhaseCode.Visible = false;
            }
        }

        private void LuePhaseCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void TxtEstimated_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtEstimated, 0);
        }

        private void DteStartDt1_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteStartDt1, ref fCell, ref fAccept);
        }

        private void DteStartDt1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd2, ref fAccept, e);
        }

        private void LueCostCenter_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCostCenter, new Sm.RefreshLue2(SetLueCCCode), string.Empty);
        }

        private void LueScope_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueScope, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectScope");
        }

        private void LueResource_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueResource, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectResource");
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue3(SetLuePGCode), string.Empty, "Y");
        }

        private void LueConfidentLvl_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueConfidentLvl, new Sm.RefreshLue2(Sl.SetLueOption), "ConfidentLvl");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite ? "Y" : "N");
        }

        private void LueProcessInd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProcessInd, new Sm.RefreshLue1(SetLueStatus));
        }

        private void DteEndDt1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd2, ref fAccept, e);
        }

        private void DteEndDt1_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteEndDt1, ref fCell, ref fAccept);
        }
        private void TxtQtLetterNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtQtLetterNo);
        }

        #endregion

        #region Class

        private class LOP
        {
            public string CompanyLogo { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string CtName { get; set; }
            public string ProjectType {get; set;}
            public string Status { get; set; }
            public string ConfidentLvl { get; set; }
            public string CancelReason { get; set; }
            public string PICName { get; set; }

        }


        private class LOPDtl
        {
            public int No { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string Phase { get; set; }
            public string Status { get; set; }
            public string FileName { get; set; }

        }
        
        
        private class LOPYK
        {
            public string ProjectName { get; set; }
            public string ProjectType { get; set; }
            public string CtName { get; set; }
            public string ProjectResource { get; set; }
            public decimal EstValue { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string ConfidentLvl { get; set; }
            public string Status { get; set; }
            public string Remark { get; set; }

        }

        private class LOPYKSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string LastUpDt { get; set; }

        }
        
        private class LOPYKSign2
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string LastUpDt { get; set; }
            public string Remark { get; set; }

        }

        private class LOPYKSign3
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string LastUpDt { get; set; }

        }


        #endregion




    }
}
