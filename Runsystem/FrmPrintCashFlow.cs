﻿#region update
// 27/10/2017 [ARI] Header sesuai draft MAI
// 17/07/2017 [ARI] tambah printout selain MAI
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPrintCashFlow : RunSystem.FrmBase11
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mSQL = string.Empty, mPOPrintOutCompanyLogo = "1";

        #endregion

        #region Constructor

        public FrmPrintCashFlow(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region methode

        protected override void FrmLoad(object sender, EventArgs e)
        {
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
        }

        protected override void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        private void ParPrint()
        {
            string mDoctitle = Sm.GetParameter("DocTitle");
            mPOPrintOutCompanyLogo = Sm.GetParameter("POPrintOutCompanyLogo");

            if (Sm.IsDteEmpty(DteDocDt1, "Periode") || Sm.IsDteEmpty(DteDocDt2, "Periode") || Decimal.Parse(Sm.GetDte(DteDocDt2)) < Decimal.Parse(Sm.GetDte(DteDocDt1))) return;

            var l = new List<CashFlowHdr>();
            var ldtl = new List<CashFlowDtl>();
            var ldtl2 = new List<CashFlowDtl2>();
            var lLogo = new List<Logo>();

            string[] TableName = { "CashFlowHdr", "CashFlowDtl", "CashFlowDtl2", "Logo" };
            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();
            
            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddressCity', ");
            SQL.AppendLine("DATE_FORMAT(@DocDt1, '%M %d, %Y')As DocDt1, DATE_FORMAT(@DocDt2, '%M %d, %Y') As DocDt2 ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2).Substring(0, 8));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyAddressCity",
                         "DocDt1",
                         //6
                         "DocDt2"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new CashFlowHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyAddressCity = Sm.DrStr(dr, c[4]),
                            Period1 = Sm.DrStr(dr, c[5]),
                            Period2 =  Sm.DrStr(dr, c[6]),
                            //PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail

            int countCoaIn = Convert.ToInt32(Sm.GetValue("Select Count(*) As Numb From TblOption Where Optcat = 'COACashIn'"));

            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.OptCode, B.AcDesc, 'IN' As AcType, ifnull(C.Amt, 0) As AmtCashIn ");
                SQLDtl.AppendLine("From TblOption A  ");
                SQLDtl.AppendLine("Inner Join TblCoa B On A.OptCode = B.Acno  ");
                SQLDtl.AppendLine("Left Join (  ");
                SQLDtl.AppendLine("    Select X.parent, SUM(DAmt) As Amt  ");
                SQLDtl.AppendLine("    from ( ");
                SQLDtl.AppendLine("        Select Left(B.Acno, 3) As parent, A.DocNo, B.AcNo, C.Actype, C.AcDesc, B.DAmt, B.CAmt  ");
                SQLDtl.AppendLine("        From TblJournalhdr A ");
                SQLDtl.AppendLine("        Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
                SQLDtl.AppendLine("        Inner Join TblCOA C On B.AcNo = C.Acno  ");
                SQLDtl.AppendLine("        Where A.DocDt between @DocDt1 And @DocDt2 And   ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText =
                        "Select OptCode From TblOption Where Optcat = 'COACashIn' ";
                    var drX = cm.ExecuteReader();
                    var cX = Sm.GetOrdinal(drX, new string[] { "OptCode" });
                    if (drX.HasRows)
                    {
                        int nomor = 0;
                        while (drX.Read())
                        {
                            nomor = nomor + 1;
                            if (nomor == countCoaIn)
                            {
                                SQLDtl.AppendLine("(B.AcNo like '" + string.Concat(Sm.DrStr(drX, cX[0]), "%") + "' ) ");
                            }
                            else
                            {
                                SQLDtl.AppendLine("(B.AcNo like '" + string.Concat(Sm.DrStr(drX, cX[0]), "%") + "' ) OR ");
                            }
                        }
                    }
                    drX.Close();
                }

                SQLDtl.AppendLine("    )X  ");
                SQLDtl.AppendLine("    Where X.DAmt>0 group By X.parent ");
                SQLDtl.AppendLine(")C On A.OptCode = C.Parent ");
                SQLDtl.AppendLine("Where A.OptCat = 'COACashIn'  ");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParamDt(ref cmDtl, "@DocDt1", Sm.GetDte(DteDocDt1).Substring(0, 8));
                Sm.CmParamDt(ref cmDtl, "@DocDt2", Sm.GetDte(DteDocDt2).Substring(0, 8));

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "OptCode" ,

                     //1-5
                     "AcDesc" ,
                     "AcType",
                     "AmtCashIn",
                    });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new CashFlowDtl()
                        {
                            OptCode = Sm.DrStr(drDtl, cDtl[0]),
                            AcDesc = Sm.DrStr(drDtl, cDtl[1]),
                            AcType = Sm.DrStr(drDtl, cDtl[2]),
                            AmtCashIn = Sm.DrDec(drDtl, cDtl[3]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail 2
            int countCoaOut = Convert.ToInt32(Sm.GetValue("Select Count(*) As Numb From TblOption Where Optcat = 'COACashOut'"));

            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select A.OptCode, B.AcDesc, 'OUT' As AcType, ifnull(C.Amt, 0) As AmtCashOut ");
                SQLDtl2.AppendLine("From TblOption A  ");
                SQLDtl2.AppendLine("Inner Join TblCoa B On A.OptCode = B.Acno  ");
                SQLDtl2.AppendLine("Left Join (  ");
                SQLDtl2.AppendLine("    Select X.parent, SUM(CAmt) As Amt  ");
                SQLDtl2.AppendLine("    from ( ");
                SQLDtl2.AppendLine("        Select Left(B.Acno, 3) As parent, A.DocNo, B.AcNo, C.Actype, C.AcDesc, B.DAmt, B.CAmt  ");
                SQLDtl2.AppendLine("        From TblJournalhdr A ");
                SQLDtl2.AppendLine("        Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
                SQLDtl2.AppendLine("        Inner Join TblCOA C On B.AcNo = C.Acno  ");
                SQLDtl2.AppendLine("        Where A.DocDt between @DocDt1 And @DocDt2 And  ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText =
                        "Select OptCode From TblOption Where Optcat = 'COACashOut' ";
                    var drX = cm.ExecuteReader();
                    var cX = Sm.GetOrdinal(drX, new string[] { "OptCode" });
                    if (drX.HasRows)
                    {
                        int nomor = 0;
                        while (drX.Read())
                        {
                            nomor = nomor + 1;
                            if (nomor == countCoaOut)
                            {
                                SQLDtl2.AppendLine("(B.AcNo like '" + string.Concat(Sm.DrStr(drX, cX[0]), "%") + "' ) ");
                            }
                            else
                            {
                                SQLDtl2.AppendLine("(B.AcNo like '" + string.Concat(Sm.DrStr(drX, cX[0]), "%") + "' ) OR ");
                            }
                        }
                    }
                    drX.Close();
                }
                
                                
                SQLDtl2.AppendLine("    )X  ");
                SQLDtl2.AppendLine("    Where X.CAmt>0 group By X.parent ");
                SQLDtl2.AppendLine(")C On A.OptCode = C.Parent ");
                SQLDtl2.AppendLine("Where A.OptCat = 'COACashOut'  ");

                cmDtl2.CommandText = SQLDtl2.ToString();

                Sm.CmParamDt(ref cmDtl2, "@DocDt1", Sm.GetDte(DteDocDt1).Substring(0, 8));
                Sm.CmParamDt(ref cmDtl2, "@DocDt2", Sm.GetDte(DteDocDt2).Substring(0, 8));

                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "OptCode" ,

                     //1-5
                     "AcDesc" ,
                     "AcType",
                     "AmtCashOut",
                    });
                if (drDtl2.HasRows)
                {
                    int nomor = 0;
                    while (drDtl2.Read())
                    {
                        nomor = nomor + 1;
                        ldtl2.Add(new CashFlowDtl2()
                        {
                            OptCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            AcDesc = Sm.DrStr(drDtl2, cDtl2[1]),
                            AcType = Sm.DrStr(drDtl2, cDtl2[2]),
                            AmtCashOut = Sm.DrDec(drDtl2, cDtl2[3]),
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Logo2

            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName' ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo(mPOPrintOutCompanyLogo));

                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[]
                {
                //0-1
                "CompanyLogo",
                "CompanyName",
               
                });

                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        lLogo.Add(new Logo()
                        {
                            CompanyLogo = Sm.DrStr(dr2, c2[0]),
                            CompanyName = Sm.DrStr(dr2, c2[1]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(lLogo);

            #endregion

           if (mDoctitle == "MAI")
           {
               Sm.PrintReport("CashFlow", myLists, TableName, false);
           }
           else
           {
               Sm.PrintReport("CashFlow2", myLists, TableName, false);
           }
        }

        #endregion

    }

    #region Class

    class CashFlowHdr
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyAddressCity { get; set; }
        public string Period1 { get; set; }
        public string Period2 { get; set; }
    }

    class CashFlowDtl
    {
        public string OptCode { get; set; }
        public string AcDesc { get; set; }
        public string AcType { get; set; }
        public decimal AmtCashIn { get; set; }
    }

    class CashFlowDtl2
    {
        public string OptCode { get; set; }
        public string AcDesc { get; set; }
        public string AcType { get; set; }
        public decimal AmtCashOut { get; set; }
    }
    class Logo
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string PrintBy { get; set; }
    }

    #endregion
}
