﻿#region Update
/*
    15/06/2017 [TKG] data dari TblCOA belum difilter berdasarkan status aktif atau tidak.
    06/07/2017 [ARI] tambah filter Entity, berdasarkan parameter IsEntityMandatory
    19/04/2018 [TKG] bug proses journal ketika pilih start from
    21/05/2018 [TKG] Ditambah kolom untuk Date di Reporting Profit Loss
    22/05/2018 [TKG] bug fixing total biaya dan pendapatan
    26/05/2018 [HAR] ganti nama header balance jdi year to date untuk KIM brdsrkan param doctitle
    28/05/2018 [TKG] bug fixing karena status aktif atau tidak nomor coa
    19/07/2018 [TKG] logic perhitungan nomor rekening parent diubah untuk mengakomodasi apabila anak nomor rekening anak yg panjangnya tidak sama.
    15/10/2019 [WED/IMS] filter COA Alias
    28/04/2020 [WED/WOM] Journal ambil dari Period (bukan DocDt) berdasarkan parameter IsAccountingRptUseJournalPeriod
    22/09/2021 [TKG/ALL] Berdasarkan parameter IsRptAccountingShowJournalMemorial, menampilkan outstanding journal memorial
    22/09/2021 [TKG/ALL] Berdasarkan parameter IsRptFinancialNotUseStartFromFilter, menonaktifkan start from.
    15/02/2022 [TKG/PHT] merubah GetParameter()
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProfitLoss : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mAcNoForCurrentEarning = "3.3",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mAccountingRptStartFrom = string.Empty,
            mDocTitle = string.Empty;
            
        private bool 
            mIsEntityMandatory = false,
            mIsRptProfitLossUseFilterPeriod = false,
            mIsJournalCostCenterEnabled = false,
            mIsAccountingRptUseJournalPeriod = false,
            mIsRptAccountingShowJournalMemorial = false,
            mIsRptFinancialNotUseStartFromFilter = false;

        #endregion

        #region Constructor

        public FrmRptProfitLoss(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));

                if (mIsRptFinancialNotUseStartFromFilter)
                    Sm.SetControlReadOnly(LueStartFrom, true);
                else
                {
                    if (mAccountingRptStartFrom.Length > 0)
                    {
                        Sl.SetLueYr(LueStartFrom, mAccountingRptStartFrom);
                        Sm.SetLue(LueStartFrom, mAccountingRptStartFrom);
                    }
                    else
                    {
                        Sl.SetLueYr(LueStartFrom, string.Empty);
                        Sm.SetLue(LueStartFrom, CurrentDateTime.Substring(0, 4));
                    }
                }

                SetLueEntCode(ref LueEntCode);
                if (mIsEntityMandatory) LblEntCode.ForeColor = Color.Red;

                PnlCCCode.Visible = mIsJournalCostCenterEnabled;
                Sl.SetLueCCCode(ref LueCCCode);

                PnlPeriod.Visible = mIsRptProfitLossUseFilterPeriod;
                if (mIsRptProfitLossUseFilterPeriod)
                {
                    Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                    SetGrd2();
                }
                else
                    SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {            
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptAccountingShowJournalMemorial', 'IsRptFinancialNotUseStartFromFilter', 'IsAccountingRptUseJournalPeriod', 'IsEntityMandatory', 'IsRptProfitLossUseFilterPeriod', ");
            SQL.AppendLine("'IsJournalCostCenterEnabled', 'AcNoForIncome', 'AcNoForCost', 'AccountingRptStartFrom', 'DocTitle', ");
            SQL.AppendLine("'AcNoForCurrentEarning' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //string
                            case "AcNoForIncome": mAcNoForIncome = ParValue; break;
                            case "AcNoForCost": mAcNoForCost = ParValue; break;
                            case "AccountingRptStartFrom": mAccountingRptStartFrom = ParValue; break;
                            case "DocTitle": mDocTitle = ParValue; break;
                            case "AcNoForCurrentEarning": mAcNoForCurrentEarning = ParValue; break;

                            //boolean
                            case "IsRptProfitLossUseFilterPeriod": mIsRptProfitLossUseFilterPeriod = ParValue == "Y"; break;
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                            case "IsAccountingRptUseJournalPeriod": mIsAccountingRptUseJournalPeriod = ParValue == "Y"; break;
                            case "IsJournalCostCenterEnabled": mIsJournalCostCenterEnabled = ParValue == "Y"; break;
                            case "IsRptAccountingShowJournalMemorial": mIsRptAccountingShowJournalMemorial = ParValue == "Y"; break;
                            case "IsRptFinancialNotUseStartFromFilter": mIsRptFinancialNotUseStartFromFilter = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 3;

            Grd1.Cols[0].Width = 250;
            Grd1.Header.Cells[0, 0].Value = "Account#";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 250;
            Grd1.Header.Cells[0, 1].Value = "Alias";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 250;
            Grd1.Header.Cells[0, 2].Value = "Account Name";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Cols[3].Width = 130;
            Grd1.Header.Cells[0, 3].Value = "Debit";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Cols[4].Width = 130;
            Grd1.Header.Cells[0, 4].Value = "Credit";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Grd1.Cols[5].Width = 130;
            Grd1.Header.Cells[0, 5].Value = mDocTitle=="KIM"? "Year to Date":"Balance";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 2;

            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4 }, false);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        private void SetGrd2()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 3;

            Grd1.Cols[0].Width = 250;
            Grd1.Header.Cells[0, 0].Value = "Account#";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 250;
            Grd1.Header.Cells[0, 1].Value = "Alias";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 250;
            Grd1.Header.Cells[0, 2].Value = "Account Name";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Cols[3].Width = 130;
            Grd1.Header.Cells[0, 3].Value = "Debit";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Cols[4].Width = 130;
            Grd1.Header.Cells[0, 4].Value = "Credit";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Grd1.Cols[5].Width = 130;
            Grd1.Header.Cells[0, 5].Value = mDocTitle == "KIM" ? "Year to Date":"Balance";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 2;

            Grd1.Cols[6].Width = 130;
            Grd1.Header.Cells[0, 6].Value = "Debit" + Environment.NewLine + "(Period)";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 2;

            Grd1.Cols[7].Width = 130;
            Grd1.Header.Cells[0, 7].Value = "Credit" + Environment.NewLine + "(Period)";
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 7].SpanRows = 2;

            Grd1.Cols[8].Width = 130;
            Grd1.Header.Cells[0, 8].Value = mDocTitle == "KIM" ? "Year to Date" + Environment.NewLine + "(Period)" : "Balance" + Environment.NewLine + "(Period)";
            Grd1.Header.Cells[0, 8].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 8].SpanRows = 2;

            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6, 7 }, false);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        override protected void HideInfoInGrd()
        {
            if (mIsRptProfitLossUseFilterPeriod)
                Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6, 7 }, !ChkHideInfoInGrd.Checked);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsLueEmpty(LueYr, "Year") || 
                Sm.IsLueEmpty(LueMth, "Month") || 
                (mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity"))
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            if (ChkDocDt.Checked)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 6, 7 }, !ChkHideInfoInGrd.Checked);
                Sm.GrdColInvisible(Grd1, new int[] { 7 }, true);
            }
            else
            {
                if (mIsRptProfitLossUseFilterPeriod)
                    Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8 }, false);
            }

            var Yr = Sm.GetLue(LueYr);
            var Mth = Sm.GetLue(LueMth);
            var StartFrom = Sm.GetLue(LueStartFrom);
            try
            {
                var lCOA = new List<COA>();
                var lEntityCOA = new List<EntityCOA>();

                Process1(ref lCOA);
                if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                    Process6(ref lEntityCOA);
                if (lCOA.Count > 0)
                {
                    if (StartFrom.Length > 0)
                    {
                        Process2(ref lCOA, StartFrom);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process3_JournalMemorial(ref lCOA, Yr, Mth, StartFrom);
                        else
                            Process3(ref lCOA, Yr, Mth, StartFrom);
                    }
                    else
                    {
                        Process2(ref lCOA, Yr);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process3_JournalMemorial(ref lCOA, Yr, Mth, string.Empty);
                        else
                            Process3(ref lCOA, Yr, Mth, string.Empty);
                    }
                    if (ChkDocDt.Checked)
                    {
                        if (mIsRptAccountingShowJournalMemorial)
                            Process7_JournalMemorial(ref lCOA);
                        else
                            Process7(ref lCOA);
                    }
                    Process4(ref lCOA);
                    Process5(ref lCOA);
                    ComputeProfitLoss(ref lCOA);

                    #region Filter by Entity

                    if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                    {
                    if (lEntityCOA.Count > 0)
                     {
                         if (lCOA.Count > 0)
                         {
                             Grd1.BeginUpdate();
                             Grd1.Rows.Count = 0;

                             iGRow r;

                             r = Grd1.Rows.Add();
                             r.Level = 0;
                             r.TreeButton = iGTreeButtonState.Visible;
                             r.Cells[0].Value = "COA";
                             for (var j = 0; j < lEntityCOA.Count; j++)
                             {
                                 for (var i = 0; i < lCOA.Count; i++)
                                 {
                                     if (lCOA[i].AcNo == lEntityCOA[j].AcNo)
                                     {
                                         r = Grd1.Rows.Add();
                                         r.Level = lCOA[i].Level;
                                         r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                         r.Cells[0].Value = lCOA[i].AcNo;
                                         r.Cells[1].Value = lCOA[i].Alias;
                                         r.Cells[2].Value = lCOA[i].AcDesc;
                                         if (ChkDocDt.Checked)
                                         {
                                             for (var c = 3; c < 9; c++)
                                                 r.Cells[c].Value = 0m;
                                             r.Cells[3].Value = lCOA[i].YearToDateDAmt;
                                             r.Cells[4].Value = lCOA[i].YearToDateCAmt;
                                             r.Cells[5].Value = lCOA[i].Balance;
                                             r.Cells[6].Value = lCOA[i].PeriodDAmt;
                                             r.Cells[7].Value = lCOA[i].PeriodCAmt;
                                             r.Cells[8].Value = lCOA[i].PeriodBalance;
                                         }
                                         else
                                         {
                                             for (var c = 3; c < 6; c++)
                                                 r.Cells[c].Value = 0m;
                                             r.Cells[3].Value = lCOA[i].YearToDateDAmt;
                                             r.Cells[4].Value = lCOA[i].YearToDateCAmt;
                                             r.Cells[5].Value = lCOA[i].Balance;
                                         }
                                     }
                                 }
                             }

                             Grd1.TreeLines.Visible = true;
                             Grd1.Rows.CollapseAll();

                             Grd1.EndUpdate();
                         }
                      }
                    }
                    #endregion

                    #region Not Filter By Entity
                    else
                    {
                        if (lCOA.Count > 0)
                        {
                            Grd1.BeginUpdate();
                            Grd1.Rows.Count = 0;

                            iGRow r;

                            r = Grd1.Rows.Add();
                            r.Level = 0;
                            r.TreeButton = iGTreeButtonState.Visible;
                            r.Cells[0].Value = "COA";
                            for (var i = 0; i < lCOA.Count; i++)
                            {
                                r = Grd1.Rows.Add();
                                r.Level = lCOA[i].Level;
                                r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                r.Cells[0].Value = lCOA[i].AcNo;
                                r.Cells[1].Value = lCOA[i].Alias;
                                r.Cells[2].Value = lCOA[i].AcDesc;

                                if (ChkDocDt.Checked)
                                {
                                    for (var c = 3; c < 9; c++)
                                        r.Cells[c].Value = 0m;
                                    r.Cells[3].Value = lCOA[i].YearToDateDAmt;
                                    r.Cells[4].Value = lCOA[i].YearToDateCAmt;
                                    r.Cells[5].Value = lCOA[i].Balance;
                                    r.Cells[6].Value = lCOA[i].PeriodDAmt;
                                    r.Cells[7].Value = lCOA[i].PeriodCAmt;
                                    r.Cells[8].Value = lCOA[i].PeriodBalance;
                                }
                                else
                                {
                                    for (var c = 3; c < 6; c++)
                                        r.Cells[c].Value = 0m;
                                    r.Cells[3].Value = lCOA[i].YearToDateDAmt;
                                    r.Cells[4].Value = lCOA[i].YearToDateCAmt;
                                    r.Cells[5].Value = lCOA[i].Balance;
                                }
                            }
                            Grd1.TreeLines.Visible = true;
                            Grd1.Rows.CollapseAll();

                            Grd1.EndUpdate();
                        }
                    }

                    #endregion

                    lCOA.Clear();
                    lEntityCOA.Clear();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        //show account number, account description, parent, type
        private void Process1(ref List<COA> lCOA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT AcNo, Alias, AcDesc, Parent, AcType FROM TblCOA ");
            SQL.AppendLine("WHERE (AcNo LIKE '4%' Or AcNo LIKE '5%') ");
            SQL.AppendLine("And ActInd='Y' ");
            SQL.AppendLine("ORDER BY AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Alias", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Alias = Sm.DrStr(dr, c[1]),
                            AcDesc = Sm.DrStr(dr, c[2]),
                            Parent = Sm.DrStr(dr, c[3]),
                            Level = Sm.DrStr(dr, c[3]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[4]),
                            HasChild = false,
                            //ParentRow = 0,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            PeriodDAmt = 0m,
                            PeriodCAmt = 0m,
                            PeriodBalance = 0m,
                        });
                    }
                }
                dr.Close();
            }
        }

        //get opening balance
        private void Process2(ref List<COA> lCOA, string Yr)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.Yr=@Yr And A.CancelInd='N' ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("    And A.EntCode = @EntCode ");
            SQL.AppendLine("    And (Left(B.AcNo, 1)='4' Or Left(B.AcNo, 1)='5') ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            SQL.AppendLine("ORDER BY B.AcNo; ");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        //sum debit and credit amount
        private void Process3(ref List<COA> lCOA, string Yr, string Mth, string StartFrom)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT B.AcNo, SUM(B.DAmt) AS DAmt, SUM(B.CAmt) AS CAmt ");
            SQL.AppendLine("FROM TblJournalHdr A  ");
            SQL.AppendLine("INNER JOIN TblJournalDtl B ON A.DocNo=B.DocNo And Left(B.AcNo, 1) In ('4', '5')  ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("    And B.EntCode = @EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (ChkCCCode.Checked) SQL.AppendLine("    And A.CCCode Is Not Null And A.CCCode=@CCCode ");
            if (StartFrom.Length == 0)
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.Period, 6) <= @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) <= @YrMth ");
                }
            }
            else
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period IS Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.Period, 6) <= @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) <= @YrMth ");
                }
            }
            SQL.AppendLine("GROUP BY B.AcNo; ");
            
            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            if (ChkCCCode.Checked) Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3_JournalMemorial(ref List<COA> lCOA, string Yr, string Mth, string StartFrom)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt From ( ");

            #region Standard

            SQL.AppendLine("SELECT B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("FROM TblJournalHdr A  ");
            SQL.AppendLine("INNER JOIN TblJournalDtl B ON A.DocNo=B.DocNo And Left(B.AcNo, 1) In ('4', '5')  ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("    And B.EntCode = @EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (ChkCCCode.Checked) SQL.AppendLine("    And A.CCCode Is Not Null And A.CCCode=@CCCode ");
            if (StartFrom.Length == 0)
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.Period, 6) <= @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) <= @YrMth ");
                }
            }
            else
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period IS Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.Period, 6) <= @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) <= @YrMth ");
                }
            }

            #endregion

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("SELECT B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("FROM TblJournalMemorialHdr A  ");
            SQL.AppendLine("INNER JOIN TblJournalMemorialDtl B ON A.DocNo=B.DocNo And Left(B.AcNo, 1) In ('4', '5')  ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("    And B.EntCode = @EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status='O' ");
            if (ChkCCCode.Checked) SQL.AppendLine("    And A.CCCode Is Not Null And A.CCCode=@CCCode ");
            if (StartFrom.Length == 0)
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.Period, 6) <= @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) <= @YrMth ");
                }
            }
            else
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period IS Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.Period, 6) <= @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) <= @YrMth ");
                }
            }

            #endregion

            SQL.AppendLine(") Tbl Group By AcNo; ");

            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            if (ChkCCCode.Checked) Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        //sum debit and credit amount untuk filter period
        private void Process7(ref List<COA> lCOA)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) AS DAmt, Sum(B.CAmt) AS CAmt ");
            SQL.AppendLine("From TblJournalHdr A  ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo=B.DocNo And Left(B.AcNo, 1) In ('4', '5') ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("    And B.EntCode = @EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) Between Left(@StartDt, 6) And Left(@EndDt, 6) ");
            }
            else
                SQL.AppendLine("    And A.DocDt Between @StartDt And @EndDt ");
            if (ChkCCCode.Checked) SQL.AppendLine("    And A.CCCode Is Not Null And A.CCCode=@CCCode ");
            SQL.AppendLine("Group By B.AcNo;");
            
            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteDocDt2));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            if (ChkCCCode.Checked) Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1), 
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].PeriodDAmt += lJournal[i].DAmt;
                            lCOA[j].PeriodCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process7_JournalMemorial(ref List<COA> lCOA)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt From (");

            #region Standard

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A  ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo=B.DocNo And Left(B.AcNo, 1) In ('4', '5') ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("    And B.EntCode = @EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) Between Left(@StartDt, 6) And Left(@EndDt, 6) ");
            }
            else
                SQL.AppendLine("    And A.DocDt Between @StartDt And @EndDt ");
            if (ChkCCCode.Checked) SQL.AppendLine("    And A.CCCode Is Not Null And A.CCCode=@CCCode ");

            #endregion

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalMemorialHdr A  ");
            SQL.AppendLine("Inner Join TblJournalMemorialDtl B On A.DocNo=B.DocNo And Left(B.AcNo, 1) In ('4', '5') ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("    And B.EntCode = @EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status='O' ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) Between Left(@StartDt, 6) And Left(@EndDt, 6) ");
            }
            else
                SQL.AppendLine("    And A.DocDt Between @StartDt And @EndDt ");
            if (ChkCCCode.Checked) SQL.AppendLine("    And A.CCCode Is Not Null And A.CCCode=@CCCode ");

            #endregion

            SQL.AppendLine(") Tbl Group By AcNo; ");

            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteDocDt2));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            if (ChkCCCode.Checked) Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].PeriodDAmt += lJournal[i].DAmt;
                            lCOA[j].PeriodCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        //sum to column year to date
        private void Process4(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                lCOA[i].YearToDateDAmt =
                    lCOA[i].OpeningBalanceDAmt +
                    lCOA[i].MonthToDateDAmt;

                lCOA[i].YearToDateCAmt =
                    lCOA[i].OpeningBalanceCAmt +
                    lCOA[i].MonthToDateCAmt;

                if (Sm.Left(lCOA[i].AcNo, 1) == "4")
                    lCOA[i].Balance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;

                if (Sm.Left(lCOA[i].AcNo, 1) == "5")
                    lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                if (ChkDocDt.Checked)
                {
                    if (Sm.Left(lCOA[i].AcNo, 1) == "4")
                        lCOA[i].PeriodBalance = lCOA[i].PeriodCAmt - lCOA[i].PeriodDAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "5")
                        lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;
                }
            }
        }

        //checking whether this account has parent or nah
        private void Process5(ref List<COA> lCOA)
        {
            var Parent = string.Empty;
            var ParentLevel = 0;
            //var ParentRow = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].Level == -1)
                {
                    if (string.Compare(lCOA[i].Parent, Parent) == 0)
                    {
                        lCOA[i].Level = ParentLevel + 1;
                        //lCOA[i].ParentRow = ParentRow;
                    }
                    else
                    {
                        for (var j = 0; j < lCOA.Count; j++)
                        {
                            if (string.Compare(lCOA[i].Parent, lCOA[j].AcNo) == 0)
                            {
                                Parent = lCOA[i].Parent;
                                ParentLevel = lCOA[j].Level;
                                //ParentRow = j;
                                lCOA[i].Level = lCOA[j].Level + 1;
                                //lCOA[i].ParentRow = ParentRow;
                                break;
                            }
                        }
                    }
                }
                for (var j = i + 1; j < lCOA.Count; j++)
                {
                    if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
                        {
                            lCOA[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
        }

        private void Process6(ref List<EntityCOA> lEntityCOA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.AcNo, U.EntCode, V.EntName ");
            SQL.AppendLine("From TblCOA T ");
            SQL.AppendLine("Inner Join TblCOADtl U On T.AcNo = U.AcNo ");
            SQL.AppendLine("Inner Join TblEntity V On U.EntCode = V.EntCode ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("And (Left(T.AcNo, 1) In ('4','5')) ");
            SQL.AppendLine("And U.EntCode = @EntCode ");
            SQL.AppendLine("Order By T.AcNo; ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "EntCode", "EntName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEntityCOA.Add(new EntityCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            EntCode = Sm.DrStr(dr, c[1]),
                            EntName = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ComputeProfitLoss(ref List<COA> lCOA)
        {
            decimal balance4 = 0m;
            decimal balance5 = 0m;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].AcNo == "4")
                    balance4 = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;

                if (lCOA[i].AcNo == "5")
                    balance5 = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
            }

            decimal ProfitLoss = balance4 - balance5;
            TxtProfitLoss.EditValue = Sm.FormatNum(ProfitLoss, 0);
        }

        private void SetLueEntCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'Consolidate' As Col1, 'Consolidate' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select EntCode As Col1, EntName As Col2 From TblEntity Where ActInd='Y';");

            Sm.SetLue2(ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Period");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost center");
        }

        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string Alias { get; set; }
            public string AcDesc { get; set; }
            public string Parent { get; set; }
            public string AcType { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal Balance { get; set; }
            public decimal PeriodDAmt { get; set; }
            public decimal PeriodCAmt { get; set; }
            public decimal PeriodBalance { get; set; }            
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class EntityCOA
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        #endregion
    }
}
