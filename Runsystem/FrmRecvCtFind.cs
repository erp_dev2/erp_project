﻿#region Update
/*
    27/11/2019 [WED/IMS] munculkan kolom ProjectCode, ProjectName, Customer PO#, berdasarkan parameter IsBOMShowSpecifications
    30/01/2020 [TKG/IMS] munculkan kolom ProjectCode, ProjectName, Customer PO#
    13/07/2020 [IBL/KSM] menyambungkan dengan DO to customer based on DRSC
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRecvCtFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmRecvCt mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRecvCtFind(FrmRecvCt FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CancelInd, C.WhsName, D.CtName, ");
            SQL.AppendLine("B.DOCtDocNo, A.ReplaceInd, ");
            SQL.AppendLine("IfNull(E.DocDt, H.DocDt) As DOCtDocDt, ");
            SQL.AppendLine("IfNull(F.ItCode, I.ItCode) As ItCode, ");
            SQL.AppendLine("IfNull(G.ItName, J.ItName) As ItName, ");
            SQL.AppendLine("IfNull(F.BatchNo, I.BatchNo) As BatchNo, ");
            SQL.AppendLine("IfNull(F.Source, I.Source) As Source, ");
            SQL.AppendLine("IfNull(F.Lot, I.Lot) As Lot, ");
            SQL.AppendLine("IfNull(F.Bin, I.Bin) As Bin, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("IfNull(G.InventoryUomCode, J.InventoryUomCode) As InventoryUomCode, ");
            SQL.AppendLine("IfNull(G.InventoryUomCode2, J.InventoryUomCode2) As InventoryUomCode2, ");
            SQL.AppendLine("IfNull(G.InventoryUomCode3, J.InventoryUomCode3) As InventoryUomCode3, ");
            SQL.AppendLine("IfNull(E.CurCode, Null) As CurCode, ");
            SQL.AppendLine("IfNull(F.UPrice, 0) As UPrice, ");
            SQL.AppendLine("(B.Qty*IfNull(F.UPrice, 0)) As Amt, B.DOType,  ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, B.LastUpBy, B.LastUpDt, ");
            SQL.AppendLine("K.ProjectCode, K.ProjectName, K.PONo, K.SiteCode ");
            SQL.AppendLine("From TblRecvCtHdr A ");
            SQL.AppendLine("Inner Join TblRecvCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select WhsCode From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=C.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblCustomer D On A.CtCode=D.CtCode ");
            SQL.AppendLine("Left Join TblDOCtHdr E On B.DOCtDocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblDOCtDtl F On B.DOCtDocNo=F.DocNo And B.DOCtDNo=F.DNo ");
            SQL.AppendLine("Left Join TblItem G On F.ItCode=G.ItCode ");
            SQL.AppendLine("Left Join TblDOCt2Hdr H On B.DOCtDocNo=H.DocNo ");
            SQL.AppendLine("Left Join TblDOCt2Dtl I On B.DOCtDocNo=I.DocNo And B.DOCtDNo=I.DNo ");
            SQL.AppendLine("Left Join TblItem J On I.ItCode=J.ItCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select X1.DocNo, Group_Concat(Distinct X5.ProjectCode) ProjectCode, ");
            SQL.AppendLine("    Group_Concat(Distinct X5.ProjectName) ProjectName, ");
            SQL.AppendLine("    Group_Concat(Distinct X2.PONo) PONo, Null As SiteCode ");
            SQL.AppendLine("    From TblDRDtl X1 ");
            SQL.AppendLine("    Inner Join TblSOContractHdr X2 On X1.SODocNo = X2.DocNo ");
            SQL.AppendLine("    Inner Join TblBOQHdr X3 On X2.BOQDocNo = X3.DocNo ");
            SQL.AppendLine("    Inner Join TblLOPHdr X4 On X3.LOPDocNo = X4.DocNo ");
            SQL.AppendLine("    Left Join TblProjectGroup X5 On X4.PGCode = X5.PGCode ");
            SQL.AppendLine("    Where X1.DocNo In ( ");
            SQL.AppendLine("        Select T3.DRDocNo ");
            SQL.AppendLine("        From TblRecvCtHdr T1 ");
            SQL.AppendLine("        Inner Join TblRecvCtDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo And T3.DRDocNo Is Not Null ");
            SQL.AppendLine("        Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By X1.DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select X1.DocNo, Null As ProjectCode, ");
            SQL.AppendLine("    Null As ProjectName, Null As PONo, X3.SiteCode ");
            SQL.AppendLine("    From TblDRDtl X1 ");
            SQL.AppendLine("    Inner Join TblSalesContract X2 On X1.SCDocNo = X2.DocNo ");
            SQL.AppendLine("    Inner Join TblSalesMemoHdr X3 On X2.SalesMemoDocNo = X3.DocNo ");
            SQL.AppendLine("    Where X1.DocNo In ( ");
            SQL.AppendLine("        Select T3.DRDocNo ");
            SQL.AppendLine("        From TblRecvCtHdr T1 ");
            SQL.AppendLine("        Inner Join TblRecvCtDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo = T3.DocNo And T3.DRDocNo Is Not Null ");
            SQL.AppendLine("        Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    )");
            SQL.AppendLine("    Group By X1.DocNo ");
            SQL.AppendLine(") K On H.DRDocNo = K.DocNo ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 36;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "Warehouse",
                    "Customer",

                    //6-10
                    "DO#",
                    "",
                    "DO Date",
                    "Replaced",
                    "Item's"+Environment.NewLine+"Code",
                    
                    //11-15
                    "Item's Name",
                    "Batch#",
                    "Source",
                    "Lot",
                    "Bin",

                    //16-20
                    "Quantity",
                    "UoM",
                    "Quantity",
                    "UoM",
                    "Quantity",

                    //21-25
                    "Uom",
                    "Currency",
                    "Unit Price",
                    "Total",
                    "DOType",
                    
                    //26-30
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date",
                    //31-35
                    "Last"+Environment.NewLine+"Updated Time",
                    "Project Code",
                    "Project Name",
                    "Customer PO#",
                    "SiteCode"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 80, 50, 150, 150, 
                    
                    //6-10
                    130, 20, 80, 80, 100, 
                    
                    //11-15
                    200, 150, 200, 60, 80, 
                    
                    //16-20
                    100, 100, 100, 100, 100,
                    
                    //21-25
                    100, 60, 100, 120, 10, 
                    
                    //26-30
                    100, 100, 100, 100, 100, 

                    //31-34
                    100, 120, 200, 120, 0
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 7 });
            Sm.GrdColCheck(Grd1, new int[] { 3, 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 16, 18, 20, 23, 24 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 8, 27, 30 });
            Sm.GrdFormatTime(Grd1, new int[] { 28, 31 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 10, 13, 14, 15, 18, 19, 20, 21, 25, 26, 27, 28, 29, 30, 31, 35 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35 });
            if (!mFrmParent.mIsRecvCtShowPriceInfo)
                Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24 }, false);
            Grd1.Cols[32].Move(26);
            Grd1.Cols[33].Move(27);
            Grd1.Cols[34].Move(28);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 13, 14, 15, 26, 27, 28, 29, 30, 31 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "F.ItCode", "G.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "F.BatchNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocDt", "CancelInd", "WhsName", "CtName", "DOCtDocNo", 
                        
                        //6-10
                        "DOCtDocDt", "ReplaceInd", "ItCode", "ItName", "BatchNo", 
                        
                        //11-15
                        "Source", "Lot", "Bin", "Qty", "InventoryUomCode", 
                        
                        //16-20
                        "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", "CurCode", 
                        
                        //21-25
                        "UPrice", "Amt", "DOtype", "CreateBy", "CreateDt", 

                        //26-30
                        "LastUpBy", "LastUpDt", "ProjectCode", "ProjectName", "PONo",

                        //31
                        "SiteCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 27, 25);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 28, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 30, 27);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 31, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 28);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 29);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 30);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 31);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 25) == "1")
                {
                    var f = new FrmDOCt(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.ShowDialog();
                }
                else
                {
                    if (mFrmParent.mIsSalesContractEnabled)
                    {
                        var f = new FrmDOCt7(mFrmParent.mMenuCode);
                        f.Tag = mFrmParent.mMenuCode;
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                        f.ShowDialog();
                    }
                    else
                    {
                        var f = new FrmDOCt2(mFrmParent.mMenuCode);
                        f.Tag = mFrmParent.mMenuCode;
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                        f.ShowDialog();
                    }
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 25) == "1")
                {
                    var f = new FrmDOCt(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.ShowDialog();
                }
                else
                {
                    if (mFrmParent.mIsSalesContractEnabled && Sm.GetGrdStr(Grd1, e.RowIndex, 35).Length > 0)
                    {
                        var f = new FrmDOCt7(mFrmParent.mMenuCode);
                        f.Tag = mFrmParent.mMenuCode;
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                        f.ShowDialog();
                    }
                    else
                    {
                        var f = new FrmDOCt2(mFrmParent.mMenuCode);
                        f.Tag = mFrmParent.mMenuCode;
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                        f.ShowDialog();
                    }
                }
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        #endregion

        #endregion
    }
}
