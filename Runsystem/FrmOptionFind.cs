﻿#region Update
/*
    23/06/2021 [TKG/IMS] Tambah property dan remark
    27/06/2021 [TKG/IMS] Tambah property 4-10
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmOptionFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmOption mFrmParent;

        #endregion

        #region Constructor

        public FrmOptionFind(FrmOption FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Category", 
                        "Code",
                        "Description",   
                        "Custom", 
                        "Property 1",

                        //6-10
                        "Property 2",
                        "Property 3",
                        "Property 4",
                        "Property 5",
                        "Property 6",

                        //11-15
                        "Property 7",
                        "Property 8",
                        "Property 9",
                        "Property 10",
                        "Remark",

                        //16-20
                        "Created By",
                        "Created Date", 
                        "Created Time",
                        "Last Updated By", 
                        "Last Updated Date", 

                        //21
                        "LastUpdated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 17, 20 });
            Sm.GrdFormatTime(Grd1, new int[] { 18, 21 });
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 20, 21 }, !ChkHideInfoInGrd.Checked);
            if (!mFrmParent.mIsOptionPropertyEnabled)
                Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 20, 21 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                var Filter = string.Empty;
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                Sm.FilterStr(ref Filter, ref cm, TxtOptCode.Text, new string[] { "OptCat", "OptCode", "OptDesc" });

                SQL.AppendLine("Select OptCat, OptCode, OptDesc, Customize, ");
                if (mFrmParent.mIsOptionPropertyEnabled)
                {
                    SQL.AppendLine("Property1, Property2, Property3, Property4, Property5, ");
                    SQL.AppendLine("Property6, Property7, Property8, Property9, Property10, ");
                }
                else
                {
                    SQL.AppendLine("Null As Property1, Null As Property2, Null As Property3, Null As Property4, Null As Property5, ");
                    SQL.AppendLine("Null As Property6, Null As Property7, Null As Property8, Null As Property9, Null As Property10, ");
                }
                SQL.AppendLine("Remark, CreateBy, CreateDt, LastUpBy, LastUpDt ");
                SQL.AppendLine("From TblOption ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(" Order By OptCat, OptCode;");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "OptCat", 
                                
                            //1-5
                            "OptCode", "OptDesc", "Customize", "Property1", "Property2", 
                            
                            //6-10
                            "Property3", "Property4", "Property5", "Property6", "Property7", 

                            //11-15
                            "Property8", "Property9", "Property10", "Remark", "CreateBy", 
                            
                            //16-18
                            "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 18);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(
                    Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), 
                    Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtOptCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkOptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Option");
        }

        #endregion

        #endregion

    }
}
