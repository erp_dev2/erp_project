﻿#region Update
/*
    27/09/2017 [TKG] menggunakan employee salary for social security berdasarkan parameter SSSalaryInd
    19/12/2017 [TKG] tambah filter dept+site berdasarkan group
    16/05/2018 [TKG] tambah entity
    06/01/2019 [TKG] Untuk KMI, komponen allowance deduction berdasarkan start date dan end date-nya yg dibandingkan dengan cut off date
    23/10/2019 [TKG/SRN] SSForRetiring bisa diisi lebih dari 1 code
    01/11/2019 [TKG/SRN] perhitungan divalidasi minimum salary 
    03/09/2020 [TKG/SRN] Parameter SSProgramForEmployment bisa diisi lbh dari 1 kode ss
    13/01/2020 [TKG/PHT] Berdasarkan parameter IsEmpSSListExclPassAwalEmp, karyawan yg sudah meninggal tidak akan diproses datanya.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpSSList2 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            //mSSProgramForEmployment = string.Empty, 
            //mSSProgramForPension = string.Empty, 
            mSSForRetiring = string.Empty,
            mSSSalaryInd = string.Empty, 
            mSSAllowance = string.Empty;
        private decimal mSSRetiredMaxAge = 0m, mMaxLimitSalaryForSS = 0m;
        private List<SS> mListofSS = new List<SS>();
        private bool 
            mIsFilterBySiteHR = false, 
            mIsFilterByDeptHR = false, 
            mIsEmpSSBasedOnEntity = false,
            mIsEmpSSListExclPassAwalEmp = false
            ;

        #endregion

        #region Constructor

        public FrmRptEmpSSList2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                GenerateSS();
                DteDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                if (mIsEmpSSBasedOnEntity)
                    LblEntCode.ForeColor = Color.Red;
                else
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueEntCode }, true);
                Sl.SetLueEntCode(ref LueEntCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            //mSSProgramForEmployment = Sm.GetParameter("SSProgramForEmployment");
            //mSSProgramForPension = Sm.GetParameter("SSProgramForPension");
            mSSForRetiring = Sm.GetParameter("SSForRetiring");
            if (mSSForRetiring.Length == 0) mSSForRetiring = "XXX";
            mSSRetiredMaxAge = Sm.GetParameterDec("SSRetiredMaxAge");
            mSSSalaryInd = Sm.GetParameter("SSSalaryInd");
            mSSAllowance = Sm.GetParameter("SSAllowance");
            mMaxLimitSalaryForSS = Sm.GetParameterDec("MaxLimitSalaryForSS");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsEmpSSBasedOnEntity = Sm.GetParameterBoo("IsEmpSSBasedOnEntity");
            mIsEmpSSListExclPassAwalEmp = Sm.GetParameterBoo("IsEmpSSListExclPassAwalEmp");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.EmpCode, T.EmpName, T.EmpCodeOld, T.PosName, T.DeptName, T.SiteName, T.EntName, ");
            SQL.AppendLine("T.JoinDt, T.ResignDt, T.IDNo, T.BirthDt, ");
            SQL.AppendLine("T.Age, T.CardNo, T.StartDt, T.EndDt, T.SSCode, T.Salary, T.EmployerAmt, T.EmployeeAmt, T.TotalAmt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select A.EmpCode, C.EmpName, C.EmpCodeOld, D.PosName, C.DeptCode, E.DeptName, C.SiteCode, F.SiteName, H.EntName, ");
            SQL.AppendLine("C.JoinDt, C.ResignDt, C.IDNumber As IDNo, C.BirthDt, ");
            SQL.AppendLine("Case When C.BirthDt Is Null Then 0 Else TIMESTAMPDIFF(YEAR, STR_TO_DATE(C.BirthDt, '%Y%m%d'), STR_TO_DATE(@DtForAge, '%Y%m%d')) End As Age, ");
            SQL.AppendLine("A.SSCode, A.CardNo, A.StartDt, A.EndDt, ");
            if (mSSSalaryInd == "1")
            {
                SQL.AppendLine("Case When IfNull(G.EmploymentSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.EmploymentSSSalary, 0) End As Salary, ");
                SQL.AppendLine("B.EmployerPerc/100*Case When IfNull(G.EmploymentSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.EmploymentSSSalary, 0) End  As EmployerAmt, ");
                SQL.AppendLine("B.EmployeePerc/100*Case When IfNull(G.EmploymentSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.EmploymentSSSalary, 0) End  As EmployeeAmt, ");
                SQL.AppendLine("B.TotalPerc/100*Case When IfNull(G.EmploymentSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.EmploymentSSSalary, 0) End  As TotalAmt ");
            }
            else
            {
                SQL.AppendLine("Case When IfNull(B.SalaryMaxLimit, 0.00)=0.00 Then ");
                SQL.AppendLine("    Case When IfNull(B.SalaryMinLimit, 0.00)=0.00 Then ");
                SQL.AppendLine("        IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When IfNull(G.Salary, 0)>B.SalaryMaxLimit Then ");
                SQL.AppendLine("        B.SalaryMaxLimit ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("End As Salary, ");

                SQL.AppendLine("B.EmployerPerc, ");

                SQL.AppendLine("Case When B.SalaryMaxLimit=0.00 Then ");
                SQL.AppendLine("    Case When B.SalaryMinLimit=0.00 Then ");
                SQL.AppendLine("        B.EmployerPerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.EmployerPerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.EmployerPerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When IfNull(G.Salary, 0.00)>B.SalaryMaxLimit Then ");
                SQL.AppendLine("        B.EmployerPerc*0.01*B.SalaryMaxLimit ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.EmployerPerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.EmployerPerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("End As EmployerAmt, ");

                SQL.AppendLine("B.EmployeePerc, ");

                SQL.AppendLine("Case When B.SalaryMaxLimit=0.00 Then ");
                SQL.AppendLine("    Case When B.SalaryMinLimit=0.00 Then ");
                SQL.AppendLine("        B.EmployeePerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.EmployeePerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.EmployeePerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When IfNull(G.Salary, 0.00)>B.SalaryMaxLimit Then ");
                SQL.AppendLine("        B.EmployeePerc*0.01*B.SalaryMaxLimit ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.EmployeePerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.EmployeePerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("End As EmployeeAmt, ");

                SQL.AppendLine("B.TotalPerc, ");

                SQL.AppendLine("Case When B.SalaryMaxLimit=0.00 Then ");
                SQL.AppendLine("    Case When B.SalaryMinLimit=0.00 Then ");
                SQL.AppendLine("        B.TotalPerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.TotalPerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.TotalPerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When IfNull(G.Salary, 0.00)>B.SalaryMaxLimit Then ");
                SQL.AppendLine("        B.TotalPerc*0.01*B.SalaryMaxLimit ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.TotalPerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.TotalPerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("End As TotalAmt ");
            }

            SQL.AppendLine("From TblEmployeeSS A ");
            SQL.AppendLine("Inner Join TblSS B On A.SSCode=B.SSCode And B.SSPCode Is Not Null ");
            if (mSSSalaryInd != "2")
            {
                //SQL.AppendLine("And B.SSPCode=@SSProgramForEmployment ");
                SQL.AppendLine("        And Find_In_Set( ");
                SQL.AppendLine("            IfNull(B.SSPCode, ''), ");
                SQL.AppendLine("            IfNull((Select ParValue From TblParameter Where ParCode='SSProgramForEmployment' And ParValue Is Not Null), '') ");
                SQL.AppendLine("        ) ");
            }
            else
            {
                //SQL.AppendLine("And (B.SSPCode Like '%" + mSSProgramForEmployment + "%' Or B.SSPCode Like '%" + mSSProgramForPension + "%') ");
                SQL.AppendLine("        And ( ");
                SQL.AppendLine("        Find_In_Set( ");
                SQL.AppendLine("            IfNull(B.SSPCode, ''), ");
                SQL.AppendLine("            IfNull((Select ParValue From TblParameter Where ParCode='SSProgramForEmployment' And ParValue Is Not Null), '') ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        Or ");
                SQL.AppendLine("        Find_In_Set( ");
                SQL.AppendLine("            IfNull(B.SSPCode, ''), ");
                SQL.AppendLine("            IfNull((Select ParValue From TblParameter Where ParCode='SSProgramForPension' And ParValue Is Not Null), '') ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        )");
            }
            SQL.AppendLine("Inner Join TblEmployee C ");
            SQL.AppendLine("    On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("    And (C.ResignDt Is Null Or (C.ResignDt Is Not Null And C.ResignDt>Left(@CutOffDt, 8))) ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And (C.SiteCode Is Null Or (");
                SQL.AppendLine("    C.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    )) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And (C.DeptCode Is Null Or (");
                SQL.AppendLine("    C.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=C.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    )) ");
            }
            if (mIsEmpSSListExclPassAwalEmp)
            {
                SQL.AppendLine("And C.EmpCode Not In ( ");
                SQL.AppendLine("    Select Distinct A.EmpCode  ");
                SQL.AppendLine("    From TblPPS A, TblParameter B ");
                SQL.AppendLine("    Where B.ParValue Is Not Null  ");
                SQL.AppendLine("    And B.ParCode='JobTransferPassAway' ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status='A'  ");
                SQL.AppendLine("    And A.JobTransfer=B.ParValue ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblSite F On C.SiteCode=F.SiteCode ");
            if (mSSSalaryInd == "1")
            {
                SQL.AppendLine("Left Join TblGradeLevelHdr G On C.GrdLvlCode=G.GrdLvlCode ");
            }

            if (mSSSalaryInd == "2")
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.EmpCode, T1.StartDt, T1.Amt+IfNull(T3.Amt, 0) As Salary ");
                SQL.AppendLine("    From TblEmployeeSalary T1 ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select EmpCode, Max(StartDt) As StartDt ");
                SQL.AppendLine("        From TblEmployeeSalary ");
                SQL.AppendLine("        Where EmpCode In (");
                SQL.AppendLine("            Select EmpCode From TblEmployee ");
                SQL.AppendLine("            Where (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>Left(@CutOffDt, 8))) ");
                SQL.AppendLine("        ) And StartDt<Left(@CutOffDt, 8) ");
                SQL.AppendLine("        Group By EmpCode ");
                SQL.AppendLine("    ) T2 On T1.EmpCode=T2.EmpCode And T1.StartDt=T2.StartDt ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select EmpCode, Sum(Amt) As Amt ");
                SQL.AppendLine("        From TblEmployeeAllowanceDeduction ");
                SQL.AppendLine("        Where Position(ADCode In @SSAllowance) ");
                SQL.AppendLine("        And ( ");
                SQL.AppendLine("        (StartDt Is Null And EndDt Is Null) Or ");
                SQL.AppendLine("        (StartDt Is Not Null And EndDt Is Null And StartDt<=@CutOffDt) Or ");
                SQL.AppendLine("        (StartDt Is Null And EndDt Is Not Null And @CutOffDt<=EndDt) Or ");
                SQL.AppendLine("        (StartDt Is Not Null And EndDt Is Not Null And StartDt<=@CutOffDt And @CutOffDt<=EndDt) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("        Group By EmpCode ");
                SQL.AppendLine("    ) T3 On T1.EmpCode=T3.EmpCode ");
                SQL.AppendLine(") G On A.EmpCode=G.EmpCode ");
            }

            if (mSSSalaryInd == "3")
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.EmpCode, T1.Amt As Salary ");
                SQL.AppendLine("    From TblEmployeeSalarySS T1 ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select EmpCode, Max(StartDt) As StartDt ");
                SQL.AppendLine("        From TblEmployeeSalarySS ");
                SQL.AppendLine("        Where EmpCode In (");
                SQL.AppendLine("            Select EmpCode From TblEmployee ");
                SQL.AppendLine("            Where (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>Left(@CutOffDt, 8))) ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        And Left(@CutOffDt, 8)>=StartDt ");
                SQL.AppendLine("        And Left(@CutOffDt, 8)<=IfNull(EndDt, '99990101') ");
                //SQL.AppendLine("        And SSPCode=@SSProgramForEmployment ");
                SQL.AppendLine("        And SSPCode Is Not Null ");
                SQL.AppendLine("        And Find_In_Set( ");
                SQL.AppendLine("            IfNull(SSPCode, ''), ");
                SQL.AppendLine("            IfNull((Select ParValue From TblParameter Where ParCode='SSProgramForEmployment' And ParValue Is Not Null), '') ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        Group By EmpCode ");
                SQL.AppendLine("    ) T2 On T1.EmpCode=T2.EmpCode And T1.StartDt=T2.StartDt ");
                SQL.AppendLine("Where T1.SSPCode Is Not Null ");
                SQL.AppendLine("And Find_In_Set( ");
                SQL.AppendLine("    IfNull(T1.SSPCode, ''), ");
                SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParCode='SSProgramForEmployment' And ParValue Is Not Null), '') ");
                SQL.AppendLine("    ) ");
                //SQL.AppendLine("    Where T1.SSPCode=@SSProgramForEmployment ");
                SQL.AppendLine(") G On A.EmpCode=G.EmpCode ");
            }
            SQL.AppendLine("Left Join TblEntity H On A.EntCode=H.EntCode ");
            if (mIsEmpSSBasedOnEntity)
            {
                SQL.AppendLine("Where A.EntCode Is Not Null ");
                SQL.AppendLine("And A.EntCode=@EntCode ");
            }

            SQL.AppendLine(") T ");
            //SQL.AppendLine("Where (T.SSCode<>@SSForRetiring ");
            //SQL.AppendLine("Or ( ");
            //SQL.AppendLine("    T.SSCode=@SSForRetiring ");
            SQL.AppendLine("Where (Not Find_in_set(T.SSCode, IfNull(( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='SSForRetiring' And ParValue is Not Null ");
            SQL.AppendLine("    ), '')) ");
            SQL.AppendLine("Or ( ");
            SQL.AppendLine("    Find_in_set(T.SSCode, IfNull(( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='SSForRetiring' And ParValue is Not Null ");
            SQL.AppendLine("    ), '')) ");
            SQL.AppendLine("    And T.BirthDt Is Not Null ");
            SQL.AppendLine("    And TIMESTAMPDIFF(YEAR, STR_TO_DATE(T.BirthDt, '%Y%m%d'), STR_TO_DATE(@DtForAge, '%Y%m%d'))<@SSRetiredMaxAge ");
            SQL.AppendLine("    And Not ( ");
            SQL.AppendLine("        TIMESTAMPDIFF(YEAR, STR_TO_DATE(T.BirthDt, '%Y%m%d'), STR_TO_DATE(@DtForAge, '%Y%m%d'))=@SSRetiredMaxAge-1 ");
            SQL.AppendLine("        And (Substring(T.BirthDt, 5, 2)=@RetiringMth) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(")) ");
            SQL.AppendLine("And T.EndDt Is Null ");
            SQL.AppendLine("And ");
            SQL.AppendLine("(");
            SQL.AppendLine("    T.StartDt Is Null ");
            SQL.AppendLine("    Or ( T.StartDt Is Not Null And T.StartDt<=Left(@CutOffDt, 8))");
            SQL.AppendLine(") ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.Header.Rows.Count = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Employee"+Environment.NewLine+"Code",
                        "",
                        "Participants Name",
                        "Old"+Environment.NewLine+"Code",
                        "Position",
                        
                        //6-10
                        "Department",
                        "Site",
                        "Entity",
                        "Join"+Environment.NewLine+"Date",
                        "Resign"+Environment.NewLine+"Date",
                        
                        //11-15
                        "Identity#",
                        "Birth Date",
                        "Age",
                        "Card#",
                        "Start"+Environment.NewLine+"Date",

                        //16
                        "End"+Environment.NewLine+"Date"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        100, 20, 200, 80, 150, 
                        
                        //6-10
                        150, 150, 200, 100, 100, 
                        
                        //11-15
                        150, 100, 80, 130, 100, 
                        
                        //16
                        100
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.GrdFormatDec(Grd1, new int[] { 13 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 9, 10, 12, 15, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 9, 12 }, false);

            for (int Col = 0; Col <= 16; Col++)
                Grd1.Header.Cells[0, Col].SpanRows = 2;
        }

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 4].Value = "'" + Sm.GetGrdStr(Grd1, Row, 4);
                Grd1.Cells[Row, 11].Value = "'" + Sm.GetGrdStr(Grd1, Row, 11);
                Grd1.Cells[Row, 14].Value = "'" + Sm.GetGrdStr(Grd1, Row, 14);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                Grd1.Cells[Row, 4].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 4), Sm.GetGrdStr(Grd1, Row, 4).Length - 1);
                Grd1.Cells[Row, 11].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 11), Sm.GetGrdStr(Grd1, Row, 11).Length - 1);
                Grd1.Cells[Row, 14].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 14), Sm.GetGrdStr(Grd1, Row, 14).Length - 1);
            }
            Grd1.EndUpdate();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 9, 12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                if (Sm.IsDteEmpty(DteDt, "Cut-off date")) return;
                if (mIsEmpSSBasedOnEntity && Sm.IsLueEmpty(LueEntCode, "Entity")) return;

                Cursor.Current = Cursors.WaitCursor;

                int Index = -1;
                string Filter = " And 1=1 ";
                string EmpCode = string.Empty, SSCode = string.Empty;
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T.SiteCode", true);

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                //Sm.CmParam<String>(ref cm, "@SSProgramForEmployment", mSSProgramForEmployment);
                //if (mSSSalaryInd != "1") Sm.CmParam<String>(ref cm, "@SSProgramForPension", mSSProgramForPension);
                Sm.CmParam<String>(ref cm, "@SSForRetiring", mSSForRetiring);
                Sm.CmParam<Decimal>(ref cm, "@SSRetiredMaxAge", mSSRetiredMaxAge);
                Sm.CmParam<String>(ref cm, "@DtForAge", Sm.GetDte(DteDt).Substring(0, 6) + "01");
                Sm.CmParam<String>(ref cm, "@CutOffDt", Sm.GetDte(DteDt).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@RetiringMth", Sm.GetDte(DteDt).Substring(4, 2));
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

                if (mSSSalaryInd == "2")
                {
                    Sm.CmParam<String>(ref cm, "@SSAllowance", mSSAllowance);
                    Sm.CmParam<Decimal>(ref cm, "@MaxLimitSalaryForSS", mMaxLimitSalaryForSS);
                }

                Sm.ClearGrd(Grd1, false);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = mSQL.ToString() + Filter + " Order By T.DeptName, T.SiteName, T.EmpCode;";
                    cm.CommandTimeout = 600;
                    using (var dr = cm.ExecuteReader())
                    {
                        var c = Sm.GetOrdinal(dr, new string[] 
                            { 
                                //0
                                "EmpCode",  

                                //1-5
                                "EmpName", "EmpCodeOld", "PosName", "DeptName", "SiteName", 
                                
                                //6-10
                                "EntName", "JoinDt", "ResignDt", "IDNo", "BirthDt", 
                                
                                //11-15
                                "Age", "CardNo", "StartDt", "EndDt", "SSCode", 
                                
                                //16-19
                                "Salary", "EmployerAmt", "EmployeeAmt", "TotalAmt"
                            });
                        if (!dr.HasRows)
                            Sm.StdMsg(mMsgType.NoData, "");
                        else
                        {
                            int Row = 0, r = 0;
                            Grd1.Rows.Count = 0;
                            Grd1.BeginUpdate();
                            while (dr.Read())
                            {
                                if (!Sm.CompareStr(EmpCode, Sm.DrStr(dr, 0)))
                                {
                                    Grd1.Rows.Add();
                                    r++;
                                    Row=r-1;
                                    Grd1.Cells[Row, 0].Value = r;
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 7);

                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 10, 8);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 12, 10);
                                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 11);
                                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 12);
                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 15, 13);
                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 16, 14);
                                    SSCode = Sm.DrStr(dr, 15);
                                    if (Grd1.Cols.Count > 16)
                                    {
                                        for (int Col = 17; Col <= Grd1.Cols.Count - 1; Col++)
                                            Grd1.Cells[Row, Col].Value = 0m;
                                    }
                                }
                                else
                                    SSCode = Sm.DrStr(dr, 15);

                                Index = GetCol(SSCode);
                                if (Index >= 0)
                                {
                                    Index *= 4;
                                    Grd1.Cells[Row, 13 + Index].Value = Sm.DrDec(dr, 16);
                                    Grd1.Cells[Row, 14 + Index].Value = Sm.DrDec(dr, 17);
                                    Grd1.Cells[Row, 15 + Index].Value = Sm.DrDec(dr, 18);
                                    Grd1.Cells[Row, 16 + Index].Value = Sm.DrDec(dr, 19);
                                }
                                EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                            }
                        }

                        iGSubtotalManager.BackColor = Color.LightSalmon;
                        iGSubtotalManager.ShowSubtotalsInCells = true;
                        for (int Col = 17; Col <= Grd1.Cols.Count - 1; Col++)
                            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { Col });

                        Grd1.EndUpdate();
                        dr.Close();
                        dr.Dispose();
                        cm.Dispose();
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private int GetCol(string SSCode)
        {
            foreach(SS s in mListofSS)
            {
                if (Sm.CompareStr(s.SSCode, SSCode))
                    return s.Index;
            }
            return -1;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployeeSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployeeSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GenerateSS()
        {
            int Index = 0, Col1 = 0, Col2 = 0, Col3 = 0, Col4 = 0;
            
            var cm = new MySqlCommand();
            //Sm.CmParam<String>(ref cm, "@SSProgramForEmployment", mSSProgramForEmployment);
            //if (mSSSalaryInd != "1") Sm.CmParam<String>(ref cm, "@SSProgramForPension", mSSProgramForPension);
            
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SSCode, SSName From TblSS ");
            //SQL.AppendLine("Where (SSPCode=@SSProgramForEmployment ");
            //if (mSSSalaryInd != "1") SQL.AppendLine("Or SSPCode=@SSProgramForPension ");
            //SQL.AppendLine(");");

            SQL.AppendLine("Where SSPCode Is Not Null ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    Find_In_Set( ");
            SQL.AppendLine("        IfNull(SSPCode, ''), ");
            SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParCode='SSProgramForEmployment' And ParValue Is Not Null), '') ");
            SQL.AppendLine("        ) ");
            if (mSSSalaryInd != "1")
            {
                SQL.AppendLine("    Or ");
                SQL.AppendLine("    Find_In_Set( ");
                SQL.AppendLine("        IfNull(SSPCode, ''), ");
                SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParCode='SSProgramForPension' And ParValue Is Not Null), '') ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("); ");

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(), new string[]{ "SSCode", "SSName" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Index+=1;
                        mListofSS.Add(new SS()
                        {
                            Index = Index,
                            SSCode = Sm.DrStr(dr, c[0])
                        });
                        Grd1.Cols.Count += 4;
                        Col1 = 13 + (Index * 4);
                        Col2 = 14 + (Index * 4);
                        Col3 = 15 + (Index * 4);
                        Col4 = 16 + (Index * 4);

                        Grd1.Header.Cells[1, Col1].Value = Sm.DrStr(dr, c[1]);
                        Grd1.Header.Cells[1, Col1].TextAlign = iGContentAlignment.MiddleCenter;
                        Grd1.Header.Cells[1, Col1].SpanCols = 4;

                        Grd1.Cols[Col1].Text = "Salary";
                        Grd1.Cols[Col1].Width = 100;
                        Grd1.Cols[Col1].CellStyle.ValueType = typeof(Decimal);
                        Grd1.Header.Cells[0, Col1].TextAlign = iGContentAlignment.MiddleCenter;

                        Grd1.Cols[Col2].Text = "Employer";
                        Grd1.Cols[Col2].Width = 100;
                        Grd1.Cols[Col2].CellStyle.ValueType = typeof(Decimal);
                        Grd1.Header.Cells[0, Col2].TextAlign = iGContentAlignment.MiddleCenter;

                        Grd1.Cols[Col3].Text = "Employee";
                        Grd1.Cols[Col3].Width = 100;
                        Grd1.Cols[Col3].CellStyle.ValueType = typeof(Decimal);
                        Grd1.Header.Cells[0, Col3].TextAlign = iGContentAlignment.MiddleCenter;

                        Grd1.Cols[Col4].Text = "Total";
                        Grd1.Cols[Col4].Width = 100;
                        Grd1.Cols[Col4].CellStyle.ValueType = typeof(Decimal);
                        Grd1.Header.Cells[0, Col4].TextAlign = iGContentAlignment.MiddleCenter;
                        
                        Sm.GrdFormatDec(Grd1, new int[] { Col1, Col2, Col3, Col4 }, 0);
                        Sm.GrdColReadOnly(Grd1, new int[] { Col1, Col2, Col3, Col4 });
                    }, true
                );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
        }

        #endregion

        #endregion

        #region Class

        internal class SS
        {
            public int Index { get; set; }
            public string SSCode { get; set; }
        }

        #endregion
    }
}
