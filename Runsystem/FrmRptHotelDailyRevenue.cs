﻿#region Update
/*
    18/09/2017 [TKG] reporting Hotel's Daily Revenue.
    25/09/2017 [TKG] Revisi tampilan kolom
    05/10/2017 [TKG] menggunakan COA 4.1
    11/10/2017 [TKG] perhitungan menggunakan data tgl 1 hari sebelumnya.
    18/10/2017 [ARI] tambah kolom month
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptHotelDailyRevenue : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptHotelDailyRevenue(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                DteDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        private void SetGrd()
        {
            Grd1.Cols.Count = 34;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Date",
                        "Month",
                        "Name",
                        "Posting",
                        "Section",
                       

                        //6-10
                        "COA",
                        "Today",
                        "Budget"+Environment.NewLine+"Today",
                        "Variance"+Environment.NewLine+"Today",
                        "%"+Environment.NewLine+"Today",
                                                
                        //11-15
                        "Service Charge"+Environment.NewLine+"Today",
                        "Tax"+Environment.NewLine+"Today",
                        "Cost"+Environment.NewLine+"Today",
                        "Profit"+Environment.NewLine+"Today",
                        "% Profit"+Environment.NewLine+"Today",
                        
                        //16-20
                        "MTD",
                        "Budget"+Environment.NewLine+"MTD",
                        "Variance"+Environment.NewLine+"MTD",
                        "%"+Environment.NewLine+"MTD",
                        "Service Charge"+Environment.NewLine+"MTD",
                                                
                        //21-25
                        "Tax"+Environment.NewLine+"MTD",
                        "Cost"+Environment.NewLine+"MTD",
                        "Profit"+Environment.NewLine+"MTD",
                        "% Profit"+Environment.NewLine+"MTD",
                        "YTD",
                                                
                        //26-30
                        "Budget"+Environment.NewLine+"YTD",
                        "Variance"+Environment.NewLine+"YTD",
                        "%"+Environment.NewLine+"YTD",
                        "Service Charge"+Environment.NewLine+"YTD",
                        "Tax"+Environment.NewLine+"YTD",
                                                
                        //31-33
                        "Cost"+Environment.NewLine+"YTD",
                        "Profit"+Environment.NewLine+"YTD",
                        "% Profit"+Environment.NewLine+"YTD"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 100, 200, 200, 200,  
                        
                        //6-10
                        200, 120, 120, 120, 120,  
                        
                        //11-15
                        120, 120, 120, 120, 120,   

                        //16-20
                        120, 120, 120, 120, 120,  

                        //21-25
                        120, 120, 120, 120, 120,  

                        //26-30
                        120, 120, 120, 120, 120,  

                        //31-33
                        120, 120, 120
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 8, 9, 10, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 8, 9, 10, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Sm.IsDteEmpty(DteDt, "Date")) return;

            var mlResult = new List<Result>();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref mlResult);
                if (mlResult.Count > 0) 
                    Process2(ref mlResult);
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                mlResult.Clear();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void Process1(ref List<Result> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Name, Item_Type, Posting, Section, ");
            SQL.AppendLine("COA_ID, COA_Desc, ");
            SQL.AppendLine("Sum(Today) As Today, ");
            SQL.AppendLine("Sum(ServiceChargeToday) As ServiceChargeToday, ");
            SQL.AppendLine("Sum(TaxToday) As TaxToday, ");
            SQL.AppendLine("Sum(CostToday) As CostToday, ");
            SQL.AppendLine("Sum(MTD) As MTD, ");
            SQL.AppendLine("Sum(ServiceChargeMTD) As ServiceChargeMTD, ");
            SQL.AppendLine("Sum(TaxMTD) As TaxMTD, ");
            SQL.AppendLine("Sum(CostMTD) As CostMTD, ");
            SQL.AppendLine("Sum(YTD) As YTD, ");
            SQL.AppendLine("Sum(ServiceChargeYTD) As ServiceChargeYTD, ");
            SQL.AppendLine("Sum(TaxYTD) As TaxYTD, ");
            SQL.AppendLine("Sum(CostYTD) As CostYTD ");
            SQL.AppendLine("From ( ");

            //1
            SQL.AppendLine("Select Name, Item_Type, Posting, Section, ");
            SQL.AppendLine("COA_ID, COA_Desc, ");
            SQL.AppendLine("Sum(IfNull(Amount, 0.00)*IfNull(Currency_Rate, 0.00)) As Today, ");
            SQL.AppendLine("Sum(IfNull(service_tax_amt, 0.00)*IfNull(Currency_Rate, 0.00)) As ServiceChargeToday, ");
            SQL.AppendLine("Sum(IfNull(local_tax_amt, 0.00)*IfNull(Currency_Rate, 0.00)) As TaxToday, ");
            SQL.AppendLine("Sum(IfNull(cost, 0.00)*IfNull(Currency_Rate, 0.00)) As CostToday, ");
            SQL.AppendLine("0.00 As MTD, ");
            SQL.AppendLine("0.00 As ServiceChargeMTD, ");
            SQL.AppendLine("0.00 As TaxMTD, ");
            SQL.AppendLine("0.00 As CostMTD, ");
            SQL.AppendLine("0.00 As YTD, ");
            SQL.AppendLine("0.00 As ServiceChargeYTD, ");
            SQL.AppendLine("0.00 As TaxYTD, ");
            SQL.AppendLine("0.00 As CostYTD ");
            SQL.AppendLine("From TblTransactions Where Date=@Dt1 And COA_ID Like '4.1%' ");
            SQL.AppendLine("Group By Name, Item_Type, Posting, Section, ");
            SQL.AppendLine("COA_ID, COA_Desc ");

            //2
            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select Name, Item_Type, Posting, Section, ");
            SQL.AppendLine("COA_ID, COA_Desc, ");
            SQL.AppendLine("0.00 As Today, ");
            SQL.AppendLine("0.00 As ServiceChargeToday, ");
            SQL.AppendLine("0.00 As TaxToday, ");
            SQL.AppendLine("0.00 As CostToday, ");
            SQL.AppendLine("Sum(IfNull(Amount, 0.00)*IfNull(Currency_Rate, 0.00)) As MTD, ");
            SQL.AppendLine("Sum(IfNull(service_tax_amt, 0.00)*IfNull(Currency_Rate, 0.00)) As ServiceChargeMTD, ");
            SQL.AppendLine("Sum(IfNull(local_tax_amt, 0.00)*IfNull(Currency_Rate, 0.00)) As TaxMTD, ");
            SQL.AppendLine("Sum(IfNull(cost, 0.00)*IfNull(Currency_Rate, 0.00)) As CostMTD, ");
            SQL.AppendLine("0.00 As YTD, ");
            SQL.AppendLine("0.00 As ServiceChargeYTD, ");
            SQL.AppendLine("0.00 As TaxYTD, ");
            SQL.AppendLine("0.00 As CostYTD ");
            SQL.AppendLine("From TblTransactions Where Date Between @Dt2 And @Dt1 And COA_ID Like '4.1%' ");
            SQL.AppendLine("Group By Name, Item_Type, Posting, Section, ");
            SQL.AppendLine("COA_ID, COA_Desc ");

            //3
            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select Name, Item_Type, Posting, Section, ");
            SQL.AppendLine("COA_ID, COA_Desc, ");
            SQL.AppendLine("0.00 As Today, ");
            SQL.AppendLine("0.00 As ServiceChargeToday, ");
            SQL.AppendLine("0.00 As TaxToday, ");
            SQL.AppendLine("0.00 As CostToday, ");
            SQL.AppendLine("0.00 As MTD, ");
            SQL.AppendLine("0.00 As ServiceChargeMTD, ");
            SQL.AppendLine("0.00 As TaxMTD, ");
            SQL.AppendLine("0.00 As CostMTD, ");
            SQL.AppendLine("Sum(IfNull(Amount, 0.00)*IfNull(Currency_Rate, 0.00)) As MTD, ");
            SQL.AppendLine("Sum(IfNull(service_tax_amt, 0.00)*IfNull(Currency_Rate, 0.00)) As ServiceChargeMTD, ");
            SQL.AppendLine("Sum(IfNull(local_tax_amt, 0.00)*IfNull(Currency_Rate, 0.00)) As TaxMTD, ");
            SQL.AppendLine("Sum(IfNull(cost, 0.00)*IfNull(Currency_Rate, 0.00)) As CostMTD ");
            SQL.AppendLine("From TblTransactions Where Date Between @Dt3 And @Dt1 And COA_ID Like '4.1%' ");
            SQL.AppendLine("Group By Name, Item_Type, Posting, Section, ");
            SQL.AppendLine("COA_ID, COA_Desc ");

            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By Name, Item_Type, Posting, Section, ");
            SQL.AppendLine("COA_ID, COA_Desc; ");

            var Dt = Sm.GetDte(DteDt);
            var Dt1 = Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteDt)).AddDays(-1));
            var Dt2 = Sm.FormatDate(Sm.ConvertDate(String.Concat(Sm.Left(Dt, 6), "01")).AddDays(-1));
            var Dt3 = Sm.FormatDate(Sm.ConvertDate(String.Concat(Sm.Left(Dt, 4), "0101")).AddDays(-1));

            Sm.CmParamDt(ref cm, "@Dt1", Dt1);
            Sm.CmParamDt(ref cm, "@Dt2", Dt2);
            Sm.CmParamDt(ref cm, "@Dt3", Dt3);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Name", 
                    
                    //1-5
                    "Item_Type", 
                    "Posting", 
                    "Section", 
                    "COA_ID", 
                    "COA_Desc", 

                    //6-10
                    "Today", 
                    "ServiceChargeToday", 
                    "TaxToday", 
                    "CostToday", 
                    "MTD",
 
                    //11-15
                    "ServiceChargeMTD", 
                    "TaxMTD", 
                    "CostMTD", 
                    "YTD", 
                    "ServiceChargeYTD", 

                    //16-17
                    "TaxYTD", 
                    "CostYTD"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result()
                        {
                            name = Sm.DrStr(dr, c[0]),
                            item_type = Sm.DrStr(dr, c[1]),
                            posting = Sm.DrStr(dr, c[2]),
                            section = Sm.DrStr(dr, c[3]),
                            coa_id = Sm.DrStr(dr, c[4]),
                            coa_desc = Sm.DrStr(dr, c[5]),
                            Today = Sm.DrDec(dr, c[6]),
                            BudgetToday = 0m,
                            ServiceChargeToday = Sm.DrDec(dr, c[7]),
                            TaxToday = Sm.DrDec(dr, c[8]),
                            CostToday = Sm.DrDec(dr, c[9]),
                            MTD = Sm.DrDec(dr, c[10]),
                            BudgetMTD = 0m,
                            ServiceChargeMTD = Sm.DrDec(dr, c[11]),
                            TaxMTD = Sm.DrDec(dr, c[12]),
                            CostMTD = Sm.DrDec(dr, c[13]),
                            YTD = Sm.DrDec(dr, c[14]),
                            BudgetYTD = 0m,
                            ServiceChargeYTD = Sm.DrDec(dr, c[15]),
                            TaxYTD = Sm.DrDec(dr, c[16]),
                            CostYTD = Sm.DrDec(dr, c[17])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Result> l)
        {
            iGRow r;
            string Dt = Sm.Left(Sm.GetDte(DteDt), 8);
            int Yr = Int32.Parse(Sm.Left(Dt,4));
            var Month = Dt.Substring(4, 2);
            int Mth = Int32.Parse(Month);
            int Day = Int32.Parse(Sm.Right(Dt, 2));
            DateTime dt = new DateTime(Yr, Mth, Day, 0, 0, 0, 0);
            var Name = string.Empty;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[1].Value = Sm.ConvertDate(Dt);
                r.Cells[2].Value = string.Format("{0:MMMM}",dt);
                Name = string.Empty;
                if (l[i].name.Length > 0)
                {
                    if (l[i].item_type.Length > 0)
                        Name = string.Concat(l[i].name, " (", l[i].item_type, ")");
                    else
                        Name = l[i].name;
                }
                else
                {
                    if (l[i].item_type.Length > 0) Name = l[i].item_type;
                }
                r.Cells[3].Value = Name;
                r.Cells[4].Value = l[i].posting;
                r.Cells[5].Value = l[i].section;
                r.Cells[6].Value = string.Concat(l[i].coa_id, " (", l[i].coa_desc, ")");
                r.Cells[7].Value = l[i].Today;
                r.Cells[8].Value = l[i].BudgetToday;
                r.Cells[9].Value = l[i].Today - l[i].BudgetToday;
                r.Cells[10].Value = l[i].Today==0m?0m:((l[i].Today - l[i].BudgetToday) / l[i].Today) * 100m;
                r.Cells[11].Value = l[i].ServiceChargeToday;
                r.Cells[12].Value = l[i].TaxToday;
                r.Cells[13].Value = l[i].CostToday;
                r.Cells[14].Value = l[i].Today - l[i].CostToday;
                r.Cells[15].Value = l[i].CostToday==0m?0m:((l[i].Today - l[i].CostToday) / l[i].CostToday) * 100m;
                r.Cells[16].Value = l[i].MTD;
                r.Cells[17].Value = l[i].BudgetMTD;
                r.Cells[18].Value = l[i].Today - l[i].BudgetMTD;
                r.Cells[19].Value = l[i].Today == 0m ? 0m : ((l[i].MTD - l[i].BudgetMTD) / l[i].MTD) * 100m;
                r.Cells[20].Value = l[i].ServiceChargeMTD;
                r.Cells[21].Value = l[i].TaxMTD;
                r.Cells[22].Value = l[i].CostMTD;
                r.Cells[23].Value = l[i].MTD - l[i].CostMTD;
                r.Cells[24].Value = l[i].CostMTD == 0m ? 0m : ((l[i].MTD - l[i].CostMTD) / l[i].CostMTD) * 100m;
                r.Cells[25].Value = l[i].YTD;
                r.Cells[26].Value = l[i].BudgetYTD;
                r.Cells[27].Value = l[i].YTD - l[i].BudgetYTD;
                r.Cells[28].Value = l[i].YTD == 0m ? 0m : ((l[i].YTD - l[i].BudgetYTD) / l[i].YTD) * 100m;
                r.Cells[29].Value = l[i].ServiceChargeYTD;
                r.Cells[30].Value = l[i].TaxYTD;
                r.Cells[31].Value = l[i].CostYTD;
                r.Cells[32].Value = l[i].YTD - l[i].CostYTD;
                r.Cells[33].Value = l[i].CostYTD == 0m ? 0m : ((l[i].YTD - l[i].CostYTD) / l[i].CostYTD) * 100m; ;
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, 
                new int[] { 
                    7, 8, 9, 11,
                    12, 13, 14, 16,
                    17, 18, 20, 21,
                    22, 23, 25, 26,
                    27, 29, 30, 31,
                    32
                });
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string name { get; set; }
            public string item_type { get; set; }
            public string posting { get; set; }
            public string section { get; set; }
            public string coa_id { get; set; }
            public string coa_desc { get; set; }
            public decimal Today { get; set; }
            public decimal BudgetToday { get; set; }
            public decimal ServiceChargeToday { get; set; }
            public decimal TaxToday { get; set; }
            public decimal CostToday { get; set; }
            public decimal MTD { get; set; }
            public decimal BudgetMTD { get; set; }
            public decimal ServiceChargeMTD { get; set; }
            public decimal TaxMTD { get; set; }
            public decimal CostMTD { get; set; }
            public decimal YTD { get; set; }
            public decimal BudgetYTD { get; set; }
            public decimal ServiceChargeYTD { get; set; }
            public decimal TaxYTD { get; set; }
            public decimal CostYTD { get; set; }
        }

        #endregion
    }
}
