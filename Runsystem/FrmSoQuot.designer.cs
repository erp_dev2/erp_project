﻿namespace RunSystem
{
    partial class FrmSoQuot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSoQuot));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgQuotationCity = new System.Windows.Forms.TabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgQuotationItem = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.label2 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtPreviousDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LueRingCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.DteQtStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.LuePtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.LueSPCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.DteDepositExpDt = new DevExpress.XtraEditors.DateEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.BtnCtContactPersonName = new DevExpress.XtraEditors.SimpleButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.MeeWhsCapacity = new DevExpress.XtraEditors.MemoExEdit();
            this.LueShpMCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtCreditLimit = new DevExpress.XtraEditors.TextEdit();
            this.ChkPrintSignatureInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtGoodsReturnDay = new DevExpress.XtraEditors.TextEdit();
            this.TxtMinDelivery = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtDepositAmt = new DevExpress.XtraEditors.TextEdit();
            this.LueAgingAP = new DevExpress.XtraEditors.LookUpEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtSalesTarget = new DevExpress.XtraEditors.TextEdit();
            this.TxtQtMth = new DevExpress.XtraEditors.TextEdit();
            this.LueCtContactPersonName = new DevExpress.XtraEditors.LookUpEdit();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.TpgQuotationCity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.TpgQuotationItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPreviousDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueRingCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDepositExpDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDepositExpDt.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeWhsCapacity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShpMCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreditLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPrintSignatureInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGoodsReturnDay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinDelivery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDepositAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAgingAP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesTarget.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtMth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtContactPersonName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Size = new System.Drawing.Size(70, 572);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 572);
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgQuotationCity);
            this.tabControl1.Controls.Add(this.TpgQuotationItem);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 270);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(772, 302);
            this.tabControl1.TabIndex = 59;
            // 
            // TpgQuotationCity
            // 
            this.TpgQuotationCity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgQuotationCity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgQuotationCity.Controls.Add(this.Grd1);
            this.TpgQuotationCity.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgQuotationCity.Location = new System.Drawing.Point(4, 26);
            this.TpgQuotationCity.Name = "TpgQuotationCity";
            this.TpgQuotationCity.Size = new System.Drawing.Size(764, 272);
            this.TpgQuotationCity.TabIndex = 0;
            this.TpgQuotationCity.Text = "City";
            this.TpgQuotationCity.UseVisualStyleBackColor = true;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(760, 268);
            this.Grd1.TabIndex = 60;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // TpgQuotationItem
            // 
            this.TpgQuotationItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgQuotationItem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgQuotationItem.Controls.Add(this.Grd2);
            this.TpgQuotationItem.Location = new System.Drawing.Point(4, 26);
            this.TpgQuotationItem.Name = "TpgQuotationItem";
            this.TpgQuotationItem.Size = new System.Drawing.Size(764, 0);
            this.TpgQuotationItem.TabIndex = 1;
            this.TpgQuotationItem.Text = "Item";
            this.TpgQuotationItem.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 20;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.ReadOnly = true;
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(764, 0);
            this.Grd2.TabIndex = 63;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(57, 52);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Document Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(155, 48);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(117, 20);
            this.DteDocDt.TabIndex = 15;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(155, 90);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 12;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 500;
            this.LueCtCode.Size = new System.Drawing.Size(230, 20);
            this.LueCtCode.TabIndex = 19;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(92, 92);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 18;
            this.label3.Text = "Customer";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(155, 26);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(67, 22);
            this.ChkActInd.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(40, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document Number";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(155, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(29, 72);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "Previous Document#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPreviousDocNo
            // 
            this.TxtPreviousDocNo.EnterMoveNextControl = true;
            this.TxtPreviousDocNo.Location = new System.Drawing.Point(155, 69);
            this.TxtPreviousDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPreviousDocNo.Name = "TxtPreviousDocNo";
            this.TxtPreviousDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPreviousDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPreviousDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPreviousDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPreviousDocNo.Properties.MaxLength = 30;
            this.TxtPreviousDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtPreviousDocNo.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(4, 114);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 14);
            this.label5.TabIndex = 20;
            this.label5.Text = "Customer Contact Person";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueRingCode
            // 
            this.LueRingCode.EnterMoveNextControl = true;
            this.LueRingCode.Location = new System.Drawing.Point(155, 132);
            this.LueRingCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueRingCode.Name = "LueRingCode";
            this.LueRingCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.Appearance.Options.UseFont = true;
            this.LueRingCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueRingCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueRingCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueRingCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueRingCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueRingCode.Properties.DropDownRows = 12;
            this.LueRingCode.Properties.NullText = "[Empty]";
            this.LueRingCode.Properties.PopupWidth = 500;
            this.LueRingCode.Size = new System.Drawing.Size(230, 20);
            this.LueRingCode.TabIndex = 24;
            this.LueRingCode.ToolTip = "F4 : Show/hide list";
            this.LueRingCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueRingCode.EditValueChanged += new System.EventHandler(this.LueRingCode_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(36, 135);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 14);
            this.label6.TabIndex = 23;
            this.label6.Text = "Customer Ring Area";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(28, 177);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 14);
            this.label8.TabIndex = 26;
            this.label8.Text = "Quotation Start Date";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteQtStartDt
            // 
            this.DteQtStartDt.EditValue = null;
            this.DteQtStartDt.EnterMoveNextControl = true;
            this.DteQtStartDt.Location = new System.Drawing.Point(155, 174);
            this.DteQtStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteQtStartDt.Name = "DteQtStartDt";
            this.DteQtStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQtStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteQtStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQtStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteQtStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteQtStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteQtStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQtStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteQtStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQtStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteQtStartDt.Properties.MaxLength = 8;
            this.DteQtStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteQtStartDt.Size = new System.Drawing.Size(117, 20);
            this.DteQtStartDt.TabIndex = 27;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(76, 218);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 14);
            this.label9.TabIndex = 31;
            this.label9.Text = "Sales Target";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePtCode
            // 
            this.LuePtCode.EnterMoveNextControl = true;
            this.LuePtCode.Location = new System.Drawing.Point(130, 6);
            this.LuePtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePtCode.Name = "LuePtCode";
            this.LuePtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.Appearance.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePtCode.Properties.DropDownRows = 12;
            this.LuePtCode.Properties.NullText = "[Empty]";
            this.LuePtCode.Properties.PopupWidth = 500;
            this.LuePtCode.Size = new System.Drawing.Size(241, 20);
            this.LuePtCode.TabIndex = 35;
            this.LuePtCode.ToolTip = "F4 : Show/hide list";
            this.LuePtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePtCode.EditValueChanged += new System.EventHandler(this.LuePtCode_EditValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(22, 9);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 14);
            this.label10.TabIndex = 44;
            this.label10.Text = "Term Of Payment";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(10, 198);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(118, 14);
            this.label12.TabIndex = 54;
            this.label12.Text = "Warehouse Capacity";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(28, 135);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(99, 14);
            this.label14.TabIndex = 46;
            this.label14.Text = "Shipping Method";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(130, 48);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 12;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 500;
            this.LueCurCode.Size = new System.Drawing.Size(117, 20);
            this.LueCurCode.TabIndex = 39;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(72, 51);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 14);
            this.label15.TabIndex = 38;
            this.label15.Text = "Currency";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSPCode
            // 
            this.LueSPCode.EnterMoveNextControl = true;
            this.LueSPCode.Location = new System.Drawing.Point(155, 153);
            this.LueSPCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSPCode.Name = "LueSPCode";
            this.LueSPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.Appearance.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSPCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSPCode.Properties.DropDownRows = 12;
            this.LueSPCode.Properties.NullText = "[Empty]";
            this.LueSPCode.Properties.PopupWidth = 500;
            this.LueSPCode.Size = new System.Drawing.Size(230, 20);
            this.LueSPCode.TabIndex = 25;
            this.LueSPCode.ToolTip = "F4 : Show/hide list";
            this.LueSPCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSPCode.EditValueChanged += new System.EventHandler(this.LueSPCode_EditValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(76, 156);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 14);
            this.label16.TabIndex = 24;
            this.label16.Text = "Sales Person";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(14, 30);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(113, 14);
            this.label18.TabIndex = 36;
            this.label18.Text = "Aging AP Based On";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(59, 72);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 14);
            this.label19.TabIndex = 40;
            this.label19.Text = "Credit Limit";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(45, 177);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(82, 14);
            this.label20.TabIndex = 51;
            this.label20.Text = "Goods Return";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(30, 156);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(100, 14);
            this.label21.TabIndex = 48;
            this.label21.Text = "Minimum Delivery";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(31, 93);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(96, 14);
            this.label24.TabIndex = 42;
            this.label24.Text = "Deposit Amount";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(5, 114);
            this.label25.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(122, 14);
            this.label25.TabIndex = 44;
            this.label25.Text = "Deposit Expired Date";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDepositExpDt
            // 
            this.DteDepositExpDt.EditValue = null;
            this.DteDepositExpDt.EnterMoveNextControl = true;
            this.DteDepositExpDt.Location = new System.Drawing.Point(130, 111);
            this.DteDepositExpDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDepositExpDt.Name = "DteDepositExpDt";
            this.DteDepositExpDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDepositExpDt.Properties.Appearance.Options.UseFont = true;
            this.DteDepositExpDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDepositExpDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDepositExpDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDepositExpDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDepositExpDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDepositExpDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDepositExpDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDepositExpDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDepositExpDt.Properties.MaxLength = 8;
            this.DteDepositExpDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDepositExpDt.Size = new System.Drawing.Size(117, 20);
            this.DteDepositExpDt.TabIndex = 45;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.BtnCtContactPersonName);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.TxtSalesTarget);
            this.panel3.Controls.Add(this.TxtQtMth);
            this.panel3.Controls.Add(this.LueCtContactPersonName);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.LueSPCode);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.DteQtStartDt);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.LueRingCode);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.TxtPreviousDocNo);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.ChkActInd);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.LueCtCode);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 270);
            this.panel3.TabIndex = 10;
            // 
            // BtnCtContactPersonName
            // 
            this.BtnCtContactPersonName.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtContactPersonName.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtContactPersonName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtContactPersonName.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtContactPersonName.Appearance.Options.UseBackColor = true;
            this.BtnCtContactPersonName.Appearance.Options.UseFont = true;
            this.BtnCtContactPersonName.Appearance.Options.UseForeColor = true;
            this.BtnCtContactPersonName.Appearance.Options.UseTextOptions = true;
            this.BtnCtContactPersonName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtContactPersonName.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtContactPersonName.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtContactPersonName.Image")));
            this.BtnCtContactPersonName.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtContactPersonName.Location = new System.Drawing.Point(361, 110);
            this.BtnCtContactPersonName.Name = "BtnCtContactPersonName";
            this.BtnCtContactPersonName.Size = new System.Drawing.Size(24, 21);
            this.BtnCtContactPersonName.TabIndex = 22;
            this.BtnCtContactPersonName.ToolTip = "Find Item";
            this.BtnCtContactPersonName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtContactPersonName.ToolTipTitle = "Run System";
            this.BtnCtContactPersonName.Click += new System.EventHandler(this.BtnCtContactPersonName_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.MeeWhsCapacity);
            this.panel5.Controls.Add(this.LueShpMCode);
            this.panel5.Controls.Add(this.LuePtCode);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.TxtCreditLimit);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.ChkPrintSignatureInd);
            this.panel5.Controls.Add(this.TxtGoodsReturnDay);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.TxtMinDelivery);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.label28);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.TxtDepositAmt);
            this.panel5.Controls.Add(this.LueAgingAP);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.DteDepositExpDt);
            this.panel5.Controls.Add(this.label24);
            this.panel5.Controls.Add(this.label25);
            this.panel5.Controls.Add(this.LueCurCode);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(390, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(378, 266);
            this.panel5.TabIndex = 34;
            // 
            // MeeWhsCapacity
            // 
            this.MeeWhsCapacity.EnterMoveNextControl = true;
            this.MeeWhsCapacity.Location = new System.Drawing.Point(130, 195);
            this.MeeWhsCapacity.Margin = new System.Windows.Forms.Padding(5);
            this.MeeWhsCapacity.Name = "MeeWhsCapacity";
            this.MeeWhsCapacity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWhsCapacity.Properties.Appearance.Options.UseFont = true;
            this.MeeWhsCapacity.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWhsCapacity.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeWhsCapacity.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWhsCapacity.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeWhsCapacity.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWhsCapacity.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeWhsCapacity.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWhsCapacity.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeWhsCapacity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeWhsCapacity.Properties.MaxLength = 1000;
            this.MeeWhsCapacity.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeWhsCapacity.Properties.ShowIcon = false;
            this.MeeWhsCapacity.Size = new System.Drawing.Size(241, 20);
            this.MeeWhsCapacity.TabIndex = 55;
            this.MeeWhsCapacity.ToolTip = "F4 : Show/hide text";
            this.MeeWhsCapacity.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeWhsCapacity.ToolTipTitle = "Run System";
            // 
            // LueShpMCode
            // 
            this.LueShpMCode.EnterMoveNextControl = true;
            this.LueShpMCode.Location = new System.Drawing.Point(130, 132);
            this.LueShpMCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueShpMCode.Name = "LueShpMCode";
            this.LueShpMCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.Appearance.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShpMCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShpMCode.Properties.DropDownRows = 12;
            this.LueShpMCode.Properties.NullText = "[Empty]";
            this.LueShpMCode.Properties.PopupWidth = 500;
            this.LueShpMCode.Size = new System.Drawing.Size(241, 20);
            this.LueShpMCode.TabIndex = 47;
            this.LueShpMCode.ToolTip = "F4 : Show/hide list";
            this.LueShpMCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShpMCode.EditValueChanged += new System.EventHandler(this.LueShpMCode_EditValueChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(251, 155);
            this.label23.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(25, 14);
            this.label23.TabIndex = 50;
            this.label23.Text = "Kg ";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(251, 177);
            this.label22.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 14);
            this.label22.TabIndex = 53;
            this.label22.Text = "Day(s)";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCreditLimit
            // 
            this.TxtCreditLimit.EnterMoveNextControl = true;
            this.TxtCreditLimit.Location = new System.Drawing.Point(130, 69);
            this.TxtCreditLimit.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCreditLimit.Name = "TxtCreditLimit";
            this.TxtCreditLimit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCreditLimit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCreditLimit.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCreditLimit.Properties.Appearance.Options.UseFont = true;
            this.TxtCreditLimit.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCreditLimit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCreditLimit.Properties.MaxLength = 18;
            this.TxtCreditLimit.Size = new System.Drawing.Size(117, 20);
            this.TxtCreditLimit.TabIndex = 41;
            this.TxtCreditLimit.Validated += new System.EventHandler(this.TxtCreditLimit_Validated);
            // 
            // ChkPrintSignatureInd
            // 
            this.ChkPrintSignatureInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkPrintSignatureInd.Location = new System.Drawing.Point(130, 238);
            this.ChkPrintSignatureInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkPrintSignatureInd.Name = "ChkPrintSignatureInd";
            this.ChkPrintSignatureInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkPrintSignatureInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPrintSignatureInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkPrintSignatureInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkPrintSignatureInd.Properties.Appearance.Options.UseFont = true;
            this.ChkPrintSignatureInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkPrintSignatureInd.Properties.Caption = "Print Signature";
            this.ChkPrintSignatureInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPrintSignatureInd.Size = new System.Drawing.Size(117, 22);
            this.ChkPrintSignatureInd.TabIndex = 58;
            // 
            // TxtGoodsReturnDay
            // 
            this.TxtGoodsReturnDay.EnterMoveNextControl = true;
            this.TxtGoodsReturnDay.Location = new System.Drawing.Point(130, 174);
            this.TxtGoodsReturnDay.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGoodsReturnDay.Name = "TxtGoodsReturnDay";
            this.TxtGoodsReturnDay.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGoodsReturnDay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGoodsReturnDay.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGoodsReturnDay.Properties.Appearance.Options.UseFont = true;
            this.TxtGoodsReturnDay.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGoodsReturnDay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtGoodsReturnDay.Properties.MaxLength = 6;
            this.TxtGoodsReturnDay.Size = new System.Drawing.Size(117, 20);
            this.TxtGoodsReturnDay.TabIndex = 52;
            this.TxtGoodsReturnDay.Validated += new System.EventHandler(this.TxtGoodsReturnDay_Validated);
            // 
            // TxtMinDelivery
            // 
            this.TxtMinDelivery.EnterMoveNextControl = true;
            this.TxtMinDelivery.Location = new System.Drawing.Point(130, 153);
            this.TxtMinDelivery.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMinDelivery.Name = "TxtMinDelivery";
            this.TxtMinDelivery.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMinDelivery.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMinDelivery.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMinDelivery.Properties.Appearance.Options.UseFont = true;
            this.TxtMinDelivery.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMinDelivery.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMinDelivery.Properties.MaxLength = 18;
            this.TxtMinDelivery.Size = new System.Drawing.Size(117, 20);
            this.TxtMinDelivery.TabIndex = 49;
            this.TxtMinDelivery.Validated += new System.EventHandler(this.TxtMinDelivery_Validated);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(81, 219);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(47, 14);
            this.label28.TabIndex = 56;
            this.label28.Text = "Remark";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(130, 216);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 1000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(241, 20);
            this.MeeRemark.TabIndex = 57;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // TxtDepositAmt
            // 
            this.TxtDepositAmt.EnterMoveNextControl = true;
            this.TxtDepositAmt.Location = new System.Drawing.Point(130, 90);
            this.TxtDepositAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDepositAmt.Name = "TxtDepositAmt";
            this.TxtDepositAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDepositAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDepositAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDepositAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtDepositAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDepositAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDepositAmt.Properties.MaxLength = 18;
            this.TxtDepositAmt.Size = new System.Drawing.Size(117, 20);
            this.TxtDepositAmt.TabIndex = 43;
            // 
            // LueAgingAP
            // 
            this.LueAgingAP.EnterMoveNextControl = true;
            this.LueAgingAP.Location = new System.Drawing.Point(130, 27);
            this.LueAgingAP.Margin = new System.Windows.Forms.Padding(5);
            this.LueAgingAP.Name = "LueAgingAP";
            this.LueAgingAP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.Appearance.Options.UseFont = true;
            this.LueAgingAP.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAgingAP.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAgingAP.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAgingAP.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAgingAP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAgingAP.Properties.DropDownRows = 12;
            this.LueAgingAP.Properties.NullText = "[Empty]";
            this.LueAgingAP.Properties.PopupWidth = 500;
            this.LueAgingAP.Size = new System.Drawing.Size(241, 20);
            this.LueAgingAP.TabIndex = 37;
            this.LueAgingAP.ToolTip = "F4 : Show/hide list";
            this.LueAgingAP.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAgingAP.EditValueChanged += new System.EventHandler(this.LueAgingAP_EditValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(275, 219);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 14);
            this.label17.TabIndex = 33;
            this.label17.Text = "/month";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(275, 198);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 14);
            this.label11.TabIndex = 30;
            this.label11.Text = "month(s)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(127, 198);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 14);
            this.label7.TabIndex = 28;
            this.label7.Text = "For";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSalesTarget
            // 
            this.TxtSalesTarget.EnterMoveNextControl = true;
            this.TxtSalesTarget.Location = new System.Drawing.Point(155, 216);
            this.TxtSalesTarget.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSalesTarget.Name = "TxtSalesTarget";
            this.TxtSalesTarget.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSalesTarget.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSalesTarget.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSalesTarget.Properties.Appearance.Options.UseFont = true;
            this.TxtSalesTarget.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSalesTarget.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSalesTarget.Properties.MaxLength = 18;
            this.TxtSalesTarget.Size = new System.Drawing.Size(117, 20);
            this.TxtSalesTarget.TabIndex = 32;
            this.TxtSalesTarget.Validated += new System.EventHandler(this.TxtSalesTarget_Validated);
            // 
            // TxtQtMth
            // 
            this.TxtQtMth.EnterMoveNextControl = true;
            this.TxtQtMth.Location = new System.Drawing.Point(155, 195);
            this.TxtQtMth.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQtMth.Name = "TxtQtMth";
            this.TxtQtMth.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtQtMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQtMth.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQtMth.Properties.Appearance.Options.UseFont = true;
            this.TxtQtMth.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQtMth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQtMth.Properties.MaxLength = 6;
            this.TxtQtMth.Size = new System.Drawing.Size(117, 20);
            this.TxtQtMth.TabIndex = 29;
            this.TxtQtMth.Validated += new System.EventHandler(this.TxtQtMth_Validated);
            // 
            // LueCtContactPersonName
            // 
            this.LueCtContactPersonName.EnterMoveNextControl = true;
            this.LueCtContactPersonName.Location = new System.Drawing.Point(155, 111);
            this.LueCtContactPersonName.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtContactPersonName.Name = "LueCtContactPersonName";
            this.LueCtContactPersonName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.Appearance.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtContactPersonName.Properties.DropDownRows = 12;
            this.LueCtContactPersonName.Properties.NullText = "[Empty]";
            this.LueCtContactPersonName.Properties.PopupWidth = 500;
            this.LueCtContactPersonName.Size = new System.Drawing.Size(205, 20);
            this.LueCtContactPersonName.TabIndex = 21;
            this.LueCtContactPersonName.ToolTip = "F4 : Show/hide list";
            this.LueCtContactPersonName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtContactPersonName.EditValueChanged += new System.EventHandler(this.LueCtContactPersonName_EditValueChanged);
            this.LueCtContactPersonName.ProcessNewValue += new DevExpress.XtraEditors.Controls.ProcessNewValueEventHandler(this.LueCtContactPersonName_ProcessNewValue);
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 530);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 8;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 550);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 9;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // FrmSoQuot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 572);
            this.Name = "FrmSoQuot";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.TpgQuotationCity.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.TpgQuotationItem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPreviousDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueRingCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDepositExpDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDepositExpDt.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeWhsCapacity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShpMCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreditLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPrintSignatureInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGoodsReturnDay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinDelivery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDepositAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAgingAP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesTarget.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtMth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtContactPersonName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgQuotationCity;
        private System.Windows.Forms.TabPage TpgQuotationItem;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.DateEdit DteDepositExpDt;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.LookUpEdit LueSPCode;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.LookUpEdit LuePtCode;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.DateEdit DteQtStartDt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueRingCode;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtPreviousDocNo;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueCtCode;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private DevExpress.XtraEditors.CheckEdit ChkPrintSignatureInd;
        private DevExpress.XtraEditors.LookUpEdit LueCtContactPersonName;
        private DevExpress.XtraEditors.LookUpEdit LueAgingAP;
        internal DevExpress.XtraEditors.TextEdit TxtQtMth;
        internal DevExpress.XtraEditors.TextEdit TxtSalesTarget;
        internal DevExpress.XtraEditors.TextEdit TxtCreditLimit;
        internal DevExpress.XtraEditors.TextEdit TxtGoodsReturnDay;
        internal DevExpress.XtraEditors.TextEdit TxtMinDelivery;
        internal DevExpress.XtraEditors.TextEdit TxtDepositAmt;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        protected System.Windows.Forms.Panel panel5;
        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        private DevExpress.XtraEditors.LookUpEdit LueShpMCode;
        private DevExpress.XtraEditors.MemoExEdit MeeWhsCapacity;
        public DevExpress.XtraEditors.SimpleButton BtnCtContactPersonName;





    }
}