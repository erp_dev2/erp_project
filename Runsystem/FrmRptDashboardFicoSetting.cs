﻿#region Update
/*
    20/09/2022 [SET/YK] New Application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Reflection;
using System.IO;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmRptDashboardFicoSetting : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, IsProcFormat = string.Empty,
            mDocNo = string.Empty;
        internal FrmRptDashboardFicoSettingFind FrmFind;
        internal bool 
            mIsSiteMandatory = false, 
            mIsFilterBySite = false,
            IsInsert = false,
            mIsRptFicoSettingUseIndent = false,
            mIsRptFicoSettingUseAlignment = false,
            mIsPrintOutAccountingShowBeginningAndEndingInventory = false;
        internal List<COARow> lCR = null;
        internal List<RptFicoSettingDtl2> lD2 = null;
        internal List<Entity> lEnt = null;
        iGCell fCell;
        bool fAccept;
        
        #endregion

        #region Constructor

        public FrmRptDashboardFicoSetting(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "FICO Report's Setting";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                //Tp2.PageVisible = false;
                lCR = new List<COARow>();
                lD2 = new List<RptFicoSettingDtl2>();
                lEnt = new List<Entity>();
                GetAllEntCode(ref lEnt);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                //Sl.SetLueOption(ref LueFontCode, "PrintFicoFontCode");
                //Sl.SetLueOption(ref LueLineCode, "PrintFicoLineCode");
                //Sl.SetLueOption(ref LueAlignCode, "PrintFicoAlignCode");
                Sl.SetLueOption(ref LueDashboardType, "RptDashboardType");
                //LueFontCode.Visible = LueLineCode.Visible = LueAlignCode.Visible = false;
                //if (mIsSiteMandatory) LblSiteCode.ForeColor = Color.Red;
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "Setting Code",                    
                    //1-5
                    "Setting Description",
                    "Sequence",
                    "",
                    "COA Account#",
                    "Formula",
                    //6-10
                    "Show",
                    "Current Month",
                    "Year to Date",
                    "Debit",
                    "Credit",
                    //11
                    "End. Balance",
                    "CBP",
                    "",
                },
                new int[] 
                {
                    //0
                    100,
                    //1-5
                    200, 80, 20, 300, 150,
                    //6-10
                    50, 100, 100, 50, 50, 
                    //11-13
                    100, 50, 20
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 3, 13 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 4 });
            //Sm.GrdColInvisible(Grd1, new int[] { 6, 8, 12 });
            Sm.GrdColCheck(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 12 });
            Grd1.Cols[13].Move(5);
            //Grd1.Cols[13].Move(10);
            //Grd1.Cols[14].Move(11);
            //if(!mIsRptFicoSettingUseAlignment)
            //    Sm.GrdColInvisible(Grd1, new int[] { 13 });
            //if (!mIsRptFicoSettingUseIndent)
            //    Sm.GrdColInvisible(Grd1, new int[] { 14 });
            //if (!mIsPrintOutAccountingShowBeginningAndEndingInventory) Sm.GrdColInvisible(Grd1, new int[] { 15, 16 });
            #endregion

            #region Grid 2

            Grd2.Cols.Count = 7;
            Grd2.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd2, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Setting Code",
                    "Setting Description",
                    "COA's Account#",
                    "COA's Account Description",
                    "Entity Code",

                    //6
                    "Entity Name"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    100, 250, 100, 250, 100, 

                    //6
                    200
                }
            );
            Sm.SetGrdProperty(Grd2, false);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtRptFicoCode, LueDashboardType, LueSiteCode, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12 });
                    BtnRefreshData.Enabled = true;
                    TxtRptFicoCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtRptFicoCode, LueDashboardType, LueSiteCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12 });
                    BtnRefreshData.Enabled = false;
                    TxtRptFicoCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDashboardType, LueSiteCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12 });
                    BtnRefreshData.Enabled = false;
                    TxtRptFicoCode.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtRptFicoCode, LueDashboardType, LueSiteCode, MeeRemark });
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRptDashboardFicoSettingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                //SetLueDashboardCode(ref LueDashboardType, "");
                SetLueSiteCode(ref LueSiteCode, string.Empty, "");
                IsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtRptFicoCode, "Setting code", false)) return;
            SetFormControl(mState.Edit);
            IsInsert = true;
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtRptFicoCode, "Report code", false) || 
                Sm.StdMsgYN("Delete", string.Empty) == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() 
                { 
                    CommandText =
                        "Delete From TblRptDashboardFicoSettingHdr Where RptDashboardCode=@RptFicoCode;" +
                        "Delete From TblRptDashboardFicoSettingDtl Where RptDashboardCode=@RptFicoCode;" 
                };
                Sm.CmParam<String>(ref cm, "@RptFicoCode", TxtRptFicoCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtRptFicoCode.Properties.ReadOnly)
                    EditData();
                else
                    InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(SaveRptFicoSettingHdr());
            cml.Add(SaveRptFicoSettingDtl());
            //for (int r = 0; r < Grd1.Rows.Count; r++)
            //{
            //    if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
            //    {
            //        string AcNo = string.Empty;
            //        if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
            //        {
            //            int mListIndex = 0;
            //            for (int t = 0; t < lCR.Count; ++t)
            //            {
            //                if (lCR[t].Row == r)
            //                {
            //                    mListIndex = t;
            //                    break;
            //                }
            //            }

            //            AcNo = lCR[mListIndex].AcNo;
            //        }

            //        cml.Add(SaveRptFicoSettingDtl(r, AcNo));
            //    }
            //}

            //if (lD2.Count > 0)
            //{
            //    foreach (var x in lD2)
            //        cml.Add(SaveRptFicoSettingDtl2(x));
            //}

            Sm.ExecCommands(cml);

            ShowData(TxtRptFicoCode.Text);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtRptFicoCode, "Report code", false) ||
                Sm.IsLueEmpty(LueDashboardType, "Report name") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                IsRptFicoCodeAlreadyExisted() ||
                IsGrdEmpty() ||
            IsGrdValueNotValid();
        }

        private bool IsRptFicoCodeAlreadyExisted()
        {
            if (Sm.IsDataExist(
                "Select 1 From TblRptDashboardFicoSettingHdr Where RptDashboardCode=@Param1 " +
                "And RptDashboardType=@Param2 " +
                "And SiteCode=@Param3;",
                TxtRptFicoCode.Text, Sm.GetLue(LueDashboardType), Sm.GetLue(LueSiteCode)
                ))
            {
                Sm.StdMsg(mMsgType.Warning, "Report Code, Report Name and Site is existed!");
                return true;
            }
            else
                return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 setting.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 0, false, "Setting code is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Setting name is empty.")) 
                    return true;

                //if (mIsPrintOutAccountingShowBeginningAndEndingInventory)
                //{
                    //if (Sm.GetGrdBool(Grd1, r, 15) && Sm.GetGrdBool(Grd1, r, 16))
                    //{
                    //    Tc1.SelectedTabPage = Tp1;
                    //    Sm.FocusGrd(Grd1, r, 15);

                    //    var msg = new StringBuilder();

                    //    msg.AppendLine("Setting Code : " + Sm.GetGrdStr(Grd1, r, 0));
                    //    msg.AppendLine("You could only checked either Beginning or Ending Inventory.");

                    //    Sm.StdMsg(mMsgType.Warning, msg.ToString());

                    //    return true;
                    //}

                    // kalau dia pakai Formula, tapi di centang Beginning dan/atau Ending nya
                    // karena kalau di centang salah satunya, rumusnya akan jadi berantakan karna setting code yang di formula juga harus di pasangin Beginning dan/atau Ending Inv juga
                    
                    //if (Sm.GetGrdStr(Grd1, r, 5).Trim().Length > 0 &&
                    //    (
                    //    Sm.GetGrdBool(Grd1, r, 15) ||
                    //    Sm.GetGrdBool(Grd1, r, 16)
                    //    ))
                    //{
                    //    Tc1.SelectedTabPage = Tp1;
                    //    Sm.FocusGrd(Grd1, r, 15);

                    //    var msg = new StringBuilder();

                    //    msg.AppendLine("Setting Code : " + Sm.GetGrdStr(Grd1, r, 0));
                    //    msg.AppendLine("Formula : " + Sm.GetGrdStr(Grd1, r, 5));
                    //    msg.AppendLine("You could not checked the Beginning and/or Ending Inventory when Formula is exists. ");

                    //    Sm.StdMsg(mMsgType.Warning, msg.ToString());

                    //    return true;
                    //}
                //}
            }
            return false;
        }

        private MySqlCommand SaveRptFicoSettingHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRptDashboardFicoSettingHdr(RptDashboardCode, RptDashboardType, SiteCode, Remark, CreateBy, CreateDt) " +
                    "Values(@RptDashboardCode, @RptDashboardName, @SiteCode, @Remark, @UserCode, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@RptDashboardCode", TxtRptFicoCode.Text);
            Sm.CmParam<String>(ref cm, "@RptDashboardName", Sm.GetLue(LueDashboardType));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRptFicoSettingDtl()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var IsFirstOrExisted = true;
            var AcNo = string.Empty;
            int mListIndex = 0;

            SQL.AppendLine("/* FICO reporting Setting (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblRptDashboardFicoSettingDtl ");
                        SQL.AppendLine("(RptDashboardCode, SettingCode, SettingDesc, Sequence, AcNo, Formula, ShowInd, CurMonth, YrtoDt, Debit, Credit, EndBalance, CBP, ");
                        SQL.AppendLine("CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    AcNo = string.Empty;
                    mListIndex = 0;
                    if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                    {
                        for (int t = 0; t < lCR.Count; ++t)
                        {
                            if (lCR[t].Row == r)
                            {
                                mListIndex = t;
                                break;
                            }
                        }
                        AcNo = lCR[mListIndex].AcNo;
                    }

                    SQL.AppendLine(
                        " (@RptDashboardCode, @SettingCode_" + r.ToString() +
                        ", @SettingDesc_" + r.ToString() +
                        ", @Sequence_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @Formula_" + r.ToString() +
                        ", @ShowInd_" + r.ToString() +
                        ", @CurMonth_" + r.ToString() +
                        ", @YrtoDt_" + r.ToString() +
                        ", @Debit_" + r.ToString() +
                        ", @Credit_" + r.ToString() +
                        ", @EndBalance_" + r.ToString() +
                        ", @CBP_" + r.ToString() +
                        ", ");

                    SQL.AppendLine("@UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@SettingCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                    Sm.CmParam<String>(ref cm, "@SettingDesc_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<String>(ref cm, "@Sequence_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), AcNo);
                    Sm.CmParam<String>(ref cm, "@Formula_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
                    Sm.CmParam<String>(ref cm, "@ShowInd_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 6) ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@CurMonth_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 7) ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@YrtoDt_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 8) ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@Debit_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 9) ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@Credit_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 10) ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@EndBalance_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 11) ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@CBP_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 12) ? "Y" : "N");
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@RptDashboardCode", TxtRptFicoCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveRptFicoSettingDtl(int r, string AcNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblRptFicoSettingDtl ");
        //    SQL.AppendLine("(RptFicoCode, SettingCode, SettingDesc, Sequence, AcNo, Formula, Font, Line, Align, IndentInd, PrintInd, ");
        //    if (mIsPrintOutAccountingShowBeginningAndEndingInventory) SQL.AppendLine("BeginningInvInd, EndingInvInd, ");
        //    SQL.AppendLine("CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@RptFicoCode, @SettingCode, @SettingDesc, @Sequence, @AcNo, @Formula, @Font, @Line, @Align, @IndentInd, @PrintInd, ");
        //    if (mIsPrintOutAccountingShowBeginningAndEndingInventory) SQL.AppendLine("@BeginningInvInd, @EndingInvInd, ");
        //    SQL.AppendLine("@UserCode, CurrentDateTime()); ");

        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@RptFicoCode", TxtRptFicoCode.Text);
        //    Sm.CmParam<String>(ref cm, "@SettingCode", Sm.GetGrdStr(Grd1, r, 0));
        //    Sm.CmParam<String>(ref cm, "@SettingDesc", Sm.GetGrdStr(Grd1, r, 1));
        //    Sm.CmParam<String>(ref cm, "@Sequence", Sm.GetGrdStr(Grd1, r, 2));
        //    Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
        //    Sm.CmParam<String>(ref cm, "@Formula", Sm.GetGrdStr(Grd1, r, 5));
        //    Sm.CmParam<String>(ref cm, "@Font", Sm.GetGrdStr(Grd1, r, 6));
        //    Sm.CmParam<String>(ref cm, "@Line", Sm.GetGrdStr(Grd1, r, 8));
        //    Sm.CmParam<String>(ref cm, "@Align", Sm.GetGrdStr(Grd1, r, 12));
        //    Sm.CmParam<String>(ref cm, "@IndentInd", Sm.GetGrdBool(Grd1, r, 14) == true ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@PrintInd", Sm.GetGrdBool(Grd1, r, 10)==true?"Y":"N");
        //    if (mIsPrintOutAccountingShowBeginningAndEndingInventory)
        //    {
        //        Sm.CmParam<String>(ref cm, "@BeginningInvInd", Sm.GetGrdBool(Grd1, r, 15) ? "Y" : "N");
        //        Sm.CmParam<String>(ref cm, "@EndingInvInd", Sm.GetGrdBool(Grd1, r, 16) ? "Y" : "N");
        //    }
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveRptFicoSettingDtl2(RptFicoSettingDtl2 x)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("/* FICO reporting Setting (Dtl2) */ ");
        //    SQL.AppendLine("Set @Dt:=CurrentDateTime();");

        //    SQL.AppendLine("Insert Into TblRptDashboardFicoSettingTestDtl2(RptDashboardCode, SettingCode, AcNo, EntCode, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@RptDashboardCode, @SettingCode, @AcNo, @EntCode, @UserCode, @Dt) ");
        //    SQL.AppendLine("On Duplicate Key Update ");
        //    SQL.AppendLine("    EntCode = @EntCode, LastUpBy=@UserCode, LastUpDt=@Dt; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@RptDashboardCode", TxtRptFicoCode.Text);
        //    Sm.CmParam<String>(ref cm, "@SettingCode", x.SettingCode);
        //    Sm.CmParam<String>(ref cm, "@AcNo", x.AcNo);
        //    Sm.CmParam<String>(ref cm, "@EntCode", x.EntCode.Length <= 0 ? "-" : x.EntCode);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditRptFicoSettingHdr());
            cml.Add(SaveRptFicoSettingDtl());
            //for (int r = 0; r < Grd1.Rows.Count; r++)
            //{
            //    if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
            //    {
            //        string AcNo = string.Empty;
            //        if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
            //        {
            //            int mListIndex = 0;
            //            for (int t = 0; t < lCR.Count; ++t)
            //            {
            //                if (lCR[t].Row == r)
            //                {
            //                    mListIndex = t;
            //                    break;
            //                }
            //            }

            //            AcNo = lCR[mListIndex].AcNo;
            //        }

            //        cml.Add(SaveRptFicoSettingDtl(r, AcNo));
            //    }
            //}

            //if (lD2.Count > 0)
            //{
            //    cml.Add(DeleteRptFicoSettingDtl2());
            //    foreach (var x in lD2)
            //    {
            //        cml.Add(SaveRptFicoSettingDtl2(x));
            //    }
            //}

            Sm.ExecCommands(cml);

            ShowData(TxtRptFicoCode.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                //Sm.IsTxtEmpty(TxtRptFicoCode, "Report code", false) ||
                Sm.IsLueEmpty(LueDashboardType, "Report name") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid(); ;
        }

        private MySqlCommand EditRptFicoSettingHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/*RptFicoSetting - EditRptFicoSettingHdr*/ ");
            SQL.AppendLine("Update TblRptDashboardFicoSettingHdr Set ");
            SQL.AppendLine("    RptDashboardType=@RptDashboardType, SiteCode=@SiteCode, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where RptDashboardCode=@RptDashboardCode; ");

            SQL.AppendLine("Delete From TblRptDashboardFicoSettingDtl Where RptDashboardCode=@RptDashboardCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@RptDashboardCode", TxtRptFicoCode.Text);
            //Sm.CmParam<String>(ref cm, "@RptFicoName", TxtRptFicoName.Text);
            Sm.CmParam<String>(ref cm, "@RptDashboardType", Sm.GetLue(LueDashboardType));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string RptDashboardCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowRptFicoSettingHdr(RptDashboardCode);
                ShowRptFicoSettingDtl(RptDashboardCode);
                //ShowRptFicoSettingDtl2(RptDashboardCode);
                IsInsert = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRptFicoSettingHdr(string RptDashboardCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("/*RptFicoSetting - ShowRptFicoSettingHdr*/ ");
            SQL.AppendLine("Select RptDashboardType, SiteCode, Remark ");
            SQL.AppendLine("From TblRptDashboardFicoSettingHdr ");
            SQL.AppendLine("Where RptDashboardCode=@RptDashboardCode; ");

            Sm.CmParam<String>(ref cm, "@RptDashboardCode", RptDashboardCode);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]{ "RptDashboardType", "SiteCode", "Remark" },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtRptFicoCode.EditValue = RptDashboardCode;
                    //TxtRptFicoName.EditValue = Sm.DrStr(dr, c[0]);
                    SetLueDashboardCode(ref LueDashboardType, Sm.DrStr(dr, c[0]));
                    SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[1]), string.Empty);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[2]);
                }, true
            );
        }

        private void ShowRptFicoSettingDtl(string RptDashboardCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@RptDashboardCode", RptDashboardCode);
                lCR.Clear();

                var SQL = new StringBuilder();

                SQL.AppendLine("/*RptFicoSetting - ShowRptFicoSettingDtl*/ ");
                SQL.AppendLine("Select A.SettingCode, A.SettingDesc, A.Sequence, A.AcNo, A.Formula, A.ShowInd, A.CurMonth, A.YrtoDt, A.Debit, A.Credit, A.EndBalance, A.CBP ");
                SQL.AppendLine("From TblRptDashboardFicoSettingDtl A ");
                //SQL.AppendLine("Left Join TblOption B On A.Font = B.OptCode And B.OptCat = 'PrintFicoFontCode' ");
                //SQL.AppendLine("Left Join TblOption C On A.Line = C.OptCode And C.OptCat = 'PrintFicoLineCode' ");
                //SQL.AppendLine("Left Join TblOption D On A.Align = D.OptCode And D.OptCat = 'PrintFicoAlignCode' ");
                SQL.AppendLine("Where A.RptDashboardCode=@RptDashboardCode Order By A.Sequence;");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "SettingCode", 

                        //1-5
                        "SettingDesc", "Sequence", "AcNo", "Formula", "ShowInd",
                        //6-10
                        "CurMonth", "YrtoDt", "Debit", "Credit", "EndBalance", 
                        //11
                        "CBP", 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Grd.Cells[Row, 4].Value = Sm.DrStr(dr, c[3]).Length == 0 ? null : "[Collection]";
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 12, 11);

                        if (Sm.DrStr(dr, c[3]).Length > 0) lCR.Add(new COARow() { Row = Row, AcNo = Sm.DrStr(dr, c[3]) });
                    }, false, false, true, false
                );
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRptFicoSettingDtl2(string RptDashboardCode)
        {            
            Cursor.Current = Cursors.WaitCursor;

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@RptDashboardCode", RptDashboardCode);
            lD2.Clear();

            var SQL = new StringBuilder();

            SQL.AppendLine("/*RptFicoSetting - ShowRptFicoSettingDtl2*/ ");
            SQL.AppendLine("Select A.SettingCode, A.AcNo, A.EntCode ");
            SQL.AppendLine("From TblRptDashboardFicoSettingTestDtl2 A ");
            SQL.AppendLine("Where A.RptDashboardCode=@RptDashboardCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SettingCode", "AcNo", "EntCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lD2.Add(new RptFicoSettingDtl2()
                        {
                            SettingCode = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            EntCode = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Additional Method

        private void GetAllEntCode(ref List<Entity> x)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select EntCode, EntName ");
            SQL.AppendLine("From TblEntity; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EntCode", "EntName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        x.Add(new Entity() { EntCode = Sm.DrStr(dr, c[0]), EntName = Sm.DrStr(dr, c[1]) });
                }
                dr.Close();
            }
        }

        internal void GetCOAEntList(string SettingCode, string[] Lists)
        {
            if (lD2.Count > 0) lD2.RemoveAll(t => t.SettingCode == SettingCode);
            foreach(var x in Lists)
            {
                string[] data = x.Split('#');
                lD2.Add(new RptFicoSettingDtl2() 
                {
                    SettingCode = SettingCode,
                    AcNo = data[0],
                    EntCode = data[1]
                });
            }
        }

        internal void GetCOAList(int Row, string AcNo)
        {
            if (lCR.Count > 0) lCR.RemoveAll(t => t.Row == Row);
            lCR.Add(new COARow() { Row = Row, AcNo = AcNo });
            if (AcNo.Length == 0) Grd1.Cells[Row, 4].Value = null;
            else Grd1.Cells[Row, 4].Value = "[Collection]";
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsSiteMandatory', 'IsFilterBySite', 'IsRptFicoSettingUseAlignment', 'IsRptFicoSettingUseIndent', 'IsPrintOutAccountingShowBeginningAndEndingInventory' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            #region boolean

                            case "IsSiteMandatory": mIsSiteMandatory = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsRptFicoSettingUseAlignment": mIsRptFicoSettingUseAlignment = ParValue == "Y"; break;
                            case "IsRptFicoSettingUseIndent": mIsRptFicoSettingUseIndent = ParValue == "Y"; break;
                            case "IsPrintOutAccountingShowBeginningAndEndingInventory": mIsPrintOutAccountingShowBeginningAndEndingInventory = ParValue == "Y"; break;

                            #endregion
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetLueSiteCode(ref LookUpEdit Lue, string Code, string IsFilterBySite)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.SiteCode As Col1, ");
            SQL.AppendLine("Concat(A.SiteName, Case When C.EntName Is Null Then '' Else Concat(' (', C.EntName, ')') End) As Col2 ");
            SQL.AppendLine("From TblSite A ");
            SQL.AppendLine("Left Join TblProfitCenter B On A.ProfitCenterCode=B.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity C On B.EntCode=C.EntCode ");
            if (Code.Length > 0)
                SQL.AppendLine("Where A.SiteCode=@Code;");
            else
            {
                SQL.AppendLine("Where A.ActInd='Y' ");
                if (IsFilterBySite == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Order By C.EntName, A.SiteName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }
        
        private void SetLueDashboardCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.RptDashboardType As Col1, ");
            //SQL.AppendLine("Concat(A.SiteName, Case When C.EntName Is Null Then '' Else Concat(' (', C.EntName, ')') End) As Col2 ");
            SQL.AppendLine("B.OptDesc As Col2 ");
            SQL.AppendLine("From TblRptDashboardFicoSettingHdr A ");
            SQL.AppendLine("Left Join TblOption B on A.RptDashboardType = B.OptCode And B.OptCat = 'RptDashboardType' ");
            //SQL.AppendLine("Left Join TblEntity C On B.EntCode=C.EntCode ");
            if (Code.Length > 0)
                SQL.AppendLine("Where A.RptDashboardType=@Code;");
            //else
            //{
            //    SQL.AppendLine("Where A.ActInd='Y' ");
            //    if (IsFilterBySite == "Y")
            //    {
            //        SQL.AppendLine("And Exists( ");
            //        SQL.AppendLine("    Select 1 From TblGroupSite ");
            //        SQL.AppendLine("    Where SiteCode=A.SiteCode ");
            //        SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            //        SQL.AppendLine("    ) ");
            //    }
            //    SQL.AppendLine("Order By C.EntName, A.SiteName;");
            //}

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            //else
            //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void ProcessCOA(string AcNo, ref List<COA> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var lCOA = new List<COA>();

            SQL.AppendLine("Select AcNo From TblCOA ");
            SQL.AppendLine("Where Find_In_Set(AcNo, @AcNo);");

            Sm.CmParam<string>(ref cm, "@AcNo", AcNo);
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        lCOA.Add(new COA(){ AcNo = Sm.DrStr(dr, c[0]) });
                }
                dr.Close();
            }

            if (lCOA.Count > 0) ProcessCOA(ref lCOA, ref l);
        }

        private void ProcessCOA(ref List<COA> lCOA, ref List<COA> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = string.Empty;
            int i = 0;
            SQL.AppendLine("Select AcNo, AcDesc From TblCOA ");
            
            foreach (var x in lCOA)
            {
                i++;
                if (Filter.Length > 0) Filter += " Or ";
                Filter += " (AcNo Like @AcNo"+i.ToString() + ") ";    
                Sm.CmParam<string>(ref cm, "@AcNo"+i.ToString(), string.Concat(x.AcNo, "%"));
            }
            if (Filter.Length > 0)
            {
                SQL.AppendLine("Where (" + Filter + ") ");
                SQL.AppendLine("And AcNo Not In (Select Parent From TblCOA Where Parent is Not Null) ");
                SQL.AppendLine("Order By AcNo; ");
            }
            else
                SQL.AppendLine(" Where 1=0;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA() 
                        { 
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]) 
                        });
                    }
                }
                dr.Close();
            }
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }
        
        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnRefreshData_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtRptFicoCode, "Setting code", false)) return;

            Sm.ClearGrd(Grd2, false);
            var l1 = new List<RptFicoSetting>();
            var l2 = new List<COA>();

            try
            {
                if (Grd1.Rows.Count > 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(Grd1, r, 0).Length > 0 && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                        {
                            int mListIndex = 0;
                            for (int t = 0; t < lCR.Count; ++t)
                            {
                                if (lCR[t].Row == r)
                                {
                                    mListIndex = t;
                                    break;
                                }
                            }

                            //ProcessCOA(Sm.GetGrdStr(Grd1, r, 4), ref l2);
                            ProcessCOA(lCR[mListIndex].AcNo, ref l2);
                            if (l2.Count > 0)
                            {
                                foreach (var x in l2)
                                {
                                    l1.Add(new RptFicoSetting
                                    {
                                        SettingCode = Sm.GetGrdStr(Grd1, r, 0),
                                        SettingDesc = Sm.GetGrdStr(Grd1, r, 1),
                                        AcNo = x.AcNo,
                                        AcDesc = x.AcDesc
                                    });
                                }
                            }
                        }
                    }
                }

                if (l1.Count > 0)
                {
                    iGRow r;
                    Grd2.BeginUpdate();
                    for (var i = 0; i < l1.Count; i++)
                    {
                        r = Grd2.Rows.Add();
                        r.Cells[0].Value = i + 1;
                        r.Cells[1].Value = l1[i].SettingCode;
                        r.Cells[2].Value = l1[i].SettingDesc;
                        r.Cells[3].Value = l1[i].AcNo;
                        r.Cells[4].Value = l1[i].AcDesc;
                        
                        foreach(var x in lD2.Where(w => w.SettingCode == l1[i].SettingCode &&
                            w.AcNo == l1[i].AcNo &&
                            w.EntCode != "-"))
                        {
                            r.Cells[5].Value = x.EntCode;
                            foreach(var y in lEnt.Where(w => w.EntCode == x.EntCode))
                            {
                                r.Cells[6].Value = y.EntName;
                            }
                        }
                    }

                    Grd2.EndUpdate();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            l1.Clear();
            l2.Clear();
        }

        #endregion

        #region Misc Control Event

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(SetLueSiteCode), string.Empty, mIsFilterBySite ? "Y" : "N");
        }

        private void TxtRptFicoCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtRptFicoCode);
        }

        private void LueFontCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueLineCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueAlignCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueDashboardType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDashboardType, new Sm.RefreshLue2(Sl.SetLueOption), "RptDashboardType");
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 3)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmRptDashboardFicoSettingDlg(this, e.RowIndex));
                }
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                //if (e.ColIndex == 0)
                //{
                //    Grd1.Cells[e.RowIndex, 10].Value = true;
                //}

                //if (Sm.IsGrdColSelected(new int[] { 7, 9, 13 }, e.ColIndex))
                //{
                //    if (e.ColIndex == 7) Sm.LueRequestEdit(ref Grd1, ref LueFontCode, ref fCell, ref fAccept, e);
                //    if (e.ColIndex == 9) Sm.LueRequestEdit(ref Grd1, ref LueLineCode, ref fCell, ref fAccept, e);
                //    if (e.ColIndex == 13) Sm.LueRequestEdit(ref Grd1, ref LueAlignCode, ref fCell, ref fAccept, e);
                //    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                //}
                   
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3) Sm.FormShowDialog(new FrmRptDashboardFicoSettingDlg(this, e.RowIndex));
            if (e.ColIndex == 13 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length > 0) Sm.FormShowDialog(new FrmRptDashboardFicoSettingDlg2(this, e.RowIndex));
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 0, 1, 2, 5 }, e);
            if(e.ColIndex == 7)
            {
                if (Sm.GetGrdBool(Grd1, e.RowIndex, 9) == true)
                {
                    Grd1.Cells[e.RowIndex, 10].Value = false;
                    Grd1.Cells[e.RowIndex, 11].Value = false;
                }
            }
            if(e.ColIndex == 8)
            {
                if (Sm.GetGrdBool(Grd1, e.RowIndex, 10) == true)
                {
                    Grd1.Cells[e.RowIndex, 9].Value = false;
                    Grd1.Cells[e.RowIndex, 11].Value = false;
                }
            }
            if(e.ColIndex == 9)
            {
                if (Sm.GetGrdBool(Grd1, e.RowIndex, 11) == true)
                {
                    Grd1.Cells[e.RowIndex, 9].Value = false;
                    Grd1.Cells[e.RowIndex, 10].Value = false;
                }
            }
            if(e.ColIndex == 7)
            {
                if (Sm.GetGrdBool(Grd1, e.RowIndex, 7) == true)
                {
                    Grd1.Cells[e.RowIndex, 8].Value = false;
                }
            }
            if (e.ColIndex == 8)
            {
                if (Sm.GetGrdBool(Grd1, e.RowIndex, 8) == true)
                {
                    Grd1.Cells[e.RowIndex, 7].Value = false;
                }
            }
        }

        private void Grd1_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                e.Text = "Choose COA#";
            }
            else if (e.ColIndex == 13)
            {
                e.Text = "Show Chosen COA# and Entity";
                e.GetType();
            }


        }

        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
        }

        private class RptFicoSetting
        {
            public string SettingCode { get; set; }
            public string SettingDesc { get; set; }
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
        }

        internal class COARow
        {
            public int Row { get; set; }
            public string AcNo { get; set; }
            public string EntCode { get; set; }
        }

        internal class RptFicoSettingDtl2
        {
            public string SettingCode { get; set; }
            public string AcNo { get; set; }
            public string EntCode { get; set; }
        }

        internal class Entity
        {
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        #endregion

    }
}
